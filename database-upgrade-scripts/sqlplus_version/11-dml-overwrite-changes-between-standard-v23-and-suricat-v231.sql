SPOOL 11-dml-overwrite-changes-between-standard-v23-and-suricat-v231.txt
SET TERMOUT OFF;
SET SQLBLANKLINES ON;
SET DEFINE OFF;
ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/SYYYY HH24:MI:SS';
ALTER SESSION SET NLS_TIMESTAMP_TZ_FORMAT = 'MM/DD/SYYYY HH24:MI:SS.FF TZH:TZM';
ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'MM/DD/SYYYY HH24:MI:SS.FF';
ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,';
ALTER SESSION SET NLS_NCHAR_CONV_EXCP = FALSE;
ALTER SESSION SET TIME_ZONE = '+05:30';

UPDATE AFM.AFM_SCMPREF SET UNITS = 0, DOC_CLUSTER_INTERNAL_USE = 'internal.xml' WHERE AFM_SCMPREF = 0;

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '... Equipment (PM Only)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '... Equipment Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Parts Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Cause Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Description Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Resolution Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Repair Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Assigned Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Craftsperson';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Craftsperson by Supervisor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Date to Perform';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Equipment Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Primary Trade';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Priority';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Status';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Parts Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Cause Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Description Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Resolution Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Repair Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information'
 AND SUBTASK = 'Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Parts Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Cause Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Description Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Resolution Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Repair Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Parts Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Cause Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Description Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Resolution Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Repair Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis'
 AND SUBTASK = 'Equipment Failure Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis'
 AND SUBTASK = 'Equipment Maintenance History';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis'
 AND SUBTASK = 'Equipment Replacement Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment by Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment by Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment by Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Parts'
 AND SUBTASK = 'Equipment Bill of Materials';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Parts'
 AND SUBTASK = 'Equipment and Parts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Parts'
 AND SUBTASK = 'Equipment by Part';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Physical Inventory'
 AND SUBTASK = 'Equipment Data Collection Form';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Physical Inventory'
 AND SUBTASK = 'Equipment Labels';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Schedules'
 AND SUBTASK = 'Equipment Production Schedule';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Schedules'
 AND SUBTASK = 'Equipment Work Schedule';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Equipment and Service Contracts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Equipment and Warranties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Equipment by Service Contract';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Equipment by Warranty';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Equipment, Warranties, and Service Contracts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Service Contracts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts'
 AND SUBTASK = 'Warranties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Craftspersons Analysis'
 AND SUBTASK = 'Craftspersons Availability';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Craftspersons Analysis'
 AND SUBTASK = 'Craftspersons Performance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Craftspersons Analysis'
 AND SUBTASK = 'Craftspersons Time Usage by Date and Time';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Craftspersons Analysis'
 AND SUBTASK = 'Craftspersons Time Usage by Work Type';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Craftspersons Analysis'
 AND SUBTASK = 'Craftspersons Workload';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons Labels';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons by Trade';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information'
 AND SUBTASK = 'Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Trades Analysis'
 AND SUBTASK = 'Trades Availability';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Trades Analysis'
 AND SUBTASK = 'Trades Performance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Trades Analysis'
 AND SUBTASK = 'Trades Workload';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Activity Based Costing';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Inventory by Value';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Parts Understocked';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Parts Usage History';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Reserved Parts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Where Used';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Information'
 AND SUBTASK = 'Parts Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Information'
 AND SUBTASK = 'Parts and Alternates';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Information'
 AND SUBTASK = 'Parts and Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Information'
 AND SUBTASK = 'Parts by Vendor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Information'
 AND SUBTASK = 'Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Physical Inventory'
 AND SUBTASK = 'Inventory Labels';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Physical Inventory'
 AND SUBTASK = 'Physical Inventory Check List';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Physical Inventory'
 AND SUBTASK = 'Physical Inventory Variance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = '52 Week PM Work Schedule by ...'
 AND SUBTASK = '... Equipment Weekly';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = '52 Week PM Work Schedule by ...'
 AND SUBTASK = '... Procedure Weekly';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = '52 Week PM Work Schedule by ...'
 AND SUBTASK = '... Trade Monthly';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = '52 Week PM Work Schedule by ...'
 AND SUBTASK = '... Trade Weekly';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...'
 AND SUBTASK = '... Labor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...'
 AND SUBTASK = '... Labor, Parts, and Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...'
 AND SUBTASK = '... Parts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...'
 AND SUBTASK = '... Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Work Forecast by ...'
 AND SUBTASK = '... Date';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Work Forecast by ...'
 AND SUBTASK = '... Trade';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Work Forecast by ...'
 AND SUBTASK = '... Trade by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Craftspersons';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information'
 AND SUBTASK = 'Trades';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Parts Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information'
 AND SUBTASK = 'Vendors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Cause Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Description Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Resolution Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Repair Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Code Compliance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Environmental Compliance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Facility Loss';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Life Safety';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Mission Support';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Code Compliance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Environmental Compliance';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Facility Loss';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Life Safety';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Mission Support';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Classification';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Condition Assessment Date Range';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Cost to Replace';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Estimated Capital Cost to Restore';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Estimated Expense Cost to Restore';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Quarter';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Recommended Action';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Total Cost to Restore';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Condition Assessments by ...'
 AND SUBTASK = '... Condition Rating';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Condition Assessments by ...'
 AND SUBTASK = '... Recommended Action';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Condition Assessments by ...'
 AND SUBTASK = '... Condition Rating';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Condition Assessments by ...'
 AND SUBTASK = '... Recommended Action';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 2 by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 2 by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 3 by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 3 by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...'
 AND SUBTASK = '... Completed';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...'
 AND SUBTASK = '... Issued';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...'
 AND SUBTASK = '... Requested';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...'
 AND SUBTASK = '... Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Employee Trial Layouts for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Employee Trial Layouts for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Employee Trial Layouts for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Equipment Trial Layouts for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Equipment Trial Layouts for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Equipment Trial Layouts for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Furniture Standards Trial Layouts for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Furniture Standards Trial Layouts for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Furniture Standards Trial Layouts for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Tagged Furniture Trial Layouts for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Tagged Furniture Trial Layouts for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Tagged Furniture Trial Layouts for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees From Locations for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees From Locations for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees From Locations for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees To Locations for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees To Locations for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees To Locations for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment From Locations for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment From Locations for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment From Locations for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment To Locations for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment To Locations for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment To Locations for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture From Locations for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture From Locations for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture From Locations for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture To Locations for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture To Locations for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture To Locations for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Employees Layout Report for ...'
 AND SUBTASK = '... Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Employees Layout Report for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Employees Layout Report for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Employees Layout Report for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report for ...'
 AND SUBTASK = '... Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Layout Report for ...'
 AND SUBTASK = '... Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Layout Report for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Layout Report for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Layout Report for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Inventory for ...'
 AND SUBTASK = '... Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Inventory for ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Inventory for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Inventory for ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Layout Report for ...'
 AND SUBTASK = '... Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Layout Report for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Layout Report for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Layout Report for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Building for ...'
 AND SUBTASK = '... Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Building for ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Building for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Building for ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Floor for ...'
 AND SUBTASK = '... Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Floor for ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Floor for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Floor for ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...'
 AND SUBTASK = '... Department by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...'
 AND SUBTASK = '... Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Equipment Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Department by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Furniture Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...'
 AND SUBTASK = '... Furniture Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...'
 AND SUBTASK = '... Survey';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Department by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Furniture Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...'
 AND SUBTASK = '... Department by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...'
 AND SUBTASK = '... Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...'
 AND SUBTASK = '... Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...'
 AND SUBTASK = '... Furniture Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories by Class';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Classes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Companies';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities by State';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities by State by Country';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Counties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Countries';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites by City, State, and Country';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'States';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories by Class';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Classes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Companies';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Counties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Countries';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'States';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories by Class';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Classes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Companies';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Counties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Countries';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'States';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Lease Option by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Lease Responsibility by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...'
 AND SUBTASK = '... City';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...'
 AND SUBTASK = '... Country';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...'
 AND SUBTASK = '... Country, Region, State, City, and Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...'
 AND SUBTASK = '... State';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Communication Log Items by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Communication Log Items by ...'
 AND SUBTASK = '... Lease Activity';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Lease Asset Reports ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Lease Asset Reports ...'
 AND SUBTASK = 'Leasehold Improvements by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Lease Asset Reports ...'
 AND SUBTASK = 'Parking Spaces by Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Lease Asset Reports ...'
 AND SUBTASK = 'Parking Spaces by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Lease Asset Reports ...'
 AND SUBTASK = 'Parking Spaces by Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = '-';

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = 'Lease Abstract';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = 'Lease Abstract (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = 'Lease Communications by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = 'Lease Options by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = 'Lease Responsibilities by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...'
 AND SUBTASK = 'Leases and Base Rents';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...'
 AND SUBTASK = '... City';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...'
 AND SUBTASK = '... Country';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...'
 AND SUBTASK = '... Country, Region, State, City, and Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...'
 AND SUBTASK = '... State';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tenant and Landlord Reports ...'
 AND SUBTASK = 'Landlords Leases by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tenant and Landlord Reports ...'
 AND SUBTASK = 'Tenant and Landlord Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tenant and Landlord Reports ...'
 AND SUBTASK = 'Tenants Leases by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Expirations';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Option Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Option Exercise Dates';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease and Option Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Sublease Expirations';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = '---';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Costs for Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Costs for Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Costs for Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items by Lease Option by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Logs Items by Lease Resp. by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Projects';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Communication Log Items by Lease Activity';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Assets ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Assets ...'
 AND SUBTASK = 'Leasehold Improvement Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Assets ...'
 AND SUBTASK = 'Parking Space Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Assets ...'
 AND SUBTASK = 'Parking Spaces';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Assets ...'
 AND SUBTASK = 'Parking Spaces by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Communications ...'
 AND SUBTASK = 'Lease Communication Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Communications ...'
 AND SUBTASK = 'Lease Communication Log Items by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Options ...'
 AND SUBTASK = 'Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Options ...'
 AND SUBTASK = 'Lease Options by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Responsibilities ...'
 AND SUBTASK = 'Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Responsibilities ...'
 AND SUBTASK = 'Lease Responsibilities by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Leases ...'
 AND SUBTASK = 'Base Rents by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Leases ...'
 AND SUBTASK = 'Lease Contacts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Leases ...'
 AND SUBTASK = 'Leases and Subleases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Leases ...'
 AND SUBTASK = 'Leases by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Cost Class';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Receivables';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... Approve';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... Generate';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... List Exceptions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Budgets ...'
 AND SUBTASK = 'Budget Items by Budget';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Budgets ...'
 AND SUBTASK = 'Budget vs. Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Lease Building Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Lease Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Leases Net Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Leases by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Rent Rolls';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Sublease Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...'
 AND SUBTASK = 'Accounts Receivable';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...'
 AND SUBTASK = 'Costs and Payments by Invoice';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...'
 AND SUBTASK = 'Invoices';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...'
 AND SUBTASK = 'Invoices (Fixed Format) by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...'
 AND SUBTASK = 'Invoices by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...'
 AND SUBTASK = 'Unissued Invoices';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = 'Lease Cash Flow';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = 'Lease Cost Projections';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Companies';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Counties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Countries';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'States';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...'
 AND SUBTASK = '... Landlord';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...'
 AND SUBTASK = '... Tenant';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Regulation Compliance Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Taxes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...'
 AND SUBTASK = 'Comm. Log Items By Activity Log by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...'
 AND SUBTASK = 'Communication Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...'
 AND SUBTASK = 'Communication Log Items By Activity Log Item';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...'
 AND SUBTASK = 'Communication Log Items by Contact';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...'
 AND SUBTASK = 'Communication Log Items by Date';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts ...'
 AND SUBTASK = 'Contacts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts ...'
 AND SUBTASK = 'Contacts by Company';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts ...'
 AND SUBTASK = 'Contacts by Type';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts ...'
 AND SUBTASK = 'Contacts by Type by City';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs ...'
 AND SUBTASK = 'Costs and Sched. Costs by Activity by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs ...'
 AND SUBTASK = 'Costs and Scheduled Costs by Activity Log';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects ...'
 AND SUBTASK = 'Projects';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects ...'
 AND SUBTASK = 'Projects by Contact';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects ...'
 AND SUBTASK = 'Projects by Start Date';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects ...'
 AND SUBTASK = 'Projects by Status';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects ...'
 AND SUBTASK = 'Projects by Type';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Categories by Class';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...'
 AND SUBTASK = 'Cost Classes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Companies';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Cities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Counties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Countries';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'States';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Amenities by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Buildings by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Parcels by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Property Abstract';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Property Abstract (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Property Cost Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Property Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Property by Property Status';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Sold Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...'
 AND SUBTASK = 'Undeveloped Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Activity Type';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Parcels by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Parcels by ...'
 AND SUBTASK = '... Property by County';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Account by Company';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... City';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Country';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Country, Region, State, City, and Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Primary Contact';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...'
 AND SUBTASK = '... State';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Costs for Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Log Items by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Projects';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = '---';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Buildings by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Contacts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Parcels';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Property Amenities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Property Amenities by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...'
 AND SUBTASK = 'Property Amenity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Runoff Information ...'
 AND SUBTASK = 'Runoff Area Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Runoff Information ...'
 AND SUBTASK = 'Runoff Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Runoff Information ...'
 AND SUBTASK = 'Runoff Areas by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Schedules for ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Schedules for ...'
 AND SUBTASK = '... Property Assets';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Schedules for ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'General Ledger Journal Entries for ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'General Ledger Journal Entries for ...'
 AND SUBTASK = '... Property Assets';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'General Ledger Journal Entries for ...'
 AND SUBTASK = '... Tagged Furniture';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Employee';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Parking Spaces by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Leases';

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Cost Class';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...'
 AND SUBTASK = '... Receivables';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... Approve';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... Generate';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... List Exceptions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Regulation Compliance Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...'
 AND SUBTASK = '... Taxes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for ...'
 AND SUBTASK = '... Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for ...'
 AND SUBTASK = '... Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for...'
 AND SUBTASK = '... Regulation Compliance Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for...'
 AND SUBTASK = '... Taxes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Budgets ...'
 AND SUBTASK = 'Budget Items by Budgets';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Budgets ...'
 AND SUBTASK = 'Budget vs. Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Analysis ...'
 AND SUBTASK = 'Property Cost Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Analysis ...'
 AND SUBTASK = 'Property and Building Benchmarks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Analysis ...'
 AND SUBTASK = 'Tax Re-assessment Evaluation';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Property Building Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Property Financial Summary (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Property Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices (Fixed Format) by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices (Fixed Format) by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices (Fixed Format) by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices (Fixed Format) by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = 'Property Cash Flow';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = 'Property Cost Projections';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = 'Tax Cash Flow';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...'
 AND SUBTASK = 'Tax Projections';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Regulation Compliance Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...'
 AND SUBTASK = '... Taxes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Invoices by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items by ...'
 AND SUBTASK = '... Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items by ...'
 AND SUBTASK = '... Regulation';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items by ...'
 AND SUBTASK = '... Type';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Compliance Items by ...'
 AND SUBTASK = '... Authority';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Compliance Items by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Compliance Items by ...'
 AND SUBTASK = '... Regulation';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... Approve';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... Generate';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...'
 AND SUBTASK = '... List Exceptions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log ...'
 AND SUBTASK = '... Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log ...'
 AND SUBTASK = '... Detail';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log ...'
 AND SUBTASK = '... Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Chargeback Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Chargeback Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Chargeback Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...'
 AND SUBTASK = '... Authority';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...'
 AND SUBTASK = '... City';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...'
 AND SUBTASK = '... Parcel';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...'
 AND SUBTASK = '... Property & Parcel';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...'
 AND SUBTASK = '... State';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Combined Building Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Lease Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Financial Profiles ...'
 AND SUBTASK = 'Property Profile (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Details ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Details ...'
 AND SUBTASK = 'Lease Benchmarks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Details ...'
 AND SUBTASK = 'Lease Detail';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Details ...'
 AND SUBTASK = 'Property Detail';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Details ...'
 AND SUBTASK = 'Property and Building Benchmarks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...'
 AND SUBTASK = 'Lease Financial Summary (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...'
 AND SUBTASK = 'Lease Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...'
 AND SUBTASK = 'Property Financial Summary (Fixed Format)';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...'
 AND SUBTASK = 'Property Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...'
 AND SUBTASK = 'Property and Lease Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...'
 AND SUBTASK = '... Lease Responsibilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...'
 AND SUBTASK = '... Regulations';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...'
 AND SUBTASK = '... Taxes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...'
 AND SUBTASK = '... All Activities for a Date Range';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...'
 AND SUBTASK = '... Lease Options';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...'
 AND SUBTASK = '... Leases';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...'
 AND SUBTASK = '... Properties';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...'
 AND SUBTASK = '... Regulations';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...'
 AND SUBTASK = '... Taxes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Account';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...'
 AND SUBTASK = '... Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Forecast Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Forecast Counts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Furniture Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Move Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Personnel Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Total Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Forecast Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Forecast Counts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Furniture Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Move Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Personnel Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Total Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Forecast Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Forecast Counts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Furniture Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Move Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Personnel Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Total Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Forecast Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Forecast Counts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Furniture Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Move Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Personnel Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Total Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Forecast Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Forecast Counts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Furniture Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Move Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Personnel Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Total Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Inventory and Trial Space Budgets for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Inventory and Trial Space Budgets for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Inventory and Trial Space Budgets for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Trial Budget from ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Trial Budget from ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Trial Budget from ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Trial Room Layouts for ...'
 AND SUBTASK = '... Trial 1';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Trial Room Layouts for ...'
 AND SUBTASK = '... Trial 2';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Trial Room Layouts for ...'
 AND SUBTASK = '... Trial 3';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Programmed Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Programmed Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Programmed Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Programmed Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Programmed Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Status ...'
 AND SUBTASK = '... by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Status ...'
 AND SUBTASK = '... by Employee Name';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Status ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Emergency Information ...'
 AND SUBTASK = '... by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Emergency Information ...'
 AND SUBTASK = '... by Employee Name';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Emergency Information ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Chemicals';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Emissions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Energy Use';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Hazardous Waste';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Solid Waste';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Chemicals';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Emissions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Energy Use';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Hazardous Waste';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Solid Waste';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Sustainability Assessments by ...'
 AND SUBTASK = '... Recommended Action';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Sustainability Assessments by ...'
 AND SUBTASK = '... Sustainability Rating';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Sustainability Assessments by ...'
 AND SUBTASK = '... Recommended Action';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Sustainability Assessments by ...'
 AND SUBTASK = '... Sustainability Rating';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Classification';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Cost to Replace';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Estimated Capital Cost to Restore';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Estimated Expense Cost to Restore';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Quarter';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Recommended Action';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Sustainability Assessment Date Range';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Total Cost to Restore';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Active Work by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Active Work by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Active Work by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Active Work by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Active Work by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...'
 AND SUBTASK = '... Region';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 2 by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 2 by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 3 by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 3 by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = '---';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = '----';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Types by Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Types by Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Prorate Report';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Location';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Overview';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Room-by-Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis - Buildings without Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Divisions without Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Prorate Report';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Location Breakdown';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Types by Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Uses';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Overview';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Room-by-Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Use';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'All Areas by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'All Areas by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors by Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'BOMA Chargeback Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Group Chargeback - Detailed Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Group Chargeback - Detailed Analysis - Buildings without Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Group Chargeback - Financial Statement';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Group Chargeback - Financial Stmt - Divisions w/o Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Group Chargeback - Prorate Report';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Location Breakdown';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Facility Percentage Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Facility Percentage Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Facility Percentage Analysis ...'
 AND SUBTASK = '... by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...'
 AND SUBTASK = '... by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis - Buildings without Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Divisions without Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Prorate Report';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Room Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Room Analysis ...'
 AND SUBTASK = '... Location Breakdown';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Room Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Room Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Room Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Types by Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...'
 AND SUBTASK = 'Room Uses';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Overview';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Room-by-Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Use';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Prorate Report';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Location Breakdown';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Type and Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Overview';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... Area Comparison: Room-by-Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Analysis ...'
 AND SUBTASK = '... by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations Analysis ...'
 AND SUBTASK = '... by Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis - All Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis - All Room - Buildings without Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis - Composite';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Detailed Analysis - Composite - Buildings without Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - All Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - All Room - Divisions w/o Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Composite';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Composite - Divisions w/o Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... Summary';

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...'
 AND SUBTASK = '... by Location';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Departments, Divisions, Bus. Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Floors, Buildings and Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Room Types and Categories';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...'
 AND SUBTASK = '... Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...'
 AND SUBTASK = '... by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...'
 AND SUBTASK = '... by Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...'
 AND SUBTASK = '... by Department per Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...'
 AND SUBTASK = '... by Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...'
 AND SUBTASK = '... by Floor per Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Department';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Floor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Room';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Site';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Asset Text Fields';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Asset Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Drawing Publishing Rules Summary';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Layers';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Title Sheets';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Activity Parameters';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Connections';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Drawings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Find Connections with Duplicates';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Find Connections with Invalid Keys';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Telecom:';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Schema Settings ...'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Schema Settings ...'
 AND SUBTASK = 'Check Table for Invalid Data in Field';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Schema Settings ...'
 AND SUBTASK = 'Messages';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Schema Settings ...'
 AND SUBTASK = 'Schema Preferences';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = '---';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'All Room Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Composite Inventory';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Employee Chargeback Views';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Room Percentage Summary Views';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL All Room Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL All Room Pct. Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Employee All Room Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Employee Composite Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Group Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Pct. Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Room Chrgbk. View';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Room Pct. Dist Cat & Type Views';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Update SQL Room Pct. Distinct Org. Views';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Audit Log ...'
 AND SUBTASK = '... Add Table to be Logged';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Audit Log ...'
 AND SUBTASK = '... View Log';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...'
 AND SUBTASK = '... Add Conversion Table';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...'
 AND SUBTASK = '... Add Converted Units Field';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...'
 AND SUBTASK = '... Add Currency Display Field';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...'
 AND SUBTASK = '... Add Source Currency Units Field';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...'
 AND SUBTASK = '... View Conversion Table';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Backbone Cable Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Backbone Cables';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Cable Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Backbone Wireways';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Backbone Wireways & Assigned Cables';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Wireway Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Cable Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Horizontal Cable Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Horizontal Cables';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables'
 AND SUBTASK = 'Horizontal Cables and Pairs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = 'Faceplate Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = 'Faceplates';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = 'Faceplates and Jacks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = 'Jack Configuration';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = 'Jack Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks'
 AND SUBTASK = 'Jacks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = '--';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Horizontal Wireways';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Horizontal Wireways & Assigned Cables';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Horizontal Wireways & Assigned Faceplates';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Horizontal Wireways & Assigned Jacks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways'
 AND SUBTASK = 'Wireway Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms'
 AND SUBTASK = 'All Telecom Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms'
 AND SUBTASK = 'Entrance Facilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms'
 AND SUBTASK = 'Equipment Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms'
 AND SUBTASK = 'Telecom Closets';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = 'Card Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = 'Cards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = 'Cards and Ports by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = 'Cards and Ports by Network Device';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = 'Port Configuration';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards'
 AND SUBTASK = 'Port Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices'
 AND SUBTASK = 'Network Device Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices'
 AND SUBTASK = 'Network Devices';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices'
 AND SUBTASK = 'Network Devices and Ports';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices'
 AND SUBTASK = 'Port Configuration';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices'
 AND SUBTASK = 'Port Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels'
 AND SUBTASK = '---';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels'
 AND SUBTASK = 'Patch Panel Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels'
 AND SUBTASK = 'Patch Panels';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels'
 AND SUBTASK = 'Patch Panels and Ports';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels'
 AND SUBTASK = 'Port Configuration';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels'
 AND SUBTASK = 'Port Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Punch Blocks'
 AND SUBTASK = 'Punch Block Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Punch Blocks'
 AND SUBTASK = 'Punch Blocks';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Standalone Ports'
 AND SUBTASK = 'Port Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Standalone Ports'
 AND SUBTASK = 'Standalone Ports';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = '---';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Equipment and Ports';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Port Configuration';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Port Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Accounts';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Division';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'All Telecom Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Buildings';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Entrance Facilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Equipment Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Floors';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Room Types by Category';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Rooms by Building';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Sites';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Telecom Closets';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Employees';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...'
 AND SUBTASK = '... Completed';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...'
 AND SUBTASK = '... Issued';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...'
 AND SUBTASK = '... Requested';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = '-';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Cause Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Description Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Resolution Codes';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Problem Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications'
 AND SUBTASK = 'Repair Types';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Queries' AND TASK = 'Actual Costs vs. Bldg. Ops. Budgets for Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Queries' AND TASK = 'Actual Costs vs. Bldg. Ops. Budgets for Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Queries' AND TASK = 'Actual Costs vs. Bldg. Ops. Budgets for Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = 'Actual Costs vs. Bldg. Ops. Budgets for Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = 'Actual Costs vs. Bldg. Ops. Budgets for Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = 'Actual Costs vs. Bldg. Ops. Budgets for Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = 'Bldg. Ops. Budgets for Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = 'Bldg. Ops. Budgets for Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Reports' AND TASK = 'Bldg. Ops. Budgets for Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Tables' AND TASK = 'Bldg. Ops. Budgets for Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Tables' AND TASK = 'Bldg. Ops. Budgets for Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Analyze Finances' AND TASK_CAT = 'Tables' AND TASK = 'Bldg. Ops. Budgets for Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Queries' AND TASK = 'Closed Work Requests by Cause Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Queries' AND TASK = 'Closed Work Requests by Problem Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Queries' AND TASK = 'Closed Work Requests by Repair Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Reports' AND TASK = 'Labor Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Reports' AND TASK = 'Work Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed PM Work Orders, Requests and Resources';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Orders and Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Orders, Requests and Resources';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Closed Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Closed Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Actions' AND TASK = 'Create Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Actions' AND TASK = 'Review Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Open Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Actions' AND TASK = 'Estimate Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Queries' AND TASK = 'Trades Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Open Requests by Part';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Backlog by Trade';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Craftspersons';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Parts Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Tools';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Trades';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Assign Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Create Work Order(s)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Issue (Print) Work Order(s)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Orders by Number Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Unissued Work Orders with Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Orders by Number Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Requests for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'On Demand Work Backlog';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'On Hold On Demand Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'Active PM Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'Active PM Work Orders by Number Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'Active PM Work Orders for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'On Hold PM Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'Preventive Maintenance Backlog';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Queries' AND TASK = 'Equipment with Active Work';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Queries' AND TASK = 'Rooms with Active Work';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Orders by Number Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = 'Close Out Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = 'Enter Work Order Completion Dates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = 'Update Work Order Calculations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = 'Update Work Order Details';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Completed Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Closed Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Closed Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Craftspersons';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Parts Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Tools';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Trades';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Parts Inventory Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Tools Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Parts Inventory Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Tools Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Parts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Physical Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment, Warranties, and Service Contracts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Schedules by Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Service Contracts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Queries' AND TASK = 'Craftspersons Workload';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Queries' AND TASK = 'Trades Workload';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Craftspersons Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Trades Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Tables' AND TASK = 'Craftspersons';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Tables' AND TASK = 'Trades';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Actions' AND TASK = 'Adjust Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Inventory Usage';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Actions' AND TASK = 'Survey Physical Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Physical Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Tables' AND TASK = 'Parts Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Tables' AND TASK = 'Parts and Alternates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Tables' AND TASK = 'Parts and Vendors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Tables' AND TASK = 'Vendors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Status of Tools';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tool Types Availability';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tools';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tools Availability';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tools Labels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tools by Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Tables' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Tables' AND TASK = 'Tools';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Queries' AND TASK = '12 Month PM Work Schedule by Trade';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Queries' AND TASK = '52 Week PM Work Schedule by Trade';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = '52 Week PM Work Schedule by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'Overdue Preventive Maintenance Work';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Work Forecast by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Tables' AND TASK = 'PM Procedures and Steps';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Assign Craftspersons and Tools to EQ PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Assign Craftspersons and Tools to HK PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Generate Equipment PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Generate Housekeeping PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Issue (Print) Equipment PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Issue (Print) Housekeeping PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Equipment PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Housekeeping PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Equipment PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Housekeeping PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'PM Procedures and Steps';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedule Groupings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Actions' AND TASK = 'Create Equipment PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Actions' AND TASK = 'Create Housekeeping PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Actions' AND TASK = 'Create Procedures and Steps';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Parts Inventory Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Work Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment PM Schedules with Schedule Dates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Housekeeping PM Schedules by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Housekeeping PM Schedules with Schedule Dates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'PM Schedules by Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'PM Schedules by Primary Trade';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'PM Schedules by Schedule Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Procedures and Steps';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Procedures by Primary Trade';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Procedures, Steps, and Resource Requirements';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'Housekeeping PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'PM Procedures and Steps';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedule Dates by PM Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedule Groupings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Show Single Closed Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Show Single Closed Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Work Order Details';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Create Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Equipment Maintenance History - Air Handlers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Equipment PM Procedures - Air Handlers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Expense Budget Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Update Work Order Details';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Edit Condition Assessments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Manage Condition Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Scoreboard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Empty Site Codes from Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Verify Completed Condition Assessments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Problem Description Codes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Items with Images';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Condition Ratings Greater than 25';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Condition Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Open Safety Issues';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Problem Description Codes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Condition Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Status Statistics by Condition Assessment Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 2 by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 3 by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Building Conditions Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Equipment Condition Assessments by Classification Level 3';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Unsuitable Condition Assessment Items Budget by Priority';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Condition Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Condition Assessment Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Door Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Door Designators';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Door Finishes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Door Frame Finishes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Door Materials';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Door Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Door Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Door Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Door Finishes';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Door Frame Finishes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Door Materials';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Door Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Door Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Doors' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Door Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Room Finish Designators';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Finish - Finishes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Finish - Materials';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Finish Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Finish - Finishes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Finish - Materials';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Room Finishes' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Finish Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Window Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Window Designators';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Window Frame Materials';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Window Glass Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Window Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Window Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Window Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Window Frame Material';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Window Glass Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Window Schedule';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Window Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Design' AND ACT_CLASS = 'Windows' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Window Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Churn Statistics From Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Employees Move History - Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Move History - Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Move History - Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Move History - Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Move History - Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Equipment Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Tagged Furniture Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Equipment General Ledger Journal Entry';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Property Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture General Ledger Journal Entry';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Property Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Tables' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Tables' AND TASK = 'Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Standards Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Standards List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Room Standards and Furniture Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Room Standards and Furniture List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards and Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Move Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Create Employee Move Orders for Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Create Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Edit Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Manage Move Orders by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Room Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Move Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Move Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Move Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Move Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Order by Move Date';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Order by Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Orders by Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Orders by Move Project - Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = 'Move Order Equipment Assignments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = 'Move Order Furniture Standards Assignments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = 'Move Order Tagged Furniture Assignments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = 'Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = 'Move Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Furniture Standard Inventory Trials Counts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Create Move Orders From Trial Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Update Employee Inventory Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Update Equipment Inventory Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Update Furniture Standards Inventory Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Update Tagged Furniture Inventory Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Employee Trial Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Equipment Trial Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Furniture Standards Trial Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Tagged Furniture Trial Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees From Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Employees To Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment From Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment To Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture From Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Tagged Furniture To Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Employees Layout Report for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Layout Report for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Inventory for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Layout Report for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Building for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Floor for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Trial Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Locate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Locate Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Review Furniture Standards Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Tagged Furniture Depreciation';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Chair Replacement Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Locate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Locate Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Review Furniture Standards Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment & Policies for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Policies about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Policy';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Policy by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Policy for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Insurers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Policies for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment by Policy for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Insurers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Leases for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Lease for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Leases about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Lessor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Lessors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Equipment by Lease for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Leases by Lessor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Lessors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Warranties about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Warranties for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Warranties for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Compare Equipment Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Edit New Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Equipment Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Update Equipment Inventory from Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Drawings' AND TASK = 'Display Equipment Inventory and Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Disposition History';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Room Bar Code Labels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Audits';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Actions' AND TASK = 'Enter Equipment Locations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Drawings' AND TASK = 'Lay Out Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Queries' AND TASK = 'Locate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Disposition';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Disposition for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standard Inventory by Policy';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standard Inventory by Policy by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Std. Inventory by Policies about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Insurers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Insurers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standard Inventory by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards and Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Std. Inventory by Leases about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Lessor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Lessors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards and Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Leases by Lessor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Lessors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standard Inventory by Warranty';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Std. Inv. by Warranties about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards and Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Actions' AND TASK = 'Generate Furniture Std. Survey Audit from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Drawings' AND TASK = 'Lay Out Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory Counts by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory Layout Report';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standard Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Add Furniture Standard Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Compare Furniture Standard Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Disposition Survey by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Replace Furniture Std. Inventory with Survey Audit';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Drawings' AND TASK = 'Populate CAD Layout from Standards Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Audits by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inv. Counts by Standard by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory Counts by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards Inventory by ...';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Room Bar Code Labels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standard Audits';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standard Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Insurers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Policies for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Policies about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Policy';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Policy by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Policy for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Insurers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Policies for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture by Policy for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Lessor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Lessors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Leases for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Lease for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Leases about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Leases by Lessor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Lessors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture by Lease for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Warranties about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Warranties for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Warranties for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Warranties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Compare Tagged Furniture Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Edit New Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Update Tagged Furniture Inventory from Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Drawings' AND TASK = 'Display Tagged Furniture and Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Room Bar Code Labels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Disposition History';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Audits';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Actions' AND TASK = 'Enter Tagged Furniture Locations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Furniture Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Drawings' AND TASK = 'Lay Out Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Queries' AND TASK = 'Locate Tagged Furniture';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Counts by Standard by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Disposition';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Disposition for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Inventory by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Layout Report';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Add Required Cost Classes and Cost Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Cities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Counties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Countries';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Floor Stack';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Sites';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw States';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight States by Region';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update All Building Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Gross Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Service Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Vertical Penetrations Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Vertical Penetration Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Common Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Service Areas by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Vertical Penetrations by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Gross Areas by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Building Performance';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetration Analysis Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Service Area Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Vertical Penetration Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Building Areas' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Vertical Penetration Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Approve Base Rent Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Choose Proration and Lease Area Methods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Leasehold Improvement Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Perform Proration and Update Lease Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Base Rent Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Summarize Lease Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Summarize Lease Costs for Current Month';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parking Runoff Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parking Spaces';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Buildings by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Communication Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Lease Asset Reports ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases Abstracts ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Leases by Building by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tenant and Landlord Reports ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Assets ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Communications ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Options ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Responsibilities ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Leases ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Actions' AND TASK = 'Create Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Actions' AND TASK = 'Edit Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Background' AND TASK = 'Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Background' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Background' AND TASK = 'Properties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Queries' AND TASK = 'Budget Items by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Queries' AND TASK = 'Budget Projection';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Queries' AND TASK = 'Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Comparison';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories by Class';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Cost Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Choose Proration and Lease Area Methods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Leasehold Improvement Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Issue Invoices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Perform Proration and Update Lease Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Summarize Lease Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Queries' AND TASK = 'Budget vs. Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Queries' AND TASK = 'Cost History Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Queries' AND TASK = 'Lease Benchmarks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Queries' AND TASK = 'Lease Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Budgets ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories by Class';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Cost History Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Lease Benchmarks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Lease Chargeback Agreements';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Lease Financial Summary (Fixed Format)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Leasehold Improvement Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Cost Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Invoices by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Lease Chargeback Agreements';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Leasehold Improvement Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Actions' AND TASK = 'Choose Proration and Lease Area Methods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Actions' AND TASK = 'Perform Proration and Update Lease Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Actions' AND TASK = 'Update Suite Area from Manual Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Gross Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Service Area Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Vertical Penetration Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Vertical Penetration Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Suites';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Vertical Penetration Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Group Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Groups by Building Drill Down';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Groups by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Groups by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Groups without Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Room Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Rooms by Building Drill Down';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Rooms by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Rooms by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Rooms without Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Suite Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Suites by Building Drill Down';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Suites by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Queries' AND TASK = 'Vacant Suites';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Group Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Groups without Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Room Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Rooms without Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suite Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Suites by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Unaccounted Group Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Unaccounted Room Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Unaccounted Suite Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Reports' AND TASK = 'Vacant Suites';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Groups by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Leases and Subleases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Rooms by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Suites';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Tables' AND TASK = 'Suites by Lease';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Lease Options';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Lease Responsibilities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Parcels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Properties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Regulation Compliance Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Regulations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Activity Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Activity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Communication Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Communication Log Items By Activity Log Item';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Communication Logs By Activity Log Item by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by Activity Log';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by Activity Log by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Projects';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for Properties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Summarize Property Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Update Building and Property Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Cost Categories ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parcel Boundaries';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Property Boundaries';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Runoff Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = 'Areas by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = 'Properties Drill Down';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = 'Properties Drill Down (Fixed Format)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = 'Properties by State Drill Down';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = 'Properties by Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Queries' AND TASK = 'Runoff Areas by Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Abstract Reports ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for Properties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Parcels by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Properties by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Property Activity Log Tickler';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Runoff Area Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Runoff Areas by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Runoff Areas by Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Property Information ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Runoff Information ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Equipment Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Property Assets Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Tagged Furniture Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Edit Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Update Property Parking Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parking Runoff Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parking Spaces';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Queries' AND TASK = 'Parking Spaces by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Queries' AND TASK = 'Parking Spaces by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Schedules for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Property Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Furniture and Equipment Assets by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'General Ledger Journal Entries for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Parking Space Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Parking Spaces by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Property Assets by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Property Assets by Property Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Property Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Depreciation Property Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Parking Space Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Parking Spaces';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Parking Spaces by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Property Assets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Actions' AND TASK = 'Create Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Actions' AND TASK = 'Edit Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Background' AND TASK = 'Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Background' AND TASK = 'Leases';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Background' AND TASK = 'Properties';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Queries' AND TASK = 'Budget Items by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Queries' AND TASK = 'Budget Projection';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Queries' AND TASK = 'Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Comparison';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Items by Budget for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories by Class';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Cost Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Budgets' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Apply Payments to Invoices for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Choose Proration and Lease Area Methods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Issue Invoices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Perform Proration and Update Lease Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Summarize Property Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Update Building and Property Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = 'Budget vs. Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = 'Cost History Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = 'Property Benchmarks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = 'Property Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Queries' AND TASK = 'Property Cost Projections';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Accounts Receivable';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Budgets ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Cost Categories by Class';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Cost History Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Payments by Invoice';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Financial Profiles ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices (Fixed Format) by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Invoices by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Projections ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Scheduled Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Unissued Invoices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Cost Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Invoices by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Property Sale Data';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Recurring Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for Regulation Comp. Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Regulation Compliance Boundaries';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Queries' AND TASK = 'Locate Non-complying Regulation Items by Regulation';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Queries' AND TASK = 'Regulation Areas by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Queries' AND TASK = 'Regulation Areas by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Non-Compliant Regulation Items by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Costs by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Tickler';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Authorities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Compliance Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for Regulation Compliance Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Log Items by Regulation Compliance Item';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Regulation Authorities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Regulation Compliance Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Regulations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for Taxes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Create Tax Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Create Tax Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Edit Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Sum Tax Costs to Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Update Property Tax Rates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Budget Projection';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Overdue Taxes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log Items by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Authority Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Payments Needing Authorization';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Projections by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Reassessment Evaluation';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Taxes Due for Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for Taxes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Activity Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Activity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Cost Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Cost Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Activity Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Activity Log Items by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Activity Log Items by Tax Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Authority Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Costs and Scheduled Costs by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Recurring Costs by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Budget vs. Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Cash Flow';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Financial Profiles ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Details ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Property and Lease Summaries ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Group Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Suite Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Group Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Lease Abstract';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Property Abstract';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Suite Lease Expirations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Taxes Due for Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Allocate';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Enter Affinities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Allocation Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Affinities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Affinities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departments by Strongest Affinity';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Allocations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Allocations by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Affinities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Budgets or Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Business Unit Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Department Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Division Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Functional Group Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Forecast from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Standards Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Budgets or Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare History to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare History to Requirements Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create History from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Historical Trends Analysis - Business Unit Level';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Historical Trends Analysis - Department Level';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Historical Trends Analysis - Division Level';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Inventory and Trial Space Budgets for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Inventory Budget from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Trial Budget from ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Update Room Inventory Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Trial Room Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Trial 1';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Trial 2';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Trial 3';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Trial 1';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Trial 2';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Trial 3';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Trial Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Allocation Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Level Forecast Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Level Forecast Space Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Level Program Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Historical Space Usage Trends';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Layout for Trial 1';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Budgets or Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Requirements Program Budget from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Business Unit Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Department Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Division Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Functional Group Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Standards Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '... by Division';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '... by Employee Name';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '... by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Employee Status ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Equipment Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Room Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update System Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'System Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Egress Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Hazardous Material Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Zones';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Update Equipment Status by Zone';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Update Room Status by Zone';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Egress Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Hazardous Materials Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Systems and their Dependent Zones';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Systems, Zones, and Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Systems, Zones, and Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Advisory Bulletin for Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Advisory Bulletin for Managers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Emergency Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Status ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Emergency Information ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Status by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Escalation Contacts List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Recovery Team';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Site Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Systems Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Systems and Dependent Systems Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Advisory Bulletin for Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Advisory Bulletin for Managers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Contacts by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Emergency Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employees and Emergency Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Escalation Contacts List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Recovery Team';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Recovery Team Call List';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Systems';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Systems and Dependent Systems';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Zones';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Edit Sustainability Assessments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Manage Sustainability Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Scoreboard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Empty Site Codes from Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Verify Completed Sustainability Assessments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Problem Description Codes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Sustainability Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Open Energy Usage Issues';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Problem Description Codes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Sustainability Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Items with Images';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Active Work by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments with Ratings Greater than 25';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Status Statistics by Sustainability Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 1 by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 2 by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Budgets and Ratings by Classification Level 3 by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Building Sustainability Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Equipment Sustainability Assessments by Classification Level 3';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Unsuitable Sustainability Assessment Items Budget by Priority';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Sustainability Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Sustainability Assessment Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Actions' AND TASK = 'Add Vertical Penetration and Service Area Room Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Gross Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Areas by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Areas by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Areas by Site';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Perform Percentage Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Synchronize Room and Percentage Records';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Update Area Totals - Space & Time Percentage';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Update Area Totals - Space Percentage';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Queries' AND TASK = 'Common Area Breakdown';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Breakdown';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Queries' AND TASK = 'Room Type Breakdown';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Tables' AND TASK = 'Allocate Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Actions' AND TASK = 'Perform Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Actions' AND TASK = 'Update Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Actions' AND TASK = 'Update Room Area from Manual Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Common Area Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Occupancy Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = '-----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'All Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Remaining Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Tables' AND TASK = 'Designate Common Area Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Tables' AND TASK = 'Room Types by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Tables' AND TASK = 'Room Uses';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Gross Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Area by Building';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Area by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Area by Site';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Gross Areas' AND TASK_CAT = 'Reports' AND TASK = 'Gross Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform BOMA 96 Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform BOMA Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform Enhanced BOMA Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform Group Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update All Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Group Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Common Area Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Groups by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Groups by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Facility Percentage Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Remaining Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Tables' AND TASK = 'Designate Common Group Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Tables' AND TASK = 'Group Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Tables' AND TASK = 'Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform Room Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update All Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Room Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Room Area from Manual Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight All Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Common Area Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms by Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Occupancy Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Occupiable vs. Non-Occupiable';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'All Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Room Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Remaining Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Classifications ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Room Type and Category Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Tables' AND TASK = 'Designate Common Area Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Tables' AND TASK = 'Room Types by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Tables' AND TASK = 'Room Uses';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Perform Percentage Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Synchronize Room and Percentage Records';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Update Area Totals - Space & Time Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Update Area Totals - Space Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Room Types by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Background Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Queries' AND TASK = 'Common Area Breakdown';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Breakdown';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Queries' AND TASK = 'Room Type Breakdown';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Type and Category Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Tables' AND TASK = 'Allocate Percentages';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Service Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Common Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Service Areas by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Building Performance';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Area Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Reports' AND TASK = 'Service Areas by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Tables' AND TASK = 'Designate Common Service Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Tables' AND TASK = 'Service Area Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Service Areas' AND TASK_CAT = 'Tables' AND TASK = 'Service Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Vertical Penetrations Area Totals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Vertical Penetrations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Vertical Penetrations by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetration Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Reports' AND TASK = 'Vertical Penetrations by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Tables' AND TASK = 'Vertical Penetration Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Vertical Penetration Areas' AND TASK_CAT = 'Tables' AND TASK = 'Vertical Penetrations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Book Room(s)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Cancel Room Booking(s)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Employee Move Orders from Bookings';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Budget from Bookings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Infer Room Bookings Dept. Assignments from Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Room Bookings for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms Without Bookings for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Bookings by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Bookings by Department for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Bookings by Employee';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Bookings for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Bookings with Images for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Rooms Without Bookings for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Rooms Without Bookings with Images for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Bookings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Infer Room Departments from Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Perform Employee Chargeback - All Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Perform Employee Chargeback - Composite';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Employee Headcounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Employee Designators';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Locate Employee';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Occupancy Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Highlight All Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Highlight Groups by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Highlight Rooms by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Locate Employee';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Department Allocation Report - HQ';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Departmental Stack Plan - Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Highlight All Vacant Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Highlight Groups by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Highlight Rooms by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Locate Employee';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Room Standards Report - Exec';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Rooms by Department Chargeback Report';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Cancel a Room Reservation';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Confirm a Room Reservation';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Reserve a Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Buildings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Departments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Divisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Floors';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms Reserved for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms Reserved for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms Reserved for a Date Range with Images';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Reserved Rooms Percent Occupancy for Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Reservations by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Reservations for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Reservations for a Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Reservations for a Date Range with Images';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Amenities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Amenities by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Amenity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Reservations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Unconfirmed Reservations for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Amenities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Amenities by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Amenity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Reservations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Room Reservations by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Export';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Export from View with Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Import';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Read HRIS Data from Access (EM.MDB)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Read HRIS Data from Oracle (oracle_dsn)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Summarize To Excel';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Archive Documents';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Un-archive Documents';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Archived Revisions by Document';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Deleted Document Revisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Documents';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Revisions by Document';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Template Documents';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Archived Revisions by Document';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Deleted Document Revisions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Documents';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Revisions by Document';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Document Management' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Template Documents';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Drawing Publishing' AND ACT = 'NONE' AND TASK_CAT = 'DrawingPublishing' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Drawing Publishing' AND ACT = 'NONE' AND TASK_CAT = 'DrawingPublishing' AND TASK = 'Asset Types and Publishing Rules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Drawing Publishing' AND ACT = 'NONE' AND TASK_CAT = 'DrawingPublishing' AND TASK = 'Drawing Publishing Rules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Drawing Publishing' AND ACT = 'NONE' AND TASK_CAT = 'DrawingPublishing' AND TASK = 'Drawing Publishing Rules Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Drawing Publishing' AND ACT = 'NONE' AND TASK_CAT = 'DrawingPublishing' AND TASK = 'Publish Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Questionnaires' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Questionnaires';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Questionnaires' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Questions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Questionnaires' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Questions by Questionnaire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Questionnaires' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Questionnaires';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Questionnaires' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Questions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Questionnaires' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Questions by Questionnaire';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Schema Settings ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '-----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = '-------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Add Field';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Add Work Wizard SQL Views';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Allow Cascading Deletion on Bldg, Floor, & Division';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Copy Field(s)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Copy Table';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Fields';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Fields by Table';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Re-Create BldgOps PM WO Generation Indexes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Re-Create Telecom Connections Table Index';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Restrict Cascading Deletion on Bldg, Floor, & Div.';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Tables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update BldgOps Work Order and Request Triggers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Emergency Preparedness Triggers';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Projects to V15 Status Codes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Reservations SQL Views';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Service Desk and On Demand Work SQL Views';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Validated Fields by Table';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Validating Tables and the Fields that Reference Them';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Activities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Activity Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Activity Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Modules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Sub-Tasks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Task Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Task Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Task Summary with Sub-Tasks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Tasks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = '-----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = '------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Activities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Activities by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Activities by Domain';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Activity Assignments to Domains and Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Activity Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Assign Processes to Users';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Domains';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Licensed Tables and Their Activities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Process Tasks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Processes and Roles';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Processes by Activity';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Tables Licensed by Activity';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Tasks by Process and Activity';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Users and Processes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Users and Their Profiles';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Users by Process';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Activity Categories Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Activity Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Field Title Translation';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Messages Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Process SubTask Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Process Task Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Process and Roles Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Products Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Table Title Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '-----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '-------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = '--------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Add Primary Key Values to New Schema Validating Tables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Add Schema DB Module to Custom Project DB';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Audit Log ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Convert GDI patterns to AutoCAD';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'List Project and Schema DB Definition Differences';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Remove Project Data From Schema Database';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Reset Schema';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Run SQL Script';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Update Project Wizard';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Update Schema';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = 'Decrypt Security Account Passwords';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = 'Encrypt Security Account Passwords';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = 'Set Windows Application Security Off';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = 'Set Windows Application Security On';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = '----';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = '------';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Activities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Activity Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Assign Processes to Users';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Create User Identities from Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Database Field Security Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Hotlist';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Modules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Roles';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Roles and Their Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Roles and Their Groups and Users';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Schema Preferences';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Security Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Sub Tasks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Synchronize User and Employee Identities';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Task Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Task Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Tasks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Users';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Convert Windows View File to Web View file';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Publish As Web Template (for CF 3.x)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Publish As Web Template (for CF MX, 5.x, 4.x)';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Hotlist';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Web Template Publishing' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Hotlist';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Workflow Rules' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Workflow Rules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Workflow Rules' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Workflow Rules by Activity';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Workflow Rules' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Workflow Rules';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Workflow Rules' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Workflow Rules by Activity';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Actions' AND TASK = 'Connect';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Backbone Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Backbone Wireways';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Backbone Cable Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Backbone Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Backbone Wireways';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Backbone Wireways & Assigned Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Backbone Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Actions' AND TASK = 'Connect';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Actions' AND TASK = 'Create Jacks for Faceplates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Actions' AND TASK = 'Review Jacks Not Connected to the Telecom Closet';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Actions' AND TASK = 'Update Telecom Links';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Faceplates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Horizontal Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Horizontal Wireways';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Telecom Grid';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Queries' AND TASK = 'Horizontal Cabling Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Queries' AND TASK = 'Quick Trace';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Data Jack Horizontal Terminations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Faceplates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Horizontal Cable Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Horizontal Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Horizontal Cables by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Horizontal Wireways';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Horizontal Wireways & Assigned Components';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Jacks by Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Reports' AND TASK = 'Voice Jack Horizontal Terminations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Cables';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Faceplates and Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Horizontal Cabling' AND TASK_CAT = 'Tables' AND TASK = 'Wireways';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Connect';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Create Ports for Cards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Create Ports for Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Create Ports for Network Devices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Create Ports for Patch Panels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Show Equipment Served by Network Device';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Room Types by Category';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Network Devices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Patch Panels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Punch Blocks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Racks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Telecom Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'All Telecom Area Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'Entrance Facility Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'Equipment Room Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'Quick Trace';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'Telecom Closet Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'All Telecom Areas Summary';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Network Devices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Patch Panels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Punch Blocks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Review Entrance Facility';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Review Equipment Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Review Telecom Closet';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Telecom Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Cards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Network Devices';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Patch Panels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Punch Blocks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Racks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Standalone Ports';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = 'Auto-Connect Work Area Equipment to Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = 'Connect';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = 'Review Work Area Equipment Not Connected to Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = 'Update Telecom Links';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Data Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Voice Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Work Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Queries' AND TASK = 'Quick Trace';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Queries' AND TASK = 'Work Area Equipment Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Data Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Data Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Data Equipment and Peripherals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Voice Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Voice Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Work Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Work Area Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Work Area Equipment by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Data Equipment';

UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Data Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Data Equipment and Peripherals';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Voice Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Voice Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Work Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Work Area Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Work Area Equipment by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Hierarchy Levels';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Edit Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Manage Move Orders by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Room Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Move Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Order by Move Date';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Order by Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Orders Status - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Orders by Move Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Move Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Move Order Equipment Assignments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Move Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Connect';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Telecom Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Telecom Links';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Locate Employee with Telecom Profile';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Quick Trace';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Show Equipment Served by Network Device';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '---';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Combined Extension Directory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Data Jack Horizontal Terminations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Review Telecom Area';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Voice Jack Horizontal Terminations';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Area Equipment by Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Close Out Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Connect';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Telecom Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Issue and Print Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Locate Employee with Telecom Profile';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Quick Trace';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Review Telecom Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Work Order Details';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Orders by Numeric Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Open Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Unissued Work Orders with Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Backlog';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Accounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Classifications';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Actions' AND TASK = 'Auto-Link Employees to Faceplates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Actions' AND TASK = 'Review Employees Linked to Faceplates and Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Actions' AND TASK = 'Review Employees Not Linked to Faceplates';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Actions' AND TASK = 'Update Telecom Links';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Queries' AND TASK = 'Locate Employee with Telecom Profile';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = 'Employee Telecom Directory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = 'Employee Telecom Directory by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = 'Employee Telecom Profile';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Data Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Voice Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Tables' AND TASK = 'Employee Telecom Directory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Tables' AND TASK = 'Employee Telecom Directory by Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Employees' AND TASK_CAT = 'Tables' AND TASK = 'Employee Telecom Profile';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Actions' AND TASK = 'Update Combined Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = 'Combined Extension Directory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = 'Combined Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = 'Employee Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = 'Room Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = 'Voice Jack Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Reports' AND TASK = 'Voice Jacks With Multiple Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Tables' AND TASK = 'Combined Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Tables' AND TASK = 'Employee Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Tables' AND TASK = 'Room Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Tables' AND TASK = 'Voice Jack Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Employees and Extensions' AND ACT = 'Telephone Extensions' AND TASK_CAT = 'Tables' AND TASK = 'Voice Jacks With Multiple Extensions';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Reports' AND TASK = 'Employees by Net Segment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Net Segment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Reports' AND TASK = 'Net Segments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Reports' AND TASK = 'Network Devices by Net Segment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Tables' AND TASK = 'Assign Employees to Net Segment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Tables' AND TASK = 'Assign Equipment to Net Segment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Tables' AND TASK = 'Assign Network Devices to Net Segment';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Tables' AND TASK = 'Net Segments';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Reports' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Reports' AND TASK = '--';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Reports' AND TASK = 'Client Software Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Reports' AND TASK = 'Server Software Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Reports' AND TASK = 'Software Inventory by Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Reports' AND TASK = 'Software Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Tables' AND TASK = '-';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Tables' AND TASK = 'Client Software Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Tables' AND TASK = 'Server Software Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = NULL WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Software' AND TASK_CAT = 'Tables' AND TASK = 'Software Standards';

UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ac';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'acbu';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'active_plantypes';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'activity_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'activity_log_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'activity_log_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'activitytype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'actscns';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'advisory';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_act_tbls';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_activities';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_activity_cats';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_activity_params';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_actprods';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_acts';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_atyp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_bim_categories';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_bim_families';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_bim_params';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_cal_dates';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_cats';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_class';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_conn_flds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_conn_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_conn_logh';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_conn_rule_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_connector';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_conversions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_currencies';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_data_event_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_docs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_docvers';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_docversarch';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_dwgpub';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_dwgs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_dwgvers';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_flds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_flds_lang';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_groups';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_groupsforroles';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_hmetric_trend_values';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_holiday_dates';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_hotlist';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_layr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_legal';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_definitions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_gran_defs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_grans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_notify';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_scard_defs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_scards';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_metric_trend_values';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_mob_dev_reg_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_mobile_apps';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_mobile_menu';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_mobile_sync_history';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_mobile_table_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_mods';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_notifications_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_processes';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_products';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_psubtasks';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_ptasks';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_redlines';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_roleprocs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_roles';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_scmpref';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_subtasks';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_tasks';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_tbl_map';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_tbls';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_tccn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_tclevel';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_tctrace';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_timezones';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_titlesheet';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_userprocs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_users';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_wf_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_wf_rules';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'afm_wf_steps';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'aisle';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ap';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'asset_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bar_code_list';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bas_data_clean_num';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bas_data_point';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bas_data_time_norm_num';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bas_measurement_scope';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_archive';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_connector';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_line';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_line_archive';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_proration_group';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bill_unit';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bim_param_vals';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bin';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bl_amenity';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'blbu';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'bu';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'budget';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'budget_item';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ca';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cabinet';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'card';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cardstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'castd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'catype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'causetyp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_accredit_person';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_accredit_source';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_accredit_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hazard_rank';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hazard_rating';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hazard_status';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hcm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hcm_class';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hcm_cond';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hcm_loc_typ';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hcm_loc_typ_chk';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_hcm_places';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_sample_comp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_sample_result';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_samples';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cb_units';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cbudcmpr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ccost_sum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ccostprjn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ccshflw';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cdport';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cf_work_team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'checkin';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'city';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'coa_cost_group';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'coa_source';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'combext';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'commsubs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'commtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'company';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'compliance_locations';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'contact';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_class';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_index';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_index_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_index_values';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_tran';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_tran_recur';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cost_tran_sched';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'county';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'csi';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ctry';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'cw';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'dep_reports';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'doc_templates';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'doccat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'docfolder';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'docs_assigned';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'docs_assigned_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'doctype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'dp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'dpbu';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'dr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'drfnsh';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'drfrmatl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'drmatl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'drstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'drtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'dv';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_chemicals';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_em_ppe_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_cause_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_injury_areas';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_injury_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_long_tm_ca';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_short_tm_ca';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_witness';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incident_witness_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incidents';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_incidents_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_medical_mon_results';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_medical_monitoring';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_ppe_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_restriction_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_restrictions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_training';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_training_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_training_results';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_training_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_work_cat_med_mon';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_work_cat_ppe_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ehs_work_cat_training';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'em';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'em_compinvtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'em_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'em_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'emstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'emsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'energy_bl_svc_period';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'energy_chart_point';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'energy_time_period';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ep';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_audit';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_bar_code_list';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_compinvsur';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_compinvtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_dep';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_eqstdcnts';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_req_items';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_reserve';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_rm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_sched';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_system';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eq_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eqport';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eqprph';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eqstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'eqstd_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'facility_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_analyses';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_analyses_flds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_loc_group';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_matrix';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_matrix_flds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_params';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_sum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'finanal_sum_life';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fn_compinvsur';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fn_compinvtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fn_comptrialtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fn_fnstdcnts';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fn_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fnstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fpstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'funding';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'fw';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_credits';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_docs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_levels';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_proj';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_scores';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_cert_std';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_airc_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_carbon_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_comm_airc_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_egrid_subregions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_egrid_zip';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_emiss_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_energystar_fuels';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_fuel_dens_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_fuel_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_fuels';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_gwp_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_heat_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_mobile_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_oxid_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_refrig_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s1_co_airc';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s1_refrig_ac';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s1_s3_mobile';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s2_purch_e';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s3_em_air';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s3_mat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s3_outs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s3_serv';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s3_waste_liq';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s3_waste_sol';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_s_other';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_sectors';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_setup';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_totals';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_versions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_waste_liq_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gb_fp_waste_sol_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'geo_region';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gpstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gpsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'grid';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'gros';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'grp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'grp_agency';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'grp_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'grp_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'grp_use';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hactivity_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hazard_container_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hazard_container_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'helpdesk_roles';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'helpdesk_sla_request';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'helpdesk_sla_response';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'helpdesk_sla_steps';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'helpdesk_step_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hhelpdesk_step_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hist_em_count';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hprorate';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hreserve';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hreserve_rm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hreserve_rs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hrmpct';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwo';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwr_other';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrcf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrcfana';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrpt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrtl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrtr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'hwrtt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'insurer';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'invoice';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'invoice_line_item';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'invoice_payment';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'it';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'jk';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'jkcfg';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'jkext';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'jkstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'jw';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'lang_enum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'lang_files';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'lang_glossary';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'lang_lang';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'lang_strings';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'lessor';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_alert_definition';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_amendment';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_cam_profile';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_cam_rec_report';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_chrgbck_agree';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_clause_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_comm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_comm_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_contacts';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_index_profile';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ls_resp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'messages';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo_churn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo_eq';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo_fncount';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo_scenario';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo_scenario_em';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mo_ta';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mobile_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'mp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_chemical';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_constituent';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_h_chemical';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_h_constituent';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_h_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_h_haz_classification';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_h_location';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_haz_classification';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_hazard_category';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_hazard_class';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_hazard_system';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_location';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'msds_location_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ndport';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'net';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'netdev';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'netdevstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'notifications';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'notify_templates';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'notifycat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'op';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'org';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ot';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'other_rs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pa';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pa_dep';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'parcel';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'parking';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'parkingstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pb';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pbstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'plantype_groups';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmforecast_tr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmforecast_trm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmgen';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmgp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmps';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmpspt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmpstr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmpstt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmpsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmressum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pms';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pmsd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pnport';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pnstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'po';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'po_line';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'policy';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'port';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'portcfg';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'portfolio_scenario';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'portstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'probtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'prog_budget_items';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'program';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'programtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'proj_forecast';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'proj_forecast_item';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'project';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'projecttype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'projfunds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'projphase';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'projscns';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'projteam';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'prop_amenity';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'prop_amenity_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'property';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'property_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pt_store_loc';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pt_store_loc_pt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'pv';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'questionnaire';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'questionnaire_map';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'questions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rack';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'recovery_team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regcat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regcomplevel';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regcompliance';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regloc';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regnotify';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regprogcat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regprogram';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regprogtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regreq_pmp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regreqcat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regrequirement';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regulation';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regviolation';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'regviolationtyp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'repairty';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'resavail';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'reserve';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'reserve_rm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'reserve_rs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'resource_std';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'resources';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rf_activity';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rf_reader';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rffnsh';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rfmatl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_amenity';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_amenity_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_arrange';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_arrange_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_config';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_reserve';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_resource_std';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rm_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmcat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmpct';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmpctmob_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmstd_emstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmstd_fn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmtype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'rmuse';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'runoffarea';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'runofftype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'sb';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'sb_items';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'sbaffin';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'sbperiods';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'scenario';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'serv';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'servcont';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'servstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'servsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'shelf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'site';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'softinv';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'softstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'state';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'status_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'su';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'supply_req';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'survey';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'surveymob_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'surveyrm_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'system_bl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'system_dep';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'system_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_audit';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_compinvsur';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_compinvtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_dep';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_fnstdcnts';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_lease';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'ta_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'team_assoc';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'team_category';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'team_properties';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'telext';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'tl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'tr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'trialproject';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'tt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vat_percent';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vert';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vertstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vertsum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'visitors';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vn_ac';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vn_rate';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vn_svcs_contract';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_bl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_groups';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_groupstoroles';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_groupstousers';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_legal';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_rest';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'vpa_site';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'warranty';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_areas';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_categories';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_dispositions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_facilities';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_generators';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_manifests';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_mgmt_methods';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_mgmt_methods_groups';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_out';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_profile_reg_codes';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_profiles';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'waste_regulated_codes';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'weather_model';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'weather_station';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'weather_station_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'weather_station_data_sum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'weather_station_source';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wnfrmatl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wngltype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wnstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wntype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wo';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_categories';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_categories_em';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_pkg_bids';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_pkgs';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_roles';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_roles_location';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'work_team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'workflow_substitutes';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wr_other';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wr_other_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wr_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrcf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrcf_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrpt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrpt_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrtl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrtl_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrtr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrtr_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wrtt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wy';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'wytype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = NULL WHERE TABLE_NAME = 'zone';

UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'ACT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'AET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'AGT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'ART';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'AST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Abidjan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Accra';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Addis_Ababa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Algiers';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Asmera';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Bamako';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Bangui';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Banjul';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Bissau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Blantyre';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Brazzaville';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Bujumbura';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Cairo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Casablanca';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Ceuta';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Conakry';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Dakar';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Dar_es_Salaam';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Djibouti';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Douala';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/El_Aaiun';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Freetown';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Gaborone';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Harare';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Johannesburg';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Kampala';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Khartoum';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Kigali';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Kinshasa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Lagos';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Libreville';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Lome';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Luanda';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Lubumbashi';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Lusaka';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Malabo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Maputo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Maseru';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Mbabane';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Mogadishu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Monrovia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Nairobi';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Ndjamena';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Niamey';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Nouakchott';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Ouagadougou';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Porto-Novo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Sao_Tome';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Timbuktu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Tripoli';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Tunis';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Africa/Windhoek';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Adak';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Anchorage';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Anguilla';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Antigua';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Araguaina';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Buenos_Aires';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Catamarca';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/ComodRivadavia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Cordoba';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Jujuy';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/La_Rioja';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Mendoza';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Rio_Gallegos';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/San_Juan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Tucuman';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Argentina/Ushuaia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Aruba';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Asuncion';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Atikokan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Atka';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Bahia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Barbados';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Belem';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Belize';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Blanc-Sablon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Boa_Vista';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Bogota';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Boise';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Buenos_Aires';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Cambridge_Bay';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Campo_Grande';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Cancun';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Caracas';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Catamarca';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Cayenne';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Cayman';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Chicago';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Chihuahua';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Coral_Harbour';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Cordoba';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Costa_Rica';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Cuiaba';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Curacao';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Danmarkshavn';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Dawson';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Dawson_Creek';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Denver';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Detroit';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Dominica';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Edmonton';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Eirunepe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/El_Salvador';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Ensenada';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Fort_Wayne';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Fortaleza';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Glace_Bay';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Godthab';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Goose_Bay';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Grand_Turk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Grenada';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Guadeloupe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Guatemala';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Guayaquil';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Guyana';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Halifax';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Havana';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Hermosillo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indiana/Indianapolis';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indiana/Knox';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indiana/Marengo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indiana/Petersburg';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indiana/Vevay';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indiana/Vincennes';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Indianapolis';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Inuvik';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Iqaluit';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Jamaica';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Jujuy';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Juneau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Kentucky/Louisville';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Kentucky/Monticello';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Knox_IN';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/La_Paz';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Lima';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Los_Angeles';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Louisville';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Maceio';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Managua';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Manaus';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Martinique';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Mazatlan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Mendoza';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Menominee';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Merida';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Mexico_City';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Miquelon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Moncton';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Monterrey';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Montevideo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Montreal';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Montserrat';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Nassau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/New_York';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Nipigon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Nome';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Noronha';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/North_Dakota/Center';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/North_Dakota/New_Salem';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Panama';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Pangnirtung';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Paramaribo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Phoenix';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Port-au-Prince';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Port_of_Spain';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Porto_Acre';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Porto_Velho';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Puerto_Rico';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Rainy_River';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Rankin_Inlet';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Recife';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Regina';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Rio_Branco';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Rosario';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Santiago';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Santo_Domingo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Sao_Paulo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Scoresbysund';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Shiprock';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/St_Johns';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/St_Kitts';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/St_Lucia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/St_Thomas';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/St_Vincent';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Swift_Current';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Tegucigalpa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Thule';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Thunder_Bay';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Tijuana';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Toronto';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Tortola';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Vancouver';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Virgin';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Whitehorse';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Winnipeg';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Yakutat';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'America/Yellowknife';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Casey';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Davis';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/DumontDUrville';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Mawson';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/McMurdo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Palmer';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Rothera';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/South_Pole';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Syowa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Antarctica/Vostok';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Arctic/Longyearbyen';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Aden';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Almaty';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Amman';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Anadyr';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Aqtau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Aqtobe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Ashgabat';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Ashkhabad';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Baghdad';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Bahrain';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Baku';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Bangkok';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Beirut';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Bishkek';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Brunei';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Calcutta';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Choibalsan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Chongqing';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Chungking';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Colombo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Dacca';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Damascus';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Dhaka';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Dili';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Dubai';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Dushanbe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Gaza';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Harbin';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Hong_Kong';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Hovd';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Irkutsk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Istanbul';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Jakarta';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Jayapura';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Jerusalem';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Kabul';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Kamchatka';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Karachi';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Kashgar';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Katmandu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Krasnoyarsk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Kuala_Lumpur';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Kuching';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Kuwait';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Macao';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Macau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Magadan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Makassar';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Manila';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Muscat';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Nicosia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Novosibirsk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Omsk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Oral';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Phnom_Penh';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Pontianak';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Pyongyang';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Qatar';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Qyzylorda';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Rangoon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Riyadh';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Riyadh87';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Riyadh88';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Riyadh89';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Saigon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Sakhalin';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Samarkand';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Seoul';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Shanghai';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Singapore';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Taipei';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Tashkent';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Tbilisi';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Tehran';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Tel_Aviv';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Thimbu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Thimphu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Tokyo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Ujung_Pandang';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Ulaanbaatar';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Ulan_Bator';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Urumqi';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Vientiane';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Vladivostok';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Yakutsk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Yekaterinburg';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Asia/Yerevan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Azores';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Bermuda';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Canary';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Cape_Verde';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Faeroe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Jan_Mayen';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Madeira';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Reykjavik';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/South_Georgia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/St_Helena';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Atlantic/Stanley';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/ACT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Adelaide';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Brisbane';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Broken_Hill';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Canberra';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Currie';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Darwin';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Hobart';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/LHI';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Lindeman';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Lord_Howe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Melbourne';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/NSW';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/North';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Perth';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Queensland';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/South';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Sydney';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Tasmania';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Victoria';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/West';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Australia/Yancowinna';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'BET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'BST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Brazil/Acre';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Brazil/DeNoronha';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Brazil/East';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Brazil/West';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'CAT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'CET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'CNT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'CST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'CST6CDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'CTT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Atlantic';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Central';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/East-Saskatchewan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Eastern';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Mountain';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Newfoundland';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Pacific';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Saskatchewan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Canada/Yukon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Chile/Continental';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Chile/EasterIsland';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Cuba';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'EAT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'ECT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'EET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'EST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'EST5EDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Egypt';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Eire';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+0';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+1';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+10';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+11';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+12';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+2';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+3';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+4';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+5';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+6';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+7';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+8';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT+9';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-0';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-1';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-10';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-11';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-12';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-13';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-14';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-2';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-3';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-4';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-5';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-6';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-7';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-8';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT-9';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/GMT0';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/Greenwich';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/UCT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/UTC';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/Universal';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Etc/Zulu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Amsterdam';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Andorra';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Athens';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Belfast';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Belgrade';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Berlin';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Bratislava';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Brussels';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Bucharest';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Budapest';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Chisinau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Copenhagen';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Dublin';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Gibraltar';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Guernsey';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Helsinki';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Isle_of_Man';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Istanbul';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Jersey';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Kaliningrad';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Kiev';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Lisbon';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Ljubljana';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/London';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Luxembourg';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Madrid';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Malta';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Mariehamn';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Minsk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Monaco';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Moscow';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Nicosia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Oslo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Paris';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Prague';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Riga';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Rome';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Samara';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/San_Marino';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Sarajevo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Simferopol';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Skopje';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Sofia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Stockholm';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Tallinn';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Tirane';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Tiraspol';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Uzhgorod';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Vaduz';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Vatican';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Vienna';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Vilnius';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Volgograd';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Warsaw';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Zagreb';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Zaporozhye';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Europe/Zurich';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'GB';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'GB-Eire';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'GMT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'GMT0';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Greenwich';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'HST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Hongkong';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'IET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'IST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Iceland';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Antananarivo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Chagos';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Christmas';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Cocos';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Comoro';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Kerguelen';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Mahe';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Maldives';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Mauritius';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Mayotte';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Indian/Reunion';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Iran';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Israel';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'JST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Jamaica';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Japan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Kwajalein';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Libya';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'MET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'MIT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'MST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'MST7MDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Mexico/BajaNorte';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Mexico/BajaSur';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Mexico/General';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Mideast/Riyadh87';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Mideast/Riyadh88';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Mideast/Riyadh89';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'NET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'NST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'NZ';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'NZ-CHAT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Navajo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'PLT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'PNT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'PRC';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'PRT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'PST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'PST8PDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Apia';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Auckland';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Chatham';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Easter';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Efate';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Enderbury';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Fakaofo';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Fiji';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Funafuti';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Galapagos';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Gambier';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Guadalcanal';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Guam';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Honolulu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Johnston';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Kiritimati';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Kosrae';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Kwajalein';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Majuro';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Marquesas';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Midway';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Nauru';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Niue';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Norfolk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Noumea';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Pago_Pago';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Palau';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Pitcairn';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Ponape';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Port_Moresby';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Rarotonga';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Saipan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Samoa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Tahiti';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Tarawa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Tongatapu';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Truk';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Wake';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Wallis';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Pacific/Yap';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Poland';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Portugal';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'ROK';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Singapore';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/AST4';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/AST4ADT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/CST6';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/CST6CDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/EST5';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/EST5EDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/HST10';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/MST7';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/MST7MDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/PST8';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/PST8PDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/YST9';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'SystemV/YST9YDT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Turkey';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'UCT';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Alaska';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Aleutian';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Arizona';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Central';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/East-Indiana';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Eastern';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Hawaii';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Indiana-Starke';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Michigan';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Mountain';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Pacific';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Pacific-New';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'US/Samoa';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'UTC';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Universal';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'VST';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'W-SU';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'WET';
UPDATE AFM.AFM_TIMEZONES SET TIMEZONE_ID_FR = NULL WHERE TIMEZONE_ID = 'Zulu';

UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Dispatch';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Employee Acceptance';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Escalation for Completion';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Escalation for Response';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to Dispatcher';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to Vendor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Request Approved';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Vendor Acceptance';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'CANCELLED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'CLOSED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED' AND STEP = 'Completion Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED' AND STEP = 'Satisfaction Survey';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED' AND STEP = 'Verification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED-V' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'CREATED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROCESS-H' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Escalation for Completion';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Forward to Vendor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Issue Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REJECTED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REJECTED' AND STEP = 'Escalation for Completion';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REJECTED' AND STEP = 'Escalation for Response';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REJECTED' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REJECTED' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Edit and Approve';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Escalation for Completion';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Escalation for Response';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Facility Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Financial Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward Review';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to Dispatcher';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to Vendor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Manager Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Manager Approval 2';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'STOPPED' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Dispatch';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Estimation';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Estimation Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Request Approved';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Schedule Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Scheduling';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Estimation';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Estimation Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Schedule Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Scheduling';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Can' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Clo' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Clo' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Com' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Com' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Com' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Com' AND STEP = 'Satisfaction Survey';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Com' AND STEP = 'Verification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Forward to Craftsperson';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Forward to Craftsperson';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Forward to Craftsperson';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Forward to Craftsperson';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Work Request Issued';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Edit and Approve';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Facility Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Financial Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward Review';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to Dispatcher';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to Vendor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Manager Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Manager Approval 2';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Notification';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'REQUESTED' AND STEP = 'Can';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Basic';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Edit and Approve - Rejected';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Estimation - Rejected';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Forward to Supervisor';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Return';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Scheduling - Rejected';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'S' AND STEP = 'Basic';

UPDATE AFM.EM SET AREA_RM = 231.36, BL_ID = '000006Z006', STATUS = 'ACTIF', RM_ID = '001-0', FL_ID = '00', EMAIL = NULL WHERE EM_ID = 'AFM';
UPDATE AFM.EM SET BL_ID = '000006Z006', RM_ID = '001-0', FL_ID = '00' WHERE EM_ID = 'AI';

UPDATE AFM.FINANAL_ANALYSES SET ANALYSIS_TITLE_FR = NULL WHERE ANALYSIS_TITLE = 'Capital and Capital Projects';
UPDATE AFM.FINANAL_ANALYSES SET ANALYSIS_TITLE_FR = NULL WHERE ANALYSIS_TITLE = 'Energy and Utilities';
UPDATE AFM.FINANAL_ANALYSES SET ANALYSIS_TITLE_FR = NULL WHERE ANALYSIS_TITLE = 'Operating Expenses';
UPDATE AFM.FINANAL_ANALYSES SET ANALYSIS_TITLE_FR = NULL WHERE ANALYSIS_TITLE = 'Unified Capital and Expense Analysis';

UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_additional_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_assets';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_cash';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_financing';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_fixed_costs';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_income';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_operations';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_ownership';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_total_cost_occup';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_total_cost_own';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_utilities';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_variable_costs';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'anlys_roll_workpoint_cost';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_appreciation';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL, BOX_SUBTITLE_FR = NULL WHERE BOX_ID = 'box_asset_depreciation';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_buildings_and_land';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_buildings_and_prop_added';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_capital_eq';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_capital_eq_added';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_capital_internal_cost';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_capital_projects';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_capital_projects_added';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_custodial';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_depreciation';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_income';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_indirect_services';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_land_parcels';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_maintenance';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_mortgage_interest';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_mortgage_principal_payments';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_other';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_outlays_capital_proj_expense';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_outstanding_debt_on_assets';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_outstanding_ls_commitments';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_property_tax';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_rent_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_security';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_structures-m';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'box_utilities';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'column_asset_net_worth';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'column_expense_rollup';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'column_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_add_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_analysis';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_bal_sheet_liability';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_cash';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_endbalance_last_fyr';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_expenses_operating';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_income';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_offbalance_liability';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = NULL, BOX_TITLE_FR = NULL WHERE BOX_ID = 'fin_rollup_projections_this_fyr';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = NULL WHERE BOX_ID = 'perc_rollup_networth-percentage';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = NULL WHERE BOX_ID = 'perc_rollup_tco-percentage';

UPDATE AFM.GEO_REGION SET GEO_REGION_NAME = 'EMEA' WHERE GEO_REGION_ID = 'EMEA';

UPDATE AFM.GPSTD SET AREA = 0, TOT_COUNT = 0 WHERE GP_STD = 'DEPT-AREA';

UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 2 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 2 AND PRIORITY = 2;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 3 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 3 AND PRIORITY = 2;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 3 AND PRIORITY = 3;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - FURNITURE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - FURNITURE' AND ORDERING_SEQ = 1 AND PRIORITY = 2;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET SUPERVISOR = 'AFM', PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - GROUP MOVE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET SUPERVISOR = 'AFM', PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - HOTELING' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET SUPERVISOR = 'AFM', PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - INDIVIDUAL MOVE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 0;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 2;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 3;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 4;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 5;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 25;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 50;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 75;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_FR = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE' AND ORDERING_SEQ = 1 AND PRIORITY = 99;

UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'ACCEPTANCE_STEP' AND MESSAGE_ID = 'ESCALATION_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'ACCEPTANCE_STEP' AND MESSAGE_ID = 'ESCALATION_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'ARCHIVEREQUEST_WFR' AND MESSAGE_ID = 'ARCHIVE_REQUEST_DESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_SUBJECT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ESCALATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ESCALATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_NOTIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_NOTIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_COND';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_NONE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_ROLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_ROLE_ALL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_ROLE_ALL_COND';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_ROLE_COND';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SLA_INFORMATION_WFR' AND MESSAGE_ID = 'STEP_TEXT_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR' AND MESSAGE_ID = 'COMMENTS';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR' AND MESSAGE_ID = 'NOTIFY_ASSIGNEE_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR' AND MESSAGE_ID = 'NOTIFY_ASSIGNEE_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR_SUBSTITUTE' AND MESSAGE_ID = 'NOTIFY_ASSIGNEE_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR_SUBSTITUTE' AND MESSAGE_ID = 'NOTIFY_ASSIGNEE_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'ARCHIVE_WFR' AND MESSAGE_ID = 'ARCHIVE_DESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'WORK REQUEST ISSUED_I_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'WORK REQUEST ISSUED_I_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_MGR_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_MGR_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_AA_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_AA_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_AA_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_AA_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_I_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_I_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_I_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_I_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SCH_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_SCH_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SCH_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_SCH_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_AA_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_AA_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_AA_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_AA_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_I_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_I_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_I_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_I_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_SCH_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_SCH_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_SCH_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_SCH_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_MGR_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_MGR_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TEXT';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TITLE';
UPDATE AFM.MESSAGES SET TRANSFER_STATUS = 'MISSING', MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_NOTIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_NOTIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_RETURN_STEP' AND MESSAGE_ID = 'NOTIFY_CRAFTSPERSON_RETURN_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_RETURN_STEP' AND MESSAGE_ID = 'NOTIFY_CRAFTSPERSON_RETURN_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'APPROVE_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'APPROVE_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'APPROVE_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'APPROVE_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'AUTOAPPROVE_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'AUTOAPPROVE_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'AUTOAPPROVE_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'AUTOAPPROVE_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'CLOSE_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'CLOSE_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'CLOSE_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'CLOSE_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_GROUP_REQUEST_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_GROUP_REQUEST_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_SINGLE_REQUEST_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ISSUE_SINGLE_REQUEST_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REJECT_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REJECT_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REJECT_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REJECT_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_GROUP_INFORM_CONTACT_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_GROUP_INFORM_CONTACT_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_SINGLE_INFORM_CONTACT_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_SINGLE_INFORM_CONTACT_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_REQUEST_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_REQUEST_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_REQUEST_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_REQUEST_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'WITHDRAW_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'WITHDRAW_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'WITHDRAW_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'WITHDRAW_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'LS_ALERT_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'LS_ALERT_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'OP_ALERT_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'OP_ALERT_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_MISSED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_MISSED_REQ';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_OVERDUE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_REMINDER';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD BLDG SCHEDULE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD PROJECT PLAN';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD VENDOR EVAL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-FIELD DOCS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-FINAL RPT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-MASTER KEYS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-NTFY DEPT MGRS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_SUBJECT' AND MESSAGE_ID = 'MOLD-NTFY DEPT MGRS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_INCIDENT_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_INCIDENT_TEXT_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_INCIDENT_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_INCIDENT_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_INCIDENT_TEXT_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_INCIDENT_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_INCIDENT_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_INCIDENT_TEXT_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_INCIDENT_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_MEDMONITORING_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_MEDMONITORING_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_PPE_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_PPE_TEXT_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_PPE_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_TRAINING_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_TRAINING_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_MEDMONITORING_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_MEDMONITORING_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_PPE_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_PPE_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_TRAINING_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_TRAINING_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_MEDMONITORING_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_MEDMONITORING_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_PPE_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_PPE_TEXT_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_PPE_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_TRAINING_TEXT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_TRAINING_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_CAUTION_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_EPA_ID_NO';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_EPA_WASTE_NO';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_GENERATOR_ADDRESS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_GENERATOR_CITY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_GENERATOR_NAME';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_GENERATOR_STATE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_GENERATOR_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_GENERATOR_ZIP';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_MANIFEST_NO';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_SHIPPING_NAME_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_START_DATE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_TITLE_1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_TITLE_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_TITLE_3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'MUNICIPAL_TITLE_1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_ADDRESS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_CAUTION_TITLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_CONTENTS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_LOCATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_SHIPPER';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_SHIPPING_NAME';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_SHIPPING_NO';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_TITLE_1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_TITLE_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_FORGOTTEN_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_FORGOTTEN_PASSWORD_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_NEW_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_NEW_PASSWORD_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_REQUEST_NEW_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_REQUEST_NEW_PASSWORD_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_TEMPORARY_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_TEMPORARY_PASSWORD_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'RESOURCEUNAVAILABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'SUBTRACTTIMESERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'TIMEPERIODINPAST';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'TIMEPERIODNOTAVAILABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'APPROVERESERVATION_WFR' AND MESSAGE_ID = 'APPROVERESERVATIONERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CALENDAR_SERVICE' AND MESSAGE_ID = 'CANCEL_NOTIFY_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CALENDAR_SERVICE' AND MESSAGE_ID = 'CANCEL_NOTIFY_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CANCELRESERVATION_WFR' AND MESSAGE_ID = 'CANCELRESERVATIONERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CHECKROOMANDRESOURCESAPPROVAL_WFR' AND MESSAGE_ID = 'CHECKROOMANDRESOURCESAPPROVALERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CHECKROOMANDRESOURCESAPPROVAL_WFR' AND MESSAGE_ID = 'CHECKROOMANDRESOURCESAPPROVALREJECTMESSAGE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CLOSERESERVATIONS_WFR' AND MESSAGE_ID = 'ARCHIVERESERVATIONSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CONFCALL_WFR' AND MESSAGE_ID = 'CONFERENCE_CALL_LOCATIONS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CONFCALL_WFR' AND MESSAGE_ID = 'CONFERENCE_CALL_LOCATIONS_SEPARATOR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CONFIRM_ROOM_RESERVATION_VIEW' AND MESSAGE_ID = 'FILLMANDATORYFIELDSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTCLEANUPDESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTRESERVATIONATTENDEES';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTRESERVATIONCOMMENTSDESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTRESERVATIONQUANTITY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTSETUPDESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'DEFINE_CRITERIA_SELECT_ROOM_VIEW' AND MESSAGE_ID = 'SELECTROOMANDTIMEERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'DEFINE_CRITERIA_SELECT_ROOM_VIEW' AND MESSAGE_ID = 'TIMESELECTEDNOTAVAILABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'DETECTRESOURCECONFLICTS_WFR' AND MESSAGE_ID = 'DETECTRESOURCECONFLICTSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'DETECTROOMCONFLICTS_WFR' AND MESSAGE_ID = 'DETECTROOMCONFLICTSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETARRANGEMENTFIXEDRESOURCES_WFR' AND MESSAGE_ID = 'FIXEDRESOURCESNOTFOUND';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETNUMBERPENDINGRESERVATIONS_WFR' AND MESSAGE_ID = 'GETNUMBERPENDINGRESERVATIONSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETNUMBERPENDINGRESOURCERESERVATIONS_WFR' AND MESSAGE_ID = 'GETNUMBERPENDINGRESOURCERESERVATIONSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETRESERVATIONINFO_WFR' AND MESSAGE_ID = 'RESERVATIONNOTFOUND';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETRESERVATIONTYPE_WFR' AND MESSAGE_ID = 'GETRESERVATIONTYPEERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_CANCEL_FAILED_NOTIFY_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_CANCEL_FAILED_NOTIFY_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_CANCEL_SUCCESS_NOTIFY_BODY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_CANCEL_SUCCESS_NOTIFY_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_BODY1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_BODY2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_BODY3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LOADTIMELINE_WFR' AND MESSAGE_ID = 'INVALIDPARAMETERERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LOADTIMELINE_WFR' AND MESSAGE_ID = 'LOADTIMELINEERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LOADTIMELINE_WFR' AND MESSAGE_ID = 'LOADTIMELINENOROOMSFOUND';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVERERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART10';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART11';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART12';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART13';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART14';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART15';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART16';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART17';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART4';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART5';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART6';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART7';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART8';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART9';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_SUBJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBYERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART10';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART11';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART11_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART12';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART13';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART2_CANCEL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART2_REJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART4';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART5';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART6';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART7';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART8';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_BODY_PART9';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_SUBJECT_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_SUBJECT_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_SUBJECT_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBY_SUBJECT_PART4';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFORERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART10';

UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART11';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART11_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART12';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART13';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART2_CANCEL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART2_REJECT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART4';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART5';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART6';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART7';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART8';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_BODY_PART9';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_SUBJECT_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_SUBJECT_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_SUBJECT_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFOR_SUBJECT_PART4';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CANCEL_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CANCEL_FAILED_RETRY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFERENCE_CALL_MEETING_LOCATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CANCELLING_1_RESERVATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CANCELLING_CONFERENCE_RESERVATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CANCELLING_C_RESERVATIONS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CANCELLING_RESERVATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CHANGE_RECURRING_CONFERENCE_CALL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CHANGE_RECURRING_RESERVATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_RECURRENCE_CONFLICTS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_RECURRENCE_CONFLICTS_CONFCALL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_REMOVING_EXCEPTIONS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFLICTS_DESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFLICTS_DESCRIPTION_CONFCALL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFLICT_LOCATION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFLICT_LOCATION_CONFERENCE_CALL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'COPYING_RESERVATION_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_CONFCALL_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_CONFCALL_RESERVATIONS_CREATED_CONFLICTS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_CONFCALL_RESERVATIONS_CREATED_ONE_CONFLICT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_RESERVATIONS_CREATED_CONFLICTS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_RESERVATIONS_CREATED_ONE_CONFLICT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED_CONFERENCE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED_RETRY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED_RETRY_CONFERENCE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CONFCALL_RESERVATION_CREATED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CONFLICT_DESCRIPTION';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CONFLICT_DESCRIPTION_CONFCALL';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_RESERVATION_CREATED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'RESCHEDULING_CONFLICT';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'RESERVED_ROOM_UNAVAILABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'REVIEW_MODIFIED_OCCURRENCES';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ROOM_CONFLICTS_SEPARATOR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SAVING_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SERVER_UNREACHABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED_CONFERENCE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED_RETRY';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED_RETRY_CONFERENCE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SHOW_RESERVATION_PANE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SHOW_RESERVATION_PANE_OFFLINE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'UPDATE_CONFCALL_ONE_CANCEL_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'UPDATE_CONFCALL_SEVERAL_CANCELS_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'VERIFYING_RECURRENCE_FAILED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_ATTENDEES_REQUIRED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_CANNOT_REMOVE_EXCEPTIONS';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_CAPACITY_REQUIRED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_NO_END_DATE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_OUT_OF_SYNC';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_ROOM_UNAVAILABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ZERO_CONFCALL_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ZERO_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'READAVAILABLERESOURCES_WFR' AND MESSAGE_ID = 'READAVAILABLERESOURCESERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'READREQUESTORPROPERTIES_WFR' AND MESSAGE_ID = 'READREQUESTORERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'REJECTRESERVATION_WFR' AND MESSAGE_ID = 'REJECTRESERVATIONERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVERESOURCERESERVATIONS_WFR' AND MESSAGE_ID = 'CANCELRESOURCEERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVERESOURCERESERVATIONS_WFR' AND MESSAGE_ID = 'CANCELRESOURCEINPAST';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVERESOURCERESERVATIONS_WFR' AND MESSAGE_ID = 'RESOURCEUNAVAILABLE';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVERESOURCERESERVATIONS_WFR' AND MESSAGE_ID = 'SAVERESOURCEERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVERESOURCERESERVATIONS_WFR' AND MESSAGE_ID = 'TIMEPERIODINPAST';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVEROOMOVERRIDE_WFR' AND MESSAGE_ID = 'SAVEROOMOVERRIDEERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SEARCHRESERVATIONSADDITIONALINFO_WFR' AND MESSAGE_ID = 'RESERVATIONADDITIONALINFOERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONSERROR';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART1_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART1_3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART2_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART2_3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART4';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART5';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART6';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART6_2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART7';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_BODY_PART8';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_SUBJECT_PART1';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_SUBJECT_PART2';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_SUBJECT_PART3';
UPDATE AFM.MESSAGES SET MESSAGE_TEXT_FR = NULL WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONS_SUBJECT_PART4';

UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'ASSESSMENT';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'ASSESSMENT - ENVIRONMENTAL';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'ASSESSMENT - HAZMAT';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'COMMISSIONING';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'Move';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'N/A';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'New Construction';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'Renovation';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = NULL WHERE PROJECT_TYPE = 'Scenario';

UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Data';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Asset';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Employee';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Leaving';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - New Hire';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Room';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - New Construction';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - Renovation';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - COPY SERVICE';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - DEPARTMENT SPACE';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - FURNITURE';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - GROUP MOVE';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - INDIVIDUAL MOVE';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - NEW';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - TEST1';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - TEST2';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - TESTQ';

UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL' AND QUEST_NAME = 'CPU_MODEL';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL' AND QUEST_NAME = 'CPU_POWER';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL' AND QUEST_NAME = 'MONITOR_MODEL';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL' AND QUEST_NAME = 'MONITOR_POWER';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL' AND QUEST_NAME = 'UP_TIME';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'coils';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'controls';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'drain-line';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'fans';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'humidifier';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'smoke-detectors';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU' AND QUEST_NAME = 'visible-damage';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler' AND QUEST_NAME = 'air-and-flue-gas-systems';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler' AND QUEST_NAME = 'boiler-setting';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler' AND QUEST_NAME = 'fuel-systems';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler' AND QUEST_NAME = 'piping-support-systems';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler' AND QUEST_NAME = 'pressure-parts';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'efficiency';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'horsepower';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'net-positive-suction-head';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'problem';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'pump-application';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'pump-capacity';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'pump-type';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'total-head-produced';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'docking-station';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'operating-system';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'setup-computer';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'test-network-connection';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice' AND QUEST_NAME = 'call-waiting';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice' AND QUEST_NAME = 'fax-modem';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice' AND QUEST_NAME = 'phone-type';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice' AND QUEST_NAME = 'transfer-mail-box';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Asset' AND QUEST_NAME = 'require-assembly';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Asset' AND QUEST_NAME = 'special-requirements';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Employee' AND QUEST_NAME = 'ergonomic-chair';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL, IS_ACTIVE = 0 WHERE QUESTIONNAIRE_ID = 'Move Order - Employee' AND QUEST_NAME = 'name-plate';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Employee' AND QUEST_NAME = 'office-key';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Employee' AND QUEST_NAME = 'phone-to-be-moved';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'data-connection';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'phone-connection';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'power-requirements';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'require-setup';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Leaving' AND QUEST_NAME = 'disconnect-network';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Leaving' AND QUEST_NAME = 'disconnect-phone';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Leaving' AND QUEST_NAME = 'reset-computer';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - New Hire' AND QUEST_NAME = 'ergonomic-chair';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - New Hire' AND QUEST_NAME = 'name-plate';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - New Hire' AND QUEST_NAME = 'office-key';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Room' AND QUEST_NAME = 'data-jacks';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Room' AND QUEST_NAME = 'name-plate';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Room' AND QUEST_NAME = 'phone-to-be-moved';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Room' AND QUEST_NAME = 'special-requirements';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Move Order - Room' AND QUEST_NAME = 'voice-jacks';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - New Construction' AND QUEST_NAME = 'Parking';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - New Construction' AND QUEST_NAME = 'Plans';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - New Construction' AND QUEST_NAME = 'TriggeringCircumstances';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - Renovation' AND QUEST_NAME = 'Alternatives';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - Renovation' AND QUEST_NAME = 'OtherDepartments';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'Project - Renovation' AND QUEST_NAME = 'TriggeringCircumstances';
UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = NULL, QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - COPY SERVICE' AND QUEST_NAME = 'color';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - COPY SERVICE' AND QUEST_NAME = 'quantity';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - DEPARTMENT SPACE' AND QUEST_NAME = 'date_start';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - FURNITURE' AND QUEST_NAME = 'category';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - GROUP MOVE' AND QUEST_NAME = 'bl_id';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - GROUP MOVE' AND QUEST_NAME = 'date_end';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - GROUP MOVE' AND QUEST_NAME = 'date_start';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - GROUP MOVE' AND QUEST_NAME = 'dp_contact';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - GROUP MOVE' AND QUEST_NAME = 'project_name';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - INDIVIDUAL MOVE' AND QUEST_NAME = 'date_start';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - INDIVIDUAL MOVE' AND QUEST_NAME = 'dp_contact';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = NULL WHERE QUESTIONNAIRE_ID = 'SERVICE DESK - TESTQ' AND QUEST_NAME = '123';

UPDATE AFM.STATE SET CTRY_ID = 'FRANCE', NAME = 'CALVADOS', REGN_ID = '28' WHERE STATE_ID = '14';
UPDATE AFM.STATE SET CTRY_ID = 'FRANCE', NAME = 'HERAULT', REGN_ID = '76' WHERE STATE_ID = '34';
UPDATE AFM.STATE SET CTRY_ID = 'FRANCE', NAME = 'YVELINES', REGN_ID = '11' WHERE STATE_ID = '78';

UPDATE AFM.WORK_ROLES SET TITLE_FR = NULL WHERE WORK_ROLE_NAME = 'SAFETY OFFICER';

UPDATE AFM_SECURE.AFM_USERS SET DATE_PWD_CHANGED = '05/12/2017 00:00:00', USER_PWD = 'sncf2017', LOCALE = 'fr_FR' WHERE USER_NAME = 'AFM';
UPDATE AFM_SECURE.AFM_USERS SET LOCALE = 'en_Us' WHERE USER_NAME = 'AI';
UPDATE AFM_SECURE.AFM_USERS SET HOME_PAGE = 'ab-dashboard.axvw' WHERE USER_NAME = 'AIDEMO3';
UPDATE AFM_SECURE.AFM_USERS SET CLR_SCHEME = 'SLATE', HOME_PAGE = 'ab-dashboard.axvw' WHERE USER_NAME = 'AIDEMO4';
UPDATE AFM_SECURE.AFM_USERS SET LOCALE = 'en_US' WHERE USER_NAME = 'AIDEMO5';
UPDATE AFM_SECURE.AFM_USERS SET LOCALE = 'en_Us', NUM_RETRIES = 5, ROLE_NAME = 'ACTIVITY LICENSEE' WHERE USER_NAME = 'SYSTEM';

COMMIT;
SPOOL OFF;
quit;