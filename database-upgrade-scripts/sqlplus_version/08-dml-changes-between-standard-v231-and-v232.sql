SPOOL 08-dml-changes-between-standard-v231-and-v232.txt
SET TERMOUT OFF;
SET SQLBLANKLINES ON;
SET DEFINE OFF;
ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/SYYYY HH24:MI:SS';
ALTER SESSION SET NLS_TIMESTAMP_TZ_FORMAT = 'MM/DD/SYYYY HH24:MI:SS.FF TZH:TZM';
ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'MM/DD/SYYYY HH24:MI:SS.FF';
ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,';
ALTER SESSION SET NLS_NCHAR_CONV_EXCP = FALSE;
ALTER SESSION SET TIME_ZONE = '+05:30';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'OdL'||CHR(13)||CHR(10)
||'autorizzato da', ML_HEADING_FR = 'Bon de travaux'||CHR(13)||CHR(10)
||'autoris� par' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'name_authorized';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'OdL'||CHR(13)||CHR(10)
||'programmato da', ML_HEADING_FR = 'Bon de travaux'||CHR(13)||CHR(10)
||'planifi� par' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'name_of_planner';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��1' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'option1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��2', ML_HEADING_ES = 'Opci�n'||CHR(13)||CHR(10)
||'2' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'option2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Priorit�'||CHR(13)||CHR(10)
||'ordine di lavoro', ML_HEADING_FR = 'Priorit�'||CHR(13)||CHR(10)
||'du bon de travaux' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��������', ML_HEADING_ES = 'N�mero de'||CHR(13)||CHR(10)
||'solicitudes abiertas', ML_HEADING_IT = 'Numero di'||CHR(13)||CHR(10)
||'richieste aperte', ML_HEADING_NL = 'Aantal lopende'||CHR(13)||CHR(10)
||'aanvragen', ML_HEADING_FR = 'Nombre de'||CHR(13)||CHR(10)
||'demandes ouvertes' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'qty_open_wr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora di chiusura'||CHR(13)||CHR(10)
||'OdL', ML_HEADING_NL = 'Tijdstip'||CHR(13)||CHR(10)
||'werkorder afgesloten', ML_HEADING_FR = 'Bon'||CHR(13)||CHR(10)
||'ferm� �' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'time_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de orden de'||CHR(13)||CHR(10)
||'trabajo terminada', ML_HEADING_IT = 'Ora completamento'||CHR(13)||CHR(10)
||'OdL', ML_HEADING_NL = 'Tijdstip'||CHR(13)||CHR(10)
||'werkorder voltooid', ML_HEADING_FR = 'Bon'||CHR(13)||CHR(10)
||'termin� �', ML_HEADING_DE = 'Auftrag '||CHR(13)||CHR(10)
||'erledigt um' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'time_completed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_IT = 'Ora creazione'||CHR(13)||CHR(10)
||'OdL', ML_HEADING_NL = 'Tijdstip'||CHR(13)||CHR(10)
||'werkorder gemaakt', ML_HEADING_FR = 'Bon'||CHR(13)||CHR(10)
||'cr�� �', ML_HEADING_DE = 'Auftrag '||CHR(13)||CHR(10)
||'erstellt um' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'time_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora emissione'||CHR(13)||CHR(10)
||'OdL', ML_HEADING_NL = 'Tijdstip'||CHR(13)||CHR(10)
||'werkorder uitgegeven', ML_HEADING_DE = 'Auftrag'||CHR(13)||CHR(10)
||'ausgestellt um' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'time_issued';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Settore primario'||CHR(13)||CHR(10)
||'richiesto', ML_HEADING_NL = 'Primaire vakdiscipline'||CHR(13)||CHR(10)
||'vereist', ML_HEADING_FR = 'Corps de m�tier'||CHR(13)||CHR(10)
||'principal requis' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'tr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�', ML_HEADING_NL = 'Werkorder-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de bon'||CHR(13)||CHR(10)
||'de travaux', ML_HEADING_DE = 'Arbeitsauftrags-Nr.' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'wo_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�'||CHR(13)||CHR(10)
||'����', ML_HEADING_IT = 'Tipo lavoro'||CHR(13)||CHR(10)
||'primario', ML_HEADING_NL = 'Primair'||CHR(13)||CHR(10)
||'werktype', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'travaux principaux' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'wo_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de equipo de trabajo', ML_HEADING_IT = 'Codice team di lavoro', ML_HEADING_NL = 'Werkteam code', ML_HEADING_FR = 'Code de l��quipe d�intervention', ML_HEADING_DE = 'Kurzzeichen Arbeitsteam' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'work_categories_em' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Offre' WHERE TABLE_NAME = 'work_pkg_bids' AND FIELD_NAME = 'bid_type';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Created;Cr��;Withdrawn;Retir�;Submitted;Envoy�;Submitted-InReview;Envoy�-En cours d�examen;Rejected;Rejet�;Approved;Approuv�;Contract Signed;Contrat sign�;In Process;En cours;In Process-On Hold;En cours-En attente;Completed;Termin�;Completed and Verified;Termin� et V�rifi�;Paid in Full;Pay� enti�rement' WHERE TABLE_NAME = 'work_pkg_bids' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 1' WHERE TABLE_NAME = 'work_pkgs' AND FIELD_NAME = 'apprv_mgr1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Approbation '||CHR(13)||CHR(10)
||'du Responsable 1' WHERE TABLE_NAME = 'work_pkgs' AND FIELD_NAME = 'apprv_mgr1_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation '||CHR(13)||CHR(10)
||'Responsable 1' WHERE TABLE_NAME = 'work_pkgs' AND FIELD_NAME = 'date_app_mgr1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation des'||CHR(13)||CHR(10)
||'documents Cx' WHERE TABLE_NAME = 'work_pkgs' AND FIELD_NAME = 'date_cx_docs_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Document - '||CHR(13)||CHR(10)
||'Transfert d�actions' WHERE TABLE_NAME = 'work_pkgs' AND FIELD_NAME = 'doc_acts_xfer';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'work_roles_location' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�le de travail'||CHR(13)||CHR(10)
||'par code d�emplacement' WHERE TABLE_NAME = 'work_roles_location' AND FIELD_NAME = 'work_role_location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Les techniciens'||CHR(13)||CHR(10)
||'s�affectent eux-m�mes�?' WHERE TABLE_NAME = 'work_team' AND FIELD_NAME = 'cf_assign';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'work_team' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'workflow_substitutes' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�employ� rempla�ant' WHERE TABLE_NAME = 'workflow_substitutes' AND FIELD_NAME = 'substitute_em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures R�elles'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'act_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t Estim� en'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'cost_est_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demande d�intervention'||CHR(13)||CHR(10)
||'ferm�e le' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique de date d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'date_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures Estim�es'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'est_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Envoi'||CHR(13)||CHR(10)
||'du Message' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Demande'||CHR(13)||CHR(10)
||'d�intervention parent' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'parent_wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demand�;Rev;Examin�/En attente;Rej;Rejet�;A;Approuv�;AA;Affect� � Bon Trvx;I;Emis/En cours;HP;En attente/Pi�ces d�t.;HA;En attente/Acc�s;HL;En attente/Md�O;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_FR = 'Statut de l��tape'||CHR(13)||CHR(10)
||'de la Demande d�Intervention' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||'pour ex�cution �' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'time_esc_comp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'time_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'wr' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�Autre Ressource' WHERE TABLE_NAME = 'wr_other' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de l�Autre'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'wr_other' AND FIELD_NAME = 'other_rs_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�Autre Ressource' WHERE TABLE_NAME = 'wr_other_sync' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par'||CHR(13)||CHR(10)
||'l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'wr_other_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de l�Autre'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'wr_other_sync' AND FIELD_NAME = 'other_rs_type';
UPDATE AFM.AFM_FLDS_LANG SET SL_HEADING_FR = 'Code Demande d�Intervention' WHERE TABLE_NAME = 'wr_other_sync' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'���', ML_HEADING_ES = 'Respuesta por'||CHR(13)||CHR(10)
||'c�digo de operario', ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Code van vakman', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'code de technicien' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'cf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha'||CHR(13)||CHR(10)
||'de creaci�n', ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'di creazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'Erstellt' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'respuesta', ML_HEADING_DE = 'Geantwortet'||CHR(13)||CHR(10)
||'am' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'date_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Naam van medewerker', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�employ�' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Nombre'||CHR(13)||CHR(10)
||'de funci�n', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'r�le' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'role_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_DE = 'Anfrage -'||CHR(13)||CHR(10)
||'Status' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Paso de flujo'||CHR(13)||CHR(10)
||'de trabajo', ML_HEADING_NL = 'Werkstroom'||CHR(13)||CHR(10)
||'Stap', ML_HEADING_FR = 'Etape du'||CHR(13)||CHR(10)
||'workflow', ML_HEADING_DE = 'Workflow-'||CHR(13)||CHR(10)
||'Schritt' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'step';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo'||CHR(13)||CHR(10)
||'de paso', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'step_code';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�� Id', ML_HEADING_ES = 'ID de registro'||CHR(13)||CHR(10)
||'de paso', ML_HEADING_IT = 'ID registro'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_NL = 'Stap'||CHR(13)||CHR(10)
||'logboek-ID', ML_HEADING_FR = 'ID de registre'||CHR(13)||CHR(10)
||'d��tapes' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'step_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'de paso', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'procedura', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape', ML_HEADING_DE = 'Schritt-'||CHR(13)||CHR(10)
||'typ' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de'||CHR(13)||CHR(10)
||'creaci�n', ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'Gecre�erd', ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Erstellungs-'||CHR(13)||CHR(10)
||'zeitpunkt' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'time_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de'||CHR(13)||CHR(10)
||'respuesta', ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'waarop gereageerd werd', ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'de r�ponse �' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'time_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Gebruikersnaam', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�utilisateur' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Code van leverancier', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'code de fournisseur' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'nummer' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la main-d�oeuvre'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'cost_est_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'main-d�oeuvre' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demande d�intervention'||CHR(13)||CHR(10)
||'ferm�e le' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc1_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc2_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc3_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc4_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Contient une �tape'||CHR(13)||CHR(10)
||'d�estimation termin�e�?' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'estimation_comp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Transf�rer le code de'||CHR(13)||CHR(10)
||'l��quipe d�intervention' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'fwd_work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Equipe d�intervention'||CHR(13)||CHR(10)
||'affect�e automatiquement�?' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'is_wt_self_assign';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demander;HP;Mettre en attente de pi�ces d�tach�es;HA;Mettre en attente d�acc�s;HL;Mettre en attente de main d�oeuvre;S;Arr�ter;Can;Annuler;Com;Terminer', ML_HEADING_FR = 'En attente '||CHR(13)||CHR(10)
||'d�une action '||CHR(13)||CHR(10)
||'mobile' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'mob_pending_action';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modification du statut'||CHR(13)||CHR(10)
||'mobile ou '||CHR(13)||CHR(10)
||'de l��tape' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'mob_stat_step_chg';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Action de'||CHR(13)||CHR(10)
||'l��tape mobile' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'mob_step_action';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires de'||CHR(13)||CHR(10)
||'l�action mobile', SL_HEADING_FR = 'Commentaires de'||CHR(13)||CHR(10)
||'l�action mobile' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'mob_step_comments';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Demande'||CHR(13)||CHR(10)
||'d�intervention parent' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'parent_wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demand�;Rev;Examin�/En attente;Rej;Rejet�;A;Approuv�;AA;Affect� � Bon Trvx;I;Emis/En cours;HP;En attente/Pi�ces d�t.;HA;En attente/Acc�s;HL;En attente/Md�O;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�', SL_HEADING_FR = 'Statut de la Demande d�Intervention' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�employ�' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'step_em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID de registre'||CHR(13)||CHR(10)
||'d��tapes' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'step_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'r�le d��tape' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'step_role_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_FR = 'Statut de l��tape'||CHR(13)||CHR(10)
||'de demande d�intervention' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�utilisateur' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'step_user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe d�intervention' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET SL_HEADING_FR = 'Code Demande d�Intervention' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t Total'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'wrcf' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Envoi'||CHR(13)||CHR(10)
||'du Message' WHERE TABLE_NAME = 'wrcf' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = '�tat de'||CHR(13)||CHR(10)
||'l�affectation' WHERE TABLE_NAME = 'wrcf' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;s/o;R;Demand�;Rev;Examin� mais en attente;Rej;Rejet�;A;Approuv�;AA;Affect� � un Bon;I;Emis et En cours;HP;En attente de pi�ces;HA;En attente d�acc�s;HL;En attente de main d�oeuvre;S;Stopp�;Can;Annul�;Com;Termin�' WHERE TABLE_NAME = 'wrcf' AND FIELD_NAME = 'status_from_remote_cf';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t Total'||CHR(13)||CHR(10)
||'Main d�Oeuvre', SL_HEADING_FR = 'Co�t Total Main d�Oeuvre' WHERE TABLE_NAME = 'wrcf_sync' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'wrcf_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET SL_HEADING_FR = 'Code Demande d�Intervention' WHERE TABLE_NAME = 'wrcf_sync' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'cuenta', ML_HEADING_NL = 'Account-'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Poste'||CHR(13)||CHR(10)
||'Comptable' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_ES = 'Horas reales de'||CHR(13)||CHR(10)
||'recursos humanos', ML_HEADING_FR = 'Heures r�elles'||CHR(13)||CHR(10)
||'main d�oeuvre', ML_HEADING_DE = 'Tats�chliche'||CHR(13)||CHR(10)
||'Arbeitsstunden' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'act_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�� ID', ML_HEADING_ES = 'Identificador'||CHR(13)||CHR(10)
||'de actividad', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'azione', ML_HEADING_NL = 'Actiepunt-ID', ML_HEADING_FR = 'Identifiant d��l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'acci�n', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action', ML_HEADING_DE = 'Ma�nahmentyp' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�������?', ML_HEADING_NL = 'Werk op'||CHR(13)||CHR(10)
||'feestdagen toestaan?', ML_HEADING_FR = 'Autoriser les travaux'||CHR(13)||CHR(10)
||'pendant les jours f�ri�s�?' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'allow_work_on_holidays';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'de cause', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ursache' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cause_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Note'||CHR(13)||CHR(10)
||'addetti alla manutenzione', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'vakmensen', ML_HEADING_FR = 'Notes des'||CHR(13)||CHR(10)
||'techniciens', ML_HEADING_DE = 'Anmerkungen zu'||CHR(13)||CHR(10)
||'Handwerkern' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cf_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Completado'||CHR(13)||CHR(10)
||'por', ML_HEADING_NL = 'Voltooid'||CHR(13)||CHR(10)
||'per' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'completed_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Coste de recursos'||CHR(13)||CHR(10)
||'humanos estimado', ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'manodopera stimato', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten van arbeid', ML_HEADING_FR = 'Co�t de la main-d�oeuvre'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_est_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Overige kosten', ML_HEADING_FR = 'Autres co�ts'||CHR(13)||CHR(10)
||'estim�s', ML_HEADING_DE = 'Gesch�tzte'||CHR(13)||CHR(10)
||'Sonstige Kosten' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_est_other';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'stimato parti', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten van onderdelen', ML_HEADING_FR = 'Co�t des pi�ces d�tach�es'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_est_parts';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'stimato attrezzi', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten van gereedschappen', ML_HEADING_FR = 'Co�t des outils'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_est_tools';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Costo totale'||CHR(13)||CHR(10)
||'stimato', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Totale  kosten', ML_HEADING_FR = 'Co�t total'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_est_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'recursos humanos', ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'main-d�oeuvre', ML_HEADING_DE = 'Arbeits-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Overig'||CHR(13)||CHR(10)
||'kosten', ML_HEADING_FR = 'Autres'||CHR(13)||CHR(10)
||'co�ts' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_other';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'piezas', ML_HEADING_IT = 'Costo delle'||CHR(13)||CHR(10)
||'parti', ML_HEADING_FR = 'Co�t des'||CHR(13)||CHR(10)
||'pi�ces d�tach�es', ML_HEADING_DE = 'Teile-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_parts';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'herramientas', ML_HEADING_IT = 'Costo degli'||CHR(13)||CHR(10)
||'attrezzi', ML_HEADING_NL = 'Kosten van'||CHR(13)||CHR(10)
||'gereedschap', ML_HEADING_FR = 'Co�t des'||CHR(13)||CHR(10)
||'outils', ML_HEADING_DE = 'Werkzeug-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_tools';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'totale', ML_HEADING_NL = 'uitgaven'||CHR(13)||CHR(10)
||'Kosten', ML_HEADING_FR = 'Co�t'||CHR(13)||CHR(10)
||'total', ML_HEADING_DE = 'Gesamt-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Lettura'||CHR(13)||CHR(10)
||'attuale [mt]', ML_HEADING_FR = 'Lecture courante'||CHR(13)||CHR(10)
||'compteur �quipt' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'curr_meter_val';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'esecuzione', ML_HEADING_NL = 'Geplande'||CHR(13)||CHR(10)
||'uitvoer.datum', ML_HEADING_FR = 'A ex�cuter'||CHR(13)||CHR(10)
||'le' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Fecha de cierre de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_FR = 'Demande d�intervention'||CHR(13)||CHR(10)
||'ferm�e le', ML_HEADING_DE = 'Datum - Arbeits-'||CHR(13)||CHR(10)
||'anforderung abgeschlossen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data completamento'||CHR(13)||CHR(10)
||'lavoro', ML_HEADING_FR = 'Travaux'||CHR(13)||CHR(10)
||'termin�s le' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_completed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding voor'||CHR(13)||CHR(10)
||'uitvoeringstijd optreedt', ML_HEADING_FR = 'Recours hi�rarchique pour'||CHR(13)||CHR(10)
||'ex�cution le' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding'||CHR(13)||CHR(10)
||'responstijd optreedt', ML_HEADING_FR = 'Remise � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse le', ML_HEADING_DE = 'Termineskalation f�r'||CHR(13)||CHR(10)
||'Reaktion tritt auf' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'terminaci�n estd.', ML_HEADING_FR = 'Fin estim�e'||CHR(13)||CHR(10)
||'pour le', ML_HEADING_DE = 'Datum der gesch�tzten'||CHR(13)||CHR(10)
||'Fertigstellung' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_est_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Fecha de trabajo'||CHR(13)||CHR(10)
||'solicitado', ML_HEADING_IT = 'Data richiesta'||CHR(13)||CHR(10)
||'di lavoro', ML_HEADING_FR = 'Travaux demand�s'||CHR(13)||CHR(10)
||'le' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de derni�re'||CHR(13)||CHR(10)
||'modification de statut' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'date_stat_chg';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Descripci�n de'||CHR(13)||CHR(10)
||'otros costes', ML_HEADING_IT = 'Descrizione altri'||CHR(13)||CHR(10)
||'costi', ML_HEADING_NL = 'Overige kosten'||CHR(13)||CHR(10)
||'gegevenspunt', ML_HEADING_FR = 'Description des'||CHR(13)||CHR(10)
||'autres co�ts', ML_HEADING_DE = 'Sonstige Kosten'||CHR(13)||CHR(10)
||'Beschreibung' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'desc_other_costs';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Werkbeschrijving', ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'des travaux' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||' (���)', ML_HEADING_ES = 'Inactividad'||CHR(13)||CHR(10)
||'de equipo (horas)', ML_HEADING_IT = 'Fermo'||CHR(13)||CHR(10)
||'apparecchiatura (ore)', ML_HEADING_NL = 'Uitvaltijd van'||CHR(13)||CHR(10)
||'uitrusting (uren)', ML_HEADING_FR = 'Equipement'||CHR(13)||CHR(10)
||'indisponible (h)', ML_HEADING_DE = 'Ausfallszeit des'||CHR(13)||CHR(10)
||'Ger�ts (Stunden)' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'down_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Ger�tekurzzeichen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'eq_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'afronding?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Erledigung?' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'escalated_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'antwoord?', ML_HEADING_FR = 'Remont� � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Reaktion?' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'escalated_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Horas estimadas'||CHR(13)||CHR(10)
||'de recursos humanos', ML_HEADING_FR = 'Heures estim�es de'||CHR(13)||CHR(10)
||'main-d�oeuvre' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'est_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Probleem-'||CHR(13)||CHR(10)
||'locatie', ML_HEADING_FR = 'Localisation du'||CHR(13)||CHR(10)
||'probl�me' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'location';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Gestor de centro'||CHR(13)||CHR(10)
||'de soporte', ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'du centre de services', ML_HEADING_DE = 'Servicedesk'||CHR(13)||CHR(10)
||'Manager' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'manager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '������', ML_HEADING_IT = 'Stato'||CHR(13)||CHR(10)
||'consegna messaggio', ML_HEADING_NL = 'Status'||CHR(13)||CHR(10)
||'berichtbezorging', ML_HEADING_FR = 'Statut de l�envoi'||CHR(13)||CHR(10)
||'du message', ML_HEADING_DE = 'Nachrichtempfang'||CHR(13)||CHR(10)
||'Status' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��1' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'option1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��2', ML_HEADING_ES = 'Opci�n'||CHR(13)||CHR(10)
||'2' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'option2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Demande'||CHR(13)||CHR(10)
||'d�intervention parent' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'parent_wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'PO-'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_DE = 'IH-'||CHR(13)||CHR(10)
||'Vorgang' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'pmp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM ���'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'programmazione MP', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'PO-schema', ML_HEADING_FR = 'Code de planning'||CHR(13)||CHR(10)
||'de MP', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'IH-Plan' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'pms_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Prioridad de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_IT = 'Priorit�'||CHR(13)||CHR(10)
||'richiesta di lavoro', ML_HEADING_NL = 'Prioriteit'||CHR(13)||CHR(10)
||'werkaanvraag', ML_HEADING_FR = 'Priorit� de la'||CHR(13)||CHR(10)
||'demande d�intervention', ML_HEADING_DE = 'Arbeitsanforderung - '||CHR(13)||CHR(10)
||'Priorit�t' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'problema', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di problema', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'probl�me' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'prob_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di riparazione', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'r�paration' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'repair_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de espacio', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'ruimtereservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de pi�ce', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Raumreservierung' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'rmres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de recurso', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'resource-reservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de ressource', ML_HEADING_DE = 'Ressourcenreservierungscode' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'rsres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Calificaci�n de'||CHR(13)||CHR(10)
||'la satisfacci�n', ML_HEADING_NL = 'Tevredenheids-'||CHR(13)||CHR(10)
||'score', ML_HEADING_FR = 'Note de'||CHR(13)||CHR(10)
||'satisfaction', ML_HEADING_DE = 'Zufriedenheitsbewertung' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'satisfaction';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Notas de'||CHR(13)||CHR(10)
||'satisfacci�n', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'bij waardering' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'satisfaction_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�����', ML_HEADING_ES = 'D�as de'||CHR(13)||CHR(10)
||'plazo de servicio', ML_HEADING_IT = 'Giorni periodo'||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Dagen'||CHR(13)||CHR(10)
||'servicetijd', ML_HEADING_FR = 'Jours de la'||CHR(13)||CHR(10)
||'plage horaire de service', ML_HEADING_DE = 'Reaktionszeitraum'||CHR(13)||CHR(10)
||'Tage' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'serv_window_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Fin de'||CHR(13)||CHR(10)
||'plazo de servicio', ML_HEADING_IT = 'Fine periodo'||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Einde'||CHR(13)||CHR(10)
||'servicetijd', ML_HEADING_FR = 'Fin de la'||CHR(13)||CHR(10)
||'plage horaire de service', ML_HEADING_DE = 'Reaktionszeitraum'||CHR(13)||CHR(10)
||'Ende' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'serv_window_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Inicio de'||CHR(13)||CHR(10)
||'plazo de servicio', ML_HEADING_IT = 'Inizio periodo'||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Begin'||CHR(13)||CHR(10)
||'servicetijd', ML_HEADING_FR = 'D�but de la'||CHR(13)||CHR(10)
||'plage horaire de service', ML_HEADING_DE = 'Reaktionszeitraum'||CHR(13)||CHR(10)
||'Anfang' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'serv_window_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demand�;Rev;Examin�/En attente;Rej;Rejet�;A;Approuv�;AA;Affect� � Bon Trvx;I;Emis/En cours;HP;En attente/Pi�ces d�t.;HA;En attente/Acc�s;HL;En attente/Md�O;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�', ML_HEADING_IT = 'Stato'||CHR(13)||CHR(10)
||'richiesta di lavoro', ML_HEADING_NL = 'Status '||CHR(13)||CHR(10)
||'werkaanvraag', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'demande d�intervention' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Orden de'||CHR(13)||CHR(10)
||'estado', ML_HEADING_NL = 'Status'||CHR(13)||CHR(10)
||'sorteren', ML_HEADING_FR = 'Tri par'||CHR(13)||CHR(10)
||'statut' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'status_sort';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_ES = 'Estado de paso de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'Stapstatus', ML_HEADING_FR = 'Statut de l��tape'||CHR(13)||CHR(10)
||'de demande d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'schritt Status' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora de finalizaci�n'||CHR(13)||CHR(10)
||'del trabajo', ML_HEADING_IT = 'Ora completamento'||CHR(13)||CHR(10)
||'lavoro', ML_HEADING_NL = 'Tijdstip'||CHR(13)||CHR(10)
||'werk voltooid', ML_HEADING_FR = 'Travaux'||CHR(13)||CHR(10)
||'termin�s �' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'time_completed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de terminaci�n', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'completamento', ML_HEADING_NL = 'Tijdstip waarop uitvoeringstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'time_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de respuesta', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'risposta', ML_HEADING_NL = 'Tijdstip waarop responstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'time_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora di richiesta'||CHR(13)||CHR(10)
||'OdL', ML_HEADING_NL = 'Tijdstip werk'||CHR(13)||CHR(10)
||'aangevraagd', ML_HEADING_FR = 'Travaux'||CHR(13)||CHR(10)
||'demand�s �' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'time_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'������', ML_HEADING_FR = 'Heure de derni�re'||CHR(13)||CHR(10)
||'modification de statut', ML_HEADING_DE = 'Uhrzeit der'||CHR(13)||CHR(10)
||'letzten Status�nderung' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'time_stat_chg';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Settore primario'||CHR(13)||CHR(10)
||'richiesto', ML_HEADING_NL = 'Primaire vakdiscipline'||CHR(13)||CHR(10)
||'vereist', ML_HEADING_FR = 'Corps de m�tier'||CHR(13)||CHR(10)
||'principal requis' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'tr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�'||CHR(13)||CHR(10)
||'���', ML_HEADING_NL = 'Primaire'||CHR(13)||CHR(10)
||'leverancier', ML_HEADING_FR = 'Fournisseur'||CHR(13)||CHR(10)
||'principal' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_ES = 'Asignado a'||CHR(13)||CHR(10)
||'orden de trabajo', ML_HEADING_IT = 'Assegnato a'||CHR(13)||CHR(10)
||'ordine di lavoro', ML_HEADING_FR = 'Affect� au'||CHR(13)||CHR(10)
||'bon de travx' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'wo_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de equipo de trabajo', ML_HEADING_IT = 'Codice team di lavoro', ML_HEADING_NL = 'Werkteam code', ML_HEADING_FR = 'Code de l��quipe d�intervention', ML_HEADING_DE = 'Kurzzeichen Arbeitsteam' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'nummer' WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'wrpt_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET SL_HEADING_FR = 'Code Demande d�Intervention' WHERE TABLE_NAME = 'wrpt_sync' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'wrtl_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'wrtl_sync' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'wrtr_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'wrtr_sync' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Type d�Outil' WHERE TABLE_NAME = 'wrtt' AND FIELD_NAME = 'tool_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'cuenta', ML_HEADING_NL = 'Account-'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Poste'||CHR(13)||CHR(10)
||'Comptable' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_ES = 'Horas reales de'||CHR(13)||CHR(10)
||'recursos humanos', ML_HEADING_FR = 'Heures r�elles'||CHR(13)||CHR(10)
||'main d�oeuvre', ML_HEADING_DE = 'Tats�chliche'||CHR(13)||CHR(10)
||'Arbeitsstunden' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'act_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�� ID', ML_HEADING_ES = 'Identificador'||CHR(13)||CHR(10)
||'de actividad', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'azione', ML_HEADING_NL = 'Actiepunt-ID', ML_HEADING_FR = 'Identifiant d��l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'acci�n', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action', ML_HEADING_DE = 'Ma�nahmentyp' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�������?', ML_HEADING_NL = 'Werk op'||CHR(13)||CHR(10)
||'feestdagen toestaan?', ML_HEADING_FR = 'Autoriser les travaux'||CHR(13)||CHR(10)
||'pendant les jours f�ri�s�?' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'allow_work_on_holidays';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'de cause', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ursache' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cause_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Note'||CHR(13)||CHR(10)
||'addetti alla manutenzione', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'vakmensen', ML_HEADING_FR = 'Notes des'||CHR(13)||CHR(10)
||'techniciens', ML_HEADING_DE = 'Anmerkungen zu'||CHR(13)||CHR(10)
||'Handwerkern' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cf_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Completado'||CHR(13)||CHR(10)
||'por', ML_HEADING_NL = 'Voltooid'||CHR(13)||CHR(10)
||'per' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'completed_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Coste de recursos'||CHR(13)||CHR(10)
||'humanos estimado', ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'manodopera stimato', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten van arbeid', ML_HEADING_FR = 'Co�t de la main-d�oeuvre'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_est_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Overige kosten', ML_HEADING_FR = 'Autres co�ts'||CHR(13)||CHR(10)
||'estim�s', ML_HEADING_DE = 'Gesch�tzte'||CHR(13)||CHR(10)
||'Sonstige Kosten' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_est_other';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'stimato parti', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten van onderdelen', ML_HEADING_FR = 'Co�t des pi�ces d�tach�es'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_est_parts';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'stimato attrezzi', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten van gereedschappen', ML_HEADING_FR = 'Co�t des outils'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_est_tools';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Costo totale'||CHR(13)||CHR(10)
||'stimato', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Totale  kosten', ML_HEADING_FR = 'Co�t total'||CHR(13)||CHR(10)
||'estim�' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_est_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'recursos humanos', ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'main-d�oeuvre', ML_HEADING_DE = 'Arbeits-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Overig'||CHR(13)||CHR(10)
||'kosten', ML_HEADING_FR = 'Autres'||CHR(13)||CHR(10)
||'co�ts' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_other';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'piezas', ML_HEADING_IT = 'Costo delle'||CHR(13)||CHR(10)
||'parti', ML_HEADING_FR = 'Co�t des'||CHR(13)||CHR(10)
||'pi�ces d�tach�es', ML_HEADING_DE = 'Teile-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_parts';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'herramientas', ML_HEADING_IT = 'Costo degli'||CHR(13)||CHR(10)
||'attrezzi', ML_HEADING_NL = 'Kosten van'||CHR(13)||CHR(10)
||'gereedschap', ML_HEADING_FR = 'Co�t des'||CHR(13)||CHR(10)
||'outils', ML_HEADING_DE = 'Werkzeug-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_tools';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'totale', ML_HEADING_NL = 'uitgaven'||CHR(13)||CHR(10)
||'Kosten', ML_HEADING_FR = 'Co�t'||CHR(13)||CHR(10)
||'total', ML_HEADING_DE = 'Gesamt-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Lettura'||CHR(13)||CHR(10)
||'attuale [mt]', ML_HEADING_FR = 'Lecture courante'||CHR(13)||CHR(10)
||'compteur �quipt' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'curr_meter_val';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'esecuzione', ML_HEADING_NL = 'Geplande'||CHR(13)||CHR(10)
||'uitvoer.datum', ML_HEADING_FR = 'A ex�cuter'||CHR(13)||CHR(10)
||'le' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Fecha de cierre de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_FR = 'Demande d�intervention'||CHR(13)||CHR(10)
||'ferm�e le', ML_HEADING_DE = 'Datum - Arbeits-'||CHR(13)||CHR(10)
||'anforderung abgeschlossen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data completamento'||CHR(13)||CHR(10)
||'lavoro', ML_HEADING_FR = 'Travaux'||CHR(13)||CHR(10)
||'termin�s le' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_completed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding voor'||CHR(13)||CHR(10)
||'uitvoeringstijd optreedt', ML_HEADING_FR = 'Recours hi�rarchique pour'||CHR(13)||CHR(10)
||'ex�cution le' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding'||CHR(13)||CHR(10)
||'responstijd optreedt', ML_HEADING_FR = 'Remise � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse le', ML_HEADING_DE = 'Termineskalation f�r'||CHR(13)||CHR(10)
||'Reaktion tritt auf' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'terminaci�n estd.', ML_HEADING_FR = 'Fin estim�e'||CHR(13)||CHR(10)
||'pour le', ML_HEADING_DE = 'Datum der gesch�tzten'||CHR(13)||CHR(10)
||'Fertigstellung' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_est_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Fecha de trabajo'||CHR(13)||CHR(10)
||'solicitado', ML_HEADING_IT = 'Data richiesta'||CHR(13)||CHR(10)
||'di lavoro', ML_HEADING_FR = 'Travaux demand�s'||CHR(13)||CHR(10)
||'le' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de derni�re'||CHR(13)||CHR(10)
||'modification de statut' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'date_stat_chg';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Descripci�n de'||CHR(13)||CHR(10)
||'otros costes', ML_HEADING_IT = 'Descrizione altri'||CHR(13)||CHR(10)
||'costi', ML_HEADING_NL = 'Overige kosten'||CHR(13)||CHR(10)
||'gegevenspunt', ML_HEADING_FR = 'Description des'||CHR(13)||CHR(10)
||'autres co�ts', ML_HEADING_DE = 'Sonstige Kosten'||CHR(13)||CHR(10)
||'Beschreibung' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'desc_other_costs';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Werkbeschrijving', ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'des travaux' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||' (���)', ML_HEADING_ES = 'Inactividad'||CHR(13)||CHR(10)
||'de equipo (horas)', ML_HEADING_IT = 'Fermo'||CHR(13)||CHR(10)
||'apparecchiatura (ore)', ML_HEADING_NL = 'Uitvaltijd van'||CHR(13)||CHR(10)
||'uitrusting (uren)', ML_HEADING_FR = 'Equipement'||CHR(13)||CHR(10)
||'indisponible (h)', ML_HEADING_DE = 'Ausfallszeit des'||CHR(13)||CHR(10)
||'Ger�ts (Stunden)' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'down_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Ger�tekurzzeichen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'eq_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'afronding?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Erledigung?' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'escalated_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'antwoord?', ML_HEADING_FR = 'Remont� � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Reaktion?' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'escalated_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Horas estimadas'||CHR(13)||CHR(10)
||'de recursos humanos', ML_HEADING_FR = 'Heures estim�es de'||CHR(13)||CHR(10)
||'main-d�oeuvre' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'est_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Probleem-'||CHR(13)||CHR(10)
||'locatie', ML_HEADING_FR = 'Localisation du'||CHR(13)||CHR(10)
||'probl�me' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'location';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Gestor de centro'||CHR(13)||CHR(10)
||'de soporte', ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'du centre de services', ML_HEADING_DE = 'Servicedesk'||CHR(13)||CHR(10)
||'Manager' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'manager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '������', ML_HEADING_IT = 'Stato'||CHR(13)||CHR(10)
||'consegna messaggio', ML_HEADING_NL = 'Status'||CHR(13)||CHR(10)
||'berichtbezorging', ML_HEADING_FR = 'Statut de l�envoi'||CHR(13)||CHR(10)
||'du message', ML_HEADING_DE = 'Nachrichtempfang'||CHR(13)||CHR(10)
||'Status' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��1' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'option1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��2', ML_HEADING_ES = 'Opci�n'||CHR(13)||CHR(10)
||'2' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'option2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'PO-'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_DE = 'IH-'||CHR(13)||CHR(10)
||'Vorgang' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'pmp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM ���'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'programmazione MP', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'PO-schema', ML_HEADING_FR = 'Code de planning'||CHR(13)||CHR(10)
||'de MP', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'IH-Plan' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'pms_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Prioridad de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_IT = 'Priorit�'||CHR(13)||CHR(10)
||'richiesta di lavoro', ML_HEADING_NL = 'Prioriteit'||CHR(13)||CHR(10)
||'werkaanvraag', ML_HEADING_FR = 'Priorit� de la'||CHR(13)||CHR(10)
||'demande d�intervention', ML_HEADING_DE = 'Arbeitsanforderung - '||CHR(13)||CHR(10)
||'Priorit�t' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'problema', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di problema', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'probl�me' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'prob_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di riparazione', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'r�paration' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'repair_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de espacio', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'ruimtereservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de pi�ce', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Raumreservierung' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'rmres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de recurso', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'resource-reservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de ressource', ML_HEADING_DE = 'Ressourcenreservierungscode' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'rsres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Calificaci�n de'||CHR(13)||CHR(10)
||'la satisfacci�n', ML_HEADING_NL = 'Tevredenheids-'||CHR(13)||CHR(10)
||'score', ML_HEADING_FR = 'Note de'||CHR(13)||CHR(10)
||'satisfaction', ML_HEADING_DE = 'Zufriedenheitsbewertung' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'satisfaction';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Notas de'||CHR(13)||CHR(10)
||'satisfacci�n', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'bij waardering' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'satisfaction_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�����', ML_HEADING_ES = 'D�as de'||CHR(13)||CHR(10)
||'plazo de servicio', ML_HEADING_IT = 'Giorni periodo'||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Dagen'||CHR(13)||CHR(10)
||'servicetijd', ML_HEADING_FR = 'Jours de la'||CHR(13)||CHR(10)
||'plage horaire de service', ML_HEADING_DE = 'Reaktionszeitraum'||CHR(13)||CHR(10)
||'Tage' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'serv_window_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Fin de'||CHR(13)||CHR(10)
||'plazo de servicio', ML_HEADING_IT = 'Fine periodo'||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Einde'||CHR(13)||CHR(10)
||'servicetijd', ML_HEADING_FR = 'Fin de la'||CHR(13)||CHR(10)
||'plage horaire de service', ML_HEADING_DE = 'Reaktionszeitraum'||CHR(13)||CHR(10)
||'Ende' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'serv_window_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Inicio de'||CHR(13)||CHR(10)
||'plazo de servicio', ML_HEADING_IT = 'Inizio periodo'||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Begin'||CHR(13)||CHR(10)
||'servicetijd', ML_HEADING_FR = 'D�but de la'||CHR(13)||CHR(10)
||'plage horaire de service', ML_HEADING_DE = 'Reaktionszeitraum'||CHR(13)||CHR(10)
||'Anfang' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'serv_window_start';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demand�;Rev;Examin�/En attente;Rej;Rejet�;A;Approuv�;AA;Affect� � Bon Trvx;I;Emis/En cours;HP;En attente/Pi�ces d�t.;HA;En attente/Acc�s;HL;En attente/Md�O;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�', ML_HEADING_IT = 'Stato'||CHR(13)||CHR(10)
||'richiesta di lavoro', ML_HEADING_NL = 'Status '||CHR(13)||CHR(10)
||'werkaanvraag', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'demande d�intervention' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Orden de'||CHR(13)||CHR(10)
||'estado', ML_HEADING_NL = 'Status'||CHR(13)||CHR(10)
||'sorteren', ML_HEADING_FR = 'Tri par'||CHR(13)||CHR(10)
||'statut' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'status_sort';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_ES = 'Estado de paso de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'Stapstatus', ML_HEADING_FR = 'Statut de l��tape'||CHR(13)||CHR(10)
||'de demande d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'schritt Status' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora de finalizaci�n'||CHR(13)||CHR(10)
||'del trabajo', ML_HEADING_IT = 'Ora completamento'||CHR(13)||CHR(10)
||'lavoro', ML_HEADING_NL = 'Tijdstip'||CHR(13)||CHR(10)
||'werk voltooid', ML_HEADING_FR = 'Travaux'||CHR(13)||CHR(10)
||'termin�s �' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'time_completed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de terminaci�n', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'completamento', ML_HEADING_NL = 'Tijdstip waarop uitvoeringstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'time_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de respuesta', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'risposta', ML_HEADING_NL = 'Tijdstip waarop responstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'time_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora di richiesta'||CHR(13)||CHR(10)
||'OdL', ML_HEADING_NL = 'Tijdstip werk'||CHR(13)||CHR(10)
||'aangevraagd', ML_HEADING_FR = 'Travaux'||CHR(13)||CHR(10)
||'demand�s �' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'time_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'������', ML_HEADING_FR = 'Heure de derni�re'||CHR(13)||CHR(10)
||'modification de statut', ML_HEADING_DE = 'Uhrzeit der'||CHR(13)||CHR(10)
||'letzten Status�nderung' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'time_stat_chg';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Settore primario'||CHR(13)||CHR(10)
||'richiesto', ML_HEADING_NL = 'Primaire vakdiscipline'||CHR(13)||CHR(10)
||'vereist', ML_HEADING_FR = 'Corps de m�tier'||CHR(13)||CHR(10)
||'principal requis' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'tr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�'||CHR(13)||CHR(10)
||'���', ML_HEADING_NL = 'Primaire'||CHR(13)||CHR(10)
||'leverancier', ML_HEADING_FR = 'Fournisseur'||CHR(13)||CHR(10)
||'principal' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_ES = 'Asignado a'||CHR(13)||CHR(10)
||'orden de trabajo', ML_HEADING_IT = 'Assegnato a'||CHR(13)||CHR(10)
||'ordine di lavoro', ML_HEADING_FR = 'Affect� au'||CHR(13)||CHR(10)
||'bon de travx' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'wo_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de equipo de trabajo', ML_HEADING_IT = 'Codice team di lavoro', ML_HEADING_NL = 'Werkteam code', ML_HEADING_FR = 'Code de l��quipe d�intervention', ML_HEADING_DE = 'Kurzzeichen Arbeitsteam' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'nummer' WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Zone'||CHR(13)||CHR(10)
||'d�Acc�s' WHERE TABLE_NAME = 'wy' AND FIELD_NAME = 'access_space';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'wy' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'zone' AND FIELD_NAME = 'ehandle';

UPDATE AFM.AFM_HOLIDAY_DATES SET DESCRIPTION_FR = 'Jour de l�an', DESCRIPTION_IT = 'Nuovo giorno dell�anno' WHERE AUTO_NUMBER = 5;

UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_DE = 'Bericht: Unternehmenseinheiten' WHERE SUPER_CAT = '1) Lease & Real Property' AND CATEGORY = 'b. Organization' AND VIEW_TITLE = 'Business Units Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Registres d�activit�s des propri�t�s par propri�t�' WHERE SUPER_CAT = '1) Lease & Real Property' AND CATEGORY = 'd. Properties' AND VIEW_TITLE = 'Property Activity Log Items by Property Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : El�ments des registres d�activit�s par projet' WHERE SUPER_CAT = '1) Lease & Real Property' AND CATEGORY = 'h. Projects' AND VIEW_TITLE = 'Activity Log Items by Project Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'M�mento des registres d�activit�s' WHERE SUPER_CAT = '1) Lease & Real Property' AND CATEGORY = 'h. Projects' AND VIEW_TITLE = 'Activity Log Tickler';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Graphe : Diagramme d�empilement des d�partements - Pi�ces' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'a. Organization Information' AND VIEW_TITLE = 'Departmental Stack Plan - Rooms Chart';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_DE = 'Bericht: Abteilungen nach Unternehmenseinheit' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'a. Organization Information' AND VIEW_TITLE = 'Departments by Business Unit Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Analyse des standards d�employ�s par �tage' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'e. Personnel' AND VIEW_TITLE = 'Employee Standard Analysis by Floor Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_DE = 'Bericht: Personal nach Unternehmenseinheit' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'e. Personnel' AND VIEW_TITLE = 'Employees by Business Unit Report';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Containment Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = '-- Informations d��valuation des employ�s' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Employee Assessment Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Employee Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = '-- Informations d��valuation des infrastructures' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Facility Assessment Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- First Responder and EMT Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Management Situation Reports';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = '-- Informations sur l��quipe de GSU' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Recovery Team Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = '-- Web Page';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Bulletins d�information � l�attention des employ�s' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Advisory Bulletin for Employees';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Bulletins d�Information � l�attention des Responsables' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Advisory Bulletin for Managers';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Plans d�Evacuation' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Egress Plan';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Contacts en Cas d�Urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Emergency Contacts';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Contacts en Situations d�Urgence par B�timent' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Emergency Contacts by Building';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Localisateur d�employ�s' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Employee Locator';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Employee Status by Building';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Employee Status by Division';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Employ�s et informations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Employees and Emergency Information';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Employ�s et Informations d�Urgence par Etage' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Employees and Emergency Information by Floor';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Employees by Room';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Hazardous Material Plan';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Highlight Systems and their Zones';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Intranet Web Page';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Plan d�Occupation' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Occupancy Plan';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Contacts Hi�rarchiques de l�Equipe de GSU' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Recovery Team Escalation Contacts';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Site Status';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Status Summary';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Systems, Zones, and Rooms';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence', VIEW_TITLE_FR = 'Mettre � Jour un Bulletin d�Information' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Update Advisory Bulletin';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Update Employee Status';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Update Equipment Status';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Update Room Status';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Gestion des situations d�urgence' WHERE SUPER_CAT = '2) Space' AND CATEGORY = 'f. Emergency Preparedness' AND VIEW_TITLE = 'Update System Status';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Standards d��quipement' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'a. Standards' AND VIEW_TITLE = 'Equipment Standards Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Polices d�assurance' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'a. Standards' AND VIEW_TITLE = 'Policies Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Polices d�assurance par assureur' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'a. Standards' AND VIEW_TITLE = 'Policies by Insurer Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Mobilier cod� par police d�assurance' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'b. Tagged Furniture' AND VIEW_TITLE = 'Tagged Furniture by Insurance Policy Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Localiser l��quipement, avec surbrillance' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment - Locate and Highlight Drawing';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : D�comptes d��quipement par standard' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Counts by Standard Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : D�comptes d��quipement par standard par b�timent' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Counts by Standard by Building Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : D�comptes d��quipement par standard par d�partement' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Counts by Standard by Department Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : D�comptes d��quipement par standard par employ�' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Counts by Standard by Employee Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : D�comptes d��quipement par standard par �tage' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Counts by Standard by Floor Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Inventaire d��quipement' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Inventory Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Plan de l��quipement' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment Plan';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Equipement par police d�assurance' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'c. Equipment' AND VIEW_TITLE = 'Equipment by Insurance Policy Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : D�comptes d�inventaire de stds de mobilier par std' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'd. Furniture Standards' AND VIEW_TITLE = 'Furniture Standard Inventory Counts by Standard Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Inventaire de stds de mobilier par police d�assurance' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'd. Furniture Standards' AND VIEW_TITLE = 'Furniture Standard Inventory by Insurance Policy Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'D�comptes d�Inventaire de stds de mobilier par std par d�pt.' WHERE SUPER_CAT = '3) Furniture & Equipment' AND CATEGORY = 'd. Furniture Standards' AND VIEW_TITLE = 'Furniture Standards Inventory Counts by Standard by Department';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Standards d��quipement donn�es des zones de travail' WHERE SUPER_CAT = '4) Telecommunications & Cabling' AND CATEGORY = 'b. Work Areas' AND VIEW_TITLE = 'Work Area Data Equipment Standards Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Standards d��quipement des zones de travail' WHERE SUPER_CAT = '4) Telecommunications & Cabling' AND CATEGORY = 'b. Work Areas' AND VIEW_TITLE = 'Work Area Equipment Standards Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Standards d��quipement voix des zones de travail' WHERE SUPER_CAT = '4) Telecommunications & Cabling' AND CATEGORY = 'b. Work Areas' AND VIEW_TITLE = 'Work Area Voice Equipment Standards Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Inventaire d��quipement' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'd. Equipment' AND VIEW_TITLE = 'Equipment Inventory Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Historique : Maintenance d�Equipement' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'd. Equipment' AND VIEW_TITLE = 'Equipment Maintenance History';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Pi�ces d�tach�es d��quipement par �quipement' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'd. Equipment' AND VIEW_TITLE = 'Equipment Parts by Equipment Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Planning de l��quipement de production' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'd. Equipment' AND VIEW_TITLE = 'Equipment Production Schedule';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Standards d��quipement' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'd. Equipment' AND VIEW_TITLE = 'Equipment Standards Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Planning des travaux d��quipement' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'd. Equipment' AND VIEW_TITLE = 'Equipment Work Schedule';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Main d�Oeuvre' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'f. Labor' AND VIEW_TITLE = 'Craftspersons Report';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Main d�Oeuvre' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'f. Labor' AND VIEW_TITLE = 'Craftspersons Workload';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Main d�Oeuvre' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'f. Labor' AND VIEW_TITLE = 'Craftspersons by Trade Report';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'f. Main d�Oeuvre' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'f. Labor' AND VIEW_TITLE = 'Trades Workload';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Demandes d�intervention en cours par d�timent' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Active Work Requests by Building Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Demandes d�intervention en cours par par �quipement' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Active Work Requests by Equipment Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Plan des demandes d�intervention en cours par par �tage' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Active Work Requests by Floor Plan';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Approuver et �mettre des demandes d�intervention' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Approve and Issue Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Techniciens - Mettre � Jour les Demandes d�Intervention' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Craftspersons - Update Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Cr�er des demandes d�intervention' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Create Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Demandes d�intervention ouvertes par statut' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Open Work Requests By Status Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Statuts des demandes d�intervention ouvertes' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Open Work Requests Status Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Examiner des Demandes d�Intervention' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'i. Work' AND VIEW_TITLE = 'Review Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Rapport: PO-schema�s per uitrusting' WHERE SUPER_CAT = '5) Building Operations' AND CATEGORY = 'j. Preventive Maintenance' AND VIEW_TITLE = 'PM Schedules by Equipment Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Plan des demandes d�intervention en cours par par �tage' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Active Work Requests by Floor Plan';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Approuver et �mettre des demandes d�intervention' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Approve and Issue Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Techniciens - Mettre � Jour les Demandes d�Intervention' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Craftspersons - Update Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Cr�er des demandes d�intervention' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Create Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Graphe : Diagramme d�empilement des d�partements - Pi�ces' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Departmental Stack Plan - Rooms Chart';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Localiser l��quipement, avec surbrillance' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Equipment - Locate and Highlight Drawing';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Examiner des Demandes d�Intervention' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Review Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Page d�Accueil version 1.0' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'a. Hotpicks' AND VIEW_TITLE = 'Version 1.0 Home Page';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'c. Barres d�outils' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'c. Toolbars' AND VIEW_TITLE = 'All Modules';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'c. Barres d�outils' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'c. Toolbars' AND VIEW_TITLE = 'Building Operations';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'c. Barres d�outils' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'c. Toolbars' AND VIEW_TITLE = 'Furniture and Equipment';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'c. Barres d�outils' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'c. Toolbars' AND VIEW_TITLE = 'Real Property and Lease';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'c. Barres d�outils' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'c. Toolbars' AND VIEW_TITLE = 'Space';
UPDATE AFM.AFM_HOTLIST SET CATEGORY_FR = 'c. Barres d�outils' WHERE SUPER_CAT = '6) Hot Stuff' AND CATEGORY = 'c. Toolbars' AND VIEW_TITLE = 'Telecommunications and Cabling';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Scenario�s van Web Central' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'b. Introduction' AND VIEW_TITLE = 'Web Central Scenarios';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Qu�est-ce que Web Central ?', VIEW_TITLE_IT = 'Che cos�� Web Central?' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'b. Introduction' AND VIEW_TITLE = 'What is Web Central?';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Visite guid�e d�ARCHIBUS/FM Web Central' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'A Tour of ARCHIBUS/FM Web Central';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Graphe : Diagramme d�empilement des d�partements - Pi�ces' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Departmental Stack Plan - Rooms Chart';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Plan de l��quipement' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Equipment Plan';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Explorations graphiques de l�organisation' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Organizational Drill Downs';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Rapport : Polices d�assurance par assureur' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Policies by Insurers Report';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Gestion de l�espace' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Space Management';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Page d�accueil Version 0' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Version 1 Home Page';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Fonctions de demandes d�intervention' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'c. Tour' AND VIEW_TITLE = 'Work Request Features';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Comment mettre � Jour des demandes d�intervention' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'How To Update Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Comment approuver et �mettre des demandes d�intervention' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'How to Approve and Issue Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Comment cr�er une demande d�intervention' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'How to Create a Work Request';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Comment consulter des demandes d�intervention' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'How to Review Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Fonctions de demandes de d�m�nagement d�ARCHBUS/FM' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'The ARCHIBUS/FM Move Request Features';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Fonctions de bons de travaux d�ARCHIBUS/FM' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'The ARCHIBUS/FM Work Order Features';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Detailpagina�s gebruiken' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'Using Detail Pages';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Doorklikken-pagina�s per tekening gebruiken', VIEW_TITLE_FR = 'Utiliser les pages d�exploration graphique' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'Using Drawing Drill-Down Pages';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Doorklikken-pagina�s gebruiken', VIEW_TITLE_FR = 'Utiliser les pages d�exploration' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'Using Drill-Down Pages';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_DE = 'Dynamisch Dialoge bearbeiten� benutzen', VIEW_TITLE_FR = 'Utiliser les formulaires d��dition dynamiques' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'Using Dynamic Edit Forms';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Rasterpagina�s gebruiken' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'd. Using' AND VIEW_TITLE = 'Using Grid Pages';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Choisir l�une ou l�autre des versions' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Choosing Either Version';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_IT = 'Configurazione di Cold Fusion per l�e-mail' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Configuring Cold Fusion for E-Mail';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_IT = 'Configurazione dell�accesso di servizio per i database Oracle' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Configuring the Service Login for Oracle Databases';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Configurer les clients de l�Explorateur Web' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Configuring the Web Browser Clients';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Contr�ler les rafra�chissements de l�Explorateur Web' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Controlling Browser Refreshes';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Installer l�application ARCHIBUS/FM', VIEW_TITLE_IT = 'Installa l�applicazione ARCHIBUS' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Install the ARCHIBUS/FM Application';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Autres sources d�informations' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Other Sources of Information';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Aper�u de l�installation', VIEW_TITLE_IT = 'Panoramica dell�installazione' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Overview of the Installation';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'D�marrer l�Applet ou la version Intranet' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Starting the Applet or Intranet Version';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'D�marrer l�Administrateur Cold Fusion' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'e. Installation' AND VIEW_TITLE = 'Starting the Cold Fusion Administrator';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Detailniveaupagina�s' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Detail Level Pages';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_IT = 'Modifica dell�elenco preferiti' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Editing the Hotlist';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Valeurs des champs de listes d��num�ration' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Enumerated Field Values';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Cacher les adresses IP dans les demandes d�intervention' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Hiding IP Addresses in Work Requests';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Web-Navigatorcommando�s uitbreiden' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'How to Add Items to the Web Navigator';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Web-Navigatorcommando�s verwijderen' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'How to Remove Items from the Web Navigator';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Limitations et notes d�utilisation', VIEW_TITLE_IT = 'Limitazioni e note sull�utilizzo' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Limitations and Usage Notes';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_IT = 'Manutenzione di un�installazione Web Central' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Maintaining a Web Central Installation';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Detailpagina�s van memovelden' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Memo Field Detail Pages';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_NL = 'Actie �Als Websjabloon publiceren� uitvoeren', VIEW_TITLE_DE = 'Aktion �Web-Vorlage ver�ffentlichen� ausf�hren', VIEW_TITLE_FR = 'Ex�cuter l�action Publier en mod�le Web', VIEW_TITLE_IT = 'Esecuzione dell�azione Pubblica come modello Web' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Running the Publish as Web Template Action';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Utiliser des liens d�acc�s s�par�s' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Using Separate Access Links';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_DE = 'Schaltfl�che  �Weitere>>� benutzen', VIEW_TITLE_FR = 'Utiliser le bouton �Suivants�' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'f. Maintaining' AND VIEW_TITLE = 'Using The More Button';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Questions d�utilisation' WHERE SUPER_CAT = '7) Help' AND CATEGORY = 'g. Questions' AND VIEW_TITLE = 'Usage Questions';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Analyse : Historique d�utilisation de l�espace' WHERE SUPER_CAT = 'A) Strategic Management' AND CATEGORY = '3. Planning' AND VIEW_TITLE = 'Historical Space Usage Trends';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Dates d�Exercice des Options' WHERE SUPER_CAT = 'B) Business Management' AND CATEGORY = '2. Leases' AND VIEW_TITLE = 'Options Exercise Dates';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Plan d�empilement des d�partements' WHERE SUPER_CAT = 'B) Business Management' AND CATEGORY = '3. Buildings' AND VIEW_TITLE = 'Department Stack Plan';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Annuler une attribution de bureau de passage' WHERE SUPER_CAT = 'C) Operations Management' AND CATEGORY = '2. Hoteling' AND VIEW_TITLE = 'Cancel Booking';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Analyse : Main d�oeuvre' WHERE SUPER_CAT = 'C) Operations Management' AND CATEGORY = '3. Work Orders' AND VIEW_TITLE = 'Labor Analysis';
UPDATE AFM.AFM_HOTLIST SET VIEW_TITLE_FR = 'Afficher le plan d�occupation' WHERE SUPER_CAT = 'D) Administrative and Corporate Services' AND CATEGORY = '3. Employees' AND VIEW_TITLE = 'Show Occupancy Plan';

UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plan de gestion d��chantillons de mati�res dangereuses relat' WHERE LAYER_NAME = 'CB-SAMPLE';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plan de gestion d��chantillons de mati�res dangereuses relat' WHERE LAYER_NAME = 'CB-SAMPLE$';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plan de gestion d��chantillons de mati�res dangereuses relat' WHERE LAYER_NAME = 'CB-SAMPLE$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plan de gestion d��chantillons de mati�res dangereuses relat' WHERE LAYER_NAME = 'CB-SAMPLE-HL';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Diagrammes d�Affinit�s' WHERE LAYER_NAME = 'DP';
UPDATE AFM.AFM_LAYR SET TITLE_ES = 'Equipo' WHERE LAYER_NAME = 'EQ';
UPDATE AFM.AFM_LAYR SET TITLE_ES = 'Equipo' WHERE LAYER_NAME = 'EQ$';
UPDATE AFM.AFM_LAYR SET TITLE_ES = 'Equipo' WHERE LAYER_NAME = 'EQ$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plans d�Evacuation' WHERE LAYER_NAME = 'REG-EGRESS';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plans d�Evacuation' WHERE LAYER_NAME = 'REG-EGRESS$';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plans d�Evacuation' WHERE LAYER_NAME = 'REG-EGRESS$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Plans d�Evacuation' WHERE LAYER_NAME = 'REG-EGRESS-HL';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 1' WHERE LAYER_NAME = 'RM-TRIAL1';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 1' WHERE LAYER_NAME = 'RM-TRIAL1$';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 1' WHERE LAYER_NAME = 'RM-TRIAL1$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 1-��' WHERE LAYER_NAME = 'RM-TRIAL1-MO';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 2' WHERE LAYER_NAME = 'RM-TRIAL2';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 2' WHERE LAYER_NAME = 'RM-TRIAL2$';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 2' WHERE LAYER_NAME = 'RM-TRIAL2$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 2 -��' WHERE LAYER_NAME = 'RM-TRIAL2-MO';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 3' WHERE LAYER_NAME = 'RM-TRIAL3';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 3' WHERE LAYER_NAME = 'RM-TRIAL3$';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 3' WHERE LAYER_NAME = 'RM-TRIAL3$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_CH = '�� 3 -��' WHERE LAYER_NAME = 'RM-TRIAL3-MO';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Texte de Requ�te d�Occupation des Pi�ces' WHERE LAYER_NAME = 'Z-EM-QTEXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Texte de Requ�te d�Imputation en Pourcentages de Pi�ces' WHERE LAYER_NAME = 'Z-RMPCT-COMN-QTEXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Texte de Requ�te d�Imputation en Pourcentages de Pi�ces' WHERE LAYER_NAME = 'Z-RMPCT-DP-QTEXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Texte de Requ�te d�Imputation en Pourcentages de Pi�ces' WHERE LAYER_NAME = 'Z-RMPCT-TYPE-QTEXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d�Eclairage de Secours' WHERE LAYER_NAME = 'ZONE-EMERGENCY';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d�Eclairage de Secours' WHERE LAYER_NAME = 'ZONE-EMERGENCY$';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d�Eclairage de Secours' WHERE LAYER_NAME = 'ZONE-EMERGENCY$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d�Eclairage de Secours' WHERE LAYER_NAME = 'ZONE-EMERGENCY-HL';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d��quipement de protection incendie ou fum�e' WHERE LAYER_NAME = 'ZONE-FIRE';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d��quipement de protection incendie ou fum�e' WHERE LAYER_NAME = 'ZONE-FIRE$';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d��quipement de protection incendie ou fum�e' WHERE LAYER_NAME = 'ZONE-FIRE$TXT';
UPDATE AFM.AFM_LAYR SET TITLE_FR = 'Zones d��quipement de protection incendie ou fum�e' WHERE LAYER_NAME = 'ZONE-FIRE-HL';

UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_AnnualWorth_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_AnnualWorth_perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_FR = 'Projets d�investissement (ex. fisc.)' WHERE METRIC_NAME = 'cap_CapitalProjects-Approved_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_FR = 'Projets d�investissement par surface brute (ex. fisc.)', REPORT_BENCHMARK_VALUE = -10, REPORT_LIMIT_HIGH_CRIT = -20, REPORT_LIMIT_HIGH_WARN = -15, REPORT_LIMIT_LOW_CRIT = -1, REPORT_LIMIT_LOW_WARN = -3, REPORT_LIMIT_TARGET = -10 WHERE METRIC_NAME = 'cap_CapitalProjects-Approved_perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_CapitalProjects-Planned-3years_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_FR = 'Projets d�investissement (ex. fisc.)' WHERE METRIC_NAME = 'cap_CapitalProjects-Planned-lifecycle_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_CapitalProjects-Planned-lifetime_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_irr_3years_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_irr_lifecycle_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_irr_lifetime_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_npv_1year_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_npv_3years_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_npv_lifecycle_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'cap_npv_lifetime_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�ts d�exploitation  des actifs des propri�t�s & b�timent' WHERE METRIC_NAME = 'eam_Costs-Operating_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Dur�e de disponibilit� de l��quipement' WHERE METRIC_NAME = 'eam_EquipmentUpTime_annually';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Ventes nettes d�importation des actifs' WHERE METRIC_NAME = 'eam_ImportNetSales_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Temps disponible de l��quipement' WHERE METRIC_NAME = 'eam_equipment_available_time_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Taux d�utilisation annuel - Equipement' WHERE METRIC_NAME = 'eam_equipment_utilization_percent_annually';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Rotation de l�actif - Tous les actifs' WHERE METRIC_NAME = 'eam_fixed_asset_turnover_all_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Ouvrir des demandes d�assistance en r�ponse � des accidents' WHERE METRIC_NAME = 'ehs_EHSIncidentResponsesOpen_daily';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Nb de jours d�arr�t de travail en raison d�accidents profes' WHERE METRIC_NAME = 'ehs_OccupationalIncidentsLostWork_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Taux de durabilit� moyen du projet d��valuation environnemen' WHERE METRIC_NAME = 'env_AssessmentsSustainabilityRatingAvg_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Empreinte carbone sur chiffre d�affaires' WHERE METRIC_NAME = 'env_CarbonFootprintToRevenue_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Consommation d��nergie' WHERE METRIC_NAME = 'env_EnergyConsumption_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Consommation d��nergie (par surface brute)' WHERE METRIC_NAME = 'env_EnergyConsumption_perGrossArea_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Consommation d��nergie (par occupant)' WHERE METRIC_NAME = 'env_EnergyConsumption_perOccupant_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Ecart de factures d�eau/gaz/�lec.' WHERE METRIC_NAME = 'env_UtilityBillDiscrepancy_daily';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Consommation d�eau' WHERE METRIC_NAME = 'env_WaterConsumption_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Consommation d�eau (par surface brute)' WHERE METRIC_NAME = 'env_WaterConsumption_perGrossArea_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_IT = 'Assegnazione punteggi ai progetti: idonei per l�aggiornamento' WHERE METRIC_NAME = 'env_alert_Scoring-SelfAboveOfficial_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Factures E/G/E en attente d�approbation' WHERE METRIC_NAME = 'env_alert_billsPendingApproval_warn_daily';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Factures E/G/E impay�es � moins d�une sem. de l��ch�ance' WHERE METRIC_NAME = 'env_alert_billsUnpaid_warn_daily';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�t total d�occupation (TCO)' WHERE METRIC_NAME = 'fin_TCO_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�t total d�occupation (TCO) (par surface brute)' WHERE METRIC_NAME = 'fin_TCO_perGrossArea_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�t total d�occupation (TCO) (par occupant)' WHERE METRIC_NAME = 'fin_TCO_perOccupant_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�t total d�occupation (TCO) (par poste)' WHERE METRIC_NAME = 'fin_TCO_perSeat_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�t total d�occupation sur chiffre d�affaires total (%)' WHERE METRIC_NAME = 'fin_TCO_perTotalRevenue_percent_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_Appreciation_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_BookValue_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_CostOfCapital_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_FR = 'Amortissement - Projets d�investissement (ex. fisc.)' WHERE METRIC_NAME = 'fin_anlys_DepreciationCapProj_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_DepreciationPPE_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_Income_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_Interest_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_MarketMinusBookValue_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_MarketValue_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_Principal_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_RealEstateOpEx-perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_RealEstateOpEx-perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_RealEstateOpEx-perSeat_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_RealEstateOpEx_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_RealEstateOpEx_lifecycle_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_RemainingMortgage_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_anlys_depreciation_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Chiffre d�affaires de l�entreprise (en millions) - Brut' WHERE METRIC_NAME = 'fin_imp_CorporateRevenueGross_Millions_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Chiffre d�affaires de l�entreprise - Brut' WHERE METRIC_NAME = 'fin_imp_CorporateRevenueGross_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Chiffre d�affaires de l�entreprise - Net' WHERE METRIC_NAME = 'fin_imp_CorporateRevenueNet_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_occ_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_occ_perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_occ_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_occ_perSeat_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_own_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_own_perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_own_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_own_perSeat_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_wkpnt_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_wkpnt_perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_wkpnt_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'fin_tc_wkpnt_perSeat_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�ts - Planification de l�espace' WHERE METRIC_NAME = 'fm_Costs-SpacePlanning_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Co�ts - Planification de l�espace (par surface locative)' WHERE METRIC_NAME = 'fm_Costs-SpacePlanning_perRentableArea_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'leas_Costs-RemainingCommitment_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'leas_Costs-Rent_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -50, REPORT_LIMIT_HIGH_CRIT = -75, REPORT_LIMIT_HIGH_WARN = -50, REPORT_LIMIT_TARGET = -50 WHERE METRIC_NAME = 'leas_Costs-Rent_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -200, REPORT_LIMIT_HIGH_CRIT = -10000, REPORT_LIMIT_HIGH_WARN = -8000, REPORT_LIMIT_TARGET = -6000 WHERE METRIC_NAME = 'leas_Costs-Rent_perSeat_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'D�m�nagement d�employ�s (d�compte)' WHERE METRIC_NAME = 'move_EmpCount_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'occ_Area_perOccupant_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'occ_Area_perSeat_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Densit� d�occupation' WHERE METRIC_NAME = 'occ_Density_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'occ_Occupancy_percent_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET COLLECT_FORMULA = 'bean:occ_OccupantsMax_an', METRIC_STATUS = 'A', METRIC_TITLE_FR = 'Nbr. max. d�occupants' WHERE METRIC_NAME = 'occ_OccupantsMax_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'occ_Occupants_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'T�l�travailleurs sur taux d�occupants (%)' WHERE METRIC_NAME = 'occ_Teleworkers_percent_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_Costs-Custodial_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -1.3, REPORT_LIMIT_HIGH_CRIT = -1.6, REPORT_LIMIT_HIGH_WARN = -1.5, REPORT_LIMIT_TARGET = -1.3 WHERE METRIC_NAME = 'ops_Costs-Custodial_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -1.75, REPORT_LIMIT_HIGH_CRIT = -2.5, REPORT_LIMIT_HIGH_WARN = -2, REPORT_LIMIT_TARGET = -1.75 WHERE METRIC_NAME = 'ops_Costs-Maintenance-perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_Costs-Maintenance_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_Costs-Other_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -0.2, REPORT_LIMIT_HIGH_CRIT = -0.26, REPORT_LIMIT_HIGH_WARN = -0.23, REPORT_LIMIT_TARGET = -0.2 WHERE METRIC_NAME = 'ops_Costs-Other_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_Costs-PropertyTaxes_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -3, REPORT_LIMIT_HIGH_CRIT = -3.8, REPORT_LIMIT_HIGH_WARN = -3.01 WHERE METRIC_NAME = 'ops_Costs-PropertyTaxes_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_Costs-Security_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -0.18, REPORT_LIMIT_HIGH_CRIT = -0.3, REPORT_LIMIT_HIGH_WARN = -0.24 WHERE METRIC_NAME = 'ops_Costs-Security_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_Costs-Utility_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -2, REPORT_LIMIT_HIGH_CRIT = -2.6, REPORT_LIMIT_HIGH_WARN = -2.3, REPORT_LIMIT_TARGET = -2 WHERE METRIC_NAME = 'ops_Costs-Utility_perGrossArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_ExpensedProjects-Approved_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'ops_FacilityConditionIndex_percent_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_NL = 'PO�s op tijd (aantal)' WHERE METRIC_NAME = 'ops_OntimePMCompletion_count_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_NL = 'PO�s op tijd voltooid (%)' WHERE METRIC_NAME = 'ops_OntimePMCompletion_percent_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET COLLECT_FORMULA = '(SELECT 1.0 * count (wr_id) FROM hwr WHERE escalated_completion = 0)/(select 1.0 * count(*) from hwr)' WHERE METRIC_NAME = 'ops_SLACompliance_percent_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Demandes d�intervention (par mois)' WHERE METRIC_NAME = 'ops_WorkRequested_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Demandes d�intervention ouvertes pendant une semaine' WHERE METRIC_NAME = 'ops_alert_WorkOpen_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Equipes d�intervention�: avec moins de 70�% de conformit� a' WHERE METRIC_NAME = 'ops_alert_WorkTeams_SLA_crit_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Equipes d�intervention�: avec moins de 85�% de conformit� a' WHERE METRIC_NAME = 'ops_alert_WorkTeams_SLA_warn_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Equipes d�intervention�: avec moins de 70�% de satisfaction' WHERE METRIC_NAME = 'ops_alert_WorkTeams_Satisfaction_crit_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Equipes d�intervention�: avec moins de 85�% de satisfaction' WHERE METRIC_NAME = 'ops_alert_WorkTeams_Satisfaction_warn_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'proj_Projects_Budget_Variance_cng_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'proj_Projects_Schedule_Variance_cng_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Valeur de remplacement d�actifs de b�timent (par ann�e)' WHERE METRIC_NAME = 'repm_BuildingAssetsReplacementValuePerYear_monthly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'En attente d�une r�ponse de d�claration de l�usine de trai' WHERE METRIC_NAME = 'risk_alert_WasteManifestResponse_warn_daily';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_IT = 'Et� dell�edificio (anni)' WHERE METRIC_NAME = 'spac_BuildingAge_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET COLLECT_FORMULA = 'bean:spac_Status_an', METRIC_STATUS = 'A' WHERE METRIC_NAME = 'spac_Status_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Espace d��quipe - Capacit� d��quipe' WHERE METRIC_NAME = 'spac_Team_Capacity';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Espace d��quipe - Effectifs d��quipe' WHERE METRIC_NAME = 'spac_Team_Headcount';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Espace d��quipe - Ratio de postes r�el' WHERE METRIC_NAME = 'spac_Team_Ratio_Actual';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Espace d��quipe - Ratio de postes en pourcentage de cible' WHERE METRIC_NAME = 'spac_Team_Ratio_Percent_Target';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Espace d��quipe - Ratio de postes cible' WHERE METRIC_NAME = 'spac_Team_Ratio_Target';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET COLLECT_FORMULA = 'bean:spac_Use_an', METRIC_STATUS = 'A', METRIC_TITLE_ES = 'Uso', METRIC_TITLE_FR = 'Consommation' WHERE METRIC_NAME = 'spac_Use_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_FR = 'Variance d�inoccupation (%)' WHERE METRIC_NAME = 'spac_Vacancy_Variance_cng_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'spac_Vacancy_percent_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'spac_VacantArea_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'D�partements�: moins de 70�% d�occupation' WHERE METRIC_NAME = 'spac_alert_Occupancy_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Demandes d�espace�: approbation requise' WHERE METRIC_NAME = 'spac_alert_Requests_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_TITLE_FR = 'Etages�: avec 30�% d�inoccupation' WHERE METRIC_NAME = 'spac_alert_Vacancy_weekly';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'spac_bl_AreaInternalGross_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'spac_bl_AreaRentable_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'spac_bl_AreaUsable_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', METRIC_TITLE_CH = '����' WHERE METRIC_NAME = 'spac_criticality_an';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'work-Costs-DirectServices_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -0.7, REPORT_LIMIT_HIGH_CRIT = -1.1, REPORT_LIMIT_HIGH_WARN = -0.85, REPORT_LIMIT_TARGET = -0.7 WHERE METRIC_NAME = 'work_Costs-DirectServices_perRentableArea_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A' WHERE METRIC_NAME = 'work_Costs-IndirectServices_an_fy';
UPDATE AFM.AFM_METRIC_DEFINITIONS SET METRIC_STATUS = 'A', REPORT_BENCHMARK_VALUE = -0.7, REPORT_LIMIT_HIGH_CRIT = -0.95, REPORT_LIMIT_HIGH_WARN = -0.85, REPORT_LIMIT_TARGET = -0.7 WHERE METRIC_NAME = 'work_Costs-IndirectServicesperRentableArea_an_fy';

UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�activit� - Gestionnaire d�actifs' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�activit� - Gestionnaire d�actifs' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Patrimoine et finance' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner FF';
UPDATE AFM.AFM_PROCESSES SET  DISPLAY_ORDER = 150, TITLE_CH = '���� - IT����', TITLE_FR = 'Propri�taire du processus d�activit� - Responsable des actifs informatiques' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - IT����', TITLE_FR = 'Propri�taire du processus d�activit� - Responsable des actifs informatiques' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Inventaire d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestion des actifs d�entreprise', TITLE_NL = 'Beheer berijfsassets' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Enterprise';
UPDATE AFM.AFM_PROCESSES SET  TITLE_NL = 'Assetbeheer' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Lifecycle';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�activit� - Gestionnaire d�actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�activit� - Gestionnaire d�actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ��,��,&��', TITLE_ES = 'Propietario del proceso de negocio - Instalaciones, finanzas, y proyecto', TITLE_FR = 'Propri�taire du processus d�activit� - Patrimoine, finance, & projet' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner FFP';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ��,��,&��', TITLE_ES = 'Propietario del proceso de negocio - Instalaciones, finanzas, y proyecto', TITLE_FR = 'Propri�taire du processus d�activit� - Patrimoine, finance, & projet' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner FFP (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - IT����', TITLE_FR = 'Propri�taire du processus d�activit� - Responsable des actifs informatiques' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - IT����', TITLE_FR = 'Propri�taire du processus d�activit� - Responsable des actifs informatiques' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Inventaire d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestion d�Actifs' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Bucket FM 1 - Assets';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rer la disposition, les contrats de garantie et d�assurance de vos actifs d��quipement.', TITLE_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment';
UPDATE AFM.AFM_PROCESSES SET  TITLE_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Inventaire d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rer l�utilisation des logiciels et les responsabilit�s de gestion des licences.', SUMMARY_IT = 'Tracciare l�utilizzo del software e le responsabilit� sulle licenze.' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Software';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�activit� - Gestionnaire de patrimoine' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�activit� - Gestionnaire de patrimoine' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - IT����', TITLE_FR = 'Propri�taire du processus d�activit� - Responsable des actifs informatiques' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - IT����', TITLE_FR = 'Propri�taire du processus d�activit� - Responsable des actifs informatiques' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Examiner le r�sum� des travaux et l�analyse de la main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Work and Labor Summary';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper le stock d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Define Tools';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper les informations sur l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper les informations sur la main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Labor Information';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rer les contrats de niveau de service, les types de demandes, �', TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Centre d�appels' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Call Center BldgOpsConsole';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Create Work Request';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Responsable d�Inventaire' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Labor Analysis';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Mettre � Jour les Demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Update Work Requests';
UPDATE AFM.AFM_PROCESSES SET  TITLE_NL = 'Planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Define Schedules';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Gestion de patrimoine' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Facilities';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�n�rer des bons de travaux de MP d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�n�rer des bons de travaux de MP d�entretien' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Labor Analysis';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Maintenance' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Programmer la MP d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Programmer la MP d�entretien' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_IT = 'Definire i tipi di problema, i tipi riparazione e le descrizioni utilizzate in questa attivit�; definire l�elenco degli addetti manutenzione ai quali � possibile assegnare il lavoro.', TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rer votre stock d�outils, l�utilisation de pi�ces d�tach�es, les localisations des stocks de pi�ces d�tach�es et les niveaux de r�assort.', SUMMARY_IT = 'Gestire l�inventario strumenti, l�utilizzo pezzi nelle apparecchiature, le posizioni a magazzino dei pezzi e riordinare i livelli.', TITLE_FR = 'Responsable d�Inventaire' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Inventory Manager';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Soumettre les projets pour notification et approbation ou approuver les projets sp�cifiques relevant de votre p�rim�tre d�action. Comparer les projets propos�s au financement approuv� pour chaque ann�e.', SUMMARY_IT = 'Instradare i progetti per la notifica e l�approvazione o approvare i progetti specifici assegnati. Confrontare i progetti proposti con i fondi assegnati per ciascun anno.' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Approve';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Cr�ez un ou plusieurs budgets d�investissements pluri-annuels. Saisir directement les �l�ments des budgets ou les r�sumer � partir de la liste des projest ou des programmes. Actualiser les budgets en fonction des modifications r�centes apport�es aux projets.', SUMMARY_IT = 'Crea uno o pi� budget investimenti di capitale pluriannuali. Inserire direttamente le cifre budget o riepilogarle dall�elenco di progetti e programmi. Aggiornare i budget in base alle modifiche apportate di recente ai progetti.' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Budget';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Saisir les sources de financement. D�finir de nouveaux sc�nario de budget, planifier les co�ts de chaque sc�nario et examiner l�impact des diff�rentes options de financement.', SUMMARY_IT = 'Inserire le fonti di finanziamento. Definire nuovi scenari budget, i costi piano per ogni scenario ed esaminare l�impatto delle diverse alternative di finanziamento.', SUMMARY_NL = 'Financieringsbronnen invoeren. Definieer nieuwe budgettencenario�s, plan de kosten voor elk scenario en beoordeel de impact van verschillende financieringsalternatieven.' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Evaluate (Optional)';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_NL = 'Registreer projectprioriteiten. Schat basislijnschema�s en duur. Beoordeel goedgekeurde projectfinanciering per jaar.' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Prioritize & Estimate (Optional)';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Soumettre une nouvelle demande de projet. Consulter vos demandes en cours et leur statut d�approbation, ou ajouter des d�tails � une demande. Organiser vos demandes par budget d�investissement. Importer des informations dans la liste de t�ches du projet � partir des r�sultats d��valuation de l��tat du patrimoine.' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Request';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'D�finir la hi�rarchie g�ographique. D�finir les classifications de types de projets. D�finir les questionnaires pour collecter les informations des demandes standard. D�finir les mod�les de projets pour que les projets utilisent des listes de t�ches similaires. T�l�charger les mod�les de documents standard tels que les factures, les appels d�offres et les registres de sites journaliers pour garantir la coh�rence de la documentation des projets.' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Set Up';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Responsable d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Assessment Manager';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapport d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Assessment Report (Dashboard)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Demandes de travaux d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Assessments Work Requests (Dash)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Evaluation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Bucket REPM 2 - Cond Assess';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer les �valuations de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Scoreboard (Dashboard)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Assistance � l�application' WHERE ACTIVITY_ID = 'AbCommonResources' AND PROCESS_ID = 'Application Support';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�inventaire' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�inventaire' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Manage Inventory';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�inventaire' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Manage Inventory';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Configurer vos types d�action standard pour l��quipement informatique, la charpenterie et autres �l�ments d''action de d�m�nagement. Etablir les questionnaires standard qui doivent �tre joints � toute demande de d�m�nagement.', TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner les bons de d�m�nagements et les �l�ments d''action qui vous sont assign�s. Utiliser la feuille de calcul des d�m�nagements pour visualiser les bons de d�m�nagements associ�s. Mettre � jour les t�ches pour refl�ter l��tat d�avancement.', SUMMARY_IT = 'Esaminare l�ordine di spostamento e le voci azione assegnate. Utilizzare il foglio di calcolo spostamento per visualizzare gli ordini di spostamento correlati. Aggiornare i compiti per riflettere lo stato di completamento .' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Craftsperson';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner et ex�cuter les �l�ments d''action concernant l��quipement donn�es associ�s � chaque demande de d�m�nagement. Acc�der � la feuille de calcul des d�m�nagements pour compl�ter rapidement les donn�es de d�m�nagement de groupe.' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Data Coordinator';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '������', TITLE_NL = 'Verhuisscenario�s ontwikkelen' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Develop Move Scenarios';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_IT = 'Esaminare e stimare gli spostamenti, indirizzare gli spostamenti per l�approvazione, emettere e completare gli spostamenti. Utilizzare il foglio di calcolo spostamento per aggiornare velocemente i dettagli spostamento di gruppo.' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Coordinator';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Demander le d�m�nagement d�une personne, d�un groupe, d�un actif, d�une pi�ce ou d�un �quipement. Consulter le statut de vos demandes de d�m�nagement ou modifier ou red�finir les d�tails d�un d�m�nagementt de groupe.' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_CH = '������', SUMMARY_NL = 'Verhuisscenario�s ontwikkelen', TITLE_CH = '��������' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Scenario Planner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Demander un d�m�nagement d�employ�' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Request an Employee Move';
UPDATE AFM.AFM_PROCESSES SET  TITLE_IT = 'Percorso per l�approvazione' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Route for Approval';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner et ex�cuter les �l�ments d''action concernant l��quipement voix associ�s � chaque demande de d�m�nagement. Acc�der � la feuille de calcul des d�m�nagements pour compl�ter rapidement les donn�es de d�m�nagement de groupe.' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Voice Coordinator';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Soumettre les projets pour notification et approbation ou approuver les projets sp�cifiques relevant de votre p�rim�tre d�action. Comparer les projets propos�s au financement approuv� pour chaque ann�e.', SUMMARY_IT = 'Instradare i progetti per la notifica e l�approvazione o approvare i progetti specifici assegnati. Confrontare i progetti proposti con i fondi assegnati per ciascun anno.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Approve';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Les prestataires utilisent ce processus pour examiner les offres soumises, les contrats envoy�s, soumettre des ordres de modification, mettre � jour le statut d�une t�che, cr�er des factures et consulter l�historique de paiement de vos projets.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Contract';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rer, � l�aide de cette console, tous les aspects de la planification, de l�acquisition, de la communication, de la programmation, des modifications et des co�ts pour tous les sites, projets, actions et programmes. Utiliser des rapports d�analyse d�indicateurs de performance pour identifier les projets critiques n�cessitant l�attention la plus urgente.  Acc�der aux documents et aux contrats relatifs aux projets.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Execute';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner les indicateurs de performance des projets et les tableaux d��valuation  qui pond�rent le risque du programme et du budget. Examiner les �tapes d�cisives et les projets en retard par rapport au programme ou les projets hors budget. Utiliser le calendrier des projets pour contr�ler les d�pendances et relations dans le temps. Utiliser les cartes spatiales et organisationnelles pour examiner l�impact des projets sur les localisations, divisions, ou d�partements.', SUMMARY_IT = 'Esaminare  i dati metrici benchmark del progetto e gli scorecard che indicano un rischio sulla pianificazione e sul budget.  Esaminare le fasi intermedie e i progetti in ritardo oppure oltre budget.  Utilizzare il calendario progetti per controllare le dipendenze puntuali.  Utilizzare le mappe spazio e organizzative per esaminare l�impatto su posizioni, divisioni o reparti.', SUMMARY_NL = 'Bekijk de projectbenchmark en scorekaarten om risico�s af te wegen qua planning en budget.  Bekijk mijlpalen en projecten die achter zijn op het schema of over het budget.  Gebruik de projectenagenda voor het controleren van tijdsfactoren.  Gebruik de ruimtelijke en organisatieoverzichten om de impact op locaties, divisies of afdelingen na te gaan.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_NL = 'Registreer projectprioriteiten. Schat basislijnschema�s en duur. Beoordeel goedgekeurde projectfinanciering per jaar.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Prioritize & Estimate';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Soumettre une nouvelle demande de projet. Consulter vos demandes en cours et leur statut d�approbation, ou ajouter des d�tails � une demande. Organiser vos demandes par budget d�investissement. Importer des informations dans la liste de t�ches du projet � partir des r�sultats d��valuation de l��tat du patrimoine.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Request';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner l�historique des donn�es de projets, notamment les paiements des fournisseurs, les co�ts par activit� et lot de travaux, les variances par rapport aux programmes, la participation des fournisseurs aux projets et l�historique des performances des fournisseurs.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Review History';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'D�finir la hi�rarchie g�ographique. D�finir les classifications de types de projets. D�finir les questionnaires pour collecter les informations des demandes standard. D�finir les mod�les de projets pour que les projets utilisent des listes de t�ches similaires. T�l�charger les mod�les de documents standard tels que les factures, les appels d�offres et les registres de sites journaliers pour garantir la coh�rence de la documentation des projets.' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Set Up';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Administrateur d�imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Chargeback Administrator';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Co�ts d�Imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Chargeback Costs';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRPLMGovPropertyRegistry' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper l�inventaire de locaux' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Develop Suite Inventory';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper l�inventaire de locaux (CAD)' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Develop Suite Inventory (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer les co�ts d�imp�ts' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Saisir les informations de base concernant les propri�taires et les locataires, ainsi que les dates d��ch�ance importantes. G�rer les loyers de base et les options des locations.', SUMMARY_IT = 'Inserire le informazioni di sintesi, quali le informazioni su locatario e conduttore e le date importanti. Tenere traccia della locazione di base e delle informazioni sull�opzione.' WHERE ACTIVITY_ID = 'AbRealPropertyAndLease' AND PROCESS_ID = 'Lease Administrative Staff';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rer et consulter les tableaux de bord, options, loyers et dates d�expiration des locations. Suivre les dates d�expiration des options et des baaux.' WHERE ACTIVITY_ID = 'AbRealPropertyAndLease' AND PROCESS_ID = 'Lease Manager';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner les locaux inoccup�s et les contrats de location arrivant � expiration ainsi que leurs dispositions pour vous aider � ma�triser l�espace disponible � la location.' WHERE ACTIVITY_ID = 'AbRealPropertyAndLease' AND PROCESS_ID = 'Leasing Agent or Broker';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ouvrier Responsable de l�Elimination' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Abatement Worker';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Gestion de patrimoine' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Facilities';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - B�timent propre' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports du propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Business Process Owner Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�Activit�' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Manager Dash 0';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Manager Dash 2';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer mon activit� d��limination' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Worker Dash 0';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer mes �l�ments d��limination' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Worker Dash 1';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Conformit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'BPO - Compliance';

UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Gestion de patrimoine' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'BPO - Facilities';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports du propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Business Process Owner Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_DE = 'Diagramm �Abgelaufene Lizenz�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Expired License Chart Dash';
UPDATE AFM.AFM_PROCESSES SET  TITLE_NL = 'Overzicht programma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Programs Map Dash';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Mettre � jour l��tat de l��v�nement' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Update Event Status Dash';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Examen de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Employee Review';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Responsable d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Assessment Manager';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapport d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Assessment Report Dash';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Demandes de travaux d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Assessments Work Requests Dash';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Scoreboard Dash';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Ex�cuter, � la suite d�un accident, l��valuation des syst�mes, zones, pi�ces et �quipement pour d�terminer les conditions du reprise des activit�s.', SUMMARY_IT = 'Eseguire la valutazione successiva all�incidente sullo stato dei sistemi, delle zone, dei locali e delle apparecchiature per stabilire le condizioni di ripristino delle operazioni.', TITLE_FR = 'Equipe d��valuation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Assessment Team';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestion des situations d�urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Bucket Risk 2 - Emergency Prep';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'G�rez une biblioth�que de documents comprenant tous les documents concernant la gestion des situations d�urgence.' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Documentation';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Acc�der aux contacts en situations d�urgence pour le site. Localiser et enregistrer le statut du personnel qui vous est affect�. Consulter les donn�es d�occupation pour v�rifier les listes de personnel pour chaque portion du b�timent. Acc�der aux informations d�urgence enregistr�es pour chaque membre du personnel.', SUMMARY_IT = 'Accedere ai contatti di emergenza per la struttura. Individuare e registrare lo stato dei membri del personale assegnati. Esaminare le informazioni sull�occupazione verificando nuovamente gli elenchi dei membri del personale in ogni parte dell�edificio. Accedere alle informazioni sui contatti di emergenza dei dipendenti.', TITLE_FR = '�quipes d�intervention d�urgence et de GSU' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Acc�der aux plans d��vacuation et aux plans de localisation des mati�res dangereuses par b�timent et �tage. Utiliser la console pour examiner les zones de fum�e, alarme, s�curit� et d��clairage d�urgence.' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'First Responders';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner le bulletin d�informations � l�attention des responsables.  Examiner le statut des employ�s et les r�sum�s d��valuation de l��tat des sites.' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Managers';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner les bulletins d�information concernant les fermetures de sites, les relocalisations temporaires et les proc�dures d�urgence.' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Staff';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Comptabilit�' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'BPO - Accounting';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Gestion de patrimoine' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'BPO - Facilities';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Eau/Gaz/Elec.' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'BPO - Utilities';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Donn�es d�analyse de compteurs' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 4 - Meter Analytic';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestion de la consommation d��nergie' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Risk 2 - Energy Mgmt';
UPDATE AFM.AFM_PROCESSES SET  TITLE_IT = 'Responsabile dell�energia/Direttore delle strutture' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Entrer/�diter les valeurs de l�empreinte carbone du b�timent' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Enter Building Footprint Data';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit� - Gestion de patrimoine' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Business Process Owner - Facilit';
UPDATE AFM.AFM_PROCESSES SET  TITLE_CH = '���� - ����', TITLE_FR = 'Propri�taire du processus d�entreprise - Mati�res dangereuses' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Business Process Owner - Risk';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports du propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Business Process Owner Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�inventaire des mati�res' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Manage MSDSs';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Examiner l�inventaire des mati�res' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Review MSDSs';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Allouer l�espace futur' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Dessiner des diagrammes d�empilement des affectations' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Disposer l�espace futur' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Dessiner les am�nagements d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer les budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�historique d�utilisation de l�espace' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer les budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner l�affectation actuelle de l�espace de votre d�partement et l�ajuster en ajoutant on en lib�rant de l�espace.', SUMMARY_IT = 'Esaminare l�assegnazione dello spazio corrente del reparto e regolarlo richiedendo o rilasciando spazio.' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Department Manager';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Utiliser ce processus pour consulter les affectations de tous les d�partements et r�affecter l�espace entre eux.', TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Space Manager';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Chargeback Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Examiner les attributions' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Review Bookings';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbSpaceOccupancy' AND PROCESS_ID = 'Business Process Owner';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Localiser le personnel, identifier l�espace inoccup� et initier des d�m�nagements, des ajouts et des modifications.' WHERE ACTIVITY_ID = 'AbSpaceOccupancy' AND PROCESS_ID = 'Department Manager';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer le plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer le plan d�occupation (CAD)' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager Dash 3';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Planificateur de l�espace' WHERE ACTIVITY_ID = 'AbSpacePlanning' AND PROCESS_ID = 'Space Planner';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Planificateur de l�espace' WHERE ACTIVITY_ID = 'AbSpacePlanning' AND PROCESS_ID = 'Space Planner (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Analyse d�Imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Chargeback Dash';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Chargeback Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation (CAD)' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Chargeback Reports (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Imputation de l�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Imputation de l�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Imputation d�espace standard' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Imputation d�espace standard' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Imputation d�espace standard' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback1';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackCI' AND PROCESS_ID = 'Chargeback Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackCI' AND PROCESS_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Sp�cialiste CAD - Inventaire d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory(CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper l�inventaire sans CAD' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory w/o CAD';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�inventaire de pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'D�velopper l�inventaire sans CAD' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBC' AND PROCESS_ID = 'Develop Inventory w/o CAD';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�inventaire de pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBC' AND PROCESS_ID = 'Room Inventory Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�inventaire de pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Room Inventory Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�inventaire de pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Room Inventory Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR' AND PROCESS_ID = 'Chargeback Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation (CAD)' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR' AND PROCESS_ID = 'Chargeback Reports (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR' AND PROCESS_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI' AND PROCESS_ID = 'Chargeback Reports';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Rapports d�imputation (CAD)' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI' AND PROCESS_ID = 'Chargeback Reports (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI' AND PROCESS_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Administrateur ARCHIBUS - Configuration de l�application' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) -App';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Administrateur ARCHIBUS - Configuration de l�application' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Gestionnaire d�applications mobiles' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Mobile Apps Manager';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'D�finir les profils d�acc�s utilisateurs et les r�les de s�curit� ; d�finir les processus qui apparaissent lorsque chaque utilisateur d�marre une session.' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'System Administrator';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Dessiner l��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Dessiner l��quipement voix et donn�es (CAD)' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment (CAD)';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'G�rer l�inventaire de logiciels' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Enregistrer des visiteurs, g�rer leurs autorisations d�acc�s et les permis de parking et les diriger vers leur lieu de rendez-vous.' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Security';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Examiner l�affectation actuelle de l�espace de votre d�partement et l�ajuster en ajoutant on en lib�rant de l�espace.', SUMMARY_IT = 'Esaminare l�assegnazione dello spazio corrente del reparto e regolarlo richiedendo o rilasciando spazio.', TITLE_FR = 'Affectation de l�Espace' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Space Allocation';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Localiser le personnel, identifier l�espace inoccup� et initier des d�m�nagements, des ajouts et des modifications. Ajoutet ou mettre � jour les donn�es concernant le personnel et les prestataires.', TITLE_FR = 'Occupation de l�espace' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Space Occupancy';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Acc�der aux services aux employ�s, tels que les demandes d�intervention, les r�servations de salles de r�union, les autorisations visiteurs et le r�pertoire du personnel.', SUMMARY_IT = 'Accedere ai servizi generali per membri del personale, ad esempio le richieste di lavoro, le prenotazioni sale riunioni, le approvazioni permessi visitatori e l�elenco membri del personale.' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Staff';
UPDATE AFM.AFM_PROCESSES SET  SUMMARY_FR = 'Acc�der aux services r�serv�s aux visiteurs, par exemple la carte du campus ; localiser les salles de r�union ou l�employ� qui vous re�oit ; et consulter les informations relatives aux autorisations visiteurs et permis parking.' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Visitors';
UPDATE AFM.AFM_PROCESSES SET  TITLE_FR = 'Nouvelle r�servation d�un appel de conf�rence' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'New Conference Call Reservation';

UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'G�rer et assurer la maintenance des actifs de mobilier et d��quipeement au sein de votre organisation et faire le suivi de donn�es telles que valeur mon�taire, garanties, locations, assurances, amortissements, d�m�nagements, localisations, utilisation, �tat et propri�taires.', SUMMARY_IT = 'Gestire e mantenere in modo efficiente gli arredi e le apparecchiature dell�organizzazione e tenere traccia delle informazioni quali il valore monetario, le garanzie, le locazioni, le assicurazioni, il deprezzamento, gli spostamenti, l�utilizzo, le condizioni e la propriet�.', TITLE_NL = 'Assetbeheer' WHERE PRODUCT_ID = 'AbAsset';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_DE = 'Ideal f�r Geb�ude- und Wartungsmanager, die alle Arbeitsanforderungen verfolgen und �ber Instandhaltungskosten und Prognosen Bericht erstatten m�ssen, ist die Dom�ne �Instandhaltungsmanagement und Service Desk�. Dabei handelt es sich um eine webbasierte, praxisorientierte L�sung, die allen Beteiligten ohne Aufwand bereitgestellt werden kann. Wartungsmanager haben die M�glichkeit, ihre Workflows anzupassen um Arbeitsabl�ufe zu automatisieren, Fristen einzuhalten und Aufgaben zu organisieren. Geb�udemanager k�nnen zur�ckliegende Instandhaltungsarbeiten und deren Kosten analysieren um k�nftige Anforderungen zu prognostizieren.', SUMMARY_FR = 'Le domaine Gestion de Maintenance est l�outil tout d�sign� pour les responsables de b�timent et de maintenance charg�s de suivre les demandes d�intervention, de rendre compte des d�penses de maintenance et de fournir des pr�visions � leur hi�rarchie. Cette solution Web bas�e sur les op�rations est facile � d�ployer aupr�s de tous les intervenants.  Les responsables de maintenance peuvent adapter les workflows � leurs besoins afin d�automatiser les processus, de faire respecter les d�lais et d�organiser les interventions.  Les responsables de b�timent peuvent analyser l�historique et les co�ts des interventions sur le b�timent afin de pr�voir les besoins ult�rieurs.', SUMMARY_IT = 'Per i responsabili degli edifici e della manutenzione che devono registrare tutte le richieste di lavoro e riportare i costi e le previsioni di manutenzione all�azienda, il dominio Operazioni edificio � una soluzione basata sul Web e incentrata sulle operazioni di facile implementazione, appositamente ideata per coloro che si occupano di manutenzione.  I responsabili della manutenzione possono personalizzare i flussi di lavoro per automatizzare i processi e organizzare le operazioni.  I responsabili degli edifici possono analizzare la cronologia e i costi delle operazioni correlate agli edifici per creare previsioni sulle esigenze future.' WHERE PRODUCT_ID = 'AbBldgOps';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'Cr�er des panneaux de contr�le simples d�acc�s pour chaque r�le que peut occuper un membre du personnel.' WHERE PRODUCT_ID = 'AbDashboards';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'G�rer les modifications, la croissance et les �quipes de projets � l�aide de cette activit� collaborative. Piloter les d�m�nagements de groupes et individuels, les d�tails associ�s aux d�m�nagements d�actifs ou � l�arriv�e de nouveaux employ�s. G�rer les bons de d�m�nagements ainsi que les demandes provenant des �quipes de maintenance, informatique ou s�curit�.' WHERE PRODUCT_ID = 'AbMove';
UPDATE AFM.AFM_PRODUCTS SET TITLE_FR = 'Gestion des projets d�investissement' WHERE PRODUCT_ID = 'AbProject';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'Cr�er un inventaire �lectronique de vos propri�t�s et de vos locations, avec des donn�es d�taill�es concernant l�occupation, les taxes, les actions r�guli�rement requises, la conformit� aux r�glementations, les co�ts etc.' WHERE PRODUCT_ID = 'AbRPLM';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'G�rer les activit�s relatives au risque organisationnel et l�att�nuation des risques.', TITLE_FR = 'Gestion des risques et'||CHR(13)||CHR(10)
||'de l�environnement' WHERE PRODUCT_ID = 'AbRisk';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'D�velopper un plan complet pour une utilisation optimale de votre espace bas�e sur les besoins op�rationnels de votre organisation. Visualiser et planifier les besoins en espace, pr�voir, allouer et disposer l�espace disponible dans les b�timents en fonction de vos objectifs d�entreprise et enregistrer des clich�s d�historique d�utilisation de l�espace des b�timents � des fins d�analyse.', SUMMARY_IT = 'Sviluppare un piano completo per ottimizzare l�utilizzo dello spazio in base alle necessit� dell�organizzazione.  Visualizzare e pianificare i requisiti per l�utilizzo dello spazio, prevedere, assegnare e tracciare il layout dello spazio dell�edificio in base agli obiettivi aziendali e registrare una cronologia di istantanee dell�utilizzo dello spazio dell�edificio per eseguire un�analisi.' WHERE PRODUCT_ID = 'AbSMP';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'Cette suite d�applications est un outil pr�cieux pour les gestionnaires d�espace et les responsables de d�partement souhaitant optimiser l�utilisation de l�espace. Elle leur permet de suivre pr�cis�ment les espaces disponibles et indisponibles, et de conna�tre les responsabilit�s de chaque intervenant.  Ainsi dot�s de fonctions analytiques pr�cises leur permettant de prendre des d�cisions fond�es, ainsi que de fonctions d�imputation/facturation ayant un impact sur la mani�re dont les diff�rents intervenants g�rent l�espace, les gestionnaires d�espace parviennent g�n�ralement � r�duire l�utilisation d�espace de 5�% � 10�%. Ces optimisations entra�nent en outre une r�duction des co�ts indirects et une diminution de l�empreinte carbone dans les m�mes proportions.', SUMMARY_IT = 'Per i responsabili dello spazio e di reparto che hanno l�esigenza di ottimizzare l�utilizzo dello spazio, questa suite di applicazioni consente di registrare in modo accurato lo spazio, l�occupazione, i posti vacanti, nonch� le responsabilit� dei singoli gestori.  Attraverso l�analisi accurata che permette di prendere decisioni informate e attraverso le funzionalit� di riaddebito che permettono di cambiare la consapevolezza e il comportamento rispetto ai costi all�interno dell�azienda, i responsabili dello spazio possono generalmente ridurre l�utilizzo dello spazio del 5% - 10%. Queste ottimizzazioni riducono inoltre i costi indiretti e l�impronta di carbonio nella stessa proporzione.', TITLE_FR = 'Gestion & planification de l�espace' WHERE PRODUCT_ID = 'AbSpace';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'G�rer � distance l�utilisation des licences, cr�er ou modifier les r�les de s�curit�, ajouter ou supprimer des utilisateurs, assigner des processus m�tier aux utilisateurs et g�rer les listes d��l�ments personnalis�s par utilisateur.', SUMMARY_IT = 'Gestire l�utilizzo di licenze, creare o modificare i ruoli sicurezza, aggiungere o rimuovere utenti, assegnare processi aziendali agli utenti e gestire le voci dell�elenco preferiti degli utenti da qualsiasi posizione.' WHERE PRODUCT_ID = 'AbSystem';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_FR = 'D�velopper un inventaire �lectronique complet des �l�ments de t�l�communications, en enregistrant la mani�re dont ces'||CHR(13)||CHR(10)
||'�l�ments sont connect�s pour former un syst�me de c�blage et en assurant la maintenance de cet inventaire'||CHR(13)||CHR(10)
||'au moyen de r�parations, projets de mise � niveau et d�m�nagements d�employ�s. Le module T�l�com fournit une solution flexible pour g�rer �lectroniquement votre r�seau physique.' WHERE PRODUCT_ID = 'AbTelecom';
UPDATE AFM.AFM_PRODUCTS SET SUMMARY_DE = 'Service Desk- und Betriebsmanagern, die eine Vielzahl unterschiedlicher Serviceanforderungen verfolgen und gleichzeitig Dienstleister �berwachen und verwalten m�ssen, bietet die Dom�ne �Arbeitsplatz-Service� eine webbasierte L�sung, mit der Angestellte unternehmensweit selbstst�ndig Anfragen eingeben k�nnen, welche dann von Managern zentral �berwacht und kontrolliert werden. Durch Automatisierung von Arbeitsprozessen, Analysen, Service-�berwachung und Selbstbedienungszugriff k�nnen Manager die Service-Effizienz und Leistung erheblich verbessern.', SUMMARY_FR = 'Pour les responsables de centre de services ou d�op�rations devant suivre des demandes de service nombreuses et vari�es tout en assurant la gestion et le suivi des prestataires, le domaine Workplace Service est une solution Web tr�s pratique. Elle permet aux employ�s d�entrer des demandes en libre-service et aux responsables de surveiller et contr�ler ces services depuis un point central.  En automatisant les processus et en mettant en oeuvre les fonctions d�analyse, de surveillance des services et d�acc�s en libre-service, les responsables augmentent consid�rablement l�efficacit� et les performances des services.', SUMMARY_IT = 'Per i responsabili Service Desk o delle operazioni che hanno la necessit� di registrare una serie vasta e variegata di richieste di servizio durante la gestione e il monitoraggio dei fornitori di servizio, il dominio Servizi luogo di lavoro � una soluzione basata sul Web che consente ai dipendenti di tutta l�azienda di immettere richieste self-service e permette ai responsabili di monitorare e controllare centralmente i servizi.  Attraverso l�automazione dei processi, l�analisi, il monitoraggio dei servizi e l�accesso self-service, i responsabili sono in grado di aumentare notevolmente l�efficienza e le prestazioni dei servizi.' WHERE PRODUCT_ID = 'AbWorkplaceServices';

UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Console d��limination des actifs' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Asset Manager' AND TASK_ID = 'Asset Disposal Console';
UPDATE AFM.AFM_PTASKS SET  TASK_NL = 'Console uitrustingsystemen' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Asset Manager' AND TASK_ID = 'Asset Lifecycle Console';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Console d�enregistrement des actifs' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Asset Manager' AND TASK_ID = 'Asset Registration Console';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Asset Manager' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Arri�re-plan de l�actif' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Asset Background';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Affecter des membres d��quipe � un �quipement', TASK_IT = 'Assegna membri team all�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Assign Team Members to Equipment';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Equipment Systems';
UPDATE AFM.AFM_PTASKS SET  TASK_IT = 'Definizione spazi serviti dall�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Spaces Served by Equipment';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Team Properties';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les cat�gories de propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Team Property Categories';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Arri�re-plan des syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Equipment Systems Background';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Arri�re-plan de l�actif' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Asset Background';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner FF' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir la configuration des ports de l��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Define Equipment Port Configuration';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Standards d�espace de travail' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Workspace Standards';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir la configuration des ports de l��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Define Equipment Port Configuration';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Standards d�espace de travail' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Workspace Standards';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Surfaces de b�timents par type d�infrastructure' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Building Area by Facility Type';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Analyse des Pannes d�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Failure Analysis';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Historique : Maintenance d�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Maintenance History';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Indicateurs d��quipement par CSI' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Metrics by CSI';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Analyse de Remplacement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Replacement Analysis';
UPDATE AFM.AFM_PTASKS SET  TASK_DE = 'Geb�ude & R�ume markieren als Problemf�lle bez�glich: ... Gef�hrliche Abf�lle', TASK_ES = 'Resaltar edificios y espacios con problemas de: ...residuos peligrosos', TASK_FR = 'Surbrillance des b�timents & Pi�ces avec probl�mes de�: ... D�chets toxiques', TASK_IT = 'Evidenzia edifici e locali con problemi di: ... rifiuti pericolosi', TASK_NL = 'Gebouwen en ruimtes & markeren met problemen mbt: ... Gevaarlijke afvalstoffen' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Highlight Buildings & Rooms with issues of: � Hazardous Waste';
UPDATE AFM.AFM_PTASKS SET  TASK_FILE = 'ab-rplm-pfadmin-manage-land-by-location-masterview.axvw' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Manage Property by Location';
UPDATE AFM.AFM_PTASKS SET  TASK_FILE = 'ab-repm-pfadmin-struc-book-vs-market-by-chart.axvw' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Structure Book and Market Value by State';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement, les garanties et les contrats de maintenance' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment Warranties and Service Contracts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Etage' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�impact organisationnel des projets par b�timent' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Projects Org. Impact by Building';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Afficher et affecter l��quipement par location' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View and Assign Equipment by Lease';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Consulter et �diter les informations d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View and Edit Equipment Information';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demande d�intervention pour le m�me �quipement ou lieu' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Work Request for Same Equipment or Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'EQUIPMENT';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer l��quipement par type de propri�t�' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Edit Equipment by Property Type';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateEquipmentDepreciation', TASK_FR = 'Recalculer l�Amortissement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Recalculate Equipment Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Abschreibung f�r �Markierte M�bel� neu berechnen', TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateFurnitureDepreciation', TASK_FR = 'Recalculer l�Amortissement du Mobilier Cod�' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Recalculate Tagged Furniture Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les tableaux d�amortissements de l��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Equipment Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les entr�es au grand journal d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Equipment General Ledger Journal Entries';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Tableaux d�Amortissements du Mobilier' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Furniture Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'EQUIPMENT';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer l��quipement par type de propri�t�' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Edit Equipment by Property Type';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateEquipmentDepreciation', TASK_FR = 'Recalculer l�Amortissement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Recalculate Equipment Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Abschreibung f�r �Markierte M�bel� neu berechnen', TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateFurnitureDepreciation', TASK_FR = 'Recalculer l�Amortissement du Mobilier Cod�' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Recalculate Tagged Furniture Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les tableaux d�amortissements de l��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Equipment Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les entr�es au grand journal d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Equipment General Ledger Journal Entries';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Tableaux d�Amortissements du Mobilier' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Furniture Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Define Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�Equipement par Pi�ce' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Define Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Draw Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement par pi�ce' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Peupler des Pi�ces avec de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Populate Equipment to Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de fabrication d��quipements et de prises' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Publish Equipment and Jacks Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer l��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Set Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de standards de mobilier par�:' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Furniture (SC)' AND TASK_ID = 'View Furniture Standards Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de mobilier cod� par�:' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Furniture (SC)' AND TASK_ID = 'View Tagged Furniture Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Furniture Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de mobilier' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Furniture Inventory (SC-CAD)' AND TASK_ID = 'Publish Furniture Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de standards de mobilier par ...' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'Furniture Standards Inventory' AND TASK_ID = 'View Furniture Standards Inventory Counts by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d��limination des actifs' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Asset Disposal Console';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Console uitrustingsystemen' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Asset Lifecycle Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�enregistrement des actifs' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Asset Registration Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier les actifs d�entreprise' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - ERP Integration' AND TASK_ID = 'Reconcile Enterprise Assets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�m�nagements d�actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Lifecycle' AND TASK_ID = 'Asset Moves';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'Syst�mes d�actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Lifecycle' AND TASK_ID = 'Asset Systems';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Utilisation de l�espace' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Lifecycle' AND TASK_ID = 'Space Utilization';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Crit�res d��valuation' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Mobile' AND TASK_ID = 'Assessment Criteria';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Relev�s d�actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Bucket EAM - Mobile' AND TASK_ID = 'Asset Surveys';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Arri�re-plan de l�actif' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Asset Background';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter des membres d��quipe � un �quipement', TASK_IT = 'Assegna membri team all�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Assign Team Members to Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Equipment Systems';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Definizione spazi serviti dall�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Spaces Served by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Team Properties';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les cat�gories de propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Define Team Property Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Arri�re-plan des syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM' AND TASK_ID = 'Equipment Systems Background';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Arri�re-plan de l�actif' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Asset Background';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter des membres d��quipe � un �quipement', TASK_IT = 'Assegna membri team all�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Assign Team Members to Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Define Equipment Systems';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Definizione spazi serviti dall�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Define Spaces Served by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Define Team Properties';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les cat�gories de propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Define Team Property Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Arri�re-plan des syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner AM (SC)' AND TASK_ID = 'Equipment Systems Background';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner FFP' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner FFP' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner FFP (SC)' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner FFP (SC)' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la configuration des ports de l��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Define Equipment Port Configuration';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d�espace de travail' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Workspace Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la configuration des ports de l��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Define Equipment Port Configuration';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Surfaces de b�timents par type d�infrastructure' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Building Area by Facility Type';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse des Pannes d�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Failure Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique : Maintenance d�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Maintenance History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Indicateurs d��quipement par CSI' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Metrics by CSI';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse de Remplacement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Equipment Replacement Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Geb�ude & R�ume markieren als Problemf�lle bez�glich: ... Gef�hrliche Abf�lle', TASK_ES = 'Resaltar edificios y espacios con problemas de: ...residuos peligrosos', TASK_FR = 'Surbrillance des b�timents & Pi�ces avec probl�mes de�: ... D�chets toxiques', TASK_IT = 'Evidenzia edifici e locali con problemi di: ... rifiuti pericolosi', TASK_NL = 'Gebouwen en ruimtes & markeren met problemen mbt: ... Gevaarlijke afvalstoffen' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Highlight Buildings & Rooms with issues of: � Hazardous Waste';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-rplm-pfadmin-manage-land-by-location-masterview.axvw' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Manage Property by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-repm-pfadmin-struc-book-vs-market-by-chart.axvw' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Structure Book and Market Value by State';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement, les garanties et les contrats de maintenance' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment Warranties and Service Contracts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Etage' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�impact organisationnel des projets par b�timent' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View Projects Org. Impact by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher et affecter l��quipement par location' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View and Assign Equipment by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les informations d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View and Edit Equipment Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demande d�intervention pour le m�me �quipement ou lieu' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Console Reports' AND TASK_ID = 'Work Request for Same Equipment or Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d��limination des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Department Asset Manager' AND TASK_ID = 'Asset Disposal Console';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Console uitrustingsystemen' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Department Asset Manager' AND TASK_ID = 'Asset Lifecycle Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�enregistrement des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Department Asset Manager' AND TASK_ID = 'Asset Registration Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Department Asset Manager' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'EQUIPMENT';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer l��quipement par type de propri�t�' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Edit Equipment by Property Type';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateEquipmentDepreciation', TASK_FR = 'Recalculer l�Amortissement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Recalculate Equipment Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Abschreibung f�r �Markierte M�bel� neu berechnen', TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateFurnitureDepreciation', TASK_FR = 'Recalculer l�Amortissement du Mobilier Cod�' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Recalculate Tagged Furniture Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les tableaux d�amortissements de l��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Equipment Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les entr�es au grand journal d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Equipment General Ledger Journal Entries';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Tableaux d�Amortissements du Mobilier' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Furniture Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'EQUIPMENT';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer l��quipement par type de propri�t�' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Edit Equipment by Property Type';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateEquipmentDepreciation', TASK_FR = 'Recalculer l�Amortissement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Recalculate Equipment Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Abschreibung f�r �Markierte M�bel� neu berechnen', TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateFurnitureDepreciation', TASK_FR = 'Recalculer l�Amortissement du Mobilier Cod�' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Recalculate Tagged Furniture Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les tableaux d�amortissements de l��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Equipment Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les entr�es au grand journal d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Equipment General Ledger Journal Entries';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Tableaux d�Amortissements du Mobilier' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Furniture Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Define Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�Equipement par Pi�ce' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Define Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Draw Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement par pi�ce' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Peupler des Pi�ces avec de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Populate Equipment to Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de fabrication d��quipements et de prises' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Publish Equipment and Jacks Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer l��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Set Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d��limination des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Facilities Asset Manager' AND TASK_ID = 'Asset Disposal Console';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Console uitrustingsystemen' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Facilities Asset Manager' AND TASK_ID = 'Asset Lifecycle Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�enregistrement des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Facilities Asset Manager' AND TASK_ID = 'Asset Registration Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Facilities Asset Manager' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier les actifs d�entreprise' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Facilities Asset Manager' AND TASK_ID = 'Reconcile Enterprise Assets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d��limination des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Finance Asset Manager' AND TASK_ID = 'Asset Disposal Console';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Console uitrustingsystemen' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Finance Asset Manager' AND TASK_ID = 'Asset Lifecycle Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�enregistrement des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Finance Asset Manager' AND TASK_ID = 'Asset Registration Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Finance Asset Manager' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de standards de mobilier par�:' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Furniture (SC)' AND TASK_ID = 'View Furniture Standards Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de mobilier cod� par�:' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Furniture (SC)' AND TASK_ID = 'View Tagged Furniture Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Furniture Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de mobilier' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Furniture Inventory (SC-CAD)' AND TASK_ID = 'Publish Furniture Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de standards de mobilier par ...' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'Furniture Standards Inventory' AND TASK_ID = 'View Furniture Standards Inventory Counts by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d��limination des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Asset Disposal Console';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Console uitrustingsystemen' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Asset Lifecycle Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�enregistrement des actifs' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Asset Registration Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier les actifs d�entreprise' WHERE ACTIVITY_ID = 'AbAssetEAM' AND PROCESS_ID = 'IT Asset Manager' AND TASK_ID = 'Reconcile Enterprise Assets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Actifs par police d�assurance' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Bucket FM 1 - Assets' AND TASK_ID = 'Assets by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Afschrijvingsschema�s' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Bucket FM 1 - Assets' AND TASK_ID = 'Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Bucket FM 2 - Telecommunications' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan de l��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Bucket FM 2 - Telecommunications' AND TASK_ID = 'Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Catalogue de standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Bucket FM 2 - Telecommunications' AND TASK_ID = 'Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'EQUIPMENT';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer l��quipement par type de propri�t�' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Edit Equipment by Property Type';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateEquipmentDepreciation', TASK_FR = 'Recalculer l�Amortissement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Recalculate Equipment Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Abschreibung f�r �Markierte M�bel� neu berechnen', TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateFurnitureDepreciation', TASK_FR = 'Recalculer l�Amortissement du Mobilier Cod�' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'Recalculate Tagged Furniture Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les tableaux d�amortissements de l��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Equipment Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les entr�es au grand journal d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Equipment General Ledger Journal Entries';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Tableaux d�Amortissements du Mobilier' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation' AND TASK_ID = 'View Furniture Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Journaux d�Amortissements' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Define Depreciation Logs';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'EQUIPMENT';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer l��quipement par type de propri�t�' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Edit Equipment by Property Type';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateEquipmentDepreciation', TASK_FR = 'Recalculer l�Amortissement de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Recalculate Equipment Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Abschreibung f�r �Markierte M�bel� neu berechnen', TASK_FILE = 'ab-single-job.axvw?ruleId=AbCommonResources-DepreciationService-updateFurnitureDepreciation', TASK_FR = 'Recalculer l�Amortissement du Mobilier Cod�' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'Recalculate Tagged Furniture Depreciation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les tableaux d�amortissements de l��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Equipment Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les entr�es au grand journal d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Equipment General Ledger Journal Entries';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Tableaux d�Amortissements du Mobilier' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Depreciation (SC)' AND TASK_ID = 'View Furniture Depreciation Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement, les garanties et les contrats de maintenance' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment Warranties and Service Contracts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Etage' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les informations d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View and Edit Equipment Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter l��quipement par location' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View and Edit Equipment by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Define Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�Equipement par Pi�ce' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Define Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l�Equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Draw Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement par pi�ce' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment (SC)' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Peupler des Pi�ces avec de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Populate Equipment to Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de fabrication d��quipements et de prises' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Publish Equipment and Jacks Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise T�l�com et d��quipements' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Publish Equipment and Telecom Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer l��quipement' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Equipment Inventory (SC-CAD)' AND TASK_ID = 'Set Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de standards de mobilier par�:' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Furniture' AND TASK_ID = 'View Furniture Standards Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de mobilier cod� par�:' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Furniture' AND TASK_ID = 'View Tagged Furniture Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de standards de mobilier par�:' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Furniture (SC)' AND TASK_ID = 'View Furniture Standards Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les d�comptes d�inventaire de mobilier cod� par�:' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Furniture (SC)' AND TASK_ID = 'View Tagged Furniture Inventory Counts by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Furniture Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de mobilier' WHERE ACTIVITY_ID = 'AbAssetManagement' AND PROCESS_ID = 'Furniture Inventory (SC-CAD)' AND TASK_ID = 'Publish Furniture Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d�actifs' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Asset Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d�actifs' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'Asset Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter des membres d��quipe � un �quipement', TASK_IT = 'Assegna membri team all�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM' AND TASK_ID = 'Assign Team Members to Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM' AND TASK_ID = 'Define Equipment Systems';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Definizione spazi serviti dall�apparecchiatura' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM' AND TASK_ID = 'Define Spaces Served by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM' AND TASK_ID = 'Define Team Properties';

UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les cat�gories de propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM' AND TASK_ID = 'Define Team Property Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Arri�re-plan des syst�mes d��quipements' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner FM' AND TASK_ID = 'Equipment Systems Background';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la configuration des ports de l��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Define Equipment Port Configuration';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d�espace de travail' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT' AND TASK_ID = 'Workspace Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la configuration des ports de l��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Define Equipment Port Configuration';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d�espace de travail' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Business Process Owner IT (SC)' AND TASK_ID = 'Workspace Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Peupler des Pi�ces avec de l�Equipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Inventory (SC-CAD)' AND TASK_ID = 'Populate Equipment to Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise de fabrication d��quipements et de prises' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Inventory (SC-CAD)' AND TASK_ID = 'Publish Equipment and Jacks Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise T�l�com et d��quipements' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Inventory (SC-CAD)' AND TASK_ID = 'Publish Equipment and Telecom Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer l��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Inventory (SC-CAD)' AND TASK_ID = 'Set Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement, les garanties et les contrats de maintenance' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment Warranties and Service Contracts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Etage' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les informations d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View and Edit Equipment Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter l��quipement par location' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View and Edit Equipment by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'Define Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�Equipement par Pi�ce' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'Define Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l�Equipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'Draw Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des actifs d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'Equipment Assets Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Etage' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement par pi�ce' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter l��quipement par location' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View and Edit Equipment by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbAssetTAM' AND PROCESS_ID = 'Telecom Management (SC)' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le graphe des co�ts des demandes d�intervention ferm�es par' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Analyze Finances' AND TASK_ID = 'View Closed Work Requests Cost Chart by';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Bons de travaux de MP, demandes d�intervention et ressources' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'PM Work Orders, Requests and Resources';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Une unique demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'Single Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les demandes d�intervention ferm�es par' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'View Closed Work Requests By';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Bons de travaux et demandes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'Work Orders and Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Bons de travaux, demandes d�intervention et ressources' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'Work Orders, Requests and Resources';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Review Work History' AND TASK_ID = 'Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�analyse de la main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis' AND PROCESS_ID = 'Work and Labor Summary' AND TASK_ID = 'Review Labor Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Define Tools' AND TASK_ID = 'Define Tool Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Define Tools' AND TASK_ID = 'View Tool Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les pi�ces d�tach�es d��quipement par �quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information' AND TASK_ID = 'Define Equipment Parts by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information' AND TASK_ID = 'View Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les pi�ces d�tach�es d��quipement par �quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information' AND TASK_ID = 'View Equipment Parts by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Equipment Information' AND TASK_ID = 'View Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�inventaire de pi�ces d�tach�es' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND PROCESS_ID = 'Develop Parts Inventory' AND TASK_ID = 'Define Parts Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les demandes d�intervention non soumises' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND PROCESS_ID = 'Client' AND TASK_ID = 'Manage Un-submitted Service Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sultats de l��tude de satisfaction sur les demandes de service archiv�es' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND PROCESS_ID = 'Reports' AND TASK_ID = 'Satisfaction Survey Results of Archived Service Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Rapports d�analyse du Centre de services' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND PROCESS_ID = 'Service Contract Manager' AND TASK_ID = 'Service Desk Analysis Reports';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND PROCESS_ID = 'Service Desk Manager' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�historique des interventions' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND PROCESS_ID = 'Service Provider' AND TASK_ID = 'Review Service History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket FM 1 - Building Ops' AND TASK_ID = 'Labor Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Equipo' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket FM 2 - Building Ops' AND TASK_ID = 'Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Localisation des demandes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket FM 2 - Building Ops' AND TASK_ID = 'Location of Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Avanc�' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Advanced';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir d�autres types de ressource' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Define Other Resource Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Define Tool Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les �quipes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Background' AND TASK_ID = 'Define Work Teams';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention archiv�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Reports' AND TASK_ID = 'Archived Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�Intervention Ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Reports' AND TASK_ID = 'Open Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique d�Utilisation des Pi�ces D�tach�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Reports' AND TASK_ID = 'Part Usage History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique d�utilisation des outils' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Bucket QS - BldgOps Reports' AND TASK_ID = 'Tool Usage History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les �quipes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Work Teams';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut des demandes d�intervention ferm�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'View Closed Work Requests Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut des demandes d�intervention ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'View Open Work Requests Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter une unique demande d�intervention ouverte' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'View Single Open Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour heures de main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Craftsperson_WEB BldgOpsConsole' AND TASK_ID = 'Update Labor Hours';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Create Work Request' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Estimer des demandes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Estimate and Schedule Requests' AND TASK_ID = 'Estimate Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Programmer des demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Estimate and Schedule Requests' AND TASK_ID = 'Schedule Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les demandes d�intervention ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Estimate and Schedule Requests' AND TASK_ID = 'View Open Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter une unique demande d�intervention ouverte' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Estimate and Schedule Requests' AND TASK_ID = 'View Single Open Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les arri�r�s de demandes d�intervention par m�tier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Estimate and Schedule Requests' AND TASK_ID = 'View Work Requests Backlog by Trade';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter des demandes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Generate Work Orders' AND TASK_ID = 'Assign Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut des demandes d�intervention ferm�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Generate Work Orders' AND TASK_ID = 'View Closed Work Requests Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les demandes d�intervention ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Generate Work Orders' AND TASK_ID = 'View Open Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut des demandes d�intervention ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Generate Work Orders' AND TASK_ID = 'View Open Work Requests Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter une unique demande d�intervention ouverte' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Generate Work Orders' AND TASK_ID = 'View Single Open Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les pi�ces d�tach�es d��quipement par �quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL' AND TASK_ID = 'Define Equipment Parts by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir d�autres types de ressource' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL' AND TASK_ID = 'Define Other Resource Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL' AND TASK_ID = 'Define Tool Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Inventory Manager MPSL' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention archiv�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Archived Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Budgets et co�ts des demandes d�intervention termin�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Completed Work Request Budgets and Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Co�ts des demandes d�intervention termin�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Costs of Completed Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique : Maintenance d�Equipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Equipment Maintenance History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Labor  Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Labor Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�Intervention Ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Open Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention en retard' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Overdue Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Vieillissement demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Request Aging';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Budgets et co�ts demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Request Budgets and Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plannings et co�ts demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Request Schedules and Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Performance de l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Team Performance';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention archiv�es et ressources' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Archived Work Requests and Resources';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Main-d�oeuvre actuelle' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Current Labor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse des Pannes d�Equipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Failure Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Informations sur l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Pi�ces d�tach�es d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Parts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse de Remplacement de l�Equipement' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Replacement Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plannings d��quipement', TASK_NL = 'Productieschema�s' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Labor  Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Labor Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Labor Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Localisation des demandes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Location of Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Disponibilit� des types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Tool Types Availability';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique d�utilisation des outils' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Tool Usage History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demande d�intervention pour le m�me �quipement ou lieu' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Work Request for Same Equipment or Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Requestor' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut des demandes d�intervention ferm�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Requestor' AND TASK_ID = 'View Closed Work Requests Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut des demandes d�intervention ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Requestor' AND TASK_ID = 'View Open Work Requests Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter une unique demande d�intervention ouverte' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Requestor' AND TASK_ID = 'View Single Open Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Archiver les demandes d�intervention rejet�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Review and Approve Requests' AND TASK_ID = 'Archive Rejected Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Review and Approve Requests' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner les demandes d�intervention non assign�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Review and Approve Requests' AND TASK_ID = 'Review Unassigned Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les demandes d�intervention ferm�es' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Review and Approve Requests' AND TASK_ID = 'View Closed Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les demandes d�intervention ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Review and Approve Requests' AND TASK_ID = 'View Open Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter une unique demande d�intervention ouverte' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Review and Approve Requests' AND TASK_ID = 'View Single Open Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Service Desk Mgr BldgOpsConsole' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Rapport de performance de l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Supervisor_WEB BldgOpsConsole' AND TASK_ID = 'Work Team Performance Report';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour les Demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND PROCESS_ID = 'Update Work Requests' AND TASK_ID = 'Update Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'PO-planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Bucket FM 2 - PM' AND TASK_ID = 'Define PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Achterstallige PO-schema�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Bucket FM 2 - PM' AND TASK_ID = 'Overdue PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour heures de main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Craftsperson BldgOpsConsole' AND TASK_ID = 'Update Labor Hours';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'PO-planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Define Schedules' AND TASK_ID = 'Define PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Facilities' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Facilities' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Main d�Oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Forecast Work and Resources' AND TASK_ID = 'Labor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Main d�oeuvre, pi�ces d�tach�es et outils' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Forecast Work and Resources' AND TASK_ID = 'Labor, Parts, and Tools';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter des ressources aux bons de travaux de MP d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders' AND TASK_ID = 'Assign Resources to Equipment PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�n�rer des bons de travaux de MP d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders' AND TASK_ID = 'Generate Equipment PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Emettre et imprimer des bons de travaux de MP d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders' AND TASK_ID = 'Issue and Print Equipment PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher un unique bon de travaux d��quipement ouvert' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders' AND TASK_ID = 'Show Single Open Equipment Work Order';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter tous les bons de travaux de MP d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders' AND TASK_ID = 'View All Equipment PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les bons de travaux de MP d��quipement non �mis' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate EQ PM Work Orders' AND TASK_ID = 'View Unissued Equipment PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter des ressources aux bons de travaux de MP d�entretien' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders' AND TASK_ID = 'Assign Resources to Housekeeping PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�n�rer des bons de travaux de MP d�entretien' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders' AND TASK_ID = 'Generate Housekeeping PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Emettre et imprimer des bons de travaux de MP d�entretien' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders' AND TASK_ID = 'Issue and Print Housekeeping PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher un unique bon de travaux d�entretien ouvert' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders' AND TASK_ID = 'Show Single Open Housekeeping Work Order';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter tous les bons de travaux de MP d�entretien' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders' AND TASK_ID = 'View All Housekeeping PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les bons de travaux de MP d�entretien non �mis' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Generate House PM Work Orders' AND TASK_ID = 'View Unissued Housekeeping PM Work Orders';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les pi�ces d�tach�es d��quipement par �quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Define Equipment Parts by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir d�autres types de ressource' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Define Other Resource Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Define Tool Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les �quipes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Define Work Teams';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des syst�mes d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance MPSL' AND TASK_ID = 'Equipment Systems Console';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Datums van PO-planningsschema�s per PO-schema defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance Manager' AND TASK_ID = 'Define PM Schedule Dates by PM Schedule';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Groeperingen van PO-planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance Manager' AND TASK_ID = 'Define PM Schedule Groupings';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'PO-planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance Manager' AND TASK_ID = 'Define PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance Manager' AND TASK_ID = 'Define Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'PO-schema�s weergeven' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Maintenance Manager' AND TASK_ID = 'View PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention archiv�es' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Archived Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Budgets et co�ts des demandes d�intervention termin�es' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Completed Work Request Budgets and Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Co�ts des demandes d�intervention termin�es' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Costs of Completed Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique : Maintenance d�Equipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Equipment Maintenance History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Labor  Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Labor Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�Intervention Ouvertes' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Open Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention en retard' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Overdue Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Vieillissement demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Request Aging';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Budgets et co�ts demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Request Budgets and Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plannings et co�ts demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Request Schedules and Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Performance de l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Work Team Performance';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes d�intervention archiv�es et ressources' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Archived Work Requests and Resources';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Main-d�oeuvre actuelle' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Current Labor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse des Pannes d�Equipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Failure Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Informations sur l��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Pi�ces d�tach�es d��quipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Parts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse de Remplacement de l�Equipement' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Replacement Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plannings d��quipement', TASK_NL = 'Productieschema�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Equipment Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Labor  Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Labor Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Labor Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Localisation des demandes d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Location of Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Achterstallige PO-schema�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Overdue PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'PO-schema�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Disponibilit� des types d�outils' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Tool Types Availability';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Historique d�utilisation des outils' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Tool Usage History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demande d�intervention pour le m�me �quipement ou lieu' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Work Request for Same Equipment or Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les plannings de MP d��quipement', TASK_NL = 'PO-planningsschema�s voor uitrusting defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'Define Equipment PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Datums van PO-planningsschema�s per PO-schema defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'Define PM Schedule Dates by PM Schedule';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Groeperingen van PO-planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'Define PM Schedule Groupings';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'PO-planningsschema�s per uitrusting defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'Define PM Schedules by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plannings de MP d��quipement avec dates de planning', TASK_NL = 'Weergeven PO-schema�s voor uitrusting met geplande datums' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'View Equipment PM Schedules with Schedule Dates';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven groeperingen van PO-schema�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'View PM Schedule Groupings';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven PO-schema�s per uitrusting' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'View PM Schedules by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven PO-schema�s per primaire vakdiscipline' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'View PM Schedules by Primary Trade';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven PO-schema�s per schemagroep' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Equipment PM' AND TASK_ID = 'View PM Schedules by Schedule Groups';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les plannings de MP d�entretien', TASK_NL = 'PO-schema�s voor schoonmaakdienst defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'Define Housekeeping PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Datums van PO-planningsschema�s per PO-schema defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'Define PM Schedule Dates by PM Schedule';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Groeperingen van PO-planningsschema�s defini�ren' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'Define PM Schedule Groupings';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plannings de MP d�entretien avec dates de planning', TASK_NL = 'Weergeven PO-schema�s voor schoonmaakdienst met geplande datums' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'View Housekeeping PM Schedules with Schedule Dates';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven groeperingen van PO-schema�s' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'View PM Schedule Groupings';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven PO-schema�s per primaire vakdiscipline' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'View PM Schedules by Primary Trade';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven PO-schema�s per schemagroep' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Schedule Housekeeping PM' AND TASK_ID = 'View PM Schedules by Schedule Groups';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbBldgOpsPM' AND PROCESS_ID = 'Service Desk Mgr BldgOpsConsole' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Surbrillance des pi�ces avec demandes d�Intervention en cours' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'Highlight Rooms With Active Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner des Demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'Review Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour une Demande d�Intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'Update Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer le stock d�outils' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Inventory Manager' AND TASK_ID = 'Edit Inventory of Tools';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�utilisation de pi�ces d�tach�es pour l��quipement', TASK_IT = 'Visualizza uso pezzi nell�apparecchiatura' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Inventory Manager' AND TASK_ID = 'View Parts Usage in Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter l�inventaire d��quipement' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Inventory Manager' AND TASK_ID = 'View and Edit Equipment Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les standards d��quipement' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Inventory Manager' AND TASK_ID = 'View and Edit Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Requestor' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Approuver et �mettre des demandes d�intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Supervisor' AND TASK_ID = 'Approve and Issue Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Supervisor' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�historique de maintenance d�un �quipement' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Supervisor' AND TASK_ID = 'Review Maintenance History for Equipment Item';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Preventief onderhoudsschema�s per uitrusting nakijken' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Supervisor' AND TASK_ID = 'Review Preventive Maintenance Schedules by Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner des Demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Supervisor' AND TASK_ID = 'Review Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour les Demandes d�Intervention' WHERE ACTIVITY_ID = 'AbBuildingOperations' AND PROCESS_ID = 'Supervisor' AND TASK_ID = 'Update Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Instrada progetti per l�approvazione' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Approve' AND TASK_ID = 'Route Projects for Approval';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Fiche d��valuation des projets' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Bucket REPM 1 - Capital Planning' AND TASK_ID = 'Projects Scorecard';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s toevoegen en bewerken' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Budget' AND TASK_ID = 'Add and Edit Programs';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Typen programma�s defini�ren' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Budget' AND TASK_ID = 'Define Program Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�n�rer un Budget d�Investissements' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Budget' AND TASK_ID = 'Generate Capital Budget';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Scenario�s per fonds bekijken' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Evaluate (Optional)' AND TASK_ID = 'Review Scenarios by Fund';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyser les sc�narios de l�Indice de v�tust� physique', TASK_NL = 'Analyseren FCI-scenario�s (Facility Condition Index)' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Prioritize & Estimate (Optional)' AND TASK_ID = 'Analyze Facility Condition Index Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s toevoegen of bewerken' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Request' AND TASK_ID = 'Add or Edit Programs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Copier des objets d��valuation dans les projets' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Request' AND TASK_ID = 'Copy Assessment Items to Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes projets' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Request' AND TASK_ID = 'Edit My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter mes projets' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Request' AND TASK_ID = 'View My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Typen programma�s defini�ren' WHERE ACTIVITY_ID = 'AbCapitalBudgeting' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Program Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les objets d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Assessment Manager' AND TASK_ID = 'Manage Condition Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Statistiques sur le statut des demandes d�intervention par projet d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Assessments Work Requests (Dash)' AND TASK_ID = 'Work Requests Status Statistics by Assessment Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tableau d�evaluation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Bucket REPM 2 - Cond Assess' AND TASK_ID = 'Condition Assessment Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les objets d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Bucket REPM 2 - Cond Assess' AND TASK_ID = 'Manage Condition Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les questionnaires d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Assessment Questionnaires';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l��quipement' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Enter Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'View Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d��quipement' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'View Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer mes objets d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Field Assessor' AND TASK_ID = 'Manage My Condition Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les objets d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Assessments (Dashboard)' AND TASK_ID = 'Manage Condition Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention pour une d�ficience' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment' AND TASK_ID = 'Create Work Request for Deficiency';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Exporter des Objets d�Evaluation pour un Contr�leur' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment' AND TASK_ID = 'Export Items for Assessor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Importer le Fichier d�Exportation pour PDA' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment' AND TASK_ID = 'Import PDA Export File';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Aper�u du Fichier d�Exportation pour PDA' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment' AND TASK_ID = 'Preview PDA Export File';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour le statut � partir des demandes d�intervention' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment' AND TASK_ID = 'Update Status from Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le tableau d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Manage Condition Assessment' AND TASK_ID = 'View Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le tableau d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'View Conditions Assessments Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation de l��tat de l��quipement par classification de nivea' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'View Equipment Assessment Items by Class Level 3';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le budget des objets d��valuation non acceptables par priorit�' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'View Unacceptable Assessment Items Budget by Priority';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Evaluations d��quipement par classif. de niveau 3' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Management Reports (Web)' AND TASK_ID = 'Equipment Assessments by Classification Level 3';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Objets d��valuation non acceptables par priorit�' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Management Reports (Web)' AND TASK_ID = 'Unacceptable Assessment Items by Priority';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer mes objets d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'My Assessments (Dashboard)' AND TASK_ID = 'Manage My Condition Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation actifs par...' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Active Assessment Items by...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation par...' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Assessment Items by...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation avec un taux de v�tust� > 25' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Assessment Items with Ratings > 25';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les stats des projets d��valuation de l��tat du patrimoine par...' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Cond. Assessment Project Stats by...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les projets d��valuation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Conditions Assessment Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation de l��tat de l��quipement par ...' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Equipment Assessment Items by...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuations des pi�ces par...' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Room Assessment Items by...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les stats des demandes d�intervention par projet d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'View Work Request Stats by Assessment Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Surbrillance des b�timents avec des probl�mes de �' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports (Drawings)' AND TASK_ID = 'Highlight Buildings with Issues of';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Statistiques des projets d��valuation par localisation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Assessment Project Statistics by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Statistiques sur le statut des demandes d�intervention par projet d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Work Requests Status Statistics by Assessment Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sum� des demandes d�intervention par objets d��valuation en cours' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Work Requests Summary by Active Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les �valuations de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Perform Field Assessment' AND TASK_ID = 'Edit Condition Assessments';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �valuations de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Perform Field Assessment' AND TASK_ID = 'View Condition Assessments';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PROCESS_ID = 'Scoreboard (Dashboard)' AND TASK_ID = 'Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�tails d�am�nagement de pi�ce', TASK_IT = 'Dettagli tipologia d�uso del locale' WHERE ACTIVITY_ID = 'AbCommonResources' AND PROCESS_ID = 'Application Support' AND TASK_ID = 'Room Arrangement Details';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'Enter Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Entrer des localisations d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'Enter Equipment Locations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire d��quipement par ...' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'View Equipment  Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter la disposition de l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'View Equipment Disposition';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le livre d�inventaire de l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'View Equipment Inventory Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'View Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer le relev� d��quipement � l�inventaire' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Compare Equipment Survey to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les audits d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Edit Equipment Audits';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les audits d��quipement par relev�' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Edit Equipment Audits by Survey';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les audits d��quipement par relev� par pi�ce' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Edit Equipment Audits for Survey by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour l�inventaire � partir du relev� d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Update Inventory from Equipment Survey';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les audits et l�inventaire d��quipement par...' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'View Equipment Audits and Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�historique de disposition de l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'View Equipment Disposition History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'View Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer le relev� d��quipement � l�inventaire' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'Compare Equipment Survey to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les audits et l�inventaire d��quipement par...' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'View Equipment Audits and Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le livre d�inventaire de l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'View Equipment Inventory Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'View Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire d��quipement par ...' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Equipment  Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter la disposition de l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Equipment Disposition';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le livre d�inventaire de l��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Equipment Inventory Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de standards de mobilier par ...' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'View Furniture Standards Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ajouter un relev� de standards de mobilier � l�inventaire' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Add Furniture Std Survey Audit to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer le relev� de standards de mobilier � l�inventaire' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Compare Furniture Std Survey to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Remplacer l�inventaire par le relev� de standards de mobilier' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Replace Inventory with Furniture Std Survey';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les audits et l�inventaire de stds de mobilier par ...' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'View Furniture Stds Audits and Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer le relev� de standards de mobilier � l�inventaire' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'Compare Furniture Std Survey to Inventory';

UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les audits et l�inventaire de stds de mobilier par ...' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'View Furniture Stds Audits and Inventory by ...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�comptes d�inventaire de standards de mobilier par' WHERE ACTIVITY_ID = 'AbFEStandardsInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'Furniture Standards Inventory Counts by';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de mobilier cod�...' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Manage Inventory' AND TASK_ID = 'View Tagged Furniture Inventory...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer le relev� � l�inventaire' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Compare Survey to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour l�inventaire � partir du relev�' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'Update Inventory from Survey';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�historique de disposition' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'View Disposition History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les audits et l�inventaire de mobilier cod�...' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Manage Surveys' AND TASK_ID = 'View Tagged Furn. Audits and Inventory...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer le relev� � l�inventaire' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'Compare Survey to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les audits et l�inventaire de mobilier cod�...' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Perform Survey' AND TASK_ID = 'View Tagged Furn. Audits and Inventory...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de mobilier cod�...' WHERE ACTIVITY_ID = 'AbFETaggedFurnitureInv' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Tagged Furniture Inventory...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les questionnaires d�actions' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Action Questionnaires';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes actions' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Craftsperson' AND TASK_ID = 'Edit my Actions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les donn�es d�action' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Data Coordinator' AND TASK_ID = 'Edit Data Actions';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Instrada spostamenti di gruppo per l�approvazione' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Coordinator' AND TASK_ID = 'Route Group Moves for Approval';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Instrada spostamenti per l�approvazione' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Coordinator' AND TASK_ID = 'Route Moves for Approval';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Percorso per l�approvazione' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Coordinator' AND TASK_ID = 'Route for Approval';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes d�m�nagements de groupes' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Edit my Group Moves';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes d�m�nagements' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Edit my Moves';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner mes d�m�nagements de groupes' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Examine my Group Moves';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner mes d�m�nagements' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Examine my Moves';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demander un d�m�nagement pour un d�part d�employ�' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Request a Move for an Employee Leaving';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demander un d�m�nagement d�actif' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Request an Asset Move';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demander un d�m�nagement d�employ�' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Request an Employee Move';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demander un d�m�nagement d��quipement', TASK_IT = 'Richiedi lo spostamento di un�apparecchiatura' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Requestor' AND TASK_ID = 'Request an Equipment Move';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '������', TASK_NL = 'Verhuisscenario�s ontwikkelen' WHERE ACTIVITY_ID = 'AbMoveManagement' AND PROCESS_ID = 'Move Scenario Planner' AND TASK_ID = 'Develop Move Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivi du cycle de vie de l�actif' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Bucket REPM 2 - Commissioning' AND TASK_ID = 'Asset Life Cycle Tracking';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l��quipe de mise en service' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Bucket REPM 2 - Commissioning' AND TASK_ID = 'Assign Commissioning Team';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en service l��quipement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Bucket REPM 2 - Commissioning' AND TASK_ID = 'Commission Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes projets' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Bucket REPM 2 - Commissioning' AND TASK_ID = 'Edit My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les projets actifs en cours d�acquisition' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Bucket REPM 2 - Commissioning' AND TASK_ID = 'Manage Active Projects Pipeline';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l��quipe de mise en service' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Design' AND TASK_ID = 'Assign Commissioning Team';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Copier des objets d��valuation dans les projets' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Planning' AND TASK_ID = 'Copy Assessment Items to Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les exigences du propri�taire avec l�occupant du b�timent�:', TASK_IT = 'Definisci requisiti proprietario con occupante dell�edificio:' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Planning' AND TASK_ID = 'Define Owner''s Requirements with Building Occupant:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes projets' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Planning' AND TASK_ID = 'Edit My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter mes projets' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Planning' AND TASK_ID = 'View My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en service l��quipement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'Commission Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d��quipement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View Equipment Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le catalogue de standards d�equipement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View Equipment Standards Book';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�equipement par d�partement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View Equipment by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Etage' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View Equipment by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Police d�Assurance' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View Equipment by Insurance Policy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Equipement par Garantie' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View Equipment by Warranty';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les informations d��quipement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View and Edit Equipment Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter et �diter les polices d�assurance' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Cx Post Construction' AND TASK_ID = 'View and Edit Insurance Policies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivi du cycle de vie de l�actif' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Asset Life Cycle Tracking';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les projets actifs en cours d�acquisition' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Manage Active Projects Pipeline';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l��quipe de mise en service' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Process' AND TASK_ID = 'Assign Commissioning Team';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en service l��quipement' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Process' AND TASK_ID = 'Commission Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes projets' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Process' AND TASK_ID = 'Edit My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les questionnaires d��valuation' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Assessment Questionnaires';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Typen programma�s defini�ren' WHERE ACTIVITY_ID = 'AbProjCommissioning' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Program Types';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Instrada progetti per l�approvazione' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Approve' AND TASK_ID = 'Route Projects for Approval';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Fiche d��valuation des projets' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Bucket FM 2 - Expensed Projects' AND TASK_ID = 'Projects Scorecard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'En cours d�acquisition' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Bucket REPM 2 - Capital Projects' AND TASK_ID = 'Pipeline';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner mes factures et paiements' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Contract' AND TASK_ID = 'Review My Invoices and Payments';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut de la progression de l''engagement (CPS)' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'FCPM' AND TASK_ID = 'View Commitment Progress Status (CPS)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les projections TEC' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'FCPM' AND TASK_ID = 'View Work-In-Place (WIP) Projections';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse des projets' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor' AND TASK_ID = 'View Projects Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�impact organisationnel des projets par b�timent' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor' AND TASK_ID = 'View Projects Org. Impact by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�impact organisationnel des projets par espace' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor' AND TASK_ID = 'View Projects Org. Impact by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter la fiche d��valuation des projets' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor' AND TASK_ID = 'View Projects Scorecard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la courbe en S' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor' AND TASK_ID = 'View S-Curve Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter la fiche d��valuation des lots de travaux' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Monitor' AND TASK_ID = 'View Work Packages Scorecard';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s toevoegen of bewerken' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Request' AND TASK_ID = 'Add or Edit Programs';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s toevoegen of bewerken volgens type' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Request' AND TASK_ID = 'Add or Edit Programs by Type';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Copier des objets d��valuation dans les projets' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Request' AND TASK_ID = 'Copy Assessment Items to Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer mes projets' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Request' AND TASK_ID = 'Edit My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter mes projets' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Request' AND TASK_ID = 'View My Projects';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner les variances des programmes d�actions' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Review History' AND TASK_ID = 'Review Actions Schedule Variances';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner les Co�ts par Type d�Action' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Review History' AND TASK_ID = 'Review Costs by Action Type';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�historique des performances des fournisseurs' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Review History' AND TASK_ID = 'Review Vendor Performance History';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Typen programma�s defini�ren' WHERE ACTIVITY_ID = 'AbProjectManagement' AND PROCESS_ID = 'Set Up' AND TASK_ID = 'Define Program Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter la synth�se d�historique des co�ts' WHERE ACTIVITY_ID = 'AbRPLMBudgetPlan' AND PROCESS_ID = 'Financial Planning' AND TASK_ID = 'View Cost History Summary';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le graphe de synth�se d�historique des co�ts' WHERE ACTIVITY_ID = 'AbRPLMBudgetPlan' AND PROCESS_ID = 'Financial Planning' AND TASK_ID = 'View Cost History Summary Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse des co�ts des propri�t�s' WHERE ACTIVITY_ID = 'AbRPLMBudgetPlan' AND PROCESS_ID = 'Financial Planning' AND TASK_ID = 'View Property Cost Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbRPLMBuildingPerformance' AND PROCESS_ID = 'Draw Building Areas (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbRPLMBuildingPerformance' AND PROCESS_ID = 'Draw Building Areas (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbRPLMBuildingPerformance' AND PROCESS_ID = 'Examine Building Performance' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les accords d�imputation des locations' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Lease Chargeback Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les accords d�imputation des locations' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'View Lease Chargeback Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '... Unternehmenseinheit' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Chargeback Administrator' AND TASK_ID = '... Business Unit';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Assistant des co�ts d�imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Chargeback Administrator' AND TASK_ID = 'Chargeback Cost Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les accords d�imputation des locations' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Chargeback Administrator' AND TASK_ID = 'View Lease Chargeback Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Assistant des co�ts d�imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Chargeback Costs' AND TASK_ID = 'Chargeback Cost Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Approuver les Co�ts d�Imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Approve Chargeback Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les accords d�imputation des locations' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Define Lease Chargeback Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�n�rer des co�ts d�imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Generate Chargeback Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Liste des exceptions d�imputation' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'List Chargeback Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�imputation par' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'View Chargeback Costs by';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les accords d�imputation des locations' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'View Lease Chargeback Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'El�ments d�actions pour ...' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Approve Costs' AND TASK_ID = 'Action Items for...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�am�liorations locatives par location' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Approve Costs' AND TASK_ID = 'View Leasehold Improvement Costs by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�actions li�s aux imp�ts' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Action Types for Taxes';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les �l�ments d�action d�imp�ts' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Cost Administrator' AND TASK_ID = 'Manage Tax Action Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�imp�ts' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Cost Administrator' AND TASK_ID = 'View Tax Costs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�am�liorations locatives par location' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Define Lease Std Costs' AND TASK_ID = 'View Leasehold Improvement Costs by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Programmer les co�ts des �l�ments d�action pour...' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Unified Costs' AND TASK_ID = 'Schedule Action Item Costs for...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts approuv�s des �l�ments d�action pour...' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Unified Costs' AND TASK_ID = 'View Approved Action Item Costs for...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts programm�s des �l�ments d�action pour...' WHERE ACTIVITY_ID = 'AbRPLMCosts' AND PROCESS_ID = 'Unified Costs' AND TASK_ID = 'View Scheduled Action Item Costs for...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ajouter un bureau d�agence' WHERE ACTIVITY_ID = 'AbRPLMGovPropertyRegistry' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Add Agency Bureau';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Transactions par identifiant unique d�actif de propri�t�' WHERE ACTIVITY_ID = 'AbRPLMGovPropertyRegistry' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Transactions by Property Asset Unique Identifier';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Transactions par identifiant unique d�actif de propri�t�' WHERE ACTIVITY_ID = 'AbRPLMGovPropertyRegistry' AND PROCESS_ID = 'Portfolio Manager' AND TASK_ID = 'Transactions by Property Asset Unique Identifier';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbRPLMGroupSpaceAllocation' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Graphe de pr�vision d�effectif' WHERE ACTIVITY_ID = 'AbRPLMGroupSpaceAllocation' AND PROCESS_ID = 'Portfolio Forecasting' AND TASK_ID = 'Headcount Projection Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Graphe d�analyse des �carts d�espace' WHERE ACTIVITY_ID = 'AbRPLMGroupSpaceAllocation' AND PROCESS_ID = 'Portfolio Forecasting' AND TASK_ID = 'Space Gap Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Regio�s defini�ren' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Define Regions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les dates d�expiration des locations' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Lease Manager' AND TASK_ID = 'View Lease Expirations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les dates d�expiration des sous-locations' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Lease Manager' AND TASK_ID = 'View Sublease Expirations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les dates d�expiration des locations' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Lease Expirations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les dates d�expiration des sous-locations' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Sublease Expirations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�infrastructure' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Facility Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Locations par date d�expiration' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Bucket REPM 2 - Leases' AND TASK_ID = 'Leases by Expiration Date';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Options par date d�expiration' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Bucket REPM 2 - Leases' AND TASK_ID = 'Options by Expiration Date';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... par El�ment du Registre d�Activit�s par Projet' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Lease Portfolio' AND TASK_ID = '... by Activity Log Item by Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '....par El�ment du Registre d�Activit�s' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Lease Portfolio' AND TASK_ID = '....by Activity Log Item';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '-� Ausgaben f�r Grundmiete', TASK_ES = '-� Gastos de renta base' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '-... Base Rent Expenses';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '-� Bereitstellungskosten', TASK_ES = '-� Costes de compromiso' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '-... Commitment Costs';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '-� Einnahmen', TASK_ES = '-� Ingresos' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '-... Income';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '-� Nettoeink�nfte', TASK_ES = '-� Ingresos netos', TASK_FR = '-� Revenu net' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '-... Net Income';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Ausgaben f�r Grundmiete', TASK_ES = '� Gastos de renta base' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Base Rent Expenses';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Bereitstellungskosten', TASK_ES = '� Costes de compromiso' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Commitment Costs';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Abl�ufe', TASK_ES = '� Vencimientos' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Expirations';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Einnahmen', TASK_ES = '� Ingresos' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Income';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Grund und Boden', TASK_ES = '� Terreno' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Land';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '... �����' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Landlord Name';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Nettoeink�nfte', TASK_ES = '� Ingresos netos' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Net Income';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Sonstiges Objekt', TASK_ES = '� Estructura' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... Structure';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... par El�ment du Registre d�Activit�s' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... by Activity Log Item';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... par El�ment du Registre d�Activit�s par Projet' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = '... by Activity Log Item by Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Locations par date d�expiration' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = 'Leases by Expiration Date';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Options par date d�expiration' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = 'Options by Expiration Date';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les D�tails :' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Details:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Suite Analysis (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les Graphiques d�Entreprise' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND PROCESS_ID = 'Suite Analysis (SC-CAD)' AND TASK_ID = 'Publish Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�am�liorations locatives par location' WHERE ACTIVITY_ID = 'AbRPLMLeaseCosts' AND PROCESS_ID = 'Approve Lease Costs' AND TASK_ID = 'View Leasehold Improvement Costs by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�am�liorations locatives par location' WHERE ACTIVITY_ID = 'AbRPLMLeaseCosts' AND PROCESS_ID = 'Define Lease Std Costs' AND TASK_ID = 'View Leasehold Improvement Costs by Lease';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les dates d�exercice des options de locations' WHERE ACTIVITY_ID = 'AbRPLMLeaseOptions' AND PROCESS_ID = 'Lease Administrator' AND TASK_ID = 'View Lease Option Exercise Dates';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les dates d�exercice des options de locations' WHERE ACTIVITY_ID = 'AbRPLMLeaseOptions' AND PROCESS_ID = 'Lease Manager' AND TASK_ID = 'View Lease Option Exercise Dates';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�infrastructure' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Facility Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�analyse financi�re' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Bucket REPM 1 - Portfolio' AND TASK_ID = 'Financial Analysis Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Graphe de pr�vision d�effectif' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Bucket REPM 2 - Portfolio' AND TASK_ID = 'Headcount Projection Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Graphe d�analyse des �carts d�espace' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Bucket REPM 2 - Portfolio' AND TASK_ID = 'Space Gap Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Transactions par identifiant unique d�actif de propri�t�' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Bucket REPM 2 - Portfolio' AND TASK_ID = 'Transactions by Property Asset Unique Identifier';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Grund und Boden', TASK_ES = '� Terreno' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Land' AND TASK_ID = '... Land';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '� Sonstiges Objekt', TASK_ES = '� Estructura' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration' AND PROCESS_ID = 'Structures' AND TASK_ID = '... Structure';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Konsole �Raum- u. Portfolioplanung�', TASK_FR = 'Console de planification de l�espace et du portefeuille' WHERE ACTIVITY_ID = 'AbRPLMPortfolioForecasting' AND PROCESS_ID = 'Advanced Portfolio Forecasting' AND TASK_ID = 'Space & Portfolio Planning Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbRPLMPortfolioForecasting' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Regio�s defini�ren' WHERE ACTIVITY_ID = 'AbRPLMPropertyAbstracts' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Define Regions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le graphe de l�utilisation des propri�t�s' WHERE ACTIVITY_ID = 'AbRPLMPropertyAbstracts' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Properties by Use Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er des co�ts d�imp�ts par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Real Property Portfolio Manager' AND TASK_ID = 'Create Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les arri�r�s d�imp�ts' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Real Property Portfolio Manager' AND TASK_ID = 'View Overdue Taxes';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les paiements d�imp�ts n�cessitant une autorisation' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Real Property Portfolio Manager' AND TASK_ID = 'View Tax Payments Needing Authorization';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les arri�r�s d�imp�ts' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Overdue Taxes';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�imp�ts par' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Tax Costs by';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les paiements d�imp�ts n�cessitant une autorisation' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Tax Payments Needing Authorization';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er des co�ts d�imp�ts par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'Create Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les co�ts d�imp�ts r�currents par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'Define Recurring Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Programmer les co�ts d�imp�ts r�currents par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'Schedule Recurring Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consolider les co�ts d�imp�ts par propri�t�' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'Sum Tax Costs to Property';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisie des co�ts d�imp�ts' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'Tax Costs Entry';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour les taux d�imposition sur les propri�t�s' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'Update Property Tax Rates';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�imp�ts r�currents par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'View Recurring Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts programm�s d�imp�ts par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'View Scheduled Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les co�ts d�imp�ts par parcelle' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes' AND PROCESS_ID = 'Tax Costs' AND TASK_ID = 'View Tax Costs by Parcel';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-ac-edit-gd-dflt.axvw' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Accounts';
UPDATE AFM.AFM_PTASKS SET TASK_FILE = 'ab-finanal-activitype-edit.axvw', TASK_FR = 'D�finir les types d�actions' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�exercice fiscal' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Fiscal Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier les actifs d�entreprise�' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Reconcile Enterprise Assets�';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Kosten aggregieren und prognostizieren f�r...', TASK_ES = 'Agregar y prever costes para...', TASK_FR = 'Regrouper et pr�voir les co�ts de...', TASK_IT = 'Costi aggregati e previsti per...', TASK_NL = 'Kosten samenvoegen en voorspellen voor...' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = 'Aggregate and Forecast Costs for�';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Kostenassistent f�r...', TASK_ES = 'Asistente de coste para...', TASK_FR = 'Assistant de co�ts de ...', TASK_IT = 'Procedura guidata costi per...', TASK_NL = 'Kostenwizard voor ...' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = 'Cost Wizard for �';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�analyse financi�re' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = 'Financial Analysis Console';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '... Geb�ude', TASK_ES = '...Edificios', TASK_FR = '... B�timents', TASK_IT = '... edifici', TASK_NL = '... Gebouwen' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = '� Buildings';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '... Ger�te', TASK_ES = '...Equipo', TASK_FR = '... Equipement', TASK_IT = '... apparecchiatura', TASK_NL = '... Uitrusting' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = '� Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '... Projekte', TASK_ES = '...Proyectos', TASK_FR = '... Projets', TASK_IT = '... progetti', TASK_NL = '... Projecten' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = '� Projects';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '... Liegenschaften', TASK_ES = '...Propiedades', TASK_FR = '... Propri�t�s', TASK_IT = '... propriet�', TASK_NL = '... Vastgoedobjecten' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis' AND TASK_ID = '� Properties';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les groupes d�emplacements d�analyse' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis (SC)' AND TASK_ID = 'Define Analysis Location Groups';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les indicateurs d�analyse' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis (SC)' AND TASK_ID = 'Define Analysis Metrics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la matrice d�investissements et de d�penses' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis (SC)' AND TASK_ID = 'Define Capital and Expense Matrix';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les champs de la matrice d�investissements et de d�penses' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis (SC)' AND TASK_ID = 'Define Capital and Expense Matrix Fields';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis (SC)' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les param�tres d�application ASF' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PROCESS_ID = 'Financial Analysis (SC)' AND TASK_ID = 'Define SFA Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer mes El�ments d�Activit� d�Elimination' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Abatement Worker' AND TASK_ID = 'Manage My Abatement Activity Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer Mes el�ments d�elimination de mati�res dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Abatement Worker' AND TASK_ID = 'Manage My Hazard Abatement Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation par localisation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Abatement Worker' AND TASK_ID = 'View Assessment Items by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Analyse D�taill�e des Localisations' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Abatement Worker' AND TASK_ID = 'View Locations Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Motifs d�Elimination' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk' AND TASK_ID = 'Define Abatement Reasons';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Source d�Accr�ditation pour les Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk' AND TASK_ID = 'Define Hazard Accreditation Sources';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Types d�Accr�ditation pour les Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk' AND TASK_ID = 'Define Hazard Accreditation Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Types d�Actions relatives aux Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk' AND TASK_ID = 'Define Hazard Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les R�sultats de l�Elimination des Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk' AND TASK_ID = 'Define Hazard Responses';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��chantillon de mati�re' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'BPO - Risk' AND TASK_ID = 'Define Material Sample Composition';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��chantillon de mati�re' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Background Data - Risk' AND TASK_ID = 'Define Material Sample Composition';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer le �l�ment d��ctivit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Bucket Risk 2 - Clean Building' AND TASK_ID = 'Manage Project Activity Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sum� d�activit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Bucket Risk 2 - Clean Building' AND TASK_ID = 'Project Activity Summary';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Analyse D�taill�e des Localisations' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Bucket Risk 2 - Clean Building' AND TASK_ID = 'View Locations Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Motifs d�Elimination' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Abatement Reasons';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Sources d�Accr�ditation pour les Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Hazard Accreditation Sources';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Types d�Accr�ditation pour les Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Hazard Accreditation Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Types d�Actions relatives aux Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Hazard Action Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sultats de l�Elimination des Mati�res Dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Hazard Responses';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les r�sultats de tests d��chantillon de mati�res dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Define Hazards and Samples (SC)' AND TASK_ID = 'Define Hazard Sample Test Results';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Define Hazards and Samples SCCAD' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les Graphiques d�Entreprise' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Define Hazards and Samples SCCAD' AND TASK_ID = 'Publish Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer le �l�ment d��ctivit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Environmental Hazard Manager' AND TASK_ID = 'Manage Project Activity Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation par localisation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Environmental Hazard Manager' AND TASK_ID = 'View Assessment Items by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Analyse D�taill�e des Localisations' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Environmental Hazard Manager' AND TASK_ID = 'View Locations Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�evaluations de mati�res dangereuses par localisation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Hazard Assessment Count by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer Mes objets d�evaluation de mati�res dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Field Assessor' AND TASK_ID = 'Manage My Hazard Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer mes el�ments d�activit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Field Assessor' AND TASK_ID = 'Manage My Project Activity Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les objets d��valuation par localisation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Field Assessor' AND TASK_ID = 'View Assessment Items by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Analyse D�taill�e des Localisations' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Field Assessor' AND TASK_ID = 'View Locations Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Co�ts d�evaluation (analyse) de mati�res dangereuses par projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Hazard Assessment Costs by Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�evaluations de mati�res dangereuses par localisation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Hazard Assessment Count by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d��valuations de mati�res dangereuses par localisation/ann�e' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Hazard Assessment Count by Location/Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�Evaluations de Mati�res Dangereuses par Ann�e' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Hazard Assessment Count by Year';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tous les objets d�evaluation de mati�res dangereuses' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'All Hazard Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Objets d��valuation de mati�res dangereuses par localisation' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Hazard Assessment Items by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sum� d�activit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Project Activity Summary';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer le �l�ment d��ctivit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Process' AND TASK_ID = 'Manage Project Activity Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sum� d�activit�s de projet' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Process' AND TASK_ID = 'Project Activity Summary';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Analyse D�taill�e des Localisations' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding' AND PROCESS_ID = 'Process' AND TASK_ID = 'View Locations Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les cat�gories d�exigences' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'BPO - Compliance' AND TASK_ID = 'Define Requirement Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�infraction' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'BPO - Compliance' AND TASK_ID = 'Define Violation Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'BPO - Facilities' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'BPO - Facilities' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Overzicht van complianceprogramma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Bucket Risk 1 - Compliance' AND TASK_ID = 'Compliance Programs Map';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�accidents' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Bucket Risk 1 - Compliance' AND TASK_ID = 'Incidents Count';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Journal d�accidents' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Bucket Risk 1 - Compliance' AND TASK_ID = 'Incidents Log';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Bucket Risk 1 - Compliance' AND TASK_ID = 'Programs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer l�analyse d�taill�e de conformit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Bucket Risk 2 - Compliance Mgmt' AND TASK_ID = 'Manage Compliance Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cat�gories d�exigences' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Requirement Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Types d�infraction' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Business Process Owner Reports' AND TASK_ID = 'Violation Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer l�analyse d�taill�e de conformit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Compliance  Program Manager' AND TASK_ID = 'Manage Compliance Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Complianceprogramma�s beheren' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Compliance  Program Manager' AND TASK_ID = 'Manage Compliance Programs';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Status und Schlie�en von �Meine Ereignisse� aktualisieren' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Compliance Program Coordinator' AND TASK_ID = 'Update Status and Close My Events';
UPDATE AFM.AFM_PTASKS SET TASK_DE = '�Meine Ereignisse�-Kalender anzeigen' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Compliance Program Coordinator' AND TASK_ID = 'View My Events Calendar';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Overzicht van complianceprogramma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Compliance Programs Map';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�exigences de conformit� par�:' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Compliance Requirements Count by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�infractions � la conformit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Compliance Violations Count';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '-Niveau de conformit� et type d�exigence' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Management Reports' AND TASK_ID = '-Compliance Level and Requirement Type';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Overzicht van complianceprogramma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Compliance Programs Map';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�exigences de conformit� par�:' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Compliance Requirements Count by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�infractions � la conformit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Compliance Violations Count';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d��v�nements manqu�s ou en souffrance' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Management Reports' AND TASK_ID = 'Missed and Overdue Events Count';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Gebouwen/ruimtes met complianceprogramma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Buildings/Rooms with Compliance Programs';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Complianceprogramma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Compliance Programs';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Overzicht van complianceprogramma�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Operational Reports' AND TASK_ID = 'Compliance Programs Map';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer l�analyse d�taill�e de conformit�' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Process' AND TASK_ID = 'Manage Compliance Drill-down';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Complianceprogramma�s beheren' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Process' AND TASK_ID = 'Manage Compliance Programs';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s met actieve werkzaamheden' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Work Status Reports' AND TASK_ID = 'Programs with Active Work';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s met achterstallige PO-schema�s' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Work Status Reports' AND TASK_ID = 'Programs with Overdue PM Schedules';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Programma�s met achterstallig werk' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND PROCESS_ID = 'Work Status Reports' AND TASK_ID = 'Programs with Overdue Work';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre la formation de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Bucket Risk 2 - EHS' AND TASK_ID = 'Track Employee Training';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre les cat�gories de travail de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Bucket Risk 2 - EHS' AND TASK_ID = 'Track Employee Work Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Param�tres de l�Application' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Background Data' AND TASK_ID = 'Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Background Data' AND TASK_ID = 'Configure Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d�accident' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Background Data' AND TASK_ID = 'Define Incident Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les types d��quipement de protection individuelle (EPI)' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Background Data' AND TASK_ID = 'Define Personal Protective Equipment (PPE) Types';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Opleidingsprogramma�s defini�ren' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Background Data' AND TASK_ID = 'Define Training Programs';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Opleidingsprogramma�s' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Background Data' AND TASK_ID = 'Training Programs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Accidents de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Employee Review' AND TASK_ID = 'Employee Incidents';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Formation, types d�EPI et suivi m�dical de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Employee Review' AND TASK_ID = 'Employee Training, PPE Types and Medical Monitoring';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Restrictions de travail de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Employee Review' AND TASK_ID = 'Employee Work Restrictions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�accidents' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Management Reports' AND TASK_ID = 'Incidents Count';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Journal d�accidents' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Management Reports' AND TASK_ID = 'Incidents Log';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Renouvellement des types d�EPI' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Management Reports' AND TASK_ID = 'PPE Types Renewal';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivi m�dical de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Employee Medical Monitoring';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Types d�EPI de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Employee PPE Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Donn�es sur la formation de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Employee Training Details';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Formation, types d�EPI et suivi m�dical de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Employee Training, PPE Types and Medical Monitoring';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Restrictions de travail de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Employee Work Restrictions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demandes de service sur l�accident' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Incident Service Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Formation, types d�EPI et suivi m�dical par cat�gorie de travail' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Training, PPE Types and Medical Monitoring by Work Category';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Restrictions de travail par standard d�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Operational Reports' AND TASK_ID = 'Work Restrictions by Employee Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le suivi m�dical de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Track' AND TASK_ID = 'Track Employee Medical Monitoring';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre les types d��quipement de protection individuelle de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Track' AND TASK_ID = 'Track Employee Personal Protective Equipment Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre le t�moin de l�accident' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Track' AND TASK_ID = 'Track Incident Witness';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre les cat�gories de travail de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'EHS - Work Categories' AND TASK_ID = 'Track Employee Work Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nombre d�accidents' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Incidents Count';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Renouvellement des types d�EPI' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'PPE Types Renewal';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Facilities - Background Data' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Facilities - Background Data' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Facilities - Background Data' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre l��quipement de protection individuelle de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Process' AND TASK_ID = 'Track Employee Personal Protective Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre la formation de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Process' AND TASK_ID = 'Track Employee Training';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre les cat�gories de travail de l�employ�' WHERE ACTIVITY_ID = 'AbRiskEHS' AND PROCESS_ID = 'Process' AND TASK_ID = 'Track Employee Work Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les objets d��valuation environnementale' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Assessment Manager' AND TASK_ID = 'Manage Sustainability Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Statistiques sur le statut des demandes d�intervention par projet d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Assessments Work Requests Dash' AND TASK_ID = 'Work Requests Status Statistics by Assessment Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les objets d��valuation environnementale' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Bucket Risk 2 - Env Sustain' AND TASK_ID = 'Manage Sustainability Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les questionnaires d��valuation environnementale' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Assessment Environment Questionnaires';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les questionnaires d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Assessment Questionnaires';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer mes objets d��valuation environnementale' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Field Assessor' AND TASK_ID = 'Manage My Sustainability Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les objets d��valuation environnementale' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Manage Assessments Dash' AND TASK_ID = 'Manage Sustainability Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Evaluations d��quipement par classif. de niveau 3' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Management Reports (Web)' AND TASK_ID = 'Equipment Assessments by Classification Level 3';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Objets d��valuation non acceptables par priorit�' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Management Reports (Web)' AND TASK_ID = 'Unacceptable Assessment Items by Priority';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer mes objets d��valuation environnementale' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'My Assessments Dash' AND TASK_ID = 'Manage My Sustainability Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Statistiques des projets d��valuation par localisation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Assessment Project Statistics by Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Probl�mes de consommation d��nergie non r�solus' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Open Energy Usage Issues';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Statistiques sur le statut des demandes d�intervention par projet d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Work Requests Status Statistics by Assessment Project';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sum� des demandes d�intervention par objets d��valuation en cours' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Operational Reports (Web)' AND TASK_ID = 'Work Requests Summary by Active Assessment Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbRiskES' AND PROCESS_ID = 'Scoreboard Dash' AND TASK_ID = 'Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour un Bulletin d�Information' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Bucket Risk 2 - Emergency Prep' AND TASK_ID = 'Update Advisory Bulletin';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les contacts en situations d�urgence par b�timents' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Bucket Risk 2 - Emergency Prep' AND TASK_ID = 'View Emergency Contacts by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les employ�s et leurs informations d�urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Bucket Risk 2 - Emergency Prep' AND TASK_ID = 'View Employees and their Emergency Information';

UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d�occupation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Bucket Risk 2 - Emergency Prep' AND TASK_ID = 'View Occupancy Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Contacts en Situations d�Urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Emergency Contacts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les informations d�urgence pour les employ�s' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Employee Emergency Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipe de GSU' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Recovery Team';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner des plans d��vacuation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Define Systems and Zones (SC)' AND TASK_ID = 'Draw Egress Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner des zones d��clairage de secours' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Define Systems and Zones (SC)' AND TASK_ID = 'Draw Emergency Lighting Zones';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Define Systems and Zones (SCCAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les Graphiques d�Entreprise' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Define Systems and Zones (SCCAD)' AND TASK_ID = 'Publish Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer des plans d��vacuation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Define Systems and Zones (SCCAD)' AND TASK_ID = 'Set Egress Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer les zones d��clairage de secours' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Define Systems and Zones (SCCAD)' AND TASK_ID = 'Set Emergency Lighting Zones';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en surbrillance la vue d�ensemble des dangers' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'Highlight Hazard Overview';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour un Bulletin d�Information' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'Update Advisory Bulletin';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d��vacuation et d�occupation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Egress and Occupancy Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les contacts en situations d�urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Emergency Contacts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les contacts en situations d�urgence par b�timents' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Emergency Contacts by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les employ�s et leurs informations d�urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Employees and their Emergency Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les employ�s et leurs informations d�urgence par etage' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Employees and their Emergency Information by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le statut de l��quipement' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Equipment Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�annuaire t�l�phonique de l��quipe de GSU' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Recovery Team Call List';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les contacts hi�rarchiques de l��quipe de GSU' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Emergency Response Team' AND TASK_ID = 'View Recovery Team Escalation Contacts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour un Bulletin d�Information' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Update Advisory Bulletin';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en surbrillance la vue d�ensemble des dangers' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'First Responders' AND TASK_ID = 'Highlight Hazard Overview';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d��vacuation et d�occupation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'First Responders' AND TASK_ID = 'View Egress and Occupancy Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner le bulletin d�information � l�attention des responsables' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Managers' AND TASK_ID = 'Review Advisory Bulletin for Managers';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour un Bulletin d�Information' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Managers' AND TASK_ID = 'Update Advisory Bulletin';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Equipe d��valuation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Assessment Team';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Propri�taire du processus d�activit�' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Business Process Owner';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Contacts en Situations d�Urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Define Emergency Contacts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Equipe d�intervention d�urgence et de GSU' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Emergency Response and Recovery Team';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en surbrillance la vue d�ensemble des dangers' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Highlight Hazard Overview';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre en surbrillance la vue d�ensemble des dangers' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Highlight Hazards Overview';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � Jour un Bulletin d�Information' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'Update Advisory Bulletin';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d��vacuation et d�occupation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'View Egress and Occupancy Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les contacts en situations d�urgence par b�timents' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'View Emergency Contacts by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les employ�s et leurs informations d�urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Process' AND TASK_ID = 'View Employees and their Emergency Information';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le bulletin d�information � l�attention des employ�s' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Staff' AND TASK_ID = 'View Advisory Bulletin for Employees';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d��vacuation et d�occupation' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness' AND PROCESS_ID = 'Staff' AND TASK_ID = 'View Egress and Occupancy Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'BPO - Facilities' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'BPO - Facilities' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '��������: �����' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'Compare Billed Usage: Current vs Previous';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Graphes de co�ts et d�utilisation' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'Cost and Usage Charts';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '��������' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'Cost per Area for Multiple Locations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Carte th�matique d��nergie' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'Thematic Energy Map';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Scenario�s voor geval dat' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'What-if Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Synth�se depuis le d�but de l�exercice' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'Year-to-Date Summary';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '��������' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 2 - Utility Metric' AND TASK_ID = 'Year-to-Year Cost Comparison';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Uso con modelo meteorol�gico' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Energy 3 - Weather Model' AND TASK_ID = 'Usage with Weather Model';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Uso con modelo meteorol�gico' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Bucket Risk 2 - Energy Mgmt' AND TASK_ID = 'Usage with Weather Model';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '��������: �����' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Compare Billed Usage: Current vs Previous';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '��������' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Cost per Area for Multiple Locations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Synth�se annuelle d��lectricit�' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Electric Annual Summary';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Donn�es d�analyse de compteurs' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Meter Analytics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Carte th�matique d��nergie' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Thematic Energy Map';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Uso con modelo meteorol�gico' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Usage with Weather Model';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Donn�es d�analyse E/G/E' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Utility Analytics';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Scenario�s voor geval dat' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'What-if Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Synth�se depuis le d�but de l�exercice' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Year-to-Date Summary';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '��������' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Energy Manager' AND TASK_ID = 'Year-to-Year Cost Comparison';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Donn�es d�Analyse de Portefeuille' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement' AND PROCESS_ID = 'Operations Manager' AND TASK_ID = 'Portfolio Analytics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Facteurs d��mission (non CO2)' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'BPO - Carbon Footprint Protocols' AND TASK_ID = 'Emission Factors (Non-CO2)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Facteurs d�oxydation' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'BPO - Carbon Footprint Protocols' AND TASK_ID = 'Oxidation Factors';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'BPO - Facilities' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'BPO - Facilities' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Tableau d��valuation' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Bucket Risk 1 - Sustainability' AND TASK_ID = 'Assessment Scoreboard';
UPDATE AFM.AFM_PTASKS SET TASK_ES = 'Uso con modelo meteorol�gico' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Bucket Risk 1 - Sustainability' AND TASK_ID = 'Usage with Weather Model';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Scenario�s voor geval dat' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Bucket Risk 1 - Sustainability' AND TASK_ID = 'What-if Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les sc�narios d�empreinte carbone', TASK_NL = 'Footprint-scenario�s defini�ren' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Bucket Risk 2 - Green Bldg - Car' AND TASK_ID = 'Define Footprint Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Entrer les donn�es de l�empreinte carbone du b�timent', TASK_IT = 'Immettere l�impronta di carbonio dell�edificio' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Bucket Risk 2 - Green Bldg - Car' AND TASK_ID = 'Enter Building Footprint Data';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le score du b�timent' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Bucket Risk 2 - Green Bldg - Sco' AND TASK_ID = 'View Building Score';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les sc�narios d�empreinte carbone', TASK_NL = 'Footprint-scenario�s defini�ren' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Process' AND TASK_ID = 'Define Footprint Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Entrer les donn�es de l�empreinte carbone du b�timent', TASK_IT = 'Immettere l�impronta di carbonio dell�edificio' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Process' AND TASK_ID = 'Enter Building Footprint Data';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les sc�narios d�empreinte carbone', TASK_NL = 'Footprint-scenario�s defini�ren' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Track Carbon Footprint' AND TASK_ID = 'Define Footprint Scenarios';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Entrer les donn�es de l�empreinte carbone du b�timent', TASK_IT = 'Immettere l�impronta di carbonio dell�edificio' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding' AND PROCESS_ID = 'Track Carbon Footprint' AND TASK_ID = 'Enter Building Footprint Data';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Assigner une mati�re � des pi�ces avec un plan d��tage' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Bucket Risk 2 - MSDS' AND TASK_ID = 'Assign Material to Rooms Using a Floor Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l��quipement' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Business Process Owner - Facilit' AND TASK_ID = 'Define Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d��quipement' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Business Process Owner - Facilit' AND TASK_ID = 'Define Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Assigner une mati�re � des pi�ces avec un plan d��tage' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Manage MSDSs' AND TASK_ID = 'Assign Material to Rooms Using a Floor Plan';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Veiligheidsinformatiebladen (VIB�s) defini�ren' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Manage MSDSs' AND TASK_ID = 'Define Materials and Safety Data Sheets (SDS)';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Veiligheidsinformatiebladen (VIB�s) defini�ren' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Process' AND TASK_ID = 'Define Materials and Safety Data Sheets (SDS)';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Veiligheidsinformatiebladen (VIB�s) ophalen' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Process' AND TASK_ID = 'Retrieve Safety Data Sheets (SDS)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Exceptions d�inventaire des mati�res' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Review MSDSs' AND TASK_ID = 'Material Inventory Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Veiligheidsinformatiebladen (VIB�s) ophalen' WHERE ACTIVITY_ID = 'AbRiskMSDS' AND PROCESS_ID = 'Review MSDSs' AND TASK_ID = 'Retrieve Safety Data Sheets (SDS)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND PROCESS_ID = 'Waste - Background Data' AND TASK_ID = 'Configure Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Zones d�Entreposage Satellite, de Stockage et de Citernes' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND PROCESS_ID = 'Waste - Background Data' AND TASK_ID = 'Define Satellite Accumulation, Storage and Tank Areas';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�sum� d�Entreposage des D�chets' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND PROCESS_ID = 'Waste - Review' AND TASK_ID = 'Waste Accumulation Summary';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Suivre l�Entreposage de D�chets' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND PROCESS_ID = 'Waste - Track' AND TASK_ID = 'Track Waste Accumulation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'Allocate Space';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner un diagramme d�empilement des affectations' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'Draw Allocation Stack Diagram';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Recalculer la surface d�affectation' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'Recalculate Allocation Area';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir le niveau d�affectation' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'Set Allocation Level';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le diagramme d�empilement des affectations' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'View Allocation Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les affectations des budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'View Space Budget Allocations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les affectations des budgets d�espace par �tage' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'View Space Budget Allocations by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space' AND TASK_ID = 'View Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner un diagramme d�empilement des affectations' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Allocate Future Space (CAD)' AND TASK_ID = 'Draw Allocation Stack Diagram';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Comparer l�inventaire � la simulation' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'Compare Inventory and Trial';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er un budget d�inventaire' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'Create Inventory Budget';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 1 ����' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'Create Trial 1 Room Layout';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 2 ����' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'Create Trial 2 Room Layout';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 3 ����' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'Create Trial 3 Room Layout';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 1 ����' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'View Trial 1 Room Layout';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 1 ��' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'View Trial 1 Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 2 ����' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'View Trial 2 Room Layout';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 2 ��' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'View Trial 2 Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 3 ����' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'View Trial 3 Room Layout';
UPDATE AFM.AFM_PTASKS SET TASK_CH = '���� 3 ��' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space' AND TASK_ID = 'View Trial 3 Rooms';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Copier l�inventaire dans la couche de simulation' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space (CAD)' AND TASK_ID = 'Copy Inventory to Trial Layer';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Copier la couche de simulation dans l�inventaire' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space (CAD)' AND TASK_ID = 'Copy Trial Layer to Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Effacer la couche d�inventaire' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space (CAD)' AND TASK_ID = 'Delete Inventory Layer';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Disposition de l�inventaire' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space (CAD)' AND TASK_ID = 'Inventory Layout';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Charger la disposition de l�inventaire de pi�ces' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Layout Future Space (CAD)' AND TASK_ID = 'Load Room Inventory Layout';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les �l�ments des budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budget Items';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les p�riodes des budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budget Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budgets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les standards de pr�visions d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Edit Space Forecast Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les p�riodes des budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Budget Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Budgets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSMPBackgroundData' AND PROCESS_ID = 'Develop Employee Information' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�employ�s' WHERE ACTIVITY_ID = 'AbSMPBackgroundData' AND PROCESS_ID = 'Develop Employee Information' AND TASK_ID = 'View Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er un historique � partir de l�inventaire' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History' AND TASK_ID = 'Create History from Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les budgets d�espace et les p�riodes' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History' AND TASK_ID = 'Define Space Budget & Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Budget d�espace' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History' AND TASK_ID = 'Space Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�historique d�utilisation de l�espace' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History' AND TASK_ID = 'View Historical Space Use';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse des tendances historiques' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History' AND TASK_ID = 'View Historical Trend Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Manage Space Usage History' AND TASK_ID = 'View Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse des tendances historiques' WHERE ACTIVITY_ID = 'AbSMPHistory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Historical Trend Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_NL = '---�Standaardenniveau' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Forecast Space Needs' AND TASK_ID = '---...Standards Level';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une pr�vision d�espace � partir de l�inventaire' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Forecast Space Needs' AND TASK_ID = 'Create Space Forecast From Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les budgets d�espace et les p�riodes' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Forecast Space Needs' AND TASK_ID = 'Define Space Budgets and Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Forecast Space Needs' AND TASK_ID = 'View Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les p�riodes des budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budget Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Define Space Budgets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les standards d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'Edit Space Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les p�riodes des budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Budget Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les budgets d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Budgets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Manage Space Budgets' AND TASK_ID = 'View Space Standards';
UPDATE AFM.AFM_PTASKS SET TASK_NL = '---�Standaardenniveau' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Program Space Requirements' AND TASK_ID = '---...Standards Level';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er un budget d�espace � partir de l�inventaire' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Program Space Requirements' AND TASK_ID = 'Create Space Budget From Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les budgets d�espace et les p�riodes' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Program Space Requirements' AND TASK_ID = 'Define Space Budgets and Periods';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les �l�ments des budgets d�espace par budget' WHERE ACTIVITY_ID = 'AbSMPProgramForecast' AND PROCESS_ID = 'Program Space Requirements' AND TASK_ID = 'View Space Budget Items by Budget';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demander ou lib�rer de l�espace pour votre d�partement' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Department Manager' AND TASK_ID = 'Claim or Release Space for Your Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�espace actuellement imput� � votre d�partement' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Department Manager' AND TASK_ID = 'Review Your Current Space Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�affecter l�Espace entre les D�partements' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Space Manager' AND TASK_ID = 'Reallocate Space Between Departments';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�Imputation d�Espace Actuelle' WHERE ACTIVITY_ID = 'AbSpaceAllocation' AND PROCESS_ID = 'Space Manager' AND TASK_ID = 'Review the Current Space Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbSpaceBldgPerformance' AND PROCESS_ID = 'Examine Building Performance' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�imputation selon BOMA' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View BOMA Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation selon BOMA' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform BOMA Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation selon BOMA96' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform BOMA96 Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation avanc�e selon BOMA' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform Enhanced BOMA Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation par groupes' WHERE ACTIVITY_ID = 'AbSpaceGroupChargeback' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform Group Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le graphe d�analyse d�partementale' WHERE ACTIVITY_ID = 'AbSpaceGroupInventory' AND PROCESS_ID = 'Group Inventory Reports' AND TASK_ID = 'Show Departmental Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceGroupInventory' AND PROCESS_ID = 'Group Inventory Reports' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceGroupInventory' AND PROCESS_ID = 'Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceGroupInventory' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceGroupInventory' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres d�approbation et de notification' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Configure Approval and Notification Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir le Responsable du D�partement charg� de l�Approbation' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Department Approving Manager';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards d�employ�s autoris�s par standard de pi�ces' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Employee Standards Allowed by Room Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les standards de pi�ces autoris�s par standard d�employ�' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Room Standards Allowed by Employee Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner les attributions' WHERE ACTIVITY_ID = 'AbSpaceHotelling' AND PROCESS_ID = 'Create and Review Bookings' AND TASK_ID = 'Review Bookings';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpaceOccupancy' AND PROCESS_ID = 'Business Process Owner' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�Espace Inoccup�' WHERE ACTIVITY_ID = 'AbSpaceOccupancy' AND PROCESS_ID = 'Department Manager' AND TASK_ID = 'Review Vacancies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpaceOrgLocEm' AND PROCESS_ID = 'Develop Employee Information' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�employ�s' WHERE ACTIVITY_ID = 'AbSpaceOrgLocEm' AND PROCESS_ID = 'Develop Employee Information' AND TASK_ID = 'View Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour les effectifs d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Update Employee Headcounts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'View Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour les effectifs d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'Update Employee Headcounts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter les standards d�employ�s au standard de pi�ce' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Assign Employee Standards to Room Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application spatiale' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Configure Space Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Employee Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'View Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter les standards d�employ�s au standard de pi�ce' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Assign Employee Standards to Room Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application spatiale' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Configure Space Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Employee Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Develop Inventory' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Mettre � jour les effectifs d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Develop Inventory' AND TASK_ID = 'Update Employee Headcounts';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Develop Inventory' AND TASK_ID = 'View Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er un plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan' AND TASK_ID = 'Create Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan' AND TASK_ID = 'View Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Effacer le plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan (CAD)' AND TASK_ID = 'Clear Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er un plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manage Occupancy Plan (CAD)' AND TASK_ID = 'Create Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les cat�gories de propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager' AND TASK_ID = 'Define Team Property Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�espace' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager' AND TASK_ID = 'Space Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�partementale des employ�s par�:' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager' AND TASK_ID = 'View Employee Departmental Analysis by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager' AND TASK_ID = 'View Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les cat�gories de propri�t�s d��quipe' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager (SC)' AND TASK_ID = 'Define Team Property Categories';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�espace' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager (SC)' AND TASK_ID = 'Space Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�partementale des employ�s par�:' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager (SC)' AND TASK_ID = 'View Employee Departmental Analysis by:';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager (SC)' AND TASK_ID = 'View Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le plan d�occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Manager Dash 3' AND TASK_ID = 'View Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Operational Reports RmTrans' AND TASK_ID = 'Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Operational Reports RmTrans (SC)' AND TASK_ID = 'Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�partementale des employ�s...' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Employee Departmental Analysis...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse des standards d�employ�s...' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Employee Standard Analysis...';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les standards d�employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Service Desk Manager RmTrans' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Service Desk Mgr RmTrans (SC)' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l�occupation et les attributs des pi�ces' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Assign Room Attributes and Occupancy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Define Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Employee Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�RER L�INVENTAIRE ET L�OCCUPATION' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'MANAGE INVENTORY & OCCUPANCY';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des transactions d�espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Workspace Transactions Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l�occupation et les attributs des pi�ces' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Assign Room Attributes and Occupancy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Define Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Employee Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�RER L�INVENTAIRE ET L�OCCUPATION' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'MANAGE INVENTORY & OCCUPANCY';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des transactions d�espaces de travail' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Workspace Transactions Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpacePlanning' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les types de plan d�espace standard' WHERE ACTIVITY_ID = 'AbSpacePlanning' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Edit Standard Space Plan Types';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Konsole �Raum- u. Portfolioplanung�', TASK_FR = 'Console de planification de l�espace et du portefeuille' WHERE ACTIVITY_ID = 'AbSpacePlanning' AND PROCESS_ID = 'Space Planner' AND TASK_ID = 'Space & Portfolio Planning Console';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Konsole �Raum- u. Portfolioplanung�', TASK_FR = 'Console de planification de l�espace et du portefeuille' WHERE ACTIVITY_ID = 'AbSpacePlanning' AND PROCESS_ID = 'Space Planner (SC)' AND TASK_ID = 'Space & Portfolio Planning Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Chargeback Dash' AND TASK_ID = 'View Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Visualizza dati riaddebito per reparto dell�utente corrente' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Department Manager' AND TASK_ID = 'View Department Shared Workplace Financial Statement';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Visualizza dati riaddebito per reparto dell�utente corrente' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Department Manager (SC)' AND TASK_ID = 'View Department Shared Workplace Financial Statement';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... Pour le b�timent et l��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback' AND TASK_ID = '... For Building and Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback' AND TASK_ID = 'View Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Utilisation G�n�rale par B�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback' AND TASK_ID = 'View Overall Utilization by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Utilisation G�n�rale par D�partement' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback' AND TASK_ID = 'View Overall Utilization by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Utilisation G�n�rale par Pi�ce' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback' AND TASK_ID = 'View Overall Utilization by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... Pour le b�timent et l��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback (SC)' AND TASK_ID = '... For Building and Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback (SC)' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback (SC)' AND TASK_ID = 'View Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Utilisation G�n�rale par B�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback (SC)' AND TASK_ID = 'View Overall Utilization by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Utilisation G�n�rale par D�partement' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback (SC)' AND TASK_ID = 'View Overall Utilization by Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�Utilisation G�n�rale par Pi�ce' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback (SC)' AND TASK_ID = 'View Overall Utilization by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback1' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback1' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Shared Workplace Chargeback1' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... Pour le b�timent et l��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans' AND TASK_ID = '... For Building and Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans' AND TASK_ID = 'Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... Pour le b�timent et l��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans (SC)' AND TASK_ID = '... For Building and Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans (SC)' AND TASK_ID = 'Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans (SC)' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans (SC)' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Space Chargeback RmTrans (SC)' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... Pour le b�timent et l��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback' AND TASK_ID = '... For Building and Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Rapports d�exception' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback' AND TASK_ID = 'EXCEPTION REPORTS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback' AND TASK_ID = 'View Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire m�lang� dans la surface au prorata' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback' AND TASK_ID = 'View Mixed Inventory in Prorate Area';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '... Pour le b�timent et l��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback (SC)' AND TASK_ID = '... For Building and Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Rapports d�exception' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback (SC)' AND TASK_ID = 'EXCEPTION REPORTS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback (SC)' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback (SC)' AND TASK_ID = 'View Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire m�lang� dans la surface au prorata' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback (SC)' AND TASK_ID = 'View Mixed Inventory in Prorate Area';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback1' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback1' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR' AND PROCESS_ID = 'Standard Space Chargeback1' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackCI' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackCI' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackCI' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les types de plan d�espace standard' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Background Data' AND TASK_ID = 'Edit Standard Space Plan Types';
UPDATE AFM.AFM_PTASKS SET TASK_DE = 'Unternehmenseinheiten definieren' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Background Data (SC)' AND TASK_ID = 'Define Business Units';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application spatiale' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Background Data RmTrans' AND TASK_ID = 'Configure Space Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application spatiale' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Background Data RmTrans (SC)' AND TASK_ID = 'Configure Space Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Basic All Room Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Basic All Room Inventory (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Conversieprogramma�s' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Basic All Room Inventory (CAD)' AND TASK_ID = 'CONVERSION TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Basic All Room Inventory (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Tous les dessins)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Basic All Room Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (All Drawings)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Ce dessin)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Basic All Room Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (This Drawing)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Gestion d�Actifs' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket Exec 2 - Facility' AND TASK_ID = 'Assets';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Analyse d�imputation d�taill�e' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket FM 1 - Space' AND TASK_ID = 'Detailed Chargeback Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Pr�vision d�effectif' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket FM 1 - Space' AND TASK_ID = 'Headcount Projection';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket FM 1 - Space' AND TASK_ID = 'Space Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l�occupation et les attributs des pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket FM 2 - Space' AND TASK_ID = 'Assign Room Attributes and Occupancy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Localisateur d�employ�s' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket FM 2 - Space' AND TASK_ID = 'Employee Locator';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket FM 2 - Space' AND TASK_ID = 'Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Standards d�Employ�s' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket QS - Space Background' AND TASK_ID = 'Define Employee Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket QS - Space Favorites' AND TASK_ID = 'Space Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Plan d�Occupation' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Bucket QS - Space Reports' AND TASK_ID = 'Occupancy Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET DISPLAY_ORDER = 50 WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance (SC)' AND TASK_ID = 'Define Locations';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance (SC)' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Conversieprogramma�s' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance (SC-CAD)' AND TASK_ID = 'CONVERSION TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les Graphiques d�Entreprise' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance (SC-CAD)' AND TASK_ID = 'Publish Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse de la performance des b�timents' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Building Performance (Win)' AND TASK_ID = 'View Building Performance Analysis';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory(CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Conversieprogramma�s' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory(CAD)' AND TASK_ID = 'CONVERSION TOOLS';

UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory(CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Tous les dessins)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory(CAD)' AND TASK_ID = 'Publish Enterprise Graphics (All Drawings)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Ce dessin)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Enhanced All Room Inventory(CAD)' AND TASK_ID = 'Publish Enterprise Graphics (This Drawing)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des transactions d�espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Executive Reports' AND TASK_ID = 'Workspace Transactions Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Aligner le plan d��tage sur le plan de base' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Extensions for ArcGIS (SC-CAD)' AND TASK_ID = 'Align Floor Plan to Basemap';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Joindre un plan d��tage' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Extensions for ArcGIS (SC-CAD)' AND TASK_ID = 'Attach Floor Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Naviguer jusqu�� l�emplacement du b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Extensions for ArcGIS (SC-CAD)' AND TASK_ID = 'Navigate to Building Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Group Inventory (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Group Inventory (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Group Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les Graphiques d�Entreprise' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Group Inventory (SC-CAD)' AND TASK_ID = 'Publish Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le graphe d�analyse d�partementale' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Group Inventory (Win)' AND TASK_ID = 'Show Departmental Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Group Inventory (Win)' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Conversieprogramma�s' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'CONVERSION TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Tous les dessins)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (All Drawings)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Ce dessin)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (This Drawing)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Inventory w/o CAD' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inv - Setup RmTrans (SC)' AND TASK_ID = 'Define Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inv - Setup RmTrans (SC)' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inv - Setup RmTrans (SC)' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory (SC-CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les Graphiques d�Entreprise' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory (SC-CAD)' AND TASK_ID = 'Publish Enterprise Graphics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory - Setup RmTrans' AND TASK_ID = 'Define Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory - Setup RmTrans' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory - Setup RmTrans' AND TASK_ID = 'View Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory - Setup RmTrans' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Room Inventory Reports' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Service Desk Manager RmTrans' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�terminer l�ordre des contrats de niveau de service' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Service Desk Mgr RmTrans (SC)' AND TASK_ID = 'Determine Ordering Sequence of Service Level Agreements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le graphe d�analyse d�partementale' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager' AND TASK_ID = 'Show Departmental Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager' AND TASK_ID = 'Space Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le graphe d�analyse d�partementale' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager (SC)' AND TASK_ID = 'Show Departmental Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager (SC)' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console d�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager (SC)' AND TASK_ID = 'Space Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le graphe d�analyse d�partementale' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager Dash 2' AND TASK_ID = 'Show Departmental Analysis Chart';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager Dash 2' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l�occupation et les attributs des pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Assign Room Attributes and Occupancy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Define Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer l�inventaire' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'MANAGE INVENTORY';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des transactions d�espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans' AND TASK_ID = 'Workspace Transactions Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Affecter l�occupation et les attributs des pi�ces' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Assign Room Attributes and Occupancy';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Define Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer l�inventaire' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'MANAGE INVENTORY';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�concilier l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Reconcile Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ecarts avec la modification d�espace de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Workspace Transaction Exceptions';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Console des transactions d�espaces de travail' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR' AND PROCESS_ID = 'Space Manager RmTrans (SC)' AND TASK_ID = 'Workspace Transactions Console';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBC' AND PROCESS_ID = 'Develop Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBC' AND PROCESS_ID = 'Develop Inventory w/o CAD' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBC' AND PROCESS_ID = 'Room Inventory Reports' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Tous les dessins)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (All Drawings)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Ce dessin)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (This Drawing)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR' AND PROCESS_ID = 'Room Inventory Reports' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Develop Inventory' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'OUTILS D�ACTIFS' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Develop Inventory (CAD)' AND TASK_ID = 'ASSET TOOLS';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Annuler la surbrillance' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Develop Inventory (CAD)' AND TASK_ID = 'Clear Highlight';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Tous les dessins)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Develop Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (All Drawings)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les graphiques d�entreprise (Ce dessin)' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Develop Inventory (CAD)' AND TASK_ID = 'Publish Enterprise Graphics (This Drawing)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Afficher le diagramme d�empilement des d�partements' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC' AND PROCESS_ID = 'Room Inventory Reports' AND TASK_ID = 'Show Departmental Stack Plan';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�analyse d�taill�e par b�timent et site' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI' AND PROCESS_ID = 'Chargeback Reports' AND TASK_ID = 'View Detailed Analysis by Building and Site';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI' AND PROCESS_ID = 'Perform Chargeback' AND TASK_ID = 'Perform Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ajouter ou �diter des r�les d�utilisateurs' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) - US' AND TASK_ID = 'Add or Edit User Roles';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le num�ro de r�vision de la base de donn�es' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) - US' AND TASK_ID = 'View Database Revision Number';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les informations sur les logiciels et l�utilisation des licences' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) - US' AND TASK_ID = 'View Program Information and License Usage';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) -App' AND TASK_ID = 'Configure Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Valuta�s defini�ren' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) -App' AND TASK_ID = 'Define Currencies';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'BTW en meerdere valuta�s in-/uitschakelen' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) -App' AND TASK_ID = 'Enable/Disable VAT and Multicurrency';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Meerdere valuta�s' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) -App' AND TASK_ID = 'Multi-Currency';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Utilitaires', TASK_NL = 'Hulpprogramma�s' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator (SC) -App' AND TASK_ID = 'Utilities';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps' AND TASK_ID = 'Configure Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Valuta�s defini�ren' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps' AND TASK_ID = 'Define Currencies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Activer ou d�sactiver l�historique des espaces de travail' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps' AND TASK_ID = 'Enable or Disable Workspace Transactions';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'BTW en meerdere valuta�s in-/uitschakelen' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps' AND TASK_ID = 'Enable/Disable VAT and Multicurrency';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Meerdere valuta�s' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps' AND TASK_ID = 'Multi-Currency';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Utilitaires', TASK_NL = 'Hulpprogramma�s' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - Apps' AND TASK_ID = 'Utilities';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ajouter ou �diter des r�les d�utilisateurs' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - UserSec' AND TASK_ID = 'Add or Edit User Roles';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter le num�ro de r�vision de la base de donn�es' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - UserSec' AND TASK_ID = 'View Database Revision Number';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les informations sur les logiciels et l�utilisation des licences' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'ARCHIBUS Administrator - UserSec' AND TASK_ID = 'View Program Information and License Usage';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�n�rateur de licences d�applications compl�mentaires' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Add-in Application License Generator';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Indicateurs d�analyse' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Analysis Metrics';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les indicateurs d�analyse' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Define Analysis Metrics';
UPDATE AFM.AFM_PTASKS SET  TASK_FR = 'G�rer les pages d�accueil et les processus' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Manage Home Pages and Processes';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Publier les pages d�accueil par r�le' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Publish Home Pages by Role';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�assistant de r�gle de base' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Run Basic Rule Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�assistant de d�finition de tableau de bord' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Run Dashboard Definition Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�Assistant de mise � jour de base de donn�es' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Run Database Update Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�Assistant de d�finition de vue de rapport uniquement' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Run Report Only View Definition Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�Assistant de modification de sch�ma' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Run Schema Change Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�Assistant de d�finition de vue' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'Run View Definition Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les Affectations d�Applications aux Domaines' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager' AND TASK_ID = 'View Application Assignments to Domains';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les Affectations d�Application aux Domaines' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager (SC)' AND TASK_ID = 'Edit Application Assignments to Domains';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�Assistant de mise � jour de base de donn�es' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager (SC)' AND TASK_ID = 'Run Database Update Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Ex�cuter l�Assistant de modification de sch�ma' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Add-In Manager (SC)' AND TASK_ID = 'Run Schema Change Wizard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la visibilit� du texte d�actif' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'CAD and BIM Manager' AND TASK_ID = 'Set Asset Text Visibility';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les Champs de Texte d�Actif' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'CAD and BIM Manager (SC)' AND TASK_ID = 'Edit Asset Text Fields';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer les Types d�Actifs' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'CAD and BIM Manager (SC)' AND TASK_ID = 'Edit Asset Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir la visibilit� du texte d�actif' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'CAD and BIM Manager (SC)' AND TASK_ID = 'Set Asset Text Visibility';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Editer la table d��num�ration de langues (lang_enum)' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Localization' AND TASK_ID = 'Edit Language Enum Table (lang_enum)';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Exporter ou importer les fichiers d�extraction de localisation' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Localization' AND TASK_ID = 'Export or Import Localization Extract Files';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres d�application mobile' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Mobile Apps Manager' AND TASK_ID = 'Configure Application Parameters for Mobile';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'G�rer les types d�activit�' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Mobile Apps Manager' AND TASK_ID = 'Manage Activity Types';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Relev�s d�espace mobiles en attente' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Mobile Apps Manager' AND TASK_ID = 'Pending Mobile Occupancy Surveys';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Effectuer le suivi de l�enregistrement des p�riph�riques mobiles' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'Mobile Apps Manager' AND TASK_ID = 'Track Mobile Devices Registration';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les registres des connecteurs' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'System Administrator' AND TASK_ID = 'View Connector Logs';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les informations sur les logiciels et l�utilisation des licences' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND PROCESS_ID = 'System Administrator' AND TASK_ID = 'View Program Information and License Usage';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '-Standards d��quipement donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = '-Data Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '-Standards d��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = '-Voice & Data Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = '-Standards d��quipement voix' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = '-Voice Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d��quipement donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Data Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Voice & Data Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Standards d��quipement voix' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Voice Equipment Standards';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l��quipement donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment' AND TASK_ID = 'Draw Data Equiment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment' AND TASK_ID = 'Draw Voice & Data Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l��quipement voix' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment' AND TASK_ID = 'Draw Voice Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment' AND TASK_ID = 'View Voice & Data Equipment Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l��quipement donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment (CAD)' AND TASK_ID = 'Draw Data Equiment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment (CAD)' AND TASK_ID = 'Draw Voice & Data Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Dessiner l��quipement voix' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment (CAD)' AND TASK_ID = 'Draw Voice Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les plans d��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Draw Work Area Equipment (CAD)' AND TASK_ID = 'View Voice & Data Equipment Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l��quipement donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'Enter Data Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir les p�riph�riques d��quipement donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'Enter Data Equipment Peripherals';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l��quipement voix et donn�es' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'Enter Voice & Data Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l��quipement voix et donn�es par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'Enter Voice & Data Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l��quipement voix' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'Enter Voice Equipment';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement donn�es et les p�riph�riques par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'View Data Equipment & Peripherals by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement donn�es par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'View Data Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement voix et donn�es par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'View Voice & Data Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement voix par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Manage Work Area Eq' AND TASK_ID = 'View Voice Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement donn�es et les p�riph�riques par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Data Equipment & Peripherals by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement donn�es par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Data Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement voix et donn�es par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Voice & Data Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l��quipement voix par pi�ce' WHERE ACTIVITY_ID = 'AbTelecomBasicIT' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Voice Equipment by Room';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven aansluitingschema�s' WHERE ACTIVITY_ID = 'AbTelecomHorizontalCabling' AND PROCESS_ID = 'Draw Jacks and Faceplates' AND TASK_ID = 'View Jack Plans';
UPDATE AFM.AFM_PTASKS SET TASK_NL = 'Weergeven aansluitingschema�s' WHERE ACTIVITY_ID = 'AbTelecomHorizontalCabling' AND PROCESS_ID = 'Draw Jacks and Faceplates (CAD)' AND TASK_ID = 'View Jack Plans';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l�inventaire de logiciels des PCs de bureau' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'Enter Desktop Software Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l�inventaire de logiciels des PCs de bureau par �tage' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'Enter Desktop Software Inventory by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l�inventaire de logiciels serveurs' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'Enter Server Software Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Saisir l�inventaire de logiciels serveurs par �tage' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'Enter Server Software Inventory by Floor';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels des PCs de bureau' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'View Desktop Software Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels serveurs' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'View Server Software Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels par �quipement et localisation' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'View Software Inventory by Equipment & Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels par standard' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Manage Software Inventory' AND TASK_ID = 'View Software Inventory by Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels des PCs de bureau' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Desktop Software Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels serveurs' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Server Software Inventory';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels par �quipement et localisation' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Software Inventory by Equipment & Location';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�inventaire de logiciels par standard' WHERE ACTIVITY_ID = 'AbTelecomSoftware' AND PROCESS_ID = 'Reports' AND TASK_ID = 'View Software Inventory by Standard';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter les visiteurs autoris�s pour aujourd�hui' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Security' AND TASK_ID = 'View Visitors Authorized for Today';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Demander ou lib�rer de l�espace pour votre d�partement' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Space Allocation' AND TASK_ID = 'Claim or Release Space for Your Department';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner votre Imputation d�Espace' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Space Allocation' AND TASK_ID = 'Review Your Space Chargeback';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner l�Espace Inoccup�' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Space Occupancy' AND TASK_ID = 'Review Vacancies';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une demande d�intervention' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Staff' AND TASK_ID = 'Create Work Request';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Examiner le statut des demandes d�intervention' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Staff' AND TASK_ID = 'Review Work Request Status';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Consulter l�historique des r�visions des mod�les de documents' WHERE ACTIVITY_ID = 'AbWorkplacePortal' AND PROCESS_ID = 'Staff' AND TASK_ID = 'View Document Templates Revision History';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Definizione destinazioni d�uso di un locale' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Bucket FM 2 - Reservations' AND TASK_ID = 'Define Room Arrangements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Types d�Am�nagement de Pi�ce', TASK_IT = 'Definizione tipi di destinazione d�uso di un locale' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Define Room Arrangement Types';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Definizione destinazioni d�uso di un locale' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Develop Background Data' AND TASK_ID = 'Define Room Arrangements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Nouvelle r�servation d�un appel de conf�rence' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'New Conference Call Reservation' AND TASK_ID = 'New Conference Call Reservation';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�servations - Demandes d�Intervention de Corps de M�tier' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Reports' AND TASK_ID = 'Reservations - Trades Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'R�servations - Demandes d�Intervention de Fournisseurs' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Reports' AND TASK_ID = 'Reservations - Vendor Work Requests';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Configurer les param�tres de l�application' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Reservation Manager' AND TASK_ID = 'Configure Application Parameters';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'D�finir les Types d�Am�nagement de Pi�ce', TASK_IT = 'Definizione tipi di destinazione d�uso di un locale' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Reservation Manager' AND TASK_ID = 'Define Room Arrangement Types';
UPDATE AFM.AFM_PTASKS SET TASK_IT = 'Definizione destinazioni d�uso di un locale' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Reservation Manager' AND TASK_ID = 'Define Room Arrangements';
UPDATE AFM.AFM_PTASKS SET TASK_FR = 'Cr�er une nouvelle r�servation d�appel de conf�rence' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND PROCESS_ID = 'Reservations' AND TASK_ID = 'Create New Conference Call Reservation';

UPDATE AFM.AFM_SCMPREF SET AFM_DB_VERSION_NUM = 143, DATE_LANG_CH = NULL, DATE_LANG_DE = NULL, DATE_LANG_EN = '10/20/2017 00:00:00', DATE_LANG_ES = NULL, DATE_LANG_FR = NULL, DATE_LANG_IT = NULL, DATE_LANG_NL = NULL WHERE AFM_SCMPREF = 0;

UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Standard d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...'
 AND SUBTASK = '... Equipment Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Pi�ces d�tach�es d��quipement par �quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Standard d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...'
 AND SUBTASK = '... Equipment Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Pi�ces d�tach�es d��quipement par �quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Pi�ces d�tach�es d��quipement par �quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Pi�ces d�tach�es d��quipement par �quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Analyse des Pannes d�Equipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis'
 AND SUBTASK = 'Equipment Failure Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Historique : Maintenance d�Equipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis'
 AND SUBTASK = 'Equipment Maintenance History';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Analyse de Remplacement de l�Equipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Analysis'
 AND SUBTASK = 'Equipment Replacement Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Fiche d�Assemblage d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Parts'
 AND SUBTASK = 'Equipment Bill of Materials';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Formulaire de saisie de donn�es d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Physical Inventory'
 AND SUBTASK = 'Equipment Data Collection Form';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Etiquetas de equipos', SUBTASK_FR = 'Etiquettes pour l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Physical Inventory'
 AND SUBTASK = 'Equipment Labels';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Planning de l��quipement de production' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Schedules'
 AND SUBTASK = 'Equipment Production Schedule';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Planning des travaux d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Schedules'
 AND SUBTASK = 'Equipment Work Schedule';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Analyse : Co�ts bas�s sur l�activit�' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Reports' AND TASK = 'Inventory Analysis'
 AND SUBTASK = 'Activity Based Costing';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...'
 AND SUBTASK = '... Labor';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Main d�oeuvre, pi�ces d�tach�es et outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...'
 AND SUBTASK = '... Labor, Parts, and Tools';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Pi�ces d�tach�es d��quipement par �quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Tools Information'
 AND SUBTASK = 'Tool Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... P�riode d��valuation de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...'
 AND SUBTASK = '... Condition Assessment Date Range';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Facilities Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = '� Est�ndares de mobiliario' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Reports' AND TASK = 'Move Items by Move Date - ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = '� Est�ndares de mobiliario' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = '� Est�ndares de mobiliario' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Inventory for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = '� Est�ndares de mobiliario' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Building for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = '� Est�ndares de mobiliario' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Trials by Floor for ...'
 AND SUBTASK = '... Furniture Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Standard d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...'
 AND SUBTASK = '... Equipment Standard';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Abteilungen nach Unternehmenseinheiten' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bereiche nach Unternehmenseinheiten' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_NL = 'Regio�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_NL = 'Regio�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_NL = 'Regio�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'M�mento d�activit�s li�es aux locations' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'M�mento d�activit�s li�es aux options des contrats de location' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Option Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Date d�exercice des options des contrats de location' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease Option Exercise Dates';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'M�mento d�activit�s li�es aux locations et Options' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Tickler Reports ...'
 AND SUBTASK = 'Lease and Option Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s par location' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s par option par location' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items by Lease Option by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments d�activit�s par responsabilit� par location' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Logs Items by Lease Resp. by Lease';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_CH = '��', SUBTASK_FR = 'Projets d�Activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Projects';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Activities ...'
 AND SUBTASK = 'Activity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_NL = 'Regio�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Suites and Dept. Spaces' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'M�mento des registres d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Log Tickler';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activities ...'
 AND SUBTASK = 'Activity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments de communication par registre d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...'
 AND SUBTASK = 'Comm. Log Items By Activity Log by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Co�ts et co�ts programm�s par registre d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs ...'
 AND SUBTASK = 'Costs and Scheduled Costs by Activity Log';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_NL = 'Regio�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Background' AND TASK = 'Space Information'
 AND SUBTASK = 'Regions';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Type d�activit�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...'
 AND SUBTASK = '... Activity Type';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Log Items';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'El�ments des registres d�activit�s par propri�t�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Log Items by Property';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_CH = '��', SUBTASK_FR = 'Projets d�Activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Projects';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...'
 AND SUBTASK = 'Activity Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�abords' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Runoff Information ...'
 AND SUBTASK = 'Runoff Area Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Surfaces d�abords' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Runoff Information ...'
 AND SUBTASK = 'Runoff Areas';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_IT = '... Modifica nell�area' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Co�ts de l�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_IT = '... Modifica nell�area' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Co�ts de l�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_IT = '... Modifica nell�area' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Co�ts de l�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_IT = '... Modifica nell�area' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Co�ts de l�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_IT = '... Modifica nell�area' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Change in Area';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Co�ts de l�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level ...'
 AND SUBTASK = '... Space Costs';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... par Nom d�employ�' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Status ...'
 AND SUBTASK = '... by Employee Name';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... par Nom d�employ�' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Emergency Information ...'
 AND SUBTASK = '... by Employee Name';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Consommation d��nergie' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Buildings with Issues of ...'
 AND SUBTASK = '... Energy Use';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Consommation d��nergie' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms with Issues of ...'
 AND SUBTASK = '... Energy Use';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... P�riode d��valuation environnementale' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessments by ...'
 AND SUBTASK = '... Sustainability Assessment Date Range';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Abteilungen nach Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bereiche nach Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Percentages by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Percentages by ...'
 AND SUBTASK = '... Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bilanz: Bereiche ohne Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Divisions without Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Non-Occupiable Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Occupiable Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Abteilungen nach Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Departments by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bereiche nach Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Divisions by Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Background Data' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Analyse d�imputation selon BOMA' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'BOMA Chargeback Analysis';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... nach Gruppe - Bilanz: Bereiche ohne Unternehmenseinheiten.' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Group Chargeback - Financial Stmt - Divisions w/o Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Groups by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bilanz: Bereiche ohne Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Divisions without Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Reports' AND TASK = 'Rooms by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Reports' AND TASK = 'Percentages by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bilanz: Multiraum - Bereiche ohne Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - All Room - Divisions w/o Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Bilanz: Multilayer - Bereiche ohne Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Chargeback Analysis ...'
 AND SUBTASK = 'Financial Statement - Composite - Divisions w/o Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Abteilungen, Bereiche, Unternehmenseinheiten' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Departments, Divisions, Bus. Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Standards d�employ�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Average Area of ...'
 AND SUBTASK = '... Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = '... Unternehmenseinheit' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees by ...'
 AND SUBTASK = '... Business Unit';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Champs de texte d�actifs' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Asset Text Fields';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Types d�actifs' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Drawing Standards Settings ...'
 AND SUBTASK = 'Asset Types';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Param�tres d�Activit�' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Project Settings ...'
 AND SUBTASK = 'Activity Parameters';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Identifier les donn�es invalides pour un champ d�une table' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Control' AND TASK = 'Schema Settings ...'
 AND SUBTASK = 'Check Table for Invalid Data in Field';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Vues d�Imputation par employ�s' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views'
 AND SUBTASK = 'Employee Chargeback Views';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = '... Ajouter un champ d�affichage de devise' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Currency Conversion ...'
 AND SUBTASK = '... Add Currency Display Field';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Dispositif d�arriv�e des services t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Background' AND TASK = 'Rooms'
 AND SUBTASK = 'Entrance Facilities';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_ES = 'Equipo' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Equipment';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Tables' AND TASK = 'Telecom Area Equipment'
 AND SUBTASK = 'Equipment Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Information'
 AND SUBTASK = 'Employee Standards';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_DE = 'Unternehmenseinheiten', SUBTASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Organization Information'
 AND SUBTASK = 'Business Units';
UPDATE AFM.AFM_SUBTASKS SET SUBTASK_FR = 'Dispositif d�arriv�e des services t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Information'
 AND SUBTASK = 'Entrance Facilities';

UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention ferm�es par type de cause' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Queries' AND TASK = 'Closed Work Requests by Cause Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention ferm�es par type de probl�me' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Queries' AND TASK = 'Closed Work Requests by Problem Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention ferm�es par type de r�paration' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Queries' AND TASK = 'Closed Work Requests by Repair Type';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Analyse : Main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Create Management Reports' AND TASK_CAT = 'Reports' AND TASK = 'Labor Analysis';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention ferm�es' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention ferm�es par ...' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ferm�e' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances' AND ACT = 'Review Work History' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Closed Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des demandes d�intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Actions' AND TASK = 'Create Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Examiner des Demandes d�Intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Actions' AND TASK = 'Review Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statut des demandes d�intervention ferm�es' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statut des demandes d�intervention ouvertes' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Open Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�Intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Create and Review Requests' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Estimer des demandes d�intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Actions' AND TASK = 'Estimate Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Programmer des demandes d�Intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Actions' AND TASK = 'Schedule Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention ouvertes par pi�ce D�tach�e' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Open Requests by Part';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Arri�r�s de demandes d�intervention par corps de m�tier' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Backlog by Trade';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�Intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Estimate and Schedule Requests' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affecter des demandes d�intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Assign Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux sur demande en cours par date d�affectation' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�Intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Establish On Demand Work' AND ACT = 'Generate On Demand Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux sur demande en cours par date d�affectation' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention en cours pour aujourd�hui' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'Active On Demand Work Requests for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention en attente' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active On Demand Work' AND TASK_CAT = 'Reports' AND TASK = 'On Hold On Demand Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux de MP en cours par dates d�affectation' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'Active PM Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux de MP en cours pour aujourd�hui' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'Active PM Work Orders for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention de MP en attente' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage Active Preventive Work' AND TASK_CAT = 'Reports' AND TASK = 'On Hold PM Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux en cours par dates d�affectation' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention en cours par ...' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Manage All Active Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Entrer les dates d�ach�vement de bons de travaux' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Actions' AND TASK = 'Enter Work Order Completion Dates';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ferm�e' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Closed Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�Intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Active Work' AND ACT = 'Update Work' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ins�rer de l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Pi�ces d�tach�es d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Parts';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire physique d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Physical Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings d��quipement', TASK_NL = 'Productieschema�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Schedules';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Equipo' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Pi�ces d�tach�es d��quipement par �quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Parts by Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de l��quipement par �quipement', TASK_NL = 'Productieschema�s per uitrusting' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Schedules by Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor' AND TASK_CAT = 'Reports' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Calculer l�utilisation du stock' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Parts Inventory' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Inventory Usage';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Disponibilit� des types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Reports' AND TASK = 'Tool Types Availability';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�outils' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Tools' AND TASK_CAT = 'Tables' AND TASK = 'Tool Types';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Calendario de trabajo para 52 de MP por�' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = '52 Week PM Work Schedule by ...';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Res�menes de previsi�n de recursos de MP para�' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Reports' AND TASK = 'PM Resource Requirements Forecast for ...';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'PO-schema�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Forecast Work and Resources' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'G�n�rer des bons de travaux de MP d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Generate Equipment PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'G�n�rer des bons de travaux de MP d�entretien' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Generate Housekeeping PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Emettre des bons de travaux de MP d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Issue (Print) Equipment PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Emettre des bons de travaux de MP d�entretien' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Actions' AND TASK = 'Issue (Print) Housekeeping PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux de MP d��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Equipment PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux de MP d�entretien' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Reports' AND TASK = 'Housekeeping PM Work Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d��quipement', TASK_NL = 'PO-planningsschema�s voor uitrusting' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Equipment PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d�entretien', TASK_NL = 'PO-schema�s voor schoonmaakdienst' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'Housekeeping PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Groeperingen van PO-schema�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Generate PM Work Orders' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedule Groupings';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des plannings de MP d��quipement', TASK_NL = 'PO-planningsschema�s voor uitrusting aanmaken' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Actions' AND TASK = 'Create Equipment PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des plannings de MP d�entretien', TASK_NL = 'PO-planningsschema�s voor schoonmaakdienst aanmaken' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Actions' AND TASK = 'Create Housekeeping PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Informations sur la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Background Tables' AND TASK = 'Labor Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d��quipement avec dates de planning', TASK_NL = 'PO-planningsschema�s voor uitrusting met geplande datums' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment PM Schedules with Schedule Dates';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d�entretien par pi�ce' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Housekeeping PM Schedules by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d�entretien avec dates de planning', TASK_NL = 'PO-schema�s voor schoonmaakdienst met geplande datums' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'Housekeeping PM Schedules with Schedule Dates';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'PO-schema�s per uitrusting' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'PM Schedules by Equipment';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'PO-schema�s per primaire vakdiscipline' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'PM Schedules by Primary Trade';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'PO-schema�s per schemagroep' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Reports' AND TASK = 'PM Schedules by Schedule Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d��quipement', TASK_NL = 'PO-planningsschema�s voor uitrusting' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plannings de MP d�entretien', TASK_NL = 'PO-schema�s voor schoonmaakdienst' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'Housekeeping PM Schedules';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Datums van PO-schema�s per PO-schema' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedule Dates by PM Schedule';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Groeperingen van PO-schema�s' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Plan Preventive Maintenance' AND ACT = 'Schedule Preventive Maintenance' AND TASK_CAT = 'Tables' AND TASK = 'PM Schedule Groupings';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des demandes d�intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ferm�e' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Show Single Closed Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des demandes d�intervention' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Create Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Maintenance d��quipement - A�ration' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Equipment Maintenance History - Air Handlers';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Proc�dures de MP d��quipement - Unit�s d�a�ration' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Equipment PM Procedures - Air Handlers';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Editer les �valuations de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Edit Condition Assessments';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'G�rer les objets d��valuation de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Manage Condition Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tableau d��valuation' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Scoreboard';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'V�rifier les �valuations de l��tat du patrimoine termin�es' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Verify Completed Condition Assessments';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Equipo' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation de l��tat du patrimoin avec photos' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Items with Images';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statist. des projets d��valuation de l��tat du patrimoine par ...' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessment Project Statistics by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de l��tat du patrimoine par ...' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de l��tat du patrimoine avec travaux en cours par ...' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Active Work by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de l�Etat du Patrimoine avec Taux de V�tust� Sup�rieur � 25 �' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Condition Assessments with Condition Ratings Greater than 25';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de l��tat de l��quipement par ...' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Condition Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de l��tat des pi�ces par ...' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Condition Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statist. sur statut des DI par projet d��val. de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Status Statistics by Condition Assessment Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de l��tat de l��quipement par classif. de Niveau 3' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Equipment Condition Assessments by Classification Level 3';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Budget f�r Objekten mit Zustandsbeurteilung �Ungeeignet� nach Priorit�t', TASK_FR = 'Budget des Objets d�Evaluation de l�Etat du Patrimoine Non Satisfaisants par Priorit�', TASK_NL = 'Budget voor objecten met conditiemeting �ongeschikt� naar prioriteit' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Unsuitable Condition Assessment Items Budget by Priority';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Condition Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation de l��tat du patrimoin par projet' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Condition Assessment Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Mouvements d�employ�s - Bons de d�m�nagement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Employees Move History - Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Mouvements d��quipement - Bons de d�m�nagement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Move History - Move Orders';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Mouvements d��quipement - Relev�s' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Manage Churn' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Move History - Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Calculer les tableaux d�amortissements de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Equipment Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Calculer les tableaux d�amortissements du mobilier cod�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Tagged Furniture Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Journaux d�amortissements' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tableaux d�amortissements de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Entr�e au grand journal d�un �quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Equipment General Ledger Journal Entry';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tableaux d�amortissements du mobilier cod�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Entr�e au grand journal d�un mobilier cod�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture General Ledger Journal Entry';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Journaux d�amortissements' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Create Management Reports' AND ACT = 'Report on Costs and Depreciation' AND TASK_CAT = 'Tables' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Catalogue de standards d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Standards Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Liste des standards d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Standards List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'Manage Standards and Libraries' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des bons de d�m�nagement d�employ�s pour un d�partement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Create Employee Move Orders for Department';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bon de d�m�nagement d�employ�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Actions' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'D�m�nager l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Move Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Peupler avec de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affectations de l��quipement aux bons de d�m�nagements' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Manage Move Orders' AND TASK_CAT = 'Tables' AND TASK = 'Move Order Equipment Assignments';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer les simulations � l�inventaire pour ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Actions' AND TASK = 'Compare Trials to Inventory for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mettre � jour la disposition de l��quipement en inventaire' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Update Equipment Inventory Layouts';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Utiliser les simulations d�employ�s pour ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Employee Trial Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Utiliser les simulations d��quipement pour ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Equipment Trial Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Provenance de l��quipement d�m�nag� pour ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment From Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Destination de l��quipement d�m�nag� pour ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Queries' AND TASK = 'Relocated Equipment To Locations for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Disposition de l��quipement pour...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Manage Moves and Layouts' AND ACT = 'Plan Moves and Layouts' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bon de d�m�nagement d�employ�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bon de d�m�nagement d�employ�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Equipement et polices d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Equipement par police sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Policies about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Polices d�assurance par assureur' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Liste de restriction d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Equipement et polices d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Equipement et polices d�assurance pour la liste de restriction' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Equipment and Policies for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Polices d�assurance par assureur' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Equipement par location sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Equipment by Leases about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Liste de restriction d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Leases' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Equipement et contrats de garantie sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Equipment and Warranties about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Liste de restriction d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer un relev� d��quipement � l�inventaire' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Compare Equipment Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Faire des audits pour les relev�s d�affectations d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Equipment Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'M � J l�inventaire d��quip. en fonction des relev�s d�affect.' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Update Equipment Inventory from Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher l�inventaire d��quipement et les dessins' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Drawings' AND TASK = 'Display Equipment Inventory and Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Audits d��quipement par relev� d�affectation' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Affectation de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Disposition History';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Relev�s d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Audits d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Audits';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Audits d��quipement par relev� d�affectation' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Entrer des localisations d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Actions' AND TASK = 'Enter Equipment Locations';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Disposer de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Drawings' AND TASK = 'Lay Out Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affectation de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Disposition';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affectation de l��quipement dans la liste de restriction' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Disposition for Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Catalogue d�inventaire d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Book';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'D�comptes d��quipement par standard' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'D�comptes d��quipement par standard par ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory Counts by Standard by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire d��quipement par ...' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Inventory by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Disposition de l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Layout Report';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Liste de restriction d��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment' AND ACT = 'Track Locations and Ownership' AND TASK_CAT = 'Tables' AND TASK = 'Equipment Restriction List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards de mobilier et polices d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Standards and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire stds mob par police sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Std. Inventory by Policies about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Polices d�assurance par assureur' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards de mobilier et polices d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standards and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Polices d�assurance par assureur' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire stds mob par location sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Std. Inventory by Leases about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Inventaire stds mob par garantie sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Furniture Std. Inv. by Warranties about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'G�n�rer un audit de stds de mobilier � partir de l�inventaire' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from CAD Layout' AND TASK_CAT = 'Actions' AND TASK = 'Generate Furniture Std. Survey Audit from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ajouter un relev� de standards de mobilier � l�inventaire' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Add Furniture Standard Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer un relev� de standards de mobilier � l�inventaire' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Compare Furniture Standard Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Faire des audits pour les relev�s d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Faire des audits pour les relev�s d�affectations par pi�ce' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Disposition Survey by Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Remplacer l�inventaire de stds de mobilier par un relev�' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Replace Furniture Std. Inventory with Survey Audit';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Peupler plans CAO � partir de l�inventaire de stds' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Drawings' AND TASK = 'Populate CAD Layout from Standards Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Relev�s d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Audits de standards de mobilier par relev� d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Furniture By Standards' AND ACT = 'Manage from Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Furniture Standard Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Polices d�assurance par assureur' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mobilier cod� et polices d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mobilier cod� par police sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Policies about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mobilier cod� par police d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Policy';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Polices d�assurance par assureur' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Policies by Insurer';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mobilier cod� et polices d�assurance' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Insurance' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture and Policies';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mobilier cod� par location sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Leases' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture by Leases about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mobilier cod� et garanties sur le point d�expirer' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Manage Warranties' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture and Warranties about to Expire';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer un relev� de mobilier cod� � l�inventaire' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Compare Tagged Furniture Survey Audit to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Faire des audits pour les relev�s d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Perform Audits for Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'M � J l�inventaire de mob cod� en fonction des relev�s' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Actions' AND TASK = 'Update Tagged Furniture Inventory from Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Audits de mobilier cod� par relev� d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Reports' AND TASK = 'Tagged Furniture Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Relev�s d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Disposition Surveys';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Audits de mobilier cod� par relev� d�affectations' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Tagged Furniture' AND ACT = 'Perform Surveys' AND TASK_CAT = 'Tables' AND TASK = 'Tagged Furniture Audits by Disposition Survey';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner un diagramme d�empilement d��tages' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Background Data' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Floor Stack';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Crear costes de actividad para�', TASK_FR = 'Cr�er des co�ts d�activit�s pour les ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�am�liorations locatives' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Leasehold Improvement Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner des zones d�abords de parking' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parking Runoff Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des registres d�activit�s par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Items in communicatielogboek per �' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Communication Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Leaseverplichtingen �' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Lease Responsibilities ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imputation - ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Crear costes de actividad para�', TASK_FR = 'Cr�er des co�ts d�activit�s pour les ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�am�liorations locatives' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Leasehold Improvement Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�activit�s li�es aux ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts et co�ts d�imputation par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Accords d�Imputation des locations' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Reports' AND TASK = 'Lease Chargeback Agreements';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�activit�s li�es aux ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Accords d�Imputation des locations' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Leases' AND ACT = 'Manage Lease Financials' AND TASK_CAT = 'Tables' AND TASK = 'Lease Chargeback Agreements';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs by Project';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Crear costes de actividad para�', TASK_FR = 'Cr�er des co�ts d�activit�s pour les ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Communicatielogboeken �' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Communication Logs ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des registres d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Activity Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des registres d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Activity Log Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Activity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Registres de communication par �l�ment d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Communication Logs By Activity Log Item by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts et co�ts programm�s par registre d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by Activity Log';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts et co�ts programm�s par registre d�activit�s par projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Projects' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Costs and Scheduled Costs by Activity Log by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�activit�s pour les propri�t�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for Properties';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner des surfaces d�abords' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Runoff Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des registres d�activit�s par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'M�mento d�activit�s li�es aux propri�t�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Reports' AND TASK = 'Property Activity Log Tickler';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Registres d�activit�s ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Abstracts' AND TASK_CAT = 'Tables' AND TASK = 'Activity Logs ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Calculer les tableaux d�amortissements de l��quipement' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Equipment Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Calculer les tableaux d�amortissements des actifs de propri�t�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Property Assets Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Calculer les tableaux d�amortissements du mobilier cod�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Calculate Tagged Furniture Depreciation Schedules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Editer les journaux d�amortissements' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Actions' AND TASK = 'Edit Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner des zones d�abords de parking' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Parking Runoff Areas';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Journaux d�amortissements' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tableaux d�amortissements pour ...', TASK_NL = 'Afschrijvingsschema�s voor ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Reports' AND TASK = 'Depreciation Schedules for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Journaux d�amortissements' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Assets' AND TASK_CAT = 'Tables' AND TASK = 'Depreciation Logs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imputation - ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Crear costes de actividad para�', TASK_FR = 'Cr�er des co�ts d�activit�s pour les ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_NL = 'Opstellen facturen voor �' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Actions' AND TASK = 'Create Invoices for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts et co�ts d�imputation par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�activit�s li�es aux ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Property Financials' AND TASK_CAT = 'Tables' AND TASK = 'Activity Costs for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�activit�s pour les �l�ments r�glement�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for Regulation Comp. Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s li�es aux r�glementations' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s li�es aux r�glementations par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Items by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'M�mento d�activit�s li�es aux r�glementations' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Reports' AND TASK = 'Regulation Activity Log Tickler';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s par �l�ment r�glement�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Log Items by Regulation Compliance Item';
UPDATE AFM.AFM_TASKS SET TASK_CH = '��', TASK_FR = 'Projets d�Activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Regulations' AND TASK_CAT = 'Tables' AND TASK = 'Activity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imputation - ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Chargeback Costs - ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�activit�s li�es aux imp�ts' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Create Activity Costs for Taxes';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er un budget d�imp�ts' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Create Tax Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des co�ts d�imp�ts' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Create Tax Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Consolider les co�ts d�imp�ts par propri�t�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Sum Tax Costs to Property';

UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mettre � jour les taux d�imposition sur les propri�t�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Actions' AND TASK = 'Update Property Tax Rates';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Arri�r�s d�imp�ts' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Overdue Taxes';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Registre d�activit�s li�es aux Imp�ts ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s d�imp�ts par Projet' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s d�imp�ts par propri�t�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Activity Log Items by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imp�ts � imputer par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imp�ts par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Paiements d�imp�ts soumis � autorisation' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Reports' AND TASK = 'Tax Payments Needing Authorization';
UPDATE AFM.AFM_TASKS SET TASK_CH = '��', TASK_FR = 'Projets d�Activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Activity Projects';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�activit�s' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Activity Types';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s li�es aux Imp�ts' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Activity Log Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s d�imp�ts par propri�t�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Activity Log Items by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments d�activit�s d�imp�ts par projet fiscal' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Activity Log Items by Tax Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�imp�ts' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imp�ts et co�ts programm�s par propri�t�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Costs and Scheduled Costs by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts d�imp�ts r�currents par propri�t�' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Manage Properties' AND ACT = 'Manage Taxes' AND TASK_CAT = 'Tables' AND TASK = 'Tax Recurring Costs by Property';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des registres d�activit�s li�es aux ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Items for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Rapports : M�mentos d�activit�s li�es aux ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Activity Log Tickler Reports for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts et co�ts d�imputation par ...' WHERE AFM_MODULE = 'Lease' AND ACT_CLASS = 'Quick Access & Executive Reports' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Costs and Chargeback Costs by ...';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Unternehmenseinheiten', TASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Diagramme d�empilement des affectations' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Allocation Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affectations des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Allocations';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affectations des budgets d�espace par �tage' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Allocations by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'P�riodes des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Allocation' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Prognose auf Unternehmenseinheitebene erstellen' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Business Unit Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er une pr�vision d�espace � partir de l�inventaire' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Forecast from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er une pr�vision au niveau Standard d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Standards Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Unternehmenseinheiten', TASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Auf Unternehmenseinheitsebene ...', TASK_NL = 'Niveau van organisatie-eenheid �' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'P�riodes des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Forecasting' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer l�historique � l�inventaire' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare History to Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer l�historique au programme des besoins' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare History to Requirements Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er un historique � partir de l�inventaire' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create History from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Historische Trendanalyse auf Unternehmenseinheitebene+' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Historical Trends Analysis - Business Unit Level';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Raumnutzungshistorie auf Unternehmenseinheitsebene', TASK_FR = 'Historique : Utilisation de l�espace au niveau Unit�' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Utilisation de l�espace au niveau D�partement' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Department Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Utilisation de l�espace au niveau Division' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Division Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Historique : Utilisation de l�espace au niveau groupe' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Functional Group Level Historical Space Use';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'P�riodes des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'History' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Comparer les budgets d�espace inventaire / simulation pour ...' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Compare Inventory and Trial Space Budgets for ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er un budget d�espace � partir de l�inventaire' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Inventory Budget from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er un budget d�espace � partir de ...' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Trial Budget from ...';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Unternehmenseinheiten', TASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_CH = '��������...' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Work with Trial Room Layouts for ...';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 1 ����' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Trial 1';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 2 ����' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Trial 2';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 2 ����' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Room Layout from Trial 3';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 1' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Trial 1';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 2' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Trial 2';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 3' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Trial 3';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'P�riodes des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�����' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Layout' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Trial Rooms';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Diagramme d�empilement des affectations' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Allocation Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Co�ts pr�visionnels de l�espace au niveau D�partement' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Departmental Level Forecast Space Costs';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Analyse : Historique d�utilisation de l�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Historical Space Usage Trends';
UPDATE AFM.AFM_TASKS SET TASK_CH = '�� 1 ����' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Room Layout for Trial 1';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Pr�vision au niveau Standard d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Standards Level Forecast';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Programmer les besoins en espace � partir de l�inventaire' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Requirements Program Budget from Inventory';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Unternehmenseinheiten', TASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Auf Unternehmenseinheitsebene ...', TASK_NL = 'Niveau van organisatie-eenheid �' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Business Unit Level ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'P�riodes des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Bedarfsplan auf Unternehmenseinheitsebene' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Business Unit Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'El�ments des budgets d�espace par Budget' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Items by Budget';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'P�riodes des budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budget Periods';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Budgets d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Space Budgets';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Programme au niveau Standard d�espace' WHERE AFM_MODULE = 'Planning' AND ACT_CLASS = 'Requirements Programming' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Standards Level Program';
UPDATE AFM.AFM_TASKS SET TASK_FR = '... par Nom d�employ�' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = '... by Employee Name';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner des plans d��vacuation' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Egress Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mettre � jour le statut de l��quipement par zone' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Update Equipment Status by Zone';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plans d�Evacuation', TASK_NL = 'Nooduitgangenschema�s' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Egress Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bulletins d�information � l�attention des employ�s' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Advisory Bulletin for Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bulletins d�Information � l�attention des Responsables' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Advisory Bulletin for Managers';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Contacts en Cas d�Urgence' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Emergency Contacts';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Empleados e informaci�n de emergencia �', TASK_FR = 'Employ�s et informations d�urgence ...' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employees and Emergency Information ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statut de l��quipement par �tage' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Status by Floor';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bulletins d�information � l�attention des employ�s' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Advisory Bulletin for Employees';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bulletins d�Information � l�attention des Responsables' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Advisory Bulletin for Managers';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Contacts en Cas d�Urgence' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Emergency Contacts';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Employ�s et informations d�urgence' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employees and Emergency Information';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Annuaire t�l�phonique de l��quipe de GSU' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Recovery Team Call List';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'G�rer les objets d��valuation environnementale' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Manage Sustainability Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tableau d��valuation' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Scoreboard';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'Equipo' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Background Tables' AND TASK = 'Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de durabilit� d��quipement par ...' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Equipment Sustainability Assessments by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Probl�mes de consommation d��nergie non r�solus' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Open Energy Usage Issues';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation environnementale' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation environnementale avec photos' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Items with Images';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statistiques des projets d��valuation environnementale par ...' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Sustainability Assessment Project Statistics by ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statist. sur le statut des DI par projet d��val. environnmentale' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Status Statistics by Sustainability Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Evaluations de durabilit� d��quipement par classif. de Niveau 3' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Equipment Sustainability Assessments by Classification Level 3';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Budget f�r Obj. mit �koeffiz.analyse �Ungeeignet� nach Prior.', TASK_FR = 'Budget des objets d��val. environ. non satisfaisants par priorit�', TASK_NL = 'Budget voor obj. met milieueff.beoord. �ongeschikt� naar prioriteit' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Summary Reports' AND TASK = 'Unsuitable Sustainability Assessment Items Budget by Priority';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation environnementale' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Sustainability Assessment Items';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Objets d��valuation environnementale par projet' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Environmental Sustainability' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Sustainability Assessment Items by Project';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation selon les pourcentages' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Perform Percentage Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Actions' AND TASK = 'Perform Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Diagramme d�Empilement des D�partements' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plan d�Occupation' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'All Room Inventory' AND ACT = 'Rooms' AND TASK_CAT = 'Queries' AND TASK = 'Occupancy Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation selon BOMA 96' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform BOMA 96 Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation selon BOMA' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform BOMA Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation avanc�e selon BOMA' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform Enhanced BOMA Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation par groupes' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform Group Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Diagramme d�Empilement des D�partements' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_ES = 'An�lisis de est�ndar de grupo�' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Group Areas' AND TASK_CAT = 'Reports' AND TASK = 'Group Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation par pi�ces' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Actions' AND TASK = 'Perform Room Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Diagramme d�Empilement des D�partements' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Departmental Stack Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plan d�Occupation' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Areas' AND TASK_CAT = 'Queries' AND TASK = 'Occupancy Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation selon les pourcentages' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Composite Inventory' AND ACT = 'Room Percentages' AND TASK_CAT = 'Actions' AND TASK = 'Perform Percentage Chargeback';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des bons de d�mgt d�employ�s � partir des bureaux attribu�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Employee Move Orders from Bookings';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er un budget d�espace � partir des bureaux attribu�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Space Budget from Bookings';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Unternehmenseinheiten', TASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Hoteling' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation par employ�s - Inv. Pi�ces Seult' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Perform Employee Chargeback - All Room';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ex�cuter l�imputation par employ�s - Inv. Compos�' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Perform Employee Chargeback - Composite';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mettre � jour les effectifs d�employ�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Update Employee Headcounts';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ins�rer des d�signateurs d�employ�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Insert Employee Designators';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plan d�Occupation' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Occupancy Plan';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Analyse : Standards d�employ�s ...' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standard Analysis ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Employee Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d�Employ�s' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Personnel' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Employee Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Diagramme d�empilement des d�partements - Groupes' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Executive Reports' AND TASK = 'Departmental Stack Plan - Groups';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Annuler une r�servation de pi�ce' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Cancel a Room Reservation';
UPDATE AFM.AFM_TASKS SET TASK_DE = 'Unternehmenseinheiten', TASK_FR = 'Unit�s organisationnelles' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Background' AND TASK = 'Business Units';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Surbrillance des pi�ces r�serv�es pour aujourd�hui' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Highlight Rooms Reserved for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Pourcentage d�occupation des pi�ces r�serv�es pour une p�riode' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Reserved Rooms Percent Occupancy for Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'R�servations pour aujourd�hui' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Reservations for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'R�servations non confirm�es pour aujourd�hui' WHERE AFM_MODULE = 'Space' AND ACT_CLASS = 'Room Reservations' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Unconfirmed Reservations for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Exporter � partir d�une vue avec standard' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Connectivity' AND ACT = 'NONE' AND TASK_CAT = 'Connect' AND TASK = 'Export from View with Standard';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Types d�actifs et r�gles de publication' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Drawing Publishing' AND ACT = 'NONE' AND TASK_CAT = 'DrawingPublishing' AND TASK = 'Asset Types and Publishing Rules';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Ajouter les vues SQL de l�Assistant de maintenance' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Add Work Wizard SQL Views';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Recr�er l�index de la table des connexions t�l�com' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Re-Create Telecom Connections Table Index';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'M � J les D�clencheurs de bons de travx et dem. d�intervention' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update BldgOps Work Order and Request Triggers';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'M � J les d�clencheurs de gestion des situations d�urgence' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Emergency Preparedness Triggers';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Mettre � jour les vues SQL de gestion de l�espace' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Define' AND TASK = 'Update Space Management SQL Views';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Classes d�activit�s' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate' AND TASK = 'Activity Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cat�gories d�Activit�s' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Activity Categories';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tables sous Licence d�utilisation et leurs activit�s' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Licensed Tables and Their Activities';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Tables sous licence d�utilisation par activit�' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Navigate Activities' AND TASK = 'Tables Licensed by Activity';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Traductions des cat�gories d�activit�s' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Translate' AND TASK = 'Activity Categories Translations';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Journal d�audit ...' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Schema' AND ACT = 'NONE' AND TASK_CAT = 'Update' AND TASK = 'Audit Log ...';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'D�sactiver la s�curit� de l�application Windows' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = 'Set Windows Application Security Off';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Activer la s�curit� de l�application Windows' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Enable' AND TASK = 'Set Windows Application Security On';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Classes d�activit�s' WHERE AFM_MODULE = 'System' AND ACT_CLASS = 'Security' AND ACT = 'NONE' AND TASK_CAT = 'Secure' AND TASK = 'Activity Classes';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des ports pour l��quipement' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Create Ports for Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher l��quipement desservi par un �l�ment de r�seau' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Actions' AND TASK = 'Show Equipment Served by Network Device';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner l��quipement des zones t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Telecom Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessins des dispositifs d�arriv�e des services t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'Entrance Facility Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessins des pi�ces d��quipement' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Queries' AND TASK = 'Equipment Room Drawings';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Examiner les dispositifs d�arriv�e des services t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Telecom Areas' AND TASK_CAT = 'Reports' AND TASK = 'Review Entrance Facility';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Connecter automatiquement l��quipt des zones de travail aux prises' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = 'Auto-Connect Work Area Equipment to Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Examiner l��quipt des zones travail non connect� aux prises' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Actions' AND TASK = 'Review Work Area Equipment Not Connected to Jacks';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner de l��quipement donn�es' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Data Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner l��quipement voix' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Voice Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Dessiner l��quipement de zones de travail' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Drawings' AND TASK = 'Draw Work Area Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Plans d��quipement des zones de travail', TASK_NL = 'Schema�s van uitrusting voor werkplekoppervlaktes' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Queries' AND TASK = 'Work Area Equipment Plans';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement donn�es' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Data Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement voix' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Voice Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement des zones de travail' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Reports' AND TASK = 'Work Area Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement donn�es' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Data Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement voix' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Voice Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Standards d��quipement des zones de travail' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Develop Inventory' AND ACT = 'Work Areas' AND TASK_CAT = 'Tables' AND TASK = 'Work Area Equipment Standards';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bon de d�m�nagement d�employ�' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Employee Move Order';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'D�m�nager l��quipement' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Move Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Peupler avec de l��quipement' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Drawings' AND TASK = 'Populate Equipment';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Affectations de l��quipement aux bons de d�m�nagements' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Manage Moves' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Move Order Equipment Assignments';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des demandes d�intervention t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Telecom Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher l��quipement desservi par un �l�ment de r�seau' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Quick Access' AND ACT = 'NONE' AND TASK_CAT = 'Queries' AND TASK = 'Show Equipment Served by Network Device';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Cr�er des demandes d�intervention t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Create Telecom Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Examiner les demandes d�intervention t�l�com' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Actions' AND TASK = 'Review Telecom Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Bons de travaux en cours par dates d�affectation' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Orders by Assigned Date Range';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�intervention en cours pour aujourd�hui' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Active Work Requests for Today';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statut des demandes d�intervention ferm�es' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Closed Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Statut des demandes d�intervention ouvertes' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Open Work Requests Status';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Afficher une unique demande d�intervention ouverte' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Show Single Open Work Request';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Arri�r�s de demandes d�intervention' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Reports' AND TASK = 'Work Request Backlog';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Demandes d�Intervention' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Run Help Desk' AND ACT = 'NONE' AND TASK_CAT = 'Tables' AND TASK = 'Work Requests';
UPDATE AFM.AFM_TASKS SET TASK_FR = 'Assigner de l��quipement � un segment du r�seau' WHERE AFM_MODULE = 'T&C' AND ACT_CLASS = 'Track Net Segments and Software' AND ACT = 'Net Segments' AND TASK_CAT = 'Tables' AND TASK = 'Assign Equipment to Net Segment';

UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-ac-gd-dflt.axvw' WHERE TABLE_NAME = 'ac';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-acbu-gd-dflt.axvw' WHERE TABLE_NAME = 'acbu';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-active-plantypes-gd-dflt.axvw' WHERE TABLE_NAME = 'active_plantypes';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'El�ments d�Action', DEFAULT_VIEW = 'ab-activity-log-gd-dflt.axvw' WHERE TABLE_NAME = 'activity_log';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������', TITLE_DE = 'Alle Ma�nahmenobjekte', TITLE_ES = 'Todas las actividades', TITLE_FR = 'Tous les �l�ments d�action', TITLE_IT = 'Tutte le voci azione', TITLE_NL = 'Alle actiepunten' WHERE TABLE_NAME = 'activity_log_hactivity_log';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '��������', TITLE_DE = 'Aktivit�tenprotokoll - Bevorstehende Schritte', TITLE_ES = 'Pasos pendientes de registro de actividad', TITLE_FR = 'Etapes en attente du registre d�activit�s', TITLE_IT = 'Procedure in sospeso registro attivit�', TITLE_NL = 'Activiteitenlogboek onafgehandelde stappen' WHERE TABLE_NAME = 'activity_log_step_waiting';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Sync. des actions de modification du sch�ma d�affectation d��tat' WHERE TABLE_NAME = 'activity_log_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Transactions des �l�ments d�Action' WHERE TABLE_NAME = 'activity_log_trans';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������', TITLE_DE = 'Ma�nahmenobjekte - Ansicht', TITLE_ES = 'Vista de actividades', TITLE_FR = 'Vue des �l�ments d�action', TITLE_IT = 'Visualizzazione voci azione', TITLE_NL = 'Actiepuntenweergave' WHERE TABLE_NAME = 'activity_logview';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�activit�s' WHERE TABLE_NAME = 'activitytype';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Actiescenario�s' WHERE TABLE_NAME = 'actscns';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Bulletins d�Information' WHERE TABLE_NAME = 'advisory';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Tables d�applications ARCHIBUS' WHERE TABLE_NAME = 'afm_act_tbls';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-activities-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_activities';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cat�gories d�activit�s ARCHIBUS' WHERE TABLE_NAME = 'afm_activity_cats';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Param�tres de l�Application' WHERE TABLE_NAME = 'afm_activity_params';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�actifs ARCHIBUS' WHERE TABLE_NAME = 'afm_atyp';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-bim-categories-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_bim_categories';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-bim-families-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_bim_families';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-bim-params-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_bim_params';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Classes d�activit�s ARCHIBUS' WHERE TABLE_NAME = 'afm_class';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-conversions-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_conversions';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'ARCHIBUS-valuta�s', DEFAULT_VIEW = 'ab-afm-currencies-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_currencies';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-data-event-log-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_data_event_log';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-docs-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_docs';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-docvers-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_docvers';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-dwgpub-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_dwgpub';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-dwgs-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_dwgs';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-dwgvers-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_dwgvers';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-flds-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_flds';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-flds-lang-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_flds_lang';
UPDATE AFM.AFM_TBLS SET TITLE_CH = 'ARCHIBUS ������', TITLE_DE = 'ARCHIBUS-Felderdefinitions�bertragung', TITLE_ES = 'Transferencia de definici�n de campos de ARCHIBUS', TITLE_FR = 'Transfert des d�finitions de champs ARCHIBUS', TITLE_IT = 'Trasferimento definizione campi ARCHIBUS', TITLE_NL = 'Overzetten definitie ARCHIBUS-velden' WHERE TABLE_NAME = 'afm_flds_trans';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-groups-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_groups';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-groupsforroles-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_groupsforroles';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-layr-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_layr';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Juridische ID�s', TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'afm_legal';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-metric-definitions-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_metric_definitions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Notifications d�indicateurs' WHERE TABLE_NAME = 'afm_metric_notify';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-metric-scards-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_metric_scards';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-metric-trend-values-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_metric_trend_values';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Registre d�enregistrement des p�riph�riques mobiles des utilisateurs ARCHIBUS' WHERE TABLE_NAME = 'afm_mob_dev_reg_log';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-mobile-apps-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_mobile_apps';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Historique de synchronisation d�ARCHIBUS Mobile' WHERE TABLE_NAME = 'afm_mobile_sync_history';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-notifications-log-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_notifications_log';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-processes-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_processes';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-ptasks-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_ptasks';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-redlines-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_redlines';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-roleprocs-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_roleprocs';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-roles-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_roles';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-tbls-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_tbls';
UPDATE AFM.AFM_TBLS SET TITLE_CH = 'ARCHIBUS ���', TITLE_DE = 'ARCHIBUS-�bertragungssatz', TITLE_ES = 'Conjunto de transferencia de ARCHIBUS', TITLE_FR = 'Jeu de transfert ARCHIBUS', TITLE_IT = 'Set trasferimento ARCHIBUS', TITLE_NL = 'ARCHIBUS-transferset' WHERE TABLE_NAME = 'afm_transfer_set';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-afm-users-gd-dflt.axvw' WHERE TABLE_NAME = 'afm_users';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Historique de transactions d�actif' WHERE TABLE_NAME = 'asset_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Donn�es des syst�mes d�automatisation des b�timents (BAS) - Valeurs num�riques propres' WHERE TABLE_NAME = 'bas_data_clean_num';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Donn�es des syst�mes d�automatisation des b�timents (BAS) - Valeurs num�riques normalis�e' WHERE TABLE_NAME = 'bas_data_time_norm_num';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Etendue des mesures des syst�mes d�automatisation des b�timents (BAS)' WHERE TABLE_NAME = 'bas_measurement_scope';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-bl-gd-dflt.axvw' WHERE TABLE_NAME = 'bl';
UPDATE AFM.AFM_TBLS SET TITLE_DE = 'Unternehmenseinheiten', TITLE_FR = 'Unit�s organisationnelles' WHERE TABLE_NAME = 'bu';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Sources d�accr�ditation pour les mati�res dangereuses' WHERE TABLE_NAME = 'cb_accredit_source';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�accr�ditation pour les mati�res dangereuses' WHERE TABLE_NAME = 'cb_accredit_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Composition de l��chantillon de mati�res dangereuses' WHERE TABLE_NAME = 'cb_sample_comp';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Equipe d�intervention du technicien', TITLE_IT = 'Tema di lavoro dell�addetto' WHERE TABLE_NAME = 'cf_work_team';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-coa_cost_group-gd-dflt.axvw' WHERE TABLE_NAME = 'coa_cost_group';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-coa_source-gd-dflt.axvw' WHERE TABLE_NAME = 'coa_source';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Transactions d�indices des co�ts' WHERE TABLE_NAME = 'cost_index_trans';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Valeurs d�indices des co�ts' WHERE TABLE_NAME = 'cost_index_values';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Journaux d�amortissements' WHERE TABLE_NAME = 'dep_reports';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-dp-gd-dflt.axvw' WHERE TABLE_NAME = 'dp';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-dv-gd-dflt.axvw' WHERE TABLE_NAME = 'dv';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Equipement de protection individuelle de l�employ�' WHERE TABLE_NAME = 'ehs_em_ppe_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cat�gories de causes premi�res d�accident' WHERE TABLE_NAME = 'ehs_incident_cause_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Surfaces touch�es lors de l�accident', TITLE_IT = 'Area infortunata a causa dell�incidente' WHERE TABLE_NAME = 'ehs_incident_injury_areas';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cat�gories de blessures li�es � l�accident' WHERE TABLE_NAME = 'ehs_incident_injury_cat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Mesures correctives � long terme de l�accident' WHERE TABLE_NAME = 'ehs_incident_long_tm_ca';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Mesures correctives � court terme de l�accident' WHERE TABLE_NAME = 'ehs_incident_short_tm_ca';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�accident' WHERE TABLE_NAME = 'ehs_incident_types';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'T�moin de l�accident' WHERE TABLE_NAME = 'ehs_incident_witness';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d��quipement de protection individuelle' WHERE TABLE_NAME = 'ehs_ppe_types';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Opleidingsprogramma�s' WHERE TABLE_NAME = 'ehs_training';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Categorie�n opleidingsprogramma�s' WHERE TABLE_NAME = 'ehs_training_cat';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Typen opleidingsprogramma�s' WHERE TABLE_NAME = 'ehs_training_types';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Opleidingsprogramma�s arbeidscategorie' WHERE TABLE_NAME = 'ehs_work_cat_training';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-em-gd-dflt.axvw' WHERE TABLE_NAME = 'em';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Inventaire d�Employ�s / Simulation' WHERE TABLE_NAME = 'em_compinvtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Synchronisation d�employ�s' WHERE TABLE_NAME = 'em_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Simulations de Disposition d�Employ�s' WHERE TABLE_NAME = 'em_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Standards d�Employ�s', DEFAULT_VIEW = 'ab-emstd-gd-dflt.axvw' WHERE TABLE_NAME = 'emstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'P�riodes d�Entretien du B�timent' WHERE TABLE_NAME = 'energy_bl_svc_period';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Points du graphe de consommation d��nergie' WHERE TABLE_NAME = 'energy_chart_point';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Pi�ces d�tach�es d��quipement' WHERE TABLE_NAME = 'ep';
UPDATE AFM.AFM_TBLS SET TITLE_ES = 'Equipo', DEFAULT_VIEW = 'ab-eq-gd-dflt.axvw' WHERE TABLE_NAME = 'eq';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Audits d��quipement' WHERE TABLE_NAME = 'eq_audit';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Liste de restriction d��quipement' WHERE TABLE_NAME = 'eq_bar_code_list';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Inventaire d��quipement / Audit' WHERE TABLE_NAME = 'eq_compinvsur';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Inventaire d��quipement / Simulation' WHERE TABLE_NAME = 'eq_compinvtrial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Amortissements d�Equipement' WHERE TABLE_NAME = 'eq_dep';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'D�comptes d�Equipement par standard' WHERE TABLE_NAME = 'eq_eqstdcnts';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'R�servations d��quipement' WHERE TABLE_NAME = 'eq_reserve';
UPDATE AFM.AFM_TBLS SET TITLE_IT = 'Spazi serviti dall�apparecchiatura' WHERE TABLE_NAME = 'eq_rm';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Plannings d��quipement', TITLE_NL = 'Productieschema�s' WHERE TABLE_NAME = 'eq_sched';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Table de synchronisation de l��quipement' WHERE TABLE_NAME = 'eq_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Syst�mes d��quipements' WHERE TABLE_NAME = 'eq_system';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������', TITLE_FR = 'Simulations de disposition d��quipement' WHERE TABLE_NAME = 'eq_trial';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Ports d��quipement' WHERE TABLE_NAME = 'eqport';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Standards d��quipement' WHERE TABLE_NAME = 'eqstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Table de synchronisation des standards d��quipement' WHERE TABLE_NAME = 'eqstd_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�infrastructure' WHERE TABLE_NAME = 'facility_type';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-finanal-analyses-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_analyses';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-finanal-analyses-flds-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_analyses_flds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Groupes d�emplacements d�analyse', DEFAULT_VIEW = 'ab-finanal-loc-grp-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_loc_group';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Matrice d�investissements et de d�penses', DEFAULT_VIEW = 'ab-finanal-matrix-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_matrix';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������� - ��??', TITLE_FR = 'Matrice d�investissements et de d�penses - Champs�', DEFAULT_VIEW = 'ab-finanal-matrix-flds-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_matrix_flds';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Param�tres d�analyse financi�re', DEFAULT_VIEW = 'ab-finanal-params-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_params';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Valeurs du r�sum� d�analyse financi�re', DEFAULT_VIEW = 'ab-finanal-sum-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_sum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Valeurs du r�sum� d�analyse du cycle de vie', DEFAULT_VIEW = 'ab-finanal-sum-life-gd-dflt.axvw' WHERE TABLE_NAME = 'finanal_sum_life';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-fl-gd-dflt.axvw' WHERE TABLE_NAME = 'fl';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-fn-gd-dflt.axvw' WHERE TABLE_NAME = 'fn';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Registres d�activit�s du projet BE' WHERE TABLE_NAME = 'gb_cert_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types et valeurs de consommation en carburant d�a�ronefs BE', TITLE_IT = 'Tipi e dati consumo combustibile dell�aereo GB' WHERE TABLE_NAME = 'gb_fp_airc_data';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'GG Sub-regio�s en emissies elektriciteitsnet' WHERE TABLE_NAME = 'gb_fp_egrid_subregions';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types et valeurs des facteurs d��missions BE' WHERE TABLE_NAME = 'gb_fp_emiss_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types et valeurs d��nergie calorifique BE' WHERE TABLE_NAME = 'gb_fp_heat_data';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types et valeurs des facteurs d�oxydation BE' WHERE TABLE_NAME = 'gb_fp_oxid_data';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Geografische bedrijfsregio�s' WHERE TABLE_NAME = 'geo_region';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-gp-gd-dflt.axvw' WHERE TABLE_NAME = 'gp';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-gros-gd-dflt.axvw' WHERE TABLE_NAME = 'gros';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Tableau historique des �l�ments d�actions' WHERE TABLE_NAME = 'hactivity_log';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '��/��������', TITLE_DE = 'Archivierte Ma�nahmenobjekte pro Monat/Woche', TITLE_ES = 'Actividades archivadas por mes/semana', TITLE_FR = 'El�ments d�Action archiv�s par mois/semaine', TITLE_IT = 'Voci azione archiviate per mese/settimana', TITLE_NL = 'Gearchiveerde actiepunten per maand/week' WHERE TABLE_NAME = 'hactivity_logmonth';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Historique des Demandes d�Intervention' WHERE TABLE_NAME = 'hwr';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '��/��������', TITLE_DE = 'Archivierte Arbeitsanforderungen pro Monat/Woche', TITLE_ES = 'Solicitudes de trabajo archivadas por mes/semana', TITLE_FR = 'Demandes d�interventions archiv�es par mois/semaine', TITLE_IT = 'Richieste di lavoro archiviate per mese/settimana', TITLE_NL = 'Gearchiveerde werkaanvragen per maand/week' WHERE TABLE_NAME = 'hwr_month';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Historique DI : Main d�oeuvre' WHERE TABLE_NAME = 'hwrcf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Analyse: Md�O des travaux ferm�s' WHERE TABLE_NAME = 'hwrcfana';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Historique DI : Types d�outils' WHERE TABLE_NAME = 'hwrtt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cha�nes d�Enum�ration - Traduction' WHERE TABLE_NAME = 'lang_enum';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Loueurs d�Actifs' WHERE TABLE_NAME = 'lessor';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Accords d�Imputation des Co�ts' WHERE TABLE_NAME = 'ls_chrgbck_agree';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Profil d�indice de location' WHERE TABLE_NAME = 'ls_index_profile';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Bons de d�mgt : Affectation d��quipement', TITLE_IT = 'App. assegnate all�ordine spost' WHERE TABLE_NAME = 'mo_eq';
UPDATE AFM.AFM_TBLS SET TITLE_IT = 'Arr. assegnati all�ordine spost' WHERE TABLE_NAME = 'mo_fncount';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Verhuisscenario�s' WHERE TABLE_NAME = 'mo_scenario';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Sc�narios de d�m�nagement d�employ�s', TITLE_NL = 'Scenario�s Verhuizing van werknemer' WHERE TABLE_NAME = 'mo_scenario_em';
UPDATE AFM.AFM_TBLS SET TITLE_IT = 'Arr. cat. ass. all�ordine spost' WHERE TABLE_NAME = 'mo_ta';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Historique d�emplacement FDS' WHERE TABLE_NAME = 'msds_h_location';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Ports d��l�ment de r�seau' WHERE TABLE_NAME = 'ndport';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Standards d��l�ment de r�seau' WHERE TABLE_NAME = 'netdevstd';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Unit�s administratives', DEFAULT_VIEW = 'ab-org-gd-dflt.axvw' WHERE TABLE_NAME = 'org';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Groeperingen van PO-schema�s' WHERE TABLE_NAME = 'pmgp';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'PO-schema�s' WHERE TABLE_NAME = 'pms';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Bons d�Achats' WHERE TABLE_NAME = 'po';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Articles des Bons d�Achats' WHERE TABLE_NAME = 'po_line';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Polices d�Assurance' WHERE TABLE_NAME = 'policy';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Portefeuillescenario�s' WHERE TABLE_NAME = 'portfolio_scenario';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Projectprogramma�s' WHERE TABLE_NAME = 'program';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Type programma�s' WHERE TABLE_NAME = 'programtype';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '��', TITLE_FR = 'Projets d�Activit�s' WHERE TABLE_NAME = 'project';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Projectscenario�s' WHERE TABLE_NAME = 'projscns';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Affectations d�emplacement de conformit�' WHERE TABLE_NAME = 'regloc';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Regio�s' WHERE TABLE_NAME = 'regn';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Categorie�n complianceprogramma�s' WHERE TABLE_NAME = 'regprogcat';
UPDATE AFM.AFM_TBLS SET TITLE = 'Compliance Programs and Contracts', TITLE_CH = '�������', TITLE_DE = 'Konformit�tspr�fungsprogramme und -vertr�ge', TITLE_ES = 'Programas y contratos de cumplimiento normativo', TITLE_FR = 'Programmes de conformit� et contrats', TITLE_IT = 'Programmi di conformit� e contratti', TITLE_NL = 'Complianceprogramma�s en contracten' WHERE TABLE_NAME = 'regprogram';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cat�gories d�exigence du programme de conformit�', TITLE_NL = 'Categorie�n van vereisten voor complianceprogramma�s' WHERE TABLE_NAME = 'regreqcat';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�infraction � la r�glementation' WHERE TABLE_NAME = 'regviolationtyp';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '����', TITLE_DE = 'Alle Raumreservierungen', TITLE_ES = 'Todas las reservas de espacio', TITLE_FR = 'Toutes les R�servations de Pi�ces', TITLE_IT = 'Tutte le prenotazioni dei locali', TITLE_NL = 'Alle ruimtereserveringen' WHERE TABLE_NAME = 'resrmview';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������', TITLE_DE = 'Alle Ressourcenreservierungen', TITLE_ES = 'Todas las reservas de recursos', TITLE_FR = 'Toutes les R�servations de Ressources', TITLE_IT = 'Tutte le prenotazioni risorse', TITLE_NL = 'Alle resourcereserveringen' WHERE TABLE_NAME = 'resrsview';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '����', TITLE_DE = 'Alle Reservierungen', TITLE_ES = 'Todas las reservas', TITLE_FR = 'Toutes les R�servations', TITLE_IT = 'Tutte le prenotazioni', TITLE_NL = 'Alle reserveringen' WHERE TABLE_NAME = 'resview';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Table d�activit�s des actifs RFID' WHERE TABLE_NAME = 'rf_activity';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-rm-gd-dflt.axvw' WHERE TABLE_NAME = 'rm';
UPDATE AFM.AFM_TBLS SET TITLE_IT = 'Destinazioni d�uso dei locali' WHERE TABLE_NAME = 'rm_arrange';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�am�nagement de pi�ces', TITLE_IT = 'Tipi di destinazione d�uso del locale' WHERE TABLE_NAME = 'rm_arrange_type';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Affectation d�espace d��quipe' WHERE TABLE_NAME = 'rm_team';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '�������' WHERE TABLE_NAME = 'rm_trial';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-rmcat-gd-dflt.axvw' WHERE TABLE_NAME = 'rmcat';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-rmpct-gd-dflt.axvw' WHERE TABLE_NAME = 'rmpct';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Standards de Pi�ces et d�Employ�s' WHERE TABLE_NAME = 'rmstd_emstd';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-rmtype-gd-dflt.axvw' WHERE TABLE_NAME = 'rmtype';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-approve-detail', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-approve-detail', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-approve-detail', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-approve-detail', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-approve-detail', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-approve-detail' WHERE TABLE_NAME = 'rrappdet';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-cost-detail', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-cost-detail', TITLE_ES = 'Usado para informe de reserva: ab-rr-inf-coste-detalle', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-cost-detail', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-cost-detail', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-cost-detail' WHERE TABLE_NAME = 'rrcostdet';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������: ab-rr-rpt-day-resource-occupation', TITLE_DE = 'Verwendet f�r Reservierungsbericht: ab-rr-rpt-day-resource-occupation', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-day-resource-occupation', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-day-resource-occupation', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-day-resource-occupation', TITLE_NL = 'Gebruikt voor reserveringsrapport: ab-rr-rpt-dag-resource-occupation' WHERE TABLE_NAME = 'rrdayresocc';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������: ab-rr-rpt-day-room-occupation.axvw', TITLE_DE = 'Verwendet f�r Reservierungsbericht: ab-rr-rpt-day-room-occupation.axvw', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-day-room-occupation.axvw', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-day-room-occupation.axvw', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-day-room-occupation.axvw', TITLE_NL = 'Gebruikt voor reserveringsrapport: ab-rr-rpt-day-room-occupation.axvw' WHERE TABLE_NAME = 'rrdayrmocc';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-day-roomres', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-day-roomres', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-cost-detail', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-day-roomres', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-day-roomres', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-day-roomres' WHERE TABLE_NAME = 'rrdayrmres';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-day-roomres-plus', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-day-roomres-plus', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-day-roomres-plus', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-day-roomres-plus', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-day-roomres-plus', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-day-roomres-plus' WHERE TABLE_NAME = 'rrdayrmresplus';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������: ab-rr-rpt-day-resourceres-plus.axvw', TITLE_DE = 'Verwendet f�r Reservierungsbericht: ab-rr-rpt-day-resourceres-plus.axvw', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-day-resourceres-plus.axvw', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-day-resourceres-plus.axvw', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-day-resourceres-plus.axvw', TITLE_NL = 'Gebruikt voor reserveringsrapport: ab-rr-rpt-day-resourceres-plus.axvw' WHERE TABLE_NAME = 'rrdayrresplus';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '���� ab-rr-rpt-month-cost-department', TITLE_DE = 'Verwendet f�r Reservierung ab-rr-rpt-month-cost-department', TITLE_ES = 'Usado para reserva ab-rr-rpt-month-cost-department', TITLE_FR = 'Utilis� pour la R�servation ab-rr-rpt-month-cost-department', TITLE_IT = 'Utilizzato per prenotazioni: ab-rr-rpt-month-cost-department', TITLE_NL = 'Gebruikt voor reservering ab-rr-rpt-month-cost-department' WHERE TABLE_NAME = 'rrmoncostdp';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-month-number-resourceres', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-month-number-resourceres', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-month-number-resourceres', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-month-number-resourceres', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-month-number-resourceres', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-month-number-resourceres' WHERE TABLE_NAME = 'rrmonnumrres';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-month-requestor', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-month-requestor', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-month-requestor', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-month-requestor', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-month-requestor', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-month-requestor' WHERE TABLE_NAME = 'rrmonreq';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-month-resource-reject', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-month-resource-reject', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-month-resource-reject', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-month-resource-reject', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-month-resource-reject', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-month-resource-reject' WHERE TABLE_NAME = 'rrmonresrej';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-month-room-capacity', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-month-room-capacity', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-month-room-capacity', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-month-room-capacity', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-month-room-capacity', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-month-room-capacity' WHERE TABLE_NAME = 'rrmonrmcap';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������: ab-rr-rpt-month-resource-quantity', TITLE_DE = 'Verwendet f�r Reservierungsbericht: ab-rr-rpt-month-resource-quantity', TITLE_ES = 'Se usa para informe de reserva: ab-rr-rpt-month-resource-quantity', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-month-resource-quantity', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-month-resource-quantity', TITLE_NL = 'Gebruikt voor reserveringsrapport: ab-rr-rpt-month-resource-quantity' WHERE TABLE_NAME = 'rrmonthresquant';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-month-use-arrangement', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-month-use-arrangement', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-month-use-arrangement', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-month-use-arrangement', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-month-use-arrangement', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-month-use-arrangement' WHERE TABLE_NAME = 'rrmonusearr';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-ressheet', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-ressheet', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-ressheet', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-ressheet', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-ressheet', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-ressheet' WHERE TABLE_NAME = 'rrressheet';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������:ab-rr-rpt-ressheet', TITLE_DE = 'Verwendet f�r Reservierungsbericht:ab-rr-rpt-ressheet', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-ressheet', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-ressheet', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-ressheet', TITLE_NL = 'Gebruikt voor reserveringsrapport:ab-rr-rpt-ressheet' WHERE TABLE_NAME = 'rrressheetplus';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������: ab-rr-rpt-wr-reservation-trade.axvw', TITLE_DE = 'Verwendet f�r Reservierungsbericht: ab-rr-rpt-wr-reservation-trade.axvw', TITLE_ES = 'Usado para informe de reserva: ab-rr-rpt-wr-reservation-trade.axvw', TITLE_FR = 'Utilis� pour le Rapport de R�servation�: ab-rr-rpt-wr-reservation-trade.axvw', TITLE_IT = 'Utilizzato per report prenotazioni: ab-rr-rpt-wr-reservation-trade.axvw', TITLE_NL = 'Gebruikt voor reserveringsrapport: ab-rr-rpt-wr-reservation-trade.axvw' WHERE TABLE_NAME = 'rrwrrestr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Surfaces d�abords' WHERE TABLE_NAME = 'runoffarea';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�abords' WHERE TABLE_NAME = 'runofftype';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Budgets d�espace' WHERE TABLE_NAME = 'sb';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'El�ments des budgets d�espace' WHERE TABLE_NAME = 'sb_items';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Affinit�s des budgets d�espace' WHERE TABLE_NAME = 'sbaffin';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'P�riodes des budgets d�espace' WHERE TABLE_NAME = 'sbperiods';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'Project Financieringsscenario�s' WHERE TABLE_NAME = 'scenario';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Journal d��tat' WHERE TABLE_NAME = 'status_log';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Relev�s d�affectations' WHERE TABLE_NAME = 'survey';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Membres de l��quipe' WHERE TABLE_NAME = 'team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Association d��quipe' WHERE TABLE_NAME = 'team_assoc';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cat�gorie d��quipe' WHERE TABLE_NAME = 'team_category';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Propri�t�s d��quipe' WHERE TABLE_NAME = 'team_properties';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Types d�outils' WHERE TABLE_NAME = 'tt';
UPDATE AFM.AFM_TBLS SET TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_bl';
UPDATE AFM.AFM_TBLS SET TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_groups';
UPDATE AFM.AFM_TBLS SET TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_groupstoroles';
UPDATE AFM.AFM_TBLS SET TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_groupstousers';
UPDATE AFM.AFM_TBLS SET TITLE_NL = 'VPA-groepen aan juridische ID�s', TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_legal';
UPDATE AFM.AFM_TBLS SET TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_rest';
UPDATE AFM.AFM_TBLS SET TABLE_TYPE = 'PROJECT SECURITY' WHERE TABLE_NAME = 'vpa_site';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Zones de citernes et d�entreposage/stockage des d�chets' WHERE TABLE_NAME = 'waste_areas';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '����', TITLE_DE = 'Alle Arbeitsauftr�ge', TITLE_ES = 'Todas las �rdenes de trabajo', TITLE_FR = 'Tous les bons de travaux', TITLE_IT = 'Tutti gli ordini di lavoro', TITLE_NL = 'Alle werkorders' WHERE TABLE_NAME = 'wohwo';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Cat�gories de travail de l�employ�' WHERE TABLE_NAME = 'work_categories_em';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Equipes d�intervention' WHERE TABLE_NAME = 'work_team';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Demandes d�Intervention' WHERE TABLE_NAME = 'wr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Dem. d�Intervention : Autres Ressources' WHERE TABLE_NAME = 'wr_other';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Table Dem. d�intervention�: autres co�ts' WHERE TABLE_NAME = 'wr_other_sync';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '��������', TITLE_DE = 'Arbeitsanforderung - Bevorstehende Schritte', TITLE_ES = 'Pasos pendientes de solicitud de trabajo', TITLE_FR = 'Etapes en attente de demande d�intervention', TITLE_IT = 'Procedure in sospeso richiesta di lavoro', TITLE_NL = 'Onafgehandelde Stappen Werkaanvraag' WHERE TABLE_NAME = 'wr_step_waiting';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Table Dem. d�intervention�: sync.' WHERE TABLE_NAME = 'wr_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Dem. d�Intervention : Main d�Oeuvre' WHERE TABLE_NAME = 'wrcf';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Sync. de main d�oeuvre pour la demande d�intervention' WHERE TABLE_NAME = 'wrcf_sync';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '������', TITLE_DE = 'Alle Arbeitsanforderungen', TITLE_ES = 'Todas las solicitudes de trabajo', TITLE_FR = 'Toutes les Demandes d�Intervention', TITLE_IT = 'Tutte le richieste di lavoro', TITLE_NL = 'Alle werkaanvragen' WHERE TABLE_NAME = 'wrhwr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Dem. d�Intervention : Pi�ces D�tach�es' WHERE TABLE_NAME = 'wrpt';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Table Dem. d�intervention�: sync. des pi�ces d�t.' WHERE TABLE_NAME = 'wrpt_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Dem. d�Intervention : Outils Requis' WHERE TABLE_NAME = 'wrtl';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Synchronisation des affectations des outils de demande d�intervention' WHERE TABLE_NAME = 'wrtl_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Dem. d�Intervention : M�tiers Requis' WHERE TABLE_NAME = 'wrtr';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Synchronisation des besoins en corps de m�tiers des demandes d�intervention' WHERE TABLE_NAME = 'wrtr_sync';
UPDATE AFM.AFM_TBLS SET TITLE_FR = 'Dem. d�Intervention : Types d�Outils Requis' WHERE TABLE_NAME = 'wrtt';
UPDATE AFM.AFM_TBLS SET TITLE_CH = '����', TITLE_DE = 'Arbeitsanforderungen', TITLE_ES = 'Solicitudes de trabajo', TITLE_FR = 'Demandes d�Intervention', TITLE_IT = 'Richieste di lavoro', TITLE_NL = 'Werkaanvragen' WHERE TABLE_NAME = 'wrview';
UPDATE AFM.AFM_TBLS SET DEFAULT_VIEW = 'ab-zone-gd-dflt.axvw' WHERE TABLE_NAME = 'zone';

UPDATE AFM.AFM_WF_RULES SET XML_RULE_PROPS = '<xml_rule_properties description="Building Ops Part Service"><eventHandlers><eventHandler class="com.archibus.app.bldgops.partinv.service.impl.PartInventoryService" method=""><inputs></inputs></eventHandler></eventHandlers></xml_rule_properties>', DESCRIPTION = 'Building Ops Part Service' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData' AND RULE_ID = 'BldgopsPartInventoryService';
UPDATE AFM.AFM_WF_RULES SET XML_SCHED_PROPS = NULL, RULE_TYPE = 'Message' WHERE ACTIVITY_ID = 'AbCommonResources' AND RULE_ID = 'UpdateAnalysisMetricsJob';

UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Acceptation de l�employ�' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Employee Acceptance';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l�employ�' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'APPROVED' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Approbation de l�ex�cution' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'COMPLETED' AND STEP = 'Completion Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l�employ�' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Approbation de la demande d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'IN PROGRESS' AND STEP = 'Issue Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REJECTED' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer l�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer l�examen' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward Review';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l�employ�' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND STATUS = 'REQUESTED' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Approbation de l�estimation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Estimation Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'A' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Approbation de l�estimation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Estimation Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'AA' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HA' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HL' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'HP' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Demande d�intervention �mise' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'I' AND STEP = 'Work Request Issued';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer l�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward Approval';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer l�examen' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward Review';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l�employ�' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to Employee';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'R' AND STEP = 'Forward to WorkTeam';
UPDATE AFM.AFM_WF_STEPS SET STEP_ES = 'Cambiar', STEP_FR = 'Modifier' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Change';
UPDATE AFM.AFM_WF_STEPS SET STEP_FR = 'Transf�rer � l��quipe d�intervention' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND STATUS = 'Rej' AND STEP = 'Forward to WorkTeam';

UPDATE AFM.FINANAL_ANALYSES SET ANALYSIS_TITLE_FR = 'Immobilisations et projets d�investissement' WHERE ANALYSIS_TITLE = 'Capital and Capital Projects';
UPDATE AFM.FINANAL_ANALYSES SET ANALYSIS_TITLE_FR = 'Analyse unifi�e d�investissements et de d�penses' WHERE ANALYSIS_TITLE = 'Unified Capital and Expense Analysis';

UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_IT = 'Spese, come l�ammortamento e gli interessi, che vengono visualizzate nel conto profitti e perdite non coperte da altre categorie di spesa.' WHERE BOX_ID = 'anlys_roll_additional_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Gestion d�Actifs', BOX_TOOLTIP_FR = 'D�penses d�investissement effectu�es ou pr�vues d��tre effectu�es pendant l�exercice en cours qui ajoutent des actifs au bilan. Ces d�penses n�apparaissent pas sur le compte de r�sultat.' WHERE BOX_ID = 'anlys_roll_assets';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'D�penses pr�vues pour l�exercice en cours afin de financer la dette des actifs immobiliers, d�infrastructure et du patrimoine.  Ces d�penses figurent sur le compte de r�sultat.' WHERE BOX_ID = 'anlys_roll_financing';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Frais d�exploitation pr�vus pour l�exercice en cours qui r�sultent de la possession d�actifs ou d�obligations contractuelles et qui ne peuvent pas �tre modifi�s sans un contrat de vente ou un accord, par exemple la ren�gociation d�un contrat de location.' WHERE BOX_ID = 'anlys_roll_fixed_costs';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Revenus pour l�exercice en cours li�s � l�immobilier, � l�infrastructure et au patrimoine.', BOX_TOOLTIP_IT = 'Entrate quest�anno fiscale relative a immobili, infrastrutture e strutture.' WHERE BOX_ID = 'anlys_roll_income';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�ts pr�vus du fonctionnement de base de l�installation.' WHERE BOX_ID = 'anlys_roll_operations';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�t de la possession et de la location des b�timents, structures et terrains pour l�exercice en cours.' WHERE BOX_ID = 'anlys_roll_ownership';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Co�t total d�occupation', BOX_TOOLTIP_FR = 'Frais d�exploitation relatifs � l�occupation, notamment la maintenance, le gardiennage, la s�curit�, l��nergie et l�eau/gaz/�lec., les imp�ts sur la propri�t�, le loyer et les services directs.', BOX_TOOLTIP_IT = 'Costi operativi relativi all�occupazione, inclusa manutenzione, custodia, sicurezza, energia e utenze, tasse di propriet�, locazione e servizi diretti.' WHERE BOX_ID = 'anlys_roll_total_cost_occup';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Valeur des co�ts totaux d�occupation plus revenus moins co�ts d�amortissement des b�timents, structures, am�liorations d�investissement et �quipement d�investissement.' WHERE BOX_ID = 'anlys_roll_total_cost_own';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Utilitaires', BOX_TITLE_NL = 'Hulpprogramma�s', BOX_TOOLTIP_FR = 'Co�t de l��nergie et des services publics pour l�exercice en cours.', BOX_TOOLTIP_IT = 'Costo dell�energia e delle utenze questo anno fiscale.' WHERE BOX_ID = 'anlys_roll_utilities';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Co�t d�infrastructure', BOX_TOOLTIP_FR = 'Valeur des co�ts de possession TCO plus co�ts de financement, co�t du capital investi, d�infrastructure (y compris l�infrastructure t�l�com) et des services indirects.' WHERE BOX_ID = 'anlys_roll_workpoint_cost';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Appr�ciation estim�e des b�timents ou des propri�t�s comprenant des actifs de terrain pendant l�exercice en cours.' WHERE BOX_ID = 'box_appreciation';
UPDATE AFM.FINANAL_MATRIX SET BOX_SUBTITLE_FR = 'B�timents, structures, projets d�investissement et �quipement', BOX_TOOLTIP_FR = 'Amortissement calcul� bas� sur le type de propri�t� des b�timents, structures, projets d�investissement, ou �quipement individuel.', BOX_TOOLTIP_IT = 'L�ammortamento calcolato basato sul tipo di propriet� delle voci di edifici, strutture, progetti capitale o apparecchiatura capitale.' WHERE BOX_ID = 'box_asset_depreciation';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Valeur des b�timents et de leurs parcelles de terrain au dernier jour de l�exercice pr�c�dent.', BOX_TOOLTIP_IT = 'Il valore degli edifici e dei rispettivi lotti di terreno l�ultimo giorno dell�anno fiscale.' WHERE BOX_ID = 'box_buildings_and_land';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'B�timents, structures et actifs de terrain ajout�s ou pr�vus d��tre ajout�s au portefeuille pendant l�exercice en cours.', BOX_TOOLTIP_IT = 'Asset di edifici, terreni e altri fabbricati aggiunti al portafoglio quest�anno o previsti per l�aggiunta al portafoglio questo anno fiscale.' WHERE BOX_ID = 'box_buildings_and_prop_added';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Equipement (d�investissement)', BOX_TOOLTIP_FR = 'Actifs d��quipement ajout�s au portefeuille jusqu�� la fin de l�exercice pr�c�dent.', BOX_TOOLTIP_IT = 'Asset dell�apparecchiatura capitale aggiunti al portafoglio a partire dalla fine dell�ultimo anno fiscale.' WHERE BOX_ID = 'box_capital_eq';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Equipement (d�investissement, ajout�)', BOX_TOOLTIP_FR = 'Mat�riel d��quipement ajout� ou pr�vu d��tre ajout� au portefeuille pendant l�exercice en cours.', BOX_TOOLTIP_IT = 'Apparecchiatura capitale aggiunta al portafoglio questo anno fiscale o prevista per l�aggiunta al portafoglio questo anno fiscale.' WHERE BOX_ID = 'box_capital_eq_added';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�t d�opportunit� du capital investi cette ann�e, �valu� au moyen du co�t du capital interne. Il s�agit uniquement d�une valeur d�analyse�; elle n�appara�t ni sur le compte de r�sultat, ni au bilan.', BOX_TOOLTIP_IT = 'Il costo opportunit� del capitale impiegato quest�anno, verificato utilizzando il costo interno del capitale. Questo � solo un valore di analisi e non � visualizzato nel conto profitti e perdite.' WHERE BOX_ID = 'box_capital_internal_cost';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Projets (d�investissement)', BOX_TOOLTIP_FR = 'Am�liorations d�investissement (r�sultant de projets d�investissement) ajout�es au portefeuille jusqu�� la fin de l�exercice pr�c�dent.  Comprend uniquement les am�liorations amorties s�par�ment et non incluses dans la valeur de base r�estim�e d�un b�timent ou d�une propri�t�.', BOX_TOOLTIP_IT = 'Miglioramenti del capitale (risultato di progetti capitale) aggiunti al portafoglio a partire dalla fine dell�ultimo anno fiscale.  Include solo i miglioramenti ammortati separatamente e non inclusi in un valore inserito nuovamente nella base di un edificio o propriet�.' WHERE BOX_ID = 'box_capital_projects';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Projets (d�investissement, ajout�s)', BOX_TOOLTIP_FR = 'Am�liorations d�investissement (r�sultant de projets d�investissement) ajout�es ou pr�vues d��tre ajout�es au portefeuille pendant l�exercice en cours.', BOX_TOOLTIP_IT = 'Miglioramenti del capitale (risultato di progetti capitale) aggiunti al portafoglio questo anno fiscale o previsti per l�aggiunta al portafoglio questo anno fiscale.' WHERE BOX_ID = 'box_capital_projects_added';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�t des services de gardiennage et d�enl�vement des d�chets pour l�exercice en cours.', BOX_TOOLTIP_IT = 'Costo dei servizi di custodia e della rimozione dell�immondizia questo anno fiscale.' WHERE BOX_ID = 'box_custodial';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Amortissement des actifs immobiliers, d�infrastructure et du patrimoine pr�vu d�avoir lieu pendant l�exercice en cours.', BOX_TOOLTIP_IT = 'Ammortamento di asset immobiliari, infrastrutture e strutture previsto per quest�anno fiscale.' WHERE BOX_ID = 'box_depreciation';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Revenus pr�vus pour l�exercice en cours provenant des actifs poss�d�s.  Ces revenus proviennent de la sous-location de bureaux, de la location de magasins de premier �tage, des frais de service li�s � l�infrastructure et aux t�l�communications, ou des co�ts MSC sur les propri�t�s poss�d�es.' WHERE BOX_ID = 'box_income';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�t des services non li�s au patrimoine servant � promouvoir les activit�s de l�entreprise. Comprend notamment la gestion des centre de photocopies, soutien administratif, garderie, restauration et centre de sant�.' WHERE BOX_ID = 'box_indirect_services';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Valeur des avoirs fonciers non construits au dernier jour de l�exercice pr�c�dent.  (Les terrains sur lesquels sont construits des b�timents sont inclus dans la cat�gorie B�timents.)', BOX_TOOLTIP_IT = 'Valore delle partecipazioni dei terreni senza edifici l�ultimo giorno dell�ultimo anno fiscale.  I terreni con edifici di propriet� sono inclusi nella categoria degli edifici.', VALUE_CALC = 'SELECT SUM ( finanal_sum.fin_anlys_bookval )'||CHR(13)||CHR(10)
||'FROM finanal_sum, property '||CHR(13)||CHR(10)
||'WHERE ${sql.isInPreviousFiscalYear( ''finanal_sum.fiscal_year'' )} AND '||CHR(13)||CHR(10)
||'finanal_sum.pr_id=property.pr_id AND '||CHR(13)||CHR(10)
||'finanal_sum.asset_type=''Property'' AND '||CHR(13)||CHR(10)
||'property.property_type=''Land''', VALUE_CALC_MARKET = 'SELECT SUM ( finanal_sum.fin_anlys_marketval )'||CHR(13)||CHR(10)
||'FROM finanal_sum, property '||CHR(13)||CHR(10)
||'WHERE ${sql.isInPreviousFiscalYear( ''finanal_sum.fiscal_year'' )} AND '||CHR(13)||CHR(10)
||'finanal_sum.pr_id=property.pr_id AND '||CHR(13)||CHR(10)
||'finanal_sum.asset_type=''Property'' AND '||CHR(13)||CHR(10)
||'property.property_type=''Land''' WHERE BOX_ID = 'box_land_parcels';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�t de la maintenance pr�ventive et curative des b�timents, terrains et �quipements pour l�exercice en cours.' WHERE BOX_ID = 'box_maintenance';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Int�r�t hypoth�caire relatif aux actifs pay�s pendant l�exercice en cours.' WHERE BOX_ID = 'box_mortgage_interest';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Paiements du capital d�hypoth�que', BOX_TOOLTIP_FR = 'Capital hypoth�caire relatif aux actifs pay�s pendant l�exercice en cours. Cette valeur est inscrite directement au bilan et n�appara�t pas sur le compte de r�sultat.' WHERE BOX_ID = 'box_mortgage_principal_payments';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Autres frais d�exploitation relatifs au patrimoine pour l�exercice en cours.', BOX_TOOLTIP_IT = 'Altri costi operativi relativi alle strutture quest�anno fiscale.' WHERE BOX_ID = 'box_other';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'D�penses d�investissement pour les projets et l��quipement d�investissement pendant l�exercice en cours. Cette valeur est inscrite directement au bilan et n�appara�t pas sur le compte de r�sultat.' WHERE BOX_ID = 'box_outlays_capital_proj_expense';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Total de la dette depuis le d�but de l�exercice fiscal jusqu�� la fin de l�obligation.', BOX_TOOLTIP_IT = 'Somma del debito dall�inizio dell�anno fiscale alla fine dell�obbligo.' WHERE BOX_ID = 'box_outstanding_debt_on_assets';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_CH = '���������', BOX_TOOLTIP_FR = 'Total des engagements de location hors bilan depuis le d�but de l�exercice fiscal jusqu�� la fin de l�obligation.', BOX_TOOLTIP_IT = 'Somma degli obblighi di locazione non presenti nel bilancio dall�inizio dell�anno fiscale alla fine dell�obbligo.' WHERE BOX_ID = 'box_outstanding_ls_commitments';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Imp�ts sur la propri�t� pay�s pendant l�exercice en cours.' WHERE BOX_ID = 'box_property_tax';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Loyer pay�s aux loueurs pendant l�exercice en cours.' WHERE BOX_ID = 'box_rent_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Co�t des syst�mes de s�curit�, du personnel et des services externalis�s pour l�exercice en cours.' WHERE BOX_ID = 'box_security';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Valeur amortie des structures au dernier jour de l�exercice pr�c�dent.', BOX_TOOLTIP_IT = 'Valore ammortizzato delle strutture a partire dall�ultimo giorno dell�ultimo anno fiscale.', VALUE_CALC = 'SELECT SUM (finanal_sum.fin_anlys_bookval) '||CHR(13)||CHR(10)
||'FROM finanal_sum, property '||CHR(13)||CHR(10)
||'WHERE ${sql.isInPreviousFiscalYear( ''finanal_sum.fiscal_year'' )} AND '||CHR(13)||CHR(10)
||'finanal_sum.pr_id=property.pr_id AND '||CHR(13)||CHR(10)
||'finanal_sum.asset_type=''Property'' AND '||CHR(13)||CHR(10)
||'property.property_type=''Structure''', VALUE_CALC_MARKET = 'SELECT SUM (finanal_sum.fin_anlys_marketval) '||CHR(13)||CHR(10)
||'FROM finanal_sum, property '||CHR(13)||CHR(10)
||'WHERE ${sql.isInPreviousFiscalYear( ''finanal_sum.fiscal_year'' )} AND '||CHR(13)||CHR(10)
||'finanal_sum.pr_id=property.pr_id AND '||CHR(13)||CHR(10)
||'finanal_sum.asset_type=''Property'' AND '||CHR(13)||CHR(10)
||'property.property_type=''Structure''' WHERE BOX_ID = 'box_structures-m';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_FR = 'Utilitaires', BOX_TITLE_NL = 'Hulpprogramma�s', BOX_TOOLTIP_FR = 'Co�t de l��nergie et des services publics comme l�eau et le gaz pour l�exercice en cours.' WHERE BOX_ID = 'box_utilities';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Actifs immobiliers, d�infrastructure et du patrimoine figurant ou pr�vus de figurer au bilan � la fin de l�exercice en cours. (Certaines autres cat�gories de co�t sont �galement pr�sent�es ci-dessous, mais ne sont pas incluses dans la valeur.)' WHERE BOX_ID = 'column_asset_net_worth';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'D�penses d�immobilier, d�infrastructure et de patrimoine pr�vues de figurer sur le compte de r�sultat cette ann�e.   (Certaines autres cat�gories de co�t sont �galement pr�sent�es ci-dessous, mais ne sont pas incluses dans la valeur.)', BOX_TOOLTIP_IT = 'Le spese immobiliari, per le infrastrutture e le strutture, che verranno visualizzate nel conto profitti e perdite quest�anno.   Alcune altre categorie di costo sono presentate in aggiunta di seguito, ma non sono sommate in questo valore.' WHERE BOX_ID = 'column_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Autres d�penses relatives � l�immobilier, � l�infrastructure et au patrimoine.', BOX_TOOLTIP_IT = 'Altre spese quest�anno fiscale relative a immobili, infrastrutture e strutture.' WHERE BOX_ID = 'fin_rollup_add_expenses';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Les valeurs d�analyse ne figurent pas sur les bilans financiers.' WHERE BOX_ID = 'fin_rollup_analysis';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Passif figurant sur le bilan.  Cette valeur inclut la valeur restante des futurs paiements hypoth�caires s�ils sont pay�s dans leur totalit� et des contrats de location-acquisition s�ils sont pay�s dans leur totalit�.' WHERE BOX_ID = 'fin_rollup_bal_sheet_liability';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Valeur des actifs au dernier jour de l�exercice pr�c�dent.', BOX_TOOLTIP_IT = 'Il valore degli asset a partire dall�ultimo giorno dell�ultimo anno fiscale.' WHERE BOX_ID = 'fin_rollup_endbalance_last_fyr';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Frais d�exploitation relatifs � l�immobilier, � l�infrastructure et au patrimoine.', BOX_TOOLTIP_IT = 'Spese operative quest�anno fiscale relative a immobili, infrastrutture e strutture.' WHERE BOX_ID = 'fin_rollup_expenses_operating';
UPDATE AFM.FINANAL_MATRIX SET BOX_TOOLTIP_FR = 'Revenus pour l�exercice en cours li�s � l�immobilier, � l�infrastructure et au patrimoine.', BOX_TOOLTIP_IT = 'Entrate quest�anno fiscale relative a immobili, infrastrutture e strutture.' WHERE BOX_ID = 'fin_rollup_income';
UPDATE AFM.FINANAL_MATRIX SET BOX_TITLE_IT = 'Proiezioni - quest�anno fiscale', BOX_TOOLTIP_FR = 'Modifications de la valeur d�actif ayant eu lieu ou pr�vue d�avoir lieu pendant l�exercice en cours.', BOX_TOOLTIP_IT = 'Cambiamenti del valore dell�asset avvenuti questo anno fiscale o previsti questo anno fiscale.' WHERE BOX_ID = 'fin_rollup_projections_this_fyr';

UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_IT = 'Predefinita' WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_IT = 'Predefinita' WHERE ACTIVITY_TYPE = 'SERVICE DESK - FURNITURE' AND ORDERING_SEQ = 1 AND PRIORITY = 2;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_IT = 'Predefinita' WHERE ACTIVITY_TYPE = 'SERVICE DESK - GROUP MOVE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_IT = 'Predefinita' WHERE ACTIVITY_TYPE = 'SERVICE DESK - HOTELING' AND ORDERING_SEQ = 1 AND PRIORITY = 1;
UPDATE AFM.HELPDESK_SLA_RESPONSE SET PRIORITY_LABEL_IT = 'Predefinita' WHERE ACTIVITY_TYPE = 'SERVICE DESK - INDIVIDUAL MOVE' AND ORDERING_SEQ = 1 AND PRIORITY = 1;

UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Les employ�s suivants ont refus� les �tapes d�acceptation de cette demande�:   <#list steps as step><#if step.step_type.value== ''acceptance''>${step.em_id} :: ${step.comments}</#if></#list>' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'ACCEPTANCE_STEP' AND MESSAGE_ID = 'ESCALATION_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'Your request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} has been approved', MESSAGE_TEXT_CH = '��: ${activity_log.activity_type}  ID: ${activity_log.activity_log_id} ��������', MESSAGE_TEXT_DE = 'Ihre Anforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} wurde genehmigt', MESSAGE_TEXT_ES = 'Su solicitud de tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} ha sido aprobada', MESSAGE_TEXT_FR = 'Votre demande de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} a �t� approuv�e', MESSAGE_TEXT_IT = 'La richiesta per il tipo di attivit�: ${activity_log.activity_type} con ID: ${activity_log.activity_log_id} � stato approvata.', MESSAGE_TEXT_NL = 'Uw aanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} is goedgekeurd' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'Your request with ID: ${activity_log.activity_log_id} has been approved', MESSAGE_TEXT_CH = 'ID: ${activity_log.activity_log_id}��������', MESSAGE_TEXT_DE = 'Ihre Anforderung mit der ID ${activity_log.activity_log_id} wurde genehmigt', MESSAGE_TEXT_ES = 'Su solicitud con ID ${activity_log.activity_log_id} ha sido aprobada', MESSAGE_TEXT_FR = 'Votre demande avec l�identifiant ${activity_log.activity_log_id} a �t� approuv�e', MESSAGE_TEXT_IT = 'La richiesta con ID: ${activity_log.activity_log_id} � stata approvata.', MESSAGE_TEXT_NL = 'Uw aanvraag met ID ${activity_log.activity_log_id} is goedgekeurd' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_SUBJECT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'Your service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} has been updated. Click the following URL to check the details: ${link}', MESSAGE_TEXT_CH = '�����: ${activity_log.activity_type}  ID�: ${activity_log.activity_log_id} ������. ��������s: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} wurde aktualisiert. Klicken Sie auf die folgende URL, um die Details zu pr�fen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} ha sido actualizada. Haga clic en la siguiente URL para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'Votre demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} a �t� mise � jour. Cliquez sur l�URL suivante pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID: ${activity_log.activity_log_id} � stata aggiornata. Fare clic sull�URL seguente per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'Uw onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} is bijgewerkt. Klik op de volgende URL om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The status of the service request with ID: ${activity_log.activity_log_id} has been changed to the following status: ${activity_log.status.text}', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id} �����������: ${activity_log.status.text}', MESSAGE_TEXT_DE = 'Status der Serviceanforderung mit der ID ${activity_log.activity_log_id} wurde in ${activity_log.status.text} ge�ndert.', MESSAGE_TEXT_ES = 'El estado de la solicitud de servicio con ID ${activity_log.activity_log_id} se ha cambiado al siguiente estado: ${activity_log.status.text}', MESSAGE_TEXT_FR = 'Le statut de la demande de service avec l�identifiant ${activity_log.activity_log_id} est d�sormais ${activity_log.status.text}', MESSAGE_TEXT_IT = 'Lo stato della richiesta di servizio con ID: ${activity_log.activity_log_id} � stato cambiato nel seguente stato: ${activity_log.status.text}.', MESSAGE_TEXT_NL = 'De status van de onderhoudsaanvraag met ID ${activity_log.activity_log_id} is gewijzigd in de status ${activity_log.status.text}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request with ID: ${activity_log.activity_log_id} has already been accepted by  <#if step.em_id??>${step.em_id}<#else><#if step.vn_id??>${step.vn_id}<#else>${step.user_name}</#if></#if>. Your acceptance is no longer required.', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id}�������� <#if step.em_id??>${step.em_id}<#else><#if step.vn_id??>${step.vn_id}<#else>${step.user_name}</#if></#if>. ��������.', MESSAGE_TEXT_DE = 'Die Serviceanforderung mit der ID ${activity_log.activity_log_id} wurde bereits abgenommen von <#if step.em_id??>${step.em_id}<#else><#if step.vn_id??>${step.vn_id}<#else>${step.user_name}</#if></#if>. Ihre Abnahme ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = 'La solicitud de servicio con ID ${activity_log.activity_log_id} ya ha sido aprobada por <#if step.em_id??>${step.em_id}<#else><#if step.vn_id??>${step.vn_id}<#else>${step.user_name}</#if></#if>. Ya no se requiere su aceptaci�n.', MESSAGE_TEXT_FR = 'La demande de service avec l�identifiant ${activity_log.activity_log_id} a d�j� �t� accept�e par <#if step.em_id??>${step.em_id}<#else><#if step.vn_id??>${step.vn_id}<#else>${step.user_name}</#if></#if>. Votre acceptation n�est plus n�cessaire.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag met ID ${activity_log.activity_log_id} is al geaccepteerd door  <#if step.em_id??>${step.em_id}<#else><#if step.vn_id??>${step.vn_id}<#else>${step.user_name}</#if></#if>. Uw acceptatie is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Annulation de la demande d�acceptation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} needs your acceptance. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id}���������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} muss von Ihnen abgenommen werden. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Es necesario que acepte la solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id}. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} a besoin de votre acceptation. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID ${activity_log.activity_log_id} richiede l�accettazione dell�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} moet door u worden geaccepteerd. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�acceptation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} needs the acceptance. You are assigned as a substitute for acceptances. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id} �������. ������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} muss abgenommen werden. Sie wurden als Stellvertreter f�r Abnahmen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} necesita ser aceptada. Se le ha asignado como sustituto/a para aceptaciones. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} a besoin d��tre accept�e. Vous �tes affect� en tant que rempla�ant pour les acceptations. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID ${activity_log.activity_log_id} richiede l�accettazione. L�utente � stato nominato come sostituto per le accettazioni. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} moet worden geaccepteerd. U bent aangewezen als vervanger voor acceptaties. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�acceptation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the service request with ID: ${activity_log.activity_log_id} to the following status: ${step.step_status_result.text}. Your approval is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${activity_log.activity_log_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Serviceanforderung mit der ID ${activity_log.activity_log_id} in ${step.step_status_result.text} ge�ndert. Ihre Genehmigung ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de servicio con ID: ${activity_log.activity_log_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su aprobaci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande de service avec l�identifiant ${activity_log.activity_log_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre approbation n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di servizio con ID: ${activity_log.activity_log_id} nel seguente stato: ${step.step_status_result.text}. Non � richiesta altra approvazione.', MESSAGE_TEXT_NL = '${step.user_name} heeft de onderhoudsaanvraag met ID ${activity_log.activity_log_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw goedkeuring is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Annulation de la demande d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} needs this action: ${step.step}. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id}�������: ${step.step}. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} erfordert folgende Ma�nahme: ${step.step}. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} necesita esta acci�n: ${step.step}. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} n�cessite l�action ${step.step}. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID ${activity_log.activity_log_id} richiede questa azione: ${step.step}. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} heeft deze actie nodig: ${step.step}. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID:${activity_log.activity_log_id} needs this action: ${step.step}. You are assigned as a substitute for approvals. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�:${activity_log.activity_log_id} �������: ${step.step}. ������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} erfordert folgende Ma�nahme: ${step.step}. Sie wurden als Stellvertreter f�r Genehmigungen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} necesita esta acci�n: ${step.step}. Se le ha asignado como sustituto/a para aprobaciones. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} n�cessite l�action ${step.step}. Vous �tes affect� en tant que rempla�ant pour les approbations. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID:${activity_log.activity_log_id} richiede questa azione: ${step.step}. L�utente � stato nominato come sostituto per le approvazioni. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} heeft deze actie nodig: ${step.step}. U bent aangewezen als vervanger voor goedkeuringen. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une demande d�intervention vous a �t� affect�e. Cliquez sur le lien suivant pour afficher les d�tails�: {0}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La demande d�intervention vous a �t� affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une demande d�intervention a �t� affect�e � ${activity_log.supervisor}. Vous �tes affect� en tant que superviseur rempla�ant. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'Una richiesta di lavoro � stata assegnata a ${activity_log.supervisor}. L�utente � stato assegnato come sostituto supervisore. Fare clic sul collegamento riportato di seguito per visualizzare i dettagli: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�intervention qui vous ont �t� affect�e en tant que rempla�ant', MESSAGE_TEXT_IT = 'Richiesta di lavoro assegnata all�utente come sostituto' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ASSIGN_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the service request with ID: ${activity_log.activity_log_id} to the following status: ${step.step_status_result.text}. Your intervention is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${activity_log.activity_log_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Serviceanforderung mit der ID ${activity_log.activity_log_id} in ${step.step_status_result.text} ge�ndert. Ihr Eingreifen ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de servicio con ID: ${activity_log.activity_log_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su intervenci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande de service avec l�identifiant ${activity_log.activity_log_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre intervention n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di servizio con ID: ${activity_log.activity_log_id} nel seguente stato: ${step.step_status_result.text}. L�intervento dell�utente non � pi� richiesto.', MESSAGE_TEXT_NL = '${step.user_name} heeft de onderhoudsaanvraag met ID ${activity_log.activity_log_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw interventie is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type and problem type: ${activity_log.activity_type} - ${activity_log.prob_type} with ID: ${activity_log.activity_log_id} needs to be dispatched. '||CHR(13)||CHR(10)
||'Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '��������: ${activity_log.activity_type} - ${activity_log.prob_type} ID�: ${activity_log.activity_log_id} ��������. '||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp und den Problemtyp ${activity_log.activity_type} - ${activity_log.prob_type} mit der ID ${activity_log.activity_log_id} muss versendet werden. '||CHR(13)||CHR(10)
||'Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Hay que enviar la solicitud de servicio para tipo de actividad y tipo de problema ${activity_log.activity_type} - ${activity_log.prob_type} con ID ${activity_log.activity_log_id}. '||CHR(13)||CHR(10)
||'Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} et de type de probl�me ${activity_log.prob_type} avec l�identifiant ${activity_log.activity_log_id} a besoin d��tre distribu�e. '||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = '� necessario inviare la richiesta di servizio per tipo di attivit� e tipo di problema: ${activity_log.activity_type} - ${activity_log.prob_type} con ID: ${activity_log.activity_log_id}. '||CHR(13)||CHR(10)
||'Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor activiteits- en probleemtype ${activity_log.activity_type} - ${activity_log.prob_type} met ID ${activity_log.activity_log_id} moet worden verzonden. '||CHR(13)||CHR(10)
||'Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type and problem type: ${activity_log.activity_type} - ${activity_log.prob_type} with ID: ${activity_log.activity_log_id} needs to be dispatched. You are assigned as a substitute for dispatching.'||CHR(13)||CHR(10)
||'Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '��������: ${activity_log.activity_type} - ${activity_log.prob_type}  ID�: ${activity_log.activity_log_id}��������. �����.'||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp und den Problemtyp ${activity_log.activity_type} - ${activity_log.prob_type} mit der ID ${activity_log.activity_log_id} muss versendet werden. Sie wurden als Stellvertreter f�r das Versenden bestimmt.'||CHR(13)||CHR(10)
||'Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Hay que enviar la solicitud de servicio para tipo de actividad y tipo de problema ${activity_log.activity_type} - ${activity_log.prob_type} con ID ${activity_log.activity_log_id}. Usted est� asignado/a como sustituto/a para los env�os.'||CHR(13)||CHR(10)
||'Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} et de type de probl�me ${activity_log.prob_type} avec l�identifiant ${activity_log.activity_log_id} a besoin d��tre distribu�e. Vous �tre affect� en tant que rempla�ant pour la distribution.'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = '� necessario inviare la richiesta di servizio per tipo di attivit� e tipo di problema: ${activity_log.activity_type} - ${activity_log.prob_type} con ID: ${activity_log.activity_log_id}. L�utente � stato nominato sostituto per la spedizione.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento seguente per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor activiteits- en probleemtype ${activity_log.activity_type} - ${activity_log.prob_type} met ID ${activity_log.activity_log_id} moet worden verzonden. U bent aangewezen als vervanger voor verzending.'||CHR(13)||CHR(10)
||'Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The step: ${step.step} occurred for service request with ID: ${activity_log.activity_log_id}. Click the following link to view the details: ${link} <#if step.comments??>${step.comments}</#if>', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id}�������${step.step}. ��������: ${link} <#if step.comments??>${step.comments}</#if>', MESSAGE_TEXT_DE = 'Schritt ${step.step} ist f�r die Serviceanforderung mit der ID ${activity_log.activity_log_id} aufgetreten. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link} <#if step.comments??>${step.comments}</#if>', MESSAGE_TEXT_ES = 'Se ha producido el paso ${step.step} para la solicitud de servicio con ID ${activity_log.activity_log_id}. Haga clic en el siguiente enlace para ver los detalles: ${link} <#if step.comments??>${step.comments}</#if>', MESSAGE_TEXT_FR = 'L��tape ${step.step} a �t� effectu�e pour la demande de service avec l�identifiant ${activity_log.activity_log_id}. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link} <#if step.comments??>${step.comments}</#if>', MESSAGE_TEXT_IT = 'Si � verificato il passaggio: ${step.step} per la richiesta di servizio con ID: ${activity_log.activity_log_id}. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link} <#if step.comments??>${step.comments}</#if>.', MESSAGE_TEXT_NL = 'De stap ${step.step} is opgetreden voor onderhoudsaanvraag met ID ${activity_log.activity_log_id}. Klik op de volgende link om de details weer te geven: ${link} <#if step.comments??>${step.comments}</#if>' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_ESCALATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The status of the service request with ID: ${activity_log.activity_log_id} has been changed to the following status: ${activity_log.status.text}.    Request Details:  Request type: ${activity_log.activity_type}  <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> Requested by: ${activity_log.requestor}  Description: ${activity_log.description}', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id} �����������: ${activity_log.status.text}.   ����:  ����: ${activity_log.activity_type}  <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> ���: ${activity_log.requestor}  ��: ${activity_log.description}', MESSAGE_TEXT_DE = 'Status der Serviceanforderung mit der ID ${activity_log.activity_log_id} wurde in ${activity_log.status.text} ge�ndert.    Anforderungsdetails:  Anforderungstyp: ${activity_log.activity_type}  <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> Angefordert von: ${activity_log.requestor}  Beschreibung: ${activity_log.description}', MESSAGE_TEXT_ES = 'El estado de la solicitud de servicio con ID ${activity_log.activity_log_id} se ha cambiado al siguiente estado: ${activity_log.status.text}.    Detalles de solicitud:  Tipo de solicitud: ${activity_log.activity_type}  <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> Solicitada por: ${activity_log.requestor}  Descripci�n: ${activity_log.description}', MESSAGE_TEXT_FR = 'Le statut de la demande de service avec l�identifiant ${activity_log.activity_log_id} est d�sormais ${activity_log.status.text}.    D�tails de la demande�:  Type de demande�: ${activity_log.activity_type} <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> Demand� par�: ${activity_log.requestor} Description�: ${activity_log.description}',
 MESSAGE_TEXT_IT = 'Lo stato della richiesta di servizio con ID: ${activity_log.activity_log_id} � stato cambiato nel seguente stato: ${activity_log.status.text}.    Dettagli richiesta:  Tipo di richiesta: ${activity_log.activity_type}  <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> Richiesta da: ${activity_log.requestor} Descrizione: ${activity_log.description}', MESSAGE_TEXT_NL = 'De status van de onderhoudsaanvraag met ID ${activity_log.activity_log_id} is gewijzigd in de status ${activity_log.status.text}.    Aanvraagdetails:  Type aanvraag: ${activity_log.activity_type}  <#if activity_log.prob_type??>- ${activity_log.prob_type}</#if> Aangevraagd door: ${activity_log.requestor} Beschrijving: ${activity_log.description}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_NOTIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the service request with ID: ${activity_log.activity_log_id} to the following status: ${step.step_status_result.text}. Your intervention is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${activity_log.activity_log_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Serviceanforderung mit der ID ${activity_log.activity_log_id} in ${step.step_status_result.text} ge�ndert. Ihr Eingreifen ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de servicio con ID: ${activity_log.activity_log_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su intervenci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande de service avec l�identifiant ${activity_log.activity_log_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre intervention n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di servizio con ID: ${activity_log.activity_log_id} nel seguente stato: ${step.step_status_result.text}. L�intervento dell�utente non � pi� richiesto.', MESSAGE_TEXT_NL = '${step.user_name} heeft de onderhoudsaanvraag met ID ${activity_log.activity_log_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw interventie is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Annulation de la demande d��dition et d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} is waiting for you to be edited and approved. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id} ������������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} muss noch von Ihnen bearbeitet und genehmigt werden. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Hay una solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} esperando a que la edite y apruebe. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} est en attente de modification et d�approbation de votre part. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID: ${activity_log.activity_log_id} � in attesa di modifica e approvazione da parte dell�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} wacht op bewerking en goedkeuring door u. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} is waiting to be edited and approved. You are assigned as a substitute for editing and approving requests. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id} ������������. ������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} muss noch bearbeitet und genehmigt werden. Sie wurden als Stellvertreter f�r die Bearbeitung und Genehmigung bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Hay una solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} esperando a ser editada y aprobada. Usted est� asignado/a como sustituto/a para editar y aprobar solicitudes. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} est en attente de modification et d�approbation. Vous �tes affect� en tant que rempla�ant pour les demandes d��dition et d�approbation. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID: ${activity_log.activity_log_id} � in attesa di modifica e approvazione. L�utente � stato nominato come sostituto per la modifica e l�approvazione delle richieste. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} wacht op bewerking en goedkeuring. U bent aangewezen als vervanger voor het bewerken en goedkeuren van aanvragen. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request with ID: ${activity_log.activity_log_id} needs your satisfaction survey. Click the following link the view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id} ����������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung mit der ID ${activity_log.activity_log_id} erfordert Ihre Zufriedenheitsumfrage. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio con ID ${activity_log.activity_log_id} necesita su encuesta de satisfacci�n. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service avec l�identifiant ${activity_log.activity_log_id} n�cessite votre enqu�te de satisfaction. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio con ID: ${activity_log.activity_log_id} richiede il rilievo di soddisfazione dell�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag met ID ${activity_log.activity_log_id} heeft uw tevredenheidssurvey nodig. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d��tude de satisfaction' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request with ID: ${activity_log.activity_log_id} needs a satisfaction survey. You are assigned as a substitute for surveys. Click the following link the view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id}����������. ������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung mit der ID ${activity_log.activity_log_id} erfordert eine Zufriedenheitsumfrage. Sie wurden als Stellvertreter f�r Umfragen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio con ID ${activity_log.activity_log_id} necesita una encuesta de satisfacci�n. Se le ha asignado como sustituto/a para encuestas. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service avec l�identifiant ${activity_log.activity_log_id} n�cessite une enqu�te de satisfaction. Vous �tes affect� en tant que rempla�ant pour les enqu�tes. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio con ID: ${activity_log.activity_log_id} richiede un sondaggio sulla soddisfazione. L�utente � stato nominato come sostituto per i rilievi. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d��tude de satisfaction' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request with ID: ${activity_log.activity_log_id} needs your verification. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id}�������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung mit der ID ${activity_log.activity_log_id} erfordert Ihre Pr�fung. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio con ID ${activity_log.activity_log_id} necesita su verificaci�n. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service avec l�identifiant ${activity_log.activity_log_id} n�cessite votre v�rification. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio con ID: ${activity_log.activity_log_id} richiede la verifica da parte dell�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag met ID ${activity_log.activity_log_id} heeft uw verificatie nodig. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request with ID: ${activity_log.activity_log_id} needs a verification. You are assigned as a substitute for verifications. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${activity_log.activity_log_id} �������. ������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung mit der ID ${activity_log.activity_log_id} erfordert eine Pr�fung. Sie wurden als Stellvertreter f�r Pr�fungen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio con ID ${activity_log.activity_log_id} necesita una verificaci�n. Se le ha asignado como sustituto/a para verificaciones. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service avec l�identifiant ${activity_log.activity_log_id} n�cessite une v�rification. Vous �tes affect� en tant que rempla�ant pour les v�rifications. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio con ID: ${activity_log.activity_log_id} richiede una verifica. L�utente � stato nominato come sostituto per le verifiche. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Fase terminata dal responsabile informazioni struttura dopo l�inoltro della richiesta' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR' AND MESSAGE_ID = 'COMMENTS';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} has been assigned to you. Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id}��������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} wurde Ihnen zugewiesen. Klicken Sie auf den folgenden Link, um die Details zu pr�fen: ${link}', MESSAGE_TEXT_ES = 'Le han asignado la solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id}. Haga clic en el siguiente enlace para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} vous a �t� affect�e. Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID ${activity_log.activity_log_id} � stata assegnata all�utente corrente. Fare clic sul seguente collegamento per verificare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} is toegewezen aan u. Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR' AND MESSAGE_ID = 'NOTIFY_ASSIGNEE_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The service request for activity type: ${activity_log.activity_type} with ID: ${activity_log.activity_log_id} has been assigned to ${activity_log.assigned_to}. You are assigned as a substitute. Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = '���: ${activity_log.activity_type} ID�: ${activity_log.activity_log_id} ������� ${activity_log.assigned_to}. �����. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Serviceanforderung f�r den Aktivit�tstyp ${activity_log.activity_type} mit der ID ${activity_log.activity_log_id} wurde ${activity_log.assigned_to} zugewiesen. Sie wurden als Stellvertreter bestimmt. Klicken Sie auf den folgenden Link, um die Details zu pr�fen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de servicio para el tipo de actividad ${activity_log.activity_type} con ID ${activity_log.activity_log_id} ha sido asignada a ${activity_log.assigned_to}. Se le ha asignado como sustituto/a. Haga clic en el siguiente enlace para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande de service de type d�activit� ${activity_log.activity_type} avec l�identifiant ${activity_log.activity_log_id} a �t� affect�e � ${activity_log.assigned_to}. Vous �tes affect� en tant que rempla�ant. Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di servizio per il tipo di attivit�: ${activity_log.activity_type} con ID ${activity_log.activity_log_id} � stata assegnata a ${activity_log.assigned_to}. L�utente � stato nominato come sostituto. Fare clic sul seguente collegamento per verificare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De onderhoudsaanvraag voor type activiteit ${activity_log.activity_type} met ID ${activity_log.activity_log_id} is toegewezen aan ${activity_log.assigned_to}. U bent aangewezen als vervanger. Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk' AND REFERENCED_BY = 'UPDATEREQUEST_WFR_SUBSTITUTE' AND MESSAGE_ID = 'NOTIFY_ASSIGNEE_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with problem type: ${wr.prob_type} and ID: ${wr.wr_id} has been approved', MESSAGE_TEXT_CH = '�����: ${wr.prob_type} ID�: ${wr.wr_id}��������', MESSAGE_TEXT_FR = 'La demande d�intervention avec le type de probl�me ${wr.prob_type} et l�identifiant ${wr.wr_id} a �t� approuv�e.', MESSAGE_TEXT_IT = 'La richiesta di lavoro con tipo di problema: ${wr.prob_type} e ID: ${wr.wr_id} � stata approvata.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'Your work request with ID: ${wr.wr_id} has been approved', MESSAGE_TEXT_CH = 'ID: ${wr.wr_id} ��������', MESSAGE_TEXT_FR = 'Votre demande d�intervention avec l�identifiant ${wr.wr_id} a �t� approuv�e', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata approvata.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'REQUEST APPROVED_APPROVED_SUBJECT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with problem type: ${wr.prob_type} and ID: ${wr.wr_id} has been issued by <#if wr.supervisor??>supervisor ${wr.supervisor} <#else> work team ${wr.work_team_id}</#if>', MESSAGE_TEXT_CH = '�����: ${wr.prob_type} ID�: ${wr.wr_id} ����� <#if wr.supervisor??>�� ${wr.supervisor} <#else> ���${wr.work_team_id}</#if>', MESSAGE_TEXT_ES = 'La solicitud de trabajo con tipo de problema ${wr.prob_type} e ID ${wr.wr_id} ha sido emitida por el <#if wr.supervisor??>supervisor ${wr.supervisor} <#else> equipo de trabajo ${wr.work_team_id}</#if>', MESSAGE_TEXT_FR = 'La demande d�intervention avec le type de probl�me ${wr.prob_type} et l�identifiant ${wr.wr_id} a �t� �mise par <#if wr.supervisor??>un responsable ${wr.supervisor} <#else> une �quipe d�intervention ${wr.work_team_id}</#if>', MESSAGE_TEXT_IT = 'La richiesta di lavoro con tipo di problema: ${wr.prob_type} e ID: ${wr.wr_id} � stata emessa da <#if wr.supervisor??>supervisore ${wr.supervisor} <#else> team di lavoro ${wr.work_team_id}</#if>.', MESSAGE_TEXT_NL = 'De werkaanvraag met probleemtype ${wr.prob_type} en ID ${wr.wr_id} is uitgegeven door <#if wr.supervisor??>supervisor ${wr.supervisor} <#else> werkteam ${wr.work_team_id}</#if>' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'WORK REQUEST ISSUED_I_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been issued', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id}������', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} ha sido emitida', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� �mise', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata emessa.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} is uitgegeven' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFICATION_STEP' AND MESSAGE_ID = 'WORK REQUEST ISSUED_I_SUBJECT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been assigned to ${cf_id}. You are assigned as a substitute. Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ������� ${cf_id}. �����. ��������: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} se ha asignado a ${cf_id}. Se le ha asignado como sustituto/a. Haga clic en el siguiente enlace para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� affect�e � ${cf_id}. Vous �tes affect� en tant que rempla�ant. Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata assegnata a ${cf_id}. L�utente � stato nominato come sostituto. Fare clic sul seguente collegamento per verificare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Nouvelle demande d�intervention affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been assigned to you. Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ��������. ��������: ${link}', MESSAGE_TEXT_ES = 'Se le ha asignado la solicitud de trabajo con ID ${wr.wr_id}. Haga clic en el siguiente enlace para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} vous a �t� affect�e. Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata assegnata all�utente corrente. Fare clic sul seguente collegamento per verificare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} is toegewezen aan u. Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Nouvelle demande d�intervention affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_CF_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request for problem type: ${wr.prob_type} with ID: ${wr.wr_id} has been assigned to ${cf_id}, but his/her email address could not be found.  Go to ${link} to update this request.', MESSAGE_TEXT_CH = '�����: ${wr.prob_type}  ID�: ${wr.wr_id}�������  ${cf_id}, ���/���������.  � ${link} ����.', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung f�r den Problemtyp ${wr.prob_type} mit der ID ${wr.wr_id} wurde ${cf_id} zugewiesen, aber die E-Mail-Adresse konnte nicht gefunden werden.  Aktualisieren Sie diese Anforderung unter ${link}.', MESSAGE_TEXT_ES = 'Se ha asignado una solicitud de trabajo para el tipo de problema ${wr.prob_type} con ID ${wr.wr_id} a ${cf_id}, pero no se encontr� su direcci�n electr�nica.  Vaya a ${link} para actualizar esta solicitud.', MESSAGE_TEXT_FR = 'La demande d�intervention de type de probl�me ${wr.prob_type} avec l�identifiant ${wr.wr_id} a �t� affect�e � ${cf_id}, mais son adresse �lectronique est introuvable.  Acc�dez � ${link} pour mettre � jour cette demande.', MESSAGE_TEXT_IT = 'La richiesta di lavoro per il tipo di problema: ${wr.prob_type} con ID: ${wr.wr_id} � stata assegnata a ${cf_id}, tuttavia l�indirizzo e-mail non � stato trovato.  Accedere a ${link} per aggiornare la richiesta.', MESSAGE_TEXT_NL = 'De werkaanvraag voor probleemtype ${wr.prob_type} met ID ${wr.wr_id} is toegewezen aan  ${cf_id}, maar zijn/haar e-mailadres is niet gevonden.  Ga naar ${link} om deze aanvraag bij te werken.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_CF_WFR' AND MESSAGE_ID = 'NOTIFY_MGR_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'Your work request with ID: ${wr.wr_id} has been updated. Click the following URL to check the details: ${link}', MESSAGE_TEXT_CH = 'ID: ${wr.wr_id} ��������. ��������: ${link}', MESSAGE_TEXT_FR = 'Votre demande d�intervention avec l�identifiant ${wr.wr_id} a �t� mise � jour. Cliquez sur l�URL suivante pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata aggiornata. Fare clic sull�URL seguente per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'Uw werkaanvraag met ID ${wr.wr_id} is bijgewerkt. Klik op de volgende URL om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The status of the work request with ID: ${wr.wr_id} has been changed to the following status: ${wr.status.text}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �����������: ${wr.status.text}', MESSAGE_TEXT_DE = 'Status der Arbeitsanforderung mit der ID ${wr.wr_id} wurde in ${wr.status.text} ge�ndert.', MESSAGE_TEXT_ES = 'El estado de la solicitud de trabajo con ID ${wr.wr_id} se ha cambiado al siguiente estado: ${wr.status.text}', MESSAGE_TEXT_FR = 'Le statut de la demande d�intervention avec l�identifiant ${wr.wr_id} est d�sormais ${wr.status.text}', MESSAGE_TEXT_IT = 'Lo stato della richiesta di lavoro con ID: ${wr.wr_id} � stato cambiato nel seguente stato: ${wr.status.text}.', MESSAGE_TEXT_NL = 'De status van de werkaanvraag met ID ${wr.wr_id} is gewijzigd in de status ${wr.status.text}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_REQUESTOR_WFR' AND MESSAGE_ID = 'NOTIFY_REQUESTOR_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'A new work order with ID: ${wo.wo_id} has been assigned to you. Click the following link to manage it: ${link}', MESSAGE_TEXT_CH = '���: ${wo.wo_id} �����. ����������: ${link}', MESSAGE_TEXT_DE = 'Ihnen wurde ein neuer Arbeitsauftrag mit der ID ${wo.wo_id} zugewiesen. Klicken Sie zur Verwaltung auf den folgenden Link: ${link}', MESSAGE_TEXT_ES = 'Se le ha asignado una nueva orden de trabajo con ID ${wo.wo_id}. Haga clic en el siguiente enlace para gestionarla: ${link}', MESSAGE_TEXT_FR = 'Un nouveau bon de travaux avec l�identifiant ${wo.wo_id} vous a �t� affect�. Cliquez sur le lien suivant pour le g�rer�: ${link}', MESSAGE_TEXT_IT = 'Un nuovo ordine di lavoro con ID: ${wo.wo_id} � stato assegnato all�utente corrente. Per gestirla, fare clic sul seguente collegamento: ${link}.', MESSAGE_TEXT_NL = 'Er is een nieuwe werkorder met ID ${wo.wo_id} aan u toegewezen. Klik op de volgende link om deze te beheren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_AA_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_AA_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been assigned to you. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ��������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} wurde Ihnen zugewiesen. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Se le ha asignado la solicitud de trabajo con ID ${wr.wr_id}. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} vous a �t� affect�e. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata assegnata all�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} is toegewezen aan u. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Nouvelle demande d�intervention affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been assigned to you. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ��������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} wurde Ihnen zugewiesen. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Se le ha asignado la solicitud de trabajo con ID ${wr.wr_id}. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} vous a �t� affect�e. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata assegnata all�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} is toegewezen aan u. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Nouvelle demande d�intervention affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'An issued work order with ID: ${wo.wo_id} has been assigned to you. Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = '�������: ${wo.wo_id} ������. ������������: ${link}+D183', MESSAGE_TEXT_DE = 'Ihnen wurde ein ausgestellter Arbeitsauftrag mit der ID ${wo.wo_id} zugewiesen. Klicken Sie auf den folgenden Link, um die Details zu pr�fen: ${link}', MESSAGE_TEXT_ES = 'Se le ha asignado una orden de trabajo emitida con ID ${wo.wo_id}. Haga clic en el siguiente enlace para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'Un bon de travaux �mis avec l�identifiant ${wo.wo_id} vous a �t� affect�. Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'Un ordine di lavoro emesso con ID: ${wo.wo_id} � stato assegnato all�utente corrente. Fare clic sul seguente collegamento per verificare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'Een uitgegeven werkorder met ID ${wo.wo_id} is aan u toegewezen. Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_I_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_I_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'A work order with ID: ${wo.wo_id} has been assigned to you. Click the following link to check the details and issue it: ${link}', MESSAGE_TEXT_CH = '���: ${wo.wo_id}�����. ��������������: ${link}', MESSAGE_TEXT_DE = 'Ihnen wurde ein Arbeitsauftrag mit der ID ${wo.wo_id} zugewiesen. Klicken Sie zum Anzeigen der Details und zum Erstellen auf den folgenden Link: ${link}', MESSAGE_TEXT_ES = 'Se le ha asignado una orden de trabajo con ID ${wo.wo_id}. Haga clic en el siguiente enlace para revisar los detalles y emitirla: ${link}', MESSAGE_TEXT_FR = 'Un bon de travaux avec l�identifiant ${wo.wo_id} vous a �t� affect�. Cliquez sur le lien suivant pour en v�rifier les d�tails et l��mettre�: ${link}', MESSAGE_TEXT_IT = 'Un ordine di lavoro con ID: ${wo.wo_id} � stato assegnato all�utente corrente. Fare clic sul collegamento di seguito per visualizzare i dettagli ed emetterla: ${link}.', MESSAGE_TEXT_NL = 'Er is een werkorder met ID ${wo.wo_id} aan u toegewezen. Klik op de volgende link om de details te controleren en uit te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SCH_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_SCH_TEXT';

UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'A new work order with ID: ${wo.wo_id} has been assigned to ${supervisor}. You are assigned as a substitute. Click the following link to manage it: ${link}', MESSAGE_TEXT_CH = '���: ${wo.wo_id} ���� ${supervisor}. �����. ����������: ${link}', MESSAGE_TEXT_DE = '${supervisor} wurde ein neuer Arbeitsauftrag mit der ID ${wo.wo_id} zugewiesen. Sie wurden als Stellvertreter bestimmt. Klicken Sie zur Verwaltung auf den folgenden Link: ${link}', MESSAGE_TEXT_ES = 'Se ha asignado una nueva orden de trabajo con ID ${wo.wo_id} a ${supervisor}. Se le ha asignado como sustituto/a. Haga clic en el siguiente enlace para gestionarla: ${link}', MESSAGE_TEXT_FR = 'Un nouveau bon de travaux avec l�identifiant ${wo.wo_id} a �t� affect� � ${supervisor}. Vous �tes affect� en tant que rempla�ant. Cliquez sur le lien suivant pour le g�rer�: ${link}', MESSAGE_TEXT_IT = '� stato assegnato un nuovo ordine di lavoro con ID: ${wo.wo_id} a ${supervisor}. L�utente � stato nominato come sostituto. Per gestirla, fare clic sul seguente collegamento: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_AA_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_AA_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been assigned to ${wr.supervisor}. You are assigned as a substitute. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������  ${wr.supervisor}. �����. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} wurde ${wr.supervisor} zugewiesen. Sie wurden als Stellvertreter bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} se ha asignado a ${wr.supervisor}. Se le ha asignado como sustituto/a. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� affect�e � ${wr.supervisor}. Vous �tes affect� en tant que rempla�ant. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata assegnata a ${wr.supervisor}. L�utente � stato nominato come sostituto. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Nouvelle demande d�intervention affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_APPROVED_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_APPROVED_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been assigned to ${supervisor}. You are assigned as a substitute. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������  ${supervisor}. �����. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} wurde ${supervisor} zugewiesen. Sie wurden als Stellvertreter bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} se ha asignado a ${supervisor}. Se le ha asignado como sustituto/a. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� affect�e � ${supervisor}. Vous �tes affect� en tant que rempla�ant. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata assegnata a ${supervisor}. L�utente � stato nominato come sostituto. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Nouvelle demande d�intervention affect�e' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_A_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_A_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'An issued work order with ID: ${wo.wo_id} has been assigned to ${supervisor}. You are assigned as a substitute.  Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = '�������: ${wo.wo_id}����� ${supervisor}. �����.  �����������: ${link}', MESSAGE_TEXT_DE = '${supervisor} wurde ein ausgestellter Arbeitsauftrag mit der ID ${wo.wo_id} zugewiesen. Sie wurden als Stellvertreter bestimmt.  Klicken Sie auf den folgenden Link, um die Details zu pr�fen: ${link}', MESSAGE_TEXT_ES = 'Se ha asignado una orden de trabajo emitida con ID ${wo.wo_id} a ${supervisor}. Se le ha asignado como sustituto/a.  Haga clic en el siguiente enlace para revisar los detalles: ${link}', MESSAGE_TEXT_FR = 'Un bon de travaux �mis avec l�identifiant ${wo.wo_id} a �t� affect� � ${supervisor}. Vous �tes affect� en tant que rempla�ant.  Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = '� stato assegnato un ordine di lavoro emesso con ID: ${wo.wo_id} a ${supervisor}. L�utente � stato nominato come sostituto.  Fare clic sul seguente collegamento per verificare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'Een uitgegeven werkorder met ID ${wo.wo_id} is toegewezen aan ${supervisor}. U bent aangewezen als vervanger.  Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_I_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_I_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'A work order with ID: ${wo.wo_id} has been assigned to  ${supervisor}. You are assigned as a substitute. Click the following link to check the details and issue it: ${link}', MESSAGE_TEXT_CH = '���ID: ${wo.wo_id} ���� ${supervisor}. �����. ��������������: ${link}', MESSAGE_TEXT_DE = '${supervisor} wurde ein Arbeitsauftrag mit der ID ${wo.wo_id} zugewiesen. Sie wurden als Stellvertreter bestimmt. Klicken Sie zum Anzeigen der Details und zum Erstellen auf den folgenden Link: ${link}', MESSAGE_TEXT_ES = 'Se ha asignado una orden de trabajo con ID ${wo.wo_id} a ${supervisor}. Se le ha asignado como sustituto/a. Haga clic en el siguiente enlace para revisar los detalles y emitirla: ${link}', MESSAGE_TEXT_FR = 'Un bon de travaux avec l�identifiant ${wo.wo_id} a �t� affect� � ${supervisor}. Vous �tes affect� en tant que rempla�ant. Cliquez sur le lien suivant pour en v�rifier les d�tails et l��mettre�: ${link}', MESSAGE_TEXT_IT = 'Un ordine di lavoro con ID: ${wo.wo_id} � stato assegnato a ${supervisor}. L�utente � stato nominato come sostituto. Fare clic sul collegamento di seguito per visualizzare i dettagli ed emetterla: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_SCH_WFR' AND MESSAGE_ID = 'NOTIFY_SUPERVISOR_SCH_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been verified and is returned as incomplete with comments ${step.comments}. You are assigned as substitute of the supervisor ${supervisor} for this work request.'||CHR(13)||CHR(10)
||'Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ������������� �� ${step.comments}. ���� ${supervisor} ����.'||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} wurde gepr�ft und mit Kommentaren ${step.comments} als unvollst�ndig zur�ckgegeben. Sie wurden als Stellvertreter des Vorarbeiters ${supervisor} f�r diese Arbeitsanforderung bestimmt.'||CHR(13)||CHR(10)
||'Klicken Sie auf den folgenden Link, um die Details zu �berpr�fen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} ha sido verificada y se devuelve como incompleta con comentarios ${step.comments}. Usted est� asignado/a como sustituto/a del supervisor ${supervisor} para esta solicitud de trabajo.'||CHR(13)||CHR(10)
||'Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� v�rifi�e et renvoy�e car incompl�te avec les commentaires ${step.comments}. Vous �tes affect� en tant que rempla�ant du superviseur ${supervisor} pour cette demande d�intervention.'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata verificata e viene restituita come incompleta con commenti ${step.comments}. L�utente � stato assegnato come sostituto del supervisore ${supervisor} per questa richiesta di lavoro.'||CHR(13)||CHR(10)
||'Fare clic sul seguente collegamento per controllare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} is geverifieerd en wordt geretourneerd als onvolledig met opmerkingen ${step.comments}. U bent toegewezen als vervanger van supervisor ${supervisor} voor deze werkaanvraag.'||CHR(13)||CHR(10)
||'Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�intervention renvoy�e car incompl�te' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_SUBSTITUTE_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'A work request for problem type: ${wr.prob_type} with ID: ${wr.wr_id} has been assigned to  <#if wr.supervisor??>${wr.supervisor} <#else>${wr.work_team_id}</#if>, but the email address could not be found.   Go to ${link} to update this request.', MESSAGE_TEXT_CH = '���������: ${wr.prob_type} with ID: ${wr.wr_id} ���� <#if wr.supervisor??>${wr.supervisor} <#else>${wr.work_team_id}</#if>, ������������.   ��� ${link}�����.', MESSAGE_TEXT_DE = '<#if wr.supervisor??>${wr.supervisor} <#else>${wr.work_team_id}</#if> wurde eine Arbeitsanforderung f�r den Problemtyp ${wr.prob_type} mit der ID ${wr.wr_id} zugewiesen, aber die E-Mail-Adresse konnte nicht gefunden werden.   Aktualisieren Sie diese Anforderung unter ${link}.', MESSAGE_TEXT_ES = 'Se ha asignado una solicitud de trabajo para el tipo de problema ${wr.prob_type} con ID ${wr.wr_id} a <#if wr.supervisor??>${wr.supervisor} <#else>${wr.work_team_id}</#if>, pero no se encontr� la direcci�n electr�nica.   Vaya a ${link} para actualizar esta solicitud.', MESSAGE_TEXT_FR = 'Une demande d�intervention pour le type de probl�me ${wr.prob_type} avec l�identifiant ${wr.wr_id} a �t� affect�e � <#if wr.supervisor??>${wr.supervisor} <#else>${wr.work_team_id}</#if>, mais l�adresse �lectronique est introuvable.   Acc�dez � ${link} pour mettre � jour cette demande.', MESSAGE_TEXT_IT = 'Una richiesta di lavoro per il tipo di problema: ${wr.prob_type} con ID: ${wr.wr_id} � stata assegnata a <#if wr.supervisor??> ${wr.supervisor}<#else>${wr.work_team_id}</#if>, tuttavia l�indirizzo e-mail non � stato trovato.   Accedere a ${link} per aggiornare la richiesta.', MESSAGE_TEXT_NL = 'Er is een werkaanvraag voor probleemtype ${wr.prob_type} met ID ${wr.wr_id} toegewezen aan <#if wr.supervisor??>${wr.supervisor} <#else>${wr.work_team_id}</#if>, maar het e-mailadres is niet gevonden.   Ga naar ${link} om deze aanvraag bij te werken.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_MGR_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been verified by ${step.user_name} and is returned as incomplete with comments ${step.comments}.'||CHR(13)||CHR(10)
||'Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id}���� ${step.user_name} ��������� �� ${step.comments}.'||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} wurde von ${step.user_name} gepr�ft und mit Kommentaren ${step.comments} als unvollst�ndig zur�ckgegeben.'||CHR(13)||CHR(10)
||'Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} ha sido verificada por ${step.user_name} y se devuelve como incompleta con comentarios ${step.comments}.'||CHR(13)||CHR(10)
||'Haga clic en el siguiente enlace para verificar los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� v�rifi�e par ${step.user_name} et est renvoy�e car incompl�te, avec les commentaires ${step.comments}.'||CHR(13)||CHR(10)
||'Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata verificata da ${step.user_name} e viene restituita come incompleta con commenti ${step.comments}.'||CHR(13)||CHR(10)
||'Fare clic sul seguente collegamento per controllare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�intervention renvoy�e car incompl�te' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'NOTIFY_SUPERVISOR_WFR' AND MESSAGE_ID = 'NOTIFY_VERIFICATION_RETURN_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�acceptation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une demande n�cessite l�acceptation. Vous �tes affect� en tant que rempla�ant pour les acceptations. Cliquez sur le lien ci-dessous pour consulter les d�tails�:{0}', MESSAGE_TEXT_IT = 'Una richiesta richiede l�accettazione. L�utente � stato assegnato come sostituto per le accettazioni. Fare clic sul collegamento riportato di seguito per visualizzare i dettagli:{0}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�acceptation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ACCEPTANCE_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the work request with ID: ${wr.wr_id} to the following status: ${step.step_status_result.text}. Your approval is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${wr.wr_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Arbeitsanforderung mit der ID ${wr.wr_id} in ${step.step_status_result.text} ge�ndert. Ihre Genehmigung ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de trabajo con ID: ${wr.wr_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su aprobaci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande d�intervention avec l�identifiant ${wr.wr_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre approbation n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di lavoro con ID: ${wr.wr_id} nel seguente stato: ${step.step_status_result.text}. Non � richiesta altra approvazione.', MESSAGE_TEXT_NL = '${step.user_name} heeft de werkaanvraag met ID ${wr.wr_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw goedkeuring is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Annulation de la demande d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs this action: ${step.step}. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������: ${step.step}. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert die Ma�nahme ${step.step}. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita esta acci�n: ${step.step}. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite l�action ${step.step}. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede questa azione: ${step.step}. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} heeft de actie ${step.step} nodig. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs this action: ${step.step}. You are assigned as a substitute for approvals. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������: ${step.step}. �������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert die Ma�nahme ${step.step}. Sie wurden als Stellvertreter f�r Genehmigungen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita esta acci�n: ${step.step}. Se le ha asignado como sustituto/a para aprobaciones. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite l�action ${step.step}. Vous �tes affect� en tant que rempla�ant pour les approbations. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede questa azione: ${step.step}. L�utente � stato nominato come sostituto per le approvazioni. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} heeft de actie ${step.step} nodig. U bent aangewezen als vervanger voor goedkeuringen. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_APPROVAL_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the work request with ID: ${wr.wr_id} to the following status: ${step.step_status_result.text}. Your intervention is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${wr.wr_id} to the following status: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Arbeitsanforderung mit der ID ${wr.wr_id} in ${step.step_status_result.text} ge�ndert. Ihr Eingreifen ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de trabajo con ID: ${wr.wr_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su intervenci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande d�intervention avec l�identifiant ${wr.wr_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre intervention n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di lavoro con ID: ${wr.wr_id} nel seguente stato: ${step.step_status_result.text}. L�intervento dell�utente non � pi� richiesto.', MESSAGE_TEXT_NL = '${step.user_name} heeft de werkaanvraag met ID ${wr.wr_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw interventie is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request for problem type: ${wr.prob_type} with ID: ${wr.wr_id} needs to be dispatched. '||CHR(13)||CHR(10)
||'Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '�����: ${wr.prob_type}  ID�: ${wr.wr_id}��� ����. '||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung f�r den Problemtyp ${wr.prob_type} mit der ID ${wr.wr_id} muss versendet werden. '||CHR(13)||CHR(10)
||'Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo para el tipo de problema ${wr.prob_type} con ID ${wr.wr_id} tiene que ser despachada. '||CHR(13)||CHR(10)
||'Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention de type de probl�me ${wr.prob_type} avec l�identifiant ${wr.wr_id} a besoin d��tre distribu�e. '||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro per il tipo di problema ${wr.prob_type} con ID: ${wr.wr_id} deve essere spedita. '||CHR(13)||CHR(10)
||'Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag voor probleemtype ${wr.prob_type} met ID ${wr.wr_id} moet worden verzonden. '||CHR(13)||CHR(10)
||'Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request for problem type: ${wr.prob_type} with ID: ${wr.wr_id} needs to be dispatched. You are assigned as a substitute for dispatching.'||CHR(13)||CHR(10)
||'Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = '�����: ${wr.prob_type}  ID�: ${wr.wr_id}��� ����.  �����.'||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung f�r den Problemtyp ${wr.prob_type} mit der ID ${wr.wr_id} muss versendet werden. Sie wurden als Stellvertreter f�r das Versenden bestimmt.'||CHR(13)||CHR(10)
||'Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo para el tipo de problema ${wr.prob_type} con ID ${wr.wr_id} tiene que ser despachada. Usted est� asignado/a como sustituto/a para los env�os.'||CHR(13)||CHR(10)
||'Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention de type de probl�me ${wr.prob_type} avec l�identifiant ${wr.wr_id} a besoin d��tre distribu�e. Vous �tre affect� en tant que rempla�ant pour la distribution.'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro per il tipo di problema ${wr.prob_type} con ID: ${wr.wr_id} deve essere spedita. L�utente � stato nominato sostituto per la spedizione.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento seguente per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag voor probleemtype ${wr.prob_type} met ID ${wr.wr_id} moet worden verzonden. U bent aangewezen als vervanger voor verzending.'||CHR(13)||CHR(10)
||'Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_DISPATCH_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the service request with ID: ${activity_log.activity_log_id} to the following status: ${step.step_status_result.text}. Your estimation is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${activity_log.activity_log_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Serviceanforderung mit der ID ${activity_log.activity_log_id} in ${step.step_status_result.text} ge�ndert. Ihre Sch�tzung ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de servicio con ID: ${activity_log.activity_log_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su estimaci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande de service avec l�identifiant ${activity_log.activity_log_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre estimation n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di servizio con ID: ${activity_log.activity_log_id} nel seguente stato: ${step.step_status_result.text}. Non � richiesta altra stima.', MESSAGE_TEXT_NL = '${step.user_name} heeft de onderhoudsaanvraag met ID ${activity_log.activity_log_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw schatting is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Annulation de la demande d�estimation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs this action: ${step.step}.  Click on the following link to view the details:  ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������: ${step.step}.  ��������:  ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert die Ma�nahme ${step.step}.  Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita esta acci�n: ${step.step}.  Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite l�action ${step.step}.  Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede questa azione: ${step.step}.  Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} heeft de actie ${step.step} nodig.  Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�estimation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs this action: ${step.step}.  You are assigned as a substitute for estimations. Click on the following link to view the details:  ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������: ${step.step}.  �������. ��������:  ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert die Ma�nahme ${step.step}.  Sie wurden als Stellvertreter f�r Sch�tzungen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita esta acci�n: ${step.step}.  Se le ha asignado como sustituto/a para estimaciones. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite l�action ${step.step}.  Vous �tes affect� en tant que rempla�ant pour les estimations. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede questa azione: ${step.step}.  L�utente � stato nominato come sostituto per le stime. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} heeft de actie ${step.step} nodig.  U bent aangewezen als vervanger voor schattingen. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�estimation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_ESTIMATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The status of the work request with ID: ${wr.wr_id} has been changed to the following status: ${wr.status.text}.  Request Details: Problem type: ${wr.prob_type} Requested by: ${wr.requestor} Description: ${wr.description}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �����������: ${wr.status.text}.  ����: Problem type: ${wr.prob_type} ���: ${wr.requestor} ��: ${wr.description}', MESSAGE_TEXT_DE = 'Status der Arbeitsanforderung mit der ID ${wr.wr_id} wurde in ${wr.status.text} ge�ndert.  Anforderungsdetails: Problemtyp: ${wr.prob_type} Angefordert von: ${wr.requestor} Beschreibung: ${wr.description}', MESSAGE_TEXT_ES = 'El estado de la solicitud de trabajo con ID ${wr.wr_id} se ha cambiado al siguiente estado: ${wr.status.text}.  Detalles de solicitud: Tipo de problema: ${wr.prob_type} Solicitado por: Descripci�n de ${wr.requestor}: ${wr.description}', MESSAGE_TEXT_FR = 'Le statut de la demande d�intervention avec l�identifiant ${wr.wr_id} est d�sormais ${wr.status.text}.  D�tails de la demande�: Type de probl�me�: ${wr.prob_type} Demand� par�: ${wr.requestor} Description�: ${wr.description}', MESSAGE_TEXT_IT = 'Lo stato della richiesta di lavoro con ID: ${wr.wr_id} � stato cambiato nel seguente stato: ${wr.status.text}.  Dettagli richiesta: Tipo di problema: ${wr.prob_type} Richiesto da: ${wr.requestor} Descrizione: ${wr.description}', MESSAGE_TEXT_NL = 'De status van de werkaanvraag met ID ${wr.wr_id} is gewijzigd in de status ${wr.status.text}.  Aanvraagdetails: Probleemtype: ${wr.prob_type} Aangevraagd door: ${wr.requestor} Beschrijving: ${wr.description}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_NOTIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} has been returned by ${wrcf.cf_id} with comments ${wrcf.comments}.'||CHR(13)||CHR(10)
||'Click the following link to check the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ���� ${wrcf.cf_id} �� �� ${wrcf.comments}.'||CHR(13)||CHR(10)
||'��������: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} a �t� retourn�e par ${wrcf.cf_id} avec les commentaires ${wrcf.comments}.'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour v�rifier les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � stata restituita da ${wrcf.cf_id} con commenti ${wrcf.comments}.'||CHR(13)||CHR(10)
||'Fare clic sul seguente collegamento per verificare il dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} is geretourneerd door ${wrcf.cf_id} met opmerkingen ${wrcf.comments}. '||CHR(13)||CHR(10)
||'Klik op de volgende link om de details te controleren: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_RETURN_STEP' AND MESSAGE_ID = 'NOTIFY_CRAFTSPERSON_RETURN_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d�intervention retourn�e par technicien' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_RETURN_STEP' AND MESSAGE_ID = 'NOTIFY_CRAFTSPERSON_RETURN_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the work request with ID: ${wr.wr_id} to the following status: ${step.step_status_result.text}. Your intervention is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${wr.wr_id} to the following status: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Arbeitsanforderung mit der ID ${wr.wr_id} in ${step.step_status_result.text} ge�ndert. Ihr Eingreifen ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de trabajo con ID: ${wr.wr_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su intervenci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande d�intervention avec l�identifiant ${wr.wr_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre intervention n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di lavoro con ID: ${wr.wr_id} nel seguente stato: ${step.step_status_result.text}. L�intervento dell�utente non � pi� richiesto.', MESSAGE_TEXT_NL = '${step.user_name} heeft de werkaanvraag met ID ${wr.wr_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw interventie is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Annulation de la demande d��dition et d�approbation' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} is waiting for you to be edited and approved. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ������������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} muss noch von Ihnen bearbeitet und genehmigt werden. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} est en attente de modification et d�approbation de votre part. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � in attesa di modifica e approvazione. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} is waiting to be edited and approved. You are assigned as a substitute for editing and approving requests. Click on the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id}������������. �����. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit ID ${wr.wr_id} muss noch bearbeitet und genehmigt werden. Sie wurden als Stellvertreter f�r die Bearbeitung und Genehmigung bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'Hay una solicitud de trabajo con ID ${wr.wr_id} esperando que se edite y apruebe. Usted est� asignado/a como sustituto/a para editar y aprobar solicitudes. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} est en attente de modification et d�approbation. Vous �tes affect� en tant que rempla�ant pour les demandes d��dition et d�approbation. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} � in attesa di essere modificate e approvata. L�utente � stato nominato come sostituto per la modifica e l�approvazione delle richieste. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_REVIEW_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the work request with ID: ${wr.wr_id} to the following status: ${step.step_status_result.text}. Your scheduling is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${wr.wr_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Arbeitsanforderung mit der ID ${wr.wr_id} in ${step.step_status_result.text} ge�ndert. Ihre Planung ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de trabajo con ID: ${wr.wr_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su programaci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande d�intervention avec l�identifiant ${wr.wr_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre programmation n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di lavoro con ID: ${wr.wr_id} nel seguente stato: ${step.step_status_result.text}. La pianificazione dell�utente non � pi� richiesta.', MESSAGE_TEXT_NL = '${step.user_name} heeft de werkaanvraag met ID ${wr.wr_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw planning is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs this action: ${step.step}.  Click on the following link to view the details:  ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������: ${step.step}.  ��������:  ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert die Ma�nahme ${step.step}.  Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita esta acci�n: ${step.step}.  Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite l�action ${step.step}.  Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede questa azione: ${step.step}.  Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} heeft de actie ${step.step} nodig.  Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs this action: ${step.step}. You are assigned as a substitute for scheduling work requests.  Click on the following link to view the details:  ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������: ${step.step}. �������.  ��������:  ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert die Ma�nahme ${step.step}. Sie wurden als Stellvertreter f�r Arbeitsanforderungen bestimmt.  Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita esta acci�n: ${step.step}. Se le ha asignado como sustituto/a para programar solicitudes de trabajo.  Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite l�action ${step.step}. Vous �tes affect� en tant que rempla�ant pour la planification des demandes d�intervention.  Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede questa azione: ${step.step}. L�utente � stato nominato come sostituto per la programmazione delle richieste di lavoro.  Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'De werkaanvraag met ID ${wr.wr_id} heeft de actie ${step.step} nodig. U bent aangewezen als vervanger voor het plannen van werkaanvragen.  Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SCHEDULING_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs your satisfaction survey. Click the following link the view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ������������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert Ihre Zufriedenheitsumfrage. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite votre enqu�te de satisfaction. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede il sondaggio sulla soddisfazione dell�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'Voor de werkaanvraag met ID ${wr.wr_id} is uw tevredenheidssurvey nodig. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d��tude de satisfaction' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs a satisfaction survey. You are assigned as a substitute for surveys. Click the following link the view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ����������. �����. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert eine Zufriedenheitsumfrage. Sie wurden als Stellvertreter f�r Umfragen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita una encuesta de satisfacci�n. Se le ha asignado como sustituto/a para encuestas. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite une enqu�te de satisfaction. Vous �tes affect� en tant que rempla�ant pour les enqu�tes. Cliquez sur le lien suivant pour afficher les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede un sondaggio sulla soddisfazione. L�utente � stato nominato come sostituto per i rilievi. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Demande d��tude de satisfaction' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_SURVEY_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = '${step.user_name} changed the work request with ID: ${wr.wr_id} to the following status: ${step.step_status_result.text}. Your verification is no longer required.', MESSAGE_TEXT_CH = '${step.user_name} ��������: ${wr.wr_id} ������: ${step.step_status_result.text}. ��������.', MESSAGE_TEXT_DE = '${step.user_name} hat den Status der Arbeitsanforderung mit der ID ${wr.wr_id} in ${step.step_status_result.text} ge�ndert. Ihre Pr�fung ist nicht mehr erforderlich.', MESSAGE_TEXT_ES = '${step.user_name} ha cambiado la solicitud de trabajo con ID: ${wr.wr_id} al siguiente estado: ${step.step_status_result.text}. Ya no se requiere su verificaci�n.', MESSAGE_TEXT_FR = '${step.user_name} a modifi� la demande d�intervention avec l�identifiant ${wr.wr_id}. Son statut est d�sormais ${step.step_status_result.text}. Votre v�rification n�est plus n�cessaire.', MESSAGE_TEXT_IT = '${step.user_name} modificata la richiesta di lavoro con ID: ${wr.wr_id} nel seguente stato: ${step.step_status_result.text}. La verifica dell�utente non � pi� richiesta.', MESSAGE_TEXT_NL = '${step.user_name} heeft de werkaanvraag met ID ${wr.wr_id} gewijzigd in de volgende status: ${step.step_status_result.text}. Uw verificatie is niet meer nodig.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_CANCEL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs your verification. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} ���������. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert Ihre Pr�fung. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita su verificaci�n. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite votre v�rification. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede la verifica da parte dell�utente corrente. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.', MESSAGE_TEXT_NL = 'Voor de werkaanvraag met ID ${wr.wr_id} is uw verificatie nodig. Klik op de volgende link om de details weer te geven: ${link}' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT = 'The work request with ID: ${wr.wr_id} needs the verification. You are assigned as a substitute for verifications. Click the following link to view the details: ${link}', MESSAGE_TEXT_CH = 'ID�: ${wr.wr_id} �������. �����. ��������: ${link}', MESSAGE_TEXT_DE = 'Die Arbeitsanforderung mit der ID ${wr.wr_id} erfordert eine Pr�fung. Sie wurden als Stellvertreter f�r Pr�fungen bestimmt. Klicken Sie auf den folgenden Link, um die Details anzuzeigen: ${link}', MESSAGE_TEXT_ES = 'La solicitud de trabajo con ID ${wr.wr_id} necesita verificaci�n. Se le ha asignado como sustituto/a para verificaciones. Haga clic en el siguiente enlace para ver los detalles: ${link}', MESSAGE_TEXT_FR = 'La demande d�intervention avec l�identifiant ${wr.wr_id} n�cessite la v�rification. Vous �tes affect� en tant que rempla�ant pour les v�rifications. Cliquez sur le lien ci-dessous pour consulter les d�tails�: ${link}', MESSAGE_TEXT_IT = 'La richiesta di lavoro con ID: ${wr.wr_id} richiede la verifica. L�utente � stato nominato come sostituto per le verifiche. Fare clic sul seguente collegamento per visualizzare i dettagli: ${link}.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork' AND REFERENCED_BY = 'SENDEMAIL_VERIFICATION_SUBSTITUTE_STEPMGR' AND MESSAGE_ID = 'SENDEMAIL_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Merci d�avoir soumis une demande de d�m�nagement. Nous vous tiendrons inform� par e-mail de l�approbation, �mission et fermeture de cette demande.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Cliquez sur le lien ci-dessous pour consulter les d�tails�:'||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-gp-examine-form.axvw?project_id=${encoded_project_id}' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = '� stata presentata una richiesta di spostamento (l�utente � il contatto).'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Fare clic sul collegamento di seguito per visualizzare i dettagli:'||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-gp-examine-form.axvw?project_id=${encoded_project_id}' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_GROUP_INFORM_CONTACT_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Merci d�avoir soumis une demande de d�m�nagement. Nous vous tiendrons inform� par e-mail de l�approbation, �mission et fermeture de cette demande.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Cliquez sur le lien ci-dessous pour consulter les d�tails�:'||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-examine-form.axvw?mo_id=${encoded_mo_id}' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = '� stata presentata una richiesta di spostamento (l�utente � il contatto).'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Fare clic sul collegamento di seguito per visualizzare i dettagli:'||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-examine-form.axvw?mo_id=${encoded_mo_id}' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'REQUEST_SINGLE_INFORM_CONTACT_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Progetto di spostamento [${project.project_id}] instradato per l�approvazione.' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_INFORM_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Spostamento instradato per l�approvazione' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Veuillez signifier votre approbation en cliquant sur l�URL ci-dessous et en s�lectionnant les options d�action qui apparaissent pr�s de votre nom.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-gp-approve-form.axvw?project_id=${encoded_project_id}', MESSAGE_TEXT_IT = 'Indicare l�approvazione facendo clic sull�URL sottostante e selezionando una delle opzioni di azione visualizzate accanto al proprio nome.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-gp-approve-form.axvw?project_id=${encoded_project_id}' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_GROUP_REQUEST_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Spostamento [${mo.mo_id}] instradato per l�approvazione.' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_INFORM_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Spostamento instradato per l�approvazione' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_INFORM_SUBJECT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Veuillez signifier votre approbation en cliquant sur l�URL ci-dessous et en s�lectionnant les options d�action qui apparaissent pr�s de votre nom.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-approve-form.axvw?mo_id=${encoded_mo_id}', MESSAGE_TEXT_IT = 'Indicare l�approvazione facendo clic sull�URL sottostante e selezionando una delle opzioni di azione visualizzate accanto al proprio nome.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'${web_central_path}/ab-mo-approve-form.axvw?mo_id=${encoded_mo_id}' WHERE ACTIVITY_ID = 'AbMoveManagement' AND REFERENCED_BY = 'MOVE_NOTIFICATIONS_WFR' AND MESSAGE_ID = 'ROUTE_SINGLE_REQUEST_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Notification de la location�: Location {0} arrivant � expiration le {1} (niveau d�alerte�: {4})' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'LS_ALERT_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L�option de location {0} arrive � expiration le�: {1}{2}Pour plus de d�tails, veuillez cliquer sur le lien suivant�:{3}' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'OP_ALERT_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Notification de l�option de location�: Option {0} arrivant � expiration le {1} (niveau d�alerte�: {4})' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration' AND REFERENCED_BY = 'LS_ALERTS_WFR' AND MESSAGE_ID = 'OP_ALERT_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L��v�nement ${activity_log.action_title} est MANQU�.  Il est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}, avec une date de r�alisation requise le ${activity_log.date_required}.', MESSAGE_TEXT_IT = 'L�evento ${activity_log.action_title} NON � INIZIATO.  � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}, con la data di completamento obbligatoria ${activity_log.date_required}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_MISSED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L��v�nement ${activity_log.action_title} est MANQU�/OMIS.  Il est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}, avec une date de r�alisation requise le ${activity_log.date_required}.', MESSAGE_TEXT_IT = 'L�evento ${activity_log.action_title} NON � INIZIATO/� SALTATO.  � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}, con la data di completamento obbligatoria ${activity_log.date_required}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_MISSED_REQ';

UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L��v�nement ${activity_log.action_title} est en SOUFFRANCE.  Il est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}, avec une date de r�alisation requise le ${activity_log.date_required}.', MESSAGE_TEXT_IT = 'L�evento ${activity_log.action_title} � IN RITARDO.  � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}, con la data di completamento obbligatoria ${activity_log.date_required}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_OVERDUE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L��v�nement ${activity_log.action_title} est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}, avec une date de r�alisation requise le ${activity_log.date_required}.', MESSAGE_TEXT_IT = 'L�evento ${activity_log.action_title} � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}, con la data di completamento obbligatoria ${activity_log.date_required}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'EVENT_REMINDER';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Il est temps de cr�er et de publier le programme b�timent par b�timent.', MESSAGE_TEXT_IT = 'Il progetto annuale di ispezione e abbattimento delle muffe � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'� necessario creare e pubblicare l�edificio per pianificazione edificio.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD BLDG SCHEDULE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}.  La r�union de planification du projet est planifi�e dans les 2�semaines � venir. Vous recevrez une invitation de r�union Outlook avec la date concr�te.  La pr�sence est obligatoire.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD PROJECT PLAN';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Il est temps de commencer le processus d��valuation et de s�lection des fournisseurs pour l�inspection et l��limination des moisissures.'||CHR(13)||CHR(10)
||'Le fournisseur choisi l�ann�e derni�re �tat ${activity_log.vn_id}.', MESSAGE_TEXT_IT = 'Il progetto annuale di ispezione e abbattimento delle muffe � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'� necessario intraprendere il processo di valutazione e scelta dei fornitori per l�ispezione e l�abbattimento di muffe.'||CHR(13)||CHR(10)
||'Lo scorso anno � stato utilizzato il fornitore ${activity_log.vn_id}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD VENDOR EVAL';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Il est temps de produire les rapports d�infrastructures, les plans et les dessins pour le fournisseur de moules.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-FIELD DOCS';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures s�est termin� le ${activity_log.date_actual}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous n�avez pas encore re�u le rapport final, contactez ${activtiy_log.vn_id}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-FINAL RPT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Il est temps de pr�parer et d�obtenir l�autorisation pour les cl�s principales afin de la fournir au fournisseur d�inspection des moisissures${activtiy_log.vn_id}.', MESSAGE_TEXT_IT = 'Il progetto annuale di ispezione e abbattimento delle muffe � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'� necessario preparare e ottenere l�autorizzazione per i passepartout da consegnare al fornitore del servizio di ispezione muffe ${activtiy_log.vn_id}.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-MASTER KEYS';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le projet annuel d�inspection et d��limination des moisissures est �tabli pour commencer le ${activity_log.date_scheduled} et se terminer le ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||'Requis par�: ${activity_log.date_required}'||CHR(13)||CHR(10)
||'Le programme b�timent par b�timent est joint.  Envisagez de laisser l�inspecteur des moisissures acc�der enti�rement � toutes les zones requises.  Il se peut qu�il y ait une interruption aux zones de travail actif.', MESSAGE_TEXT_IT = 'Il progetto annuale di ispezione e abbattimento delle muffe � impostato per iniziare il ${activity_log.date_scheduled} e terminare il ${activity_log.date_scheduled_end}.'||CHR(13)||CHR(10)
||'Richiesto da: ${activity_log.date_required}'||CHR(13)||CHR(10)
||'� allegato l�edificio per pianificazione edificio.  Pianificare l�accesso completo dell�ispettore del servizio muffe a tutte le aree necessarie.  Potrebbe verificarsi un�interruzione delle aree di lavoro attive.' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_BODY' AND MESSAGE_ID = 'MOLD-NTFY DEPT MGRS';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Pr�parer en vue d�une inspection � venir des moisissures' WHERE ACTIVITY_ID = 'AbRiskCompliance' AND REFERENCED_BY = 'NOTIFY_TEMPLATE_SUBJECT' AND MESSAGE_ID = 'MOLD-NTFY DEPT MGRS';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un accident avec les informations suivantes a �t� supprim�:'||CHR(13)||CHR(10)
||'Code d�accident�: ${ehs_incidents.incident_id}'||CHR(13)||CHR(10)
||'Date d�accident�: ${ehs_incidents.date_incident}'||CHR(13)||CHR(10)
||'Type d�accident�: ${ehs_incidents.incident_type}'||CHR(13)||CHR(10)
||'Cat�gorie d�accident�: ${ehs_incidents.injury_category_id}', MESSAGE_TEXT_IT = 'L�incidente con le seguenti informazioni � stato eliminato:'||CHR(13)||CHR(10)
||'Codice incidente: ${ehs_incidents.incident_id}'||CHR(13)||CHR(10)
||'Data incidente: ${ehs_incidents.date_incident}'||CHR(13)||CHR(10)
||'Tipologia incidente: ${ehs_incidents.incident_type}'||CHR(13)||CHR(10)
||'Categoria incidente: ${ehs_incidents.injury_category_id}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_INCIDENT_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Emplacement de l�accident�: ${ehs_incidents.site_id} - ${ehs_incidents.pr_id} - ${ehs_incidents.bl_id} - ${ehs_incidents.fl_id} - ${ehs_incidents.rm_id}'||CHR(13)||CHR(10)
||'Description de l�accident�: ${ehs_incidents.description}', MESSAGE_TEXT_IT = 'Sito dell�incidente: ${ehs_incidents.site_id} - ${ehs_incidents.pr_id} - ${ehs_incidents.bl_id} - ${ehs_incidents.fl_id} - ${ehs_incidents.rm_id}'||CHR(13)||CHR(10)
||'Descrizione dell�incidente: ${ehs_incidents.description}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_INCIDENT_TEXT_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un accident avec les informations suivantes a �t� g�n�r�: '||CHR(13)||CHR(10)
||'Code d�accident�: ${ehs_incidents.incident_id}'||CHR(13)||CHR(10)
||'Date d�accident�: ${ehs_incidents.date_incident}'||CHR(13)||CHR(10)
||'Type d�accident�: ${ehs_incidents.incident_type}'||CHR(13)||CHR(10)
||'Cat�gorie d�accident�: ${ehs_incidents.injury_category_id}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_INCIDENT_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Emplacement de l�accident�: ${ehs_incidents.site_id} - ${ehs_incidents.pr_id} - ${ehs_incidents.bl_id} - ${ehs_incidents.fl_id} - ${ehs_incidents.rm_id}'||CHR(13)||CHR(10)
||'Description de l�accident�: ${ehs_incidents.description}'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher toutes les donn�es sur l�accident�: ${link}', MESSAGE_TEXT_IT = 'Sito dell�incidente: ${ehs_incidents.site_id} - ${ehs_incidents.pr_id} - ${ehs_incidents.bl_id} - ${ehs_incidents.fl_id} - ${ehs_incidents.rm_id}'||CHR(13)||CHR(10)
||'Descrizione dell�incidente: ${ehs_incidents.description}'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare tutti i dettagli dell�incidente: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_INCIDENT_TEXT_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L�accident suivant a �t� mis � jour�: '||CHR(13)||CHR(10)
||'Code d�accident�: ${ehs_incidents.incident_id}'||CHR(13)||CHR(10)
||'Date d�accident�: ${ehs_incidents.date_incident}'||CHR(13)||CHR(10)
||'Type d�accident�: ${ehs_incidents.incident_type}'||CHR(13)||CHR(10)
||'Cat�gorie d�accident�: ${ehs_incidents.injury_category_id}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_INCIDENT_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Emplacement de l�accident�: ${ehs_incidents.site_id} - ${ehs_incidents.pr_id} - ${ehs_incidents.bl_id} - ${ehs_incidents.fl_id} - ${ehs_incidents.rm_id}'||CHR(13)||CHR(10)
||'Description de l�accident�: ${ehs_incidents.description}'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher toutes les donn�es sur l�accident�: ${link}', MESSAGE_TEXT_IT = 'Sito dell�incidente: ${ehs_incidents.site_id} - ${ehs_incidents.pr_id} - ${ehs_incidents.bl_id} - ${ehs_incidents.fl_id} - ${ehs_incidents.rm_id}'||CHR(13)||CHR(10)
||'Descrizione dell�incidente: ${ehs_incidents.description}'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare tutti i dettagli dell�incidente: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_INCIDENTS_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_INCIDENT_TEXT_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une de vos livraisons d��quipement de protection individuelle a �t� annul�e. Equipement de protection individuelle�: ${ehs_em_ppe_types.ppe_type_id}, auparavant programm� le�: <#list steps as step>${step.date_delivered} </#list>' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_PPE_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = '� l�emplacement suivant�: ${ehs_em_ppe_types.bl_id} - ${ehs_em_ppe_types.fl_id} - ${ehs_em_ppe_types.rm_id}.' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_PPE_TEXT_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Livraison d��quipement de protection individuelle annul�e' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_DELETED_PPE_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'L�utente � stato assegnato al seguente Accertamento medico: ${ehs_medical_mon_results.medical_monitoring_id} (${ehs_medical_mon_results.monitoring_type}, ${ehs_medical_monitoring.description}), che inizialmente avr� luogo nelle seguenti date:'||CHR(13)||CHR(10)
||'<#list steps as step>'||CHR(13)||CHR(10)
||'    ${step.date_actual}'||CHR(13)||CHR(10)
||'</#list>.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare i dettagli: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_MEDMONITORING_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Vous avez �t� affect� � la livraison de l��quipement de protection suivant�: ${ehs_em_ppe_types.ppe_type_id}, qui aura initialement lieu aux dates suivantes�:'||CHR(13)||CHR(10)
||'<#list steps as step>'||CHR(13)||CHR(10)
||'    ${step.date_delivered}'||CHR(13)||CHR(10)
||'</#list>'||CHR(13)||CHR(10)
||'� l�emplacement suivant�: ${ehs_em_ppe_types.bl_id}-${ehs_em_ppe_types.fl_id}-${ehs_em_ppe_types.rm_id}.'||CHR(13)||CHR(10)
||'Pour plus de d�tails, cliquez sur le lien suivant�: ${link}', MESSAGE_TEXT_IT = 'L�utente � stato assegnato alla consegna del seguente Dispositivo di Protezione Individuale (DPI): ${ehs_em_ppe_types.ppe_type_id}, che inizialmente avr� luogo nelle seguenti date:'||CHR(13)||CHR(10)
||'<#list steps as step>'||CHR(13)||CHR(10)
||'    ${step.date_delivered}'||CHR(13)||CHR(10)
||'</#list>'||CHR(13)||CHR(10)
||'presso il seguente sito: ${ehs_em_ppe_types.bl_id} - ${ehs_em_ppe_types.fl_id} - ${ehs_em_ppe_types.rm_id}.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare i dettagli: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_PPE_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Livraison d��quipement de protection individuelle affect�e' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_PPE_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'L�utente � stato assegnato al seguente Programma di formazione:'||CHR(13)||CHR(10)
||'${ehs_training_results.training_id}, che inizialmente avr� luogo nelle seguenti date:'||CHR(13)||CHR(10)
||'<#list steps as step>'||CHR(13)||CHR(10)
||'    ${step.date_actual}'||CHR(13)||CHR(10)
||'</#list>.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare i dettagli: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_NEW_TRAINING_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Il seguente Accertamento medico: ${ehs_medical_mon_results.medical_monitoring_id} (${ehs_medical_mon_results.monitoring_type}), assegnato all�utente per avere luogo inizialmente nella seguente data: ${ehs_medical_mon_results.date_actual}, � stato ripianificato in questa data: <#list steps as step>${step.date_actual} </#list>.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare i dettagli: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_MEDMONITORING_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L��quipement de protection individuelle suivant�: ${ehs_em_ppe_types.ppe_type_id}, dont la livraison vous a �t� affect�e initialement pour avoir lieu � la date suivante�: ${ehs_em_ppe_types.date_delivered}, a �t� reprogramm� � cette date�: <#list steps as step>${step.date_delivered} </#list>', MESSAGE_TEXT_IT = 'Il seguente Dispositivo di Protezione Individuale (DPI): ${ehs_em_ppe_types.ppe_type_id}, la cui consegna era stata assegnata all�utente per avere luogo inizialmente nella seguente data: ${ehs_em_ppe_types.date_delivered}, � stato ripianificato in questa data: <#list steps as step>${step.date_delivered} </#list>' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_PPE_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = '� l�emplacement suivant�: ${ehs_em_ppe_types.bl_id} - ${ehs_em_ppe_types.fl_id} - ${ehs_em_ppe_types.rm_id}.'||CHR(13)||CHR(10)
||'Cliquez sur le lien suivant pour afficher les d�tails�: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_PPE_TEXT_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Passer � la livraison d��quipement de protection individuelle programm�e' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_PPE_TITLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Il seguente programma di formazione: ${ehs_training_results.training_id}, assegnato all�utente per avere luogo inizialmente nella seguente data: ${ehs_training_results.date_actual}, � stato ripianificato in questa data: <#list steps as step>${step.date_actual} </#list>.'||CHR(13)||CHR(10)
||'Fare clic sul collegamento riportato di seguito per visualizzare i dettagli: ${link}' WHERE ACTIVITY_ID = 'AbRiskEHS' AND REFERENCED_BY = 'NOTIFICATION_WFR' AND MESSAGE_ID = 'NOTIFY_UPDATED_TRAINING_TEXT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'DATE DE D�BUT D�ENTREPOSAGE' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_START_DATE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'LA L�GISLATION F�D�RALE INTERDIT L��VACUATION INAPPROPRI�E DES D�CHETS.' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_TITLE_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'LE CAS �CH�ANT, CONTACTEZ LES SERVICES DE POLICE OU L�AUTORIT� DE S�CURIT� PUBLIQUE COMP�TENTE LA PLUS PROCHE, OU L�AGENCE AM�RICAINE POUR LA PROTECTION DE L�ENVIRONNEMENT (EPA).', MESSAGE_TEXT_IT = 'SE TROVATO, CONTATTARE LA STAZIONE DI POLIZIA O LA SEDE DI TUTELA DELLA SICUREZZA PUBBLICA PI� VICINA, OPPURE L� ENTE PER LA TUTELA DELL�AMBIENTE (EPA) DEGLI STATI UNITI.' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'HAZARDOUS_TITLE_3';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'CES D�CHETS NE FONT PAS L�OBJET D�UNE R�GLEMENTATION PAR L�AGENCE AM�RICAINE POUR LA PROTECTION DE L�ENVIRONNEMENT (EPA).', MESSAGE_TEXT_IT = 'QUESTI RIFIUTI NON SONO SOGGETTI ALLA NORMATIVA DELL�ENTE PER LA TUTELA DELL�AMBIENTE (EPA) DEGLI STATI UNITI.' WHERE ACTIVITY_ID = 'AbRiskWasteMgmt' AND REFERENCED_BY = 'GENERATESELECTEDLABELS_WFR' AND MESSAGE_ID = 'NON_HAZARDOUS_TITLE_2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = '*** Ceci est une r�ponse automatique faisant suite � votre demande de recherche de mot de passe. Merci de ne pas r�pondre � ce message ***'||CHR(13)||CHR(10)
||'   Vous recevez ce message du serveur d�application ARCHIBUS Web Central, parce que vous avez demand� que votre mot de passe vous soit communiqu�.'||CHR(13)||CHR(10)
||'   Votre mot de passe est�: {0}' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_FORGOTTEN_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = '*** Ceci est un message automatique. Merci de ne pas r�pondre � ce message ***'||CHR(13)||CHR(10)
||'   L�administrateur syst�me a modifi� votre mot de passe.'||CHR(13)||CHR(10)
||'   Votre nouveau mot de passe est�: {0}', MESSAGE_TEXT_IT = '*** Questo � un messaggio generato automaticamente. Non rispondere a questo messaggio e-mail ***'||CHR(13)||CHR(10)
||'   La password � stata modificata dall�amministratore di sistema.'||CHR(13)||CHR(10)
||'   La nuova password �: {0}' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_NEW_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = '** Ceci est un message automatique. Merci de ne pas r�pondre � ce message ***'||CHR(13)||CHR(10)
||'L�utilisateur {0} a demand� une modification de mot de passe.', MESSAGE_TEXT_IT = '** Questo � un messaggio generato automaticamente. Non rispondere a questo messaggio e-mail ***'||CHR(13)||CHR(10)
||'L�utente {0} ha richiesto la modifica della password.' WHERE ACTIVITY_ID = 'AbSystemAdministration' AND REFERENCED_BY = 'PASSWORDMANAGERIMPL' AND MESSAGE_ID = 'SEND_REQUEST_NEW_PASSWORD_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le traitement de r�servation a �t� interrompu en raison d�une erreur lors de la tentative de soustraction de l�heure de fin et de d�but pour obtenir le co�t de r�servation.', MESSAGE_TEXT_IT = 'Il processo di prenotazione � stato interrotto a causa di un errore durante il tentativo di sottrarre l�ora di fine e l�ora di inizio per ottenere il costo della prenotazione.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'SUBTRACTTIMESERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le processus de r�servation a �t� stopp� car l�am�nagement de pi�ce ne peut pas �tre r�serv� de mani�re r�troactive.', MESSAGE_TEXT_IT = 'Il processo di prenotazione � stato interrotto, poich� non � possibile prenotare la destinazione d�uso del locale per un periodo antecedente ad oggi' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'TIMEPERIODINPAST';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le traitement de r�servation a �t� interrompu car l�am�nagement de pi�ce n�est plus disponible pour cette date et cette p�riode horaire.', MESSAGE_TEXT_IT = 'Il processo di prenotazione � stato interrotto poich� l�assetto del locale non � pi� disponibile nella data e nell�ora correnti.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'ADDROOMRESERVATION_WFR' AND MESSAGE_ID = 'TIMEPERIODNOTAVAILABLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le processus d�approbation a �t� interrompu en raison d�une erreur.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'APPROVERESERVATION_WFR' AND MESSAGE_ID = 'APPROVERESERVATIONERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L�annulation de la demande a �t� interrompue car il est impossible d�annuler la r�servation � ce stade.', MESSAGE_TEXT_IT = 'L�azione di annullamento � stata interrotta poich� la prenotazione non pu� essere annullata in questa fase.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CANCELRESERVATION_WFR' AND MESSAGE_ID = 'CANCELRESERVATIONERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu lors de la tentative de v�rification et d�ex�cution de l�action de r�servation des pi�ces et des ressources'||CHR(13)||CHR(10)
||'en attente d�approbation, dont le d�lai d�approbation a expir�.', MESSAGE_TEXT_IT = 'Si � verificato un problema durante il controllo e l�esecuzione dell�azione per le prenotazioni di locali e risorse'||CHR(13)||CHR(10)
||'in attesa di approvazione, il cui tempo di approvazione � scaduto.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CHECKROOMANDRESOURCESAPPROVAL_WFR' AND MESSAGE_ID = 'CHECKROOMANDRESOURCESAPPROVALERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La r�servation a �t� automatiquement rejet�e par le syst�me une fois que le d�lai d�approbation a expir�.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CHECKROOMANDRESOURCESAPPROVAL_WFR' AND MESSAGE_ID = 'CHECKROOMANDRESOURCESAPPROVALREJECTMESSAGE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Der Archivierungsvorgang wurde abgebrochen, da im Feld �Tage bis zur Archivierung� ein unzul�ssiger Wert steht.', MESSAGE_TEXT_ES = 'Se abort� el proceso Archivar debido a un valor incorrecto en el campo �Numero de d�as antes de archivar�.', MESSAGE_TEXT_FR = 'Le processus d�archivage a �t� interrompu en raison de la saisie d�une valeur erron�e dans le champ ��Nombre de jours avant archivage��.', MESSAGE_TEXT_IT = 'Il processo di archiviazione � stato interrotto a causa di un valore non valido nel campo �Numero di giorni prima dell�archiviazione�.', MESSAGE_TEXT_NL = 'Het archiveringsproces is afgebroken als gevolg van een slechte waarde in het veld �Aantal dagen v��r archiveren�.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CLOSERESERVATIONS_WFR' AND MESSAGE_ID = 'ARCHIVERESERVATIONSERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une erreur est survenue lors de la tentative de cr�ation des demandes d�intervention associ�es � la r�servation de pi�ces.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'CREATEWORKREQUEST_WFR' AND MESSAGE_ID = 'CREATEWORKREQUESTERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Aucune ressource fixe n�est attach�e � l�am�nagement de pi�ce s�lectionn�.', MESSAGE_TEXT_IT = 'Nessuna risorsa fissa collegata alla destinazione d�uso del locale selezionato.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETARRANGEMENTFIXEDRESOURCES_WFR' AND MESSAGE_ID = 'FIXEDRESOURCESNOTFOUND';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu lors de la tentative de recherche de r�servations en attente pour l�am�nagement mis � jour.', MESSAGE_TEXT_IT = 'Problema durante il controllo per verificare se la destinazione d�uso aggiornata dispone di prenotazioni in sospeso.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETNUMBERPENDINGRESERVATIONS_WFR' AND MESSAGE_ID = 'GETNUMBERPENDINGRESERVATIONSERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Aucune information ou r�servation n�est d�finie pour les crit�res de s�lection.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETRESERVATIONINFO_WFR' AND MESSAGE_ID = 'RESERVATIONNOTFOUND';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une erreur est survenue lors de la tentative d�obtention du type de r�servation' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'GETRESERVATIONTYPE_WFR' AND MESSAGE_ID = 'GETRESERVATIONTYPEERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La r�servation de la pi�ce indiqu�e ci-dessous n�a pas pu �tre annul�e lors de la synchronisation avec Exchange.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Objet�: ${reserve.reservation_name}'||CHR(13)||CHR(10)
||'Date et heure�: ${reserve.date_start} ${reserve.time_start} - ${reserve.time_end}'||CHR(13)||CHR(10)
||'Emplacement�: ${rm_arrange.bl_id}-${rm_arrange.fl_id}-${rm_arrange.rm_id}'||CHR(13)||CHR(10)
||'Code de r�servation�: ${reserve.res_id}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Vous pouvez examiner la r�servation dans Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_CANCEL_FAILED_NOTIFY_BODY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu lors de la mise � jour de la r�servation li�e � la r�union que vous avez modifi�e.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Les d�tails de la r�union sont donn�s ci-apr�s�:'||CHR(13)||CHR(10)
||'Objet�: ${reserve.reservation_name}'||CHR(13)||CHR(10)
||'Date et heure d�origine�: ${reserve.date_start} ${reserve.time_start} - ${reserve.time_end}'||CHR(13)||CHR(10)
||'Code de r�servation�: ${reserve.res_id}', MESSAGE_TEXT_IT = 'Si � verificato un problema durante l�aggiornamento della prenotazione collegata alla riunione modificata.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Di seguito sono riportati i dettagli della riunione:'||CHR(13)||CHR(10)
||'Oggetto: ${reserve.reservation_name}'||CHR(13)||CHR(10)
||'Data e ora originali: ${reserve.date_start} ${reserve.time_start} - ${reserve.time_end}'||CHR(13)||CHR(10)
||'Codice prenotazione: ${reserve.res_id}' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_BODY1';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce n�est pas disponible pour la nouvelle p�riode.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'D�tails de la pi�ce�:'||CHR(13)||CHR(10)
||'B�timent, �tage, code de pi�ce�: ${rm_arrange.bl_id}-${rm_arrange.fl_id}-${rm_arrange.rm_id}'||CHR(13)||CHR(10)
||'Am�nagement�: ${rm_arrange.arrange_type_id}'||CHR(13)||CHR(10)
||'Configuration�: ${rm_arrange.config_id}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Utilisez le compl�ment Outlook ou Web Central pour s�lectionner une autre pi�ce ou r�tablir la date et l�heure d�origine de la r�union.', MESSAGE_TEXT_IT = 'Il locale non � disponibile per il nuovo intervallo di tempo.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Dettagli locale:'||CHR(13)||CHR(10)
||'Edificio, piano, codice locale: ${rm_arrange.bl_id}-${rm_arrange.fl_id}-${rm_arrange.rm_id}'||CHR(13)||CHR(10)
||'Tipologia d�uso: ${rm_arrange.arrange_type_id}'||CHR(13)||CHR(10)
||'Configurazione: ${rm_arrange.config_id}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Utilizzare il plugin Outlook o Web Central per selezionare un locale diverso o ripristinare la data e l�ora originali della riunione.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_BODY2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un �quipement, un service ou une s�lection de restauration n�est pas disponible pour la nouvelle p�riode.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'D�tails�:'||CHR(13)||CHR(10)
||'ID�: ${resources.resource_id}'||CHR(13)||CHR(10)
||'Nom�: ${resources.resource_name}'||CHR(13)||CHR(10)
||'Cat�gorie�: ${resources.resource_std}'||CHR(13)||CHR(10)
||'Quantit� demand�e�: ${reserve_rs.quantity}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Utilisez Web Central pour mettre � jour votre r�servation ou r�tablir la date et l�heure d�origine de la r�union.', MESSAGE_TEXT_IT = 'La selezione di un�apparecchiatura, un servizio o una ristorazione non � disponibile per il nuovo intervallo temporale.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Dettagli:'||CHR(13)||CHR(10)
||'ID: ${resources.resource_id}'||CHR(13)||CHR(10)
||'Nome: ${resources.resource_name}'||CHR(13)||CHR(10)
||'Categoria: ${resources.resource_std}'||CHR(13)||CHR(10)
||'Quantit� richiesta: ${reserve_rs.quantity}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Utilizzare Web Central per aggiornare la prenotazione o ripristinare la data e l�ora originali della riunione.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LISTENER_WFR' AND MESSAGE_ID = 'EXCHANGE_UPDATE_CONFLICT_NOTIFY_BODY3';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Valeur non valide trouv�e pour le param�tre d�activit� suivant�:' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LOADTIMELINE_WFR' AND MESSAGE_ID = 'INVALIDPARAMETERERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Une erreur est survenue lors de la tentative d�obtention des pi�ces disponibles et des r�servations correspondantes.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LOADTIMELINE_WFR' AND MESSAGE_ID = 'LOADTIMELINEERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Aucune pi�ce n�est disponible pour l�emplacement sp�cifi� et la p�riode s�lectionn�e.', MESSAGE_TEXT_IT = 'Nessun locale disponibile per il posto specificato e l�intervallo di tempo selezionato.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'LOADTIMELINE_WFR' AND MESSAGE_ID = 'LOADTIMELINENOROOMSFOUND';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu lors de la notification par email de l�approbateur s�lectionn�. La collecte ou l�envoi des informations a g�n�r� une erreur.', MESSAGE_TEXT_IT = 'Si � verificato un problema con la notifica e-mail al revisore selezionato. La raccolta o l�invio delle informazioni ha causato un errore.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVERERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Ce message sert � vous signaler que votre approbation est exig�e pour une r�servation de pi�ce qui a d�pass� sa fen�tre d�approbation pr�d�finie.  Veuillez cliquer sur le lien ci-dessous pour approuver ou rejeter cette r�servation de pi�ce.', MESSAGE_TEXT_IT = 'Questo messaggio segnala che l�approvazione dell�utente � richiesta per una prenotazione locale che ha superato la finestra di approvazione definita.  Selezionare il collegamento riportato di seguito per approvare o rifiutare questa prenotazione locale.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART1';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Type d�am�nagement de pi�ce�:', MESSAGE_TEXT_IT = 'Tipo di destinazione d�uso del locale:' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART15';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Ce message sert � vous signaler que votre approbation est exig�e pour une r�servation de ressource qui a d�pass� sa fen�tre d�approbation pr�d�finie.  Veuillez cliquer sur le lien ci-dessous pour approuver ou rejeter cette r�servation de ressource.', MESSAGE_TEXT_IT = 'Questo messaggio segnala che l�approvazione dell�utente � richiesta per una prenotazione risorsa che ha superato la finestra di approvazione definita.  Selezionare il collegamento riportato di seguito per approvare o rifiutare questa prenotazione risorsa.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYAPPROVER_WFR' AND MESSAGE_ID = 'NOTIFYAPPROVER_BODY_PART2';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu avec la notification par e-mail des �l�ments demand�s par. Le rassemblement ou l�envoi des informations a provoqu� une erreur.', MESSAGE_TEXT_IT = 'Si � verificato un problema con la notifica e-mail alla richiesta da. La raccolta o l�invio delle informazioni ha causato un errore.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDBY_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDBYERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu avec la notification par e-mail des �l�ments demand�s pour. Le rassemblement ou l�envoi des informations a provoqu� une erreur.', MESSAGE_TEXT_IT = 'Si � verificato un problema con la notifica e-mail alla richiesta per. La raccolta o l�invio delle informazioni ha causato un errore.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'NOTIFYREQUESTEDFOR_WFR' AND MESSAGE_ID = 'NOTIFYREQUESTEDFORERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L�annulation de la r�servation a �chou�. {0}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, la r�servation de pi�ce sera dissoci�e du rendez-vous Outlook.', MESSAGE_TEXT_IT = 'Annullamento della prenotazione non riuscita. {0}'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Se si seleziona Ignora, la prenotazione del locale verr� scollegata dall�appuntamento in Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CANCEL_FAILED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'L�annulation de la r�servation a �chou�. {0}'||CHR(13)||CHR(10)
||'Voulez-vous r�essayer�?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Non, la r�servation de pi�ce sera dissoci�e du rendez-vous Outlook.', MESSAGE_TEXT_IT = 'Annullamento della prenotazione non riuscita. {0}'||CHR(13)||CHR(10)
||'Riprovare?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Se si seleziona No, la prenotazione del locale verr� scollegata dall�appuntamento in Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CANCEL_FAILED_RETRY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Posizioni multiple - vedere corpo dell�incontro' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFERENCE_CALL_MEETING_LOCATION';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Vous avez choisi de modifier la s�rie compl�te de cet appel de conf�rence r�current. '||CHR(13)||CHR(10)
||'Si vous continuez, toutes les r�servations de pi�ces et de ressources actuellement connect�es seront annul�es. De nouvelles r�servations seront cr��es uniquement pour la pi�ce s�lectionn�e, et non pour les ressources, ni pour d�autres pi�ces de l�appel de conf�rence.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_CHANGE_RECURRING_CONFERENCE_CALL';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce s�lectionn�e n�est pas disponible pour {0} des {1} occurrences. Vous pouvez s�lectionner plus tard un autre emplacement pour les occurrences en conflit.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_RECURRENCE_CONFLICTS';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Ihre Raumauswahl f�hrt zu {0} Konflikt(en) f�r die {1} Vorkommen. Sie k�nnen diese Konflikte sp�ter l�sen.', MESSAGE_TEXT_ES = 'Los espacios que ha seleccionado causan {0} conflicto/s para {1} caso/s. Puede resolver dichos conflictos en otro momento.', MESSAGE_TEXT_IT = 'La selezione dei locali presenta {0} conflitto/i per le {1} occorrenze. � possibile risolvere questi conflitti in un secondo momento.', MESSAGE_TEXT_NL = 'Uw selectie van ruimte-opbrengsten {0} is in conflict met de {1} voorvallen. U kunt deze conflicten later oplossen.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFIRM_RECURRENCE_CONFLICTS_CONFCALL';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce est attribu�e {0}�fois, mais n�est pas disponible {1}�fois le {2}. L�organiseur de la r�union peut r�soudre ces conflits dans son calendrier Outlook ou dans la vue <a href="{3}ab-rr-my-reservations-conflicts.axvw">R�soudre conflits</a> de Web Central.', MESSAGE_TEXT_IT = 'Il locale � prenotato per {0} occorrenze, ma non � disponibile per {1} occorrenza il {2}. L�organizzatore dell�incontro pu� risolvere questi conflitti sul suo calendario di Outlook o attraverso la visualizzazione <a href="{3}ab-rr-my-reservations-conflicts.axvw">Risolvi conflitti</a> in Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFLICTS_DESCRIPTION';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_ES = 'Todos los espacios est�n reservados para {0} casos, pero se produjeron conflictos con {1} casos en {2}. '||CHR(13)||CHR(10)
||'Abra los casos en cuesti�n para verificar qu� espacios no est�n reservados. '||CHR(13)||CHR(10)
||'La persona que organiz� la reuni�n puede resolver estos conflictos en su calendario de Outlook o a trav�s de la vista <a href="{3}ab-rr-my-reservations-conflicts.axvw">Resolver conflictos</a> de Web Central.', MESSAGE_TEXT_FR = 'Toutes les pi�ces sont attribu�es {0} fois, mais {1} �v�nements ayant lieu le {2} pr�sentent des conflits. '||CHR(13)||CHR(10)
||'Ouvrez ces �v�nements particuliers pour v�rifier les pi�ces qui ne sont pas attribu�es. '||CHR(13)||CHR(10)
||'L�organiseur de la r�union peut r�soudre ces conflits dans son calendrier Outlook ou dans la vue <a href="{3}ab-rr-my-reservations-conflicts.axvw">R�soudre conflits</a> de Web Central.', MESSAGE_TEXT_IT = 'Tutti i locali sono prenotati per {0} occorrenze, ma si sono verificati conflitti per {1} occorrenze il {2}. '||CHR(13)||CHR(10)
||'Aprire le occorrenze specifiche per verificare quali locali non sono prenotati. '||CHR(13)||CHR(10)
||'L�organizzatore dell�incontro pu� risolvere questi conflitti sul suo calendario di Outlook o attraverso la visualizzazione <a href="{3}ab-rr-my-reservations-conflicts.axvw">Risolvi conflitti</a> in Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'CONFLICTS_DESCRIPTION_CONFCALL';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La r�servation de pi�ce n�a pas pu �tre copi�e. S�lectionnez un nouvel emplacement.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'COPYING_RESERVATION_FAILED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'I locali selezionati sono stati prenotati per {0} appuntamenti da {1} a {2}.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_CONFCALL_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Alle ausgew�hlten R�ume wurden f�r {0} Termine gebucht, bei {1} Terminen traten jedoch Konflikte auf. Ersetzen Sie nicht verf�gbare R�ume f�r die Termine am {2}. '||CHR(13)||CHR(10)
||'�ffnen Sie den Termin �ber Ihren Outlook-Kalender oder �ber die Ansicht �Konflikte aufl�sen� in Web Central.', MESSAGE_TEXT_IT = 'Tutti i locali selezionati sono stati prenotati per {0} appuntamenti, ma si sono verificati conflitti per {1} appuntamenti. Sostituire i locali non disponibili per gli appuntamenti il {2}. '||CHR(13)||CHR(10)
||'Aprire l�appuntamento dal proprio calendario di Outlook o accedere alla visualizzazione Risolvi conflitti in Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_CONFCALL_RESERVATIONS_CREATED_CONFLICTS';

UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Alle ausgew�hlten R�ume wurden f�r {0} Termine gebucht, bei 1 Termin traten jedoch Konflikte auf. Ersetzen Sie nicht verf�gbare R�ume f�r den Termin am {1}. '||CHR(13)||CHR(10)
||'�ffnen Sie den Termin �ber Ihren Outlook-Kalender oder �ber die Ansicht �Konflikte aufl�sen� in Web Central.', MESSAGE_TEXT_IT = 'Tutti i locali selezionati sono stati prenotati per {0} appuntamenti, ma si sono verificati conflitti per 1 appuntamento. Sostituire i locali non disponibili per l�appuntamento il {1}. '||CHR(13)||CHR(10)
||'Aprire l�appuntamento dal proprio calendario di Outlook o accedere alla visualizzazione Risolvi conflitti in Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_CONFCALL_RESERVATIONS_CREATED_ONE_CONFLICT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Il locale selezionato � stato prenotato per l�appuntamento in data {0} dalle ore {1} alle ore {2}.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce s�lectionn�e a �t� attribu�e pour {0} rendez-vous, mais un conflit s�est produit. S�lectionnez un autre emplacement pour le rendez-vous en conflit le {1}.', MESSAGE_TEXT_IT = 'Il locale selezionato � stato prenotato per {0} appuntamenti, ma si � verificato un conflitto. Selezionare una posizione diversa per l�appuntamento in conflitto il {1}.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'C_RESERVATIONS_CREATED_ONE_CONFLICT';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La r�servation de pi�ce pour le rendez-vous du {0} n�a pas pu �tre annul�e.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, la r�servation de pi�ce sera dissoci�e du rendez-vous Outlook.', MESSAGE_TEXT_IT = 'Impossibile annullare la prenotazione del locale per l�appuntamento in data {0}.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Se si seleziona Ignora, la prenotazione del locale verr� scollegata dall�appuntamento in Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Eine Raumreservierung konnte nicht storniert werden.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Ignorieren� klicken, wird diese Raumreservierung vom Outlook-Termin getrennt.', MESSAGE_TEXT_FR = 'Une r�servation de pi�ce n�a pas pu �tre annul�e.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, cette r�servation de pi�ce sera dissoci�e du rendez-vous Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED_CONFERENCE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La r�servation de pi�ce pour le rendez-vous du {0} n�a pas pu �tre annul�e.'||CHR(13)||CHR(10)
||'Voulez-vous r�essayer�?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Non, la r�servation de pi�ce sera dissoci�e du rendez-vous Outlook.', MESSAGE_TEXT_IT = 'Impossibile annullare la prenotazione del locale per l�appuntamento in data {0}.'||CHR(13)||CHR(10)
||'Riprovare?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Se si seleziona No, la prenotazione del locale verr� scollegata dall�appuntamento in Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED_RETRY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Eine Raumreservierung konnte nicht storniert werden.'||CHR(13)||CHR(10)
||'M�chten Sie es erneut versuchen?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Nein� klicken, wird diese Raumreservierung vom Outlook-Termin getrennt.', MESSAGE_TEXT_FR = 'Une r�servation de pi�ce n�a pas pu �tre annul�e.'||CHR(13)||CHR(10)
||'Voulez-vous r�essayer�?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Non, cette r�servation de pi�ce sera dissoci�e du rendez-vous Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CANCEL_FAILED_RETRY_CONFERENCE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'I locali selezionati sono stati prenotati per un incontro {0}.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CONFCALL_RESERVATION_CREATED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce est attribu�e {0}�fois, mais n�est pas disponible 1�fois le {1}. L�organiseur de la r�union peut r�soudre ce conflit dans son calendrier Outlook ou dans la vue <a href="{2}ab-rr-my-reservations-conflicts.axvw">R�soudre conflits</a> de Web Central.', MESSAGE_TEXT_IT = 'Il locale � prenotato per {0} occorrenze, ma non � disponibile per 1 occorrenza il {1}. L�organizzatore dell�incontro pu� risolvere questo conflitto sul suo calendario di Outlook o attraverso la visualizzazione <a href="{2}ab-rr-my-reservations-conflicts.axvw">Risolvi conflitti</a> in Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CONFLICT_DESCRIPTION';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_ES = 'Todos los espacios est�n reservados para {0} casos, pero se produjeron conflictos con un casos en {1}. '||CHR(13)||CHR(10)
||'Abra ese caso concreto para verificar qu� espacios no est�n reservados. '||CHR(13)||CHR(10)
||'La persona que organiz� la reuni�n puede resolver estos conflictos en su calendario de Outlook o a trav�s de la vista <a href="{2}ab-rr-my-reservations-conflicts.axvw">Resolver conflictos</a> de Web Central.', MESSAGE_TEXT_FR = 'Toutes les pi�ces sont attribu�es {0} fois, mais 1��v�nement ayant lieu le {1} pr�sente des conflits. '||CHR(13)||CHR(10)
||'Ouvrez cet �v�nement particulier pour v�rifier les pi�ces qui ne sont pas attribu�es. '||CHR(13)||CHR(10)
||'L�organiseur de la r�union peut r�soudre ces conflits dans son calendrier Outlook ou dans la vue <a href="{2}ab-rr-my-reservations-conflicts.axvw">R�soudre conflits</a> de Web Central.', MESSAGE_TEXT_IT = 'Tutti i locali sono prenotati per {0} occorrenze, ma si sono verificati conflitti per 1 occorrenza il {1}. '||CHR(13)||CHR(10)
||'Aprire l�occorrenza specifica per verificare quali locali non sono prenotati. '||CHR(13)||CHR(10)
||'L�organizzatore dell�incontro pu� risolvere questi conflitti sul suo calendario di Outlook o attraverso la visualizzazione <a href="{2}ab-rr-my-reservations-conflicts.axvw">Risolvi conflitti</a> in Web Central.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_CONFLICT_DESCRIPTION_CONFCALL';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Il locale selezionato � stato prenotato per l�appuntamento in data {0}.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ONE_RESERVATION_CREATED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce pr�c�demment r�serv�e n�est pas disponible.'||CHR(13)||CHR(10)
||'S�lectionnez un nouvel emplacement.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'RESERVED_ROOM_UNAVAILABLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Entfernen Sie danach den Standort aus den ge�nderten Vorkommen am {0}.', MESSAGE_TEXT_FR = 'Apr�s I�annulation, supprimez I�emplacement de la ou des occurrences du {0}.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'REVIEW_MODIFIED_OCCURRENCES';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'N�a pas pu se connecter au serveur ARCHIBUS.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Le plug-in de r�servation de pi�ce s�ex�cutera en mode hors ligne jusqu�� ce que la connexion soit � nouveau disponible.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SERVER_UNREACHABLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = '{1} Raumreservierungen f�r die Termine am {0} konnten nicht storniert werden.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Ignorieren� klicken, werden diese Raumreservierungen von ihrem Outlook-Termin getrennt.', MESSAGE_TEXT_FR = '{1} r�servations de pi�ce pour les rendez-vous le {0} n�ont pas pu �tre annul�es.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, ces r�servations de pi�ce seront dissoci�es du rendez-vous Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = '{0} Raumreservierungen konnten nicht storniert werden.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Ignorieren� klicken, werden diese Raumreservierungen von ihrem Outlook-Termin getrennt.', MESSAGE_TEXT_FR = '{0} r�servations de pi�ce n�ont pas pu �tre annul�es.'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, ces r�servations de pi�ce seront dissoci�es du rendez-vous Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED_CONFERENCE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = '{1} Raumreservierungen f�r die Termine am {0} konnten nicht storniert werden.'||CHR(13)||CHR(10)
||'M�chten Sie es erneut versuchen?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Nein� klicken, werden diese Raumreservierungen von ihrem Outlook-Termin getrennt.', MESSAGE_TEXT_FR = '{1} r�servations de pi�ce pour les rendez-vous le {0} n�ont pas pu �tre annul�es.'||CHR(13)||CHR(10)
||'Voulez-vous r�essayer�?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Non, ces r�servations de pi�ce seront dissoci�es du rendez-vous Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED_RETRY';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = '{0} Raumreservierungen konnten nicht storniert werden.'||CHR(13)||CHR(10)
||'M�chten Sie es erneut versuchen?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Nein� klicken, werden diese Raumreservierungen von ihrem Outlook-Termin getrennt.', MESSAGE_TEXT_FR = '{0} r�servations de pi�ce n�ont pas pu �tre annul�es.'||CHR(13)||CHR(10)
||'Voulez-vous r�essayer�?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Non, ces r�servations de pi�ce seront dissoci�es du rendez-vous Outlook.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'SEVERAL_CANCELS_FAILED_RETRY_CONFERENCE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Sie haben die Reservierung {0} f�r {1} aus Ihrer Auswahl entfernt, die Reservierung konnte jedoch nicht storniert werden.'||CHR(13)||CHR(10)
||'Wenn Sie auf �Ignorieren� klicken, wird diese Raumreservierung vom Outlook-Termin getrennt. Wenn Sie auf �Abbruch� klicken, k�nnen Sie den Raum erneut hinzuf�gen oder den Termin schlie�en, ohne die �nderungen zu speichern.', MESSAGE_TEXT_ES = 'Ha quitado de su selecci�n la reserva de espacio {0} para {1} pero no pudo cancelarse.'||CHR(13)||CHR(10)
||'Si hace clic en Omitir, se desconectar� la reserva del espacio de la cita de Outlook. Si hace clic en Anular, podr� agregar ese espacio nuevamente o cerrar la cita sin guardar sus cambios.', MESSAGE_TEXT_FR = 'Vous avez supprim� la r�servation de pi�ce {0} pour {1} de votre s�lection, mais elle n�a pas pu �tre annul�e.'||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, la r�servation de pi�ce sera dissoci�e du rendez-vous Outlook. Si vous cliquez sur Abandonner, vous pouvez ajouter la pi�ce � nouveau ou fermer le rendez-vous sans enregistrer les modifications.', MESSAGE_TEXT_IT = 'La prenotazione {0} per {1} � stata rimossa dalla selezione, ma potrebbe non essere annullata.'||CHR(13)||CHR(10)
||'Facendo clic su Ignora, la prenotazione del locale verr� scollegata dall�appuntamento di Outlook. Selezionando Annulla, � possibile aggiungere nuovamente il locale o chiudere l�appuntamento senza salvare le modifiche.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'UPDATE_CONFCALL_ONE_CANCEL_FAILED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = '{0} Reservierungen, die Sie aus Ihrer Auswahl entfernt haben, konnten nicht storniert werden: {1}.'||CHR(13)||CHR(10)
||'Wenn Sie auf �Ignorieren� klicken, werden diese Raumreservierungen vom Outlook-Termin getrennt. Wenn Sie auf �Abbruch� klicken, k�nnen Sie die R�ume erneut hinzuf�gen oder den Termin schlie�en, ohne die �nderungen zu speichern.', MESSAGE_TEXT_ES = 'Las {0} reservas que ha quitado de su selecci�n no han podido cancelarse: {1}. Si hace clic en Omitir, se desconectar�n las reservas del espacio de la cita de Outlook. Si hace clic en Anular, podr� agregar los espacios nuevamente o cerrar la cita sin guardar sus cambios.', MESSAGE_TEXT_FR = '{0} r�servations que vous avez supprim�es de votre s�lection n�ont pas pu �tre annul�es�: {1}.'||CHR(13)||CHR(10)
||'Si vous cliquez sur Ignorer, ces r�servations de pi�ces seront dissoci�es du rendez-vous Outlook. Si vous cliquez sur Abandonner, vous pouvez ajouter les pi�ces � nouveau ou fermer le rendez-vous sans enregistrer les modifications.', MESSAGE_TEXT_IT = 'Le {0} prenotazioni rimosse dalla selezione non possono essere annullate: {1}. Se si seleziona Ignora, le prenotazioni dei locali verranno scollegate dall�appuntamento in Outlook. Selezionando Annulla, � possibile aggiungere nuovamente i locali o chiudere l�appuntamento senza salvare le modifiche.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'UPDATE_CONFCALL_SEVERAL_CANCELS_FAILED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Legen Sie die Teilnehmeranzahl f�r die Raumreservierung mithilfe des Kapazit�tsfilters im Panel �Raumreservierung� fest.', MESSAGE_TEXT_FR = 'Veuillez sp�cifier le nombre de participants pour la r�servation de pi�ce dans le filtre Capacit� dans le volet R�servation de pi�ce.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_CAPACITY_REQUIRED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Die mit diesem Termin verkn�pfte Raumreservierung ist nicht synchronisiert. M�chten Sie die Reservierung jetzt �ndern?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Wenn Sie auf �Nein� klicken, wird stattdessen der Termin ge�ndert.', MESSAGE_TEXT_FR = 'La r�servation de pi�ce li�e � ce rendez-vous n�est pas synchronis�e. Voulez-vous modifier la r�servation maintenant�?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Si vous cliquez sur Non, c�est le rendez-vous qui sera modifi�.', MESSAGE_TEXT_IT = 'La prenotazione del locale collegata a questo appuntamento non � sincronizzata. Modificare la prenotazione adesso?'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'Se si fa clic su No, verr� modificato l�appuntamento.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_OUT_OF_SYNC';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce {0}-{1}-{2} n�est pas disponible pour la s�lection actuelle. Modifiez ou supprimez toutes les pi�ces non disponibles avant d�enregistrer la r�union. Les pi�ces non disponibles sont marqu�es d�une croix rouge.', MESSAGE_TEXT_IT = 'Il locale {0}-{1}-{2} non � disponibile per la selezione corrente. Modificare o rimuovere tutti i locali non disponibili prima di salvare l�incontro. I locali non disponibili sono contrassegnati con un�icona a x rossa.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'WARNING_ROOM_UNAVAILABLE';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Les pi�ces s�lectionn�es n�ont pas �t� attribu�es.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ZERO_CONFCALL_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'La pi�ce s�lectionn�e n�a pas �t� attribu�e.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'OUTLOOK_PLUGIN' AND MESSAGE_ID = 'ZERO_RESERVATIONS_CREATED';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_DE = 'Die Werte �Angefordert von� bzw. �Angefordert f�r� k�nnen nicht aus der Mitarbeitertabelle abgerufen werden.', MESSAGE_TEXT_ES = 'No se pueden recuperar los valores �Solicitado por� o �Solicitado para� desde la tabla de empleados.', MESSAGE_TEXT_IT = 'Impossibile recuperare il valore �Richiesta da� o �Richiesta per� dalla tabella dei dipendenti.', MESSAGE_TEXT_NL = 'De waarden voor �Aangevraagd door� of �Aangevraagd voor� kunnen niet uit de Personeelstabel worden opgehaald.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'READREQUESTORPROPERTIES_WFR' AND MESSAGE_ID = 'READREQUESTORERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Le processus de rejet a �t� interrompu en raison d�une erreur.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'REJECTRESERVATION_WFR' AND MESSAGE_ID = 'REJECTRESERVATIONERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_IT = 'Si � verificato un errore durante l�aggiornamento delle propriet� del locale o durante la generazione della configurazione del locale.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SAVEROOMOVERRIDE_WFR' AND MESSAGE_ID = 'SAVEROOMOVERRIDEERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu lors de l�extraction des informations compl�mentaires sur les r�servations. Le syst�me ne peut pas trouver les informations ou la requ�te a provoqu� une erreur.', MESSAGE_TEXT_IT = 'Si � verificato un problema durante il recupero di informazioni aggiuntive sulle prenotazioni. Il sistema non ha trovato le informazioni o l�interrogazione ha causato un errore.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SEARCHRESERVATIONSADDITIONALINFO_WFR' AND MESSAGE_ID = 'RESERVATIONADDITIONALINFOERROR';
UPDATE AFM.MESSAGES SET  MESSAGE_TEXT_FR = 'Un probl�me est survenu avec les notifications d�invitation par e-mail pour les directions d�e-mail s�lectionn�es. Le rassemblement ou l�envoi des informations a provoqu� une erreur.', MESSAGE_TEXT_IT = 'Si � verificato un problema con la notifica e-mail dell�invito agli indirizzi e-mail selezionati. La raccolta o l�invio delle informazioni ha causato un errore.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations' AND REFERENCED_BY = 'SENDEMAILINVITATIONS_WFR' AND MESSAGE_ID = 'SENDEMAILINVITATIONSERROR';

UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = 'Projet d�Evaluation', PROJECT_TYPE_DE = 'Bewertung', PROJECT_TYPE_ES = 'Evaluaci�n', PROJECT_TYPE_FR = 'Evaluation', PROJECT_TYPE_IT = 'Valutazione', PROJECT_TYPE_NL = 'Evaluatie' WHERE PROJECT_TYPE = 'ASSESSMENT';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = 'Projet d��valuation environnementale', PROJECT_TYPE_DE = 'BEWERTUNG - UMWELT', PROJECT_TYPE_ES = 'EVALUACI�N - MEDIOAMBIENTAL', PROJECT_TYPE_FR = 'EVALUATION ENVIRONNEMENTALE', PROJECT_TYPE_IT = 'VALUTAZIONE - AMBIENTALE', PROJECT_TYPE_NL = 'EVALUATIE - MILIEUTECHNISCH' WHERE PROJECT_TYPE = 'ASSESSMENT - ENVIRONMENTAL';
UPDATE AFM.PROJECTTYPE SET DESCRIPTION_FR = 'Projet d��valuation des mati�res dangereuses', PROJECT_TYPE_DE = 'BEWERTUNG - GEFAHRENSTOFFE', PROJECT_TYPE_ES = 'EVALUACI�N - MATERIAL PELIGROSO', PROJECT_TYPE_FR = 'EVALUATION - MATIERES DANGEREUSES', PROJECT_TYPE_IT = 'VALUTAZIONE - MATERIALI POTENZIALMENTE PERICOLOS', PROJECT_TYPE_NL = 'EVALUATIE - GEVAARLIJKE STOFFEN' WHERE PROJECT_TYPE = 'ASSESSMENT - HAZMAT';
UPDATE AFM.PROJECTTYPE SET PROJECT_TYPE_DE = 'Abnahme', PROJECT_TYPE_ES = 'Puesta en servicio', PROJECT_TYPE_FR = 'Mise en service', PROJECT_TYPE_IT = 'Commissioning', PROJECT_TYPE_NL = 'Overdracht' WHERE PROJECT_TYPE = 'COMMISSIONING';
UPDATE AFM.PROJECTTYPE SET PROJECT_TYPE_DE = 'Umzug', PROJECT_TYPE_ES = 'Traslado', PROJECT_TYPE_FR = 'D�m�nagement', PROJECT_TYPE_IT = 'Spostamento', PROJECT_TYPE_NL = 'Verplaatsen' WHERE PROJECT_TYPE = 'Move';
UPDATE AFM.PROJECTTYPE SET PROJECT_TYPE_CH = 'N/A', PROJECT_TYPE_DE = 'nicht verf�gbar', PROJECT_TYPE_ES = 'N/A', PROJECT_TYPE_FR = 'ND', PROJECT_TYPE_IT = 'N/D', PROJECT_TYPE_NL = 'N.v.t.' WHERE PROJECT_TYPE = 'N/A';
UPDATE AFM.PROJECTTYPE SET PROJECT_TYPE_DE = 'Neubau', PROJECT_TYPE_ES = 'Nueva construcci�n', PROJECT_TYPE_FR = 'Nouvelle Construction', PROJECT_TYPE_IT = 'Nuova costruzione', PROJECT_TYPE_NL = 'Nieuwbouw' WHERE PROJECT_TYPE = 'New Construction';
UPDATE AFM.PROJECTTYPE SET PROJECT_TYPE_DE = 'Renovierung', PROJECT_TYPE_ES = 'Renovaci�n', PROJECT_TYPE_FR = 'R�novation', PROJECT_TYPE_IT = 'Ristrutturazione', PROJECT_TYPE_NL = 'Renovatie' WHERE PROJECT_TYPE = 'Renovation';
UPDATE AFM.PROJECTTYPE SET PROJECT_TYPE_DE = 'Ablauf', PROJECT_TYPE_ES = 'Escenario', PROJECT_TYPE_FR = 'Sc�nario', PROJECT_TYPE_IT = 'Scenario', PROJECT_TYPE_NL = 'Scenario' WHERE PROJECT_TYPE = 'Scenario';

UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Evaluaci�n', PROJ_PHASE_DE = 'Bewertung', PROJ_PHASE_FR = 'Evaluation', PROJ_PHASE_IT = 'Valutazione', PROJ_PHASE_NL = 'Evaluatie' WHERE PROJ_PHASE = 'Assessment';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Cierre', PROJ_PHASE_DE = 'Auslauf', PROJ_PHASE_FR = 'Cl�ture', PROJ_PHASE_IT = 'Chiusura', PROJ_PHASE_NL = 'Afsluiten en archiveren' WHERE PROJ_PHASE = 'Closeout';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Terminaci�n', PROJ_PHASE_DE = 'Erledigung', PROJ_PHASE_FR = 'Fin', PROJ_PHASE_IT = 'Completamento', PROJ_PHASE_NL = 'Voltooid' WHERE PROJ_PHASE = 'Completion';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Dise�o', PROJ_PHASE_DE = 'Architekturmanagement', PROJ_PHASE_FR = 'Conception', PROJ_PHASE_IT = 'Progettazione', PROJ_PHASE_NL = 'Ontwerp' WHERE PROJ_PHASE = 'Design';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Ejecuci�n', PROJ_PHASE_DE = 'Durchf�hrung', PROJ_PHASE_FR = 'R�alisation', PROJ_PHASE_IT = 'Esecuzione', PROJ_PHASE_NL = 'Uitvoering' WHERE PROJ_PHASE = 'Execution';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Financiaci�n', PROJ_PHASE_DE = 'Finanzierung', PROJ_PHASE_FR = 'Financement', PROJ_PHASE_IT = 'Finanziamento', PROJ_PHASE_NL = 'Financiering' WHERE PROJ_PHASE = 'Funding';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Mejora', PROJ_PHASE_DE = 'Verbesserung', PROJ_PHASE_FR = 'Am�lioration', PROJ_PHASE_IT = 'Miglioramento', PROJ_PHASE_NL = 'Verbetering' WHERE PROJ_PHASE = 'Improvement';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Iniciaci�n', PROJ_PHASE_DE = 'Initiierung', PROJ_PHASE_FR = 'Lancement', PROJ_PHASE_IT = 'Avvio', PROJ_PHASE_NL = 'Initialisatie' WHERE PROJ_PHASE = 'Initiation';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Mantenimiento', PROJ_PHASE_DE = 'Wartung', PROJ_PHASE_FR = 'Maintenance', PROJ_PHASE_IT = 'Manutenzione', PROJ_PHASE_NL = 'Onderhoud' WHERE PROJ_PHASE = 'Maintenance';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Gesti�n', PROJ_PHASE_DE = 'Management', PROJ_PHASE_FR = 'Gestion', PROJ_PHASE_IT = 'Gestione', PROJ_PHASE_NL = 'Beheer' WHERE PROJ_PHASE = 'Management';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Planificaci�n', PROJ_PHASE_DE = 'Planung', PROJ_PHASE_FR = 'Planification', PROJ_PHASE_IT = 'Pianificazione', PROJ_PHASE_NL = 'Planning' WHERE PROJ_PHASE = 'Planning';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Adquisici�n', PROJ_PHASE_DE = 'Beschaffung', PROJ_PHASE_FR = 'Acquisition', PROJ_PHASE_IT = 'Approvvigionamento', PROJ_PHASE_NL = 'Aankoop' WHERE PROJ_PHASE = 'Procurement';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Programaci�n', PROJ_PHASE_DE = 'Programmierung', PROJ_PHASE_FR = 'Programmation', PROJ_PHASE_IT = 'Programmazione', PROJ_PHASE_NL = 'Programmering' WHERE PROJ_PHASE = 'Programming';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Comprando', PROJ_PHASE_DE = 'Kauf', PROJ_PHASE_FR = 'Achat', PROJ_PHASE_IT = 'Acquisto', PROJ_PHASE_NL = 'Aankopen' WHERE PROJ_PHASE = 'Purchasing';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Repair', PROJ_PHASE_DE = 'Repair', PROJ_PHASE_FR = 'R�paration', PROJ_PHASE_IT = 'Ripara', PROJ_PHASE_NL = 'Herstellen' WHERE PROJ_PHASE = 'Repair';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Alcance', PROJ_PHASE_DE = 'Umfangsbestimmung', PROJ_PHASE_FR = 'Port�e', PROJ_PHASE_IT = 'Ambito', PROJ_PHASE_NL = 'Scope vastleggen' WHERE PROJ_PHASE = 'Scoping';
UPDATE AFM.PROJPHASE SET PROJ_PHASE_ES = 'Apagar/iniciar', PROJ_PHASE_DE = 'Stopp/Start', PROJ_PHASE_FR = 'Arr�t/d�marrage', PROJ_PHASE_IT = 'Spegnimento/Avvio', PROJ_PHASE_NL = 'Afsluiten/Opstarten' WHERE PROJ_PHASE = 'Shutdown/Startup';

UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = 'Evaluation mobile - Questionnaire Unit� d�a�ration' WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - AHU';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = 'Questionnaire de bon de d�m�nagement pour l��quipement' WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment';
UPDATE AFM.QUESTIONNAIRE SET TITLE_FR = 'Questionnaire de bon de d�m�nagement pour les d�parts d�employ�s' WHERE QUESTIONNAIRE_ID = 'Move Order - Leaving';

UPDATE AFM.QUESTIONS SET ENUM_LIST_FR = '20CRT;CRT 20�;24FLAT;�cran plat 24�;30FLAT;�cran plat 30�' WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - ASSESSMENT - ENVIRONMENTAL' AND QUEST_NAME = 'MONITOR_MODEL';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Circuits d�air et de gaz de combustion�?' WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Boiler' AND QUEST_NAME = 'air-and-flue-gas-systems';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Hauteur nette d�aspiration' WHERE QUESTIONNAIRE_ID = 'ASSESSMENT - CA - Asset - Pump' AND QUEST_NAME = 'net-positive-suction-head';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Station d�accueil ?' WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'docking-station';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Syst�me d�exploitation ?' WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'operating-system';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Configurer l�ordinateur ?' WHERE QUESTIONNAIRE_ID = 'Action - Move-Data' AND QUEST_NAME = 'setup-computer';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Besoin d�une ligne modem/t�l�copie ?' WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice' AND QUEST_NAME = 'fax-modem';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Type d�appareil t�l�phonique :' WHERE QUESTIONNAIRE_ID = 'Action - Move-Voice' AND QUEST_NAME = 'phone-type';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Besoin d�assemblage ?' WHERE QUESTIONNAIRE_ID = 'Move Order - Asset' AND QUEST_NAME = 'require-assembly';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_CH = '�������' WHERE QUESTIONNAIRE_ID = 'Move Order - Employee' AND QUEST_NAME = 'ergonomic-chair';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_CH = '�����' WHERE QUESTIONNAIRE_ID = 'Move Order - Employee' AND QUEST_NAME = 'name-plate';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Besoin d�une connexion donn�es ?' WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'data-connection';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Besoin d�une connexion t�l�phone ?' WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'phone-connection';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Besoin d�aide pour la configuration ?' WHERE QUESTIONNAIRE_ID = 'Move Order - Equipment' AND QUEST_NAME = 'require-setup';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'R�initialiser l�ordinateur ?' WHERE QUESTIONNAIRE_ID = 'Move Order - Leaving' AND QUEST_NAME = 'reset-computer';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_CH = '�������' WHERE QUESTIONNAIRE_ID = 'Move Order - New Hire' AND QUEST_NAME = 'ergonomic-chair';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_CH = '�����' WHERE QUESTIONNAIRE_ID = 'Move Order - New Hire' AND QUEST_NAME = 'name-plate';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_CH = '�����' WHERE QUESTIONNAIRE_ID = 'Move Order - Room' AND QUEST_NAME = 'name-plate';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Circonstances d�initialisation du projet ?' WHERE QUESTIONNAIRE_ID = 'Project - New Construction' AND QUEST_NAME = 'TriggeringCircumstances';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'D�autres d�partements sont-ils affect�s ?' WHERE QUESTIONNAIRE_ID = 'Project - Renovation' AND QUEST_NAME = 'OtherDepartments';
UPDATE AFM.QUESTIONS SET QUEST_TEXT_FR = 'Quelles sont les circonstances qui ont conduit � l�initialisation du projet de r�novation ?' WHERE QUESTIONNAIRE_ID = 'Project - Renovation' AND QUEST_NAME = 'TriggeringCircumstances';

COMMIT;
SPOOL OFF;
quit;