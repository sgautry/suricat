SPOOL 15-change-connector-id.txt
SET TERMOUT OFF;
--  Change connector ID
alter table afm.afm_conn_flds disable constraint afm_conn_flds_connector_id;
delete from afm_conn_log where connector_id='Import DECISIS Operations';
update afm_connector set connector_id='Import DECISIS Operation' where connector_id='Import DECISIS Operations';
update afm_conn_flds set connector_id='Import DECISIS Operation' where connector_id='Import DECISIS Operations';
alter table afm.afm_conn_flds enable constraint afm_conn_flds_connector_id;
commit;

SPOOL OFF;
quit;
