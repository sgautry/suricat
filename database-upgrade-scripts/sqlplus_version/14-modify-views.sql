
SPOOL 14-modify-views.txt
SET TERMOUT OFF;
--  UPDATE VIEW "ROOMS_OCCUPANCY" 
CREATE OR REPLACE FORCE VIEW "AFM"."ROOMS_OCCUPANCY" ("SITE_ID", "BL_ID", "SHORT_BL_ID", "FL_ID", "RM_ID", "OCCUPANCY", "HPATTERN_ACAD", "OCCUPANCY_LEVEL1", "OCCUPANCY_LEVEL2", "OCCUPANCY_LEVEL3", "OCCUPANCY_LEVEL4", "RM_CAT", "RM_TYPE", "EXPLOITABLE", "VALUABLE", "VALO_COMMENT") AS 
SELECT bl.site_id, bl.bl_id, bl.short_bl_id, fl.fl_id, rm.rm_id, (select param_value from afm_activity_params where param_id = 'External_Label' and activity_id='AbRPLMPortfolioAdministration') AS occupancy, 
(SELECT param_value FROM afm_activity_params WHERE param_id = 'External' AND activity_id='AbRPLMPortfolioAdministration') AS hpattern_acad,
ar_tiers_act.rs_id AS occupancy_level1, ar_tiers_act.act_id AS occupancy_level2, 
ar_tiers_ssact.ssact_id AS occupancy_level3, ar_tiers_ens.ens_id AS occupancy_level4,
rmcat.rm_cat, rmtype.rm_type, rmcat.exploitable, rm.valuable, rm.valo_comment FROM rm
LEFT OUTER JOIN rmcat ON rmcat.rm_cat = rm.rm_cat
LEFT OUTER JOIN rmtype ON rmtype.rm_cat = rm.rm_cat AND rmtype.rm_type = rm.rm_type
INNER JOIN fl ON rm.fl_id = fl.fl_id AND rm.bl_id = fl.bl_id
INNER JOIN bl ON fl.bl_id = bl.bl_id
INNER JOIN ls ON rm.ls_id = ls.ls_id
INNER JOIN ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id
INNER JOIN ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id
INNER JOIN ar_tiers_act ON ar_tiers_ssact.act_id = ar_tiers_act.act_id
WHERE ls.tn_name_ext IS NOT NULL
UNION
SELECT bl.site_id, bl.bl_id, bl.short_bl_id, fl.fl_id, rm.rm_id, (select param_value from afm_activity_params where param_id = 'Internal_Label' and activity_id='AbRPLMPortfolioAdministration') AS occupancy, 
(SELECT param_value FROM afm_activity_params WHERE param_id = 'Internal' AND activity_id='AbRPLMPortfolioAdministration') AS hpattern_acad,
bu.epic_id AS occupancy_level1, bu.bu_id AS occupancy_level2, 
dv.dv_id AS occupancy_level3, dp.dp_id AS occupancy_level4,
rmcat.rm_cat, rmtype.rm_type, rmcat.exploitable, rm.valuable, rm.valo_comment FROM rm
LEFT OUTER JOIN rmcat ON rmcat.rm_cat = rm.rm_cat
LEFT OUTER JOIN rmtype ON rmtype.rm_cat = rm.rm_cat AND rmtype.rm_type = rm.rm_type
INNER JOIN fl ON rm.fl_id = fl.fl_id AND rm.bl_id = fl.bl_id
INNER JOIN bl ON fl.bl_id = bl.bl_id
INNER JOIN ls ON rm.ls_id = ls.ls_id
INNER JOIN dp ON ls.tn_name = dp.dp_id
INNER JOIN dv ON dp.dv_id = dv.dv_id
INNER JOIN bu ON dv.bu_id = bu.bu_id
WHERE ls.tn_name IS NOT NULL
UNION
SELECT bl.site_id, bl.bl_id, bl.short_bl_id, fl.fl_id, rm.rm_id, (select param_value from afm_activity_params where param_id = 'Free_Label' and activity_id='AbRPLMPortfolioAdministration') AS occupancy, 
(SELECT param_value FROM afm_activity_params WHERE param_id = 'Free' AND activity_id='AbRPLMPortfolioAdministration') AS hpattern_acad,
'' AS occupancy_level1, '' AS occupancy_level2, 
'' AS occupancy_level3, '' AS occupancy_level4,
rmcat.rm_cat, rmtype.rm_type, rmcat.exploitable, rm.valuable, rm.valo_comment FROM rm
LEFT OUTER JOIN rmcat ON rmcat.rm_cat = rm.rm_cat
LEFT OUTER JOIN rmtype ON rmtype.rm_cat = rm.rm_cat AND rmtype.rm_type = rm.rm_type
LEFT OUTER JOIN fl ON rm.fl_id = fl.fl_id AND rm.bl_id = fl.bl_id
LEFT OUTER JOIN bl ON fl.bl_id = bl.bl_id
LEFT OUTER JOIN ls ON rm.ls_id = ls.ls_id
WHERE rm.ls_id IS NULL;

--  UPDATE VIEW "ROOMS_OCCUPANCY_REGULATION" 
CREATE OR REPLACE FORCE VIEW "AFM"."ROOMS_OCCUPANCY_REGULATION" ("BL_ID", "FL_ID", "RM_ID", "OCCUPANCY", "HPATTERN_ACAD") AS 
SELECT bl.bl_id, fl.fl_id, rm.rm_id, 'R�gul� pour GC' AS occupancy, (SELECT param_value FROM afm_activity_params WHERE param_id = 'Regulated' AND activity_id='AbRPLMPortfolioAdministration') AS hpattern_acad
FROM rm
INNER JOIN fl ON rm.fl_id = fl.fl_id AND rm.bl_id = fl.bl_id
INNER JOIN bl ON fl.bl_id = bl.bl_id
INNER JOIN ls ON rm.ls_id = ls.ls_id
WHERE ls.ls_cat = 'Regulates'
UNION
SELECT bl.bl_id, fl.fl_id, rm.rm_id, 'Non R�gul� pour GC' AS occupancy, (SELECT param_value FROM afm_activity_params WHERE param_id = 'Non-Regulated' AND activity_id='AbRPLMPortfolioAdministration') AS hpattern_acad
FROM rm
INNER JOIN fl ON rm.fl_id = fl.fl_id AND rm.bl_id = fl.bl_id
INNER JOIN bl ON fl.bl_id = bl.bl_id
INNER JOIN ls ON rm.ls_id = ls.ls_id
WHERE ls.ls_cat = 'No regulates'
UNION
SELECT bl.bl_id, fl.fl_id, rm.rm_id, (select param_value from afm_activity_params where param_id = 'Free_Label' and activity_id='AbRPLMPortfolioAdministration') AS occupancy, (SELECT param_value FROM afm_activity_params WHERE param_id = 'Free' AND activity_id='AbRPLMPortfolioAdministration') AS hpattern_acad
FROM rm
LEFT OUTER JOIN fl ON rm.fl_id = fl.fl_id AND rm.bl_id = fl.bl_id
LEFT OUTER JOIN bl ON fl.bl_id = bl.bl_id
LEFT OUTER JOIN ls ON rm.ls_id = ls.ls_id
WHERE rm.ls_id IS NULL;

UPDATE afm_tbls set is_sql_view = 1 WHERE upper(table_name) in (select upper(view_name) from all_views) and is_sql_view=0;
COMMIT;

SPOOL OFF;
quit;

