SPOOL 07-dml-changes-between-standard-v231-and-v232.txt
SET TERMOUT OFF;
SET SQLBLANKLINES ON;
SET DEFINE OFF;
ALTER SESSION SET NLS_DATE_FORMAT = 'MM/DD/SYYYY HH24:MI:SS';
ALTER SESSION SET NLS_TIMESTAMP_TZ_FORMAT = 'MM/DD/SYYYY HH24:MI:SS.FF TZH:TZM';
ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'MM/DD/SYYYY HH24:MI:SS.FF';
ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,';
ALTER SESSION SET NLS_NCHAR_CONV_EXCP = FALSE;
ALTER SESSION SET TIME_ZONE = '+05:30';

UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'Bewertung', ACTIVITY_TYPE_ES = 'Evaluaci�n', ACTIVITY_TYPE_FR = 'Evaluation', ACTIVITY_TYPE_IT = 'Valutazione', ACTIVITY_TYPE_NL = 'Evaluatie' WHERE ACTIVITY_TYPE = 'ASSESSMENT';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'BEWERTUNG - GEFAHRENSTOFFE', ACTIVITY_TYPE_ES = 'EVALUACI�N - MATERIAL PELIGROSO', ACTIVITY_TYPE_FR = 'EVALUATION - MATIERES DANGEREUSES', ACTIVITY_TYPE_IT = 'VALUTAZIONE - MATERIALI POTENZIALMENTE PERICOLOSI', ACTIVITY_TYPE_NL = 'EVALUATIE - GEVAARLIJKE STOFFEN' WHERE ACTIVITY_TYPE = 'ASSESSMENT - HAZMAT';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - ABNAHME', ACTIVITY_TYPE_ES = 'PS - PUESTA EN SERVICIO', ACTIVITY_TYPE_FR = 'CX - MISE EN SERVICE', ACTIVITY_TYPE_IT = 'CX - COMMISSIONING', ACTIVITY_TYPE_NL = 'CX - OVERDRACHT' WHERE ACTIVITY_TYPE = 'CX - COMMISSIONING';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - KONSTRUKTIONSCHECKLISTEN', ACTIVITY_TYPE_ES = 'PS - LISTAS DE VERIFICACI�N DE CONSTRUCCI�N', ACTIVITY_TYPE_FR = 'CX - LISTE DE CONTR�LE DE CONSTRUCTION', ACTIVITY_TYPE_IT = 'CX - LISTE DI CONTROLLO COSTRUZIONE', ACTIVITY_TYPE_NL = 'CX - CONSTRUCTIECHECKLISTS' WHERE ACTIVITY_TYPE = 'CX - CONSTRUCTION CHECKLISTS';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - VERTRAGSDOKUMENTE', ACTIVITY_TYPE_ES = 'PS - DOCUMENTOS DE CONTRATOS', ACTIVITY_TYPE_FR = 'CX - DOCUMENTS RELATIFS AUX CONTRATS', ACTIVITY_TYPE_IT = 'CX - DOCUMENTI CONTRATTO', ACTIVITY_TYPE_NL = 'CX - CONTRACTDOCUMENTEN' WHERE ACTIVITY_TYPE = 'CX - CONTRACT DOCUMENTS';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - AUSGESTELLTE VERTR�GE', ACTIVITY_TYPE_ES = 'PS - CONTRATOS ADJUDICADOS', ACTIVITY_TYPE_FR = 'CX - CONTRATS ATTRIBUES', ACTIVITY_TYPE_IT = 'CX - CONTRATTI AGGIUDICATI', ACTIVITY_TYPE_NL = 'CX - TOEGEKENDE CONTRACTEN' WHERE ACTIVITY_TYPE = 'CX - CONTRACTS AWARDED';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - ENTWURFSBEITR�GE', ACTIVITY_TYPE_ES = 'PS - PRESENTACIONES DE DISE�OS', ACTIVITY_TYPE_FR = 'CX - SOUMISSIONS DE CONCEPTION', ACTIVITY_TYPE_IT = 'CX - PRESENTAZIONI DI PROGETTI', ACTIVITY_TYPE_NL = 'CX - ONTWERPAANVRAGEN' WHERE ACTIVITY_TYPE = 'CX - DESIGN SUBMISSIONS';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - PROBLEMEPROTOKOLL', ACTIVITY_TYPE_ES = 'PS - REGISTRO DE PROBLEMAS', ACTIVITY_TYPE_FR = 'CX - REGISTRE DES PROBLEMES', ACTIVITY_TYPE_IT = 'CX - REGISTRO DEI PROBLEMI', ACTIVITY_TYPE_NL = 'CX - PROBLEMENLOGBOEK' WHERE ACTIVITY_TYPE = 'CX - ISSUES LOG';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - INSTANDHALTUNGS- UND WARTUNGSPR�FUNG', ACTIVITY_TYPE_ES = 'PS - REVISI�N DE OPERACIONES Y MANTENIMIENTO', ACTIVITY_TYPE_FR = 'CX - EXAMEN DES OPERATIONS ET DE LA MAINTENANCE', ACTIVITY_TYPE_IT = 'CX - REVISIONE GESTIONE E MANUTENZIONE', ACTIVITY_TYPE_NL = 'CX - EVALUATIE BEDRIJFSVOERING EN ONDERHOUD' WHERE ACTIVITY_TYPE = 'CX - OPERATION AND MAINTENANCE REVIEW';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - HANDBUCH ZUM WIEDERINBETRIEBNAHMEMANAGEMENT', ACTIVITY_TYPE_ES = 'PS - MANUAL PARA GESTIONAR NUEVAS PUESTAS EN SERVICIO', ACTIVITY_TYPE_FR = 'CX - MANUEL DE GESTION DE LA REMISE EN SERVICE', ACTIVITY_TYPE_IT = 'CX - MANUALE GESTIONE RECOMMISSIONING', ACTIVITY_TYPE_NL = 'CX - INGEBRUIKNAME MANAGEMENTHANDLEIDING' WHERE ACTIVITY_TYPE = 'CX - RECOMMISSIONING MANAGEMENT MANUAL';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - ZUSAMMENFASSUNGSBERICHT', ACTIVITY_TYPE_ES = 'PS - INFORME DE RESUMEN', ACTIVITY_TYPE_FR = 'CX - RAPPORT DE SYNTHESE', ACTIVITY_TYPE_IT = 'CX - REPORT RIEPILOGO', ACTIVITY_TYPE_NL = 'CX - OVERZICHTSRAPPORT' WHERE ACTIVITY_TYPE = 'CX - SUMMARY REPORT';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - DATENS�TZE MIT TESTDATEN', ACTIVITY_TYPE_ES = 'PS - REGISTROS DE DATOS DE PRUEBAS', ACTIVITY_TYPE_FR = 'CX - ENREGISTREMENTS DES DONNEES DE TEST', ACTIVITY_TYPE_IT = 'CX - RECORD DATI TEST', ACTIVITY_TYPE_NL = 'CX - GEGEVENSRECORDS TESTEN' WHERE ACTIVITY_TYPE = 'CX - TESTING DATA RECORDS';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - TESTVERFAHREN', ACTIVITY_TYPE_ES = 'PS - PROCEDIMIENTOS DE PRUEBAS', ACTIVITY_TYPE_FR = 'CX - PROCEDURES DE TEST', ACTIVITY_TYPE_IT = 'CX - PROCEDURE TEST', ACTIVITY_TYPE_NL = 'CX - TESTPROCEDURES' WHERE ACTIVITY_TYPE = 'CX - TESTING PROCEDURES';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - SCHULUNGSDOKUMENTATION', ACTIVITY_TYPE_ES = 'PS - DOCUMENTACI�N DE FORMACI�N', ACTIVITY_TYPE_FR = 'CX - DOCUMENTATION DE FORMATION', ACTIVITY_TYPE_IT = 'CX - DOCUMENTAZIONE FORMAZIONE', ACTIVITY_TYPE_NL = 'CX - TRAININGSDOCUMENTATIE' WHERE ACTIVITY_TYPE = 'CX - TRAINING DOCUMENTATION';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'CX - GARANTIEPR�FUNG', ACTIVITY_TYPE_ES = 'PS - REVISI�N DE GARANT�A', ACTIVITY_TYPE_FR = 'CX - EXAMEN DE LA GARANTIE', ACTIVITY_TYPE_IT = 'CX - REVISIONE GARANZIA', ACTIVITY_TYPE_NL = 'CX - GARANTIEBEOORDELING' WHERE ACTIVITY_TYPE = 'CX - WARRANTY REVIEW';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - LUFT�BERWACHUNG', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - CONTROL DEL AIRE', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - SUIVI DE LA QUALITE DE L�AIR', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - MONITORAGGIO ARIA', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - BEWAKING LUCHTKWALITEIT' WHERE ACTIVITY_TYPE = 'HAZMAT - AIR MONITORING';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - ST�RUNG', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - PERTURBACI�N', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - PERTURBATION', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - DISTURBO', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - VERSTORING' WHERE ACTIVITY_TYPE = 'HAZMAT - DISTURBANCE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - EVAKUIERUNG', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - EVACUACI�N', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - EVACUATION', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - EVACUAZIONE', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - EVACUATIE' WHERE ACTIVITY_TYPE = 'HAZMAT - EVACUATION';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - ARBEITSUNFALL', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - INCIDENTE', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - ACCIDENT', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - INCIDENTE', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - INCIDENT' WHERE ACTIVITY_TYPE = 'HAZMAT - INCIDENT';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - LABORTEST', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - PRUEBAS DE LABORATORIO', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - TESTS EN LABORATOIRE', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - TEST DI LABORATORIO', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - LABTESTS' WHERE ACTIVITY_TYPE = 'HAZMAT - LAB TESTING';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - QUARANT�NE', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - CUARENTENA', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - QUARANTAINE', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - QUARANTENA', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - QUARANTAINE' WHERE ACTIVITY_TYPE = 'HAZMAT - QUARANTINE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - ERFASSUNG', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - INSPECCI�N F�SICA', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - RELEVE', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - RILIEVO', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - SURVEY' WHERE ACTIVITY_TYPE = 'HAZMAT - SURVEY';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - PROBENENTNAHME', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - TOMA DE MUESTRAS', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - PRISE D�ECHANTILLONS', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - PRELIEVO CAMPIONI', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - SAMPLES NEMEN' WHERE ACTIVITY_TYPE = 'HAZMAT - TAKE SAMPLES';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - AUFGABE', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - TAREA', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - TACHE', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - COMPITO', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - TAAK' WHERE ACTIVITY_TYPE = 'HAZMAT - TASK';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'GEFAHRENSTOFFE - VERSTOSS', ACTIVITY_TYPE_ES = 'MATERIAL PELIGROSO - INCUMPLIMIENTO', ACTIVITY_TYPE_FR = 'MATIERES DANGEREUSES - INFRACTION', ACTIVITY_TYPE_IT = 'MATERIALI POTENZIALMENTE PERICOLOSI - VIOLAZIONE', ACTIVITY_TYPE_NL = 'GEVAARLIJKE STOFFEN - OVERTREDING' WHERE ACTIVITY_TYPE = 'HAZMAT - VIOLATION';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_CH = NULL, MENU_ICON = 'reservations_sq.jpg' WHERE ACTIVITY_TYPE = 'MOBILE_RESERVATIONS';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'PROJEKT - �NDERUNGSAUFTRAG', ACTIVITY_TYPE_ES = 'PROYECTO - ORDEN DE CAMBIO', ACTIVITY_TYPE_FR = 'PROJET - BON DE MODIFICATION', ACTIVITY_TYPE_IT = 'PROGETTO - ORDINE DI MODIFICA', ACTIVITY_TYPE_NL = 'PROJECT - WIJZIGINGSOPDRACHT' WHERE ACTIVITY_TYPE = 'PROJECT - CHANGE ORDER';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'PROJEKT - DOKUMENT', ACTIVITY_TYPE_ES = 'PROYECTO - DOCUMENTO', ACTIVITY_TYPE_FR = 'PROJET - DOCUMENT', ACTIVITY_TYPE_IT = 'PROGETTO - DOCUMENTO', ACTIVITY_TYPE_NL = 'PROJECT - DOCUMENT' WHERE ACTIVITY_TYPE = 'PROJECT - DOCUMENT';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'PROJEKT - MEILENSTEIN', ACTIVITY_TYPE_ES = 'PROYECTO - HITO', ACTIVITY_TYPE_FR = 'PROJET - JALON', ACTIVITY_TYPE_IT = 'PROGETTO - TAPPA', ACTIVITY_TYPE_NL = 'PROJECT - MIJLPAAL' WHERE ACTIVITY_TYPE = 'PROJECT - MILESTONE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'PROJEKT - AUFGABE', ACTIVITY_TYPE_ES = 'PROYECTO - TAREA', ACTIVITY_TYPE_FR = 'PROJET - TACHE', ACTIVITY_TYPE_IT = 'PROGETTO - COMPITO', ACTIVITY_TYPE_NL = 'PROJECT - TAAK' WHERE ACTIVITY_TYPE = 'PROJECT - TASK';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'VORGESCHLAGENER PROJEKTSTANDORT', ACTIVITY_TYPE_ES = 'UBICACI�N DE PROYECTO PROPUESTA', ACTIVITY_TYPE_FR = 'EMPLACEMENT DE PROJETS PROPOSES', ACTIVITY_TYPE_IT = 'POSIZIONE PROGETTO PROPOSTO', ACTIVITY_TYPE_NL = 'VOORGESTELDE PROJECTLOCATIE' WHERE ACTIVITY_TYPE = 'PROPOSED PROJECT LOCATION';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_CH = NULL, MENU_ICON = 'copier_sq.jpg' WHERE ACTIVITY_TYPE = 'SERVICE DESK - COPY SERVICE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_CH = NULL, MENU_ICON = 'furniture_sq.jpg' WHERE ACTIVITY_TYPE = 'SERVICE DESK - FURNITURE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_DE = 'SERVICEDESK - GRUPPENUMZUG', ACTIVITY_TYPE_ES = 'CENTRO DE SOPORTE - TRASLADO DE GRUPO', ACTIVITY_TYPE_FR = 'CENTRE DE SERVICES - D�M�NAGEMENT DE GROUPE', ACTIVITY_TYPE_IT = 'SERVICE DESK - SPOSTAMENTO GRUPPI', ACTIVITY_TYPE_NL = 'SERVICEDESK - GROEPSVERHUIZING' WHERE ACTIVITY_TYPE = 'SERVICE DESK - GROUP MOVE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_CH = NULL, MENU_ICON = 'hotelling_sq.jpg' WHERE ACTIVITY_TYPE = 'SERVICE DESK - HOTELING';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_CH = NULL WHERE ACTIVITY_TYPE = 'SERVICE DESK - INDIVIDUAL MOVE';
UPDATE AFM.ACTIVITYTYPE SET ACTIVITY_TYPE_CH = NULL, MENU_ICON = 'maintenance_sq.jpg' WHERE ACTIVITY_TYPE = 'SERVICE DESK - MAINTENANCE';

UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les gestionnaires des actifs doivent g�rer efficacement les actifs tout au long de leur cycle de vie op�rationnel, de leur acceptation � leur �limination en passant par leur exploitation.� L�application offre une vue int�gr�e de tous les actifs, notamment les propri�t�s, les b�timents, les terrains, les structures, les �quipements et le mobilier.� Les applications mobiles mettent � disposition des interfaces d�acceptation des actifs et de relev� de terrain directement l� o� l�activit� est r�alis�e.� Les consoles Web apportent de la transparence en permettant de suivre la valeur, l�emplacement, l��tat et les co�ts associ�s aux actifs.� Les fonctions int�gr�es facilitent le travail des �quipes informatiques et de gestion de maintenance.', SUMMARY_IT = 'I responsabili asset devono amministrare efficacemente gli asset nell�arco dell�intero ciclo di vita operativo, dall�accettazione fino all�uso pratico e allo smaltimento.� L�applicazione fornisce una visualizzazione integrata di tutti gli asset, incluse propriet�, edifici, terreni, strutture, apparecchiatura e arredi.� Le app mobili forniscono interfacce del punto di attivit� per l�accettazione degli asset e i rilievi sul campo.� Le console Web-based forniscono trasparenza a valore, posizione, stato e costo degli asset.� Le funzionalit� integrate supportano il lavoro della divisione IT e della gestione dell�edificio.', TITLE_NL = 'Assetbeheer' WHERE ACTIVITY_ID = 'AbAssetAM';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les gestionnaires d�actifs doivent s�assurer que l�ensemble de leurs immobilisations est en mesure de fournir les ressources coordonn�es et de bout en bout n�cessaires � la r�ussite de la mission de l�activit�. Cette application coordonne le sch�ma strat�gique de l�immobilier, le programme d�investissements et le plan d�actifs dans un seul cadre qui regroupe toutes les parties prenantes et les aligne � la mission de l�entreprise. Gr�ce � ses interfaces sp�cifiquement con�ues pour les professionnels de la finance, de l�immobilier, du patrimoine, de l�informatique et de la maintenance, et gr�ce aux applications mobiles destin�es au personnel sur le terrain et au quai d�exp�dition, ARCHIBUS acc�l�re la t�che de l�ensemble de l��quipe de gestion des actifs et enregistre des donn�es pr�cises sur l��limination des actifs en tant que processus naturel d�coulant de l�activit� quotidienne. Les analyses optimisent la valeur tout au long du cycle de vie des actifs. Un registre des actifs int�gr� permet une vue panoramique de la propri�t�, de l�emplacement, de l��tat et de la valeur de tous les actifs.', SUMMARY_IT = 'I responsabili asset devono assicurare che la loro base di asset capitali fornisca l�assegnazione di risorse coordinata end-to-end necessaria per garantire il successo della missione della linea. Questa applicazione coordina piano strategico del patrimonio immobiliare, piano capitale e piano asset all�interno di un quadro con operativit� comune che allinea tutte le parti in causa alla missione. Grazie ad apposite interfacce integrate per i professionisti delle divisioni finanza, patrimonio immobiliare, strutture, IT, manutenzione e grazie alle app mobili per il personale sul campo e l�area spedizioni, ARCHIBUS velocizza il lavoro dell�intero team di gestione degli asset e registra dati precisi sulla relativa disposizione in quanto conseguenza naturale dei processi giornalieri. Le analisi ottimizzano il valore attraverso il ciclo di vita dell�asset. Un registro asset integrato fornisce una visuale a 360� della propriet�, della posizione, dello stato e del valore di tutti gli asset.', TITLE_FR = 'Gestion des actifs d�entreprise', TITLE_NL = 'Beheer berijfsassets' WHERE ACTIVITY_ID = 'AbAssetEAM';
UPDATE AFM.AFM_ACTIVITIES SET IS_ACTIVE = 0, SUMMARY_FR = 'Les gestionnaires de patrimoine savent � quel point la bonne gestion des actifs tels que meubles, �quipements et logiciels contribue � la bonne marche de l�entreprise.  Cependant, g�rer les �volutions des biens et du personnel tout en gardant la ma�trise des co�ts peut repr�senter une difficult� majeure.  Cette activit� apporte de la transparence en permettant de suivre la valeur, l�emplacement et l�utilisation des actifs.  Elle permet aux gestionnaires de patrimoine d��tayer leurs prises de d�cision, de r�orienter les biens stock�s ou sous-utilis�s pour r�duire les co�ts, de dimensionner les bases d�actifs en fonction des conditions d�assurance et des besoins de continuit� d�activit� afin de r�duire les risques, et d�obtenir plus facilement des donn�es d�amortissement pr�cises pour satisfaire les normes comptables en vigueur.', SUMMARY_IT = 'I responsabili delle strutture sanno che una gestione efficace dei beni, quali l�arredamento, le apparecchiature e il software, � fondamentale per la mission dell�azienda.  La gestione della sostituzione dei beni e del personale mantenendo il controllo dei costi pu� risultare complessa.  Questa attivit� offre trasparenza relativamente al valore dei beni, alla posizione e alla disposizione.  Ci� consente ai responsabili delle strutture di aumentare la responsabilit� dell�organizzazione, riproporre beni a magazzino o sottoutilizzati a basso costo, ridimensionare la base dei beni per l�assicurazione e la continuit� aziendale a basso rischio e fornire in modo semplice informazioni precise sull�ammortamento in conformit� agli standard di contabilit�.', TITLE_FR = 'Portail d�actifs' WHERE ACTIVITY_ID = 'AbAssetManagement';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Pour le personnel responsable de la gestion des actifs informatiques mat�riels et de leurs connexions, l�application Gestion des actifs de t�l�communications d�ARCHIBUS fournit un inventaire complet de l�ensemble des �quipements t�l�com de pi�ce, des �l�ments r�seau, des baies, des platines, des prises et des �quipements de postes de travail, ainsi qu�un plan de connectivit�.� L�application est en mesure d�identifier les d�partements responsables des actifs informatiques, le secteur d�activit� dans lequel les actifs sont utilis�s, l�emplacement physique des actifs, ainsi que leurs statuts d�utilisation et leurs �tats.', SUMMARY_IT = 'Per i membri del personale responsabili della gestione delle risorse IT fisiche e delle loro connessioni, l�applicazione Gestione asset Telecomunicazioni ARCHIBUS fornisce un inventario completo di tutta l�apparecchiatura del locale apparecchiature, dei dispositivi di rete, degli armadi, delle placche, dei jack e dell�apparecchiatura per workstation, oltre a una mappa della sua connettivit�.� L�applicazione � in grado di tracciare le risorse IT al reparto responsabile dell�asset; l�area di attivit� supportata dall�asset; e la posizione fisica in cui si trova l�asset, oltre al suo stato di utilizzo e alla sua condizione.', TITLE_NL = 'Beheer telecomassets' WHERE ACTIVITY_ID = 'AbAssetTAM';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Consulter des informations de gestion compl�tes donnant acc�s aux bons de travaux archiv�s ainsi qu�aux donn�es de performance en termes de main d�oeuvre, bons de travaux et co�ts.' WHERE ACTIVITY_ID = 'AbBldgOpsAnalysis';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'D�finir les donn�es fondamentales concernant l��quipement, la main d�oeuvre, les pi�ces d�tach�es, les outils, les techniciens et les types de probl�mes.' WHERE ACTIVITY_ID = 'AbBldgOpsBackgroundData';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Pour les responsables de centres de services et les prestataires devant g�rer un nombre croissant de demandes d�intervention tout en continuant � rester performants et organisationnellement efficaces, cette application est un outil pr�cieux�: elle permet de proposer une assistance en libre-service ininterrompue (24�heures sur 24, sept jours sur sept), et de souligner les obligations de performance des intervenants par le biais de contrats de niveau de service. Les responsables de centres de services peuvent configurer des workflows sp�cialis�s pour r�duire leurs frais g�n�raux d�administration et pour responsabiliser les prestataires en appliquant des mesures de r�sultats.', SUMMARY_IT = 'Per i responsabili Service Desk e i fornitori di servizi che hanno l�esigenza di gestire un numero crescente di richieste di servizio mantenendo i livelli di prestazioni dei servizi e di efficienza aziendale, questa applicazione fornisce l�assistenza self-service 24x7 e garantisce le prestazioni dei fornitori di servizi attraverso contratti di servizio. I responsabili Service Desk possono configurare flussi di lavoro service desk per ridurre il carico amministrativo e per ritenere i fornitori di servizio responsabili attraverso risultati misurabili.' WHERE ACTIVITY_ID = 'AbBldgOpsHelpDesk';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Pour les responsables, superviseurs et techniciens des b�timents et de la maintenance devant suivre les nouvelles demandes de maintenance, g�rer les prestataires de services, planifier les interventions et les ressources et rendre des comptes sur la base d�indicateurs-cl�s, cette application constitue une solution compl�te. Elle leur permet de cr�er des workflows sp�cifiques aux probl�mes et d�automatiser les processus de maintenance � l�intention des clients, des approbateurs, des superviseurs et des techniciens.  Les superviseurs peuvent planifier les t�ches graphiquement, tandis que les responsables peuvent consolider les donn�es afin d�identifier et de corriger les tendances n�gatives.', SUMMARY_IT = 'Per i responsabili, i supervisori e gli addetti degli edifici e della manutenzione che hanno l�esigenza di registrare le richieste di manutenzione, gestire i fornitori di servizi, pianificare il lavoro e le risorse e creare reportistiche decisionali basate su indicatori di prestazione, questa applicazione rappresenta una soluzione completa che consente di definire flussi di lavoro personalizzati in base a problemi specifici e di automatizzare i processi di manutenzione di clienti, responsabili dell�approvazione, supervisori e addetti.  I supervisori possono pianificare le attivit� graficamente e i responsabili possono consolidare i dati per evidenziare e correggere le tendenze negative.' WHERE ACTIVITY_ID = 'AbBldgOpsOnDemandWork';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Cette application est id�ale pour les responsables de b�timent et de maintenance souhaitant identifier plus efficacement les travaux de maintenance r�currents et �viter les r�parations co�teuses et les indisponibilit�s prolong�es. Cette solution de planification compl�te leur permet de d�finir des proc�dures de maintenance, d��tablir des plannings pour les actifs critiques et d��quilibrer ces plannings avec les ressources disponibles.  Les responsables de maintenance peuvent en outre pr�voir pr�cis�ment les ressources et budgets � pr�voir pour les t�ches de maintenance planifi�es.' WHERE ACTIVITY_ID = 'AbBldgOpsPM';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Maintenir le bon d�roulement des activit�s op�rationnelles. G�rer les travaux, techniciens, outils et ressources planifi�s � partir de n�importe quel poste.' WHERE ACTIVITY_ID = 'AbBuildingOperations';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les soci�t�s immobili�res font g�n�ralement l�objet de nombreuses demandes internes portant sur la cr�ation de projets d�investissement aux motifs de planification g�n�rale, d�expansion, de modernisation, de consolidation, de r�novation ou de durabilit�.  Cette activit� collaborative permet aux responsables de classer ces projets par ordre de priorit� en fonction des objectifs essentiels de la soci�t� et de les doter des ressources les plus ad�quates pour assurer leur succ�s.  Elle comprend des fonctions permettant d��tudier des possibilit�s d�affectations budg�taires sur plusieurs ann�es, de comparer des sc�narios possibles et des hypoth�ses de financements issus de diverses sources, et permet de prendre des d�cisions en toute confiance dans des environnements financiers complexes.', SUMMARY_IT = 'Le organizzazioni immobiliari ricevono spesso numerose richieste di nuovi progetti capitali a favore della pianificazione principale, dell�espansione, della modernizzazione, del consolidamento, del rinnovamento e della sostenibilit�.  Questa attivit� collaborativa consente ai responsabile di dare priorit� ai progetti maggiormente allineati alla mission aziendale e di fornire le risorse per tali progetti.  Le funzionalit� di revisione delle allocazioni di budget pluriennali, di analisi degli scenari alternativi e di budget di fondi capitali e spese diversi consentono di prendere le giuste decisioni in scenari di capitale complessi.', SUMMARY_NL = 'Onroerendgoedorganisaties verdrinken vaak in de diverse aanvragen van belanghebbenden overal in de organisatie voor nieuwe kapitaalprojecten als ondersteuning van de master-planning, uitbreiding, modernisering, consolidatie, renovatie en duurzaamheid.  Deze gezamenlijke activiteit geeft het managementteam de kans om die projecten te prioriteren die het meest in lijn staan met de missie van de organisatie en deze voor succes aan te wenden.  Functies voor evaluatie van budgettoewijzingen over meerdere jaren, alternatieve "Wat als" scenario�s overwegen en vanuit verschillende investerings- en uitgavefondsen budgetteren, maakt het hun mogelijk om vol vertrouwen beslissingen te nemen tegen hun complexe kapitaalachtergrond.', TITLE_FR = 'Budgets d�Investissements' WHERE ACTIVITY_ID = 'AbCapitalBudgeting';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_DE = 'Wartungsmanager haben stets ein waches Auge auf Situationen, die das Leben der Mitarbeiter oder das Unternehmensziel gef�hrden k�nnten, weil sie wissen, dass selbst kleinere M�ngel, die nicht behoben werden, zu Ger�teausf�llen, Betriebsunterbrechungen oder Unf�llen f�hren k�nnen.  Den �berblick �ber alle potenziellen St�rungen innerhalb einer Anlage zu behalten, kann selbst die f�higste Wartungsfirma auf eine harte Probe stellen.  Die Aktivit�t �Zustandsbeurteilung� verringert das Risiko durch die direkte Erstellung eines umfassenden Beurteilungsprogramms aus den aktuellen Raum- und Ger�tebestandslisten.  Sie reduziert die Kosten �ber die gesamte Lebensdauer durch eine fr�hzeitige Beseitigung von M�ngeln.  �ber eine visuelle �bersicht, in der die Auswirkungen des Anlagenzustands auf Leib und Leben der Mitarbeiter, Unternehmensziele, Produktivit�t und Image zusammengefasst sind, gibt sie dem Management zudem genauestens Auskunft �ber die Gesamtkosten und die potenziellen Folgen.', SUMMARY_FR = 'Les responsables de maintenance sont tenus � la vigilance�: charg�s de d�celer tout danger susceptible de menacer la vie des personnes ou d�entraver la bonne marche de l�entreprise, ils sont bien plac�s pour savoir qu�une anomalie, m�me mineure, peut entra�ner une panne, une interruption d�activit� ou un accident si elle n�est pas corrig�e � temps.  Cependant, m�me le service de maintenance le plus efficace peut trouver �prouvant de suivre et r�soudre chaque anomalie sans exception dans l�ensemble d�une infrastructure.  L�activit� Evaluation de l�Etat du Patrimoine r�duit les risques en cr�ant un programme d��valuation complet directement depuis des inventaires d�espaces et d��quipements r�actualis�s.  Elle r�duit les co�ts du cycle de vie total en corrigeant les anomalies dommageables � un stade pr�coce.  De plus, elle pr�sente clairement, � l�attention des d�cisionnaires, les co�ts totaux et les cons�quences possibles au moyen d�un tableau d��valuation r�capitulant l�impact de l��tat des installations sur la s�curit� des personnes, sur la bonne marche de l�entreprise, sur la productivit� et sur l�image.',
 SUMMARY_IT = 'I responsabili della manutenzione vigilano sulla sicurezza e sui pericoli per la mission aziendale, poich� sono consapevoli che anche minimi difetti trascurati possono causare un guasto delle apparecchiature, l�interruzione delle attivit� o incidenti.  Tuttavia, mantenere il controllo di ogni potenziale difetto all�interno di una struttura pu� mettere a dura prova anche il pi� perfetto sistema di manutenzione.  L�attivit� Valutazione condizioni riduce i rischi creando un programma di valutazione completo direttamente basato sugli inventari degli spazi e delle apparecchiature pi� aggiornati.  Ci� consente di ridurre i costi totali del ciclo di vita correggendo in anticipo i difetti di corrosione.  � inoltre possibile comunicare chiaramente ai responsabili i costi totali e le potenziali conseguenze tramite un pannello che visualizza il riepilogo dell�impatto delle condizioni della struttura sulla sicurezza e sulla mission e l�immagine aziendale.', TITLE_FR = 'Evaluation de l��tat du patrimoine' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les ressources telles que les tables qui sont utilis�es dans diff�rentes activit�s sont assign�es � celle-ci au lieu de faire l�objet de licences individuelles.' WHERE ACTIVITY_ID = 'AbCommonResources';
UPDATE AFM.AFM_ACTIVITIES SET TITLE_FR = 'Inventaire d��quipement' WHERE ACTIVITY_ID = 'AbFEEqInventory';
UPDATE AFM.AFM_ACTIVITIES SET TITLE_FR = 'Gestion des D�m�nagements d�Entreprise' WHERE ACTIVITY_ID = 'AbMoveManagement';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les propri�taires de b�timents et les responsables de projets de construction doivent offrir l�acc�s � des informations de mise en service pertinentes qui pourront �tre utilis�es tout au long du cycle de vie des b�timents. Ils doivent �galement disposer d�un processus de r�vision des conceptions efficace afin d��liminer les risques de rappels co�teux apr�s la construction. En permettant un transfert transparent de la documentation relative aux constructions, comme les donn�es BIM (mod�lisation des donn�es du b�timent), dans votre syst�me de gestion de patrimoine au moment de la collecte des informations de mise en service et de fonctionnement, l�application Building Commissioning (Mise en service du B�timent) garantit que tous les syst�mes fonctionnent comme pr�vu tout au long du cycle de vie du b�timent. L�application permet l�automatisation du processus de mise en service et de fonctionnement et son d�ploiement rapide en fournissant des mod�les permettant de d�velopper des listes de contr�le, plans de projet, �tats d�avancement et archives documentaires propres � chaque site.', SUMMARY_IT = 'I proprietari di immobili e i responsabili di progetti di costruzione hanno bisogno di creare l�accesso a informazioni utili relative al commissioning affinch� possano essere utilizzate nel corso del ciclo di vita utile dell�edificio. Necessitano inoltre di un processo di revisione della progettazione efficiente per eliminare costosi richiami di post-costruzione. Mediante il trasferimento semplificato della documentazione relativa alla costruzione, quale il modello BIM (Building Information Model), al sistema gestionale della struttura all�atto di raccolta dei dati di commissioning, l�applicazione Commissioning sistemi dell�edificio garantisce il corretto funzionamento di tutti i sistemi nel corso del ciclo di vita dell�edificio. L�applicazione automatizza il processo di commissioning e abilita una rapida distribuzione fornendo modelli per lo sviluppo di liste di controllo, piani di progetto, stati di completamento e archivi di documentazione specifici per il sito.' WHERE ACTIVITY_ID = 'AbProjCommissioning';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les gestionnaires de portefeuille immobilier sont charg�s de superviser de nombreux projets simultan�ment.  R�novations, acquisitions de b�timents, modernisation de syst�me CVC, programmes environnementaux, d�m�nagements de service-... toutes ces op�rations sont fortement interd�pendantes et ne peuvent pas �tre g�r�es s�par�ment.  Cette activit� coordonne les programmes, projets, actions et co�ts selon une perspective descendante qui assure le respect, par tous les participants, des priorit�s g�n�rales de l�entreprise que tous ces projets desservent.  Des diagrammes de Gantt et des workflows permettent � tous de visualiser les �tapes, t�ches et changements d��tat.  Des �valuations de performances pond�r�es mettent en �vidence les anomalies pouvant menacer le budget ou la mission.', SUMMARY_NL = 'Managers van onroerendgoedportefeuilles houden tegelijkertijd constant toezicht op projecten in een omvangrijke portefeuille.  Renovaties, aankoop van gebouwen, HVAC-upgrades, duurzaamheidsprogramma�s, afdelingsverhuizingen-- deze zijn allemaal complex en onderling afhankelijk en kunnen niet apart worden afgehandeld.  Deze activiteit co�rdineert programma�s, projecten, handelingen en kosten van boven naar beneden op een manier die ervoor zorgt dat alle deelnemers in lijn staan met de prioriteiten van de overkoepelende organisatie waaraan alle projecten zich moeten houden.  Gantt-tabellen en workflows bouwen mijlpalen, taken en statusveranderingen die voor iedereen zichtbaar zijn.  Scorekaarten met gewogen prestaties markeren uitschieters die een risico vormen voor het budget of de missie.' WHERE ACTIVITY_ID = 'AbProjectManagement';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Analyser le taux d�efficacit� locative des b�timents de votre portefeuille immobilier en calculant les ratios entre les surfaces Utilisable et Locative.' WHERE ACTIVITY_ID = 'AbRPLMBuildingPerformance';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Pour les administrateurs de locations devant cr�er des factures d�taill�es selon des conditions d�imputation complexes, cette activit� r�duit les erreurs, puisqu�elle offre des fonctions automatiques de calcul des imputations et de suivi des factures et r�glements.'||CHR(13)||CHR(10)
||'Pour les administrateurs de co�ts devant rendre tous les co�ts transparents afin de faciliter la prise de d�cision, cette activit� met en corr�lation les co�ts et les lieux afin d�indiquer dans le d�tail les frais d�exploitation totaux aff�rents � chaque secteur d�activit� ou centre de co�ts.', SUMMARY_IT = 'Per gli amministratori locazioni che devono produrre fatture basate su condizioni di riaddebito complesse, questa attivit� consente di ridurre gli errori grazie ai calcoli di riaddebito automatici e al monitoraggio della fatturazione e del pagamento.'||CHR(13)||CHR(10)
||'Per gli amministratori dei costi che devono fornire una trasparenza dei costi a supporto del processo decisionale, l�attivit� consente di correlare i costi alle locazioni per ottenere i costi totali precisi dell�operazione per ogni linea di business o centro di costo.' WHERE ACTIVITY_ID = 'AbRPLMChargebackInvoice';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les administrateurs de co�ts cherchent � donner un avantage concurrentiel � leur entreprise en analysant le plus pr�cis�ment possible les co�ts de l�immobilier.  Cependant, s�ils ne disposent pas des outils ad�quats et que leur portefeuille pr�sente la moindre complexit�, cette t�che est un v�ritable d�fi.  Cette activit� permet de suivre les co�ts avec une grande pr�cision, d�une part par l�exploitation des donn�es de location et d�autre part par l�utilisation d�outils d�analyse de type Assistant.  Elle permet de localiser les anomalies et les gaspillages en r�alisant des �valuations de performances comparatives de chaque b�timent, propri�t�, location et compte.  De plus, elle accro�t la transparence et facilite la prise de d�cision gr�ce � des outils d�analyse qui d�c�lent les tendances, mettent en �vidence les aberrations et permettent de r�aliser des pr�visions des recettes, d�penses et flux de tr�sorerie.', SUMMARY_IT = 'Gli amministratori costi mirano a offrire all�azienda un vantaggio competitivo grazie a una migliore conoscenza dei costi immobiliari.  Nel caso di portafogli complessi, tale obiettivo risulta difficile da raggiungere senza adeguati strumenti.  Questa attivit� consente una pi� precisa analisi dei costi grazie ai dati sulle locazioni e all�utilizzo di strumenti di definizione dei costi basati su procedure guidate.  La ricerca di anomalie e sprechi risulta pi� semplice tramite l�utilizzo di benchmark comparativi per ciascun edificio, propriet�, locazione e commessa.  La trasparenza e il processo decisionale vengono agevolati da strumenti di analisi che individuano le tendenze, evidenziano le deviazioni e prevedono entrate, uscite e flusso di cassa.' WHERE ACTIVITY_ID = 'AbRPLMCosts';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Pour les administrateurs de portefeuille des agences f�d�rales am�ricaines tenus de se conformer � l�ordonnance ex�cutive (Executive Order) 13327, cette activit� enregistre les donn�es relatives aux biens immobiliers dans le r�f�rentiel f�d�ral centralis� en appliquant le format exig�.  L�activit� utilise un workflow pour suivre et approuver les transactions et peut produire des rapports par demandeur ou par identifiant unique d�actif de propri�t�.', SUMMARY_IT = 'Per gli amministratori di portafoglio delle agenzie federali che devono rispettare l�EO (Executive Order) 13327, questa attivit� invia i dati sulla propriet� immobiliare all�archivio federale centrale nel formato previsto.  L�attivit� utilizza un flusso di lavoro per la registrazione e l�approvazione delle transazioni e consente di recuperare i dati in base al richiedente o in base all�Identificatore univoco propriet� immobiliare .' WHERE ACTIVITY_ID = 'AbRPLMGovPropertyRegistry';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les gestionnaires d�espace doivent pr�voir pr�cis�ment les besoins en espace et les frais d�occupation futurs de leur entreprise. Si leurs pr�visions sont trop �lev�es, ils entra�nent celle-ci dans des d�penses superflues en b�timents et en locations.  Si elles sont trop faibles, les activit�s de l�entreprise ne peuvent plus fonctionner normalement.  Cette activit� utilise les donn�es d�inventaire, les tendances historiques et des simulations pour donner un aper�u r�aliste des futurs besoins en espace.  Les gestionnaires d�espace peuvent alors mod�liser les rotations, la croissance interne, les fusions ou acquisitions possibles et d�autres facteurs pour r�duire les incertitudes et d�celer les opportunit�s �mergentes dans leur portefeuille.', SUMMARY_IT = 'I pianificatori degli spazi devono prevedere in modo preciso le future esigenze di spazio e i costi di occupazione. Previsioni di spazi eccessivi comporteranno per l�azienda edifici e locazioni inutili.  Previsioni di spazi insufficienti bloccheranno le attivit� aziendali.  Questa attivit� utilizza i dati dell�inventario, le tendenze cronologiche e gli scenari alternativi per offrire una visione delle esigenze di spazio future.  I pianificatori degli spazi possono considerare gli spostamenti, la crescita organica, le fusioni o le acquisizioni potenziali e altri fattori per ridurre l�incertezza e individuare le opportunit� emergenti all�interno del loro portafoglio.', SUMMARY_NL = 'Space Master-planners moeten nauwkeurig de toekomstige behoefte aan ruimte en de bezettingskosten voorspellen. Als de prognose te hoog is, belasten zij de onderneming met onnodige gebouwen en leases.  Bij een te lage prognose verlammen zij de onderneming.  Deze activiteit gebruikt inventarisgegevens, historische trends evenals "Wat als" scenario�s die inzichten verschaffen in de toekomstige behoefte aan ruimte.  Master planners kunnen omloop, autonome groei, potenti�le fusies of aankopen modelleren evenals andere factoren voor het verminderen van onzekerheden en het opmerken van opkomende inkomensmogelijkheden binnen hun portefeuille.' WHERE ACTIVITY_ID = 'AbRPLMGroupSpaceAllocation';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Consolider les donn�es des contrats de locations pour produire des tableaux de bord de locations qui fournissent un acc�s rapide et facile � l�essentiel des donn�es pr�cises et � jour concernant chaque location.' WHERE ACTIVITY_ID = 'AbRPLMLeaseAbstracts';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les responsables de locations savent que toute erreur dans le traitement des conditions complexes d�une location peut entra�ner des manques � gagner, des erreurs de facturation, des sanctions financi�res, voire des d�m�nagements de derni�re minute en raison de dates de reconduction manqu�es.  Cependant, il peut �tre d�courageant de suivre un grand nombre de locations pr�sentant des modalit�s diff�rentes, des dates d�exercice d�option vari�es et des conditions financi�res compliqu�es.  Cette activit� automatise l�administration des locations pour r�duire les erreurs et les risques, signaler toute anomalie et pr�venir l�utilisateur en cas d�action � mener imminente.', SUMMARY_IT = 'I responsabili locazioni sanno che gli errori di elaborazione dei complessi termini di una locazione possono causare inadempienze, errori di fatturazione, sanzioni e persino traslochi immediati a causa di un mancato rinnovo.  La gestione di numerosi contratti di locazione con clausole d�affitto differenti, date di esercizio opzioni diverse e complessi termini finanziari pu� risultare impegnativa.  Questa attivit� limita il rischio di errore tramite un processo di amministrazione automatizzato che riduce gli errori, individua le anomalie e fornisce notifiche automatiche delle azioni successive.' WHERE ACTIVITY_ID = 'AbRPLMLeaseAdministration';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les gestionnaires de portefeuille s�efforcent de stimuler la rentabilit� des investissements immobiliers de grande ampleur r�alis�s par leur soci�t�.  Leur d�termination est n�anmoins mise � rude �preuve, en raison d�une part de la diversit� des besoins des diff�rentes activit�s de l�entreprise, et d�autre part de la complexit� des �ch�anciers des locations et des transactions.  Cette activit�, qui offre une vue consolid�e des donn�es immobili�res, permet de visualiser l�ensemble du portefeuille et d�en rationaliser sa gestion.  Elle fournit diff�rents moyens pour comparer les performances des portefeuilles afin de favoriser une prise de d�cision rapide bas�e sur des faits.  De plus, elle procure des dispositifs essentiels pour d�celer les tendances, analyser les d�penses et cibler les anomalies.', SUMMARY_IT = 'I gestori dei portafogli devono garantire che gli investimenti massivi effettuati dall�azienda nel portafoglio delle propriet� immobiliari sostengono la mission aziendale.  Nonostante l�ampia variet� di partecipazioni, le esigenze specifiche delle diverse linee di business e la complicata tempistica delle locazioni e delle transazioni compromettono spesso tale sostegno.  Questa attivit� fornisce una visione consolidata dei dati immobiliari per la visualizzazione del portafoglio e il miglioramento della gestione dei beni.  Fornisce pi� benchmark della redditivit� del portafoglio per migliorare e rendere pi� rapidi i processi decisionali.  Offre le analisi necessarie per individuare le tendenze, analizzare le spese e rilevare le anomalie.' WHERE ACTIVITY_ID = 'AbRPLMPortfolioAdministration';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les membres d��quipes immobili�res, les gestionnaires de portefeuilles et les responsables de l�espace ont besoin d�une aide � la d�cision avanc�e pour �laborer, communiquer et appuyer leur sch�ma strat�gique de portefeuille. Cette application pr�sente clairement les besoins en terme de mission cl�, r�duit la complexit� de la planification, simplifie le processus de r�vision et fournit les chiffres qui pourront justifier les mesures pr�conis�es.', SUMMARY_IT = 'Team immobiliari, responsabili del portafoglio e responsabili della programmazione dello spazio richiedono un supporto decisionale per creare, comunicare e sostenere il proprio piano del portafoglio strategico. Questa applicazione esprime chiaramente le esigenze prioritarie essenziali, semplifica il complesso processo di programmazione e analisi e offre i dati concreti necessari per giustificare una linea d�azione.' WHERE ACTIVITY_ID = 'AbRPLMPortfolioForecasting';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Consolider les donn�es des propri�t�s pour produire des tableaux de bord de propri�t�s qui fournissent un acc�s rapide et facile � l�essentiel des donn�es pr�cises et � jour concernant chaque propri�t�.' WHERE ACTIVITY_ID = 'AbRPLMPropertyAbstracts';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Faire le suivi et g�rer toutes les informations relatives aux imp�ts, les dates d��ch�ance et les co�ts pour vos propri�t�s et parcelles. G�n�rer des rapports de tr�sorerie d�imp�ts et de r��valuation.' WHERE ACTIVITY_ID = 'AbRPLMPropertyTaxes';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_DE = 'Die Anwendung �Strategische Finanzanalyse� stellt ein einheitliches Kostenmodell f�r Ihre Immobilien, Infrastruktur und Anlagen bereit.  Dadurch vereinheitlicht es Kostendaten f�r alle Immobilienaktivit�ten, Kapital, und Ausgaben.  Sie bietet au�erdem eine einheitliche Ansicht der Kosten f�r die verschiedenen BeteiIigten im Portfolio- und Infrastrukturmanagement.  Dadurch bietet die Anwendung einen Einblick in das Drittel der Bilanz Ihrer Organisation, das Sachanlagen verwaltet.  ARCHIBUS ist insofern einzigartig, als dass es nicht nur die Gesamtkosten der Infrastruktur summiert, sondern auch den Wert f�r das Ziel ausdr�cken kann.  Aus diesem Grund unterst�tzt ARCHIBUS Sie bei der Koordination Ihrer Immobilien- und Infrastrukturausgaben wie kein anderes Tool.', SUMMARY_FR = 'L�application Analyse financi�re strat�gique offre une mod�lisation de co�ts unifi�e de l�immobilier, de l�infrastructure et du patrimoine qui vous appartient.  Ainsi, les donn�es de co�t de toutes les activit�s immobili�res, des immobilisations et des d�penses sont unifi�es.  Elle fournit �galement une vue unifi�e des co�ts attribu�s aux diff�rentes parties prenantes dans la gestion du portefeuille et de l�infrastructure.  De ce fait, l�application rend visible le tiers du bilan de votre organisation qui g�re les actifs fixes.  ARCHIBUS est unique dans la mesure o� l�application peut non seulement r�sumer le co�t total de l�infrastructure, mais �galement d�gager la valeur de la mission de l�entreprise.  C�est pourquoi ARCHIBUS vous permet d�aligner vos d�penses immobili�res et d�infrastructure comme nul autre outil.', SUMMARY_IT = 'La richiesta di analisi finanziaria strategica fornisce un modello di costo unificato per immobili, infrastrutture e strutture.  In questo modo, unifica i dati dei costi di tutte le attivit� immobiliari, capitale e spese.  Inoltre fornisce una visualizzazione unificata dei costi per le diverse parti in causa per la gestione del portafoglio e dell�infrastruttura.  In questo modo, l�applicazione fornisce visibilit� nel terzo del bilancio dell�organizzazione che gestisce gli asset fissi.  ARCHIBUS � unico, poich� non solo riepiloga il costo totale dell�infrastruttura, ma esprime anche il valore per la missione.  Per questo motivo, ARCHIBUS � in grado di aiutare ad allineare la spesa per l�immobile e le infrastrutture come nessun altro strumento.' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'G�rer le processus de localisation, d�identification et d��limination des substances dangereuses, telles que l�amiante, la peinture � base de plomb, les moisissures et le radon, au sein du portefeuille immobilier de l�organisation.  Cette activit� permet d�acc�der rapidement et ais�ment � des informations compl�tes sur la localisation et l��tat de l�amiante, la peinture � base de plomb et autres mati�res dangereuses. Cette activit� simplifie la gestion des obligations r�glementaires en termes d��valuation, d��limination, de surveillance et d��tablissement de rapports sur les substances et mati�res dangereuses.', SUMMARY_IT = '� possibile gestire il processo di individuazione, identificazione e abbattimento delle sostanze pericolose, quali amianto, vernici a base di piombo, muffe e radon, in tutto il portafoglio di propriet� immobiliari di un�organizzazione. L�attivit� consente di accedere rapidamente e facilmente a informazioni complete relative alla posizione e alla condizione di amianto, vernici a base di piombo e altri materiali pericolosi. Questa attivit� consente di gestire gli obblighi normativi per la valutazione, l�abbattimento, il monitoraggio e la segnalazione di materiali e sostanze pericolose.' WHERE ACTIVITY_ID = 'AbRiskCleanBuilding';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Garantir la conformit� aux r�glementations en vigueur en mati�re d�environnement' WHERE ACTIVITY_ID = 'AbRiskCompliance';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les responsables de la s�curit� sont charg�s de s�assurer que le personnel est correctement form� � l�utilisation de tous les mat�riaux et dispose d��quipements de protection individuelle (EPI) ad�quats. Ils doivent en outre mettre en oeuvre un syst�me de suivi m�dical conforme � la r�glementation OSHA ou autres r�glementations gouvernementales. L�application Environnement, hygi�ne et s�curit� d�ARCHIBUS offre un cadre structur� et souple qui leur permet d�effectuer un suivi continu des proc�dures de s�curit� et des probl�mes li�s � celle-ci. En cas d�accident li� � la s�curit�, l�application permet aux responsables de rechercher et de suivre les causes premi�res, les mesures correctives, les informations relatives aux blessures, l�emplacement de l�usine et les restrictions de travail futures impos�es � l�employ�. En associant ces donn�es sur les accidents avec le suivi m�dical, les EPI et les fonctions de formation, ils disposent d�un outil puissant pour mettre en place et faire appliquer des programmes et proc�dures de s�curit�.', SUMMARY_IT = 'Per i responsabili della sicurezza che devono garantire un�adeguata formazione affinch� il personale possa lavorare con tutti i materiali e disporre di Dispositivi di Protezione Individuale (DPI) idonei, che devono inoltre implementare un sistema di accertamenti medici conforme alle disposizioni OSHA o ad altre normative governative, l�applicazione Ambiente, Salute e Sicurezza (EHS) di ARCHIBUS offre una struttura organizzata e flessibile per il monitoraggio costante delle procedure e dei problemi relativi alla sicurezza. In caso di incidente correlato alla sicurezza, i responsabili possono utilizzare l�applicazione per individuarne la causa principale, le azioni correttive, le informazioni sull�infortunio, la posizione dell�impianto e le future limitazioni lavorative applicate al dipendente. La combinazione dei dati relativi all�incidente con quelli relativi agli accertamenti medici, ai DPI e alle funzioni di formazione, fornisce ai responsabili un potente strumento per garantire e applicare programmi e procedure di sicurezza.', 
SUMMARY_NL = 'Voor veiligheidsmanagers die ervoor moeten zorgen dat personeel de juiste opleiding krijgt voor het werken met alle materialen en beschikt over de juiste persoonlijke beschermingsmiddelen (PPE) en die een systeem voor medische controle moeten implementeren dat voldoet aan OSHA en andere overheidsvoorschriften, biedt de ARCHIBUS-toepassing Arbeidsomstandigheden een gestructureerd en flexibel kader voor het doorlopend controleren van veiligheidsprocedures en -kwesties.Als er een veiligheidsincident optreedt, kunnen managers met behulp van de toepassing aspecten volgen zoals hoofdoorzaak, corrigerende maatregelen, letselgegevens, locatie van de vestiging en voor de medewerker geldende toekomstige arbeidsbeperkingen.De combinatie van deze incidentgegevens met medische controle, persoonlijke beschermingsmiddelen en opleidingsfuncties biedt managers een krachtig hulpmiddel bij het inzetten van veiligheidsprogramma�s en -procedures.' WHERE ACTIVITY_ID = 'AbRiskEHS';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les responsables immobiliers et les gestionnaires de patrimoine sont d�termin�s � satisfaire les objectifs que leur fixe leur hi�rarchie en mati�re de durabilit� et de r�duction de l�empreinte carbone.  Cependant, compte tenu de la multiplicit� des b�timents qu�ils doivent g�rer et des conditions � prendre en compte, le passage de la th�orie � la pratique g�n�re des probl�mes complexes.  Cette activit� r�duit les risques en �valuant tous les aspects d�une infrastructure par rapport aux directives des dirigeants et aux r�glementations en vigueur. Elle r�duit les co�ts en identifiant les possibilit�s d��conomie d��nergie et de r�duction des gaspillages.  De plus, elle simplifie la prise de d�cision en affectant des notations aux solutions correctives possibles et en identifiant les utilisations les plus pertinentes par leur priorit� et leur impact, ce qui est particuli�rement appr�ciable lorsque les budgets correctifs sont limit�s.', SUMMARY_IT = 'I responsabili degli immobili e delle strutture sono decisi a rispettare le indicazioni dei dirigenti per imporre standard elevati di sostenibilit� e riduzione del carbonio.  Scoprono tuttavia che applicare tali standard ai diversi edifici e alle condizioni attuali risulta essere un problema complesso.  Questa attivit� riduce il rischio valutando l�intera struttura in ogni dettaglio relativamente alle direttive e alle normative ambientali. Consente di ridurre i costi identificando le opportunit� di risparmio energetico e riduzione dei rifiuti.  Migliora la gestione creando schede di valutazione delle alternative di risanamento e identificando la priorit� pi� elevata, l�uso di maggior impatto per i budget di risanamento limitati.' WHERE ACTIVITY_ID = 'AbRiskES';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Le personnel d�infrastructure et d�exploitation se pr�pare soigneusement aux urgences pouvant survenir sur le lieu de travail, telles que temp�tes de neige, inondations, d�versement de produits chimiques, fuites de gaz, etc.  Cependant, pour mettre en oeuvre ces plans, il lui faut coordonner les premiers secours, les employ�s, l��quipe d��valuation et la direction -- et ce, m�me dans des conditions de crise emp�chant d�acc�der au r�seau t�l�phonique, � l�e-mail ou � l�intranet du site ou de la soci�t�.  L�activit� Gestion des situations d�urgence fournit cette coordination, et procure des informations rapides et des proc�dures qui peuvent contribuer � sauver des vies, � minimiser les dommages, � traiter plus rapidement les demandes d�indemnisation aupr�s des compagnies d�assurance et � restaurer rapidement les fonctions vitales de l�entreprise.', SUMMARY_IT = 'Il personale addetto alle strutture e alle operazioni si prepara ad affrontare le possibili emergenze del luogo di lavoro, quali temporali e allagamenti, fuoruscite di prodotti chimici, perdite di gas e altre situazioni critiche.  Tuttavia, per implementare tali progetti il personale necessita di strumenti per il coordinamento degli addetti al pronto intervento, del personale, del team di valutazione e del management -- anche in situazioni di emergenza nelle quali non � possibile accedere alle strutture o al telefono aziendale, all�e-mail o alla Intranet.  La Preparazione in caso di emergenza offre tale coordinamento e tutte le informazioni e le procedure che possono salvare vite, ridurre al minimo gli eventuali danni, velocizzare le richieste di risarcimento e ripristinare le funzioni aziendali strategiche.', TITLE_FR = 'Gestion des situations d�urgence' WHERE ACTIVITY_ID = 'AbRiskEmergencyPreparedness';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Il est courant que les gestionnaires d��nergie re�oivent des ordres stricts leur imposant d�atteindre des objectifs de r�duction de la production de carbone et de l�utilisation d��nergie de l�ordre de 3 � 5�% par an.  Leur t�che est d�autant plus difficile que, bien souvent, ils ne disposent pas d�une vision globale de la consommation d��lectricit�, ne connaissent pas la r�partition de cette consommation par secteur d�activit� et ne disposent d�aucun moyen pour mesurer objectivement les effets des actions correctives.  Cette activit� �tablit des corr�lations entre les donn�es des factures d��lectricit� et les informations du b�timent, ainsi qu�avec une mod�lisation m�t�o, afin de d�celer les erreurs de facturation, d�identifier les b�timents sources de probl�mes et d��liminer les gaspillages.  Les gestionnaires d��nergie r�ussissent ainsi � atteindre des objectifs exigeants en termes de r�duction de la consommation d��nergie et � aligner les d�penses �nerg�tiques avec les priorit�s de leur soci�t�.', SUMMARY_IT = 'I dirigenti richiedono spesso ai responsabili dell�energia di attenersi a precise procedure di riduzione delle emissioni di carbonio e di utilizzo dell�energia con un target preciso, in genere il 3-5% all�anno.  I responsabili dell�energia tuttavia non sempre dispongono di un quadro preciso dei consumi, della linea di business di utilizzo dell�energia o della modalit� con la quale misurare in modo obiettivo gli effetti delle azioni correttive.  Questa attivit� collega le fatture dei costi energetici alle informazioni sull�edificio evidenziando gli errori di fatturazione, identificando gli edifici problematici e individuando gli sprechi.  I responsabili dell�energia possono quindi impegnarsi su obiettivi ambiziosi di riduzione dei consumi energetici e allineare i consumi di energia alle priorit� aziendali.', TITLE_FR = 'Gestion de la consommation d��nergie' WHERE ACTIVITY_ID = 'AbRiskEnergyManagement';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_DE = 'Nachhaltigkeitsbeauftrage und f�r ein �gr�nes Geb�ude� zust�ndige Manager sind bestrebt, die Nutzung und Nachhaltigkeit von Geb�uden zu optimieren und die �ko- & CO2-Bilanz zu verbessern. Die Bewertung des Fortschritts und die Quantifizierung von Ergebnissen erfordern allerdings komplexe Berechnungen und eine Vielzahl an Daten, die �ber mehrere Jahre erfasst wurden. Diese Aktivit�t berechnet die Nettoauswirkungen der Treibhausgas�quivalente, zeigt die Ver�nderung der CO2-Bilanz im Laufe der Zeit f�r jedes Geb�ude oder ein ganzes Portfolio, verfolgt den Fortschritt durch Projekte zur Nachhaltigkeitszertifizierung und verwaltet Daten und Dokumente, die zur Requalifizierung erforderlich sind. Zudem zeigt sie die R�ckzahlungen von Investitionen in die Nachhaltigkeit und korreliert Umweltdaten mit der Anlagennutzung und dem Betrieb und stellt so fr�hzeitig zuverl�ssige Daten bereit, die zur Findung und Verteidigung strategischer Entscheidungen hinsichtlich der Nachhaltigkeit erforderlich sind.', SUMMARY_FR = 'Les personnes en charge des questions de durabilit� et les gestionnaires de b�timents ��verts�� s�efforcent d�accro�tre les performances et les niveaux de certification des b�timents �cologiques, et de r�duire l�empreinte carbone. Cependant, pour �valuer les progr�s r�alis�s et quantifier les r�sultats, il est n�cessaire d�effectuer des calculs complexes et de disposer de donn�es couvrant plusieurs ann�es. Cette activit� calcule l�impact net des �quivalents des gaz � effet de serre, pr�sente l��volution de l�empreinte carbone dans le temps d�un certain b�timent ou d�un portefeuille complet, suit les progr�s r�alis�s au moyen de projets de certification de durabilit�, et g�re les donn�es et documents requis pour une requalification. De plus, elle pr�sente le retour pouvant �tre escompt�s des investissements �cologiques et �tablit des corr�lations entre les donn�es environnementales et l�utilisation des infrastructure et les op�rations. Elle procure ainsi des donn�es fiables et opportunes permettant de prendre et de d�fendre des d�cisions strat�giques en mati�re de durabilit�.', 
SUMMARY_IT = 'I funzionari che si occupano di sostenibilit� e i responsabili degli edifici sostenibili si impegnano per incrementare le prestazioni di questo genere di edifici e i livelli di certificazione, oltre che per ridurre l�impronta di carbonio. Tuttavia, le operazioni di valutazione dei progressi e quantificazione dei risultati richiedono calcoli complessi e tutta una serie di dati relativi a svariati anni. Questa attivit� consente di: calcolare l�impatto dell�equivalente netto di gas serra; mostrare i cambiamenti nel corso del tempo dell�impronta di carbonio per ogni edificio o per un intero portafoglio; tenere traccia dei progressi fatti attraverso i progetti di certificazione per la sostenibilit�; gestire i dati e i documenti richiesti per la riqualificazione. Inoltre l�attivit� mostra il ritorno previsto sugli investimenti nella sostenibilit� e mette in correlazione i dati ambientali con l�utilizzo e le operazioni in merito alle strutture, fornendo in tal modo i dati affidabili e tempestivi necessari per prendere e sostenere decisioni strategiche in termini di sostenibilit�.' WHERE ACTIVITY_ID = 'AbRiskGreenBuilding';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Aux responsables de la s�curit� qui doivent tenir � jour un catalogue des derni�res FDS du fabricant et un historique des FDS et partager ces donn�es avec les employ�s de la soci�t�, l�application Fiche de donn�es de s�curit� (FDS) offre des outils permettant de '||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'stocker les listes de produits chimiques utilis�es dans les espaces de travail, de cataloguer leurs FDS et d�associer des FDS � des emplacements sp�cifiques. Une fois saisies, ces donn�es fournissent des informations utiles sur l�emplacement et '||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'la gravit� des dangers potentiels, tout en respectant la r�glementation de conformit�.', SUMMARY_IT = 'Per i responsabili della sicurezza che devono mantenere un catalogo delle Schede tecniche materiale, pi� recenti nonch� meno recenti dei produttori, e che devono condividere tali dati con i dipendenti dell�azienda, l�applicazione SDS di ARCHIBUS fornisce gli strumenti per '||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'l�archiviazione di elenchi dei prodotti chimici utilizzati sul luogo di lavoro, la catalogazione delle relative schede e l�associazione delle schede a siti specifici. Una volta immessi, tali dati possono fornire utili dettagli sul sito e sulla '||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'gravit� di potenziali rischi nonch� per soddisfare i requisiti dei programmi di conformit�.', SUMMARY_NL = 'Voor veiligheidsmanagers die een catalogus van de laatste en historische SDS�s  van de fabrikant moeten bijhouden en deze gegevens moeten delen met de medewerkers van het bedrijf, biedt de ARCHIBUS-toepassing SDS gereedschappen voor '||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'het vastleggen van lijsten met chemische producten die op werkplekken worden gebruikt, voor het catalogiseren van hun SDS�s en voor het koppelen van SDS�s aan specifieke locaties. Eenmaal ingevoerd kunnen deze gegevens waardevolle inzichten bieden in de locatie en '||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'ernst van potenti�le gevaren, maar er ook voor zorgen dat wordt voldaan aan complianceregelingen.' WHERE ACTIVITY_ID = 'AbRiskMSDS';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Affecter des espaces aux unit�s administrative et les disposer sur des plans.', TITLE_FR = 'Affectation & disposition de l�espace' WHERE ACTIVITY_ID = 'AbSMPAllocationLayout';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'G�rer les donn�es spatiales, organisationnelles et personnel utilis�es pour la planification de l�espace' WHERE ACTIVITY_ID = 'AbSMPBackgroundData';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Examiner les donn�es de l�historique et les tendances sur l�utilisation de l�espace organisationnel.', SUMMARY_IT = 'Verificare i dati cronologici e le tendenze relativi all�utilizzo dello spazio organizzativo.', TITLE_FR = 'Historique d�utilisation de l�espace' WHERE ACTIVITY_ID = 'AbSMPHistory';
UPDATE AFM.AFM_ACTIVITIES SET TITLE_FR = 'Programmation & pr�vision de l�espace' WHERE ACTIVITY_ID = 'AbSMPProgramForecast';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Garantir que les responsables disposent de l�espace ad�quat et du type d�espace appropri� pour ex�cuter leur mission. Identifier et r�soudre rapidement toute dispute concernant l�attribution et l�utilisation de l�espace.', TITLE_FR = 'Affectation de l�Espace' WHERE ACTIVITY_ID = 'AbSpaceAllocation';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Analyser le taux d�efficacit� locative des b�timents de votre portefeuille immobilier en calculant les ratios entre les surfaces Utilisable et Locative.' WHERE ACTIVITY_ID = 'AbSpaceBldgPerformance';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Maintenir un inventaire d�espace pr�cis et � jour au niveau des groupes d�partementaux. Fournir un acc�s facile et rapide � des informations pr�cises concernant l�occupation de l�espace.' WHERE ACTIVITY_ID = 'AbSpaceGroupInventory';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Cette application Web est particuli�rement pratique pour les responsables d�op�rations et les gestionnaires d�espace souhaitant r�duire les co�ts aff�rents � l�espace en appliquant des strat�gies novatrices sur le lieu de travail. Elle fournit aux employ�s et aux visiteurs de l�entreprise un syst�me en libre-service de localisation et de r�servation d�espace pour les activit�s � court terme. En transformant des pi�ces en bureaux de passage et en surveillant les rapports d�utilisation d�espace, les gestionnaires d�espace peuvent identifier et mettre � disposition des espaces sous-utilis�s afin de r�duire les co�ts d�occupation. Cette application se d�ploie rapidement et exploite un inventaire des espaces existants.' WHERE ACTIVITY_ID = 'AbSpaceHotelling';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'D�ployer cette activit� pour donner aux responsables un acc�s � la demande � leurs donn�es d�occupation et aux services de support et d�m�nagement dont ils ont besoin pour former rapidement leurs �quipes de projets.', SUMMARY_IT = 'Rendere disponibile questa attivit� per consentire ai responsabili di accedere alle informazioni sull�occupazione e al supporto e spostare i servizi necessari per comporre velocemente i team progetto.', TITLE_FR = 'Occupation de l�espace' WHERE ACTIVITY_ID = 'AbSpaceOccupancy';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Pour les gestionnaires d�espace et les responsables de d�partement cherchant � rentabiliser les espaces et � accro�tre la productivit� du personnel, cette application Web int�gr�e pr�sente divers avantages. Elle r�duit les taux d�inoccupation en fournissant un acc�s en libre-service aux inventaires d''espaces et aux listes d�employ�s, et permet aux responsables d�optimiser l�affectation du personnel suivant les affinit�s et les disponibilit�s.  Elle fournit des outils pour planifier les besoins futurs, ainsi que des indicateurs de performance favorisant l�homog�n�it� et l��quit� en mati�re d�affectation du personnel.', SUMMARY_IT = 'Per i responsabili dello spazio e di reparto che desiderano utilizzare lo spazio in modo pi� efficiente e aumentare la produttivit� del personale, questa applicazione basata sul Web riduce le percentuali di posti vacanti fornendo l�accesso self-service all�inventario degli spazi e agli elenchi del personale, consentendo ai responsabili di ottimizzare l�impiego dei dipendenti in base alle affinit� e identificare la disponibilit�.  Questa soluzione fornisce strumenti che permettono di pianificare le esigenze e i benchmark futuri per promuovere gli standard e la correttezza nell�impiego dei dipendenti.' WHERE ACTIVITY_ID = 'AbSpacePersonnelInventory';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les responsables de l�espace sont charg�s de garantir que l�espace disponible de l�organisation couvre la demande actuelle ainsi que future. Ils peuvent coop�rer avec les services immobiliers pour planifier les besoins futurs en espace, qu�il s�agisse de consolider l�espace, de r�silier des locations, de rechercher de nouvelles locations, d�acheter des propri�t�s ou d�apporter son concours � la construction de nouveaux espaces. La planification de l�espace dans le but d�appuyer les objectifs de l�organisation va de la prise de d�cisions importantes concernant l�immobilier et les locations � des plans plus sp�cifiques concernant l�utilisation et l�occupation de l�espace. Dans le cadre de ce processus, de nouveaux b�timents et �tages peuvent �tre mis en service et l�affectation et l�occupation des b�timents et �tages existants peuvent �voluer au fil du temps. Cette application met � disposition des responsables de l�espace les outils permettant de g�rer l�ensemble de ces transformations et de pr�senter ces sc�narios � la direction.', SUMMARY_IT = 'I responsabili della pianificazione dello spazio si accertano che la dotazione di spazio dell�organizzazione ne soddisfi le domande, sia nel presente che nel futuro. Possono lavorare con il settore immobiliare dell�organizzazione per pianificare future esigenze di spazio, ad esempio consolidando gli spazi, eliminando locazioni, trovando nuove locazioni, acquistando propriet� o supportando nuove costruzioni. La pianificazione dello spazio a supporto degli obiettivi dell�organizzazione si estende dalle decisioni immobiliari/di locazioni di alto livello a pianificazioni pi� specifiche per l�uso e l�occupazione degli spazi. Nell�ambito di questo processo, possono essere inseriti online nuovi edifici e piani e quelli esistenti nel tempo possono cambiare allocazione e occupazione. Questa applicazione offre ai responsabili della pianificazione gli strumenti per gestire queste modifiche complete e presentare vari scenari alla direzione.', 
SUMMARY_NL = 'Ruimteplanners dienen ervoor te zorgen dat de ruimtevoorraad van de organisatie aan de vraag voldoet, zowel nu als in de toekomst. Ze kunnen werken met de onroerendgoedorganisatie voor het plannen van toekomstige behoefte aan ruimte, of dat nu plaatsvindt via het consolideren van ruimte, het opzeggen van leases, het vinden van nieuwe leases, de aanschaf van vastgoed of het ondersteunen van nieuwbouw. Ruimteplanning ter ondersteuning van de organisatiedoelen vari�ren van beslissingen op hoog niveau inzake onroerendgoed/lease tot specifieke plannen voor gebruik en bezetting van ruimtes. Als onderdeel van dit proces kunnen nieuwe gebouwen en verdiepingen online komen en kunnen bestaande gebouwen en verdiepingen in de loop der tijd andere toewijzingen en bezettingen krijgen. Deze toepassing biedt ruimteplanners de nodige hulpmiddelen om deze grootschalige wijzigingen te beheren en scenario�s aan het management te presenteren.', TITLE_FR = 'Planification strat�gique de l�espace' WHERE ACTIVITY_ID = 'AbSpacePlanning';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Les gestionnaires d�espace cherchant � accro�tre la rentabilit� des espaces et � sensibiliser les responsables de d�partement � l�importance de l�utilisation des espaces trouvent dans cette application Web int�gr�e un outil pr�cieux qui leur permet de facturer les responsables pour les espaces qu�ils occupent.  Les fonctions d�imputation/facturation permettent ainsi de responsabiliser chacun.  Les fonctions d�analyse permettent aux responsables de d�partement de prendre des d�cisions plus avis�es quant aux utilisations d�espace.  Celles-ci se traduisant directement en co�ts, ils sont davantage susceptibles d�exploiter plus rationnellement les surfaces et de r�duire les frais d�occupation.', SUMMARY_IT = 'Per i responsabili dello spazio che desiderano ottimizzare l�efficienza e rendere i dirigenti di reparto pi� responsabili rispetto all�uso dello spazio, questa applicazione basata sul Web integrata addebita l�utilizzo dello spazio ai responsabili di reparto.  Le funzionalit� di riaddebito rendono i dirigenti pi� responsabili rispetto all�utilizzo dello spazio  e l�analisi consente loro di prendere decisioni pi� informate sulle iniziative correlate allo spazio.  Quando i responsabili di reparto si rendono conto di come l�uso dello spazio si traduce in costi, si impegnano di pi� per migliorare l�efficienza e ridurre i costi di occupazione.', TITLE_FR = 'Imputation de l�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomChargebackAR';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Particuli�rement pratique pour les gestionnaires d�espace et les responsables de d�partement souhaitant ma�triser davantage l�utilisation de l�espace dont ils ont la charge et la mani�re dont cet espace impacte leur activit�, cette application Web int�gr�e fournit un acc�s en libre-service aux rapports d�utilisation d�espace et aux statistiques d�inventaire des espaces.  En suivant pr�cis�ment cet inventaire des espaces, l�application rationalise l�utilisation de l�espace, accro�t la responsabilisation de chacun et facilite la planification d�espace.', SUMMARY_IT = 'Per i responsabili dello spazio e di reparto che desiderano migliorare l�utilizzo degli spazi e la gestione delle operazioni, questa applicazioni basata sul Web integrata fornisce l�accesso self-service ai report sull�uso dello spazio e alle statistiche sull�inventario degli spazi.  Attraverso la registrazione accurata dell�inventario degli spazi, questa applicazione semplifica l�utilizzo dello spazio, migliora la contabilit� e favorisce la pianificazione degli spazi.', TITLE_FR = 'Inventaire et performance de l�espace' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBAR';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Maintenir un inventaire de pi�ces pr�cis et � jour. Fournir un acc�s facile et rapide � des informations pr�cises concernant l�occupation de l�espace.' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryBC';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Maintenir un inventaire de pi�ces pr�cis et � jour. Fournir un acc�s facile et rapide � des informations pr�cises concernant l�occupation de l�espace.', TITLE_FR = 'G�rer l�inventaire des pi�ces am�lior�es' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEAR';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Maintenir un inventaire de pi�ces pr�cis et � jour. Fournir un acc�s facile et rapide � des informations pr�cises concernant l�occupation de l�espace.' WHERE ACTIVITY_ID = 'AbSpaceRoomInventoryEC';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Am�liorer la planification et la gestion financi�re de la soci�t� en r�partissant de mani�re pr�cise le co�t de TOUS les espaces entre les d�partements, y compris l�espace associ� aux surfaces communes, aux pi�ces partag�es entre plusieurs occupants et m�me les espaces communs � l�int�rieur de pi�ces.' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackAR';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Am�liorer la planification et la gestion financi�re de la soci�t� en r�partissant de mani�re pr�cise le co�t de TOUS les espaces entre les d�partements, y compris l�espace associ� aux surfaces communes, aux pi�ces partag�es entre plusieurs occupants et m�me les espaces communs � l�int�rieur de pi�ces.' WHERE ACTIVITY_ID = 'AbSpaceRoomPctChargebackCI';

UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'G�rer � distance l�utilisation des licences, cr�er ou modifier les r�les de s�curit�, ajouter ou supprimer des utilisateurs, assigner des processus m�tier aux utilisateurs et g�rer les listes d��l�ments personnalis�s par utilisateur.', SUMMARY_IT = 'Gestire l�utilizzo di licenze, creare o modificare i ruoli sicurezza, aggiungere o rimuovere utenti, assegnare processi aziendali agli utenti e gestire le voci dell�elenco preferiti degli utenti da qualsiasi posizione.' WHERE ACTIVITY_ID = 'AbSystemAdministration';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_IT = 'Gestire l�impianto di cablaggio orizzontale, oggetto contenuto all�interno delle pareti della struttura.' WHERE ACTIVITY_ID = 'AbTelecomHorizontalCabling';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'G�rez l�utilisation et l�affectation des logiciels sur votre serveur et vos ordinateurs de bureau.', SUMMARY_IT = 'Gestire l�utilizzo e l�assegnazione di software su computer desktop e server.' WHERE ACTIVITY_ID = 'AbTelecomSoftware';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Faire en sorte que les infrastructures supportent la mission de l�entreprise. Acc�der aux services dont le personnel et la direction ont besoin pour travailler efficacement.' WHERE ACTIVITY_ID = 'AbWorkplacePortal';
UPDATE AFM.AFM_ACTIVITIES SET SUMMARY_FR = 'Cette application Web est id�ale pour les responsables d�op�rations ou de services souhaitant fournir � tous les employ�s un moyen simple de r�server des salles de conf�rence et des ressources. Elle se d�ploie � l��chelle de l�entreprise et permet aux employ�s de g�rer tous les aspects de la r�servation de salles et de ressources.  Depuis la gestion des r�servations r�currentes jusqu�� l��tablissement de liens avec la suite d�op�rations ARCHIBUS pour le support, cette application permet aux utilisateurs de se concentrer sur la pr�paration des r�unions proprement dites, et non sur leur logistique.', SUMMARY_IT = 'Per i responsabili delle operazioni o dei servizi che desiderano fornire a tutti i dipendenti la possibilit� di prenotare sale e risorse di conferenza senza alcun problema, questa applicazione basata sul Web che pu� essere implementata in tutta l�azienda, consente di gestire i dettagli di prenotazione relativi a tutti i locali e a tutte le risorse.  Questa applicazione consente agli utenti di concentrare l�attenzione sulla preparazione delle riunioni, dalla gestione delle prenotazioni ricorrenti al collegamento diretto alla suite delle operazioni ARCHIBUS per le attivit� di assistenza, senza doversi preoccupare della logistica.' WHERE ACTIVITY_ID = 'AbWorkplaceReservations';

UPDATE AFM.AFM_ACTIVITY_PARAMS SET PARAM_VALUE = '(activity_log.cost_estimated + activity_log.cost_est_cap) / ${sql.replaceZero(''activity_log.cost_to_replace'')}' WHERE ACTIVITY_ID = 'AbCapitalPlanningCA' AND PARAM_ID = 'FacilityConditionIndex';
UPDATE AFM.AFM_ACTIVITY_PARAMS SET PARAM_VALUE = 'bl;eq;rm;rmpct;dv;dp;fl;em;pt_store_loc_pt' WHERE ACTIVITY_ID = 'AbCommonResources' AND PARAM_ID = 'MobileSyncDataChangesOnlyTables';
UPDATE AFM.AFM_ACTIVITY_PARAMS SET PARAM_VALUE = 'SVCS - DIRECT%' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PARAM_ID = 'CostCategory_DirectServicesAll';
UPDATE AFM.AFM_ACTIVITY_PARAMS SET PARAM_VALUE = 'DISPOSITION - CAPPROJ' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PARAM_ID = 'CostCategory_DispositionCapProj';
UPDATE AFM.AFM_ACTIVITY_PARAMS SET PARAM_VALUE = 'SVCS - INDIRECT%' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PARAM_ID = 'CostCategory_IndirectServicesAll';
UPDATE AFM.AFM_ACTIVITY_PARAMS SET DESCRIPTION = 'List of Analysis Metrics that should be summed to obtain Total Cost of Ownership.', PARAM_VALUE = 'ops_Costs-Maintenance_an_fy;ops_Costs-Custodial_an_fy;ops_Costs-Security_an_fy;ops_Costs-Other_an_fy;ops_Costs-Utility_an_fy;ops_Costs-PropertyTaxes_an_fy;leas_Costs-Rent_an_fy;work-Costs-DirectServices_an_fy;fin_anlys_Income_an_fy;fin_anlys_depreciation_an_fy;fin_anlys_DepreciationCapProj_an_fy;fin_anlys_DepreciationPPE_an_fy' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PARAM_ID = 'UpdateTCO-OwnershipCosts';
UPDATE AFM.AFM_ACTIVITY_PARAMS SET DESCRIPTION = 'List of Analysis Metrics that should be summed to obtain Total Workpoint Cost.', PARAM_VALUE = 'ops_Costs-Maintenance_an_fy;ops_Costs-Custodial_an_fy;ops_Costs-Security_an_fy;ops_Costs-Other_an_fy;ops_Costs-Utility_an_fy;ops_Costs-PropertyTaxes_an_fy;leas_Costs-Rent_an_fy;work-Costs-DirectServices_an_fy;fin_anlys_Income_an_fy;fin_anlys_depreciation_an_fy;fin_anlys_DepreciationCapProj_an_fy;fin_anlys_DepreciationPPE_an_fy;work_Costs-IndirectServices_an_fy;fin_anlys_Interest_an_fy;fin_anlys_CostOfCapital_an_fy' WHERE ACTIVITY_ID = 'AbRPLMStrategicFinancialAnalysis' AND PARAM_ID = 'UpdateTCO-WorkpointCosts';

UPDATE AFM.AFM_ACTS SET ACT_FR = 'G�rer l��quipement' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Equipment';
UPDATE AFM.AFM_ACTS SET ACT_FR = 'G�rer la main d�oeuvre' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources' AND ACT = 'Manage Labor';

UPDATE AFM.AFM_BIM_PARAMS SET PARAMETER_NAME_FR = 'Code d��l�ment r�seau' WHERE AUTO_NUMBER = 121;
UPDATE AFM.AFM_BIM_PARAMS SET PARAMETER_NAME_FR = 'Standard d��l�ment r�seau' WHERE AUTO_NUMBER = 122;
UPDATE AFM.AFM_BIM_PARAMS SET PARAMETER_NAME_FR = 'Code El�ment d�Action' WHERE AUTO_NUMBER = 140;

UPDATE AFM.AFM_CLASS SET ACT_CLASS_FR = 'Analyser l�historique et les finances' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Analyze History and Finances';
UPDATE AFM.AFM_CLASS SET ACT_CLASS_FR = 'G�rer l��quipement et les ressources' WHERE AFM_MODULE = 'BldgOps' AND ACT_CLASS = 'Manage Equipment and Resources';
UPDATE AFM.AFM_CLASS SET ACT_CLASS_FR = 'Evaluation de l��tat du patrimoine' WHERE AFM_MODULE = 'Capital' AND ACT_CLASS = 'Condition Assessment';
UPDATE AFM.AFM_CLASS SET ACT_CLASS_FR = 'G�rer l��quipement' WHERE AFM_MODULE = 'F&E Inv' AND ACT_CLASS = 'Track Equipment';
UPDATE AFM.AFM_CLASS SET ACT_CLASS_FR = 'Gestion des situations d�urgence' WHERE AFM_MODULE = 'Risk Management' AND ACT_CLASS = 'Emergency Preparedness';

UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Texte d�actif de l�action', TITLE_IT = 'Testo dell�asset azione' WHERE RULE_ID = 'AbActsATxt';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Texte d�actif du b�timent', TITLE_IT = 'Testo dell�asset edificio' WHERE RULE_ID = 'AbBlAtxt';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Registres d�activit�s des surbrillances dynamiques' WHERE RULE_ID = 'AbDHltActivity';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltBl';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltCBhazard';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltCBsample';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltCa';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltCity';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltCounty';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltCtry';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltDr';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltEm';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Surbrillance dynamique des simulations d�employ�s' WHERE RULE_ID = 'AbDHltEmTrial';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Surbrillance dynamique de l��quipement' WHERE RULE_ID = 'AbDHltEq';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Surbrillance dynamique des simulations d��quipement' WHERE RULE_ID = 'AbDHltEqTrial';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltFl';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltFn';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltFnTrial';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltFp';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltGp';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltGrid';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltGros';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltJk';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltNetdev';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltParcel';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltParking';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltPb';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltPn';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltProperty';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltRack';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltReg';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltRf';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltRm';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltRmTrial';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Surbrillance dynamique des surfaces d�abord' WHERE RULE_ID = 'AbDHltRunoff';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltServ';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltSite';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltState';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltSu';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltTa';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltTaTrial';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltVert';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltWn';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbDHltWy';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbHltBl';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = 'Regio�s markeren' WHERE RULE_ID = 'AbHltRegn';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbHltRm';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Activer les couches de gestion des situations d�urgence' WHERE RULE_ID = 'AbLyrEP';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Activer les couches d�employ�s' WHERE RULE_ID = 'AbLyrEm';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbLyrMisc';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Publier l�architecture d�arri�re-plan pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubBackground';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Publier l�arri�re-plan d�urgence au format SWF/EMF' WHERE RULE_ID = 'AbPubBackgroundEmergency';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Publier l�arri�re-plan d��quipement au format SWF/EMF' WHERE RULE_ID = 'AbPubBackgroundEquipment';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', RULE_TYPE = 'SVG and EMF' WHERE RULE_ID = 'AbPubBackgroundSVG';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', RULE_TYPE = 'SVG and EMF' WHERE RULE_ID = 'AbPubBlSvg';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de dangers b�timent propre pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubCBHazard';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations d��chantillons de dangers b�timent propre pour les graphiques d�e' WHERE RULE_ID = 'AbPubCBSample';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations d��quipement pour les graphiques d�entreprise', TITLE_IT = 'Pubblica informazioni sull�apparecchiatura per grafici aziendali' WHERE RULE_ID = 'AbPubEq';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de standards de mobilier pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubFn';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de mobilier cod� pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubFt';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de groupes pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubGp';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de prises pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubJk';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de r�glementation pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubRegcompliance';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'YES', RULE_SUFFIX = '-regcompliance', RULE_TYPE = 'SVG and EMF' WHERE RULE_ID = 'AbPubRegcomplianceSvg';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de pi�ces pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubRm';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', RULE_TYPE = 'SVG and EMF', TITLE_FR = 'Publier les pi�ces et l��quipement pour les plans d��tage' WHERE RULE_ID = 'AbPubRmAndEqSvg';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', RULE_TYPE = 'SVG and EMF', TITLE_FR = 'Publier les pi�ces pour les plans d��tage' WHERE RULE_ID = 'AbPubRmSvg';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de locaux pour les graphiques d�entreprise', TITLE_IT = 'Pubblica informazioni sull�unit� immobiliare per grafici aziendali' WHERE RULE_ID = 'AbPubSu';
UPDATE AFM.AFM_DWGPUB SET RULE_TYPE = 'SVG and EMF' WHERE RULE_ID = 'AbPubSuSvg';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Publier les informations de zones pour les graphiques d�entreprise' WHERE RULE_ID = 'AbPubZone';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Plan d�Occupation' WHERE RULE_ID = 'AbQTextEmOccup';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Texte des Demandes d�Intervention en Cours par Equipement' WHERE RULE_ID = 'AbQTextWrEq';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'Texte des Demandes d�Intervention en Cours par Pi�ce' WHERE RULE_ID = 'AbQTextWrRm';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_IT = 'Testo dell�interrogazione locale' WHERE RULE_ID = 'AbQueryTextRm';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Champs de texte d�actif d�Imputation par pi�ce' WHERE RULE_ID = 'AbRmAtxtChgBk';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_FR = 'Texte d�actif de pi�ce, champs minimaux' WHERE RULE_ID = 'AbRmAtxtMin';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbRmOccup';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO' WHERE RULE_ID = 'AbTblEm';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = 'URL d�action', TITLE_NL = 'Actie-URL�s' WHERE RULE_ID = 'AbURLActs';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_NL = '14i Gebouw-URL�s' WHERE RULE_ID = 'AbURLBl';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Plaats-URL�s' WHERE RULE_ID = 'AbURLCity';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Provincie-URL�s' WHERE RULE_ID = 'AbURLCounty';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Land-URL�s' WHERE RULE_ID = 'AbURLCtry';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = '14i URL d��tage', TITLE_NL = '14i Verdieping-URL�s' WHERE RULE_ID = 'AbURLFl';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Groep-URL�s' WHERE RULE_ID = 'AbURLGp';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Vastgoedobject-URL�s' WHERE RULE_ID = 'AbURLProperty';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Regio-URL�s' WHERE RULE_ID = 'AbURLRegn';
UPDATE AFM.AFM_DWGPUB SET IS_ACTIVE = 'NO', TITLE_NL = '14i Ruimte-URL�s' WHERE RULE_ID = 'AbURLRm';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Locatie-URL�s' WHERE RULE_ID = 'AbURLSite';
UPDATE AFM.AFM_DWGPUB SET TITLE_FR = '14i URL d��tat', TITLE_NL = '14i Staat-URL�s' WHERE RULE_ID = 'AbURLState';
UPDATE AFM.AFM_DWGPUB SET TITLE_NL = '14i Suite-URL�s' WHERE RULE_ID = 'AbURLSu';
UPDATE AFM.AFM_DWGPUB SET TITLE_IT = 'Esempio testo dell�interrogazione' WHERE RULE_ID = 'Z_AbAQueryText';

UPDATE AFM.AFM_FLDS SET AFM_TYPE = 2070, COMMENTS = 'v23.2 Mobile Compliance' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc1_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc2_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc3_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc4_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc_isnew';
UPDATE AFM.AFM_FLDS SET AFM_TYPE = 2070, COMMENTS = 'v23.2 Mobile Compliance', VALIDATE_DATA = 1 WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS SET AFM_TYPE = 2070, COMMENTS = 'v23.2 Mobile Compliance', SL_HEADING = NULL WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = 'NONE;None;ALL;All;SVG;SVG;SVG and EMF;SVG and EMF;SVG and JSON;SVG and JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF and EMF;JSON;JSON;BY OWNER;By Owner;LAYERED BY OWNER;Layered By Owner;QUERY TEXT;Query Text;QUERY TABLE;Query Table;ASSET;Asset;RESTRICTION;Restriction;URL;URL;DYNAMIC;Dynamic' WHERE TABLE_NAME = 'afm_dwgpub' AND FIELD_NAME = 'rule_type';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 32 WHERE TABLE_NAME = 'asset_trans' AND FIELD_NAME = 'mod_field';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 32 WHERE TABLE_NAME = 'asset_trans' AND FIELD_NAME = 'mod_table';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = 'Location;Location;Ownership;Ownership;Status;Status;Value;Value;Owner Custodian;Owner Custodian' WHERE TABLE_NAME = 'asset_trans' AND FIELD_NAME = 'trans_type';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 32 WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'campus_id';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 32 WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'contact_name';
UPDATE AFM.AFM_FLDS SET ATTRIBUTES = '<root>'||CHR(13)||CHR(10)
||'      <documentManagement maxDocumentSize="100000000" versioningOn="true" autoNameFile="false">'||CHR(13)||CHR(10)
||'              <lockingOptions lockOnCheckout="true" clearLocksOnCheckIn="true"/>'||CHR(13)||CHR(10)
||'       </documentManagement>'||CHR(13)||CHR(10)
||'</root>' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'doc';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'doc_isnew';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 36 WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'source_record_id';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = 'in;In service;out;Out of Service;rep;In Repair;stor;In storage;salv;Salvaged;sold;Sold;miss;Missing;disp;Disposed;don;Donated;sto;Stolen;todi;To be disposed' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'tc_eq_id';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'tc_eqport_id';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'tc_jk_id';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'tc_pnport_id';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'survey_photo_eq_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'survey_redline_eq_isnew';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'eqport' AND FIELD_NAME = 'tc_eqport_id';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'eqport' AND FIELD_NAME = 'tc_pnport_id';
UPDATE AFM.AFM_FLDS SET ML_HEADING = 'Original Date Escalation'||CHR(10)
||'for Completion Occurs' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'date_esc_comp_orig';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 4000 WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS SET ML_HEADING = 'Request Parameters'||CHR(10)
||'Updated?' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'request_params_updated';

UPDATE AFM.AFM_FLDS SET COMMENTS = 'Trinidad; v.23.2 Move Management - added Team to enum list', ENUM_LIST = 'Employee;Employee;New Hire;New Hire;Leaving;Employee Leaving;Equipment;Equipment;Asset;Asset;Room;Room;Team;Team;' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'mo_type';
UPDATE AFM.AFM_FLDS SET ALLOW_NULL = 1, PRIMARY_KEY = 0, VALIDATE_DATA = 0 WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS SET PRIMARY_KEY = 0 WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS SET PRIMARY_KEY = 0 WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'scenario_id';
UPDATE AFM.AFM_FLDS SET COMMENTS = 'FASB 842 Update', ML_HEADING = 'Cost - Estimated', NUM_FORMAT = 1 WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'cost_est';
UPDATE AFM.AFM_FLDS SET COMMENTS = 'FASB 842 Update', ENUM_LIST = 'N/A;N/A;AUTOMATIC TRANSFER OWNERSHIP;AUTOMATIC TRANSFER OF OWNERSHIP;CONTRACTION;CONTRACTION;EARLY TERMINATION;EARLY TERMINATION;EXPANSION;EXPANSION;EXTENSION;EXTENSION;OPTION TO BUY;OPTION TO BUY;RENEWAL;RENEWAL;RENEW LONGER TERM;RENEW LONGER TERM;OPTION TO SUBLET;OPTION TO SUBLET;IMPROVEMENTS;IMPROVEMENTS;REASSIGNMENT;REASSIGNMENT;LEASE END;LEASE END', AFM_SIZE = 32 WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'op_type';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 36 WHERE TABLE_NAME = 'org' AND FIELD_NAME = 'org_id';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = 'Pipeline;Pipeline;UNDER CONTRACT;Under Contract;ESCROWED;Escrowed;IN SERVICE;In Service;Owned;Owned;OWNED AND LEASED;Owned and Leased;LEASED;Leased;SUB-LEASED;Sub-Leased;SUB LET;Sub Let;FOR SALE;For Sale;LEASED (EXPIRED);Leased (Expired);OUT OF SERVICE;Out of Service;ABANDONED;Abandoned;DONATED;Donated;Disposed;Disposed;SOLD;Sold;UNKNOWN;Unknown;N/A;N/A;To be disposed;To be disposed' WHERE TABLE_NAME = 'ot' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS SET COMMENTS = 'V.23.2 Building Operations PM Planning', DFLT_VAL = 'r', ENUM_LIST = 'r;Recurrence Pattern;d;Days;ww;Weeks;m;Months;q;Quarters;yyyy;Years;i;Miles;h;Hours;e;Meter;a;Manual' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'interval_type';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'pnport' AND FIELD_NAME = 'tc_pnport_id';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = 'PIPELINE;Pipeline;UNDER CONTRACT;Under Contract;ESCROWED;Escrowed;IN SERVICE;In Service;OWNED;Owned;OWNED AND LEASED;Owned and Leased;LEASED;Leased;SUB-LEASED;Sub-Leased;SUB LET;Sub Let;FOR SALE;For Sale;LEASED (EXPIRED);Leased (Expired);OUT OF SERVICE;Out of Service;ABANDONED;Abandoned;DONATED;Donated;DISPOSED;Disposed;SOLD;Sold;N/A;N/A;UNKNOWN;Unknown;todi;To be disposed' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS SET COMMENTS = 'v23.2 Compliance - Contract Management', ML_HEADING = 'Cost - Penalty/Fines'||CHR(13)||CHR(10)
||'(Budget)', NUM_FORMAT = 1, AFM_SIZE = 12 WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 4000 WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'recurring_rule';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 36 WHERE TABLE_NAME = 'rm' AND FIELD_NAME = 'org_id';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 36 WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'org_id';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 4000 WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS SET ML_HEADING = 'Space Requirements '||CHR(13)||CHR(10)
||'Description / Source / Rationale' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'sb_desc';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'surveyrm_sync' AND FIELD_NAME = 'survey_photo_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'surveyrm_sync' AND FIELD_NAME = 'survey_redline_rm_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = 'in;In Use;out;Out of Service;rep;In Repair;stor;In Storage;salv;Salvaged;sold;Sold;miss;Missing;disp;Disposed;don;Donated;sto;Stolen;todi;To be disposed' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 36 WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'org_id';
UPDATE AFM.AFM_FLDS SET VALIDATE_DATA = 1 WHERE TABLE_NAME = 'vn_rate' AND FIELD_NAME = 'block_ref';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 4000 WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS SET ML_HEADING = 'Rejected'||CHR(10)
||'Step' WHERE TABLE_NAME = 'wr_step_waiting' AND FIELD_NAME = 'rejected_step';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc1_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc2_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc3_isnew';
UPDATE AFM.AFM_FLDS SET ENUM_LIST = '0;No;1;Yes' WHERE TABLE_NAME = 'wr_sync' AND FIELD_NAME = 'doc4_isnew';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 4000 WHERE TABLE_NAME = 'wrhwr' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS SET AFM_SIZE = 4000 WHERE TABLE_NAME = 'wrview' AND FIELD_NAME = 'description';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� manutenzione '||CHR(13)||CHR(10)
||'preventiva?' WHERE TABLE_NAME = 'acbu' AND FIELD_NAME = 'is_pm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Couleur de'||CHR(13)||CHR(10)
||'l��tiquette' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'label_clr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Deuxi�me couleur'||CHR(13)||CHR(10)
||'d��tiquette' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'label_clr2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Deuxi�me source de donn�es'||CHR(13)||CHR(10)
||'d��tiquette' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'label_ds2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Hauteur de'||CHR(13)||CHR(10)
||'l��tiquette' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'label_ht';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Deuxi�me hauteur'||CHR(13)||CHR(10)
||'d��tiquette' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'label_ht2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Hauteur d��tiquette'||CHR(13)||CHR(10)
||'PDF' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'label_ht_pdf';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Lignes d��tiquette'||CHR(13)||CHR(10)
||'max.' WHERE TABLE_NAME = 'active_plantypes' AND FIELD_NAME = 'max_label_lines';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de'||CHR(13)||CHR(10)
||'l�Action' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'action_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Associ� �'||CHR(13)||CHR(10)
||'l�ID d��valuation' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'assessment_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er Automatiquement une'||CHR(13)||CHR(10)
||'Demande d�Intervention ?' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Unassigned;Non affect�;Def Maint;Maintenance diff�r�e;Capital Renewal;Renouvellement/Modernisation d�investissements;Adaptation;Adaptation', ML_HEADING_FR = 'Programme'||CHR(13)||CHR(10)
||'d�Investissements' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'capital_program';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�Unit�s' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'cb_units_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeurs d��tat'||CHR(13)||CHR(10)
||'du patrimoine' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'cond_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Copi� �'||CHR(13)||CHR(10)
||'partir de l�ID' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'copied_from';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Approbation' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique de date d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'date_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Installation' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ouvrier responsable'||CHR(13)||CHR(10)
||'de l��limination' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'hcm_abate_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'N� de'||CHR(13)||CHR(10)
||'racores' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'hcm_fittings_num';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'En attente d�action' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'hcm_pending_act';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'N� de'||CHR(13)||CHR(10)
||'tuber�as' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'hcm_pipe_cnt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pourcentage '||CHR(13)||CHR(10)
||'d�Ach�vement' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'pct_complete';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Associ� au'||CHR(13)||CHR(10)
||'code �l�ment d�action' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'related_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Non saisi;1;Maintenance;2;Ressource naturelle;3;Recyclable;4;Qualit� de l�air ambiant;5;Economies d�eau;6;Produits chimiques;7;D�chets toxiques;8;D�chets solides;9;Emissions;10;Conso. d��nergie', ENUM_LIST_IT = '0;Non specificato;1;Gestione;2;Risorsa naturale;3;Materiali riciclabili;4;Qualit� dell�aria interna;5;Risparmio idrico;6;Composti chimici;7;Rifiuti pericolosi;8;Rifiuti solidi;9;Emissione;10;Utilizzo energia' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'sust_priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'pour ex�cution �' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'time_esc_comp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'time_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'activity_log' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'cuenta', ML_HEADING_NL = 'Account-'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Poste'||CHR(13)||CHR(10)
||'Comptable' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�Action' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'action_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code El�ment d�Action' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Action' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Asociado con'||CHR(13)||CHR(10)
||'ID de evaluaci�n', ML_HEADING_NL = 'Gekoppeld aan'||CHR(13)||CHR(10)
||'evaluatie-ID', ML_HEADING_FR = 'Associ� �'||CHR(13)||CHR(10)
||'l�identifiant d��valuation' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'assessment_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er automatiquement une demande d�intervention�?' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Unassigned;Non affect�;Def Maint;Maintenance diff�r�e;Capital Renewal;Renouvellement/Modernisation d�investissements;Adaptation;Adaptation', ML_HEADING_FR = 'Programme d�investissements' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'capital_program';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'addetto alla manutenzione', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'vakman', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'technicien', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Handwerker' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'cf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeurs d��tat du patrimoine' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'cond_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Copi� � partir de l�ID' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'copied_from';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'de co�ts' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'cost_cat_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'approvazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�approbation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'genehmigt' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding voor'||CHR(13)||CHR(10)
||'uitvoeringstijd optreedt', ML_HEADING_FR = 'Recours hi�rarchique pour'||CHR(13)||CHR(10)
||'ex�cution le' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'date_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding'||CHR(13)||CHR(10)
||'responstijd optreedt', ML_HEADING_FR = 'Remise � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse le', ML_HEADING_DE = 'Termineskalation f�r'||CHR(13)||CHR(10)
||'Reaktion tritt auf' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'date_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Installation' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���� /'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Handle de entidad/'||CHR(13)||CHR(10)
||'Identificador �nico', ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique', ML_HEADING_DE = 'Objektreferenz/'||CHR(13)||CHR(10)
||'Eindeutige ID' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����'||CHR(13)||CHR(10)
||'��?', ML_HEADING_ES = '�Escalado para'||CHR(13)||CHR(10)
||'terminaci�n?', ML_HEADING_FR = 'Remont� � la'||CHR(13)||CHR(10)
||'hi�rarchie pour ex�cution�?' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'escalated_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����'||CHR(13)||CHR(10)
||'��?', ML_HEADING_ES = '�Escalado para'||CHR(13)||CHR(10)
||'respuesta?', ML_HEADING_NL = 'Responstijd'||CHR(13)||CHR(10)
||'overschreden ?', ML_HEADING_FR = 'Remont� � la'||CHR(13)||CHR(10)
||'hi�rarchie pour r�ponse�?' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'escalated_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Gestor de centro'||CHR(13)||CHR(10)
||'de soporte', ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'du centre de services', ML_HEADING_DE = 'Servicedesk'||CHR(13)||CHR(10)
||'Manager' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'manager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pourcentage'||CHR(13)||CHR(10)
||'d�ach�vement' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'pct_complete';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'T�l�phone du'||CHR(13)||CHR(10)
||'demandeur', ML_HEADING_DE = 'Telefonnummer'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'phone_requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'PO-'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_DE = 'IH-'||CHR(13)||CHR(10)
||'Vorgang' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'pmp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'���', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'orden de compra', ML_HEADING_NL = 'Code van bestelbon', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'bon de commande', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Bestellung' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'Progetto', ML_HEADING_DE = 'Projektcode' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Terugkerende'||CHR(13)||CHR(10)
||'regel', ML_HEADING_FR = 'R�gle'||CHR(13)||CHR(10)
||'r�currente' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'recurring_rule';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Calificaci�n de'||CHR(13)||CHR(10)
||'la satisfacci�n', ML_HEADING_NL = 'Tevredenheids-'||CHR(13)||CHR(10)
||'score', ML_HEADING_FR = 'Note de'||CHR(13)||CHR(10)
||'satisfaction', ML_HEADING_DE = 'Zufriedenheitsbewertung' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'satisfaction';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Notas de'||CHR(13)||CHR(10)
||'satisfacci�n', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'bij waardering' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'satisfaction_notes';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_NL = 'Stap-'||CHR(13)||CHR(10)
||'status', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l��tape' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Non saisi;1;Maintenance;2;Ressource naturelle;3;Recyclable;4;Qualit� de l�air ambiant;5;Economies d�eau;6;Produits chimiques;7;D�chets toxiques;8;D�chets solides;9;Emissions;10;Conso. d��nergie', ENUM_LIST_IT = '0;Non specificato;1;Gestione;2;Risorsa naturale;3;Materiali riciclabili;4;Qualit� dell�aria interna;5;Risparmio idrico;6;Composti chimici;7;Rifiuti pericolosi;8;Rifiuti solidi;9;Emissione;10;Utilizzo energia' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'sust_priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de terminaci�n', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'completamento', ML_HEADING_NL = 'Tijdstip waarop uitvoeringstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'time_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de respuesta', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'risposta', ML_HEADING_NL = 'Tijdstip waarop responstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'time_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de'||CHR(13)||CHR(10)
||'solicitud' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'time_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora'||CHR(13)||CHR(10)
||'obbligatoria', ML_HEADING_NL = 'Benodigde'||CHR(13)||CHR(10)
||'tijd vereist', ML_HEADING_FR = 'Requis'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'time_required';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Leveranciers-'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Lieferanten-'||CHR(13)||CHR(10)
||'Code' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��������', ML_HEADING_ES = 'C�digo de estructura de desglose de trabajo', ML_HEADING_IT = 'Codice struttura scomposizione lavoro', ML_HEADING_NL = 'Structuurcode werkspecificatie', ML_HEADING_FR = 'Code de nomenclature des travaux', ML_HEADING_DE = 'Objektcode der Arbeitsanalyse' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'wbs_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�', ML_HEADING_NL = 'Werkorder-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de bon'||CHR(13)||CHR(10)
||'de travaux', ML_HEADING_DE = 'Arbeitsauftrags-Nr.' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'wo_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de equipo de trabajo', ML_HEADING_IT = 'Codice team di lavoro', ML_HEADING_NL = 'Werkteam code', ML_HEADING_FR = 'Code de l��quipe d�intervention', ML_HEADING_DE = 'Kurzzeichen Arbeitsteam' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'nummer' WHERE TABLE_NAME = 'activity_log_hactivity_log' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�� ID', ML_HEADING_ES = 'ID de solicitud '||CHR(13)||CHR(10)
||'de servicio', ML_HEADING_IT = 'ID richiesta '||CHR(13)||CHR(10)
||'di servizio', ML_HEADING_NL = 'Onderhoudsaanvraag-'||CHR(13)||CHR(10)
||'ID', ML_HEADING_FR = 'Identifiant de '||CHR(13)||CHR(10)
||'demande de service', ML_HEADING_DE = 'Serviceanforderungs-'||CHR(13)||CHR(10)
||'ID' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'���', ML_HEADING_ES = 'Respuesta por'||CHR(13)||CHR(10)
||'c�digo de operario', ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Code van vakman', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'code de technicien' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'cf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha'||CHR(13)||CHR(10)
||'de creaci�n', ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'di creazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'Erstellt' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de '||CHR(13)||CHR(10)
||'respuesta', ML_HEADING_IT = 'Data '||CHR(13)||CHR(10)
||'risposta a', ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'gereageerd op', ML_HEADING_FR = 'R�pondu '||CHR(13)||CHR(10)
||'le', ML_HEADING_DE = 'Geantwortet '||CHR(13)||CHR(10)
||'am' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'date_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Naam van medewerker', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�employ�' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_DE = 'Anfrage -'||CHR(13)||CHR(10)
||'Status' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Paso de flujo'||CHR(13)||CHR(10)
||'de trabajo', ML_HEADING_NL = 'Werkstroom'||CHR(13)||CHR(10)
||'Stap', ML_HEADING_FR = 'Etape du'||CHR(13)||CHR(10)
||'workflow', ML_HEADING_DE = 'Workflow-'||CHR(13)||CHR(10)
||'Schritt' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'step';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo'||CHR(13)||CHR(10)
||'de paso', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'step_code';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�� Id', ML_HEADING_ES = 'ID de registro'||CHR(13)||CHR(10)
||'de paso', ML_HEADING_IT = 'ID registro'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_NL = 'Stap'||CHR(13)||CHR(10)
||'logboek-ID', ML_HEADING_FR = 'ID de registre'||CHR(13)||CHR(10)
||'d��tapes' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'step_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'de paso', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'procedura', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape', ML_HEADING_DE = 'Schritt-'||CHR(13)||CHR(10)
||'typ' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de'||CHR(13)||CHR(10)
||'creaci�n', ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'Gecre�erd', ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Erstellungs-'||CHR(13)||CHR(10)
||'zeitpunkt' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'time_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora'||CHR(13)||CHR(10)
||'en que se respondi�', ML_HEADING_IT = 'Ora'||CHR(13)||CHR(10)
||'risposta a', ML_HEADING_NL = 'Tijd waarop'||CHR(13)||CHR(10)
||'gereageerd werd', ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'de r�ponse �', ML_HEADING_DE = 'Reagiert'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'time_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Gebruikersnaam', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�utilisateur' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Antwoord per'||CHR(13)||CHR(10)
||'Code van leverancier', ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'code de fournisseur' WHERE TABLE_NAME = 'activity_log_step_waiting' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'acci�n', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action', ML_HEADING_DE = 'Ma�nahmentyp' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeurs d��tat'||CHR(13)||CHR(10)
||'du patrimoine' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'cond_value';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc1_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc2_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc3_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc4_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'doc_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'Progetto', ML_HEADING_DE = 'Projektcode' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS_LANG SET SL_HEADING_CH = NULL, SL_HEADING_DE = NULL, SL_HEADING_ES = NULL, SL_HEADING_FR = NULL, SL_HEADING_IT = NULL, SL_HEADING_NL = NULL WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Non saisi;1;Maintenance;2;Ressource naturelle;3;Recyclable;4;Qualit� de l�air ambiant;5;Economies d�eau;6;Produits chimiques;7;D�chets toxiques;8;D�chets solides;9;Emissions;10;Conso. d��nergie', ENUM_LIST_IT = '0;Non specificato;1;Gestione;2;Risorsa naturale;3;Materiali riciclabili;4;Qualit� dell�aria interna;5;Risparmio idrico;6;Composti chimici;7;Rifiuti pericolosi;8;Rifiuti solidi;9;Emissione;10;Utilizzo energia' WHERE TABLE_NAME = 'activity_log_sync' AND FIELD_NAME = 'sust_priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'activity_log_trans' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pourcentage '||CHR(13)||CHR(10)
||'d�Ach�vement' WHERE TABLE_NAME = 'activity_log_trans' AND FIELD_NAME = 'pct_complete';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Importation' WHERE TABLE_NAME = 'activity_log_trans' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'cuenta', ML_HEADING_NL = 'Account-'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Poste'||CHR(13)||CHR(10)
||'Comptable' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�Action' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'action_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code El�ment d�Action' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Action' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Asociado con'||CHR(13)||CHR(10)
||'ID de evaluaci�n', ML_HEADING_NL = 'Gekoppeld aan'||CHR(13)||CHR(10)
||'evaluatie-ID', ML_HEADING_FR = 'Associ� �'||CHR(13)||CHR(10)
||'l�identifiant d��valuation' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'assessment_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er automatiquement une demande d�intervention�?' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Unassigned;Non affect�;Def Maint;Maintenance diff�r�e;Capital Renewal;Renouvellement/Modernisation d�investissements;Adaptation;Adaptation', ML_HEADING_FR = 'Programme d�investissements' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'capital_program';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'addetto alla manutenzione', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'vakman', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'technicien', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Handwerker' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'cf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeurs d��tat du patrimoine' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'cond_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Copi� � partir de l�ID' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'copied_from';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'de co�ts' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'cost_cat_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Kosten-'||CHR(13)||CHR(10)
||'afwijking', ML_HEADING_FR = 'Variance'||CHR(13)||CHR(10)
||'des co�ts', ML_HEADING_DE = 'Abweichung'||CHR(13)||CHR(10)
||'der Kosten' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'cost_var';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'approvazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�approbation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'genehmigt' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding voor'||CHR(13)||CHR(10)
||'uitvoeringstijd optreedt', ML_HEADING_FR = 'Recours hi�rarchique pour'||CHR(13)||CHR(10)
||'ex�cution le' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'date_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding'||CHR(13)||CHR(10)
||'responstijd optreedt', ML_HEADING_FR = 'Remise � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse le', ML_HEADING_DE = 'Termineskalation f�r'||CHR(13)||CHR(10)
||'Reaktion tritt auf' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'date_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Installation' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���� /'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Handle de entidad/'||CHR(13)||CHR(10)
||'Identificador �nico', ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique', ML_HEADING_DE = 'Objektreferenz/'||CHR(13)||CHR(10)
||'Eindeutige ID' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'afronding?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Erledigung?' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'escalated_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'antwoord?', ML_HEADING_FR = 'Remont� � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Reaktion?' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'escalated_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Gestor de centro'||CHR(13)||CHR(10)
||'de soporte', ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'du centre de services', ML_HEADING_DE = 'Servicedesk'||CHR(13)||CHR(10)
||'Manager' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'manager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pourcentage'||CHR(13)||CHR(10)
||'d�ach�vement' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'pct_complete';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Tel�fono '||CHR(13)||CHR(10)
||'de solicitante', ML_HEADING_NL = 'Telefoon '||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'T�l�phone du '||CHR(13)||CHR(10)
||'demandeur', ML_HEADING_DE = 'Telefonnummer '||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'phone_requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'PO-'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_DE = 'IH-'||CHR(13)||CHR(10)
||'Vorgang' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'pmp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'���', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'orden de compra', ML_HEADING_NL = 'Code van bestelbon', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'bon de commande', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Bestellung' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'Progetto', ML_HEADING_DE = 'Projektcode' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Terugkerende'||CHR(13)||CHR(10)
||'regel', ML_HEADING_FR = 'R�gle'||CHR(13)||CHR(10)
||'r�currente' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'recurring_rule';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Calificaci�n de'||CHR(13)||CHR(10)
||'la satisfacci�n', ML_HEADING_NL = 'Tevredenheids-'||CHR(13)||CHR(10)
||'score', ML_HEADING_FR = 'Note de'||CHR(13)||CHR(10)
||'satisfaction', ML_HEADING_DE = 'Zufriedenheitsbewertung' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'satisfaction';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Notas de'||CHR(13)||CHR(10)
||'satisfacci�n', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'bij waardering' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'satisfaction_notes';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_NL = 'Stap-'||CHR(13)||CHR(10)
||'status', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l��tape' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Non saisi;1;Maintenance;2;Ressource naturelle;3;Recyclable;4;Qualit� de l�air ambiant;5;Economies d�eau;6;Produits chimiques;7;D�chets toxiques;8;D�chets solides;9;Emissions;10;Conso. d��nergie', ENUM_LIST_IT = '0;Non specificato;1;Gestione;2;Risorsa naturale;3;Materiali riciclabili;4;Qualit� dell�aria interna;5;Risparmio idrico;6;Composti chimici;7;Rifiuti pericolosi;8;Rifiuti solidi;9;Emissione;10;Utilizzo energia' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'sust_priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de terminaci�n', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'completamento', ML_HEADING_NL = 'Tijdstip waarop uitvoeringstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'time_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de respuesta', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'risposta', ML_HEADING_NL = 'Tijdstip waarop responstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'time_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de'||CHR(13)||CHR(10)
||'solicitud' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'time_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora'||CHR(13)||CHR(10)
||'obbligatoria', ML_HEADING_NL = 'Benodigde'||CHR(13)||CHR(10)
||'tijd vereist', ML_HEADING_FR = 'Requis'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'time_required';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Leveranciers-'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Lieferanten-'||CHR(13)||CHR(10)
||'Code' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��������', ML_HEADING_ES = 'C�digo de estructura de desglose de trabajo', ML_HEADING_IT = 'Codice struttura scomposizione lavoro', ML_HEADING_NL = 'Structuurcode werkspecificatie', ML_HEADING_FR = 'Code de nomenclature des travaux', ML_HEADING_DE = 'Objektcode der Arbeitsanalyse' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'wbs_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�', ML_HEADING_NL = 'Werkorder-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de bon'||CHR(13)||CHR(10)
||'de travaux', ML_HEADING_DE = 'Arbeitsauftrags-Nr.' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'wo_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de equipo de trabajo', ML_HEADING_IT = 'Codice team di lavoro', ML_HEADING_NL = 'Werkteam code', ML_HEADING_FR = 'Code de l��quipe d�intervention', ML_HEADING_DE = 'Kurzzeichen Arbeitsteam' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'nummer' WHERE TABLE_NAME = 'activity_logview' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (langue 01)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (langue 02)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (langue 03)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (chinois simplifi�)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_ch';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (allemand)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_de';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (espagnol)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_es';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (fran�ais)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_fr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (italien)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_it';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (japonais)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_jp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (cor�en)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_ko';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (n�erlandais)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_nl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (norv�gien)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_no';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action (chinois traditionnel)' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'activity_type_zh';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er Automatiquement une'||CHR(13)||CHR(10)
||'Demande d�Intervention ?' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description du'||CHR(13)||CHR(10)
||'Type d�Action' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Instructions du'||CHR(13)||CHR(10)
||'Type d�Action' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'instructions';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de Probl�me du'||CHR(13)||CHR(10)
||'Type d�Action' WHERE TABLE_NAME = 'activitytype' AND FIELD_NAME = 'prob_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code El�ment d�Action' WHERE TABLE_NAME = 'actscns' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Objet'||CHR(13)||CHR(10)
||'de l�email' WHERE TABLE_NAME = 'advisory' AND FIELD_NAME = 'subject';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_activities' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ de l�employ� affect�'||CHR(13)||CHR(10)
||'au Workflow' WHERE TABLE_NAME = 'afm_activities' AND FIELD_NAME = 'workflow_assignee_field';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ de statut'||CHR(13)||CHR(10)
||'de l��tape du Workflow' WHERE TABLE_NAME = 'afm_activities' AND FIELD_NAME = 'workflow_step_status_field';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Gestionnaire'||CHR(13)||CHR(10)
||'d��tapes' WHERE TABLE_NAME = 'afm_activities' AND FIELD_NAME = 'workflow_stepmanager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'afm_activity_cats' AND FIELD_NAME = 'activity_cat_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_activity_cats' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�applique �' WHERE TABLE_NAME = 'afm_activity_params' AND FIELD_NAME = 'applies_to';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'afm_actprods' AND FIELD_NAME = 'activity_cat_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe'||CHR(13)||CHR(10)
||'d�Activit�s' WHERE TABLE_NAME = 'afm_acts' AND FIELD_NAME = 'act_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_acts' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_acts' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_acts' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�Actif'||CHR(13)||CHR(10)
||'Encadr� ?' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_boxed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Style de'||CHR(13)||CHR(10)
||'G�n�ration'||CHR(13)||CHR(10)
||'du Texte d�Actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_gen';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Hauteur du'||CHR(13)||CHR(10)
||'Texte d�Actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_ht';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Hauteur (cm)'||CHR(13)||CHR(10)
||'Texte d�actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_ht_cm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Hauteur (pouc)'||CHR(13)||CHR(10)
||'Texte d�Actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_ht_in';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Style du'||CHR(13)||CHR(10)
||'Texte d�Actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_style';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'D�calage  X (cm)'||CHR(13)||CHR(10)
||'Texte d�actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_xoff_cm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'D�calage X (pouc)'||CHR(13)||CHR(10)
||'Texte d�actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_xoff_in';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'D�calage Y (cm)'||CHR(13)||CHR(10)
||'Texte d�actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_yoff_cm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'D�calage Y (pouc)'||CHR(13)||CHR(10)
||'Texte d�actif' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'atxt_yoff_in';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Entit�' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'entity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Insertion'||CHR(13)||CHR(10)
||'� l��chelle?' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'scaled_ins';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Table'||CHR(13)||CHR(10)
||'d�Actifs' WHERE TABLE_NAME = 'afm_atyp' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Table d�actif'||CHR(13)||CHR(10)
||'ARCHIBUS' WHERE TABLE_NAME = 'afm_bim_families' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Attivo?' WHERE TABLE_NAME = 'afm_bim_params' AND FIELD_NAME = 'is_active';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Table d�actif'||CHR(13)||CHR(10)
||'ARCHIBUS' WHERE TABLE_NAME = 'afm_bim_params' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe'||CHR(13)||CHR(10)
||'d�Activit�s' WHERE TABLE_NAME = 'afm_cats' AND FIELD_NAME = 'act_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_cats' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_cats' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_cats' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe'||CHR(13)||CHR(10)
||'d�Activit�s' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Langue 01)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Langue 02)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Langue 03)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Chinois - Simplifi�)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_ch';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Allemand)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_de';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Espagnol)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_es';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Fran�ais)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_fr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Italien)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_it';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Japonais)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_jp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Cor�en)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_ko';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(N�erlandais)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_nl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Norv�gien)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_no';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe d�activit�s'||CHR(13)||CHR(10)
||'(Chinois - Traditionnel)' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'act_class_zh';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_class' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�utilisateur'||CHR(13)||CHR(10)
||'de connexion' WHERE TABLE_NAME = 'afm_connector' AND FIELD_NAME = 'conn_user';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�utilisateur'||CHR(13)||CHR(10)
||'FTP' WHERE TABLE_NAME = 'afm_connector' AND FIELD_NAME = 'ftp_user';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Connecteur'||CHR(13)||CHR(10)
||'d�en-t�te' WHERE TABLE_NAME = 'afm_connector' AND FIELD_NAME = 'header_connector';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Processus'||CHR(13)||CHR(10)
||'d�enregistrement' WHERE TABLE_NAME = 'afm_connector' AND FIELD_NAME = 'post_process';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_ES = '0;Nada;1;Comillas dobles (``);2;Comillas sencillas (`)', ENUM_LIST_FR = '0;AUCUN;1;Guillemet (�);2;Apostrophe (�)' WHERE TABLE_NAME = 'afm_connector' AND FIELD_NAME = 'text_qualifier';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'afm_data_event_log' AND FIELD_NAME = 'event_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'afm_data_event_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ '||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docs' AND FIELD_NAME = 'field_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� Principale'||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docs' AND FIELD_NAME = 'pkey_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Table '||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docs' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Enregistrement' WHERE TABLE_NAME = 'afm_docvers' AND FIELD_NAME = 'checkin_date';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�Enregistrement' WHERE TABLE_NAME = 'afm_docvers' AND FIELD_NAME = 'checkin_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ '||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docvers' AND FIELD_NAME = 'field_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� Principale'||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docvers' AND FIELD_NAME = 'pkey_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Table '||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docvers' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Enregistrement' WHERE TABLE_NAME = 'afm_docversarch' AND FIELD_NAME = 'checkin_date';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�Enregistrement' WHERE TABLE_NAME = 'afm_docversarch' AND FIELD_NAME = 'checkin_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ '||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docversarch' AND FIELD_NAME = 'field_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� Principale'||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docversarch' AND FIELD_NAME = 'pkey_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Table '||CHR(13)||CHR(10)
||'d�Inventaire' WHERE TABLE_NAME = 'afm_docversarch' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Couches'||CHR(13)||CHR(10)
||'d�Arri�re-Plan' WHERE TABLE_NAME = 'afm_dwgpub' AND FIELD_NAME = 'layer_background';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Afficher le'||CHR(13)||CHR(10)
||'Champ d�Heure ?' WHERE TABLE_NAME = 'afm_dwgpub' AND FIELD_NAME = 'lgnd_show_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ajuster le Texte de'||CHR(13)||CHR(10)
||'Requ�te dans l�Actif ?' WHERE TABLE_NAME = 'afm_dwgpub' AND FIELD_NAME = 'qtxt_fit';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'NONE;�;ALL;��;SVG;SVG;SVG and EMF;SVG � EMF;SVG and JSON;SVG � JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF � EMF;JSON;JSON;BY OWNER;���;LAYERED BY OWNER;�����;QUERY TEXT;����;QUERY TABLE;���;ASSET;��;RESTRICTION;��;URL;URL;DYNAMIC;��', ENUM_LIST_DE = 'NONE;Keine;ALL;Alle;SVG;SVG;SVG and EMF;SVG und EMF;SVG and JSON;SVG und JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF and EMF;JSON;JSON;BY OWNER;Nach Besitzer;LAYERED BY OWNER;Layer nach Besitzer;QUERY TEXT;Abfragetext;QUERY TABLE;Abfragetabelle;ASSET;Objekt;RESTRICTION;Einschr�nkung;URL;URL;DYNAMIC;Dynamisch', ENUM_LIST_ES = 'NONE;Nada;ALL;Todo;SVG;SVG;SVG and EMF;SVG y EMF;SVG and JSON;SVG y JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF y EMF;JSON;JSON;BY OWNER;Por propietario;LAYERED BY OWNER;En capas por propietario;QUERY TEXT;Texto de consulta;QUERY TABLE;Tabla de consulta;ASSET;Activo;RESTRICTION;Restricci�n;URL;URL;DYNAMIC;Din�mica', ENUM_LIST_FR = 'NONE;Aucun;ALL;Tous;SVG;SVG;SVG and EMF;SVG et EMF;SVG and JSON;SVG et JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF et EMF;JSON;JSON;BY OWNER;Par propri�taire;LAYERED BY OWNER;Couche par propri�taire;QUERY TEXT;Texte de requ�te;QUERY TABLE;Table de requ�te;ASSET;Actif;RESTRICTION;Restriction;URL;URL;DYNAMIC;Dynamique', ENUM_LIST_IT = 'NONE;Nessuno;ALL;Tutti;SVG;SVG;SVG and EMF;SVG ed EMF;SVG and JSON;SVG e JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF ed EMF;JSON;JSON;BY OWNER;Per proprietario;LAYERED BY OWNER;Layer per proprietario;QUERY TEXT;Testo query;QUERY TABLE;Tabella query;ASSET;Asset;RESTRICTION;Limitazione;URL;URL;DYNAMIC;Dinamica', ENUM_LIST_NL = 'NONE;Geen;ALL;Alles;SVG;SVG;SVG and EMF;SVG en EMF;SVG and JSON;SVG en JSON;SWF;SWF;EMF;EMF;SWF and EMF;SWF en EMF;JSON;JSON;BY OWNER;Door eigenaar;LAYERED BY OWNER;Laag door eigenaar;QUERY TEXT;Tekst van zoekopdracht;QUERY TABLE;Tabel van zoekopdracht;ASSET;Asset;RESTRICTION;Beperking;URL;URL;DYNAMIC;Dynamisch' WHERE TABLE_NAME = 'afm_dwgpub' AND FIELD_NAME = 'rule_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste'||CHR(13)||CHR(10)
||'d��num�ration' WHERE TABLE_NAME = 'afm_flds' AND FIELD_NAME = 'enum_list';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte'||CHR(13)||CHR(10)
||'d�Actif ?' WHERE TABLE_NAME = 'afm_flds' AND FIELD_NAME = 'is_atxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Langue 01)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Langue 02)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Langue 03)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Chinois - Simplifi�)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_ch';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Allemand)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_de';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Espagnol)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_es';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Fran�ais)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_fr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Italien)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_it';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Japonais)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_jp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Cor�en)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_ko';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(N�erlandais)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_nl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Norv�gien)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_no';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Chinois - Traditionnel)' WHERE TABLE_NAME = 'afm_flds_lang' AND FIELD_NAME = 'enum_list_zh';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'NONE;Aucun;CIRC_REF;Le champ a une r�f�rence circulaire;NO_DEFAULT;La valeur par d�faut ne se trouve pas dans Enum.;NO_DB_VAL_IN_ENUM;La valeur de la base de donn�es ne se trouve pas dans Enum.;NEW;Il s�agit d�un nouveau champ;TBL_IS_NEW;Il s�agit d�une nouvelle table;TBL_IN_PROJ_ONLY;La table est seulement dans le projet;PROJECT_ONLY;Le champ est seulement dans le projet;ALLOW_NULL;Vide�?;AFM_TYPE;Type d�AFM;ATTRIBUTES;Attributs;COMMENTS;Commentaires;DATA_TYPE;Type de Donn�es;DECIMALS;D�cimales;DEP_COLS;Colonnes D�pendantes;DFLT_VAL;Valeur Par D�faut;EDIT_GROUP;Groupe Edition;EDIT_MASK;Masque d�Edition;ENUM_LIST;Liste d�Enum�ration;FIELD_GROUPING;Groupement de Champs;IS_ATXT;Texte d�Actif;IS_TC_TRACEABLE;Tc_Traceable;MAX_VAL;Valeur Maximum;MIN_VAL;Valeur Minimum;ML_HEADING;En-t�te Multi-lignes;NUM_FORMAT;Format Num�rique;PRIMARY_KEY;Cl� Principale;REF_TABLE;Table de R�f�rence;REVIEW_GROUP;Groupe de Consultation;SIZE;Taille;SL_HEADING;En-t�te sur Une Lig' WHERE TABLE_NAME = 'afm_flds_trans' AND FIELD_NAME = 'change_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste'||CHR(13)||CHR(10)
||'d��num�ration' WHERE TABLE_NAME = 'afm_flds_trans' AND FIELD_NAME = 'enum_list';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte'||CHR(13)||CHR(10)
||'d�actif�?' WHERE TABLE_NAME = 'afm_flds_trans' AND FIELD_NAME = 'is_atxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pr�f�rences de'||CHR(13)||CHR(10)
||'l�Assistant de Maintenance' WHERE TABLE_NAME = 'afm_groups' AND FIELD_NAME = 'ww_preferences';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�indicateur' WHERE TABLE_NAME = 'afm_hmetric_trend_values' AND FIELD_NAME = 'metric_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur de'||CHR(13)||CHR(10)
||'l�indicateur' WHERE TABLE_NAME = 'afm_hmetric_trend_values' AND FIELD_NAME = 'metric_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_hotlist' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_hotlist' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_hotlist' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fichier de la'||CHR(13)||CHR(10)
||'Proc�dure d�Action' WHERE TABLE_NAME = 'afm_hotlist' AND FIELD_NAME = 'proc_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de la'||CHR(13)||CHR(10)
||'Proc�dure d�Action' WHERE TABLE_NAME = 'afm_hotlist' AND FIELD_NAME = 'proc_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Actif'||CHR(13)||CHR(10)
||'Par D�faut' WHERE TABLE_NAME = 'afm_layr' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;0 - Valeur de base de l�analyse du cycle de vie;1;1 - Valeur de l�analyse de base;2;2 - Somme des indicateurs d�analyse;3;3 - Ratio des indicateurs d�analyse', ML_HEADING_FR = 'Champs d�analyse - Ordre de calcul' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'analysis_calc_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champs d�analyse - Graphe d�affichage' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'analysis_display_chart';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champs d�analyse - Couleur d�affichage' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'analysis_display_color';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champs d�analyse - Ic�ne d�affichage' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'analysis_display_icon';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'INFO;Informations d�identification;BLDG;Valeurs et mesures de b�timent;CAP;Indicateurs d�investissement;EXP;Indicateurs de d�penses;FIN;Indicateurs de financement;LEAS;Indicateurs de location;OCCUP;Indicateurs d�occupation;SPAC;Indicateurs d�espace;TCO;Indicateurs de co�t total', ML_HEADING_FR = 'Champs d�analyse - Cat�gorie' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'analysis_field_cat';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champs d�analyse - Champ de r�sultat' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'analysis_result_field';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Legacy Metrics;Indicateurs h�rit�s;Tracking Metrics;Indicateurs de suivi;Analysis Metrics;Indicateurs d�analyse' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'calc_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�indicateur' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�indicateur' WHERE TABLE_NAME = 'afm_metric_definitions' AND FIELD_NAME = 'metric_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�indicateur' WHERE TABLE_NAME = 'afm_metric_notify' AND FIELD_NAME = 'metric_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_metric_scards' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�indicateur' WHERE TABLE_NAME = 'afm_metric_trend_values' AND FIELD_NAME = 'metric_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur de'||CHR(13)||CHR(10)
||'l�indicateur' WHERE TABLE_NAME = 'afm_metric_trend_values' AND FIELD_NAME = 'metric_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�inscription'||CHR(13)||CHR(10)
||'de l�appareil' WHERE TABLE_NAME = 'afm_mob_dev_reg_log' AND FIELD_NAME = 'date_registered';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de d�sinscription'||CHR(13)||CHR(10)
||'de l�appareil' WHERE TABLE_NAME = 'afm_mob_dev_reg_log' AND FIELD_NAME = 'date_unregistered';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure d�inscription'||CHR(13)||CHR(10)
||'de l�appareil' WHERE TABLE_NAME = 'afm_mob_dev_reg_log' AND FIELD_NAME = 'time_registered';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure de d�sinscription'||CHR(13)||CHR(10)
||'de l�appareil' WHERE TABLE_NAME = 'afm_mob_dev_reg_log' AND FIELD_NAME = 'time_unregistered';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�Utilisateur' WHERE TABLE_NAME = 'afm_mob_dev_reg_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_mobile_apps' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_mobile_menu' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'afm_mobile_sync_history' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Horodatage de l��v�nement' WHERE TABLE_NAME = 'afm_mobile_table_trans' AND FIELD_NAME = 'event_timestamp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_mods' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_mods' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_mods' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl�'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'afm_notifications_log' AND FIELD_NAME = 'activity_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Objet'||CHR(13)||CHR(10)
||'de l�email' WHERE TABLE_NAME = 'afm_notifications_log' AND FIELD_NAME = 'email_subject';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Message'||CHR(13)||CHR(10)
||'d��tat' WHERE TABLE_NAME = 'afm_notifications_log' AND FIELD_NAME = 'status_message';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��' WHERE TABLE_NAME = 'afm_processes' AND FIELD_NAME = 'dashboard_layout';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_processes' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_products' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_psubtasks' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�utilisateur'||CHR(13)||CHR(10)
||'de la Hotliste' WHERE TABLE_NAME = 'afm_psubtasks' AND FIELD_NAME = 'hot_user_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'LABEL;Intitul�;WEB URL;URL Web;GRID;Grille Smart Client;GRIDCAD;Grille CAD Smart Client;OVERLAY ACTION;Action d�Overlay;VIEW;Vue C/S;FIXED-FORMAT VIEW;Vue en Format Fixe C/S;DRAWING VIEW;Vue de Dessin C/S;BASICSCRIPT ACTION;Action BasicScript C/S;ACTIVEX ACTION;Action ActiveX C/S;WINDOWS CONSOLE URL;URL Console Windows C/S;SHELLEXECUTE;ShellExecute C/S;WINDOWS BROWSER URL;URL Navigateur Windows C/S' WHERE TABLE_NAME = 'afm_psubtasks' AND FIELD_NAME = 'task_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_ptasks' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�utilisateur'||CHR(13)||CHR(10)
||'de la Hotliste' WHERE TABLE_NAME = 'afm_ptasks' AND FIELD_NAME = 'hot_user_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'LABEL;Intitul�;WEB URL;URL Web;GRID;Grille Smart Client;GRIDCAD;Grille CAD Smart Client;VIEWANLYS;Analyse de vue Smart Client;OVERLAY ACTION;Action d�overlay;VIEW;Vue C/S;FIXED-FORMAT VIEW;Vue en format fixe C/S;DRAWING VIEW;Vue de dessin C/S;BASICSCRIPT ACTION;Action BasicScript C/S;ACTIVEX ACTION;Action ActiveX C/S;WINDOWS CONSOLE URL;URL console Windows C/S;SHELLEXECUTE;ShellExecute C/S;WINDOWS BROWSER URL;URL navigateur Windows C/S' WHERE TABLE_NAME = 'afm_ptasks' AND FIELD_NAME = 'task_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'afm_redlines' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Flash-based Redline;Annotation Flash;HTML5-based Map or Drawing Image;Carte ou dessin HTML5;HTML5-based Floor Plan;Plan d��tage HTML5;' WHERE TABLE_NAME = 'afm_redlines' AND FIELD_NAME = 'origin';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�'||CHR(13)||CHR(10)
||'annotation' WHERE TABLE_NAME = 'afm_redlines' AND FIELD_NAME = 'redline_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'El�ments'||CHR(13)||CHR(10)
||'d�annotation' WHERE TABLE_NAME = 'afm_redlines' AND FIELD_NAME = 'redlines';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pr�f�rences de'||CHR(13)||CHR(10)
||'l�Assistant de Maintenance' WHERE TABLE_NAME = 'afm_roles' AND FIELD_NAME = 'ww_preferences';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Premier jour'||CHR(13)||CHR(10)
||'de l�exercice fiscal' WHERE TABLE_NAME = 'afm_scmpref' AND FIELD_NAME = 'fiscalyear_startday';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Premier mois'||CHR(13)||CHR(10)
||'de l�exercice fiscal' WHERE TABLE_NAME = 'afm_scmpref' AND FIELD_NAME = 'fiscalyear_startmonth';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nombre de p�riodes'||CHR(13)||CHR(10)
||'de pr�visions d�espace' WHERE TABLE_NAME = 'afm_scmpref' AND FIELD_NAME = 'num_space_fcast_per';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe'||CHR(13)||CHR(10)
||'d�Activit�s' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'act_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fichier de la'||CHR(13)||CHR(10)
||'Proc�dure d�Action' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'proc_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de la'||CHR(13)||CHR(10)
||'Proc�dure d�Action' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'proc_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Aucun;1;S�lect Dates;2;S�lect N� BT;3;S�lect Date BT;4;S�lect Gamme;5;S�lect DI;6;Cr�er DI;7;Cr�er BT;8;G�n MP EQ;9;G�n MP EN;10;M � J BT;11;G�rer Trans. Stock.;12;M � J Stock.;13;R�sum� Travx;14;R�sum� Md�O;15;Plusieurs' WHERE TABLE_NAME = 'afm_subtasks' AND FIELD_NAME = 'test_has_dialog';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe'||CHR(13)||CHR(10)
||'d�Activit�s' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'act_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sous-dossier'||CHR(13)||CHR(10)
||'de l�Aide' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Sujet'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'help_topic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fichier de la'||CHR(13)||CHR(10)
||'Proc�dure d�Action' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'proc_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de la'||CHR(13)||CHR(10)
||'Proc�dure d�Action' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'proc_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Aucun;1;S�lect Dates;2;S�lect N� BT;3;S�lect Date BT;4;S�lect Gamme;5;S�lect DI;6;Cr�er DI;7;Cr�er BT;8;G�n MP EQ;9;G�n MP EN;10;M � J BT;11;G�rer Trans. Stock.;12;M � J Stock.;13;R�sum� Travx;14;R�sum� Md�O;15;Plusieurs' WHERE TABLE_NAME = 'afm_tasks' AND FIELD_NAME = 'test_has_dialog';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'PROJECT SECURITY;S�curit� du Projet;PROJECT APPLICATION DATA;Donn�es d�Application du Projet;PROJECT DATA;Donn�es du Projet;DATA DICTIONARY;Dictionnaire de Donn�es;APPLICATION DICTIONARY;Dictionnaire d�Applications;PROCESS NAVIGATOR;Navigateur de Processus' WHERE TABLE_NAME = 'afm_tbls' AND FIELD_NAME = 'table_type';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;s/o;WA;Zone de travail;H;Cabl�ge horizontal;TC;Armoire t�l�com;ER;Pi�ce �quipement;EF;Dispositif d�acc�s;B;Cabl�ge f�d�rateur' WHERE TABLE_NAME = 'afm_tclevel' AND FIELD_NAME = 'eia_level';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Unit�s' WHERE TABLE_NAME = 'afm_titlesheet' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Enregistrements dans'||CHR(13)||CHR(10)
||'le fichier d�extraction source' WHERE TABLE_NAME = 'afm_transfer_set' AND FIELD_NAME = 'nrecords_source';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'NONE;Aucun;PENDING;En attente;IN PROCESS;En Cours;EXPORTED;Export�;COMPARED;Compar�;IMPORTED;Import�;NO EXTRACT FILE;Pas de Fichier d�Extraction;NO PROJECT TABLE;Pas de Table de Projet;NOT PROCESSED;Non Trait�;UPDATED;Mis � jour' WHERE TABLE_NAME = 'afm_transfer_set' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'afm_userprocs' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Utilisateur-Unit�s'||CHR(13)||CHR(10)
||'de Mesure d�Affichage' WHERE TABLE_NAME = 'afm_users' AND FIELD_NAME = 'display_units';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'ab-dashboard.axvw;Tableau de bord;navigator-details.axvw;Navigateur de processus;accessible-details.axvw;Navigateur d�accessibilit�;page-navigator.html;Page d�accueil' WHERE TABLE_NAME = 'afm_users' AND FIELD_NAME = 'home_page';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nombre d��checs'||CHR(13)||CHR(10)
||'de nouveaux essais de connexion' WHERE TABLE_NAME = 'afm_users' AND FIELD_NAME = 'num_retries';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'afm_users' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl�'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'activity_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� de D�finition'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'activitydef_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'category';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l��v�nement' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'event_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'event_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fichier'||CHR(13)||CHR(10)
||'d�Aide' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'help_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'afm_wf_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'afm_wf_rules' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'afm_wf_steps' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;Sans Objet;CREATED;CR��;REQUESTED;DEMAND�;APPROVED;APPROUV�;REJECTED;REJET�;TRIAL;SIMULATION;BUDGETED;BUDG�TIS�;PLANNED;PLANIFI�;SCHEDULED;PROGRAMM�;CANCELLED;ANNUL�;IN PROGRESS;EN COURS;IN PROCESS-H;EN COURS - EN ATTENTE;STOPPED;ARR�T�;COMPLETED;TERMIN�;COMPLETED-V;TERMIN� ET V�RIFI�;CLOSED;FERM�;R;Demand�;Rev;Examin� mais En Attente;Rej;Rejet�;A;Approuv�;AA;Affect� au Bon de Travx;I;Emis et En Cours;HP;En Attente de Pi�ces D�tach�es;HA;En Attente d�Acc�s;HL;En Attente de Main d��uvre;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�' WHERE TABLE_NAME = 'afm_wf_steps' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'afm_wf_steps' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Id'||CHR(13)||CHR(10)
||'de l�objet du message' WHERE TABLE_NAME = 'afm_wf_steps' AND FIELD_NAME = 'subject_message_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�all�e', SL_HEADING_FR = 'Nom d�all�e' WHERE TABLE_NAME = 'aisle' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant d�actif' WHERE TABLE_NAME = 'asset_trans' AND FIELD_NAME = 'asset_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'Location;��;Ownership;���;Status;��;Value;�;Owner Custodian;�����', ENUM_LIST_DE = 'Location;Standort;Ownership;Besitzer;Status;Status;Value;Wert;Owner Custodian;Sachverwalter', ENUM_LIST_ES = 'Location;Ubicaci�n;Ownership;Propiedad;Status;Estado;Value;Valor;Owner Custodian;Propietario custodio', ENUM_LIST_FR = 'Location;Emplacement;Ownership;Propri�t�;Status;Statut;Value;Valeur;Owner Custodian;Gestionnaire principal', ENUM_LIST_IT = 'Location;Posizione;Ownership;Propriet�;Status;Stato;Value;Valore;Owner Custodian;Delegato responsabile', ENUM_LIST_NL = 'Location;Locatie;Ownership;Eigendom;Status;Status;Value;Waarde;Owner Custodian;Beheerder-eigenaar' WHERE TABLE_NAME = 'asset_trans' AND FIELD_NAME = 'trans_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Intervalle'||CHR(13)||CHR(10)
||'d��chantillonnage (secondes)' WHERE TABLE_NAME = 'bas_data_point' AND FIELD_NAME = 'sampling_interval';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d��tendue'||CHR(13)||CHR(10)
||'de mesure' WHERE TABLE_NAME = 'bas_measurement_scope' AND FIELD_NAME = 'measurement_scope_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Approbation'||CHR(13)||CHR(10)
||'pour Paiement' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d��ch�ance'||CHR(13)||CHR(10)
||'paiement' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'date_due';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�exportation'||CHR(13)||CHR(10)
||'vers ERP' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'date_exported';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d��mission'||CHR(13)||CHR(10)
||'de facture' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'date_issued';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Variance des D�penses par rapp. � la'||CHR(13)||CHR(10)
||'M�me P�riode l�Ann�e Derni�re (%)' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'expense_variance_year';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Variance de Revenu par rapp. � la'||CHR(13)||CHR(10)
||'M�me P�riode l�Ann�e Derni�re (%)' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'income_variance_year';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_ES = 'Created;Creado;Pending Approval;Pendiente de aprobaci�n;Pending Review;Pendiente de revisi�n;Approved;Aprobado;Rejected;Rechazado;Imported;Importado', ENUM_LIST_FR = 'Created;Cr��;Pending Approval;En attente d�approbation;Pending Review;Examen en attente;Approved;Approuv�;Rejected;Rejet�;Imported;Import�' WHERE TABLE_NAME = 'bill' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Approbation'||CHR(13)||CHR(10)
||'pour Paiement' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d��ch�ance'||CHR(13)||CHR(10)
||'paiement' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'date_due';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�exportation'||CHR(13)||CHR(10)
||'vers ERP' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'date_exported';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d��mission'||CHR(13)||CHR(10)
||'de facture' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'date_issued';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Variance des D�penses par rapp. � la'||CHR(13)||CHR(10)
||'M�me P�riode l�Ann�e Derni�re (%)' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'expense_variance_year';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Variance de Revenu par rapp. � la'||CHR(13)||CHR(10)
||'M�me P�riode l�Ann�e Derni�re (%)' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'income_variance_year';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_ES = 'Created;Creado;Pending Approval;Pendiente de aprobaci�n;Pending Review;Pendiente de revisi�n;Approved;Aprobado;Rejected;Rechazado;Imported;Importado', ENUM_LIST_FR = 'Created;Cr��;Pending Approval;En attente d�approbation;Pending Review;Examen en attente;Approved;Approuv�;Rejected;Rejet�;Imported;Import�' WHERE TABLE_NAME = 'bill_archive' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Chemin d�acc�s et nom '||CHR(13)||CHR(10)
||'de fichier par d�faut' WHERE TABLE_NAME = 'bill_connector' AND FIELD_NAME = 'default_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl�'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'bill_type' AND FIELD_NAME = 'activity_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur de conversion vers l�unit� commune' WHERE TABLE_NAME = 'bill_unit' AND FIELD_NAME = 'conversion_factor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Estimer automatiquement'||CHR(13)||CHR(10)
||'les points d��quilibre�?' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'auto_est_balance_points';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires sur l��limination' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'comment_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Point d��quilibre'||CHR(13)||CHR(10)
||'de refroidissement' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'cooling_balance_point';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Punto di equilibrio'||CHR(13)||CHR(10)
||'raffrescamento (definito dall�utente)', ML_HEADING_FR = 'Point d��quilibre'||CHR(13)||CHR(10)
||'de refroidissement (personnalis�)' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'cooling_balance_point_manual';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nombre'||CHR(13)||CHR(10)
||'d��tages' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'count_fl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d��limination' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'date_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��limination' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'disposal_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Point d��quilibre'||CHR(13)||CHR(10)
||'de chauffage' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'heating_balance_point';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Punto di equilibrio'||CHR(13)||CHR(10)
||' riscaldamento (definito dall�utente)', ML_HEADING_FR = 'Point d��quilibre'||CHR(13)||CHR(10)
||'de chauffage (personnalis�)' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'heating_balance_point_manual';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'En attente d�action' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'pending_action';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Taux (U/L)'||CHR(13)||CHR(10)
||'d�efficacit�' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'ratio_ur';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'PIPELINE;En cours d�acquisition;UNDER CONTRACT;Sous contrat;ESCROWED;D�p�t l�gal;IN SERVICE;En service;OWNED;Poss�d�;OWNED AND LEASED;Poss�d� et lou�;LEASED;Lou�;SUB-LEASED;Sous-lou�;SUB LET;Sous-lou�;FOR SALE;A vendre;LEASED (EXPIRED);Lou� (expir�);OUT OF SERVICE;Hors service;ABANDONED;Abandonn�;DONATED;Donn�;DISPOSED;Elimin�;SOLD;Vendu;N/A;S/O;UNKNOWN;Inconnu' WHERE TABLE_NAME = 'bl' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� manutenzione '||CHR(13)||CHR(10)
||'preventiva?' WHERE TABLE_NAME = 'blbu' AND FIELD_NAME = 'is_pm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Kurzzeichen Unternehmenseinheit' WHERE TABLE_NAME = 'bu' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Unit�', ML_HEADING_DE = 'Name der '||CHR(13)||CHR(10)
||'Unternehmenseinheit' WHERE TABLE_NAME = 'bu' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||'d�Article' WHERE TABLE_NAME = 'budget_item' AND FIELD_NAME = 'budget_item_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'ca' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'ca' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�armoire', SL_HEADING_FR = 'Nom d�armoire' WHERE TABLE_NAME = 'cabinet' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��l�ment'||CHR(13)||CHR(10)
||'r�seau' WHERE TABLE_NAME = 'card' AND FIELD_NAME = 'netdev_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Multiplexing?' WHERE TABLE_NAME = 'cardstd' AND FIELD_NAME = 'is_multiplexing';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��l�ment'||CHR(13)||CHR(10)
||'r�seau' WHERE TABLE_NAME = 'cardstd' AND FIELD_NAME = 'netdev_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Multiplexing?' WHERE TABLE_NAME = 'castd' AND FIELD_NAME = 'is_multiplexing';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Source'||CHR(13)||CHR(10)
||'d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'accredit_source_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'accredit_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro de Certificat'||CHR(13)||CHR(10)
||'d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'cert_num';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'date_accredited';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Expiration de l�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'date_expire';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro de Licence'||CHR(13)||CHR(10)
||'d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_person' AND FIELD_NAME = 'lic_num';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de Source'||CHR(13)||CHR(10)
||'d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_source' AND FIELD_NAME = 'accredit_source_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de Type'||CHR(13)||CHR(10)
||'d�Accr�ditation' WHERE TABLE_NAME = 'cb_accredit_type' AND FIELD_NAME = 'accredit_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'de l��valuation' WHERE TABLE_NAME = 'cb_hazard_rating' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'de l��valuation' WHERE TABLE_NAME = 'cb_hazard_rating' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'de l��tat' WHERE TABLE_NAME = 'cb_hcm_cond' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l��tat' WHERE TABLE_NAME = 'cb_hcm_cond' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'cb_hcm_loc_typ_chk' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'cb_hcm_places' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de composition'||CHR(13)||CHR(10)
||'de l��chantillon' WHERE TABLE_NAME = 'cb_sample_comp' AND FIELD_NAME = 'sample_comp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�Unit�s' WHERE TABLE_NAME = 'cb_sample_result' AND FIELD_NAME = 'cb_units_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Composition'||CHR(13)||CHR(10)
||'d��chantillon' WHERE TABLE_NAME = 'cb_sample_result' AND FIELD_NAME = 'sample_comp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d��valuation' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data dell�analisi', ML_HEADING_FR = 'Date d�Analyse' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'date_analysis';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur'||CHR(13)||CHR(10)
||'d�Entit�' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'d��chantillon' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'sample_desc';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Localisation'||CHR(13)||CHR(10)
||'d��chantillon' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'sample_loc';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de localisation '||CHR(13)||CHR(10)
||'d��chantillon' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'sample_loc_code';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��chantillon' WHERE TABLE_NAME = 'cb_samples' AND FIELD_NAME = 'sample_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'ccost_sum' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'cdport' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Possibilit� de modifier'||CHR(13)||CHR(10)
||'la demande d�intervention�?' WHERE TABLE_NAME = 'cf' AND FIELD_NAME = 'cf_change_wr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Expiration'||CHR(13)||CHR(10)
||'du Contrat' WHERE TABLE_NAME = 'cf' AND FIELD_NAME = 'date_contract_exp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'cf' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe d�intervention' WHERE TABLE_NAME = 'cf_work_team' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Actif' WHERE TABLE_NAME = 'checkin' AND FIELD_NAME = 'asset_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ordine d�acquisto', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'checkin' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Riga ordine'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'Ligne du'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'checkin' AND FIELD_NAME = 'po_line_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'city' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'city' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'combext' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Sito Web'||CHR(13)||CHR(10)
||'dell�azienda', ML_HEADING_FR = 'Site Web '||CHR(13)||CHR(10)
||'de l�entreprise' WHERE TABLE_NAME = 'company' AND FIELD_NAME = 'website';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'compliance_locations' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'compliance_locations' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'compliance_locations' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'compliance_locations' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�indice' WHERE TABLE_NAME = 'cost_index' AND FIELD_NAME = 'cost_index_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de l�indice' WHERE TABLE_NAME = 'cost_index' AND FIELD_NAME = 'cost_index_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�indice' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'cost_index_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID de transaction d�indices des co�ts' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'cost_index_trans_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date - Nouvelle valeur d�indexation' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'date_index_value_new';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = '% de modif.'||CHR(13)||CHR(10)
||'de l�indice (%)' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'index_pct_change';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur d�indice initiale' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'index_value_initial';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nouvelle valeur d�indice' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'index_value_new';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fr�quence d�indexation' WHERE TABLE_NAME = 'cost_index_trans' AND FIELD_NAME = 'indexing_frequency';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�indice' WHERE TABLE_NAME = 'cost_index_values' AND FIELD_NAME = 'cost_index_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date - Valeur d�indexation' WHERE TABLE_NAME = 'cost_index_values' AND FIELD_NAME = 'date_index_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur d�indice' WHERE TABLE_NAME = 'cost_index_values' AND FIELD_NAME = 'index_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - D�penses (Budget)' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'amount_expense_base_budget';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - D�penses (Paiement)' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'amount_expense_base_payment';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - Revenus (Budget)' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'amount_income_base_budget';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - Revenus (Paiement)' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'amount_income_base_payment';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Imputation' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'chrgbck_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Actif' WHERE TABLE_NAME = 'cost_tran' AND FIELD_NAME = 'pa_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - D�penses (Budget)' WHERE TABLE_NAME = 'cost_tran_recur' AND FIELD_NAME = 'amount_expense_base_budget';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - D�penses (Paiement)' WHERE TABLE_NAME = 'cost_tran_recur' AND FIELD_NAME = 'amount_expense_base_payment';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - Revenus (Budget)' WHERE TABLE_NAME = 'cost_tran_recur' AND FIELD_NAME = 'amount_income_base_budget';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - Revenus (Paiement)' WHERE TABLE_NAME = 'cost_tran_recur' AND FIELD_NAME = 'amount_income_base_payment';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Actif' WHERE TABLE_NAME = 'cost_tran_recur' AND FIELD_NAME = 'pa_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Zeitraum' WHERE TABLE_NAME = 'cost_tran_recur' AND FIELD_NAME = 'period';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'cost_tran_sched' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - D�penses (Budget)' WHERE TABLE_NAME = 'cost_tran_sched' AND FIELD_NAME = 'amount_expense_base_budget';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - D�penses (Paiement)' WHERE TABLE_NAME = 'cost_tran_sched' AND FIELD_NAME = 'amount_expense_base_payment';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - Revenus (Budget)' WHERE TABLE_NAME = 'cost_tran_sched' AND FIELD_NAME = 'amount_income_base_budget';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Montant -'||CHR(13)||CHR(10)
||'Base d�imposition TVA - Revenus (Paiement)' WHERE TABLE_NAME = 'cost_tran_sched' AND FIELD_NAME = 'amount_income_base_payment';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Actif' WHERE TABLE_NAME = 'cost_tran_sched' AND FIELD_NAME = 'pa_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'county' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'county' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'cp' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'ctry' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'ctry' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'UNKNOWN;INCONNU;BANK/FINANCIAL INSTITUTION;BANQUE/ETABLISSEMENT FINANCIER;COURTHOUSE;TRIBUNAL;DATA CENTER;CENTRE DE DONNEES;HOUSE OF WORSHIP;LIEU DE CULTE;HOSPICE;HOSPICE;HOSPITAL;HOPITAL;HOTEL;HOTEL;K-12 SCHOOL;ETABLISSEMENT SCOLAIRE;MANUFACTURING;FABRICATION;MEDICAL OFFICE;CABINET MEDICAL;MIXED USE;USAGE MIXTE;MULTIFAMILY HOUSING;LOGEMENT MULTIFAMILIAL;OFFICE;BUREAU;OTHER;AUTRE;RESIDENCE HALL/DORMITORY;LIEU DE RESIDENCE/DORTOIR;RETAIL;VENTE AU DETAIL;SALES OFFICE;BUREAU DE VENTE;SENIOR CARE FACILITY;LIEU DE SOIN POUR PERSONNES AGEES;SPORT/RECREATION;SPORTS/LOISIRS;STORAGE;STOCKAGE;SUPERMARKET/GROCERY STORE;SUPERMARCHE/EPICERIE;WAREHOUSE;ENTREPOT;WAREHOUSE (REFRIGERATED);ENTREPOT (REFRIGERE);WASTEWATER TREATMENT PLANT;STATION D�EPURATION DES EAUX USEES;N/A;S/O' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'bl_use1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�EPI'||CHR(13)||CHR(10)
||'associ�' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'ppe_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'docs_assigned' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'UNKNOWN;INCONNU;BANK/FINANCIAL INSTITUTION;BANQUE/ETABLISSEMENT FINANCIER;COURTHOUSE;TRIBUNAL;DATA CENTER;CENTRE DE DONNEES;HOUSE OF WORSHIP;LIEU DE CULTE;HOSPICE;HOSPICE;HOSPITAL;HOPITAL;HOTEL;HOTEL;K-12 SCHOOL;ETABLISSEMENT SCOLAIRE;MANUFACTURING;FABRICATION;MEDICAL OFFICE;CABINET MEDICAL;MIXED USE;USAGE MIXTE;MULTIFAMILY HOUSING;LOGEMENT MULTIFAMILIAL;OFFICE;BUREAU;OTHER;AUTRE;RESIDENCE HALL/DORMITORY;LIEU DE RESIDENCE/DORTOIR;RETAIL;VENTE AU DETAIL;SALES OFFICE;BUREAU DE VENTE;SENIOR CARE FACILITY;LIEU DE SOIN POUR PERSONNES AGEES;SPORT/RECREATION;SPORTS/LOISIRS;STORAGE;STOCKAGE;SUPERMARKET/GROCERY STORE;SUPERMARCHE/EPICERIE;WAREHOUSE;ENTREPOT;WAREHOUSE (REFRIGERATED);ENTREPOT (REFRIGERE);WASTEWATER TREATMENT PLANT;STATION D�EPURATION DES EAUX USEES;N/A;S/O' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'bl_use1';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'doc_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'mob_incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'docs_assigned_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� manutenzione '||CHR(13)||CHR(10)
||'preventiva?' WHERE TABLE_NAME = 'dpbu' AND FIELD_NAME = 'is_pm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'dr' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Unternehmenseinheit' WHERE TABLE_NAME = 'dv' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ehs_em_ppe_types' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d��tage'||CHR(13)||CHR(10)
||'pour la livraison' WHERE TABLE_NAME = 'ehs_em_ppe_types' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Associ� '||CHR(13)||CHR(10)
||'Code d�accident' WHERE TABLE_NAME = 'ehs_em_ppe_types' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de type'||CHR(13)||CHR(10)
||'d�EPI' WHERE TABLE_NAME = 'ehs_em_ppe_types' AND FIELD_NAME = 'ppe_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description du type'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incident_types' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incident_types' AND FIELD_NAME = 'incident_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�enregistrement', SL_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�enregistrement' WHERE TABLE_NAME = 'ehs_incident_witness' AND FIELD_NAME = 'date_recorded';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ehs_incident_witness' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incident_witness' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID du'||CHR(13)||CHR(10)
||'t�moin de l�accident', SL_HEADING_FR = 'ID du t�moin de l�accident' WHERE TABLE_NAME = 'ehs_incident_witness' AND FIELD_NAME = 'incident_witness_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ehs_incident_witness_sync' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incident_witness_sync' AND FIELD_NAME = 'mob_incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'ehs_incident_witness_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'dell�incidente', ML_HEADING_FR = 'Date de'||CHR(13)||CHR(10)
||'l�accident', SL_HEADING_FR = 'Date de l�accident', SL_HEADING_IT = 'Data dell�incidente' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'date_incident';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�accident' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�accident', SL_HEADING_FR = 'Type d�accident' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'incident_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�exigence'||CHR(13)||CHR(10)
||'� long terme' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'lt_reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�infrastructure m�dicale', SL_HEADING_FR = 'Nom de l�infrastructure m�dicale' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'medical_facility';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Adresse de'||CHR(13)||CHR(10)
||'l�infrastructure m�dicale', SL_HEADING_FR = 'Adresse de l�infrastructure m�dicale' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'medical_facility_address';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�accident regroup�' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'parent_incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�exigence'||CHR(13)||CHR(10)
||'� court terme', SL_HEADING_FR = 'Code d�exigence � court terme' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'st_reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora'||CHR(13)||CHR(10)
||'dell�incidente', ML_HEADING_FR = 'Heure de'||CHR(13)||CHR(10)
||'l�accident' WHERE TABLE_NAME = 'ehs_incidents' AND FIELD_NAME = 'time_incident';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'dell�incidente', ML_HEADING_FR = 'Date de'||CHR(13)||CHR(10)
||'l�accident' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'date_incident';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�accident' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'incident_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'mob_incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�accident regroup�' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'parent_incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora'||CHR(13)||CHR(10)
||'dell�incidente', ML_HEADING_FR = 'Heure de'||CHR(13)||CHR(10)
||'l�accident' WHERE TABLE_NAME = 'ehs_incidents_sync' AND FIELD_NAME = 'time_incident';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ehs_medical_mon_results' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�accident'||CHR(13)||CHR(10)
||'associ�' WHERE TABLE_NAME = 'ehs_medical_mon_results' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Routine;Routine;Incident Related;Li� � l�accident;Equipment Related;Li� � l��quipement;PPE Related;Li� � l�EPI' WHERE TABLE_NAME = 'ehs_medical_mon_results' AND FIELD_NAME = 'monitoring_type';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Routine;Routine;Incident Related;Li� � l�accident;Equipment Related;Li� � l��quipement;PPE Related;Li� � l�EPI' WHERE TABLE_NAME = 'ehs_medical_monitoring' AND FIELD_NAME = 'monitoring_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'ehs_medical_monitoring' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'de l�EPI' WHERE TABLE_NAME = 'ehs_ppe_types' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'ehs_ppe_types' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de type'||CHR(13)||CHR(10)
||'d�EPI' WHERE TABLE_NAME = 'ehs_ppe_types' AND FIELD_NAME = 'ppe_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ehs_restrictions' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�accident'||CHR(13)||CHR(10)
||'associ�' WHERE TABLE_NAME = 'ehs_restrictions' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Away;Non travaill�s;Remained-Restricted;Travaill�s�: transfert ou restriction d�intervention;Remained-Other;Travaill�s�: autres cas enregistrables' WHERE TABLE_NAME = 'ehs_restrictions' AND FIELD_NAME = 'restriction_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard d��quipement'||CHR(13)||CHR(10)
||'associ�' WHERE TABLE_NAME = 'ehs_training' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = '� necessario'||CHR(13)||CHR(10)
||'l�aggiornamento?' WHERE TABLE_NAME = 'ehs_training' AND FIELD_NAME = 'needs_refresh';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de type'||CHR(13)||CHR(10)
||'associ� d�EPI' WHERE TABLE_NAME = 'ehs_training' AND FIELD_NAME = 'ppe_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'ehs_training' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ehs_training_results' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�accident'||CHR(13)||CHR(10)
||'associ�' WHERE TABLE_NAME = 'ehs_training_results' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de type'||CHR(13)||CHR(10)
||'d�EPI' WHERE TABLE_NAME = 'ehs_work_cat_ppe_types' AND FIELD_NAME = 'ppe_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�'||CHR(13)||CHR(10)
||'embauche' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'date_hired';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||'d�Employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'em_number';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Photo de'||CHR(13)||CHR(10)
||'l�employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'em_photo';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard'||CHR(13)||CHR(10)
||'d�Employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'em_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�'||CHR(13)||CHR(10)
||'employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'em_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Personne � Contacter'||CHR(13)||CHR(10)
||'En Cas d�Urgence' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'emergency_contact';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'T�l�phone du Contact'||CHR(13)||CHR(10)
||'En Cas d�Urgence' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'emergency_phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Relation'||CHR(13)||CHR(10)
||'avec l�Employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'emergency_relation';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Graphique'||CHR(13)||CHR(10)
||'de l�Employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'image_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||' l�employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�Utilisateur'||CHR(13)||CHR(10)
||'R�seau' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'net_user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut'||CHR(13)||CHR(10)
||'de l�Employ�' WHERE TABLE_NAME = 'em' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'em_compinvtrial' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage dans'||CHR(13)||CHR(10)
||'simulation' WHERE TABLE_NAME = 'em_compinvtrial' AND FIELD_NAME = 'trial_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'em_compinvtrial' AND FIELD_NAME = 'trial_rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||' l�employ�', SL_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||' l�employ�' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||' d�employ�', SL_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||' d�employ�' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'em_number';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Photo de'||CHR(13)||CHR(10)
||'l�employ�', SL_HEADING_FR = 'Photo de'||CHR(13)||CHR(10)
||'l�employ�' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'em_photo';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard'||CHR(13)||CHR(10)
||' d�employ�', SL_HEADING_FR = 'Standard'||CHR(13)||CHR(10)
||' d�employ�' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'em_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', SL_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'T�l�phone'||CHR(13)||CHR(10)
||' de l�employ�', SL_HEADING_FR = 'T�l�phone'||CHR(13)||CHR(10)
||' de l�employ�' WHERE TABLE_NAME = 'em_sync' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'em_trial' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'em_trial' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage dans'||CHR(13)||CHR(10)
||'simulation' WHERE TABLE_NAME = 'em_trial' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'em_trial' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard'||CHR(13)||CHR(10)
||'d�Employ�' WHERE TABLE_NAME = 'emstd' AND FIELD_NAME = 'em_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Kurzzeichen Unternehmenseinheit' WHERE TABLE_NAME = 'emsum' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Standard'||CHR(13)||CHR(10)
||'d�Employ�' WHERE TABLE_NAME = 'emsum' AND FIELD_NAME = 'em_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Temperatura'||CHR(13)||CHR(10)
||'esterna dell�aria periodica', ML_HEADING_FR = 'Temp�rature de l�Air'||CHR(13)||CHR(10)
||'� l�Ext�rieur pour la P�riode' WHERE TABLE_NAME = 'energy_bl_svc_period' AND FIELD_NAME = 'period_oat';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant d�actif' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'asset_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires sur l��limination' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'comment_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Etat de'||CHR(13)||CHR(10)
||'l��quipement' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'condition';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Prix'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'cost_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de cr�ation de l�enregistrement' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d��limination' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'date_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Installation' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'date_purchased';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�expiration de garantie' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'date_warranty_exp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��limination' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'disposal_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Proc�dures de'||CHR(13)||CHR(10)
||'fonctionnement d�urgence' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'doc_eop';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Dov�� il manuale?' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'loc_maint_manl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'N. d�ordine', ML_HEADING_FR = 'Num�ro du'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'num_po';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Syst�me'||CHR(13)||CHR(10)
||'d�Exploitation' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'os_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'En attente d�action' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'pending_action';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Riga ordine'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'N� Ligne du'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'po_line_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Police'||CHR(13)||CHR(10)
||'d�Assurance' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'policy_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nbre d�heures'||CHR(13)||CHR(10)
||'fonctionnement'||CHR(13)||CHR(10)
||'normal/jour' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'qty_hrs_run_day';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Aantal PO-schema�s' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'qty_pms';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_IT = 'NONE;Nessuno stato;NONE-REVIEW;Nessuno stato in esame;FIT-ONLINE;Adatto all�uso;FIT-OFFLINE;Adatto all�uso - Non in linea;UNFIT-TEMP;Inadatto all�uso - Recuperabile;UNFIT-PERM;Inadatto all�uso - Distrutto' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'recovery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires d�infos source' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'source_feed_comments';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� ��������' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'source_time_update';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'in;���;out;����;rep;���;stor;���;salv;���;sold;���;miss;��;disp;���;don;���;sto;���;todi;����', ENUM_LIST_DE = 'in;In Betrieb;out;Au�er Betrieb;rep;Zur Reparatur;stor;Auf Lager;salv;Wiederverwertet;sold;Verkauft;miss;Fehlend;disp;Entsorgt;don;Gespendet;sto;Gestohlen;todi;Zu entsorgen', ENUM_LIST_ES = 'in;En servicio;out;Fuera de servicio;rep;En reparaci�n;stor;Almacenado;salv;Baja por amortizaci�n;sold;Vendido;miss;Falta;disp;Eliminado;don;Donado;sto;Robado;todi;Para ser eliminado', ENUM_LIST_FR = 'in;En service;out;Hors service;rep;En r�paration;stor;En stockage;salv;Au rebut;sold;Vendu;miss;Manquant;disp;Elimin�;don;Donn�;sto;Vol�;todi;A �liminer', ENUM_LIST_IT = 'in;In servizio;out;Fuori servizio;rep;In riparazione;stor;In deposito;salv;Recuperato;sold;Venduto;miss;Mancante;disp;Smaltito;don;Donato;sto;Rubato;todi;Da smaltire', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l��quipement', ENUM_LIST_NL = 'in;In bedrijf;out;Buiten bedrijf;rep;Wordt hersteld;stor;In opslag;salv;Gereviseerd;sold;Verkocht;miss;Ontbrekend;disp;Afgevoerd;don;Geschonken;sto;Af te voeren' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Composant de'||CHR(13)||CHR(10)
||'l��quipement :' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'subcomponent_of';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Annotation de relev� d�actif' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'survey_redline_eq';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Port d��quipement destinataire' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'tc_eqport_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure de cr�ation de l�enregistrement' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'time_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Utilisation de'||CHR(13)||CHR(10)
||'l��quipement' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'use1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur'||CHR(13)||CHR(10)
||'r�siduelle' WHERE TABLE_NAME = 'eq' AND FIELD_NAME = 'value_salvage';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard d��quipement' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l��quipement', SL_HEADING_FR = 'Statut de l��quipement' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'survey_photo_eq_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Annotation de relev� d�actif' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'survey_redline_eq';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ML_HEADING_FR = 'Le document d�annotation'||CHR(13)||CHR(10)
||'du relev� est nouveau', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'eq_audit' AND FIELD_NAME = 'survey_redline_eq_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de l�Employ�'||CHR(13)||CHR(10)
||'en Inventaire' WHERE TABLE_NAME = 'eq_compinvsur' AND FIELD_NAME = 'inv_em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Inventaire' WHERE TABLE_NAME = 'eq_compinvsur' AND FIELD_NAME = 'inv_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de l�Employ�'||CHR(13)||CHR(10)
||'du Relev�' WHERE TABLE_NAME = 'eq_compinvsur' AND FIELD_NAME = 'sur_em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'eq_compinvtrial' AND FIELD_NAME = 'trial_rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Dotation'||CHR(13)||CHR(10)
||'d�Amortissement' WHERE TABLE_NAME = 'eq_dep' AND FIELD_NAME = 'value_current_dep';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'eq_eqstdcnts' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'eq_eqstdcnts' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Actif' WHERE TABLE_NAME = 'eq_req_items' AND FIELD_NAME = 'asset_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Kurzzeichen Unternehmenseinheit' WHERE TABLE_NAME = 'eq_req_items' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'eq_req_items' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� du plan d�actif' WHERE TABLE_NAME = 'eq_req_items' AND FIELD_NAME = 'planning_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'Budget d�Espace' WHERE TABLE_NAME = 'eq_req_items' AND FIELD_NAME = 'sb_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'eq_rm' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||' l�employ�' WHERE TABLE_NAME = 'eq_sync' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard d��quipement' WHERE TABLE_NAME = 'eq_sync' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'eq_sync' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'eq_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut'||CHR(13)||CHR(10)
||'de l��quip.' WHERE TABLE_NAME = 'eq_sync' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d��quipement�:'||CHR(13)||CHR(10)
||'D�pendant' WHERE TABLE_NAME = 'eq_system' AND FIELD_NAME = 'eq_id_depend';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d��quipement�:'||CHR(13)||CHR(10)
||'Ma�tre' WHERE TABLE_NAME = 'eq_system' AND FIELD_NAME = 'eq_id_master';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'eq_trial' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'eq_trial' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage dans'||CHR(13)||CHR(10)
||'simulation' WHERE TABLE_NAME = 'eq_trial' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'eq_trial' AND FIELD_NAME = 'rm_id';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Port d��quipement destinataire' WHERE TABLE_NAME = 'eqport' AND FIELD_NAME = 'tc_eqport_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'eqport' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'d��quipement' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'category';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de l��quipement' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Doc. Graphique'||CHR(13)||CHR(10)
||'du Bloc Std d�Equipement' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'doc_block';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Doc. Graphique'||CHR(13)||CHR(10)
||'du Std d�Equipt' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'doc_graphic';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Graphique du'||CHR(13)||CHR(10)
||'std d��quipt' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'image_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Bloc du standard'||CHR(13)||CHR(10)
||'d��quipement' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'image_of_block';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Multiplexing?' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'is_multiplexing';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires d�infos source' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'source_feed_comments';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� ��������' WHERE TABLE_NAME = 'eqstd' AND FIELD_NAME = 'source_time_update';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de l��quipement' WHERE TABLE_NAME = 'eqstd_sync' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard d��quipement' WHERE TABLE_NAME = 'eqstd_sync' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'eqstd_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'de type d�infrastructure' WHERE TABLE_NAME = 'facility_type' AND FIELD_NAME = 'facility_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Supergroupe�d�analyse' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_super_group';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (langue01)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (langue02)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (langue03)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (langue04)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_04';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (langue05)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_05';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (chinois simplifi�)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_ch';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (allemand)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_de';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (espagnol)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_es';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (fran�ais)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_fr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (italien)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_it';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (japonais)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_jp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (cor�en)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_ko';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (n�erlandais)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_nl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (norv�gien)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_no';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse (chinois traditionnel)' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'analysis_title_zh';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre d�Affichage' WHERE TABLE_NAME = 'finanal_analyses' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Analysis Scorecard;Fiche d��valuation d�analyse;Asset Scorecard;Fiche d��valuation d�actif;Asset Map;Carte d�actifs;Site Plan;Plan du site;Lifecycle Analysis;Analyse du cycle de vie;Trend Analysis;Analyse de tendances', ML_HEADING_FR = 'Volet de console d�analyse' WHERE TABLE_NAME = 'finanal_analyses_flds' AND FIELD_NAME = 'analysis_console_panel';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ d�analyse' WHERE TABLE_NAME = 'finanal_analyses_flds' AND FIELD_NAME = 'analysis_field';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Tableau d�analyse' WHERE TABLE_NAME = 'finanal_analyses_flds' AND FIELD_NAME = 'analysis_table';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�analyse' WHERE TABLE_NAME = 'finanal_analyses_flds' AND FIELD_NAME = 'analysis_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre d�Affichage' WHERE TABLE_NAME = 'finanal_analyses_flds' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Groupe d�analyse' WHERE TABLE_NAME = 'finanal_loc_group' AND FIELD_NAME = 'analysis_group';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Supergroupe d�analyse' WHERE TABLE_NAME = 'finanal_loc_group' AND FIELD_NAME = 'analysis_super_group';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (langue01)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (langue02)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (langue03)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (langue04)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_04';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (langue05)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_05';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (chinois simplifi�)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_ch';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (allemand)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_de';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (espagnol)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_es';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (fran�ais)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_fr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (italien)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_it';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (japonais)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_jp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (cor�en)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_ko';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (n�erlandais)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_nl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (norv�gien)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_no';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Texte d�info-bulle de zone (chinois traditionnel)' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_tooltip_zh';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'C1;Premi�re colonne;C2;Deuxi�me colonne;C3;Colonne d�analyse;FR;Cumul financier;PR;Cumul en pourcentage;AR;Cumul d�analyse;BO;Zone individuelle' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'box_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Couleur d�arri�re-plan' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'display_bkg_color';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�Affichage' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cumul d�analyse 01' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'rollup_anlys_box_id_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cumul d�analyse 02' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'rollup_anlys_box_id_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cumul d�analyse 03' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'rollup_anlys_box_id_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cumul d�analyse 04' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'rollup_anlys_box_id_04';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cumul d�analyse 05' WHERE TABLE_NAME = 'finanal_matrix' AND FIELD_NAME = 'rollup_anlys_box_id_05';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Analysis Scorecard;Fiche d��valuation d�analyse;Asset Scorecard;Fiche d��valuation d�actif;Asset Map;Carte d�actifs;Site Plan;Plan du site;Lifecycle Analysis;Analyse du cycle de vie;Trend Analysis;Analyse de tendances', ML_HEADING_FR = 'Volet de console d�analyse' WHERE TABLE_NAME = 'finanal_matrix_flds' AND FIELD_NAME = 'analysis_console_panel';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champ d�analyse' WHERE TABLE_NAME = 'finanal_matrix_flds' AND FIELD_NAME = 'analysis_field';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Tableau d�analyse' WHERE TABLE_NAME = 'finanal_matrix_flds' AND FIELD_NAME = 'analysis_table';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre d�Affichage' WHERE TABLE_NAME = 'finanal_matrix_flds' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Base des co�ts pour l�amortissement des b�timents' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'cost_basis_for_deprec';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Prix'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'cost_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de d�but de l�appr�ciation' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'date_apprec_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'date_purchased';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Taux d�int�r�t du pr�t' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'loan_rate';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Types d�immobilisation' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'property_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Taux d�appr�ciation' WHERE TABLE_NAME = 'finanal_params' AND FIELD_NAME = 'rate_apprec';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Actif' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'asset_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Projets d�investissement (ex. fisc.)' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'cap_proj_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Projets d�investissement par surface brute (ex. fisc.)' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'cap_proj_approved_pga';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Projets d�investissement planifi�s'||CHR(13)||CHR(10)
||'(3�ans)' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'cap_proj_planned_3years';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Projets d�investissement planifi�s' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'cap_proj_planned_lifetime';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Amortissement - Projets d�investissement (ex. fisc.)' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'fin_anlys_depr_capproj';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nbr. max. d�occupants' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'occ_occupantsmax';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Et� dell�edificio (anni)' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'spac_bl_age';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'spac_criticality';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'PIPELINE;En cours d�acquisition;UNDER CONTRACT;Sous contrat;ESCROWED;D�p�t l�gal;IN SERVICE;En service;OWNED;Poss�d�;OWNED AND LEASED;Poss�d� et lou�;LEASED;Lou�;SUB-LEASED;Sous-lou�;SUB LET;Sous-lou�;FOR SALE;A vendre;LEASED (EXPIRED);Lou� (expir�);OUT OF SERVICE;Hors service;ABANDONED;Abandonn�;DONATED;Donn�;DISPOSED;Elimin�;SOLD;Vendu;N/A;S/O;UNKNOWN;Inconnu' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'spac_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Uso', ML_HEADING_FR = 'Consommation' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'spac_use1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Variance d�inoccupation (%)' WHERE TABLE_NAME = 'finanal_sum' AND FIELD_NAME = 'spac_vacancy_cng';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Actif' WHERE TABLE_NAME = 'finanal_sum_life' AND FIELD_NAME = 'asset_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Surface commune'||CHR(13)||CHR(10)
||'de l��tage du groupe' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'area_fl_comn_gp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Non occup. '||CHR(13)||CHR(10)
||'Surface commune'||CHR(13)||CHR(10)
||'� l��tage' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'area_fl_comn_nocup';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Surface'||CHR(13)||CHR(10)
||'commune � l��tage'||CHR(13)||CHR(10)
||'occupable' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'area_fl_comn_ocup';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Surface commune'||CHR(13)||CHR(10)
||'de l��tage de la pi�ce' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'area_fl_comn_rm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Surface utilitaire'||CHR(13)||CHR(10)
||'commune � l��tage' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'area_fl_comn_serv';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Graphique'||CHR(13)||CHR(10)
||'de l��tage' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'image_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Taux (U/L)'||CHR(13)||CHR(10)
||'d�efficacit�' WHERE TABLE_NAME = 'fl' AND FIELD_NAME = 'ratio_ur';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'fn' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'fn_trial' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage dans'||CHR(13)||CHR(10)
||'simulation' WHERE TABLE_NAME = 'fn_trial' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'fn_trial' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Police'||CHR(13)||CHR(10)
||'d�Assurance' WHERE TABLE_NAME = 'fnstd' AND FIELD_NAME = 'policy_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'fp' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'fp' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de l�application' WHERE TABLE_NAME = 'gb_cert_proj' AND FIELD_NAME = 'date_application';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�enregistrement' WHERE TABLE_NAME = 'gb_cert_proj' AND FIELD_NAME = 'date_registered';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Niveau d�objectif' WHERE TABLE_NAME = 'gb_cert_proj' AND FIELD_NAME = 'goal_level';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�a�ronef' WHERE TABLE_NAME = 'gb_fp_airc_data' AND FIELD_NAME = 'aircraft_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Consumo medio combustubile per aereo (metri cubi all�ora)', ML_HEADING_FR = 'Consommation moyenne d�un avion � r�action (gallons/heure)' WHERE TABLE_NAME = 'gb_fp_airc_data' AND FIELD_NAME = 'avg_fuel';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Consommation moyenne d�un'||CHR(13)||CHR(10)
||'avion � r�action' WHERE TABLE_NAME = 'gb_fp_airc_data' AND FIELD_NAME = 'avg_fuel_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_airc_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_carbon_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� CH4_N2O' WHERE TABLE_NAME = 'gb_fp_comm_airc_data' AND FIELD_NAME = 'ch4_n2o_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� de CO2' WHERE TABLE_NAME = 'gb_fp_comm_airc_data' AND FIELD_NAME = 'co2_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� CH4_N2O' WHERE TABLE_NAME = 'gb_fp_egrid_subregions' AND FIELD_NAME = 'ch4_n2o_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� de CO2' WHERE TABLE_NAME = 'gb_fp_egrid_subregions' AND FIELD_NAME = 'co2_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Protossido d�azoto (N2O) (kg/GWh)' WHERE TABLE_NAME = 'gb_fp_egrid_subregions' AND FIELD_NAME = 'n2o';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Protossido d�azoto (N2O)' WHERE TABLE_NAME = 'gb_fp_egrid_subregions' AND FIELD_NAME = 'n2o_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Protossido d�azoto (N2O)' WHERE TABLE_NAME = 'gb_fp_emiss_data' AND FIELD_NAME = 'n2o_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_emiss_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_fuel_dens_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_heat_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� CH4-N2O' WHERE TABLE_NAME = 'gb_fp_mobile_data' AND FIELD_NAME = 'ch4_n2o_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� de CO2' WHERE TABLE_NAME = 'gb_fp_mobile_data' AND FIELD_NAME = 'co2_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Protossido d�azoto (N2O)' WHERE TABLE_NAME = 'gb_fp_mobile_data' AND FIELD_NAME = 'n2o_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur d�oxydation (%)' WHERE TABLE_NAME = 'gb_fp_oxid_data' AND FIELD_NAME = 'factor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'gb_fp_refrig_data' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Emissioni operazione (% dell�addebito iniziale annuale)', ML_HEADING_FR = 'Emissions de l�exploitation (% de la charge initiale par an)' WHERE TABLE_NAME = 'gb_fp_refrig_data' AND FIELD_NAME = 'operation_emiss';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_refrig_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�a�ronef' WHERE TABLE_NAME = 'gb_fp_s1_co_airc' AND FIELD_NAME = 'aircraft_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant d�a�ronef '||CHR(13)||CHR(10)
||'appartenant � la soci�t�' WHERE TABLE_NAME = 'gb_fp_s1_co_airc' AND FIELD_NAME = 'source_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit� CH4_N2O' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'ch4_n2o_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur d��missions de CH4' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'emiss_factor_ch4_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur d��missions de CH4 (g/GJ)' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'emiss_factor_ch4_val';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur d��missions de N20' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'emiss_factor_n2o_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur d��missions de N20 (g/GJ)' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'emiss_factor_n2o_val';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de carburant' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'fuel_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Facteur d�oxydation (%)' WHERE TABLE_NAME = 'gb_fp_s1_fuel_comb' AND FIELD_NAME = 'oxid_factor_val';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nombre d�unit�s' WHERE TABLE_NAME = 'gb_fp_s1_refrig_ac' AND FIELD_NAME = 'refrig_ac_count';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s1_s3_mobile' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant de l��lectricit� achet�e' WHERE TABLE_NAME = 'gb_fp_s2_purch_e' AND FIELD_NAME = 'source_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de consommation'||CHR(13)||CHR(10)
||'d��lectricit�' WHERE TABLE_NAME = 'gb_fp_s2_purch_e' AND FIELD_NAME = 'units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s2_purch_e' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s3_em_air' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s3_mat' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de consommation'||CHR(13)||CHR(10)
||'d��lectricit�' WHERE TABLE_NAME = 'gb_fp_s3_serv' AND FIELD_NAME = 'units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s3_serv' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s3_waste_liq' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant de l��limination des d�chets' WHERE TABLE_NAME = 'gb_fp_s3_waste_sol' AND FIELD_NAME = 'source_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_s3_waste_sol' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Tonnes d��quivalent'||CHR(13)||CHR(10)
||'de dioxyde de carbone (MTCO2e)' WHERE TABLE_NAME = 'gb_fp_s_other' AND FIELD_NAME = 'mt_co2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Donn�es des facteurs d��mission' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'emiss_version';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de donn�es de facteurs d��mission' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'emiss_version_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Donn�es d��nergie calorifique' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'heat_version';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de donn�es d��nergie calorifique' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'heat_version_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Donn�es des facteurs d��missions mobiles' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'mobile_version';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de donn�es de facteurs d��missions mobiles' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'mobile_version_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Donn�es des facteurs d�oxydation' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'oxid_version';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type des donn�es des facteurs d�oxydation' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'oxid_version_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Branche de facteur d��mission' WHERE TABLE_NAME = 'gb_fp_setup' AND FIELD_NAME = 'sector_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;N/A;gb_fp_egrid_subregions;eGRID;gb_fp_fuel_dens_data;Densit� de carburant;gb_fp_heat_data;�nergie calorifique;gb_fp_carbon_data;Teneur en carbone;gb_fp_oxid_data;Facteurs d�oxydation;gb_fp_emiss_data;�mission;gb_fp_gwp_data;Potentiel de r�chauffement global;gb_fp_mobile_data;Mobile;gb_fp_airc_data;avion;gb_fp_refrig_data;R�frig�ration/climatisation;gb_fp_waste_sol_data;D�chets solides;gb_fp_waste_liq_data;Eaux us�es;gb_fp_comm_airc_data;avion commercial', ENUM_LIST_NL = 'N/A;NVT;gb_fp_egrid_subregio�s;elektriciteitsnet;gb_fp_fuel_dens_data;brandstofdichtheid;gb_fp_heat_data;warmte-inhoud;gb_fp_carbon_data;koolstofinhoud;gb_fp_oxid_data;oxidatiefactoren;gb_fp_emiss_data;emissie;gb_fp_gwp_data;invloed opwarming atmosfeer;gb_fp_mobile_data;mobiel;gb_fp_airc_data;vliegtuig;gb_fp_refrig_data;koeling/airco;gb_fp_waste_sol_data;vast afval;gb_fp_waste_liq_data;afvalwater;gb_fp_comm_airc_data;commercieel vliegtuig' WHERE TABLE_NAME = 'gb_fp_versions' AND FIELD_NAME = 'version_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_waste_liq_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fabrication de produit � partir'||CHR(13)||CHR(10)
||'d�un ensemble actuel de mati�res recycl�es (MTCE/tonne)' WHERE TABLE_NAME = 'gb_fp_waste_sol_data' AND FIELD_NAME = 'manufacture_mix';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fabrication de produit � partir'||CHR(13)||CHR(10)
||'d�un ensemble actuel de mati�res recycl�es' WHERE TABLE_NAME = 'gb_fp_waste_sol_data' AND FIELD_NAME = 'manufacture_mix_entry';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s'||CHR(13)||CHR(10)
||'d��missions nettes' WHERE TABLE_NAME = 'gb_fp_waste_sol_data' AND FIELD_NAME = 'units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s' WHERE TABLE_NAME = 'gb_fp_waste_sol_data' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'geo_region' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�affectation'||CHR(13)||CHR(10)
||'d�espace' WHERE TABLE_NAME = 'gp' AND FIELD_NAME = 'allocation_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'gp' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ev�nement'||CHR(13)||CHR(10)
||'d�affectation' WHERE TABLE_NAME = 'gp' AND FIELD_NAME = 'event_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'gp' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = '% de'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'gp' AND FIELD_NAME = 'pct_floor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Kurzzeichen Unternehmenseinheit' WHERE TABLE_NAME = 'gp' AND FIELD_NAME = 'planning_bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Kurzzeichen Unternehmenseinheit' WHERE TABLE_NAME = 'gpsum' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'grid' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'gros' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'gros' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Frais d�exploitation'||CHR(13)||CHR(10)
||'annuels (13)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'annual_operating_costs';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indice'||CHR(13)||CHR(10)
||'d��tat (11)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'condition_index';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Circonscription'||CHR(13)||CHR(10)
||'d�un Repr�sentant (20)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'congressional_district';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de l�'||CHR(13)||CHR(10)
||'affectation (24.b)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'disposition_date';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'NA;Sans objet;PB;Transfert de propri�t� pour le bien public;HA;(PB) Assistance aux sans-abris;HE;(PB) Utilisation � des fins m�dicales ou �ducatives;PR;(PB) Parcs publics et espaces publics de loisirs;HM;(PB) Monuments historiques;CF;(PB) Usage d��tablissement p�nitentiaire;PF;(PB) Installations portuaires;PA;(PB) A�roports publics;WC;(PB) Protection de la faune sauvage;NS;(PB) Ventes n�goci�es aux agences publiques;SH;(PB) Autoconstruction;LE;(PB) Application de la loi et r�ponse de gestion des urgences;FT;Transfert f�d�ral;SL;Vente;SN;(SL) Vente n�goci�e;SP;(SP) Vente publique;DM;D�molition;LX;R�siliation de bail;OT;Autre', ENUM_LIST_IT = 'NA;Non applicabile;PB;Trasferimento di vantaggi pubblici;HA;(PB) Assistenza ai senza tetto;HE;(PB) Utilizzo nel settore della sanit� o dell�istruzione;PR;(PB) Parchi pubblici e aree ricreative pubbliche;HM;(PB) Monumenti storici;CF;(PB) Utilizzo per penitenziari;PF;(PB) Installazioni portuali;PA;(PB) Aeroporti pubblici;WC;(PB) Conservazione ambientale;NS;(PB) Vendite negoziate per enti pubblici;SH;(PB) Coinvolgimento dell�abitante;LE;(PB) Forze dell�ordine e risposta di gestione delle emergenze;FT;Trasferimento federale;SL;Vendita;SN;(SL) Vendita negoziata;SP;(SP) Vendita pubblica;DM;Demolizione;LX;Termine locazione;OT;Altro', ML_HEADING_FR = 'M�thode de l�'||CHR(13)||CHR(10)
||'affection (24.a)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'disposition_method_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur de l�'||CHR(13)||CHR(10)
||'affectation (24.c)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'disposition_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant'||CHR(13)||CHR(10)
||'de l�installation (22.a)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'installation_identifier';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'de l�installation (22.a)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'installation_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indicateur de l�organisme responsable'||CHR(13)||CHR(10)
||'de la location (3.c)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'lease_authority_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'G;Propri�t�s;L;Lou�;S;Propri�t� de l��tat;F;Propri�t� d�un gouvernement �tranger;M;Mus�e', ML_HEADING_FR = 'Indicateur d�int�r�t'||CHR(13)||CHR(10)
||'l�gal (3.a)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'legal_interest_ind';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indicateur de'||CHR(13)||CHR(10)
||'droit d�utilisation (4.b)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'outgrant_indicator';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'L�actif est-il un '||CHR(13)||CHR(10)
||'candidat potentiel pour la vente�? (30.a)' WHERE TABLE_NAME = 'grp' AND FIELD_NAME = 'sale_candidate';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Frais d�exploitation'||CHR(13)||CHR(10)
||'annuels (13)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'annual_operating_costs';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indice'||CHR(13)||CHR(10)
||'d��tat (11)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'condition_index';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Circonscription'||CHR(13)||CHR(10)
||'d�un Repr�sentant (20)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'congressional_district';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de '||CHR(13)||CHR(10)
||'l�approbation/du rejet' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'date_of_app_rej';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de l�'||CHR(13)||CHR(10)
||'affectation (24.b)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'disposition_date';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'NA;Sans objet;PB;Transfert de propri�t� pour le bien public;HA;(PB) Assistance aux sans-abris;HE;(PB) Utilisation � des fins m�dicales ou �ducatives;PR;(PB) Parcs publics et espaces publics de loisirs;HM;(PB) Monuments historiques;CF;(PB) Usage d��tablissement p�nitentiaire;PF;(PB) Installations portuaires;PA;(PB) A�roports publics;WC;(PB) Protection de la faune sauvage;NS;(PB) Ventes n�goci�es aux agences publiques;SH;(PB) Autoconstruction;LE;(PB) Application de la loi et r�ponse de gestion des urgences;FT;Transfert f�d�ral;SL;Vente;SN;(SL) Vente n�goci�e;SP;(SP) Vente publique;DM;D�molition;LX;R�siliation de bail;OT;Autre', ENUM_LIST_IT = 'NA;Non applicabile;PB;Trasferimento di vantaggi pubblici;HA;(PB) Assistenza ai senza tetto;HE;(PB) Utilizzo nel settore della sanit� o dell�istruzione;PR;(PB) Parchi pubblici e aree ricreative pubbliche;HM;(PB) Monumenti storici;CF;(PB) Utilizzo per penitenziari;PF;(PB) Installazioni portuali;PA;(PB) Aeroporti pubblici;WC;(PB) Conservazione ambientale;NS;(PB) Vendite negoziate per enti pubblici;SH;(PB) Coinvolgimento dell�abitante;LE;(PB) Forze dell�ordine e risposta di gestione delle emergenze;FT;Trasferimento federale;SL;Vendita;SN;(SL) Vendita negoziata;SP;(SP) Vendita pubblica;DM;Demolizione;LX;Termine locazione;OT;Altro', ML_HEADING_FR = 'M�thode de l�'||CHR(13)||CHR(10)
||'affection (24.a)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'disposition_method_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur de l�'||CHR(13)||CHR(10)
||'affectation (24.c)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'disposition_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant'||CHR(13)||CHR(10)
||'de l�installation (22.a)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'installation_identifier';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'de l�installation (22.a)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'installation_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indicateur de l�organisme responsable'||CHR(13)||CHR(10)
||'de la location (3.c)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'lease_authority_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Z;Sans objet;G;Propri�t�;L;Location;S;Propri�t� de l��tat;F;Propri�t� d�un gouvernement �tranger;M;Mus�e', ML_HEADING_FR = 'Indicateur d�int�r�t'||CHR(13)||CHR(10)
||'l�gal (3.a)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'legal_interest_ind';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indicateur de'||CHR(13)||CHR(10)
||'droit d�utilisation (4.b)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'outgrant_indicator';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'L�actif est-il un '||CHR(13)||CHR(10)
||'candidat potentiel pour la vente�? (30.a)' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'sale_candidate';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure de '||CHR(13)||CHR(10)
||'l�approbation/du rejet' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'time_of_app_rej';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Approuver/Rejeter'||CHR(13)||CHR(10)
||'le nom d�utilisateur' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'user_name_app_rej';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom d�utilisateur'||CHR(13)||CHR(10)
||'du demandeur' WHERE TABLE_NAME = 'grp_trans' AND FIELD_NAME = 'user_name_requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�utilisation des biens '||CHR(13)||CHR(10)
||'immobiliers du gouvernement' WHERE TABLE_NAME = 'grp_use' AND FIELD_NAME = 'grp_use_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�Action' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'action_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code El�ment d�Action' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Action' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Associ� �'||CHR(13)||CHR(10)
||'l�ID d��valuation' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'assessment_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er automatiquement une demande d�intervention�?' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Unassigned;Non affect�;Def Maint;Maintenance diff�r�e;Capital Renewal;Renouvellement/Modernisation d�investissements;Adaptation;Adaptation', ML_HEADING_FR = 'Programme d�investissements' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'capital_program';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�Unit�s' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'cb_units_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeurs d��tat du patrimoine' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'cond_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Copi� � partir de l�ID' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'copied_from';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Approbation' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique de date d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'date_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Installation' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ouvrier responsable'||CHR(13)||CHR(10)
||'de l��limination' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'hcm_abate_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'N� de'||CHR(13)||CHR(10)
||'racores' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'hcm_fittings_num';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'En attente d�action' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'hcm_pending_act';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'N� de'||CHR(13)||CHR(10)
||'tuber�as' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'hcm_pipe_cnt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�accident' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'incident_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pourcentage '||CHR(13)||CHR(10)
||'d�Ach�vement' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'pct_complete';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Associ� au'||CHR(13)||CHR(10)
||'code �l�ment d�action' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'related_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Non saisi;1;Maintenance;2;Ressource naturelle;3;Recyclable;4;Qualit� de l�air ambiant;5;Economies d�eau;6;Produits chimiques;7;D�chets toxiques;8;D�chets solides;9;Emissions;10;Conso. d��nergie', ENUM_LIST_IT = '0;Non specificato;1;Gestione;2;Risorsa naturale;3;Materiali riciclabili;4;Qualit� dell�aria interna;5;Risparmio idrico;6;Composti chimici;7;Rifiuti pericolosi;8;Rifiuti solidi;9;Emissione;10;Utilizzo energia' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'sust_priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||'pour ex�cution �' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'time_esc_comp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'time_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'hactivity_log' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'cuenta', ML_HEADING_NL = 'Account-'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Poste'||CHR(13)||CHR(10)
||'Comptable' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de l�Action' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'action_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code El�ment d�Action' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Action' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Asociado con'||CHR(13)||CHR(10)
||'ID de evaluaci�n', ML_HEADING_NL = 'Gekoppeld aan'||CHR(13)||CHR(10)
||'evaluatie-ID', ML_HEADING_FR = 'Associ� �'||CHR(13)||CHR(10)
||'l�identifiant d��valuation' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'assessment_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er automatiquement une demande d�intervention�?' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Unassigned;Non affect�;Def Maint;Maintenance diff�r�e;Capital Renewal;Renouvellement/Modernisation d�investissements;Adaptation;Adaptation', ML_HEADING_FR = 'Programme d�investissements' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'capital_program';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'addetto alla manutenzione', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'vakman', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'technicien', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Handwerker' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'cf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeurs d��tat du patrimoine' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'cond_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Copi� � partir de l�ID' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'copied_from';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'de co�ts' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'cost_cat_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Kosten-'||CHR(13)||CHR(10)
||'afwijking', ML_HEADING_FR = 'Variance'||CHR(13)||CHR(10)
||'des co�ts', ML_HEADING_DE = 'Abweichung'||CHR(13)||CHR(10)
||'der Kosten' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'cost_var';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'approvazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�approbation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'genehmigt' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding voor'||CHR(13)||CHR(10)
||'uitvoeringstijd optreedt', ML_HEADING_FR = 'Recours hi�rarchique pour'||CHR(13)||CHR(10)
||'ex�cution le' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'date_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Datum waarop overschrijding'||CHR(13)||CHR(10)
||'responstijd optreedt', ML_HEADING_FR = 'Remise � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse le', ML_HEADING_DE = 'Termineskalation f�r'||CHR(13)||CHR(10)
||'Reaktion tritt auf' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'date_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Installation' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���� /'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Handle de entidad/'||CHR(13)||CHR(10)
||'Identificador �nico', ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique', ML_HEADING_DE = 'Objektreferenz/'||CHR(13)||CHR(10)
||'Eindeutige ID' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'afronding?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Erledigung?' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'escalated_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'antwoord?', ML_HEADING_FR = 'Remont� � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Reaktion?' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'escalated_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Gestor de centro'||CHR(13)||CHR(10)
||'de soporte', ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'du centre de services', ML_HEADING_DE = 'Servicedesk'||CHR(13)||CHR(10)
||'Manager' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'manager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Pourcentage'||CHR(13)||CHR(10)
||'d�ach�vement' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'pct_complete';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'T�l�phone du'||CHR(13)||CHR(10)
||'demandeur', ML_HEADING_DE = 'Telefonnummer'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'phone_requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = 'PM'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'PO-'||CHR(13)||CHR(10)
||'procedure', ML_HEADING_DE = 'IH-'||CHR(13)||CHR(10)
||'Vorgang' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'pmp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'���', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'orden de compra', ML_HEADING_NL = 'Code van bestelbon', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'bon de commande', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Bestellung' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'Progetto', ML_HEADING_DE = 'Projektcode' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Terugkerende'||CHR(13)||CHR(10)
||'regel', ML_HEADING_FR = 'R�gle'||CHR(13)||CHR(10)
||'r�currente' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'recurring_rule';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Calificaci�n de'||CHR(13)||CHR(10)
||'la satisfacci�n', ML_HEADING_NL = 'Tevredenheids-'||CHR(13)||CHR(10)
||'score', ML_HEADING_FR = 'Note de'||CHR(13)||CHR(10)
||'satisfaction', ML_HEADING_DE = 'Zufriedenheitsbewertung' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'satisfaction';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Notas de'||CHR(13)||CHR(10)
||'satisfacci�n', ML_HEADING_NL = 'Opmerkingen'||CHR(13)||CHR(10)
||'bij waardering' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'satisfaction_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Fornitore'||CHR(13)||CHR(10)
||'di servizi', ML_HEADING_NL = 'Dienst-'||CHR(13)||CHR(10)
||'verlener' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'service_provider';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_NL = 'Stap-'||CHR(13)||CHR(10)
||'status', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l��tape' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = '0;Non saisi;1;Maintenance;2;Ressource naturelle;3;Recyclable;4;Qualit� de l�air ambiant;5;Economies d�eau;6;Produits chimiques;7;D�chets toxiques;8;D�chets solides;9;Emissions;10;Conso. d��nergie', ENUM_LIST_IT = '0;Non specificato;1;Gestione;2;Risorsa naturale;3;Materiali riciclabili;4;Qualit� dell�aria interna;5;Risparmio idrico;6;Composti chimici;7;Rifiuti pericolosi;8;Rifiuti solidi;9;Emissione;10;Utilizzo energia' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'sust_priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de terminaci�n', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'completamento', ML_HEADING_NL = 'Tijdstip waarop uitvoeringstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'time_escalation_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Hora en la que se produce'||CHR(13)||CHR(10)
||'el escalado de respuesta', ML_HEADING_IT = 'Ora inoltro per'||CHR(13)||CHR(10)
||'risposta', ML_HEADING_NL = 'Tijdstip waarop responstijd'||CHR(13)||CHR(10)
||'wordt overschreden' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'time_escalation_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Hora de'||CHR(13)||CHR(10)
||'solicitud' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'time_requested';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Ora'||CHR(13)||CHR(10)
||'obbligatoria', ML_HEADING_NL = 'Benodigde'||CHR(13)||CHR(10)
||'tijd vereist', ML_HEADING_FR = 'Requis'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'time_required';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Leveranciers-'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Lieferanten-'||CHR(13)||CHR(10)
||'Code' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��������', ML_HEADING_ES = 'C�digo de estructura de desglose de trabajo', ML_HEADING_IT = 'Codice struttura scomposizione lavoro', ML_HEADING_NL = 'Structuurcode werkspecificatie', ML_HEADING_FR = 'Code de nomenclature des travaux', ML_HEADING_DE = 'Objektcode der Arbeitsanalyse' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'wbs_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�', ML_HEADING_NL = 'Werkorder-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de bon'||CHR(13)||CHR(10)
||'de travaux', ML_HEADING_DE = 'Arbeitsauftrags-Nr.' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'wo_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Werkaanvraag'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de demande'||CHR(13)||CHR(10)
||'d�intervention', ML_HEADING_DE = 'Arbeitsanforderungs-'||CHR(13)||CHR(10)
||'nummer' WHERE TABLE_NAME = 'hactivity_logmonth' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type '||CHR(13)||CHR(10)
||'d�Unit�s' WHERE TABLE_NAME = 'hazard_container_type' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Champs � faire correspondre'||CHR(13)||CHR(10)
||'provenant d�Employ�' WHERE TABLE_NAME = 'helpdesk_roles' AND FIELD_NAME = 'match_em';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'helpdesk_roles' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard'||CHR(13)||CHR(10)
||'d�Employ�' WHERE TABLE_NAME = 'helpdesk_sla_request' AND FIELD_NAME = 'em_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'helpdesk_sla_request' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Faire correspondre la s�quence d�ordre' WHERE TABLE_NAME = 'helpdesk_sla_request' AND FIELD_NAME = 'match_ordering_seq';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�quence'||CHR(13)||CHR(10)
||'d�ordre' WHERE TABLE_NAME = 'helpdesk_sla_request' AND FIELD_NAME = 'ordering_seq';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl�'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'helpdesk_sla_response' AND FIELD_NAME = 'activity_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cr�er Automatiquement une'||CHR(13)||CHR(10)
||'Demande d�Intervention ?' WHERE TABLE_NAME = 'helpdesk_sla_response' AND FIELD_NAME = 'autocreate_wr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�quence'||CHR(13)||CHR(10)
||'d�ordre' WHERE TABLE_NAME = 'helpdesk_sla_response' AND FIELD_NAME = 'ordering_seq';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�agit-il d�un mod�le'||CHR(13)||CHR(10)
||'de service�?' WHERE TABLE_NAME = 'helpdesk_sla_response' AND FIELD_NAME = 'service_template';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'helpdesk_sla_response' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�agit-il d�un mod�le'||CHR(13)||CHR(10)
||'de workflow�?' WHERE TABLE_NAME = 'helpdesk_sla_response' AND FIELD_NAME = 'workflow_template';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'Nom d�employ�' WHERE TABLE_NAME = 'helpdesk_sla_steps' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�quence d�odre'||CHR(13)||CHR(10)
||'des contrats de niveau de service' WHERE TABLE_NAME = 'helpdesk_sla_steps' AND FIELD_NAME = 'ordering_seq';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;Sans Objet;CREATED;CR��;REQUESTED;DEMAND�;APPROVED;APPROUV�;REJECTED;REJET�;TRIAL;SIMULATION;BUDGETED;BUDG�TIS�;PLANNED;PLANIFI�;SCHEDULED;PROGRAMM�;CANCELLED;ANNUL�;IN PROGRESS;EN COURS;IN PROCESS-H;EN COURS - EN ATTENTE;STOPPED;ARR�T�;COMPLETED;TERMIN�;COMPLETED-V;TERMIN� ET V�RIFI�;CLOSED;FERM�;R;Demand�;Rev;Examin� mais En Attente;Rej;Rejet�;A;Approuv�;AA;Affect� au Bon de Travx;I;Emis et En Cours;HP;En Attente de Pi�ces D�tach�es;HA;En Attente d�Acc�s;HL;En Attente de Main d��uvre;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�' WHERE TABLE_NAME = 'helpdesk_sla_steps' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�quence d�ordre'||CHR(13)||CHR(10)
||'des �tapes' WHERE TABLE_NAME = 'helpdesk_sla_steps' AND FIELD_NAME = 'step_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'helpdesk_sla_steps' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'Nom d�employ�' WHERE TABLE_NAME = 'helpdesk_step_log' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�quence d�ordre'||CHR(13)||CHR(10)
||'des �tapes' WHERE TABLE_NAME = 'helpdesk_step_log' AND FIELD_NAME = 'step_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'helpdesk_step_log' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'Nom d�utilisateur' WHERE TABLE_NAME = 'helpdesk_step_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�employ�' WHERE TABLE_NAME = 'hhelpdesk_step_log' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'hhelpdesk_step_log' AND FIELD_NAME = 'step_code';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID de registre'||CHR(13)||CHR(10)
||'d��tapes' WHERE TABLE_NAME = 'hhelpdesk_step_log' AND FIELD_NAME = 'step_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�quence d�ordre'||CHR(13)||CHR(10)
||'des �tapes' WHERE TABLE_NAME = 'hhelpdesk_step_log' AND FIELD_NAME = 'step_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��tape' WHERE TABLE_NAME = 'hhelpdesk_step_log' AND FIELD_NAME = 'step_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse par'||CHR(13)||CHR(10)
||'nom d�utilisateur' WHERE TABLE_NAME = 'hhelpdesk_step_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'hist_em_count' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Annulation' WHERE TABLE_NAME = 'hreserve' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Document'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'hreserve' AND FIELD_NAME = 'doc_event';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indice'||CHR(13)||CHR(10)
||'d�occurrence' WHERE TABLE_NAME = 'hreserve' AND FIELD_NAME = 'occurrence_index';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�;Room Conflict;Conflit de pi�ces' WHERE TABLE_NAME = 'hreserve' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Annulation' WHERE TABLE_NAME = 'hreserve_rm' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�am�nagement'||CHR(13)||CHR(10)
||'de pi�ce' WHERE TABLE_NAME = 'hreserve_rm' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�' WHERE TABLE_NAME = 'hreserve_rm' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Annulation' WHERE TABLE_NAME = 'hreserve_rs' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�' WHERE TABLE_NAME = 'hreserve_rs' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'hrmpct' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'hrmpct' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Du '||CHR(13)||CHR(10)
||'code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'hrmpct' AND FIELD_NAME = 'from_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Risorse necessarie'||CHR(13)||CHR(10)
||'per l�hoteling' WHERE TABLE_NAME = 'hrmpct' AND FIELD_NAME = 'resources';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'hrmpct' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwo' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'hwo' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'hwo' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures R�elles'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'act_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t Estim� en'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'cost_est_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demande d�intervention'||CHR(13)||CHR(10)
||'ferm�e le' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique de date d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'date_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures Estim�es'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'est_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Envoi'||CHR(13)||CHR(10)
||'du Message' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Demande'||CHR(13)||CHR(10)
||'d�intervention parent' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'parent_wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demand�;Rev;Examin�/En attente;Rej;Rejet�;A;Approuv�;AA;Affect� � Bon Trvx;I;Emis/En cours;HP;En attente/Pi�ces d�t.;HA;En attente/Acc�s;HL;En attente/Md�O;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'none;Aucun;approved;Approuv�;accepted;Accept�;surveyed;Evalu�;verified;V�rifi�;dispatched;Distribu�;estimated;Estim�;scheduled;Programm�;rejected;Rejet�;declined;Refus�;waiting;En attente d�Etape', ML_HEADING_FR = 'Statut de l��tape'||CHR(13)||CHR(10)
||'de la Demande d�Intervention' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'step_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||'pour ex�cution �' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'time_esc_comp_orig';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Recours hi�rarchique d�heure d�origine'||CHR(13)||CHR(10)
||'pour r�ponse �' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'time_esc_resp_orig';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'hwr' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures R�elles Main d�Oeuvre' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'act_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'acci�n', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�action', ML_HEADING_DE = 'Ma�nahmentyp' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t Estim� en Main d�Oeuvre' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'cost_est_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la Main d�Oeuvre' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Kosten-'||CHR(13)||CHR(10)
||'afwijking', ML_HEADING_FR = 'Variance'||CHR(13)||CHR(10)
||'des co�ts', ML_HEADING_DE = 'Abweichung'||CHR(13)||CHR(10)
||'der Kosten' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'cost_var';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Fecha de cierre de'||CHR(13)||CHR(10)
||'solicitud de trabajo', ML_HEADING_FR = 'Demande d�intervention'||CHR(13)||CHR(10)
||'ferm�e le', ML_HEADING_DE = 'Datum - Arbeits-'||CHR(13)||CHR(10)
||'anforderung abgeschlossen' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||' empleado', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||' dipendente', ML_HEADING_NL = 'Medewerker'||CHR(13)||CHR(10)
||' gegevenspunt', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||' l�employ�', ML_HEADING_DE = 'Mitarbeiter-'||CHR(13)||CHR(10)
||' name' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'afronding?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Erledigung?' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'escalated_completion';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'���?', ML_HEADING_NL = 'Ge�scaleerd voor'||CHR(13)||CHR(10)
||'antwoord?', ML_HEADING_FR = 'Remont� � la hi�rarchie'||CHR(13)||CHR(10)
||'pour r�ponse?', ML_HEADING_DE = 'Eskaliert f�r'||CHR(13)||CHR(10)
||'Reaktion?' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'escalated_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures Estim�es de Main d��uvre' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'est_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Gestor de centro'||CHR(13)||CHR(10)
||'de soporte', ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'du centre de services', ML_HEADING_DE = 'Servicedesk'||CHR(13)||CHR(10)
||'Manager' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'manager';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Envoi du Message' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'R;Demand�;Rev;Examin�/En attente;Rej;Rejet�;A;Approuv�;AA;Affect� � Bon Trvx;I;Emis/En cours;HP;En attente/Pi�ces d�t.;HA;En attente/Acc�s;HL;En attente/Md�O;S;Stopp�;Can;Annul�;Com;Termin�;Clo;Ferm�', ML_HEADING_FR = 'Statut de la Demande d�Intervention' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de equipo de trabajo', ML_HEADING_IT = 'Codice team di lavoro', ML_HEADING_NL = 'Werkteam code', ML_HEADING_FR = 'Code de l��quipe d�intervention', ML_HEADING_DE = 'Kurzzeichen Arbeitsteam' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Demande d�Intervention' WHERE TABLE_NAME = 'hwr_month' AND FIELD_NAME = 'wr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�Autre Ressource' WHERE TABLE_NAME = 'hwr_other' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de l�Autre'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'hwr_other' AND FIELD_NAME = 'other_rs_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t Total'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwrcf' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Envoi'||CHR(13)||CHR(10)
||'du Message' WHERE TABLE_NAME = 'hwrcf' AND FIELD_NAME = 'msg_delivery_status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;s/o;R;Demand�;Rev;Examin� mais en attente;Rej;Rejet�;A;Approuv�;AA;Affect� � un Bon;I;Emis et En cours;HP;En attente de pi�ces;HA;En attente d�acc�s;HL;En attente de main d�oeuvre;S;Stopp�;Can;Annul�;Com;Termin�' WHERE TABLE_NAME = 'hwrcf' AND FIELD_NAME = 'status_from_remote_cf';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Manutenzione'||CHR(13)||CHR(10)
||'preventiva?' WHERE TABLE_NAME = 'hwrcfana' AND FIELD_NAME = 'is_pm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'hwrcfana' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures R�elles'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'act_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heures Estim�es'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'est_labor_hours';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'D�compte des'||CHR(13)||CHR(10)
||'Demandes d�Interv.' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'hwr_count';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� manutenzione '||CHR(13)||CHR(10)
||'preventiva?' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'is_pm';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'hwrsum' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Outil Utilis�'||CHR(13)||CHR(10)
||'Jusqu��' WHERE TABLE_NAME = 'hwrtl' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Type d�Outil' WHERE TABLE_NAME = 'hwrtt' AND FIELD_NAME = 'tool_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Assureur' WHERE TABLE_NAME = 'insurer' AND FIELD_NAME = 'insurer_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'FAC-Stempel Unternehmenseinheit' WHERE TABLE_NAME = 'invoice' AND FIELD_NAME = 'fac_org_level_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant d��l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'invoice_line_item' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'N� d�article '||CHR(13)||CHR(10)
||'de ligne' WHERE TABLE_NAME = 'invoice_line_item' AND FIELD_NAME = 'line_item_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Lieu de'||CHR(13)||CHR(10)
||'stockage'||CHR(13)||CHR(10)
||'d�origine' WHERE TABLE_NAME = 'it' AND FIELD_NAME = 'pt_store_loc_from';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'N/A;S/O;New;Nouveau;Ready for Transit;Pr�t � l�envoi;In Transit;En transit;Received;Re�u;Error;Erreur' WHERE TABLE_NAME = 'it' AND FIELD_NAME = 'req_item_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'jk' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'jk' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Port d��quipement destinataire' WHERE TABLE_NAME = 'jk' AND FIELD_NAME = 'tc_eqport_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'jk' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Numero '||CHR(13)||CHR(10)
||'d�inizio' WHERE TABLE_NAME = 'jkcfg' AND FIELD_NAME = 'start_number';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration '||CHR(13)||CHR(10)
||'en Anglais' WHERE TABLE_NAME = 'lang_enum' AND FIELD_NAME = 'enum_english';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Traduction de la'||CHR(13)||CHR(10)
||'liste d��num�ration' WHERE TABLE_NAME = 'lang_enum' AND FIELD_NAME = 'enum_trans';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'de l��tage' WHERE TABLE_NAME = 'ls' AND FIELD_NAME = 'floors';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_IT = 'Destinazione d�uso'||CHR(13)||CHR(10)
||'spazio', ML_HEADING_FR = 'Utilisation'||CHR(13)||CHR(10)
||'de l�Espace' WHERE TABLE_NAME = 'ls' AND FIELD_NAME = 'space_use';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Lease Due Date;Date d��ch�ance du contrat de location;Option Due Date;Date d��ch�ance de l�option', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�alerte' WHERE TABLE_NAME = 'ls_alert_definition' AND FIELD_NAME = 'alert_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Couleur'||CHR(13)||CHR(10)
||'de l�alerte' WHERE TABLE_NAME = 'ls_alert_definition' AND FIELD_NAME = 'color';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Jours de notification'||CHR(13)||CHR(10)
||'avant l��ch�ance' WHERE TABLE_NAME = 'ls_alert_definition' AND FIELD_NAME = 'notification_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Jours de suppression'||CHR(13)||CHR(10)
||'apr�s la date d��ch�ance' WHERE TABLE_NAME = 'ls_alert_definition' AND FIELD_NAME = 'removal_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID de l�amendement '||CHR(13)||CHR(10)
||'de la location' WHERE TABLE_NAME = 'ls_amendment' AND FIELD_NAME = 'ls_amend_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'M�thode d�affectation MSC' WHERE TABLE_NAME = 'ls_cam_profile' AND FIELD_NAME = 'cam_alloc_method';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�ann�e MSC' WHERE TABLE_NAME = 'ls_cam_profile' AND FIELD_NAME = 'cam_year_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Doc d�assistance n��1' WHERE TABLE_NAME = 'ls_cam_rec_report' AND FIELD_NAME = 'support_doc1_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Doc d�assistance n��2' WHERE TABLE_NAME = 'ls_cam_rec_report' AND FIELD_NAME = 'support_doc2_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Doc d�assistance n��3' WHERE TABLE_NAME = 'ls_cam_rec_report' AND FIELD_NAME = 'support_doc3_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Doc d�assistance n��4' WHERE TABLE_NAME = 'ls_cam_rec_report' AND FIELD_NAME = 'support_doc4_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'ls_comm' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'ls_comm' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Urgent;Urgente;High;Elev�e;Average;Moyenne;Low;Faible' WHERE TABLE_NAME = 'ls_comm' AND FIELD_NAME = 'priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'ls_comm' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'ls_comm_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�indice' WHERE TABLE_NAME = 'ls_index_profile' AND FIELD_NAME = 'cost_index_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date - Fin de l�indexation' WHERE TABLE_NAME = 'ls_index_profile' AND FIELD_NAME = 'date_index_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date - D�but de l�indexation' WHERE TABLE_NAME = 'ls_index_profile' AND FIELD_NAME = 'date_index_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur d�indice initiale' WHERE TABLE_NAME = 'ls_index_profile' AND FIELD_NAME = 'index_value_initial';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fr�quence d�indexation' WHERE TABLE_NAME = 'ls_index_profile' AND FIELD_NAME = 'indexing_frequency';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 1' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'apprv_mgr1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 2' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'apprv_mgr2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 3' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'apprv_mgr3';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation '||CHR(13)||CHR(10)
||'Responsable 1' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'date_app_mgr1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation '||CHR(13)||CHR(10)
||'Responsable 2' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'date_app_mgr2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbbation '||CHR(13)||CHR(10)
||'Responsable 3' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'date_app_mgr3';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Approbation' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Du Type '||CHR(13)||CHR(10)
||'d�Ordinateur' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'from_comp_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'De'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'from_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'Employee;��;New Hire;���;Leaving;�� ��;Equipment;��;Asset;��;Room;��;Team;��;', ENUM_LIST_DE = 'Employee;Mitarbeiter;New Hire;Personalneuzugang;Leaving;Personalabgang;Equipment;Ger�te;Asset;Objekt;Room;Raum;Team;Team;', ENUM_LIST_ES = 'Employee;Empleado;New Hire;Nueva contrataci�n;Leaving;Baja de empleado;Equipment;Equipo;Asset;Activo;Room;Espacio;Team;Equipo;', ENUM_LIST_FR = 'Employee;Employ�;New Hire;Nouvel employ�;Leaving;D�part d�employ�;Equipment;Equipement;Asset;Actif;Room;Pi�ce;Team;Equipe;', ENUM_LIST_IT = 'Employee;Dipendente;New Hire;Nuova assunzione;Leaving;Dipendente in uscita;Equipment;Apparecchiatura;Asset;Asset;Room;Locale;Team;Team;', ML_HEADING_CH = '����', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di spostamento', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'verhuizing', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'d�m�nagement', ENUM_LIST_NL = 'Employee;Medewerker;New Hire;Nieuwe medewerker;Leaving;Vertrek van medewerker;Equipment;Uitrusting;Asset;Asset;Room;Ruimte;Team;Team;' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'mo_type';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_IT = 'Created;Creato;Requested;Richiesto;Requested-Estimated;Richiesto e stimato;Requested-On Hold;Richiesto e in attesa;Requested-Routed;Richiesto e instradato per l�approvazione;Requested-Rejected;Richiesto e rifiutato;Approved;Approvato;Approved-In Design;Approvato e in progettazione;Approved-Cancelled;Approvato e annullato;Issued-In Process;Emesso e in corso;Issued-On Hold;Emesso e in attesa;Issued-Stopped;Emesso e interrotto;Completed-Pending;Completato e azioni in sospeso;Completed-Not Ver;Completato e non verificato;Completed-Verified;Completato e verificato;Closed;Chiuso' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Vers'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'to_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� destinato ad'||CHR(13)||CHR(10)
||'una postazione'||CHR(13)||CHR(10)
||'hoteling' WHERE TABLE_NAME = 'mo' AND FIELD_NAME = 'to_hotel';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'D�compte'||CHR(13)||CHR(10)
||'d�Employ�s' WHERE TABLE_NAME = 'mo_churn' AND FIELD_NAME = 'count_em';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'mo_churn' AND FIELD_NAME = 'month';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Mouvements'||CHR(13)||CHR(10)
||'d�Employ�s' WHERE TABLE_NAME = 'mo_churn' AND FIELD_NAME = 'num_em_moves';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Mouvements'||CHR(13)||CHR(10)
||'d��quipement' WHERE TABLE_NAME = 'mo_churn' AND FIELD_NAME = 'num_eq_moves';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nouveau code'||CHR(13)||CHR(10)
||'prise d��quipt' WHERE TABLE_NAME = 'mo_eq' AND FIELD_NAME = 'eq_new_jk_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'mo_eq' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'De'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'mo_eq' AND FIELD_NAME = 'from_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'De'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'mo_fncount' AND FIELD_NAME = 'from_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_FR = 'Employ�'||CHR(13)||CHR(10)
||'� d�m�nager', ML_HEADING_DE = 'Umziehender'||CHR(13)||CHR(10)
||'Mitarbeiter' WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'Progetto', ML_HEADING_DE = 'Projektcode' WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'project_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Scenario-'||CHR(13)||CHR(10)
||'naam', ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'sc�nario', ML_HEADING_DE = 'Szenario-'||CHR(13)||CHR(10)
||'Name' WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'scenario_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Vers'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'mo_scenario_em' AND FIELD_NAME = 'to_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'De'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'mo_ta' AND FIELD_NAME = 'from_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'mobile_log' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d��mission' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'date_issued';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'de densit�' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'density_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Rayon d��vacuation '||CHR(13)||CHR(10)
||'sugg�r�' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'evacuation_radius';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de rayon '||CHR(13)||CHR(10)
||'d��vacuation' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'evacuation_radius_units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'rayon d��vacuation' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'evacuation_radius_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Op�rateur de '||CHR(13)||CHR(10)
||'point d��clair' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'flashpoint_operator';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de '||CHR(13)||CHR(10)
||'point d��clair' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'flashpoint_units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Point d��clair' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'flashpont';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'densit� de vapeur' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'vapor_density_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'VOC�s - maximum of effectief' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'voc_high';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'VOC�s - minimum' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'voc_low';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'VOC' WHERE TABLE_NAME = 'msds_data' AND FIELD_NAME = 'voc_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_chemical' AND FIELD_NAME = 'date_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_chemical' AND FIELD_NAME = 'time_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_constituent' AND FIELD_NAME = 'date_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_constituent' AND FIELD_NAME = 'time_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'date_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d��mission' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'date_issued';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'de densit�' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'density_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Rayon d��vacuation '||CHR(13)||CHR(10)
||'sugg�r�' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'evacuation_radius';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de rayon '||CHR(13)||CHR(10)
||'d��vacuation' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'evacuation_radius_units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'rayon d��vacuation' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'evacuation_radius_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Op�rateur de '||CHR(13)||CHR(10)
||'point d��clair' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'flashpoint_operator';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de '||CHR(13)||CHR(10)
||'point d��clair' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'flashpoint_units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Point '||CHR(13)||CHR(10)
||'d��clair' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'flashpont';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'time_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'densit� de vapeur' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'vapor_density_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'VOC�s - maximum of effectief' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'voc_high';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'VOC�s - minimum' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'voc_low';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'VOC' WHERE TABLE_NAME = 'msds_h_data' AND FIELD_NAME = 'voc_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_haz_classification' AND FIELD_NAME = 'date_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_haz_classification' AND FIELD_NAME = 'time_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'date_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Rayon d��vacuation '||CHR(13)||CHR(10)
||'sugg�r�' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'evacuation_radius';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de rayon '||CHR(13)||CHR(10)
||'d��vacuation' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'evacuation_radius_units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'rayon d��vacuation' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'evacuation_radius_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'de pression' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'pressure_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'quantit�' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'quantity_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�archivage' WHERE TABLE_NAME = 'msds_h_location' AND FIELD_NAME = 'time_archived';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Rayon d��vacuation '||CHR(13)||CHR(10)
||'sugg�r�' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'evacuation_radius';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Unit�s de rayon '||CHR(13)||CHR(10)
||'d��vacuation' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'evacuation_radius_units';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'rayon d��vacuation' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'evacuation_radius_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'de pression' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'pressure_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'quantit�' WHERE TABLE_NAME = 'msds_location' AND FIELD_NAME = 'quantity_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� d�emplacement '||CHR(13)||CHR(10)
||'num�rot�e automatiquement', SL_HEADING_FR = 'Cl� d�emplacement num�rot�e automatiquement' WHERE TABLE_NAME = 'msds_location_sync' AND FIELD_NAME = 'location_auto_number';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par'||CHR(13)||CHR(10)
||'l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'msds_location_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s '||CHR(13)||CHR(10)
||'de pression' WHERE TABLE_NAME = 'msds_location_sync' AND FIELD_NAME = 'pressure_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�unit�s de '||CHR(13)||CHR(10)
||'quantit�' WHERE TABLE_NAME = 'msds_location_sync' AND FIELD_NAME = 'quantity_units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'ndport' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Syst�me d�Exploitation'||CHR(13)||CHR(10)
||'du R�seau' WHERE TABLE_NAME = 'net' AND FIELD_NAME = 'net_os_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l��l�ment r�seau' WHERE TABLE_NAME = 'netdev' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'netdev' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'l��l�ment r�seau' WHERE TABLE_NAME = 'netdev' AND FIELD_NAME = 'netdev_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��l�ment'||CHR(13)||CHR(10)
||'r�seau' WHERE TABLE_NAME = 'netdev' AND FIELD_NAME = 'netdev_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Prix'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'netdevstd' AND FIELD_NAME = 'cost_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description du std'||CHR(13)||CHR(10)
||'d��l�ment r�seau' WHERE TABLE_NAME = 'netdevstd' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Graphique de'||CHR(13)||CHR(10)
||'l��l�ment r�seau' WHERE TABLE_NAME = 'netdevstd' AND FIELD_NAME = 'image_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Multiplexing?' WHERE TABLE_NAME = 'netdevstd' AND FIELD_NAME = 'is_multiplexing';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'l��l�ment r�seau' WHERE TABLE_NAME = 'netdevstd' AND FIELD_NAME = 'netdev_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��l�ment'||CHR(13)||CHR(10)
||'r�seau' WHERE TABLE_NAME = 'netdevstd' AND FIELD_NAME = 'netdev_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�envoi d�finie pour la notification' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'date_notify';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�envoi de la notification' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'date_sent';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl� Valeur d�indicateur' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'metric_value_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure d�envoi d�finie pour la notification' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'time_notify';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure d�envoi de la notification' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'time_sent';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Consulter'||CHR(13)||CHR(10)
||'hyperlien' WHERE TABLE_NAME = 'notifications' AND FIELD_NAME = 'view_url';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl�'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'activity_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Dur�e d�alerte'||CHR(13)||CHR(10)
||'(jours)' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'alert_duration_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'de dur�e d�alerte' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'alert_duration_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code interne'||CHR(13)||CHR(10)
||'de l�application' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'internal_code';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ligne d�objet de la'||CHR(13)||CHR(10)
||'notification' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'notify_subject';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID de l�objet de la'||CHR(13)||CHR(10)
||'notification' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'notify_subject_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Condition de d�clenchement'||CHR(13)||CHR(10)
||'modifi�e jusqu�au' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'trigger_condition_to';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Benachrichtigungs -'||CHR(13)||CHR(10)
||'Triggervorlaufssequenz' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'trigger_lead_seq';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Campo dell�ora'||CHR(13)||CHR(10)
||'di attivazione notifica', ML_HEADING_FR = 'Champ d�heure de'||CHR(13)||CHR(10)
||'d�clenchement de la notification' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'trigger_time_field';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Consulter'||CHR(13)||CHR(10)
||'hyperlien' WHERE TABLE_NAME = 'notify_templates' AND FIELD_NAME = 'view_url';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cl�'||CHR(13)||CHR(10)
||'de l�Application' WHERE TABLE_NAME = 'notifycat' AND FIELD_NAME = 'activity_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� - ��', ML_HEADING_ES = 'Coste - Estimado', ML_HEADING_IT = 'Costo - Stimato', ML_HEADING_NL = 'Kosten - geschat', ML_HEADING_FR = 'Co�t - Estim�', ML_HEADING_DE = 'Kosten - Gesch�tzt' WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'cost_est';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�exercice' WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'date_exercised';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�exercice'||CHR(13)||CHR(10)
||'applicable' WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'date_exercising_applicable';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date � laquelle'||CHR(13)||CHR(10)
||'l�option expire' WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'date_option';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Les dates d�option'||CHR(13)||CHR(10)
||'correspondent-elles � la location�?' WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'dates_match_lease';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'N/A;�;AUTOMATIC TRANSFER OWNERSHIP;��������;CONTRACTION;���;EARLY TERMINATION;����;EXPANSION;��;EXTENSION;��;OPTION TO BUY;����;RENEWAL;��;RENEW LONGER TERM;�������;OPTION TO SUBLET;����;IMPROVEMENTS;��;REASSIGNMENT;���;LEASE END;����', ENUM_LIST_DE = 'N/A;NICHT VERF�GBAR;AUTOMATIC TRANSFER OWNERSHIP;AUTOMATISCHE EIGENTUMS�BERTRAGUNG;CONTRACTION;WIDERSPRUCH;EARLY TERMINATION;FR�HZEITIGE K�NDIGUNG;EXPANSION;ERWEITERUNG;EXTENSION;VERL�NGERUNG;OPTION TO BUY;KAUFOPTION;RENEWAL;ERNEUERUNG;RENEW LONGER TERM;ERNEUERUNG MIT L�NGERER LAUFZEIT;OPTION TO SUBLET;UNTERVERMIETUNGSOPTION;IMPROVEMENTS;VERBESSERUNGEN;REASSIGNMENT;NEUORDNUNG;LEASE END;MIETVERTRAGSENDE', ENUM_LIST_ES = 'N/A;N/A;AUTOMATIC TRANSFER OWNERSHIP;TRANSFERENCIA AUTOM�TICA DE PROPIEDAD;CONTRACTION;CONTRACCI�N;EARLY TERMINATION;TERMINACI�N TEMPRANA;EXPANSION;EXPANSI�N;EXTENSION;PROLONGACI�N;OPTION TO BUY;OPCI�N DE COMPRA;RENEWAL;RENOVACI�N;RENEW LONGER TERM;RENOVAR PERIODO M�S LARGO;OPTION TO SUBLET;OPCI�N DE SUBARRENDAR;IMPROVEMENTS;MEJORAS;REASSIGNMENT;REASIGNACI�N;LEASE END;FIN DE ARRENDAMIENTO', ENUM_LIST_FR = 'N/A;S/O;AUTOMATIC TRANSFER OWNERSHIP;TRANSFERT DE PROPRIETE AUTOMATIQUE;CONTRACTION;CONTRACTION;EARLY TERMINATION;RESILIATION ANTICIPEE;EXPANSION;EXPANSION;EXTENSION;EXTENSION;OPTION TO BUY;OPTION D�ACHAT;RENEWAL;RENOUVELLEMENT;RENEW LONGER TERM;RENOUV. DUREE ALLONGEE;OPTION TO SUBLET;OPTION DE SOUS-LOC.;IMPROVEMENTS;AMELIORATIONS;REASSIGNMENT;REAFFECTATION;LEASE END;FIN DE LOCATION', ENUM_LIST_IT = 'N/A;N/D;AUTOMATIC TRANSFER OWNERSHIP;TRASFERIMENTO AUTOMATICO DI PROPRIET�;CONTRACTION;CONTRAZIONE;EARLY TERMINATION;CESSAZIONE ANTICIPATA;EXPANSION;ESPANSIONE;EXTENSION;ESTENSIONE;OPTION TO BUY;OPZIONE DI ACQUISTO;RENEWAL;RINNOVO;RENEW LONGER TERM;RINNOVO TERMINE PI� LUNGO;OPTION TO SUBLET;OPZIONE PER SUBAFFITTO;IMPROVEMENTS;MIGLIORIE;REASSIGNMENT;RIASSEGNAZIONE;LEASE END;FINE LOCAZIONE', ML_HEADING_CH = '����', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�option', ENUM_LIST_NL = 'N/A;N.V.T.;AUTOMATIC TRANSFER OWNERSHIP;AUTOMATISCHE OVERDRACHT VAN EIGENDOM;CONTRACTION;VERKORTING;EARLY TERMINATION;VERVROEGDE OPZEGGING.;EXPANSION;UITBREIDING;EXTENSION;VERLENGING;OPTION TO BUY;HUURKOOP;RENEWAL;VERLENGING;RENEW LONGER TERM;VERLENGEN VOOR LANGERE TERMIJN;OPTION TO SUBLET;OPTIE VOOR ONDERHUUR;IMPROVEMENTS;VERBETERINGEN;REASSIGNMENT;NIEUWE TOEWIJZING;LEASE END;EINDE LEASE' WHERE TABLE_NAME = 'op' AND FIELD_NAME = 'op_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de'||CHR(13)||CHR(10)
||'l�Organisation' WHERE TABLE_NAME = 'org' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Organisation' WHERE TABLE_NAME = 'org' AND FIELD_NAME = 'org_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Prix'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'ot' AND FIELD_NAME = 'cost_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'ot' AND FIELD_NAME = 'date_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'Pipeline;��;UNDER CONTRACT;���;ESCROWED;����;IN SERVICE;����;Owned;��;OWNED AND LEASED;�����;LEASED;��;SUB-LEASED;��;SUB LET;��;FOR SALE;��;LEASED (EXPIRED);��(��);OUT OF SERVICE;����;ABANDONED;��;DONATED;��;Disposed;��;SOLD;��;UNKNOWN;��;N/A;N/A;To be disposed;���', ENUM_LIST_DE = 'Pipeline;In Vorbereitung;UNDER CONTRACT;Unter Vertrag;ESCROWED;Treuhand;IN SERVICE;In Betrieb;Owned;Firmeneigen;OWNED AND LEASED;Firmeneigen und gemietet;LEASED;Gemietet;SUB-LEASED;Untergemietet;SUB LET;Untervermietet;FOR SALE;Zum Verkauf;LEASED (EXPIRED);Gemietet (ausgelaufen);OUT OF SERVICE;Au�er Betrieb;ABANDONED;Aufgegeben;DONATED;Gespendet;Disposed;Entsorgt;SOLD;Verkauft;UNKNOWN;Unbekannt;N/A;Entf�llt;To be disposed;Zu entsorgen', ENUM_LIST_ES = 'Pipeline;En desarrollo;UNDER CONTRACT;Con contrato;ESCROWED;En fideicomiso;IN SERVICE;En servicio;Owned;En propiedad;OWNED AND LEASED;En propiedad y arrendado;LEASED;Arrendado;SUB-LEASED;Subarrendado;SUB LET;Subalquilado;FOR SALE;En venta;LEASED (EXPIRED);Arrendado (vencido);OUT OF SERVICE;Fuera de servicio;ABANDONED;Abandonado;DONATED;Donado;Disposed;Eliminado;SOLD;Vendido;UNKNOWN;Desconocido;N/A;N/A;To be disposed;Para ser eliminado', ENUM_LIST_FR = 'Pipeline;En cours d�acquisition;UNDER CONTRACT;Sous contrat;ESCROWED;D�p�t l�gal;IN SERVICE;En service;Owned;Poss�d�;OWNED AND LEASED;Poss�d� et lou�;LEASED;Lou�;SUB-LEASED;Sous-lou�;SUB LET;Sous-lou�;FOR SALE;A vendre;LEASED (EXPIRED);Lou� (expir�);OUT OF SERVICE;Hors service;ABANDONED;Abandonn�;DONATED;Donn�;Disposed;Elimin�;SOLD;Vendu;UNKNOWN;Inconnu;N/A;S/O;To be disposed;A �liminer', 
ENUM_LIST_IT = 'Pipeline;In fase di acquisto;UNDER CONTRACT;Contratto in corso;ESCROWED;In custodia;IN SERVICE;In servizio;Owned;Di propriet�;OWNED AND LEASED;Di propriet� e locazione;LEASED;Locazione;SUB-LEASED;Sublocazione;SUB LET;Subaffitto;FOR SALE;In vendita;LEASED (EXPIRED);Locazione (scaduto);OUT OF SERVICE;Fuori servizio;ABANDONED;Abbandonato;DONATED;Donato;Disposed;Smaltito;SOLD;Venduto;UNKNOWN;Sconosciuto;N/A;N/D;To be disposed;Da smaltire', ENUM_LIST_NL = 'Pipeline;Pijplijn;UNDER CONTRACT;Onder contract;ESCROWED;In depot;IN SERVICE;In bedrijf;Owned;In eigendom;OWNED AND LEASED;In eigendom en verhuurd;LEASED;Verhuurd;SUB-LEASED;Onderverhuurd;SUB LET;Onderverhuur;FOR SALE;Te koop;LEASED (EXPIRED);Verhuurd (vervallen);OUT OF SERVICE;Buiten bedrijf;ABANDONED;Verlaten;DONATED;Geschonken;Disposed;Afgevoerd;SOLD;Verkocht;UNKNOWN;Onbekend;N/A;N.v.t.;To be disposed;Af te voeren' WHERE TABLE_NAME = 'ot' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description Type'||CHR(13)||CHR(10)
||'de l�Autre Ressource' WHERE TABLE_NAME = 'other_rs' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type de l�Autre'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'other_rs' AND FIELD_NAME = 'other_rs_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Actif' WHERE TABLE_NAME = 'pa' AND FIELD_NAME = 'pa_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur'||CHR(13)||CHR(10)
||'r�siduelle' WHERE TABLE_NAME = 'pa' AND FIELD_NAME = 'value_salvage';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Actif' WHERE TABLE_NAME = 'pa_dep' AND FIELD_NAME = 'pa_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Dotation'||CHR(13)||CHR(10)
||'d�Amortissement' WHERE TABLE_NAME = 'pa_dep' AND FIELD_NAME = 'value_current_dep';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'parcel' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'parcel' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Propri�taire d�enregistrement' WHERE TABLE_NAME = 'parcel' AND FIELD_NAME = 'owner';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'parking' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'parking' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'pb' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro de'||CHR(13)||CHR(10)
||'l��tag�re' WHERE TABLE_NAME = 'pb' AND FIELD_NAME = 'shelf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�affichage' WHERE TABLE_NAME = 'plantype_groups' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'one_pms;Un planning de MP par bon de travaux;eq_id;Code �quipement;eq_subcomponent;Composants d��quipement;eq_std;Standard de l��quipement;site_id;Sites;bl_id;B�timent;fl_id;Etage;rm_id;Pi�ce;tr_id;M�tier principal;pmp_id;Proc�dure MP', ML_HEADING_NL = 'PO'||CHR(13)||CHR(10)
||'schema�s groeperen op' WHERE TABLE_NAME = 'pmgen' AND FIELD_NAME = 'group_param';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����' WHERE TABLE_NAME = 'pmgen' AND FIELD_NAME = 'pm_group';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'EQPM;Maint. pr�v. de l��quipement;HSPM;Maint. pr�v. de la localisation' WHERE TABLE_NAME = 'pmgen' AND FIELD_NAME = 'pm_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����' WHERE TABLE_NAME = 'pmgp' AND FIELD_NAME = 'pm_group';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'pmp' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Document'||CHR(13)||CHR(10)
||'d��tape de la proc�dure' WHERE TABLE_NAME = 'pmps' AND FIELD_NAME = 'doc';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'L;Main d�oeuvre;P;Pi�ces d�t.;T;Types d�outils' WHERE TABLE_NAME = 'pmressum' AND FIELD_NAME = 'resource_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'H�ufigkeit 1'||CHR(13)||CHR(10)
||'Intervall' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'interval_1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'H�ufigkeit 2'||CHR(13)||CHR(10)
||'Intervall' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'interval_2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'H�ufigkeit 3'||CHR(13)||CHR(10)
||'Intervall' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'interval_3';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'H�ufigkeit 4'||CHR(13)||CHR(10)
||'Intervall' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'interval_4';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'r;����;d;�;ww;�;m;�;q;�;yyyy;�;i;��;h;��;e;�;a;���', ENUM_LIST_DE = 'r;Wiederholungsmuster;d;Tage;ww;Wochen;m;Monate;q;Quartale;yyyy;Jahre;i;Meilen;h;Stunden;e;Z�hler;a;Manuell', ENUM_LIST_ES = 'r;Patr�n de periodicidad;d;D�as;ww;Semanas;m;Meses;q;Trimestres;yyyy;A�os;i;Millas;h;Horas;e;Contador;a;Manual', ENUM_LIST_FR = 'r;Motif de r�currence;d;Jours;ww;Semaines;m;Mois;q;Trimestres;yyyy;Ann�es;i;Miles;h;Heures;e;Compteur;a;Manuel', ENUM_LIST_IT = 'r;Pattern periodicit�;d;Giorni;ww;Settimane;m;Mesi;q;Trimestri;yyyy;Anni;i;Miglia;h;Ore;e;Metri;a;Manuale', ML_HEADING_CH = '����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'intervalo', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di intervallo', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�intervalle', ML_HEADING_DE = 'Intervalltyp', ENUM_LIST_NL = 'r;Herhalingspatroon;d;Dagen;ww;Weken;m;Maanden;q;Kwartalen;yyyy;Jaren;i;Mijlen;h;Uren;e;Meter;a;Handmatig' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'interval_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'pm_group';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nombre Total'||CHR(13)||CHR(10)
||'d�Unit�s' WHERE TABLE_NAME = 'pms' AND FIELD_NAME = 'total_unit';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'pn' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro de'||CHR(13)||CHR(10)
||'l��tag�re' WHERE TABLE_NAME = 'pn' AND FIELD_NAME = 'shelf_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Port d��quipement destinataire' WHERE TABLE_NAME = 'pnport' AND FIELD_NAME = 'tc_eqport_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'pnport' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Signature de'||CHR(13)||CHR(10)
||'l�approbateur' WHERE TABLE_NAME = 'po' AND FIELD_NAME = 'approver_signature';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�approbation' WHERE TABLE_NAME = 'po' AND FIELD_NAME = 'date_approved';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Frais'||CHR(13)||CHR(10)
||'d�Exp�dition' WHERE TABLE_NAME = 'po' AND FIELD_NAME = 'shipping';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'po_line' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Riga ordine'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'N� Ligne du'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'po_line' AND FIELD_NAME = 'po_line_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Assureur' WHERE TABLE_NAME = 'policy' AND FIELD_NAME = 'insurer_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Police'||CHR(13)||CHR(10)
||'d�Assurance' WHERE TABLE_NAME = 'policy' AND FIELD_NAME = 'policy_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Utilisation' WHERE TABLE_NAME = 'port' AND FIELD_NAME = 'tc_use_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'portcfg' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'l��l�ment r�seau' WHERE TABLE_NAME = 'portcfg' AND FIELD_NAME = 'netdev_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Numero '||CHR(13)||CHR(10)
||'d�inizio' WHERE TABLE_NAME = 'portcfg' AND FIELD_NAME = 'start_number';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_DE = '0;Keine;bu;Unternehmenseinheiten;dv;Bereiche;dp;Abteilungen;fg;Funktionsgruppen' WHERE TABLE_NAME = 'portfolio_scenario' AND FIELD_NAME = 'scn_level';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 1' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'apprv_mgr1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Approbation '||CHR(13)||CHR(10)
||'du Responsable 1' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'apprv_mgr1_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 2' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'apprv_mgr2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Approbation '||CHR(13)||CHR(10)
||'du Responsable 2' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'apprv_mgr2_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Responsable'||CHR(13)||CHR(10)
||'d�Approbation 3' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'apprv_mgr3';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de l�Approbation '||CHR(13)||CHR(10)
||'du Responsable 3' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'apprv_mgr3_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation'||CHR(13)||CHR(10)
||'responsable�1' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'date_app_mgr1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation'||CHR(13)||CHR(10)
||'responsable�2' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'date_app_mgr2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�approbation'||CHR(13)||CHR(10)
||'responsable�3' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'date_app_mgr3';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Document - '||CHR(13)||CHR(10)
||'Transfert d�actions' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'doc_acts_xfer';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fiche d��valuation de projet' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'doc_scorecard';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_IT = 'Created;Creato;Proposed;Proposto;Requested;Richiesto;Requested-Estimated;Richiesto-Stimato;Requested-On Hold;Richiesto-In attesa;Requested-Routed;Richiesto-Instradato per l�approvazione;Requested-Rejected;Richiesto-Rifiutato;Approved;Approvato;Approved-In Design;Approvato-In progettazione;Approved-Cancelled;Approvato-Annullato;Issued-In Process;Emesso-In elaborazione;Issued-On Hold;Emesso-In attesa;Issued-Stopped;Emesso-Interrotto;Completed-Pending;Saldato-Azioni in sospeso;Completed-Not Ver;Saldato-Non verificato;Completed-Verified;Saldato-Verificato;Closed;Chiuso' WHERE TABLE_NAME = 'project' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Photo du membre'||CHR(13)||CHR(10)
||'de l��quipe' WHERE TABLE_NAME = 'projteam' AND FIELD_NAME = 'image_file';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code membre '||CHR(13)||CHR(10)
||'de l��quipe' WHERE TABLE_NAME = 'projteam' AND FIELD_NAME = 'member_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�le du membre'||CHR(13)||CHR(10)
||'de l��quipe' WHERE TABLE_NAME = 'projteam' AND FIELD_NAME = 'member_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut du membre  '||CHR(13)||CHR(10)
||'de l��quipe' WHERE TABLE_NAME = 'projteam' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Distance de'||CHR(13)||CHR(10)
||'l�A�roport' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'air_dist';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�A�roport' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'air_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires sur l��limination' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'comment_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Prix'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'cost_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d��limination' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'date_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'date_purchase';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��limination' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'disposal_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Distance �'||CHR(13)||CHR(10)
||'l�Autoroute' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'int_dist';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Autoroute' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'int_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'En attente d�action' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'pending_action';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'PIPELINE;��;UNDER CONTRACT;���;ESCROWED;����;IN SERVICE;����;OWNED;��;OWNED AND LEASED;�����;LEASED;��;SUB-LEASED;��;SUB LET;��;FOR SALE;��;LEASED (EXPIRED);��(��);OUT OF SERVICE;����;ABANDONED;��;DONATED;��;DISPOSED;��;SOLD;��;N/A;N/A;UNKNOWN;��;todi;���', ENUM_LIST_DE = 'PIPELINE;In Vorbereitung;UNDER CONTRACT;Unter Vertrag;ESCROWED;Treuhand;IN SERVICE;In Betrieb;OWNED;Firmeneigen;OWNED AND LEASED;Firmeneigen und gemietet;LEASED;Gemietet;SUB-LEASED;Untergemietet;SUB LET;Untervermietet;FOR SALE;Zum Verkauf;LEASED (EXPIRED);Gemietet (ausgelaufen);OUT OF SERVICE;Au�er Betrieb;ABANDONED;Aufgegeben;DONATED;Gespendet;DISPOSED;Entsorgt;SOLD;Verkauft;N/A;Entf�llt;UNKNOWN;Unbekannt;todi;Zu entsorgen', ENUM_LIST_ES = 'PIPELINE;En desarrollo;UNDER CONTRACT;Con contrato;ESCROWED;En fideicomiso;IN SERVICE;En servicio;OWNED;En propiedad;OWNED AND LEASED;En propiedad y arrendado;LEASED;Arrendado;SUB-LEASED;Subarrendado;SUB LET;Subalquilado;FOR SALE;En venta;LEASED (EXPIRED);Arrendado (vencido);OUT OF SERVICE;Fuera de servicio;ABANDONED;Abandonado;DONATED;Donado;DISPOSED;Eliminado;SOLD;Vendido;N/A;N/A;UNKNOWN;Desconocido;todi;Para ser eliminado', ENUM_LIST_FR = 'PIPELINE;En cours d�acquisition;UNDER CONTRACT;Sous contrat;ESCROWED;D�p�t l�gal;IN SERVICE;En service;OWNED;Poss�d�;OWNED AND LEASED;Poss�d� et lou�;LEASED;Lou�;SUB-LEASED;Sous-lou�;SUB LET;Sous-lou�;FOR SALE;A vendre;LEASED (EXPIRED);Lou� (expir�);OUT OF SERVICE;Hors service;ABANDONED;Abandonn�;DONATED;Donn�;DISPOSED;Elimin�;SOLD;Vendu;N/A;S/O;UNKNOWN;Inconnu;todi;A �liminer', 
ENUM_LIST_IT = 'PIPELINE;In fase di acquisto;UNDER CONTRACT;Contratto in corso;ESCROWED;In custodia;IN SERVICE;In servizio;OWNED;Di propriet�;OWNED AND LEASED;Di propriet� e locazione;LEASED;Locazione;SUB-LEASED;Sublocazione;SUB LET;Subaffitto;FOR SALE;In vendita;LEASED (EXPIRED);Locazione (scaduto);OUT OF SERVICE;Fuori servizio;ABANDONED;Abbandonato;DONATED;Donato;DISPOSED;Smaltito;SOLD;Venduto;N/A;N/D;UNKNOWN;Sconosciuto;todi;Da smaltire', ENUM_LIST_NL = 'PIPELINE;Pijplijn;UNDER CONTRACT;Onder contract;ESCROWED;In depot;IN SERVICE;In bedrijf;OWNED;In eigendom;OWNED AND LEASED;In eigendom en verhuurd;LEASED;Verhuurd;SUB-LEASED;Onderverhuurd;SUB LET;Onderverhuur;FOR SALE;Te koop;LEASED (EXPIRED);Verhuurd (vervallen);OUT OF SERVICE;Buiten bedrijf;ABANDONED;Verlaten;DONATED;Geschonken;DISPOSED;Afgevoerd;SOLD;Verkocht;N/A;N.v.t.;UNKNOWN;Onbekend;todi;Af te voeren' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Destinazione d�uso'||CHR(13)||CHR(10)
||'zona' WHERE TABLE_NAME = 'property' AND FIELD_NAME = 'zoning';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'M�thode'||CHR(13)||CHR(10)
||'d�Amortissement' WHERE TABLE_NAME = 'property_type' AND FIELD_NAME = 'deprec_method';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Stock� �'||CHR(13)||CHR(10)
||'l��tage' WHERE TABLE_NAME = 'pt' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Unit� '||CHR(13)||CHR(10)
||'per l�ordine' WHERE TABLE_NAME = 'pt' AND FIELD_NAME = 'units_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�actif'||CHR(13)||CHR(10)
||'mobile' WHERE TABLE_NAME = 'pt_store_loc' AND FIELD_NAME = 'eq_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'pt_store_loc' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'pt_store_loc_pt' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard' WHERE TABLE_NAME = 'questionnaire_map' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�ponse de'||CHR(13)||CHR(10)
||'l�Action' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'action_response';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Action' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'activity_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste'||CHR(13)||CHR(10)
||'d��num�ration' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Langue 01)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_01';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Langue 02)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_02';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Langue 03)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_03';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Chinois - Simplifi�)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_ch';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Allemand)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_de';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Espagnol)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_es';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Fran�ais)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_fr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Italien)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_it';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Japonais)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_jp';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Cor�en)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_ko';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(N�erlandais)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_nl';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Norv�gien)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_no';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Liste d��num�ration'||CHR(13)||CHR(10)
||'(Chinois - Traditionnel)' WHERE TABLE_NAME = 'questions' AND FIELD_NAME = 'enum_list_zh';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'rack' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'recovery_team' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'HEADCOUNT;Membre de l��quipe;ESCALATE1;Premier contact hi�rarchique;ESCALATE2;Second contact hi�rarchique;ESCALATE3;Troisi�me contact hi�rarchique', ML_HEADING_FR = 'R�le dans'||CHR(13)||CHR(10)
||'l��quipe de GSU' WHERE TABLE_NAME = 'recovery_team' AND FIELD_NAME = 'role';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'regcompliance' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'N� d��l�ment de'||CHR(13)||CHR(10)
||'r�glementation' WHERE TABLE_NAME = 'regcompliance' AND FIELD_NAME = 'regcomp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Giorni di preavviso'||CHR(13)||CHR(10)
||'per la programmazione dell�evento', ML_HEADING_FR = 'D�calage de'||CHR(13)||CHR(10)
||'l��ch�ancier (jours)' WHERE TABLE_NAME = 'regloc' AND FIELD_NAME = 'event_offset';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'regloc' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regloc' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'regn' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� Attivo?' WHERE TABLE_NAME = 'regnotify' AND FIELD_NAME = 'is_active';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regnotify' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Motivazione'||CHR(13)||CHR(10)
||'stato �In Sospeso�' WHERE TABLE_NAME = 'regprogram' AND FIELD_NAME = 'hold_reason';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regreq_pmp' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Mise � jour auto'||CHR(13)||CHR(10)
||'de l��tat du cas�?' WHERE TABLE_NAME = 'regreq_pmp' AND FIELD_NAME = 'update_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de cat�gorie'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regreqcat' AND FIELD_NAME = 'regreq_cat';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'citation';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fin de'||CHR(13)||CHR(10)
||'date d�exigibilit�' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'date_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de d�but'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Marge de'||CHR(13)||CHR(10)
||'l��ch�ancier (jours)' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'event_sched_buffer';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Motivazione'||CHR(13)||CHR(10)
||'stato �In Sospeso�' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'hold_reason';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Priorit�'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'priority';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'regreq_cat';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'Other;��;Abatement;��;Assessment;��;Audit;��;Corrective Action;����;Documentation;��;Emergency Response;����;Inspection;��;License;���;Permit;��;Maintenance;��;Measurement;��;Medical Monitoring;����;Monitoring;��;Other Action;����;Reporting;��;Sampling;��;Training;��', ENUM_LIST_FR = 'Other;Autre;Abatement;Elimination;Assessment;Evaluation/Correction;Audit;Audit;Corrective Action;Action corrective;Documentation;Documentation;Emergency Response;Intervention d�urgence;Inspection;Inspection;License;Licence;Permit;Permis;Maintenance;Maintenance;Measurement;Mesure;Medical Monitoring;Suivi m�dical;Monitoring;Surveillance;Other Action;Autre action;Reporting;Etablissement de rapport;Sampling;Echantillonnage;Training;Formation', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'regreq_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Etat'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regrequirement' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��' WHERE TABLE_NAME = 'regulation' AND FIELD_NAME = 'citation';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����' WHERE TABLE_NAME = 'regulation' AND FIELD_NAME = 'date_compliance';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'regulation' AND FIELD_NAME = 'legal_refs';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'regulation' AND FIELD_NAME = 'related_reg';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'regulation' AND FIELD_NAME = 'web_ref1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� - ��/��'||CHR(13)||CHR(10)
||'(��)', ML_HEADING_ES = 'Coste - Penalizaci�n/sanciones'||CHR(13)||CHR(10)
||'(presupuesto)', ML_HEADING_IT = 'Costo - Sanzione/Multe'||CHR(13)||CHR(10)
||'(budget)', ML_HEADING_NL = 'Kosten - Straf/boetes'||CHR(13)||CHR(10)
||'(budget)', ML_HEADING_FR = 'Co�t - P�nalit�/Amende'||CHR(13)||CHR(10)
||'(Budget)', ML_HEADING_DE = 'Kosten - Vertragsstrafe/Geldstrafe'||CHR(13)||CHR(10)
||'(Budget)' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�infraction du' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'date_from';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�infraction au' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'date_to';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�emplacement' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'location_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�exigence' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'reg_requirement';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Gravit�'||CHR(13)||CHR(10)
||'de l�infraction' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'severity';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Etat'||CHR(13)||CHR(10)
||'de l�infraction' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�infraction' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'violation_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||'d�infraction' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'violation_num';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code du'||CHR(13)||CHR(10)
||'type d�infraction' WHERE TABLE_NAME = 'regviolation' AND FIELD_NAME = 'violation_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code du'||CHR(13)||CHR(10)
||'type d�infraction' WHERE TABLE_NAME = 'regviolationtyp' AND FIELD_NAME = 'violation_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Type d�Outil' WHERE TABLE_NAME = 'resavail' AND FIELD_NAME = 'tool_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Annulation' WHERE TABLE_NAME = 'reserve' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Document'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'reserve' AND FIELD_NAME = 'doc_event';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Indice'||CHR(13)||CHR(10)
||'d�occurrence' WHERE TABLE_NAME = 'reserve' AND FIELD_NAME = 'occurrence_index';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�;Room Conflict;Conflit de pi�ces' WHERE TABLE_NAME = 'reserve' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Annulation' WHERE TABLE_NAME = 'reserve_rm' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�am�nagement'||CHR(13)||CHR(10)
||'de pi�ce' WHERE TABLE_NAME = 'reserve_rm' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�' WHERE TABLE_NAME = 'reserve_rm' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Annulation' WHERE TABLE_NAME = 'reserve_rs' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�' WHERE TABLE_NAME = 'reserve_rs' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Poste comptable interne'||CHR(13)||CHR(10)
||'pour l��tablissement du budget' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Azione se il tempo'||CHR(13)||CHR(10)
||'per l�approvazione � scaduto', ML_HEADING_FR = 'Action si la dur�e d�approbation'||CHR(13)||CHR(10)
||'a expir�' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'action_approval_expired';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Annonce'||CHR(13)||CHR(10)
||'Nbre de jours � l�avance' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'announce_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure de'||CHR(13)||CHR(10)
||'l�Annonce' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'announce_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'N. di giorni '||CHR(13)||CHR(10)
||'all�approvazione', ML_HEADING_FR = 'Nbre de jours r�serv�s'||CHR(13)||CHR(10)
||'� l�approbation' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'approve_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Annulation -'||CHR(13)||CHR(10)
||'Nbre de Jours � l�Avance' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'cancel_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de revient'||CHR(13)||CHR(10)
||'� l�Unit� par' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'cost_unit';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Gruppo autorizzato'||CHR(13)||CHR(10)
||'all�approvazione' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'group_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nbre maximum de'||CHR(13)||CHR(10)
||'jours d�avance' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'max_days_ahead';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'L�utilisateur doit notifier si'||CHR(13)||CHR(10)
||'d�lai d�approbation a expir�' WHERE TABLE_NAME = 'resources' AND FIELD_NAME = 'user_approval_expired';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de reserva'||CHR(13)||CHR(10)
||'de espacio', ML_HEADING_FR = 'Co�t de r�servation'||CHR(13)||CHR(10)
||'des pi�ces' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'cost_rmres';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'cancelaci�n', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�annulation', ML_HEADING_DE = 'Termin'||CHR(13)||CHR(10)
||'storniert' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha'||CHR(13)||CHR(10)
||'de creaci�n', ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'di creazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'Erstellt' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de derni�re'||CHR(13)||CHR(10)
||'mise � jour', ML_HEADING_DE = 'Zuletzt ge�ndert'||CHR(13)||CHR(10)
||'am' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'date_last_modified';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'rechazo', ML_HEADING_FR = 'Date de'||CHR(13)||CHR(10)
||'rejet', ML_HEADING_DE = 'Termin'||CHR(13)||CHR(10)
||'abgelehnt' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'date_rejected';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '# ����', ML_HEADING_NL = 'aantal externe'||CHR(13)||CHR(10)
||'gasten', ML_HEADING_DE = 'Anzahl'||CHR(13)||CHR(10)
||'externe G�ste' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'guests_external';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '# ����', ML_HEADING_NL = 'aantal interne'||CHR(13)||CHR(10)
||'Gasten', ML_HEADING_DE = 'Anzahl'||CHR(13)||CHR(10)
||'interne G�ste' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'guests_internal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Terugkerende'||CHR(13)||CHR(10)
||'order', ML_HEADING_FR = 'Bon'||CHR(13)||CHR(10)
||'r�current' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'recurring_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de espacio', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'ruimtereservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de pi�ce', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Raumreservierung' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'rmres_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de pi�ce' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'Modificado por'||CHR(13)||CHR(10)
||'�ltima vez por', ML_HEADING_FR = 'Derni�re'||CHR(13)||CHR(10)
||'modification par' WHERE TABLE_NAME = 'resrmview' AND FIELD_NAME = 'user_last_modified_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de reserva'||CHR(13)||CHR(10)
||'de recurso', ML_HEADING_FR = 'Co�t de r�servation'||CHR(13)||CHR(10)
||'des ressources' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'cost_rsres';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'cancelaci�n', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�annulation', ML_HEADING_DE = 'Termin'||CHR(13)||CHR(10)
||'storniert' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha'||CHR(13)||CHR(10)
||'de creaci�n', ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'di creazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'Erstellt' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de derni�re'||CHR(13)||CHR(10)
||'mise � jour', ML_HEADING_DE = 'Zuletzt ge�ndert'||CHR(13)||CHR(10)
||'am' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'date_last_modified';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'rechazo', ML_HEADING_FR = 'Date de'||CHR(13)||CHR(10)
||'rejet', ML_HEADING_DE = 'Termin'||CHR(13)||CHR(10)
||'abgelehnt' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'date_rejected';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Aangevraagde'||CHR(13)||CHR(10)
||'hoeveelheid', ML_HEADING_FR = 'Quantit�'||CHR(13)||CHR(10)
||'demand�e' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'quantity';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Terugkerende'||CHR(13)||CHR(10)
||'order', ML_HEADING_FR = 'Bon'||CHR(13)||CHR(10)
||'r�current' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'recurring_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de recurso', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'resource-reservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de ressource', ML_HEADING_DE = 'Ressourcenreservierungscode' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'rsres_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de ressource' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'Modificado por'||CHR(13)||CHR(10)
||'�ltima vez por', ML_HEADING_FR = 'Derni�re'||CHR(13)||CHR(10)
||'modification par' WHERE TABLE_NAME = 'resrsview' AND FIELD_NAME = 'user_last_modified_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_NL = 'Intern'||CHR(13)||CHR(10)
||'account', ML_HEADING_FR = 'Poste comptable'||CHR(13)||CHR(10)
||'interne' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��������', ML_HEADING_ES = 'Direcciones de correo'||CHR(13)||CHR(10)
||'electr�nico de los asistentes', ML_HEADING_FR = 'Adresses e-mail'||CHR(13)||CHR(10)
||'des participants' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'attendees';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Costo '||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_FR = 'Co�t de'||CHR(13)||CHR(10)
||'r�servation' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'cost_res';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha de'||CHR(13)||CHR(10)
||'cancelaci�n', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�annulation', ML_HEADING_DE = 'Termin'||CHR(13)||CHR(10)
||'storniert' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'date_cancelled';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Fecha'||CHR(13)||CHR(10)
||'de creaci�n', ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'di creazione', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de cr�ation', ML_HEADING_DE = 'Datum'||CHR(13)||CHR(10)
||'Erstellt' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'fine', ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Einde', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de fin', ML_HEADING_DE = 'Enddatum' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'date_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de derni�re'||CHR(13)||CHR(10)
||'mise � jour', ML_HEADING_DE = 'Zuletzt ge�ndert'||CHR(13)||CHR(10)
||'am' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'date_last_modified';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Documento'||CHR(13)||CHR(10)
||'de evento', ML_HEADING_NL = 'Event'||CHR(13)||CHR(10)
||'document', ML_HEADING_FR = 'Document'||CHR(13)||CHR(10)
||'d��v�nement' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'doc_event';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Afd. voor'||CHR(13)||CHR(10)
||'doorbelasting', ML_HEADING_FR = 'D�partement'||CHR(13)||CHR(10)
||'� imputer' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'���', ML_HEADING_NL = 'Divisie voor'||CHR(13)||CHR(10)
||'doorbelasting', ML_HEADING_FR = 'Division �'||CHR(13)||CHR(10)
||'imputer' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Correo electr�nico'||CHR(13)||CHR(10)
||'del solicitante', ML_HEADING_NL = 'E-mail'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'E-mail du'||CHR(13)||CHR(10)
||'demandeur' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'email';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'����', ML_HEADING_NL = 'Herhalende datum'||CHR(13)||CHR(10)
||'Individueel gewijzigd', ML_HEADING_FR = 'Date r�currente'||CHR(13)||CHR(10)
||'modifi�e individuellement' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'recurring_date_modified';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Terugkerende'||CHR(13)||CHR(10)
||'regel', ML_HEADING_FR = 'R�gle'||CHR(13)||CHR(10)
||'r�currente' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'recurring_rule';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'reserva padre', ML_HEADING_NL = 'Bovenliggende'||CHR(13)||CHR(10)
||'reserveringscode', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation parent' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'res_parent';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'r�servation' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'res_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'reserva', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungsname' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'reservation_name';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�;Room Conflict;Conflit de pi�ces' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'�', ML_HEADING_NL = 'Aangemaakt'||CHR(13)||CHR(10)
||'door' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'user_created_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����', ML_HEADING_ES = 'Modificado por'||CHR(13)||CHR(10)
||'�ltima vez por', ML_HEADING_FR = 'Derni�re'||CHR(13)||CHR(10)
||'modification par' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'user_last_modified_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'user_requested_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Aangevraagd'||CHR(13)||CHR(10)
||'voor', ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'resview' AND FIELD_NAME = 'user_requested_for';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'rf' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date '||CHR(13)||CHR(10)
||'d�activit�' WHERE TABLE_NAME = 'rf_activity' AND FIELD_NAME = 'date_activity';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'rf_activity' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure de '||CHR(13)||CHR(10)
||'l�activit�' WHERE TABLE_NAME = 'rf_activity' AND FIELD_NAME = 'time_activity';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'rf_reader' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'rm' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'rm' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_IT = 'NONE;Nessuno stato;NONE-REVIEW;Nessuno stato in esame;FIT-ONLINE;Adatto all�uso;FIT-OFFLINE;Adatto all�uso - Non in linea;UNFIT-TEMP;Inadatto all�uso - Recuperabile;UNFIT-PERM;Inadatto all�uso - Distrutto' WHERE TABLE_NAME = 'rm' AND FIELD_NAME = 'recovery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Poste comptable interne'||CHR(13)||CHR(10)
||'pour l��tablissement du budget' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Azione se il tempo'||CHR(13)||CHR(10)
||'per l�approvazione � scaduto', ML_HEADING_FR = 'Action si la dur�e d�approbation'||CHR(13)||CHR(10)
||'a expir�' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'action_approval_expired';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Annonce'||CHR(13)||CHR(10)
||'Nbre de jours � l�avance' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'announce_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure de'||CHR(13)||CHR(10)
||'l�Annonce' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'announce_time';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'N. di giorni '||CHR(13)||CHR(10)
||'all�approvazione', ML_HEADING_FR = 'Nbre de jours r�serv�s'||CHR(13)||CHR(10)
||'� l�approbation' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'approve_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Annulation -'||CHR(13)||CHR(10)
||'Nbre de Jours � l�Avance' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'cancel_days';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de revient'||CHR(13)||CHR(10)
||'� l�Unit� par' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'cost_unit';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Gruppo autorizzato'||CHR(13)||CHR(10)
||'all�approvazione' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'group_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nbre maximum de'||CHR(13)||CHR(10)
||'jours d�avance' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'max_days_ahead';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�am�nagement'||CHR(13)||CHR(10)
||'de pi�ce' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'L�utilisateur doit notifier si'||CHR(13)||CHR(10)
||'d�lai d�approbation a expir�' WHERE TABLE_NAME = 'rm_arrange' AND FIELD_NAME = 'user_approval_expired';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Am�nagement' WHERE TABLE_NAME = 'rm_arrange_type' AND FIELD_NAME = 'arrange_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Ordre'||CHR(13)||CHR(10)
||'d�affichage' WHERE TABLE_NAME = 'rm_arrange_type' AND FIELD_NAME = 'display_order';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�am�nagement'||CHR(13)||CHR(10)
||'de pi�ce' WHERE TABLE_NAME = 'rm_arrange_type' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�am�nagement'||CHR(13)||CHR(10)
||'de pi�ce' WHERE TABLE_NAME = 'rm_resource_std' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'rm_team' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant d�affectation'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'rm_team' AND FIELD_NAME = 'rm_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'rm_team' AND FIELD_NAME = 'team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'rm_trial' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'rm_trial' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Suppression '||CHR(13)||CHR(10)
||'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'del_user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Du '||CHR(13)||CHR(10)
||'code'||CHR(13)||CHR(10)
||'d��tage' WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'from_fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Risorse necessarie'||CHR(13)||CHR(10)
||'per l�hoteling' WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'resources';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'rmpct' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d''�l�ment'||CHR(13)||CHR(10)
||'d�action' WHERE TABLE_NAME = 'rmpctmob_sync' AND FIELD_NAME = 'activity_log_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Suppression '||CHR(13)||CHR(10)
||'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'rmpctmob_sync' AND FIELD_NAME = 'del_user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'rmpctmob_sync' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'rmpctmob_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d�Utilisateur' WHERE TABLE_NAME = 'rmpctmob_sync' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de'||CHR(13)||CHR(10)
||'l�Espace' WHERE TABLE_NAME = 'rmstd' AND FIELD_NAME = 'cost_of_space';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'E� spazio di '||CHR(13)||CHR(10)
||'supporto' WHERE TABLE_NAME = 'rmstd' AND FIELD_NAME = 'support';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Standard'||CHR(13)||CHR(10)
||'d�Employ�' WHERE TABLE_NAME = 'rmstd_emstd' AND FIELD_NAME = 'em_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Kurzzeichen Unternehmenseinheit' WHERE TABLE_NAME = 'rmsum' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de pi�ce' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'rrappdet' AND FIELD_NAME = 'user_requested_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'reserva', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungsname' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'reservation_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de pi�ce' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'rrcostdet' AND FIELD_NAME = 'user_requested_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��. ��', ML_HEADING_ES = 'Estd.'||CHR(13)||CHR(10)
||'recurso. Nombre', ML_HEADING_IT = 'Std.'||CHR(13)||CHR(10)
||'risorsa Nome', ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'std. naam', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'std de ressource', ML_HEADING_DE = 'Ressourcenstand.'||CHR(13)||CHR(10)
||' Name' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'resource_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'resource_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de recurso', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'resource-reservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de ressource', ML_HEADING_DE = 'Ressourcenreservierungscode' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'rsres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de ressource' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayresocc' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Tipo destinazione d�uso locale', ML_HEADING_FR = 'Coder le Type d�Arrangement de Pi�ce' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'rm_arrange_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de espacio', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'ruimtereservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de pi�ce', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Raumreservierung' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'rmres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de pi�ce' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrmocc' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'edificio', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'edificio', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'gebouw', ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'b�timent', ML_HEADING_DE = 'Name des'||CHR(13)||CHR(10)
||'Geb�udes' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'reserva', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungsname' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'reservation_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_IT = 'Stato della'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'la r�servation' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'oficio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'settore', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'de m�tier', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Gewerk' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'tr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Aangevraagd'||CHR(13)||CHR(10)
||'voor', ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'user_requested_for';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Leveranciers-'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Lieferanten-'||CHR(13)||CHR(10)
||'Code' WHERE TABLE_NAME = 'rrdayrmres' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Aangevraagde'||CHR(13)||CHR(10)
||'hoeveelheid', ML_HEADING_FR = 'Quantit�'||CHR(13)||CHR(10)
||'demand�e' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'quantity';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Aangevraagd'||CHR(13)||CHR(10)
||'voor', ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'rrdayrmresplus' AND FIELD_NAME = 'user_requested_for';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'edificio', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'edificio', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'gebouw', ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'b�timent', ML_HEADING_DE = 'Name des'||CHR(13)||CHR(10)
||'Geb�udes' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Aangevraagde'||CHR(13)||CHR(10)
||'hoeveelheid', ML_HEADING_FR = 'Quantit�'||CHR(13)||CHR(10)
||'demand�e' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'quantity';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'reserva', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungsname' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'reservation_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'naam', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'resource_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'resource_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de ressource' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'oficio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'settore', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'de m�tier', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Gewerk' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'tr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Aangevraagd'||CHR(13)||CHR(10)
||'voor', ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'user_requested_for';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Leveranciers-'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Lieferanten-'||CHR(13)||CHR(10)
||'Code' WHERE TABLE_NAME = 'rrdayrresplus' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��� - '||CHR(13)||CHR(10)
||'��', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'division - Code de'||CHR(13)||CHR(10)
||'d�partement', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Bereich - Kurzzeichen'||CHR(13)||CHR(10)
||'Abteilung' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'dv_dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmoncostdp' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'bl_id';

UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'resource_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_IT = 'Stato della'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'la r�servation' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonnumrres' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_IT = 'Stato della'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'la r�servation' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Utilisateur' WHERE TABLE_NAME = 'rrmonreq' AND FIELD_NAME = 'usertype';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'resource_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de ressource' WHERE TABLE_NAME = 'rrmonresrej' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_IT = 'Stato della'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'la r�servation' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonrmcap' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'ressource', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Ressource' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'resource_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��. ��', ML_HEADING_ES = 'Estd.'||CHR(13)||CHR(10)
||'recurso. Nombre', ML_HEADING_IT = 'Std.'||CHR(13)||CHR(10)
||'risorsa Nome', ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'std. naam', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'std de ressource', ML_HEADING_DE = 'Ressourcenstand.'||CHR(13)||CHR(10)
||' Name' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'resource_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'resource_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de reserva'||CHR(13)||CHR(10)
||'de recurso', ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'resource-reservering', ML_HEADING_FR = 'Code de r�servation'||CHR(13)||CHR(10)
||'de ressource', ML_HEADING_DE = 'Ressourcenreservierungscode' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'rsres_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de ressource' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonthresquant' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Mese/i' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'monthtxt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_IT = 'Stato della'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'la r�servation' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'einde', ML_HEADING_DE = 'Endet'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'time_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Tijd'||CHR(13)||CHR(10)
||'start', ML_HEADING_DE = 'Beginnt'||CHR(13)||CHR(10)
||'um' WHERE TABLE_NAME = 'rrmonusearr' AND FIELD_NAME = 'time_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrressheet' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrressheet' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Datum'||CHR(13)||CHR(10)
||'Begin', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'reserva', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'prenotazione', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungsname' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'reservation_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�;Room Conflict;Conflit de pi�ces' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'user_requested_by';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Aangevraagd'||CHR(13)||CHR(10)
||'voor', ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'pour' WHERE TABLE_NAME = 'rrressheetplus' AND FIELD_NAME = 'user_requested_for';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'configuration', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Konfiguration' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'config_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'paese', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Land' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'ctry_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'esecuzione', ML_HEADING_NL = 'Geplande'||CHR(13)||CHR(10)
||'uitvoer.datum', ML_HEADING_FR = 'A ex�cuter'||CHR(13)||CHR(10)
||'le' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'date_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Werkbeschrijving', ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'des travaux' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'dv_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'planta', ML_HEADING_NL = 'Verdieping'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage', ML_HEADING_DE = 'Geschoss-'||CHR(13)||CHR(10)
||'kurzzeichen' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Nombre de'||CHR(13)||CHR(10)
||'edificio', ML_HEADING_IT = 'Nome'||CHR(13)||CHR(10)
||'edificio', ML_HEADING_NL = 'Naam'||CHR(13)||CHR(10)
||'gebouw', ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'b�timent', ML_HEADING_DE = 'Name des'||CHR(13)||CHR(10)
||'Geb�udes' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� #', ML_HEADING_ES = 'N� tel�fono del'||CHR(13)||CHR(10)
||'solicitante', ML_HEADING_NL = 'Telefoonnr.'||CHR(13)||CHR(10)
||'aanvrager', ML_HEADING_FR = 'N� de t�l�phone'||CHR(13)||CHR(10)
||'du demandeur', ML_HEADING_DE = 'Telefon-Nr.'||CHR(13)||CHR(10)
||'des Anforderers' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'phone';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'problema', ML_HEADING_IT = 'Tipo'||CHR(13)||CHR(10)
||'di problema', ML_HEADING_FR = 'Type de'||CHR(13)||CHR(10)
||'probl�me' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'prob_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Demand�'||CHR(13)||CHR(10)
||'par' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'requestor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Code'||CHR(13)||CHR(10)
||'reservering', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'r�servation', ML_HEADING_DE = 'Reservierungscode' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'res_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Resource-'||CHR(13)||CHR(10)
||'naam', ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'ressource' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'resource_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'����', ML_HEADING_ES = 'Tipo de'||CHR(13)||CHR(10)
||'disposici�n de espacio', ML_HEADING_IT = 'Tipo di utilizzo'||CHR(13)||CHR(10)
||'del locale', ML_HEADING_NL = 'Type'||CHR(13)||CHR(10)
||'ruimtearrangement', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�am�nagement de pi�ce' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'rm_arrange_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'espacio', ML_HEADING_NL = 'Ruimte-'||CHR(13)||CHR(10)
||'code', ML_HEADING_FR = 'Code de'||CHR(13)||CHR(10)
||'pi�ce', ML_HEADING_DE = 'Raumkurzzeichen' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���', ML_HEADING_ES = 'C�digo de sede', ML_HEADING_IT = 'Codice sito', ML_HEADING_FR = 'Code site' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'site_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Awaiting App.;En attente d�approbation;Rejected;Rejet�;Cancelled;Annul�;Confirmed;Confirm�;Closed;Ferm�', ML_HEADING_FR = 'Statut de la'||CHR(13)||CHR(10)
||'r�servation de ressource' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'oficio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||'settore', ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'de m�tier', ML_HEADING_DE = 'Kurzzeichen'||CHR(13)||CHR(10)
||'Gewerk' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'tr_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Leveranciers-'||CHR(13)||CHR(10)
||'code', ML_HEADING_DE = 'Lieferanten-'||CHR(13)||CHR(10)
||'Code' WHERE TABLE_NAME = 'rrwrrestr' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'runoffarea' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Abord' WHERE TABLE_NAME = 'runoffarea' AND FIELD_NAME = 'runoff_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Classe'||CHR(13)||CHR(10)
||'d�Abords' WHERE TABLE_NAME = 'runofftype' AND FIELD_NAME = 'runoff_class';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Abord' WHERE TABLE_NAME = 'runofftype' AND FIELD_NAME = 'runoff_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'P�riode'||CHR(13)||CHR(10)
||'d�Affectation' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'alloc_period';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Score de'||CHR(13)||CHR(10)
||'l�Affectation' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'alloc_score';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Score de'||CHR(13)||CHR(10)
||'l�Affectation - Etendu' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'alloc_score_ext';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'�� / �� / ��', ML_HEADING_ES = 'Descripci�n/origen/fundamento '||CHR(13)||CHR(10)
||'de requisitos de espacio', ML_HEADING_IT = 'Descrizione/Origine/Logica '||CHR(13)||CHR(10)
||'dei requisiti di spazio', ML_HEADING_NL = 'Ruimtebehoefte '||CHR(13)||CHR(10)
||'Omschrijving/ Bron / Basis', ML_HEADING_FR = 'Description des besoins '||CHR(13)||CHR(10)
||'en espace / Source / Justification', ML_HEADING_DE = 'Raum-/Fl�chenbedarf '||CHR(13)||CHR(10)
||'Beschreibung/Quelle/Grundlage' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'sb_desc';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_DE = '0;Keine;bu;Unternehmenseinheiten;dv;Bereiche;dp;Abteilungen;fg;Funktionsgruppen' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'sb_level';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Space Requirements;Besoins en espace;Space Forecast;Pr�vision de l�espace;', ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�exigences' WHERE TABLE_NAME = 'sb' AND FIELD_NAME = 'sb_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Score de'||CHR(13)||CHR(10)
||'l�Affectation' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'alloc_score';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Score de'||CHR(13)||CHR(10)
||'l�Affectation - Etendu' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'alloc_score_ext';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Unternehmenseinheit' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'bu_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Uitrusting-'||CHR(13)||CHR(10)
||'standaard', ML_HEADING_FR = 'Standard d��quipement' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'eq_std';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Affectation'||CHR(13)||CHR(10)
||'� l��tage' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 0' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p00_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 0 ��', ML_HEADING_FR = 'P�riode 0'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p00_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 1' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p01_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 1 ��', ML_HEADING_FR = 'P�riode 1'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p01_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 2' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p02_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 12 ��', ML_HEADING_FR = 'P�riode 2'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p02_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 3' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p03_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 3 ��', ML_HEADING_FR = 'P�riode 3'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p03_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 4' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p04_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 4 ��', ML_HEADING_FR = 'P�riode 4'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p04_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 5' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p05_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 5 ��', ML_HEADING_FR = 'P�riode 5'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p05_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 6' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p06_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 6 ��', ML_HEADING_FR = 'P�riode 6'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p06_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 7' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p07_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 7 ��', ML_HEADING_FR = 'P�riode 7'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p07_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 8' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p08_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 8 ��', ML_HEADING_FR = 'P�riode 8'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p08_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 9' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p09_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 9 ��', ML_HEADING_FR = 'P�riode 9'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p09_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 10' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p10_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 10 ��', ML_HEADING_FR = 'P�riode 10'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p10_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 11' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p11_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 11 ��', ML_HEADING_FR = 'P�riode 11'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p11_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Ausdruck:'||CHR(13)||CHR(10)
||'Zeitraum 12' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p12_expr';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� 12 ��', ML_HEADING_FR = 'P�riode 12'||CHR(13)||CHR(10)
||'- Valeur' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'p12_value';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����', ML_HEADING_FR = 'Effectif'||CHR(13)||CHR(10)
||'d�unit�' WHERE TABLE_NAME = 'sb_items' AND FIELD_NAME = 'unit_headcount';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Unternehmenseinheit 1' WHERE TABLE_NAME = 'sbaffin' AND FIELD_NAME = 'bu_id_1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Unternehmenseinheit 2' WHERE TABLE_NAME = 'sbaffin' AND FIELD_NAME = 'bu_id_2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'Budget d�Espace' WHERE TABLE_NAME = 'sbaffin' AND FIELD_NAME = 'sb_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 0' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p00_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 1' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p01_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 2' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p02_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 3' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p03_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 4' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p04_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 5' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p05_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 6' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p06_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 7' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p07_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 8' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p08_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 9' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p09_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 10' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p10_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 11' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p11_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_DE = 'Titel:'||CHR(13)||CHR(10)
||'Zeitraum 12' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'p12_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom du'||CHR(13)||CHR(10)
||'Budget d�Espace' WHERE TABLE_NAME = 'sbperiods' AND FIELD_NAME = 'sb_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'serv' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d�Expiration du'||CHR(13)||CHR(10)
||'Contrat de Maintenance' WHERE TABLE_NAME = 'servcont' AND FIELD_NAME = 'date_expiration';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d��tag�re', SL_HEADING_FR = 'Nom d��tag�re' WHERE TABLE_NAME = 'shelf' AND FIELD_NAME = 'name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nombre'||CHR(13)||CHR(10)
||'d�Hectares' WHERE TABLE_NAME = 'site' AND FIELD_NAME = 'acres';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'site' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'site' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Taux (U/L)'||CHR(13)||CHR(10)
||'d�efficacit�' WHERE TABLE_NAME = 'site' AND FIELD_NAME = 'ratio_ur';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�Autorisation' WHERE TABLE_NAME = 'softinv' AND FIELD_NAME = 'authorization_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Installation' WHERE TABLE_NAME = 'softinv' AND FIELD_NAME = 'date_installed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'softinv' AND FIELD_NAME = 'date_purchased';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'state' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'su' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Id du type'||CHR(13)||CHR(10)
||'d�infrastructure' WHERE TABLE_NAME = 'su' AND FIELD_NAME = 'facility_type_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'New;Nouveau;Ready for Transit;Pr�t � l�envoi;In Transit;En transit;Received;Re�u;Partially Received;Partiellement re�u;Error;Erreur' WHERE TABLE_NAME = 'supply_req' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Modifi� par l�utilisateur mobile�?', SL_HEADING_FR = 'Modifi� par l�utilisateur mobile�?' WHERE TABLE_NAME = 'surveymob_sync' AND FIELD_NAME = 'mob_is_changed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Destinazione d�uso locale' WHERE TABLE_NAME = 'surveyrm_sync' AND FIELD_NAME = 'rm_use';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_FR = 'Claimed;Demand� par l�agent d�inventaire;Completed;Effectu� par l�agent d�inventaire' WHERE TABLE_NAME = 'surveyrm_sync' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'surveyrm_sync' AND FIELD_NAME = 'survey_photo_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = '0;�;1;�', ENUM_LIST_DE = '0;Nein;1;Ja', ENUM_LIST_ES = '0;No;1;S�', ENUM_LIST_FR = '0;Non;1;Oui', ENUM_LIST_IT = '0;No;1;S�', ML_HEADING_FR = 'Le document d�annotation'||CHR(13)||CHR(10)
||'du relev� est nouveau', ENUM_LIST_NL = '0;Nee;1;Ja' WHERE TABLE_NAME = 'surveyrm_sync' AND FIELD_NAME = 'survey_redline_rm_isnew';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_IT = 'NONE;Nessuno stato;NONE-REVIEW;Nessuno stato in esame;FIT-ONLINE;Adatto all�uso;FIT-OFFLINE;Adatto all�uso - Non in linea;UNFIT-TEMP;Inadatto all�uso - Recuperabile;UNFIT-PERM;Inadatto all�uso - Distrutto' WHERE TABLE_NAME = 'system_bl' AND FIELD_NAME = 'recovery_status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires sur l��limination' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'comment_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date d��limination' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'date_disposal';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d��limination' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'disposal_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'En attente d�action' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'pending_action';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'po_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Riga ordine'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'N� Ligne du'||CHR(13)||CHR(10)
||'Bon d�Achat' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'po_line_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code Police'||CHR(13)||CHR(10)
||'d�Assurance' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'policy_id';
UPDATE AFM.AFM_FLDS_LANG SET ENUM_LIST_CH = 'in;���;out;��;rep;���;stor;���;salv;���;sold;���;miss;��;disp;���;don;���;sto;���;todi;����', ENUM_LIST_DE = 'in;In Gebrauch;out;Au�er Betrieb;rep;Zur Reparatur;stor;Auf Lager;salv;Wiederverwertet;sold;Verkauft;miss;Fehlend;disp;Entsorgt;don;Gespendet;sto;Gestohlen;todi;Zu entsorgen', ENUM_LIST_ES = 'in;En uso;out;Fuera de servicio;rep;En reparaci�n;stor;En almacenamiento;salv;Baja por amortizaci�n;sold;Vendido;miss;Eliminado;disp;Eliminado;don;Donado;sto;Robado;todi;Para ser eliminado', ENUM_LIST_FR = 'in;Utilis�;out;Hors service;rep;En r�paration;stor;En stockage;salv;Au rebut;sold;Vendu;miss;Manquant;disp;Elimin�;don;Donn�;sto;Vol�;todi;A �liminer', ENUM_LIST_IT = 'in;In uso;out;Fuori servizio;rep;In riparazione;stor;In stoccaggio;salv;Recuperato;sold;Venduto;miss;Mancante;disp;Smaltito;don;Donato;sto;Rubato;todi;Da smaltire', ENUM_LIST_NL = 'in;In gebruik;out;Buiten bedrijf;rep;Wordt hersteld;stor;In opslag;salv;Gereviseerd;sold;Verkocht;miss;Ontbrekend;disp;Afgevoerd;don;Geschonken;sto;Gestolen;todi;Af te voeren' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Prix'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'value_original';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Valeur'||CHR(13)||CHR(10)
||'r�siduelle' WHERE TABLE_NAME = 'ta' AND FIELD_NAME = 'value_salvage';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����' WHERE TABLE_NAME = 'ta_audit' AND FIELD_NAME = 'criticality';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ta_audit' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de l�Employ�'||CHR(13)||CHR(10)
||'en Inventaire' WHERE TABLE_NAME = 'ta_compinvsur' AND FIELD_NAME = 'inv_em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de l�Employ�'||CHR(13)||CHR(10)
||'du Relev�' WHERE TABLE_NAME = 'ta_compinvsur' AND FIELD_NAME = 'sur_em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'ta_compinvtrial' AND FIELD_NAME = 'trial_rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Dotation'||CHR(13)||CHR(10)
||'d�Amortissement' WHERE TABLE_NAME = 'ta_dep' AND FIELD_NAME = 'value_current_dep';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de'||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'ta_fnstdcnts' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Option'||CHR(13)||CHR(10)
||'d�Annulation ?' WHERE TABLE_NAME = 'ta_lease' AND FIELD_NAME = 'cancel_option';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Option'||CHR(13)||CHR(10)
||'d�Achat ?' WHERE TABLE_NAME = 'ta_lease' AND FIELD_NAME = 'retain_option';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'ta_trial' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��tage dans'||CHR(13)||CHR(10)
||'simulation' WHERE TABLE_NAME = 'ta_trial' AND FIELD_NAME = 'fl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�����' WHERE TABLE_NAME = 'ta_trial' AND FIELD_NAME = 'rm_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de fin'||CHR(13)||CHR(10)
||'d�adh�sion' WHERE TABLE_NAME = 'team' AND FIELD_NAME = 'date_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but'||CHR(13)||CHR(10)
||'d�adh�sion' WHERE TABLE_NAME = 'team' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'R�le du membre'||CHR(13)||CHR(10)
||'de l��quipe' WHERE TABLE_NAME = 'team' AND FIELD_NAME = 'member_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team' AND FIELD_NAME = 'team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team' AND FIELD_NAME = 'team_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de fin'||CHR(13)||CHR(10)
||'d�association' WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'date_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'de d�but'||CHR(13)||CHR(10)
||'d�association' WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'date_start';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'d�association'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d�organisation' WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'org_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identifiant d�association'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'team_assoc_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_assoc' AND FIELD_NAME = 'team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description de cat�gorie d��quipe' WHERE TABLE_NAME = 'team_category' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie d��quipe' WHERE TABLE_NAME = 'team_category' AND FIELD_NAME = 'team_category';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'A besoin d�espace'||CHR(13)||CHR(10)
||'d��quipe�?' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'needs_team_space';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Etat'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Cat�gorie'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'team_category';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Fonction'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'team_function';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom'||CHR(13)||CHR(10)
||'d��quipe' WHERE TABLE_NAME = 'team_properties' AND FIELD_NAME = 'team_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Num�ro'||CHR(13)||CHR(10)
||'d�Extension' WHERE TABLE_NAME = 'telext' AND FIELD_NAME = 'extension';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'d�acquisto', ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Achat' WHERE TABLE_NAME = 'tl' AND FIELD_NAME = 'date_purchased';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut'||CHR(13)||CHR(10)
||'de l�Outil' WHERE TABLE_NAME = 'tl' AND FIELD_NAME = 'status';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Les r�servations'||CHR(13)||CHR(10)
||'g�n�rent-elles une '||CHR(13)||CHR(10)
||'Demande d�Intervention�?' WHERE TABLE_NAME = 'tr' AND FIELD_NAME = 'wr_from_reserve';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Description du'||CHR(13)||CHR(10)
||'Type d�Outil' WHERE TABLE_NAME = 'tt' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = '� un�eccezione?' WHERE TABLE_NAME = 'vat_percent' AND FIELD_NAME = 'is_exception';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'vert' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Expiration' WHERE TABLE_NAME = 'visitors' AND FIELD_NAME = 'date_end';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Statut de'||CHR(13)||CHR(10)
||'l�Autorisation' WHERE TABLE_NAME = 'visitors' AND FIELD_NAME = 'is_authorized';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Titre de'||CHR(13)||CHR(10)
||'l�Autre Contact' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'alt_title';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Certificat '||CHR(13)||CHR(10)
||'d�Assurance 1' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'insurance_cert1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Certificat '||CHR(13)||CHR(10)
||'d�Assurance 2' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'insurance_cert2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�Activit� '||CHR(13)||CHR(10)
||'Minorit�/Femmes' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'mwbe_code';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Commentaires d�infos source' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'source_feed_comments';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '�� ��������' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'source_time_update';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Les r�servations'||CHR(13)||CHR(10)
||'g�n�rent-elles une '||CHR(13)||CHR(10)
||'Demande d�Intervention�?' WHERE TABLE_NAME = 'vn' AND FIELD_NAME = 'wr_from_reserve';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Taux d�imposition (%)' WHERE TABLE_NAME = 'vn_rate' AND FIELD_NAME = 'tax_rate';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�applique au r�le' WHERE TABLE_NAME = 'vpa_groupstoroles' AND FIELD_NAME = 'role_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Si applica all�utente', ML_HEADING_FR = 'S�applique � l�utilisateur' WHERE TABLE_NAME = 'vpa_groupstousers' AND FIELD_NAME = 'user_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�applique au r�le' WHERE TABLE_NAME = 'vpa_rest' AND FIELD_NAME = 'role_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'S�applique � la table' WHERE TABLE_NAME = 'vpa_rest' AND FIELD_NAME = 'table_name';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Expiration' WHERE TABLE_NAME = 'warranty' AND FIELD_NAME = 'date_expiration';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'waste_areas' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Zone d�Entreposage / Stockage '||CHR(13)||CHR(10)
||'/ Citernes' WHERE TABLE_NAME = 'waste_areas' AND FIELD_NAME = 'storage_location';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�'||CHR(13)||CHR(10)
||'�vacuation' WHERE TABLE_NAME = 'waste_dispositions' AND FIELD_NAME = 'disposition_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Notes sur l�'||CHR(13)||CHR(10)
||'usine de traitement' WHERE TABLE_NAME = 'waste_facilities' AND FIELD_NAME = 'notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code fournisseur de'||CHR(13)||CHR(10)
||'l�usine de traitement' WHERE TABLE_NAME = 'waste_facilities' AND FIELD_NAME = 'vn_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date'||CHR(13)||CHR(10)
||'d�Acceptation' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'date_acceptance';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de choix d�une'||CHR(13)||CHR(10)
||'usine de traitement alternative' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'date_alternate_facility';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date du rapport'||CHR(13)||CHR(10)
||'d��cart' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'date_exception';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Date de contact'||CHR(13)||CHR(10)
||'d�une usine de traitement' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'date_facility_contact';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Notes sur le '||CHR(13)||CHR(10)
||'rapport d��cart' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'exception_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Notes du contact'||CHR(13)||CHR(10)
||'de l�usine de traitement' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'facility_contact_notes';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�usine '||CHR(13)||CHR(10)
||'de traitement alternative' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'facility_id_alt';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Port d�'||CHR(13)||CHR(10)
||'entr�e/sortie' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'port';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Choix d�une'||CHR(13)||CHR(10)
||'usine de traitement alternative' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'sign_alternate_facility';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Choix d�une'||CHR(13)||CHR(10)
||'usine de traitement' WHERE TABLE_NAME = 'waste_manifests' AND FIELD_NAME = 'sign_facility';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Nom de '||CHR(13)||CHR(10)
||'l�Employ�' WHERE TABLE_NAME = 'waste_out' AND FIELD_NAME = 'em_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Zone d�Entreposage / Stockage'||CHR(13)||CHR(10)
||' / Citernes' WHERE TABLE_NAME = 'waste_out' AND FIELD_NAME = 'storage_location';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type'||CHR(13)||CHR(10)
||'d�Unit�s' WHERE TABLE_NAME = 'waste_out' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Type d�Unit�s'||CHR(13)||CHR(10)
||'par D�faut' WHERE TABLE_NAME = 'waste_profiles' AND FIELD_NAME = 'units_type';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Point d��quilibre'||CHR(13)||CHR(10)
||'de refroidissement' WHERE TABLE_NAME = 'weather_model' AND FIELD_NAME = 'oat_c1';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Point d��quilibre'||CHR(13)||CHR(10)
||'de chauffage' WHERE TABLE_NAME = 'weather_model' AND FIELD_NAME = 'oat_h2';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'ID d�objet GIS' WHERE TABLE_NAME = 'weather_station' AND FIELD_NAME = 'geo_objectid';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Temperatura'||CHR(13)||CHR(10)
||'esterna dell�aria', ML_HEADING_FR = 'Temp�rature'||CHR(13)||CHR(10)
||'de l�Air � l�Ext�rieur' WHERE TABLE_NAME = 'weather_station_data' AND FIELD_NAME = 'temp_outside_air';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code d�acc�s'||CHR(13)||CHR(10)
||'(ID de connexion)' WHERE TABLE_NAME = 'weather_station_source' AND FIELD_NAME = 'access_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Mot de passe d�acc�s'||CHR(13)||CHR(10)
||'(Mot de passe de connexion)' WHERE TABLE_NAME = 'weather_station_source' AND FIELD_NAME = 'access_pwd';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Identificateur d�entit� /'||CHR(13)||CHR(10)
||'Identifiant unique' WHERE TABLE_NAME = 'wn' AND FIELD_NAME = 'ehandle';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'Main d�Oeuvre' WHERE TABLE_NAME = 'wo' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Heure'||CHR(13)||CHR(10)
||'d�ex�cution' WHERE TABLE_NAME = 'wo' AND FIELD_NAME = 'time_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Code de l��quipe'||CHR(13)||CHR(10)
||'d�intervention' WHERE TABLE_NAME = 'wo' AND FIELD_NAME = 'work_team_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||'cuenta', ML_HEADING_NL = 'Account-'||CHR(13)||CHR(10)
||'Code', ML_HEADING_FR = 'Poste'||CHR(13)||CHR(10)
||'Comptable' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'ac_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' edificio', ML_HEADING_NL = '�Gebouw'||CHR(13)||CHR(10)
||' code', ML_HEADING_DE = 'Geb�ude-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'bl_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_NL = 'Geschat'||CHR(13)||CHR(10)
||'Kosten', ML_HEADING_FR = 'Estim�'||CHR(13)||CHR(10)
||'Co�t' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'cost_estimated';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'recursos humanos', ML_HEADING_FR = 'Co�t de la'||CHR(13)||CHR(10)
||'main-d�oeuvre', ML_HEADING_DE = 'Arbeits-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'cost_labor';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_NL = 'Overig'||CHR(13)||CHR(10)
||'kosten', ML_HEADING_FR = 'Autres'||CHR(13)||CHR(10)
||'co�ts' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'cost_other';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'piezas', ML_HEADING_IT = 'Costo delle'||CHR(13)||CHR(10)
||'parti', ML_HEADING_FR = 'Co�t des'||CHR(13)||CHR(10)
||'pi�ces d�tach�es', ML_HEADING_DE = 'Teile-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'cost_parts';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_ES = 'Coste de'||CHR(13)||CHR(10)
||'herramientas', ML_HEADING_IT = 'Costo degli'||CHR(13)||CHR(10)
||'attrezzi', ML_HEADING_NL = 'Kosten van'||CHR(13)||CHR(10)
||'gereedschap', ML_HEADING_FR = 'Co�t des'||CHR(13)||CHR(10)
||'outils', ML_HEADING_DE = 'Werkzeug-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'cost_tools';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Costo'||CHR(13)||CHR(10)
||'totale', ML_HEADING_NL = 'uitgaven'||CHR(13)||CHR(10)
||'Kosten', ML_HEADING_FR = 'Co�t'||CHR(13)||CHR(10)
||'total', ML_HEADING_DE = 'Gesamt-'||CHR(13)||CHR(10)
||'kosten' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'cost_total';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data'||CHR(13)||CHR(10)
||'esecuzione', ML_HEADING_NL = 'Geplande'||CHR(13)||CHR(10)
||'uitvoer.datum', ML_HEADING_FR = 'A ex�cuter'||CHR(13)||CHR(10)
||'le' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'date_assigned';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data di chiusura'||CHR(13)||CHR(10)
||'ordine di lavoro', ML_HEADING_FR = 'Bon de travaux'||CHR(13)||CHR(10)
||'ferm� le' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'date_closed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_IT = 'Data di completamento'||CHR(13)||CHR(10)
||'ordine di lavoro', ML_HEADING_FR = 'Bon de travaux'||CHR(13)||CHR(10)
||'termin� le' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'date_completed';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Bon de travaux'||CHR(13)||CHR(10)
||'cr�� le' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'date_created';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_FR = 'Bon de travaux'||CHR(13)||CHR(10)
||'�mis le' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'date_issued';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '����'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'Descripci�n de'||CHR(13)||CHR(10)
||'trabajo principal', ML_HEADING_NL = 'Omschrijving'||CHR(13)||CHR(10)
||'primaire werkz.', ML_HEADING_FR = 'Description des'||CHR(13)||CHR(10)
||'travaux principaux', ML_HEADING_DE = 'Beschreibung der'||CHR(13)||CHR(10)
||'Hauptarbeit' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'description';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '��'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' departamento', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' reparto', ML_HEADING_NL = 'Afdeling'||CHR(13)||CHR(10)
||' Code', ML_HEADING_DE = 'Abteilungs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'dp_id';
UPDATE AFM.AFM_FLDS_LANG SET ML_HEADING_CH = '���'||CHR(13)||CHR(10)
||'��', ML_HEADING_ES = 'C�digo de'||CHR(13)||CHR(10)
||' divisi�n', ML_HEADING_IT = 'Codice'||CHR(13)||CHR(10)
||' divisione', ML_HEADING_FR = 'Code '||CHR(13)||CHR(10)
||'Division', ML_HEADING_DE = 'Bereichs-'||CHR(13)||CHR(10)
||' kurzzeichen' WHERE TABLE_NAME = 'wohwo' AND FIELD_NAME = 'dv_id';

COMMIT;
SPOOL OFF;
quit;