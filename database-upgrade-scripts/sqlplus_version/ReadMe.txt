-- Execute the following commands on command prompt/terminal


sqlplus afm/afm @01-ddl-schema-upgrade-to-v232.sql
sqlplus afm/afm @02-ddl-schema-upgrade-to-v232.sql
sqlplus afm/afm @03-disable-constraints.sql
sqlplus afm/afm @04-dml-new-records-between-standard-v231-and-v232.sql
sqlplus afm/afm @05-dml-new-records-between-standard-v231-and-v232.sql
sqlplus afm/afm @06-dml-delete-records-between-standard-v231-and-v232.sql
sqlplus afm/afm @07-dml-changes-between-standard-v231-and-v232.sql
sqlplus afm/afm @08-dml-changes-between-standard-v231-and-v232.sql
sqlplus afm/afm @09-dml-overwrite-changes-between-standard-v23-and-suricat-v231.sql
sqlplus afm/afm @10-dml-overwrite-changes-between-standard-v23-and-suricat-v231.sql
sqlplus afm/afm @11-dml-overwrite-changes-between-standard-v23-and-suricat-v231.sql
sqlplus afm/afm @12-enable-constraints.sql
sqlplus afm/afm @13-modify-bl_id-size.sql
sqlplus afm/afm @14-modify-views.sql
sqlplus afm/afm @15-change-connector-id.sql