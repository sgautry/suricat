CREATE OR REPLACE FORCE VIEW "AFM"."AR_LABEL_BAIL_FACTU_EURO" ("BILLING_YEAR", "CODE_UNIQUE", "FL_ID", "BL_ID", "RM_ID", "UIC_ID", "CATEGORIE", "CODE_BAIL", "LIBELLE_PRENEUR", "REDEVANCE", "CHARGE", "TOTAL") AS 
  (select billing_year,'L'|| rm.rm_id as code_unique,
        rm.fl_id  fl_id,
        rm.bl_id  bl_id,
        rm.rm_id  rm_id,
        ls.uic_id,
        externe categorie,
        CASE WHEN (rm.ls_id IS NULL)THEN 'Vacant' ELSE 'Code bail: '|| rm.ls_id END  code_bail,
        CASE WHEN (rm.ls_id IS NULL) THEN '' ELSE CASE WHEN (ls.tn_name_ext IS NOT NULL ) THEN 'Donnée externe' ELSE libelle_preneur END END libelle_preneur,
        CASE WHEN (rm.ls_id IS NULL OR ls.tn_name_ext IS NOT NULL ) THEN ' ' ELSE CASE WHEN (SUM(redevance)+ SUM(redevance_variable)  = 0) THEN 'Redevance: 0€' ELSE
        'Redevance: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(redevance)+SUM(redevance_variable),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END redevance,
        CASE WHEN (rm.ls_id IS NULL OR ls.tn_name_ext IS NOT NULL ) THEN ' ' ELSE CASE WHEN (SUM(charge)  = 0) THEN 'Charges: 0€' ELSE
        'Charges: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(charge),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END charge,
        CASE WHEN (rm.ls_id IS NULL OR ls.tn_name_ext IS NOT NULL ) THEN ' ' ELSE CASE WHEN (SUM(redevance)+SUM(redevance_variable)+SUM(charge)+SUM(autre)  = 0 OR SUM(redevance)+SUM(redevance_variable)+SUM(charge)+SUM(autre) IS NULL) THEN 'Total: 0€' ELSE
        'Total: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(redevance)+SUM(redevance_variable)+SUM(charge)+SUM(autre),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END total
        FROM rm
LEFT JOIN ls ON ls.ls_id = rm.ls_id
LEFT JOIN AR_VIEW_FACTU_VALO ON ls.ls_id = AR_VIEW_FACTU_VALO.code_id AND ls.uic_id = AR_VIEW_FACTU_VALO.uic_id
LEFT JOIN AR_CYPRES ON code_preneur = ens_id
where tn_name_ext IS NOT NULL
GROUP BY billing_year,rm.fl_id,rm.ls_id,rm.bl_id,externe,rm.rm_id,rm.ls_id,ls.uic_id,libelle_preneur,tn_name_ext);
/

CREATE OR REPLACE FORCE VIEW "AFM"."AR_LABEL_BAIL_PV_EURO" ("CODE_UNIQUE", "FL_ID", "BL_ID", "RM_ID", "CATEGORIE", "CODE_BAIL", "LIBELLE_PRENEUR", "REDEVANCE", "CHARGE", "TOTAL") AS 
  (SELECT  'L'|| rm.rm_id as code_unique,
        rm.fl_id as fl_id,
        rm.bl_id as bl_id,
        rm.rm_id as rm_id,
		CASE WHEN (tn_name_ext IS NULL)THEN 'INTERNE' ELSE 'EXTERNE' END categorie,
		CASE WHEN (rm.ls_id IS NULL )THEN 'Vacant' ELSE 'Code bail: '|| rm.ls_id END code_bail,
        CASE WHEN (tn_name_ext IS NULL) THEN CASE WHEN (libelle_preneur IS NULL)THEN ' ' ELSE  libelle_preneur END ELSE 'Occupation externe'  END libelle_preneur,
        CASE WHEN (rm.ls_id IS NULL OR tn_name_ext IS NOT NULL) THEN ' ' ELSE
        CASE WHEN (SUM(redevance_annuelle)+ SUM(redevance_variable)  = 0) THEN 'Redevance: 0€' ELSE
        'Redevance: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(redevance_annuelle)+SUM(redevance_variable),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€'
        END END redevance,
        CASE WHEN (rm.ls_id IS NULL OR tn_name_ext IS NOT NULL) THEN ' ' ELSE
        CASE WHEN (SUM(charge)  = 0) THEN 'Charges: 0€' ELSE
        'Charges: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(charge),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€'
        END END charge,
        CASE WHEN (rm.ls_id IS NULL OR tn_name_ext IS NOT NULL) THEN ' ' ELSE
        CASE WHEN ( SUM(redevance_annuelle)+ SUM(redevance_variable) + SUM(charge) + SUM(autre)  = 0) THEN 'Total: 0€' ELSE
        'Total: '||REPLACE(LTRIM(TO_CHAR(NVL( SUM(redevance_annuelle)+ SUM(redevance_variable) + SUM(charge) + SUM(autre),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€'
        END END total
        FROM rm
        LEFT JOIN ls ON ls.ls_id = rm.ls_id
        LEFT JOIN AR_VIEW_VALO_BAIL_PV on code_id = rm.ls_id
        LEFT JOIN ar_cypres B ON B.code_preneur = ls.tn_name
        GROUP BY libelle_preneur,rm.rm_id,rm.fl_id,rm.bl_id,rm.ls_id,tn_name,tn_name_ext);
/

CREATE OR REPLACE FORCE VIEW "AFM"."AR_LABEL_CONT_FACTU_EURO" ("BILLING_YEAR", "CODE_UNIQUE", "FL_ID", "BL_ID", "RM_ID", "UIC_ID", "CATEGORIE", "REF_CONTRAT", "CONTRACTANT", "REDEVANCE", "CHARGE", "CHIFFRE_AFFAIRE") AS 
  (
select billing_year,'L'|| rm.rm_id as code_unique,
        rm.fl_id  fl_id,
        rm.bl_id  bl_id,
        rm.rm_id  rm_id,
        ls.uic_id,
        externe categorie,
        CASE WHEN (rm.ls_id IS NULL)THEN 'Vacant' ELSE
        CASE WHEN (AR_VIEW_FACTU_VALO.cont_id IS NULL) THEN 'Sans ref. contrat' ELSE 'Ref. contrat: '|| AR_VIEW_FACTU_VALO.cont_id END END ref_contrat,
        CASE WHEN (AR_VIEW_FACTU_VALO.cont_id IS NULL) THEN '' ELSE libelle_tiers_parent END contractant,
        CASE WHEN (AR_VIEW_FACTU_VALO.cont_id IS NULL) THEN ' ' ELSE CASE WHEN (SUM(redevance)+ SUM(redevance_variable)  = 0) THEN 'Redevance: 0€' ELSE
        'Redevance: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(redevance)+SUM(redevance_variable),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END redevance,
        CASE WHEN (AR_VIEW_FACTU_VALO.cont_id IS NULL) THEN ' ' ELSE CASE WHEN (SUM(charge)  = 0) THEN 'Charges: 0€' ELSE
        'Charges: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(charge),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END charge,
        CASE WHEN (AR_VIEW_FACTU_VALO.cont_id IS NULL) THEN ' ' ELSE CASE WHEN (SUM(amount)  = 0  OR SUM(amount) IS NULL) THEN 'CA: 0€' ELSE
        'CA: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(amount),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END chiffre_affaire
        FROM rm
LEFT JOIN ls ON ls.ls_id = rm.ls_id
LEFT JOIN AR_VIEW_FACTU_VALO ON ls.cont_id = AR_VIEW_FACTU_VALO.cont_id AND ls.uic_id = AR_VIEW_FACTU_VALO.uic_id
LEFT JOIN AR_CYPRES ON code_preneur = ens_id
LEFT JOIN AR_CHIFFRE_AFFAIRE ON AR_VIEW_FACTU_VALO.cont_id = AR_CHIFFRE_AFFAIRE.cont_id AND billing_year = ca_year
GROUP BY billing_year,rm.fl_id,rm.ls_id,rm.bl_id,externe,rm.rm_id,AR_VIEW_FACTU_VALO.cont_id,tn_name_ext,tn_name,ls.uic_id,libelle_tiers_parent);
/

CREATE OR REPLACE FORCE VIEW "AFM"."AR_LABEL_CONTRACT_EURO" ("CODE_UNIQUE", "FL_ID", "BL_ID", "RM_ID", "CATEGORIE", "REF_CONTRAT", "REDEVANCE", "CHARGE", "CONTRACTANT") AS 
  (SELECT code_unique,fl_id,bl_id,rm_id,categorie,
        CASE WHEN (ref_contrat != 'Vacant' AND ref_contrat !='Sans ref. contrat') THEN 'Ref. contrat: '|| ref_contrat ELSE ref_contrat END ref_contrat,redevance,charge,
        CASE WHEN (contractant IS NULL) THEN ' ' ELSE contractant END contractant
        FROM(
select 'L'|| rm.rm_id as code_unique,
        rm.fl_id  fl_id,
        rm.bl_id  bl_id,
        rm.rm_id  rm_id,
        rm.ls_id,
        ls.uic_id,
		CASE WHEN (MAX(tn_name_ext) IS NULL)THEN 'INTERNE' ELSE 'EXTERNE' END categorie,
        CASE WHEN (rm.ls_id IS NULL)THEN 'Vacant' ELSE
        CASE WHEN (ar_view_valo_bail_pv.cont_id IS NULL) THEN 'Sans ref. contrat' ELSE ar_view_valo_bail_pv.cont_id END END ref_contrat,
        CASE WHEN (ar_view_valo_bail_pv.cont_id IS NULL) THEN ' ' ELSE CASE WHEN (SUM(redevance_annuelle)+ SUM(redevance_variable)  = 0) THEN 'Redevance: 0€' ELSE
        'Redevance: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(redevance_annuelle)+SUM(redevance_variable),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END redevance,
        CASE WHEN (ar_view_valo_bail_pv.cont_id IS NULL) THEN ' ' ELSE CASE WHEN (SUM(charge)  = 0) THEN 'Charges: 0€' ELSE
        'Charges: '||REPLACE(LTRIM(TO_CHAR(NVL(SUM(charge),0),'999G999G999D99','NLS_NUMERIC_CHARACTERS ='', ''')),',00','') ||'€' END
        END charge
        FROM rm
LEFT JOIN ls ON ls.ls_id = rm.ls_id
LEFT JOIN ar_view_valo_bail_pv ON ls.cont_id = ar_view_valo_bail_pv.cont_id AND ls.uic_id = ar_view_valo_bail_pv.uic_id
GROUP BY rm.fl_id,rm.ls_id,rm.bl_id,rm.rm_id,ar_view_valo_bail_pv.cont_id,tn_name_ext,tn_name,ls.uic_id) A
LEFT JOIN ar_view_contract ON ar_view_contract.cont_id= ref_contrat AND AR_VIEW_CONTRACT.uic_id = A.uic_id);
/

COMMIT;
