DELETE FROM afm_activity_params WHERE activity_id='AbSpaceRoomInventoryBAR' AND param_id='SpaceConsoleDefaultLayers';
INSERT INTO afm_activity_params(activity_id,param_id,param_value,description) VALUES ('AbSpaceRoomInventoryBAR','SpaceConsoleDefaultLayers','-',
'This parameter stores the list of default drawing layers displayed on the Suricat Space Console.the value must be separated using semicolon(;).
To display all the layers set the parameter value to hyphen(-) ');
COMMIT;