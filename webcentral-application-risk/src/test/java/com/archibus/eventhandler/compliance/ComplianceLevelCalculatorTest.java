package com.archibus.eventhandler.compliance;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.utility.ExceptionBase;

/**
 * Provides TODO. Define test class for ComplianceLevelCalculator class. -
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope.
 * Configured in TODO file.
 *
 * @since 20.3
 *
 */
@Category({ DatabaseTest.class })
public class ComplianceLevelCalculatorTest extends DataSourceTestBase {

	/**
	 * Test Create ComplianceLocations.
	 */
	public static final void testCreateComplianceLocations() {

		try {
			ComplianceLevelCalculator.calculateCompLevel();

		} catch (final ExceptionBase exceptionBase) {
			fail(exceptionBase.getLocalizedMessage());
		}
	}

}
