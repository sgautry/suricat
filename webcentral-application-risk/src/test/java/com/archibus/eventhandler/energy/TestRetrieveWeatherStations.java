package com.archibus.eventhandler.energy;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.datasource.data.DataSet;

@Category({ DatabaseTest.class })
public class TestRetrieveWeatherStations extends DataSourceTestBase {
	public void testGetWeatherStationsNearBy() {
		final RetrieveWeatherStations rws = new RetrieveWeatherStations();
		assertTrue(rws.getWeatherStationsNearBy("123", "123", "5") instanceof DataSet);
	}
}
