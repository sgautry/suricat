package com.archibus.eventhandler.energy;

import java.util.List;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;

@Category({ DatabaseTest.class })
public class TestPopulateChartData extends DataSourceTestBase {
	public void testRun() {
		try {
			assertTrue(PopulateChartDataService.run());
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}

	public void testDeleteChartPoints() {
		try {
			PopulateChartDataService.deleteChartPoints();
			final String SQL = "SELECT bl_id, value_name, time_period, VALUE, outlier FROM energy_chart_point";
			final String[] flds = { "bl_id", "value_name", "time_period", "VALUE", "outlier" };
			final List<DataRecord> records = SqlUtils.executeQuery("energy_chart_point", flds, SQL);
			assertEquals(0, records.size());
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}

	public void testPopulateElectricChartPoints() {
		try {
			PopulateChartDataService.populateElectricChartPoints();
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}

	public void testPopulateGasChartPoints() {
		try {
			PopulateChartDataService.populateGasChartPoints();
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}

	public void testPopulateRegressionChartConsumption() {
		try {
			PopulateChartDataService.populateRegressionChartConsumption();
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}

	public void testPopulateRegressionChartDemand() {
		try {
			PopulateChartDataService.populateRegressionChartDemand();
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}

	public void testPopulateRegressionChartOat() {
		try {
			PopulateChartDataService.populateRegressionChartOat();
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}
}
