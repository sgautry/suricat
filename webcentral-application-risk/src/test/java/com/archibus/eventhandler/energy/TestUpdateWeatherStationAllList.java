package com.archibus.eventhandler.energy;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;

@Category({ DatabaseTest.class })
public class TestUpdateWeatherStationAllList extends DataSourceTestBase {
	public void testGetWeatherStationAllData() {
		try {
			final UpdateWeatherStationAllList WeatherStationAllList = new UpdateWeatherStationAllList();
			WeatherStationAllList.populateWeatherStationList();
		} catch (final Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			releaseTestContext(this.c);
		}
	}
}
