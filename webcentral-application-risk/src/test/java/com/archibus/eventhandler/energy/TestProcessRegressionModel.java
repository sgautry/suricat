package com.archibus.eventhandler.energy;

import com.archibus.datasource.DataSourceTestBase;

public class TestProcessRegressionModel extends DataSourceTestBase {
    public void testCalculateRegressionModel() {
        try {
            final ProcessRegressionModel prm = new ProcessRegressionModel();
            prm.run();
        } catch (final Throwable t) {
            t.printStackTrace();
            fail();
        } finally {
            releaseTestContext(this.c);
        }
    }
}
