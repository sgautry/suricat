package com.archibus.eventhandler.compliance;

import java.util.List;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.jobmanager.JobBase;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * Compliance Survey Processor.
 *
 * Contains processing rules for Compliance Questionnaire Surveys
 *
 * @author KE
 */

@SuppressWarnings({ "PMD.AvoidUsingSql" })
public class ComplianceSurveyProcessor extends JobBase {

	/**
	 * Constant: '.'.
	 */
	public static final String DOT = ".";

	/**
	 * Constant comp_level '0 - ZERO COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_0 = "0 - ZERO COMPLIANCE";

	/**
	 * Constant comp_level '1 - VERY LOW COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_1 = "1 - VERY LOW COMPLIANCE";

	/**
	 * Constant comp_level '2 - LOW COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_2 = "2 - LOW COMPLIANCE";

	/**
	 * Constant comp_level '3 - MEDIUM LOW COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_3 = "3 - MEDIUM LOW COMPLIANCE";

	/**
	 * Constant comp_level '4 - MEDIUM COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_4 = "4 - MEDIUM COMPLIANCE";

	/**
	 * Constant comp_level '5 - MEDIUM HIGH COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_5 = "5 - MEDIUM HIGH COMPLIANCE";

	/**
	 * Constant comp_level '6 - HIGH COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_6 = "6 - HIGH COMPLIANCE";

	/**
	 * Constant comp_level '7 - NEARLY FULL COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_7 = "7 - NEARLY FULL COMPLIANCE";

	/**
	 * Constant comp_level '8 - FULL/TOTAL COMPLIANCE'.
	 */
	private static final String COMP_LEVEL_8 = "8 - FULL/TOTAL COMPLIANCE";

	/**
	 * Update Compliance Level.
	 *
	 * @param questionId
	 *            - int quest_answer_ext.question_id
	 * @param compLevel
	 *            String Comp Level value
	 *
	 */
	private void updateCompLevel(final int questionId, final String compLevel) {
		final String[] fieldNames = { Constant.REGULATION, Constant.REG_PROGRAM, Constant.REG_REQUIREMENT,
				Constant.COMP_LEVEL };
		final DataSource requirementDs = DataSourceFactory.createDataSourceForFields(Constant.REGREQUIREMENT,
				fieldNames);
		final List<DataRecord> requirements = getRequirementsToUpdate(questionId);
		for (int i = 0; i < requirements.size(); i++) {
			final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
			restriction.addClause(Constant.REGREQUIREMENT, Constant.REGULATION,
					requirements.get(i).getString(Constant.REGREQUIREMENT_REGULATION), Operation.EQUALS);
			restriction.addClause(Constant.REGREQUIREMENT, Constant.REG_PROGRAM,
					requirements.get(i).getString(Constant.REGREQUIREMENT_REG_PROGRAM), Operation.EQUALS);
			restriction.addClause(Constant.REGREQUIREMENT, Constant.REG_REQUIREMENT,
					requirements.get(i).getString(Constant.REGREQUIREMENT_REG_REQUIREMENT), Operation.EQUALS);
			final List<DataRecord> records = requirementDs.getRecords(restriction);
			final DataRecord record = records.get(0);
			record.setValue(Constant.REGREQUIREMENT_COMP_LEVEL, compLevel);
			requirementDs.saveRecord(record);
		}
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 0.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel0(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_0);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 1.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel1(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_1);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 2.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel2(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_2);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 3.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel3(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_3);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 4.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel4(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_4);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 5.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel5(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_5);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 6.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel6(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_6);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 7.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel7(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_7);
	}

	/**
	 * updateCompLevel0 - Update Compliance Level to 8.
	 *
	 * @param surveyEventId
	 *            - int activity_log_id for survey event
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext.answer_id
	 *
	 */
	public void updateCompLevel8(final int surveyEventId, final int questionnaireId, final int questionId,
			final int answerId) {
		updateCompLevel(questionId, COMP_LEVEL_8);
	}

	/**
	 * getRequirementsToUpdate. Returns requirements assigned to a dummy
	 * questionnaire (status='inactive') containing the action question. Note
	 * the action question will be a child question and so the child hierarchy
	 * field must be searched for question_id.
	 *
	 * @param questionId
	 *            question_ext.question_id
	 * @return requirements List<DataRecord>
	 */
	private List<DataRecord> getRequirementsToUpdate(final int questionId) {
		String str = "SUBSTRING(hierarchy_ids,1,(CHARINDEX('|',hierarchy_ids)-1))";
		if (SqlUtils.isOracle()) {
			str = "SUBSTR(hierarchy_ids,1,(INSTR(hierarchy_ids,'|')-1))";
		}
		final String query = "SELECT regulation, reg_program, reg_requirement FROM regrequirement rr "
				+ " WHERE rr.questionnaire_id IN "
				+ "(SELECT questionnaire_id FROM questionnaire_ext WHERE status = 'inactive' AND questionnaire_id IN "
				+ "    (SELECT questionnaire_id FROM quest_question_ext qqe WHERE question_id = "
				+ "            (SELECT " + str + " FROM question_ext WHERE question_ext.question_id = " + questionId
				+ " )))";
		final DataSource requirementDs = DataSourceFactory.createDataSource();
		requirementDs.addTable(Constant.REGREQUIREMENT, DataSource.ROLE_MAIN);
		requirementDs.addField(Constant.REGULATION);
		requirementDs.addField(Constant.REG_PROGRAM);
		requirementDs.addField(Constant.REG_REQUIREMENT);
		requirementDs.addQuery(query);
		return requirementDs.getRecords();
	}
}