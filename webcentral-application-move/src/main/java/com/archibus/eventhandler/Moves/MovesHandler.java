package com.archibus.eventhandler.Moves;

import java.util.*;

import org.json.*;

import com.archibus.app.move.common.*;
import com.archibus.app.move.notify.IMoveNotifyService;
import com.archibus.app.move.order.IMoveOrderService;
import com.archibus.app.move.project.IMoveProjectService;
import com.archibus.app.move.report.IMoveReportService;
import com.archibus.app.move.request.IMoveRequestService;
import com.archibus.app.move.scenario.*;
import com.archibus.app.move.sql.MoveAssetsSqlOperator;
import com.archibus.app.move.transactions.IMoveSpaceTransactionService;
import com.archibus.app.move.transfer.IMoveDataTransferService;
import com.archibus.datasource.data.DataRecord;

/**
 * This event handler implements business logic related to Move management. Copyright (c) 2016,
 * ARCHIBUS, Inc.
 *
 * @author Zhang Yi
 *
 * @version Refactoring since 23.2, modified from 2017.
 */

public class MovesHandler {

    /**
     * Move Order service.
     */
    private IMoveOrderService moveOrderService;

    /**
     * Move Project service.
     */
    private IMoveProjectService moveProjectService;

    /**
     * Move Request service.
     */
    private IMoveRequestService moveRequestService;

    /**
     * Move Scenario service.
     */
    private IMoveScenarioService moveScenarioService;

    /**
     * Move Notify service.
     */
    private IMoveNotifyService moveNotifyService;

    /**
     * Move Report service.
     */
    private IMoveReportService moveReportService;

    /**
     * Move Data Transfer service.
     */
    private IMoveDataTransferService moveDataTransferService;

    /**
     * Move Space Transaction service.
     */
    private IMoveSpaceTransactionService moveSpaceTransactionService;

    /**
     * Move Space Transaction service.
     */
    private IMoveCalculatorService moveCalculatorService;

    /**
     * Move Scenario Assign service.
     */
    private IMoveScenarioAssignmentService moveScenarioAssignmentService;

    /**
     *
     * Schedule WFR method delegate business logic 'calculate historical employee headcount' to Move
     * Calculator Service.
     *
     */
    public void calculateHistoricalEmployeeHeadcount() {

        new MoveCalculatorService().calculateHistoricalEmployeeHeadcount();

    }

    /**
     * WFR method delegate business logic 'generate paginate report' to Move Report Service.
     *
     * @param rptType report type
     * @param projectId project code
     * @param moId move order id
     */
    public void onPaginatedReport(final String rptType, final String projectId, final String moId) {

        this.moveReportService.generatePaginateReport(rptType, projectId, moId);

    }

    /**
     * WFR method delegate business logic 'Create Move Scenario' to Move Request Service.
     *
     * @param record record object
     * @param isDefault whether is default sign
     */
    public void createMoveScenario(final DataRecord record, final boolean isDefault) {

        this.moveScenarioService.createMoveScenario(record, isDefault);
    }

    /**
     * WFR method delegate business logic 'Copy Move Scenario' to Move Request Service.
     *
     * @param record record object
     * @param origScenarioId original scenario id
     */
    public void copyMoveScenario(final DataRecord record, final String origScenarioId) {

        this.moveScenarioService.copyMoveScenario(record, origScenarioId);
    }

    /**
     * WFR method delegate business logic 'Delete Move Scenario' to Move Request Service.
     *
     * @param scenarioId scenario id
     * @param projectId project code
     */
    public void deleteMoveScenario(final String scenarioId, final String projectId) {

        this.moveScenarioService.deleteMoveScenario(scenarioId, projectId);
    }

    /**
     * WFR method delegate business logic 'Update Move Project' to Move Request Service.
     *
     * @param scenarioId scenario id
     * @param projectId project code
     */
    public void updateMoveProject(final String projectId, final String scenarioId) {

        this.moveScenarioService.updateMoveProject(projectId, scenarioId);
    }

    /**
     * WFR method delegate business logic 'Update Move Scenario' to Move Request Service.
     *
     * @param scenarioId scenario id
     * @param projectId project code
     * @param records record object list
     */
    public void updateMoveScenario(final String projectId, final String scenarioId,
            final List<Map<String, Object>> records) {

        this.moveScenarioService.updateMoveScenario(projectId, scenarioId, records);
    }

    /**
     * WFR method delegate business logic 'add individual move' to Move Order Service.
     *
     * @param record json object of a move order record
     * @param addEq boolean indicates if add equipment to the move
     * @param addTa boolean indicates if add tagged furniture to the move
     */
    public void addIndividualMove(final DataRecord record, final boolean addEq,
            final boolean addTa) {
        this.moveOrderService.addIndividualMove(record, addEq, addTa);
    }

    /**
     * WFR method delegate business logic 'add individual move new hire' to Move Order Service.
     *
     * @param record record object
     * @param addEq whether to add equipment to move
     * @param addTa whether to adding tagged furniture to move
     */
    public void addIndividualMoveNewHire(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        this.moveOrderService.addIndividualMoveNewHire(record, addEq, addTa);

    }

    /**
     * WFR method delegate business logic 'add individual move employee leaving' to Move Order
     * Service.
     *
     * @param record record object
     * @param addEq whether to add equipment to move
     * @param addTa whether to adding tagged furniture to move
     */
    public void addIndividualMoveEmployeeLeaving(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        this.moveOrderService.addIndividualMoveEmployeeLeaving(record, addEq, addTa);

    }

    /**
     * WFR method delegate business logic 'add individual move equipment' to Move Order Service.
     *
     * @param record record object
     * @param addEq whether to add equipment to move
     * @param addTa whether to adding tagged furniture to move
     */
    public void addIndividualMoveEquipment(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        this.moveOrderService.addIndividualMoveEquipment(record, addEq, addTa);
    }

    /**
     * WFR method delegate business logic 'add individual move asset' to Move Order Service.
     *
     * @param record record object
     * @param addEq whether to add equipment to move
     * @param addTa whether to adding tagged furniture to move
     */
    public void addIndividualMoveAsset(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        this.moveOrderService.addIndividualMoveAsset(record, addEq, addTa);
    }

    /**
     * WFR method delegate business logic 'add individual move room' to Move Order Service.
     *
     * @param record record object
     * @param addEq whether to add equipment to move
     * @param addTa whether to adding tagged furniture to move
     */
    public void addIndividualMoveRoom(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        this.moveOrderService.addIndividualMoveRoom(record, addEq, addTa);

    }

    /**
     * WFR method delegate business logic 'request individual move' to Move Request Service.
     *
     * @param moId move order id
     */
    public void requestIndividualMove(final String moId) {

        this.moveRequestService.requestIndividualMove(moId);

    }

    /**
     * WFR method delegate business logic 'route individual move for approval' to Move Request
     * Service.
     *
     * @param moId move order id
     * @param approveManager1 Approve Manager 1
     * @param approveManager2 Approve Manager 2
     * @param approveManager3 Approve Manager 3
     */
    public void routeIndividualMoveForApproval(final String moId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        this.moveRequestService.routeIndividualMoveForApproval(moId, approveManager1,
            approveManager2, approveManager3);

    }

    /**
     * WFR method delegate business logic 'approve individual move' to Move Request Service.
     *
     * @param moId move order id
     * @param approveManager1 Approve Manager 1
     * @param approveManager2 Approve Manager 2
     * @param approveManager3 Approve Manager 3
     */
    public void approveIndividualMove(final String moId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        this.moveRequestService.approveIndividualMove(moId, approveManager1, approveManager2,
            approveManager3);
    }

    /**
     * WFR method delegate business logic 'auto approve individual move' to Move Request Service.
     *
     * @param moId move order id
     */
    public void autoApproveIndividualMove(final String moId) {

        this.moveRequestService.autoApproveIndividualMove(moId);

    }

    /**
     * WFR method delegate business logic 'reject individual move' to Move Request Service.
     *
     * @param moId move order id
     * @param approveManager1 Approve Manager 1
     * @param approveManager2 Approve Manager 2
     * @param approveManager3 Approve Manager 3
     */
    public void rejectIndividualMove(final String moId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        this.moveRequestService.rejectIndividualMove(moId, approveManager1, approveManager2,
            approveManager3);
    }

    /**
     * WFR method delegate business logic 'issue individual move' to Move Request Service.
     *
     * @param moId move order id
     * @param requestor request employeee id
     */
    public void issueIndividualMove(final String moId, final String requestor) {

        this.moveRequestService.issueIndividualMove(moId, requestor);

    }

    /**
     * WFR method delegate business logic 'close individual move' to Move Request Service.
     *
     * @param moId move order id
     */
    public void closeIndividualMove(final String moId) {

        this.moveRequestService.closeIndividualMove(moId);

    }

    /**
     * WFR method delegate business logic 'close group move' to Move Request Service.
     *
     * @param projectId move project id
     */
    public void closeGroupMove(final String projectId) {
        this.moveRequestService.closeGroupMove(projectId);
    }

    /**
     * WFR method delegate business logic 'request group move' to Move Request Service.
     *
     * @param projectId project code
     */
    public void requestGroupMove(final String projectId) {

        this.moveRequestService.requestGroupMove(projectId);

    }

    /**
     * WFR method delegate business logic 'route group move for approval' to Move Request Service.
     *
     * @param projectId project code
     * @param approveManager1 Approve Manager 1
     * @param approveManager2 Approve Manager 2
     * @param approveManager3 Approve Manager 3
     */
    public void routeGroupMoveForApproval(final String projectId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        this.moveRequestService.routeGroupMoveForApproval(projectId, approveManager1,
            approveManager2, approveManager3);

    }

    /**
     * WFR method delegate business logic 'approve group move' to Move Request Service.
     *
     * @param projectId project code
     * @param approveManager1 Approve Manager 1
     * @param approveManager2 Approve Manager 2
     * @param approveManager3 Approve Manager 3
     */
    public void approveGroupMove(final String projectId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        this.moveRequestService.approveGroupMove(projectId, approveManager1, approveManager2,
            approveManager3);

    }

    /**
     * WFR method delegate business logic 'auto approve group move' to Move Request Service.
     *
     * @param projectId project code
     */
    public void autoApproveGroupMove(final String projectId) {

        this.moveRequestService.autoApproveGroupMove(projectId);

    }

    /**
     * WFR method delegate business logic 'reject group move' to Move Request Service.
     *
     * @param projectId project code
     * @param approveManager1 Approve Manager 1
     * @param approveManager2 Approve Manager 2
     * @param approveManager3 Approve Manager 3
     */
    public void rejectGroupMove(final String projectId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        this.moveRequestService.rejectGroupMove(projectId, approveManager1, approveManager2,
            approveManager3);

    }

    /**
     * WFR method delegate business logic 'reject group move' to Move Request Service.
     *
     * @param projectId project code
     * @param requestor request employee id
     */
    public void issueGroupMove(final String projectId, final String requestor) {

        this.moveRequestService.issueGroupMove(projectId, requestor);

    }

    /**
     * WFR method delegate business logic 'add individual move action item' to Move Request Service.
     *
     * @param record record object
     */
    public void addActionIndividualMove(final DataRecord record) {

        this.moveRequestService.addActionIndividualMove(record);

    }

    /**
     * WFR method delegate business logic 'add group move action items' to Move Request Service.
     *
     * @param record record object
     */
    public void addActionGroupMove(final DataRecord record) {

        this.moveRequestService.addActionGroupMove(record);

    }

    /**
     * WFR method delegate business logic 'add a new project' to Move Project Service.
     *
     * @param record record object
     */
    public void addProjectMoveRecord(final DataRecord record) {

        this.moveProjectService.addMoveProject(record);

    }

    /**
     * WFR method delegate business logic 'add move project' to Move Project Service.
     *
     * @param record record object
     */
    public void addProjectMove(final DataRecord record) {

        this.moveProjectService.addProjectMove(record);

    }

    /**
     * WFR method delegate business logic 'add move project new hire' to Move Project Service.
     *
     * @param record record object
     */
    public void addProjectMoveNewHire(final DataRecord record) {

        this.moveProjectService.addProjectMoveNewHire(record);

    }

    /**
     * WFR method delegate business logic 'add move project employee leaving' to Move Project
     * Service.
     *
     * @param record record object
     */
    public void addProjectMoveEmployeeLeaving(final DataRecord record) {

        this.moveProjectService.addProjectMoveEmployeeLeaving(record);

    }

    /**
     * WFR method delegate business logic 'add move project equipment' to Move Project Service.
     *
     * @param record record object
     */
    public void addProjectMoveEquipment(final DataRecord record) {

        this.moveProjectService.addProjectMoveEquipment(record);

    }

    /**
     * WFR method delegate business logic 'add move project asset' to Move Project Service.
     *
     * @param record record object
     */
    public void addProjectMoveAsset(final DataRecord record) {

        this.moveProjectService.addProjectMoveAsset(record);

    }

    /**
     * WFR method delegate business logic 'add move project room' to Move Project Service.
     *
     * @param record record object
     */
    public void addProjectMoveRoom(final DataRecord record) {

        this.moveProjectService.addProjectMoveRoom(record);

    }

    /**
     * WFR method delegate business logic 'add bulk moves' to Move Project Service.
     *
     * @param record record object
     */
    public void addBulkMoves(final DataRecord record) {

        this.moveProjectService.addBulkMoves(record);

    }

    /**
     * WFR method delegate business logic 'delete workspace transactions' to Move Space Transaction
     * Service.
     *
     * @param tableName <String> table name, the value should be mo or project
     * @param pkeyValue <String> primary key value
     */
    public void onProcessDeleteRmpctRecord(final String tableName, final String pkeyValue) {

        this.moveSpaceTransactionService.onProcessDeleteRmpctRecord(tableName, pkeyValue);

    }

    /**
     * WFR method delegate business logic 'delete associated transactions' to Move Space Transaction
     * Service.
     *
     * @param moIds list of mo ids that deleted on client side
     */
    public void deleteRequestedSpaceTransactions(final String moIds) {

        this.moveSpaceTransactionService.deleteRequestedSpaceTransactions(moIds);

    }

    /**
     * WFR method delegate business logic 'update service request status for review and estimate' to
     * Move Space Transaction Service.
     *
     * @param tableName <String> table name, the value should be mo or project
     * @param pkeyValue <String> primary key value
     */
    public void updateStatusForReviewAndEstimate(final String tableName, final String pkeyValue) {

        this.moveSpaceTransactionService.updateStatusForReviewAndEstimate(tableName, pkeyValue);
    }

    /**
     * WFR method delegate business logic 'generate work requests' to Move Space Transaction
     * Service.
     *
     * @param activityLogId activity log id
     * @param projectId peroject code
     * @param moId move order id
     */
    public void genWorkRequest(final String activityLogId, final String projectId,
            final String moId) {

        this.moveSpaceTransactionService.genWorkRequest(activityLogId, projectId, moId);

    }

    /**
     * WFR method delegate business logic 'commit move scenario assignments' to Move Scenario
     * Assignments Service.
     *
     * @param assignments employee or team move assignments
     * @param assignType assignment type
     */
    public void commitAssignments(final JSONArray assignments, final String assignType) {

        this.moveScenarioAssignmentService.commitAssignments(assignments, assignType);

    }

    /**
     * WFR method delegate business logic 'commit move scenario assignments' to Move Scenario
     * Assignments Service.
     *
     * @param moveScenarioEmId move scenario em id
     * @param assignType assignment type
     */
    public void removeAssignment(final int moveScenarioEmId, final String assignType) {

        this.moveScenarioAssignmentService.removeAssignment(moveScenarioEmId, assignType);

    }

    /**
     * Copy selected occupancy scenario to move scenario.
     *
     * @param projectId project id
     * @param scenarioId scenario id
     * @param filename file name
     * @param toBlId to building code
     * @param toFlId to floor code
     * @param fromLayout copied scenario layout
     */
    public void copyScenarioOccupancy(final String projectId, final String scenarioId,
            final String filename, final String toBlId, final String toFlId,
            final JSONObject fromLayout) {

        final String fromFileName = fromLayout.getString("mo_scenario_em.filename");

        this.moveScenarioService.copyScenarioOccupancy(projectId, scenarioId, filename, toBlId,
            toFlId, fromFileName);

        this.moveScenarioAssignmentService.addLayoutPlaceholder(fromLayout);
    }

    /**
     * Delete teams to which from_team_id or to_team_id equals .
     *
     * @param teams selected teams
     * @param projectId project id
     */
    public void deleteSelectedTeams(final String teams, final String projectId) {

        this.moveScenarioService.deleteSelectedTeams(teams, projectId);
    }

    /**
     * WFR method delegate business logic 'remove move scenario assignment' to Move Scenario
     * Assignments Service.
     *
     * @param projectId project code
     * @param scenarioId scenario id
     */
    public void validLayoutLocation(final String projectId, final String scenarioId) {

        this.moveScenarioAssignmentService.validLayoutLocation(projectId, scenarioId);

    }

    /**
     * WFR method delegate business logic 'remove move scenario assignment' to Move Scenario
     * Assignments Service.
     *
     * @param record record object
     */
    public void deleteLayout(final JSONObject record) {

        this.moveScenarioAssignmentService.deleteLayout(record);

    }

    /**
     * Getter for the moveScenarioService property.
     *
     * @return the moveScenarioService property.
     */
    public IMoveScenarioService getMoveScenarioService() {
        return this.moveScenarioService;
    }

    /**
     * Setter for the moveScenarioService property.
     *
     * @param moveScenarioService the moveScenarioService to set.
     */
    public void setMoveScenarioService(final IMoveScenarioService moveScenarioService) {
        this.moveScenarioService = moveScenarioService;
    }

    /**
     * Getter for the moveOrderService property.
     *
     * @return the moveOrderService property.
     */
    public IMoveOrderService getMoveOrderService() {
        return this.moveOrderService;
    }

    /**
     * Setter for the moveOrderService property.
     *
     * @param moveOrderService the moveOrderService to set.
     */
    public void setMoveOrderService(final IMoveOrderService moveOrderService) {
        this.moveOrderService = moveOrderService;
    }

    /**
     * Getter for the moveProjectService property.
     *
     * @return the moveProjectService property.
     */
    public IMoveProjectService getMoveProjectService() {
        return this.moveProjectService;
    }

    /**
     * Setter for the moveProjectService property.
     *
     * @param moveProjectService the moveProjectService to set.
     */
    public void setMoveProjectService(final IMoveProjectService moveProjectService) {
        this.moveProjectService = moveProjectService;
    }

    /**
     * Getter for the moveRequestService property.
     *
     * @return the moveRequestService property.
     */
    public IMoveRequestService getMoveRequestService() {
        return this.moveRequestService;
    }

    /**
     * Setter for the moveRequestService property.
     *
     * @param moveRequestService the moveRequestService to set.
     */
    public void setMoveRequestService(final IMoveRequestService moveRequestService) {
        this.moveRequestService = moveRequestService;
    }

    /**
     * Getter for the moveNotifyService property.
     *
     * @return the moveNotifyService property.
     */
    public IMoveNotifyService getMoveNotifyService() {
        return this.moveNotifyService;
    }

    /**
     * Setter for the moveNotifyService property.
     *
     * @param moveNotifyService the moveNotifyService to set.
     */
    public void setMoveNotifyService(final IMoveNotifyService moveNotifyService) {
        this.moveNotifyService = moveNotifyService;
    }

    /**
     * Getter for the moveReportService property.
     *
     * @return the moveReportService property.
     */
    public IMoveReportService getMoveReportService() {
        return this.moveReportService;
    }

    /**
     * Setter for the moveReportService property.
     *
     * @param moveReportService the moveReportService to set.
     */
    public void setMoveReportService(final IMoveReportService moveReportService) {
        this.moveReportService = moveReportService;
    }

    /**
     * Getter for the moveDataTransferService property.
     *
     * @return the moveDataTransferService property.
     */
    public IMoveDataTransferService getMoveDataTransferService() {
        return this.moveDataTransferService;
    }

    /**
     * Setter for the moveDataTransferService property.
     *
     * @param moveDataTransferService the moveDataTransferService to set.
     */
    public void setMoveDataTransferService(final IMoveDataTransferService moveDataTransferService) {
        this.moveDataTransferService = moveDataTransferService;
    }

    /**
     * Getter for the moveSpaceTransactionService property.
     *
     * @return the moveSpaceTransactionService property.
     */
    public IMoveSpaceTransactionService getMoveSpaceTransactionService() {
        return this.moveSpaceTransactionService;
    }

    /**
     * Setter for the moveSpaceTransactionService property.
     *
     * @param moveSpaceTransactionService the moveSpaceTransactionService to set.
     */
    public void setMoveSpaceTransactionService(
            final IMoveSpaceTransactionService moveSpaceTransactionService) {
        this.moveSpaceTransactionService = moveSpaceTransactionService;
    }

    /**
     * WFR method copy selected occupancy scenario to move scenario.
     *
     * @param projectId project code
     * @param scenarioId scenario name
     * @param occupancyScenario occupancy scenarios list
     */
    public void copyOccupancyScenarioToMoveScenario(final String projectId, final String scenarioId,
            final Map<String, String> occupancyScenario) {

        // this.moveScenarioService.copyOccupancyScenarioToMoveScenario(projectId,
        // scenarioId, occupancyScenario);
    }

    /**
     * Getter for the moveCalculatorService property.
     *
     * @return the moveCalculatorService property.
     */
    public IMoveCalculatorService getMoveCalculatorService() {
        return this.moveCalculatorService;
    }

    /**
     * Setter for the moveCalculatorService property.
     *
     * @param moveCalculatorService the moveCalculatorService to set.
     */
    public void setMoveCalculatorService(final IMoveCalculatorService moveCalculatorService) {
        this.moveCalculatorService = moveCalculatorService;
    }

    /**
     * Getter for the moveScenarioAssignmentService property.
     *
     * @return the moveScenarioAssignmentService property.
     */
    public IMoveScenarioAssignmentService getMoveScenarioAssignmentService() {
        return this.moveScenarioAssignmentService;
    }

    /**
     * Setter for the moveScenarioAssignmentService property.
     *
     * @param moveScenarioAssignmentService the moveScenarioAssignmentService to set.
     */
    public void setMoveScenarioAssignmentService(
            final IMoveScenarioAssignmentService moveScenarioAssignmentService) {
        this.moveScenarioAssignmentService = moveScenarioAssignmentService;
    }

    /**
     * Assign employees to team.
     *
     * @param emIds selected employees id
     * @param teamId team id
     * @param projectId project id
     * @param type move type
     * @param dateStartReq request start date
     * @param dateToPerform perform date
     */
    public void assignEmployeesToTeam(final List<String> emIds, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrderService.assignEmployeesToTeam(emIds, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

    /**
     * Assign employees to team.
     *
     * @param rooms selected room info
     * @param teamId team id
     * @param projectId project id
     * @param type move type
     * @param dateStartReq request start date
     * @param dateToPerform perform date
     */
    public void assignRoomsToTeam(final JSONArray rooms, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrderService.assignRoomsToTeam(rooms, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

    /**
     * Remove employees from team.
     *
     * @param emIds selected employees id
     * @param teamId team id
     * @param projectId project id
     * @param type move type
     * @param dateStartReq request start date
     * @param dateToPerform perform date
     */
    public void removeEmployeesFromTeam(final List<String> emIds, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrderService.removeEmployeesFromTeam(emIds, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

    /**
     * Remove rooms from team.
     *
     * @param rooms selected rooms id
     * @param teamId team id
     * @param projectId project id
     * @param type move type
     * @param dateStartReq request start date
     * @param dateToPerform perform date
     */
    public void removeRoomsFromTeam(final JSONArray rooms, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrderService.removeRoomsFromTeam(rooms, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

    /**
     * Synchronize scenario with move project.
     *
     * @param projectId project code
     * @param scenarioId scenario name
     */
    public void synchronizeScenario(final String projectId, final String scenarioId) {

        this.moveScenarioService.synchronizeScenario(projectId, scenarioId);
    }

    /**
     * Completed all selected move orders.
     *
     * @param moIdList move order id list
     */
    public void completeSelectedMoves(final String moIdList) {

        this.moveRequestService.completeSelectedMoves(moIdList);
    }

    /**
     * Completed all selected action items.
     *
     * @param activityLogIdList action item id list
     */
    public void completeSelectedActions(final String activityLogIdList) {

        this.moveRequestService.completeSelectedActions(activityLogIdList);

    }

    /**
     * Insert assements after adding a move of an employee or a room.
     *
     * @param primaryKeys list of primary key
     * @param emId employee code
     * @param moId move order code
     * @param table table name for move asset
     * @param assetsFor table name for move asset
     **/
    public void addEmAssetsMove(final List<String> primaryKeys, final String emId,
            final String moId, final String table, final String assetsFor) {

        MoveAssetsSqlOperator.addEmAssetsMove(primaryKeys, emId, moId, table, assetsFor);

    }

    /**
     * Update associated service request status from mo record or project record.
     *
     * @param tableName <String> table name, the value should be mo or project
     * @param pkeyValue <String> primary key value
     */
    public void updateAssociatedServiceRequestStatus(final String tableName,
            final String pkeyValue) {

        if ("mo".equals(tableName)) {

            this.moveSpaceTransactionService
                .updateAssociatedServiceRequestStatusForIndividualMove(pkeyValue);

        } else {

            this.moveSpaceTransactionService
                .updateAssociatedServiceRequestStatusForGrouplMove(pkeyValue);
        }
    }

    /**
     * Delete all selected team move orders.
     *
     * @param moIdList move order id list
     */
    public void deleteSelectedTeamMoves(final String moIdList) {

        this.moveRequestService.deleteSelectedTeamMoves(moIdList);
    }

}
