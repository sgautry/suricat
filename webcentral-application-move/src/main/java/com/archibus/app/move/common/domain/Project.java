package com.archibus.app.move.common.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Project.
 * <p>
 * Mapped to project table.
 *
 * @author Zhang Yi
 *
 */
public class Project {

	/**
	 * ID of the project.
	 */
	private String id;

	/**
	 * Employee ID.
	 */
	private String employeeId;

	/**
	 * Project Status.
	 */
	private String status;

	/**
	 * Project Closed Date.
	 */
	private Date dateClosed;

	/**
	 * Project Manager.
	 */
	private String projectManager;

	/**
	 * Project department Contact.
	 */
	private String departmentContact;

	/**
	 * Project requester.
	 */
	private String requestor;

	/**
	 * Project requestor's Phone.
	 */
	private String phoneRequestor;

	/**
	 * Project division Id.
	 */
	private String divisionId;

	/**
	 * Project department Id.
	 */
	private String departmentId;

	/**
	 * Project account Id.
	 */
	private String accountId;

	/**
	 * Project Department Contact's Phone.
	 */
	private String phoneDepartmentContact;

	/**
	 * Project description.
	 */
	private String description;

	/**
	 * Project building code.
	 */
	private String buildingId;

	/**
	 * Project Contact's id.
	 */
	private String contactId;

	/**
	 * Project Start Date.
	 */
	private Date dateStart;

	/**
	 * Project End Date.
	 */
	private Date dateEnd;

	/**
	 * Project Create Date.
	 */
	private Date dateCreated;

	/**
	 * Project Type.
	 */
	private String projectType;

	/**
	 * Project Site code.
	 */
	private String siteId;

	/**
	 * Project Move approve Manager1.
	 */
	private String approveManager1;

	/**
	 * Project Move approve Manager2.
	 */
	private String approveManager2;

	/**
	 * Project Move approve Manager3.
	 */
	private String approveManager3;

	/**
	 * Project Move approve Manager1 status.
	 */
	private String approveManager1Status;

	/**
	 * Project Move approve Manager2 status.
	 */
	private String approveManager2Status;

	/**
	 * Project Move approve Manager3 status.
	 */
	private String approveManager3Status;

	/**
	 * Project Move approve Manager1 status.
	 */
	private Date approveManager1Date;

	/**
	 * Project Move approve Manager2 status.
	 */
	private Date approveManager2Date;

	/**
	 * Project Move approve Manager3 status.
	 */
	private Date approveManager3Date;

	/**
	 * Project Move approve Date.
	 */
	private Date dateApproved;

	/**
	 * Project Move Issue Date.
	 */
	private Date dateIssued;

	/**
	 * Project Move Request Date.
	 */
	private Date dateRequested;

	/**
	 * Getter for the dateCreated property.
	 *
	 * @return the dateCreated property.
	 */
	public Date getDateCreated() {
		return this.dateCreated;
	}

	/**
	 * Getter for the dateRequested property.
	 *
	 * @return the dateRequested property.
	 */
	public Date getDateRequested() {
		return this.dateRequested;
	}

	/**
	 * Setter for the dateRequested property.
	 *
	 * @param dateRequested
	 *            the dateRequested to set.
	 */
	public void setDateRequested(final Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	/**
	 * Getter for the approveManager1Status property.
	 *
	 * @return the approveManager1Status property.
	 */
	public String getApproveManager1Status() {
		return this.approveManager1Status;
	}

	/**
	 * Setter for the approveManager1Status property.
	 *
	 * @param approveManager1Status
	 *            the approveManager1Status to set.
	 */
	public void setApproveManager1Status(final String approveManager1Status) {
		this.approveManager1Status = approveManager1Status;
	}

	/**
	 * Getter for the approveManager2Status property.
	 *
	 * @return the approveManager2Status property.
	 */
	public String getApproveManager2Status() {
		return this.approveManager2Status;
	}

	/**
	 * Setter for the approveManager2Status property.
	 *
	 * @param approveManager2Status
	 *            the approveManager2Status to set.
	 */
	public void setApproveManager2Status(final String approveManager2Status) {
		this.approveManager2Status = approveManager2Status;
	}

	/**
	 * Getter for the approveManager3Status property.
	 *
	 * @return the approveManager3Status property.
	 */
	public String getApproveManager3Status() {
		return this.approveManager3Status;
	}

	/**
	 * Setter for the approveManager3Status property.
	 *
	 * @param approveManager3Status
	 *            the approveManager3Status to set.
	 */
	public void setApproveManager3Status(final String approveManager3Status) {
		this.approveManager3Status = approveManager3Status;
	}

	/**
	 * Getter for the approveManager1Date property.
	 *
	 * @return the approveManager1Date property.
	 */
	public Date getApproveManager1Date() {
		return this.approveManager1Date;
	}

	/**
	 * Setter for the approveManager1Date property.
	 *
	 * @param approveManager1Date
	 *            the approveManager1Date to set.
	 */
	public void setApproveManager1Date(final Date approveManager1Date) {
		this.approveManager1Date = approveManager1Date;
	}

	/**
	 * Getter for the approveManager2Date property.
	 *
	 * @return the approveManager2Date property.
	 */
	public Date getApproveManager2Date() {
		return this.approveManager2Date;
	}

	/**
	 * Setter for the approveManager2Date property.
	 *
	 * @param approveManager2Date
	 *            the approveManager2Date to set.
	 */
	public void setApproveManager2Date(final Date approveManager2Date) {
		this.approveManager2Date = approveManager2Date;
	}

	/**
	 * Getter for the approveManager3Date property.
	 *
	 * @return the approveManager3Date property.
	 */
	public Date getApproveManager3Date() {
		return this.approveManager3Date;
	}

	/**
	 * Setter for the approveManager3Date property.
	 *
	 * @param approveManager3Date
	 *            the approveManager3Date to set.
	 */
	public void setApproveManager3Date(final Date approveManager3Date) {
		this.approveManager3Date = approveManager3Date;
	}

	/**
	 * Getter for the dateApproved property.
	 *
	 * @return the dateApproved property.
	 */
	public Date getDateApproved() {
		return this.dateApproved;
	}

	/**
	 * Setter for the dateApproved property.
	 *
	 * @param dateApproved
	 *            the dateApproved to set.
	 */
	public void setDateApproved(final Date dateApproved) {
		this.dateApproved = dateApproved;
	}

	/**
	 * Getter for the approveManager1 property.
	 *
	 * @return the approveManager1 property.
	 */
	public String getApproveManager1() {
		return this.approveManager1;
	}

	/**
	 * Setter for the approveManager1 property.
	 *
	 * @param approveManager1
	 *            the approveManager1 to set.
	 */
	public void setApproveManager1(final String approveManager1) {
		this.approveManager1 = approveManager1;
	}

	/**
	 * Getter for the approveManager2 property.
	 *
	 * @return the approveManager2 property.
	 */
	public String getApproveManager2() {
		return this.approveManager2;
	}

	/**
	 * Setter for the approveManager2 property.
	 *
	 * @param approveManager2
	 *            the approveManager2 to set.
	 */
	public void setApproveManager2(final String approveManager2) {
		this.approveManager2 = approveManager2;
	}

	/**
	 * Getter for the approveManager3 property.
	 *
	 * @return the approveManager3 property.
	 */
	public String getApproveManager3() {
		return this.approveManager3;
	}

	/**
	 * Setter for the approveManager3 property.
	 *
	 * @param approveManager3
	 *            the approveManager3 to set.
	 */
	public void setApproveManager3(final String approveManager3) {
		this.approveManager3 = approveManager3;
	}

	/**
	 * Setter for the dateCreated property.
	 *
	 * @param dateCreated
	 *            the dateCreated to set.
	 */
	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * Getter for the dateStart property.
	 *
	 * @return the dateStart property.
	 */
	public Date getDateStart() {
		return this.dateStart;
	}

	/**
	 * Setter for the dateStart property.
	 *
	 * @param dateStart
	 *            the dateStart to set.
	 */
	public void setDateStart(final Date dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for the dateEnd property.
	 *
	 * @return the dateEnd property.
	 */
	public Date getDateEnd() {
		return this.dateEnd;
	}

	/**
	 * Setter for the dateEnd property.
	 *
	 * @param dateEnd
	 *            the dateEnd to set.
	 */
	public void setDateEnd(final Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the contactId property.
	 *
	 * @return the contactId property.
	 */
	public String getContactId() {
		return this.contactId;
	}

	/**
	 * Setter for the contactId property.
	 *
	 * @param contactId
	 *            the contactId to set.
	 */
	public void setContactId(final String contactId) {
		this.contactId = contactId;
	}

	/**
	 * Getter for the employeeId property.
	 *
	 * @return the employeeId property.
	 */
	public String getEmployeeId() {
		return this.employeeId;
	}

	/**
	 * Setter for the employeeId property.
	 *
	 * @param employeeId
	 *            the employeeId to set.
	 */
	public void setEmployeeId(final String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the dateClosed property.
	 *
	 * @return the dateClosed property.
	 */
	public Date getDateClosed() {
		return this.dateClosed;
	}

	/**
	 * Setter for the dateClosed property.
	 *
	 * @param dateClosed
	 *            the dateClosed to set.
	 */
	public void setDateClosed(final Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter for the projectManager property.
	 *
	 * @return the projectManager property.
	 */
	public String getProjectManager() {
		return this.projectManager;
	}

	/**
	 * Setter for the projectManager property.
	 *
	 * @param projectManager
	 *            the projectManager to set.
	 */
	public void setProjectManager(final String projectManager) {
		this.projectManager = projectManager;
	}

	/**
	 * Getter for the departmentContact property.
	 *
	 * @return the departmentContact property.
	 */
	public String getDepartmentContact() {
		return this.departmentContact;
	}

	/**
	 * Setter for the departmentContact property.
	 *
	 * @param departmentContact
	 *            the departmentContact to set.
	 */
	public void setDepartmentContact(final String departmentContact) {
		this.departmentContact = departmentContact;
	}

	/**
	 * Getter for the requestor property.
	 *
	 * @return the requestor property.
	 */
	public String getRequestor() {
		return this.requestor;
	}

	/**
	 * Setter for the requestor property.
	 *
	 * @param requestor
	 *            the requestor to set.
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}

	/**
	 * Getter for the phoneRequestor property.
	 *
	 * @return the phoneRequestor property.
	 */
	public String getPhoneRequestor() {
		return this.phoneRequestor;
	}

	/**
	 * Setter for the phoneRequestor property.
	 *
	 * @param phoneRequestor
	 *            the phoneRequestor to set.
	 */
	public void setPhoneRequestor(final String phoneRequestor) {
		this.phoneRequestor = phoneRequestor;
	}

	/**
	 * Getter for the phoneDepartmentContact property.
	 *
	 * @return the phoneDepartmentContact property.
	 */
	public String getPhoneDepartmentContact() {
		return this.phoneDepartmentContact;
	}

	/**
	 * Setter for the phoneDepartmentContact property.
	 *
	 * @param phoneDepartmentContact
	 *            the phoneDepartmentContact to set.
	 */
	public void setPhoneDepartmentContact(final String phoneDepartmentContact) {
		this.phoneDepartmentContact = phoneDepartmentContact;
	}

	/**
	 * Getter for the divisionId property.
	 *
	 * @return the divisionId property.
	 */
	public String getDivisionId() {
		return this.divisionId;
	}

	/**
	 * Setter for the divisionId property.
	 *
	 * @param divisionId
	 *            the divisionId to set.
	 */
	public void setDivisionId(final String divisionId) {
		this.divisionId = divisionId;
	}

	/**
	 * Getter for the departmentId property.
	 *
	 * @return the departmentId property.
	 */
	public String getDepartmentId() {
		return this.departmentId;
	}

	/**
	 * Setter for the departmentId property.
	 *
	 * @param departmentId
	 *            the departmentId to set.
	 */
	public void setDepartmentId(final String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Getter for the accountId property.
	 *
	 * @return the accountId property.
	 */
	public String getAccountId() {
		return this.accountId;
	}

	/**
	 * Setter for the accountId property.
	 *
	 * @param accountId
	 *            the accountId to set.
	 */
	public void setAccountId(final String accountId) {
		this.accountId = accountId;
	}

	/**
	 * Getter for the projectType property.
	 *
	 * @return the projectType property.
	 */
	public String getProjectType() {
		return this.projectType;
	}

	/**
	 * Setter for the projectType property.
	 *
	 * @param projectType
	 *            the projectType to set.
	 */
	public void setProjectType(final String projectType) {
		this.projectType = projectType;
	}

	/**
	 * Getter for the siteId property.
	 *
	 * @return the siteId property.
	 */
	public String getSiteId() {
		return this.siteId;
	}

	/**
	 * Setter for the siteId property.
	 *
	 * @param siteId
	 *            the siteId to set.
	 */
	public void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the dateIssued property.
	 *
	 * @return the dateIssued property.
	 */
	public Date getDateIssued() {
		return this.dateIssued;
	}

	/**
	 * Setter for the dateIssued property.
	 *
	 * @param dateIssued
	 *            the dateIssued to set.
	 */
	public void setDateIssued(final Date dateIssued) {
		this.dateIssued = dateIssued;
	}
}
