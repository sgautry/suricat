package com.archibus.app.move.scenario;

import java.util.*;

import com.archibus.datasource.data.DataRecord;
import com.archibus.utility.ExceptionBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move Scenario related business behaviors.
 * <p>
 *
 * Referenced by Workflow rule handler class of Move Management to delegate
 * their wfr methods related to move scenario logics.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveScenarioService {

	/**
	 * Insert new move scenario
	 *
	 * @param record
	 * @param isDefault
	 *
	 * @return scenario id
	 */
	String createMoveScenario(final DataRecord record, final boolean isDefault) throws ExceptionBase;

	/**
	 * Copy selected scenario
	 *
	 * @param record
	 * @param origScenarioId
	 */
	void copyMoveScenario(final DataRecord record, final String origScenarioId) throws ExceptionBase;

	/**
	 * Delete selected move scenario
	 *
	 * @param scenarioId
	 * @param projectId
	 */
	void deleteMoveScenario(final String scenarioId, final String projectId);

	/**
	 * Update move project with data from selected scenario
	 *
	 * @param projectId
	 * @param scenarioId
	 */
	void updateMoveProject(final String projectId, final String scenarioId);

	/**
	 * Update changes to selected move scenario first all settings are reseted
	 * to default and after this new changes are saved
	 *
	 * @param projectId
	 * @param scenarioId
	 * @param records
	 */
	void updateMoveScenario(final String projectId, final String scenarioId, final List<Map<String, Object>> records);

	/**
	 * Synchronize scenario with project delete records that no longer exist in
	 * project insert records from project that not exists in scenario
	 *
	 * @param projectId
	 * @param scenarioId
	 */
	void synchronizeScenario(final String projectId, final String scenarioId);

	/**
	 * Copy selected occupancy scenario to move scenario.
	 *
	 * @param projectId
	 *            project id
	 * @param scenarioId
	 *            scenario id
	 * @param filename
	 *            file name
	 * @param toBlId
	 *            to building id
	 * @param toFlId
	 *            to floor id
	 * @param occupancyScenarioRecord
	 *            copied scenario record
	 */
	void copyScenarioOccupancy(final String projectId, final String scenarioId, final String filename,
			final String toBlId, String toFlId, String occupanyScenariofilename);

	/**
	 * Delete teams to which from_team_id or to_team_id equals .
	 *
	 * @param teams
	 *            selected teams
	 *
	 * @param projectId
	 *            project id
	 */
	void deleteSelectedTeams(final String teams, String projectId);
}
