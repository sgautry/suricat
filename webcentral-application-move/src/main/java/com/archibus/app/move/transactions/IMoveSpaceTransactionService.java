package com.archibus.app.move.transactions;

import com.archibus.app.move.common.domain.*;
import com.archibus.utility.ExceptionBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Workspace Transaction related business behaviors.
 * <p>
 *
 * Referenced by Workflow rule handler class of Move Management to delegate
 * their wfr methods related to Workspace Transaction logics.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveSpaceTransactionService {

	/**
	 * Record Space Transactions fired by a group move.
	 *
	 * @param project
	 *            group move project
	 * @param isApproveTransaction
	 *            boolean sign indicate if currently is going to approve the
	 *            transaction
	 */
	void recordSpaceTransactions(Project project, boolean isApproveTransaction);

	/**
	 * Record Space Transactions fired by an individual move.
	 *
	 * @param mo
	 *            move order
	 * @param isApproveTransaction
	 *            boolean sign indicate if currently is going to approve the
	 *            transaction
	 */
	void recordSpaceTransactions(MoveOrder mo, boolean isApproveTransaction);

	/**
	 * Create service request for an individual move.
	 *
	 * @param moId
	 *            move order id
	 */
	void createServiceRequestForIndvidualMove(String moId);

	/**
	 * Create service request for a group move.
	 *
	 * @param projectId
	 *            project code
	 */
	void createServiceRequestForGroupMove(String projectId);

	/**
	 * Update space transactions when rejecting an individual move.
	 *
	 * @param mo
	 *            move order
	 */
	void updateForRejectIndividualMove(MoveOrder mo);

	/**
	 * Update space transactions when rejecting a group move.
	 *
	 * @param project
	 *            group move project
	 */
	void updateForRejectGroupMove(Project project);

	/**
	 * Update associated service requests for an individual move by move order
	 * id.
	 *
	 * @param moId
	 *            move order id
	 */
	void updateAssociatedServiceRequestStatusForIndividualMove(final String moId);

	/**
	 * Update associated service requests for a group move by move project id.
	 *
	 * @param projectId
	 *            project id
	 */
	void updateAssociatedServiceRequestStatusForGrouplMove(final String projectId);

	/**
	 * Create work request for move actions that are issued (status in
	 * 'Issued-In Process','Issued-On Hold').
	 *
	 * @param activityLogId
	 *            - id of action record from activity_log table @param projectId
	 *            - move project id, used for group moves @param moId - move id,
	 *            used for single move
	 *
	 * @throws ExceptionBase
	 */
	void genWorkRequest(final String activityLogId, final String projectId, final String moId) throws ExceptionBase;

	/**
	 * Only used for 'Review and Estimate Group Moves' and when the project
	 * Status was 'Request-Rejected' .
	 *
	 * @param tableName
	 *            <String> table name, the value should be mo or project
	 * @param pkeyValue
	 *            <String> primary key value
	 */
	void updateStatusForReviewAndEstimate(final String tableName, final String pkeyValue);

	/**
	 * Update associated space transactions for individual move/group move by
	 * given move order id or move project id.
	 *
	 * @param tableName
	 *            <String> table name, the value should be mo or project
	 * @param pkeyValue
	 *            <String> primary key value
	 */
	void onProcessDeleteRmpctRecord(final String tableName, final String pkeyValue);

	/**
	 * Delete associated space transactions by given move order ids.
	 *
	 * @param moIds
	 *            list of mo ids that deleted on client side
	 */
	void deleteRequestedSpaceTransactions(final String moIds);
}
