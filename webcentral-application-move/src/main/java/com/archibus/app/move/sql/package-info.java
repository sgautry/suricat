/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * This package contains SQL Operators for Move Management domain.
 *
 * Used by Move Management services.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
package com.archibus.app.move.sql;