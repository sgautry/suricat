package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IEquipmentStandardDao;
import com.archibus.app.move.common.domain.EquipmentStandard;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Equipment Standard.
 *
 * @author Zhang Yi
 *
 */
public class EquipmentStandardDataSource extends ObjectDataSourceImpl<EquipmentStandard>
		implements IEquipmentStandardDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "eqstd", "equipmentStandard" },
			{ "cost_moving", "costMoving" } };

	/**
	 * Constructs EquipmentStandardDataSource, mapped to <code>eq_std</code>
	 * table, using <code>EquipmentStandard</code> bean.
	 */
	public EquipmentStandardDataSource() {
		super("equipmentStandard", "eqstd");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

}
