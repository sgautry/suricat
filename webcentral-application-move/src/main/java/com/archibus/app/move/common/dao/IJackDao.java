package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.*;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Jack.
 *
 * Interface to be implemented by JackDataSource.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IJackDao extends IDao<Jack> {
	/**
	 * Gets Connected jacks of given employee.
	 *
	 * @param emId
	 *            employee id.
	 *
	 * @return Jacks object.
	 */
	Jacks getEmployeeJacks(final String emId);

}
