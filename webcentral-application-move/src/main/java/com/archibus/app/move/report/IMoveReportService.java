package com.archibus.app.move.report;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move Report related behaviors for Move Management
 * Domain.
 * <p>
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveReportService {

	/**
	 * Start a job to generate Move Management Paginate Report.
	 *
	 * @param rptType
	 * @param projectId
	 * @param selectedId
	 */
	void generatePaginateReport(String rptType, String projectId, String selectedId);

}
