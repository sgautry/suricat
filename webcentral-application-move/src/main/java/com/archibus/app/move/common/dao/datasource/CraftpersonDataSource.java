package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.ICraftpersonDao;
import com.archibus.app.move.common.domain.Craftperson;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Craft Person.
 *
 * @see ObjectDataSourceImpl.
 *
 * @author Zhang Yi
 *
 */
public class CraftpersonDataSource extends ObjectDataSourceImpl<Craftperson> implements ICraftpersonDao {

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "cf_id", "id" }, { "email", "email" } };

	/**
	 * Constructs CraftpersonDataSource, mapped to <code>cf</code> table, using
	 * <code>Craftperson</code> bean.
	 */
	public CraftpersonDataSource() {
		super("craftperson", "cf");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
