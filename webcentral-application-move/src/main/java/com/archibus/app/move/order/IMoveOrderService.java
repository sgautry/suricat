package com.archibus.app.move.order;

import java.util.List;

import org.json.JSONArray;

import com.archibus.datasource.data.DataRecord;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move Order(individual move) related business
 * behavior.
 * <p>
 *
 * Referenced by Workflow rule handler class of Move Management to delegate
 * their wfr methods related to individual move.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public interface IMoveOrderService {

	/**
	 * Add an individual move.
	 *
	 * NEW* INSERT INTO mo
	 * (status,em_id,requestor,dept_contact,date_created,mo_type,mo_class,
	 * description,
	 * date_start_req,to_bl_id,to_fl_id,to_rm_id,phone,phone_dept_contact,
	 * dv_id,dp_id,
	 * from_bl_id,from_fl_id,from_rm_id,from_dv_id,from_dp_id,to_dv_id,
	 * to_dp_id,from_phone,
	 * to_phone,from_fax,to_fax,from_mailstop,from_jk_id_data, from_jk_id_voice)
	 * SELECT %status%, em.em_id, requestor.em_id, dept_contact.em_id,
	 * %date_created% , %mo_type% , %mo_class% , %description% ,
	 * %date_start_req% , %to_bl_id% , %to_fl_id% , %to_rm_id% ,
	 * requestor.phone, dept_contact.phone, dept_contact.dv_id,
	 * dept_contact.dp_id, em_id.bl_id, em_id.fl_id, em_id.rm_id, em_id.dv_id,
	 * em_id.dp_id, em_id.dv_id, em_id.dp_id, em_id.phone, em_id.phone,
	 * em_id.fax, em_id.fax, em_id.mailstop, %from_jk_id_data%,
	 * %from_jk_id_voice% FROM em em_id, em requestor, em dept_contact WHERE
	 * em_id.em_id = %em_id% AND requestor.em_id = %requestor% AND
	 * dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 * @param addEq
	 * @param addTa
	 */
	void addIndividualMove(DataRecord record, boolean addEq, boolean addTa);

	/**
	 * Add an individual move for employee hire.
	 *
	 * *NEW* INSERT INTO mo
	 * (status,em_id,requestor,dept_contact,date_created,mo_type,mo_class,
	 * description,
	 * date_start_req,to_bl_id,to_fl_id,to_rm_id,phone,phone_dept_contact,dv_id,
	 * dp_id) SELECT %status% , %em_id% , requestor.em_id, dept_contact.em_id,
	 * %date_created% , %mo_type% , %mo_class% , %description% ,
	 * %date_start_req% , %to_bl_id% , %to_fl_id% , %to_rm_id% ,
	 * requestor.phone, dept_contact.phone, dept_contact.dv_id,
	 * dept_contact.dp_id FROM em requestor, em dept_contact WHERE
	 * requestor.em_id = %requestor% AND dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 * @param addEq
	 * @param addTa
	 */
	void addIndividualMoveNewHire(DataRecord record, boolean addEq, boolean addTa);

	/**
	 * Add an individual move for employee leaving.
	 *
	 * *NEW*
	 *
	 * INSERT INTO mo (status, em_id, requestor, dept_contact, date_created,
	 * mo_type, mo_class, description, date_start_req, phone,
	 * phone_dept_contact, dv_id, dp_id, from_bl_id, from_fl_id, from_rm_id,
	 * from_dv_id, from_dp_id, from_phone, from_fax, from_mailstop,
	 * from_jk_id_data, from_jk_id_voice) SELECT %status% , em.em_id,
	 * requestor.em_id, dept_contact.em_id, %date_created% , %mo_type%,
	 * %mo_class% , %description% , %date_start_req% , requestor.phone,
	 * dept_contact.phone, dept_contact.dv_id, dept_contact.dp_id, em_id.bl_id,
	 * em_id.fl_id, em_id.rm_id, em.dv_id, em.dp_id, em_id.phone, em_id.fax,
	 * em_id.mailstop, %from_jk_id_data%, %from_jk_id_voice% FROM em, em
	 * requestor, em dept_contact WHERE em.em_id = %em_id% AND requestor.em_id =
	 * %requestor% AND dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 * @param addEq
	 * @param addTa
	 */
	void addIndividualMoveEmployeeLeaving(DataRecord record, boolean addEq, boolean addTa);

	/**
	 * Add an individual move for asset.
	 *
	 * *NEW*
	 *
	 * INSERT INTO mo (status, em_id, requestor, dept_contact, date_created,
	 * mo_type, mo_class, description, date_start_req, to_bl_id, to_fl_id,
	 * to_rm_id, phone, phone_dept_contact, dv_id, dp_id) SELECT %status%,
	 * %em_id%, requestor.em_id, dept_contact.em_id, %date_created%, %mo_type% ,
	 * %mo_class% , %description% , %date_start_req% , %to_bl_id% , %to_fl_id% ,
	 * %to_rm_id%, requestor.phone, dept_contact.phone, dept_contact.dv_id,
	 * dept_contact.dp_id FROM em requestor, em dept_contact WHERE
	 * requestor.em_id = %requestor% AND dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 * @param addEq
	 * @param addTa
	 */
	void addIndividualMoveAsset(DataRecord record, boolean addEq, boolean addTa);

	/**
	 * Add an individual move for equipment.
	 *
	 * INSERT INTO mo
	 * (status,em_id,requestor,dept_contact,date_created,mo_type,mo_class,
	 * description, date_start_req
	 * ,to_bl_id,to_fl_id,to_rm_id,phone,phone_dept_contact,dv_id,dp_id,
	 * from_bl_id,
	 * from_fl_id,from_rm_id,from_dv_id,from_dp_id,to_dv_id,to_dp_id) SELECT
	 * %status% , em.em_id, requestor.em_id, dept_contact.em_id, %date_created%
	 * , %mo_type% , %mo_class% , %description% , %date_start_req% , %to_bl_id%
	 * , %to_fl_id% , %to_rm_id% , requestor.phone, dept_contact.phone,
	 * dept_contact.dv_id, dept_contact.dp_id, " em.bl_id, em.fl_id, em.rm_id,
	 * em.dv_id, em.dp_id, em.dv_id," + " em.dp_id, em.phone, em.phone, em.fax,
	 * em.fax, em.mailstop," + FROM em, em requestor, em dept_contact, em em_id
	 * WHERE em.em_id = %em_id% AND requestor.em_id = %requestor% AND
	 * dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 * @param addEq
	 * @param addTa
	 */
	void addIndividualMoveEquipment(DataRecord record, boolean addEq, boolean addTa);

	/**
	 * Add an individual move for room.
	 *
	 * INSERT INTO mo
	 * (status,em_id,requestor,dept_contact,date_created,mo_type,mo_class,
	 * description, date_start_req
	 * ,to_bl_id,to_fl_id,to_rm_id,phone,phone_dept_contact,dv_id,dp_id,
	 * from_bl_id, from_fl_id,from_rm_id) SELECT %status% , %em_id%,
	 * requestor.em_id, dept_contact.em_id, %date_created% , %mo_type% ,
	 * %mo_class% , %description% , %date_start_req% , %to_bl_id% , %to_fl_id% ,
	 * %to_rm_id% , requestor.phone, dept_contact.phone, dept_contact.dv_id,
	 * dept_contact.dp_id, %from_bl_id% , %from_fl_id% , %from_rm_id% FROM em
	 * requestor, em dept_contact WHERE requestor.em_id = %requestor% AND
	 * dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 * @param addEq
	 * @param addTa
	 */
	void addIndividualMoveRoom(DataRecord record, boolean addEq, boolean addTa);

	/**
	 * Assign employees to team.
	 *
	 * @param emIds
	 *            selected employees id
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	void assignEmployeesToTeam(final List<String> emIds, final String teamId, final String projectId, final String type,
			final String dateStartReq, final String dateToPerform);

	/**
	 * Assign employees to team.
	 *
	 * @param rooms
	 *            selected room info
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	void assignRoomsToTeam(final JSONArray rooms, final String teamId, final String projectId, final String type,
			final String dateStartReq, final String dateToPerform);

	/**
	 * Remove employees from team.
	 *
	 * @param emIds
	 *            selected employees id
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	void removeEmployeesFromTeam(final List<String> emIds, final String teamId, final String projectId,
			final String type, String dateStartReq, String dateToPerform);

	/**
	 * Remove rooms from team.
	 *
	 * @param rooms
	 *            selected rooms id
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	void removeRoomsFromTeam(final JSONArray rooms, final String teamId, final String projectId, final String type,
			final String dateStartReq, final String dateToPerform);

}
