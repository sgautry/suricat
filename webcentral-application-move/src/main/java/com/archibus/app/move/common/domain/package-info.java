/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * This package contains domain objects for Move Management.
 *
 *
 * Used by Move Management services.
 *
 * @author zhangyi
 * @since 23.1
 **/
package com.archibus.app.move.common.domain;
