package com.archibus.app.move.scenario;

import org.json.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move Scenario Assignment related business behaviors.
 * <p>
 *
 * Referenced by Workflow rule handler class of Move Management to delegate
 * their wfr methods related to move scenario assignments logics.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public interface IMoveScenarioAssignmentService {

	/**
	 * Insert new move scenario employee record.
	 *
	 * @param assignments
	 *            assignment array
	 * @param assignType
	 *            assignment type
	 */
	void commitAssignments(final JSONArray assignments, final String assignType);

	/**
	 * Delete placed assignment.
	 *
	 * @param moveScenarioEmId
	 *            Mover Scenario Employee Id
	 * @param assignType
	 *            assignment type
	 */
	void removeAssignment(final int moveScenarioEmId, final String assignType);

	/**
	 * Check whether the assigned layout locations are still valid and then to
	 * update corresponding Move Scenario Employee records for invalid to
	 * locations.
	 *
	 * @param projectId
	 *            project id
	 * @param scenarioId
	 *            scenario id
	 */
	void validLayoutLocation(final String projectId, final String scenarioId);

	/**
	 * Insert new move scenario employee record.
	 *
	 * @param record
	 *            move scenario employee record object passed from client side
	 */
	void deleteLayout(final JSONObject record);

	/**
	 * Insert new Layout Place holder into mo_scenario_em.
	 *
	 * @param record
	 *            a layout record object
	 */
	void addLayoutPlaceholder(final JSONObject record);
}
