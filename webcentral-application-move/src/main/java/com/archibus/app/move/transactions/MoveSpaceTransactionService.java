package com.archibus.app.move.transactions;

import java.util.*;

import org.json.*;

import com.archibus.app.common.bldgops.dao.*;
import com.archibus.app.common.bldgops.dao.datasource.WorkRequestDataSource;
import com.archibus.app.common.bldgops.domain.*;
import com.archibus.app.common.organization.domain.Employee;
import com.archibus.app.common.space.dao.IRoomTransactionDao;
import com.archibus.app.common.space.dao.datasource.BuildingDataSource;
import com.archibus.app.common.space.domain.*;
import com.archibus.app.common.util.SchemaUtils;
import com.archibus.app.move.common.dao.*;
import com.archibus.app.move.common.domain.*;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.context.ContextStore;
import com.archibus.core.dao.IDao;
import com.archibus.datasource.SqlUtils;
import com.archibus.datasource.data.*;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.*;
import com.archibus.model.view.datasource.ClauseDef.*;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveSpaceTransactionService. Provides Workspace Transaction related
 * business operations.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr methods related to
 * Workspace Transaction.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveSpaceTransactionService implements IMoveSpaceTransactionService {

    /**
     * Dao for Action Item.
     */
    private IActionItemDao actionItemDao;

    /**
     * Dao for Move Project.
     */
    private IProjectDao moveProjectDao;

    /**
     * Dao for Move Order.
     */
    private IMoveOrderDao moveOrderDao;

    /**
     * Dao for Move Project.
     */
    private IWorkRequestDao workRequestDao;

    /**
     * Dao for Employee.
     */
    private IDao<Employee> employeeDao;

    /**
     * Dao for RoomTransaction.
     */
    private IRoomTransactionDao roomTransactionDao;

    /**
     * Converter from move orders to assignments.
     */
    private IMoveOrdersToAssignmentsConverter moveOrdersToAssignmentsConverter;

    // @translatable
    protected final static String errorMsgMissingRequestor =
            "Requestor for Action Item [{0}] is invalid or is missing";

    @Override
    public void updateStatusForReviewAndEstimate(final String tableName, final String pkeyValue) {

        if ("mo".equals(tableName)) {

            this.updateAssociatedServiceRequestStatusForIndividualMove(pkeyValue);

        } else {

            this.updateAssociatedServiceRequestStatusForGrouplMove(pkeyValue);
        }
    }

    @Override
    public void onProcessDeleteRmpctRecord(final String tableName, final String pkeyValue) {

        final String useWorkspaceTransactions =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue("AbSpaceRoomInventoryBAR-UseWorkspaceTransactions");

        if (useWorkspaceTransactions.equals("1")) {

            final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
            restrictionDef.addClause("mo", "activity_log_id", null, Operation.IS_NOT_NULL);

            if ("mo".equals(tableName)) {

                restrictionDef.addClause("mo", "mo_id", pkeyValue, Operation.EQUALS);

            } else {
                restrictionDef.addClause("mo", "project_id", pkeyValue, Operation.EQUALS);
            }

            this.processDeleteRmpctRecord(this.moveOrderDao.find(restrictionDef));
        }
    }

    @Override
    public void deleteRequestedSpaceTransactions(final String moIds) {

        if (StringUtil.notNullOrEmpty(moIds)) {
            SqlUtils.executeUpdate("rmpct",
                "Delete from rmpct Where rmpct.status=0 and rmpct.mo_id in (" + moIds + ")");
        }

    }

    @Override
    public void genWorkRequest(final String activityLogId, final String projectId,
            final String moId) throws ExceptionBase {

        final ActionItem item = this.actionItemDao.get(Integer.valueOf(activityLogId));

        if (StringUtil.notNullOrEmpty(projectId)) {

            final Project project = this.moveProjectDao.get(projectId);
            if ("Issued-In Process".equalsIgnoreCase(project.getStatus())
                    || "Issued-On Hold".equalsIgnoreCase(project.getStatus())) {

                this.createWorkRequest(item);
            }

        } else {

            final MoveOrder moveOrder = this.moveOrderDao.get(Integer.valueOf(moId));
            if ("Issued-In Process".equalsIgnoreCase(moveOrder.getStatus())
                    || "Issued-On Hold".equalsIgnoreCase(moveOrder.getStatus())) {

                this.createWorkRequest(item);
            }
        }
    }

    @Override
    public void recordSpaceTransactions(final Project project, final boolean isApproveTransaction) {

        if (this.isTransactionEnabled()) {
            // update associatecd space transaction service request status
            this.updateAssociatedServiceRequestStatusForGrouplMove(project.getId());

            // update associatecd rmpct records
            final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
            restriction.addClause("mo", "project_id", project.getId(), Operation.EQUALS);

            final List<String> moTypes = new ArrayList<String>();
            moTypes.add("Employee");
            moTypes.add("Leaving");
            moTypes.add("New Hire");
            restriction.addClause("mo", "mo_type", moTypes, Operation.IN);

            this.updateAssociatedRmpct(this.moveOrderDao.find(restriction), isApproveTransaction);
        }

    }

    @Override
    public void recordSpaceTransactions(final MoveOrder mo, final boolean isApproveTransaction) {

        if (this.isTransactionEnabled()) {
            // update associatecd space transaction service request status
            this.updateAssociatedServiceRequestStatusForIndividualMove(String.valueOf(mo.getId()));

            // update associatecd rmpct records
            final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
            restriction.addClause("mo", "mo_id", mo.getId(), Operation.EQUALS);

            final List<String> moTypes = new ArrayList<String>();
            moTypes.add("Employee");
            moTypes.add("Leaving");
            moTypes.add("New Hire");
            restriction.addClause("mo", "mo_type", moTypes, Operation.IN);

            this.updateAssociatedRmpct(this.moveOrderDao.find(restriction), isApproveTransaction);
        }
    }

    @Override
    public void updateForRejectIndividualMove(final MoveOrder mo) {

        if (this.isTransactionEnabled()) {
            // delete Rmpct Record
            this.processDeleteRmpctRecord(
                this.moveOrderDao.getMoveOrdersByRestriction("mo_id=" + mo.getId()));

            // // update associatecd space transaction service request status
            this.updateAssociatedServiceRequestStatusForIndividualMove(String.valueOf(mo.getId()));
        }
    }

    @Override
    public void updateForRejectGroupMove(final Project project) {

        if (this.isTransactionEnabled()) {

            // delete Rmpct Record
            final List<MoveOrder> mos = this.moveOrderDao.getMoveOrdersByRestriction(
                "project_id=" + MoveCommonUtil.getLiteralString(project.getId()));
            this.processDeleteRmpctRecord(mos);

            // update associatecd space transaction service request status
            this.updateAssociatedServiceRequestStatusForGrouplMove(project.getId());
        }
    }

    @Override
    public void createServiceRequestForIndvidualMove(final String moId) {
        // generate service request for current issued move
        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();

        restrictionDef.addClause("activity_log", "prob_type", null, Operation.IS_NOT_NULL);
        restrictionDef.addClause("activity_log", "autocreate_wr", 1, Operation.EQUALS);
        restrictionDef.addClause("activity_log", "mo_id", moId, Operation.EQUALS);

        final List<ActionItem> items = this.actionItemDao.find(restrictionDef);
        for (final ActionItem item : items) {
            // check the status for move when the WFR is called from
            // edit action view
            final MoveOrder moveOrder = this.moveOrderDao.get(Integer.valueOf(moId));
            if ("Issued-In Process".equalsIgnoreCase(moveOrder.getStatus())
                    || "Issued-On Hold".equalsIgnoreCase(moveOrder.getStatus())) {

                this.createWorkRequest(item);
            }
        }
    }

    @Override
    public void createServiceRequestForGroupMove(final String projectId) {
        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();

        restrictionDef.addClause("activity_log", "prob_type", null, Operation.IS_NOT_NULL);
        restrictionDef.addClause("activity_log", "autocreate_wr", 1, Operation.EQUALS);
        restrictionDef.addClause("activity_log", "project_id", projectId, Operation.EQUALS);

        final List<ActionItem> items = this.actionItemDao.find(restrictionDef);
        for (final ActionItem item : items) {

            // check the status for project when the WFR is called from edit
            // action view
            final Project project = this.moveProjectDao.get(projectId);
            if ("Issued-In Process".equalsIgnoreCase(project.getStatus())
                    || "Issued-On Hold".equalsIgnoreCase(project.getStatus())) {

                this.createWorkRequest(item);
            }
        }
    }

    @Override
    public void updateAssociatedServiceRequestStatusForIndividualMove(final String moId) {

        final MoveOrder moveOrder = this.moveOrderDao.get(Integer.valueOf(moId));
        // get associated activity_log_id and store to activityLogId
        final int activityLogId =
                moveOrder.getActivityLogId() == null ? 0 : moveOrder.getActivityLogId().intValue();
        // if exists activity_log_id, then go through logic to update the
        // status,else do nothing
        if (activityLogId != 0 && this.isValidActivityLogId(activityLogId)) {

            // get the status of project or mo and store to currentStatus
            final String currentStatus = moveOrder.getStatus();

            final String activityLogStatus =
                    this.updateActivityLogStatus(currentStatus, activityLogId);

            this.callMoveProcessAndBackUpHistory(activityLogId, moId, activityLogStatus);
        }
    }

    @Override
    public void updateAssociatedServiceRequestStatusForGrouplMove(final String projectId) {

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
        restrictionDef.addClause("mo", "project_id", projectId, Operation.EQUALS);

        final List<MoveOrder> moveOrders = this.moveOrderDao.find(restrictionDef);

        if (moveOrders.size() > 0 && moveOrders.get(0).getActivityLogId() != null) {
            // get associated activity_log_id and store to activityLogId
            final int activityLogId = moveOrders.get(0).getActivityLogId() == null ? 0
                    : moveOrders.get(0).getActivityLogId().intValue();
            // if exists activity_log_id, then go through logic to update the
            // status,else do nothing
            if (activityLogId != 0 && isValidActivityLogId(activityLogId)) {

                // get the status of project or mo and store to currentStatus
                final String currentStatus = moveOrders.get(0).getStatus();

                // the flag of whether all mo record with same project are same
                final boolean isStatusSame = isSameStatus(currentStatus, moveOrders);

                // if is StatusSame is true, call SeviceDesk WFR to update the
                // status,else do
                // nothing
                if (isStatusSame) {
                    final String activityLogStatus =
                            updateActivityLogStatus(currentStatus, activityLogId);

                    this.callMoveProcessAndBackUpHistory(activityLogId,
                        String.valueOf(moveOrders.get(0).getId()), activityLogStatus);
                }
            }

        } else {
            final Project project = this.moveProjectDao.get(projectId);

            // when there is not rmpct try to get the associated activity_log
            // item by searching the activity_log table and only update activity
            // log status.
            final int activityLogId = getActivityLogOfProject(project);

            // get the status of project or mo and store to currentStatus
            final String currentStatus = project.getStatus();

            // if exists activity_log_id, then go through logic to update the
            // status,else do nothing
            if (activityLogId != 0 && isValidActivityLogId(activityLogId)) {

                final String activityLogStatus =
                        updateActivityLogStatus(currentStatus, activityLogId);

                this.callMoveProcessAndBackUpHistory(activityLogId, null, activityLogStatus);
            }

        }

    }

    /**
     * get activity_log status from current status. and call WFR
     * "AbBldgOpsHelpDesk-RequestsService-updateStatus" update status.
     *
     * @param currentStatus
     * @param activityLogId
     * @return
     */
    private String updateActivityLogStatus(final String currentStatus, final int activityLogId) {

        final Map<String, String> swapStatus = new HashMap<String, String>();
        swapStatus.put("Requested-Estimated", "REQUESTED");
        swapStatus.put("Requested-On Hold", "REQUESTED");
        swapStatus.put("Requested-Routed", "REQUESTED");
        swapStatus.put("Requested-Rejected", "REJECTED");
        swapStatus.put("Approved", "APPROVED");
        swapStatus.put("Approved-In Design", "APPROVED");
        swapStatus.put("Approved-Cancelled", "CANCELLED");
        swapStatus.put("Issued-In Process", "IN PROGRESS");
        swapStatus.put("Issued-On Hold", "IN PROCESS-H");
        swapStatus.put("Issued-Stopped", "STOPPED");
        swapStatus.put("Completed-Pending", "COMPLETED");
        swapStatus.put("Completed-Not Ver", "COMPLETED");
        swapStatus.put("Completed-Verified", "COMPLETED-V");
        swapStatus.put("Closed", "CLOSED");

        // status of activity_log, the value should be IN PROGRESS or COMPLETED
        // or CLOSED, it is determined by the value of currentStatus
        final String activityLogStatus = swapStatus.get(currentStatus);
        if (activityLogStatus != null) {
            // prepare the WFR parameter
            final JSONArray methodParameters = new JSONArray();
            methodParameters.put(activityLogId);
            methodParameters.put(activityLogStatus);

            // @translatable
            final String errorMessage =
                    "Can't update service request's status due to error of Bldgops workflow rule 'updateStatus' in class 'RequestsService'.";
            final Object[] args = { activityLogId };

            // call WFR to update the status
            this.runWorkflowRuleMethod("AbBldgOpsHelpDesk-RequestsService-updateStatus",
                methodParameters, errorMessage, args);
        }

        return activityLogStatus;
    }

    /**
     * update associated rmpct from mo record or project record.
     *
     * @param tableName <String> table name, the value should be mo or project
     * @param pkeyValue <String> primary key value
     */
    private void updateAssociatedRmpct(final List<MoveOrder> mos,
            final boolean isApproveTransaction) {

        final JSONArray insertList = new JSONArray();
        final JSONArray unchangeList = new JSONArray();
        final JSONArray deleteList = new JSONArray();

        // kb#3034176: pass current date to all rmpct records for given
        // activity_log_id
        Date moveDate = Utility.currentDate();

        for (final MoveOrder mo : mos) {

            // if the move is not approved (Issued) make sure to use the Move
            // Date
            if (!isApproveTransaction) {
                moveDate = mo.getDatePerform();
            }

            // get associated activity_log_id and store to activityLogId
            final Integer activityLogId = mo.getActivityLogId();

            // get all rmpct records where rmpct.activity_log_id=<activityLogId>
            final List<RoomTransaction> rmpcts =
                    this.roomTransactionDao.find(getRestrictionForRmpct(mo));

            if (rmpcts.size() > 0) {

                final RoomTransaction rmpct = rmpcts.get(0);

                if (this.anyToLocationChange(mo, rmpct)) {

                    final JSONObject assignment = getInsertAssignmentObject(
                        activityLogId == null ? 0 : activityLogId.intValue(), mo, rmpct, moveDate,
                        isApproveTransaction);

                    insertList.put(assignment);

                    final JSONObject assignmentD = new JSONObject();
                    assignmentD.put("pct_id", rmpct.getId());

                    // kb#3034359: add mo_id to assingment before call Space
                    // Workflow rule - ZY
                    assignmentD.put("mo_id", mo.getId());
                    assignmentD.put("activity_log_id", activityLogId);

                    deleteList.put(assignmentD);

                } else {

                    final JSONObject assignment = getUnChangedAssignment(
                        activityLogId == null ? 0 : activityLogId.intValue(), mo, rmpct,
                        isApproveTransaction);
                    unchangeList.put(assignment);
                }

                // kb#3035110:for issue#1, now update the date_start of rmpct
                // for approving to current date in Move Application side before
                // calling Space workflow rules
                rmpct.setDateStart(moveDate);
                this.roomTransactionDao.update(rmpct);

            } else {

                final JSONObject assignment = getInsertAssignmentObject(
                    activityLogId == null ? 0 : activityLogId.intValue(), mo, null, moveDate,
                    isApproveTransaction);

                insertList.put(assignment);
            }
        }

        final JSONObject assignmentsObject = new JSONObject();
        assignmentsObject.put("I", insertList);
        assignmentsObject.put("U", unchangeList);
        assignmentsObject.put("D", deleteList);

        final JSONArray methodParameters = new JSONArray();
        methodParameters.put(moveDate);
        methodParameters.put(assignmentsObject);

        if (isApproveTransaction) {

            // @translatable
            final String errorMessage =
                    "Can't close move order due to error of Space workflow rule 'closeMoveOrder'.";
            // call WFR to update the status
            this.runWorkflowRuleMethod(
                "AbSpaceRoomInventoryBAR-SpaceTransactionProcess-closeMoveOrder", methodParameters,
                errorMessage, null);

        } else {

            // @translatable
            final String errorMessage =
                    "Can't issue move order due to error of Space workflow rule 'issueMoveOrder'.";

            // call WFR to update the status
            this.runWorkflowRuleMethod(
                "AbSpaceRoomInventoryBAR-SpaceTransactionProcess-issueMoveOrder", methodParameters,
                errorMessage, null);
        }
    }

    /**
     * Create work request for given service request(activity_log record).
     *
     * @param actionItem
     */
    private void createWorkRequest(final ActionItem actionItem) {

        if (StringUtil.notNullOrEmpty(actionItem.getRequestor())) {
            final Employee em = this.employeeDao.get(actionItem.getRequestor());
            final Building bl = new BuildingDataSource().get(actionItem.getBuildingId());

            final WorkRequest wr = new WorkRequest();

            wr.setAssessmentId(Integer.valueOf(actionItem.getId()));
            wr.setActivityType("SERVICE DESK - MAINTENANCE");
            wr.setBuildingId(actionItem.getBuildingId());
            wr.setFloorId(actionItem.getFloorId());
            wr.setRoomId(actionItem.getRoomId());
            wr.setSiteId(bl != null ? bl.getSiteId() : "");

            wr.setProblemType(actionItem.getProblemType());
            wr.setDateRequired(actionItem.getDateRequired());
            wr.setDateScheduled(actionItem.getDateScheduled());
            wr.setCostEstimated(actionItem.getCostEstimated());
            wr.setHoursEstBaseline(actionItem.getHoursEstBaseline());
            wr.setDescription(actionItem.getDescription());

            wr.setDivisionId(actionItem.getDivisionId());
            wr.setDepartmentId(actionItem.getDepartmentId());
            wr.setPhoneRequestor(em.getPhone());
            wr.setAccountId(actionItem.getAccountId());
            wr.setPriority(1);
            wr.setCreatedBy(em.getId());
            wr.setRequestor(em.getId());

            final WorkRequest wrPk = this.workRequestDao.save(wr);

            // call WFR to update the status
            final String id = wrPk.getActionItemId();
            final DataRecord record =
                    new WorkRequestDataSource().getRecord("activity_log_id=" + id);
            final JSONObject jsonRecord = EventHandlerBase.toJSONObject(handleRecordValue(record));
            final JSONArray methodParameters = new JSONArray();
            methodParameters.put(id);
            methodParameters.put(jsonRecord);

            // @translatable
            final String errorMessage =
                    "Generated Service Request [{0}] cannot be submitted due to system error";
            final Object[] args = { wr.getId() };

            this.runWorkflowRuleMethod("AbBldgOpsHelpDesk-RequestsService-submitRequest",
                methodParameters, errorMessage, args);

        } else {

            final Object[] args = { actionItem.getId() };
            MoveCommonUtil.throwException(errorMsgMissingRequestor, args);
        }
    }

    /**
     * update associated rmpct from mo record or project record.
     *
     * @param tableName <String> table name, the value should be mo or project
     * @param pkeyValue <String> primary key value
     */
    private void processDeleteRmpctRecord(final List<MoveOrder> mos) {

        final JSONArray deleteList = new JSONArray();
        for (final MoveOrder mo : mos) {
            // 10/9/13 - KB 3039803 From EAR: Add move type restriction to move
            // datasoure
            if ("Employee".equalsIgnoreCase(mo.getMoveType())
                    || "Leaving".equalsIgnoreCase(mo.getMoveType())
                    || "New Hire".equalsIgnoreCase(mo.getMoveType())) {

                final JSONObject assignment = new JSONObject();
                // kb#3034359: add mo_id to assingment before call Space
                // Workflow rule - ZY
                assignment.put("mo_id", mo.getId());

                assignment.put("activity_log_id", 0);
                assignment.put("parent_pct_id", -1);
                deleteList.put(assignment);
            }
        }

        final JSONArray methodParameters = new JSONArray();
        methodParameters.put(0);
        methodParameters.put(deleteList);

        // @translatable
        final String errorMessage =
                "Can't cancel move order due to error of Space workflow rule 'cancelMoveOrder'.";

        // call WFR to update the status
        this.runWorkflowRuleMethod(
            "AbSpaceRoomInventoryBAR-SpaceTransactionProcess-cancelMoveOrder", methodParameters,
            errorMessage, null);
    }

    /**
     * call 'processDeleteRmpctRecord' if activityLogStatus not empty and exists mo, then back up
     * history.
     *
     * @param activityLogId
     * @param mo_id
     * @param activityLogStatus
     */
    private void callMoveProcessAndBackUpHistory(final int activityLogId, final String moId,
            final String activityLogStatus) {
        if (activityLogStatus != null) {
            // KB#3037377: Try to delete requested rmpct records when cancel the
            // move order or set its status to 'Requested-Rejected' in Review
            // And Estimate Moves view.
            if (activityLogStatus.equals("CANCELLED") || activityLogStatus.equals("REJECTED")) {

                final String useWorkspaceTransactions =
                        ContextStore.get().getProject().getActivityParameterManager()
                            .getParameterValue("AbSpaceRoomInventoryBAR-UseWorkspaceTransactions");
                if (useWorkspaceTransactions.equals("1") && StringUtil.notNullOrEmpty(moId)) {

                    // delete Rmpct Record
                    this.processDeleteRmpctRecord(
                        this.moveOrderDao.getMoveOrdersByRestriction("mo.mo_id=" + moId));
                }

                this.moveToHistory(activityLogId);
            }
        }
    }

    /**
     * Move cancelled or rejected activity_log record to hactivity_log
     *
     * @param activityLogId
     */
    private void moveToHistory(final int activityLogId) {
        // Field name arrays used for insert record into table 'hactivity_log'
        final List<String> fields = SchemaUtils.getFieldsForTable("hactivity_log");
        // Move cancelled or rejected activity_log record to hactivity_log
        String fieldStr = fields.get(0);
        for (int i = 1; i < fields.size(); i++) {
            fieldStr += "," + fields.get(i);
        }

        final String insert = "INSERT into hactivity_log (" + fieldStr + ") " + "SELECT " + fieldStr
                + " FROM activity_log WHERE activity_log_id=" + activityLogId;
        SqlUtils.executeUpdate("hactivity_log", insert);

        final ActionItem actionItem = this.actionItemDao.get(Integer.valueOf(activityLogId));
        this.actionItemDao.delete(actionItem);
    }

    /**
     *
     * Try to get the associated activity_log item by searching the activity_log table.
     *
     * @param project
     * @return
     */
    private int getActivityLogOfProject(final Project project) {
        // kb 3038809 try to get the associated activity_log item by searching
        // the activity_log table WHERE activitytype = 'SERVICE DESK - GROUP
        // MOVE' AND date_requested <= project.date_created
        // AND act_quest IS NOT NULL AND act_quest LIKE
        // %'quest_name="project_name" value="<project.project_id>"'%

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
        restrictionDef.addClause("activity_log", "activity_type", "SERVICE DESK - GROUP MOVE",
            Operation.EQUALS);
        restrictionDef.addClause("activity_log", "date_requested", project.getDateCreated(),
            Operation.LTE);
        restrictionDef.addClause("activity_log", "act_quest", null, Operation.IS_NOT_NULL);
        restrictionDef.addClause("activity_log", "act_quest",
            "%quest_name=%project_name%value=%" + project.getId() + "%", Operation.LIKE);

        final List<ActionItem> items = this.actionItemDao.find(restrictionDef);

        if (items.size() > 0) {
            // get associated activity_log_id and store to activityLogId
            return Integer.valueOf(items.get(0).getId());
        } else {
            return 0;
        }
    }

    /**
     * this method will encapsulate an JSONObject.
     *
     * @param activityLogId
     * @param moRecord
     * @param rmpctRecord
     * @return
     */
    private JSONObject getInsertAssignmentObject(final int activityLogId, final MoveOrder mo,
            final RoomTransaction rmpct, final Date moveDate, final boolean isApproved) {

        final JSONObject assignment = new JSONObject();

        // kb#3034359: add mo_id to assingment before call Space Workflow rule
        assignment.put("mo_id", mo.getId());

        // kb#3034359: for newly insert assignment from Move Management,
        // activity log id is not existed.
        assignment.put("activity_log_id", activityLogId);

        // kb#3035094:(FK 2012-02-13 10:06)Unless the primary_em value is
        // specified with the Space request (in other words, the move request
        // initiated from Space has a primary_em value),
        // then we must assume primary_em is 1.
        assignment.put("primary_em", rmpct == null ? 1 : rmpct.getPrimaryEmployee());

        assignment.put("em_id", mo.getEmployeeId());
        assignment.put("from_bl_id", mo.getFromBuilding());
        assignment.put("from_fl_id", mo.getFromFloor());
        assignment.put("from_rm_id", mo.getFromRoom());
        assignment.put("bl_id", mo.getToBuilding());
        assignment.put("fl_id", mo.getToFloor());
        assignment.put("rm_id", mo.getToRoom());

        assignment.put("dv_id", "");
        assignment.put("dp_id", "");
        assignment.put("rm_cat", "");
        assignment.put("rm_type", "");

        assignment.put("primary_rm", rmpct == null ? 1 : rmpct.getPrimaryRoom());
        assignment.put("parent_pct_id", "");
        assignment.put("date_start", moveDate.getTime());

        // kb 3037650 the primary_em flag of the new "to location"
        // should be the same as the primary_em of the "from location" record
        // that had the employee
        // previously.
        if (this.isLocationPrimaryOfEmployee(mo.getEmployeeId(), mo.getFromBuilding(),
            mo.getFromFloor(), mo.getFromRoom(), moveDate)) {
            assignment.put("primary_em", 1);
        }

        assignment.put("status", isApproved ? 1 : 0);

        assignment.put("action", "update");

        return assignment;
    }

    /**
     * @return a new assignment object which to location is not changed.
     *
     * @param emId String employee code
     * @param blId String building code
     * @param flId String floor code
     * @param rmId String room code
     * @param requestDate Date request date
     *
     */
    private JSONObject getUnChangedAssignment(final int activityLogId, final MoveOrder mo,
            final RoomTransaction rmpct, final boolean isApproved) {

        final JSONObject assignment = new JSONObject();

        assignment.put("activity_log_id", activityLogId);
        assignment.put("mo_id", mo.getId());
        assignment.put("status", isApproved ? 1 : 0);

        assignment.put("action", "update");
        assignment.put("em_id", rmpct.getEmployeeId());
        assignment.put("from_bl_id", mo.getFromBuilding());
        assignment.put("from_fl_id", mo.getFromFloor());
        assignment.put("from_rm_id", mo.getFromRoom());
        assignment.put("bl_id", rmpct.getBuildingId());
        assignment.put("fl_id", rmpct.getFloorId());
        assignment.put("rm_id", rmpct.getRoomId());
        assignment.put("primary_em", rmpct.getPrimaryEmployee());
        assignment.put("dv_id", rmpct.getDivisionId());
        assignment.put("dp_id", rmpct.getDepartmentId());
        assignment.put("rm_cat", rmpct.getCategory());
        assignment.put("rm_type", rmpct.getType());
        assignment.put("primary_rm", rmpct.getPrimaryRoom());
        assignment.put("parent_pct_id", rmpct.getParentId() == 0 ? "" : rmpct.getParentId());
        assignment.put("pct_id", rmpct.getId());

        return assignment;
    }

    /**
     * Return restriction for rmpct records: if move order initiated from Space.
     *
     * @param mo
     */
    private ParsedRestrictionDef getRestrictionForRmpct(final MoveOrder mo) {

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();

        final Integer activityLogId = mo.getActivityLogId();

        if (activityLogId != null && activityLogId != 0 && isValidActivityLogId(activityLogId)) {

            restrictionDef.addClause("rmpct", "em_id", mo.getEmployeeId(), Operation.EQUALS);
            restrictionDef.addClause("rmpct", "activity_log_id", activityLogId, Operation.EQUALS);

        } else {

            restrictionDef.addClause("rmpct", "mo_id", mo.getId(), Operation.EQUALS);
        }

        return restrictionDef;
    }

    private boolean isTransactionEnabled() {

        final String useWorkspaceTransactions =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue("AbSpaceRoomInventoryBAR-UseWorkspaceTransactions");

        // Activity parameter CaptureSpaceHistory
        final String captureSpaceHistory =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue("AbSpaceRoomInventoryBAR-CaptureSpaceHistory");

        return useWorkspaceTransactions.equals("1") || captureSpaceHistory.equals("1");
    }

    /**
     * @return if the given activity log id exists or not .
     *
     * @param ativityLogId <int> activity_log id
     */
    private boolean isValidActivityLogId(final int ativityLogId) {

        final ActionItem item = this.actionItemDao.get(ativityLogId);

        return item == null ? false : true;

    }

    /**
     * @return if all move orders in give move order list contain same status as given.
     *
     * @param currentStatus
     * @param ds
     * @param moRecords
     * @return
     */
    private boolean isSameStatus(final String currentStatus, final List<MoveOrder> mos) {

        for (final MoveOrder mo : mos) {

            if (!currentStatus.equalsIgnoreCase(mo.getStatus())) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return if the location is the primary location of employee.
     *
     * @param emId String employee code
     * @param blId String building code
     * @param flId String floor code
     * @param rmId String room code
     * @param requestDate Date request date
     *
     */
    private boolean isLocationPrimaryOfEmployee(final String emId, final String blId,
            final String flId, final String rmId, final Date requestDate) {

        // Construct a ParsedRestrictionDef object that contains below
        // conditions:
        // bl_id = <from_bl_id> AND fl_id = <from_fl_id> AND rm_id =
        // <from_rm_id> AND em_id=<em_id> AND status = 1 AND
        // (date_start IS NULL OR date_start <= <date>) AND (date_end IS NULL OR
        // date_end >= <date>) and primary_em=1
        final ParsedRestrictionDef rmpctResDef = new ParsedRestrictionDef();
        rmpctResDef.addClause("rmpct", "bl_id", blId, Operation.EQUALS);
        rmpctResDef.addClause("rmpct", "fl_id", flId, Operation.EQUALS);
        rmpctResDef.addClause("rmpct", "rm_id", rmId, Operation.EQUALS);
        rmpctResDef.addClause("rmpct", "em_id", emId, Operation.EQUALS);

        rmpctResDef.addClause("rmpct", "status", 1, Operation.EQUALS);

        rmpctResDef.addClause("rmpct", "date_start", requestDate, Operation.LTE,
            RelativeOperation.AND_BRACKET);
        rmpctResDef.addClause("rmpct", "date_start", null, Operation.IS_NULL, RelativeOperation.OR);

        rmpctResDef.addClause("rmpct", "date_end", requestDate, Operation.GTE,
            RelativeOperation.AND_BRACKET);
        rmpctResDef.addClause("rmpct", "date_end", null, Operation.IS_NULL, RelativeOperation.OR);

        rmpctResDef.addClause("rmpct", "primary_em", 1, Operation.EQUALS,
            RelativeOperation.AND_BRACKET);

        return this.roomTransactionDao.find(rmpctResDef).size() > 0;
    }

    /**
     * @return if to locaction had change.
     *
     * @param moRecord mo record
     * @param rmpctRecord rmpct record
     */
    private boolean anyToLocationChange(final MoveOrder mo, final RoomTransaction rmpct) {

        final boolean isBuildingChanged =
                mo.getToBuilding() == null && StringUtil.notNullOrEmpty(rmpct.getBuildingId())
                        || (mo.getToBuilding() != null
                                && !mo.getToBuilding().equalsIgnoreCase(rmpct.getBuildingId()));

        final boolean isFloorChanged =
                mo.getToFloor() == null && StringUtil.notNullOrEmpty(rmpct.getFloorId())
                        || (mo.getToFloor() != null
                                && !mo.getToFloor().equalsIgnoreCase(rmpct.getFloorId()));

        final boolean isRoomChanged = mo.getToRoom() == null
                && StringUtil.notNullOrEmpty(rmpct.getRoomId())
                || (mo.getToRoom() != null && !mo.getToRoom().equalsIgnoreCase(rmpct.getRoomId()));

        return isBuildingChanged || isFloorChanged || isRoomChanged;
    }

    /**
     * Call workflow rule method of Space Transaction.
     *
     * @param context <EventHandlerContext> user context
     * @param ruleName <String> workflow rule name that with method name like
     *            "AbBldgOpsHelpDesk-RequestsService-updateStatus"
     */
    private void runWorkflowRuleMethod(final String ruleName, final JSONArray methodParameters,
            final String errorMsg, final Object[] args) {
        // get rule id and method name

        final List<String> ruleParts = StringUtil.tokenizeString(ruleName, "-");
        final String ruleId = ruleParts.get(0) + "-" + ruleParts.get(1);
        final String methodName = ruleParts.get(2);

        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
        context.addResponseParameter("methodParameters", methodParameters);

        try {

            final WorkflowRulesContainer.ThreadSafe workflowRulesContainer =
                    ContextStore.get().getUserSession().findProject().loadWorkflowRules();

            workflowRulesContainer.runRule(workflowRulesContainer.getWorkflowRule(ruleId),
                methodName, context);

        } catch (final Throwable exception) {

            MoveCommonUtil.throwException(errorMsg, args);
        }
    }

    /**
     * Getter for the moveOrdersToAssignmentsConverter property.
     *
     * @return the moveOrdersToAssignmentsConverter property.
     */
    public IMoveOrdersToAssignmentsConverter getMoveOrdersToAssignmentsConverter() {
        return this.moveOrdersToAssignmentsConverter;
    }

    /**
     * Setter for the moveOrdersToAssignmentsConverter property.
     *
     * @param moveOrdersToAssignmentsConverter the moveOrdersToAssignmentsConverter to set.
     */
    public void setMoveOrdersToAssignmentsConverter(
            final IMoveOrdersToAssignmentsConverter moveOrdersToAssignmentsConverter) {
        this.moveOrdersToAssignmentsConverter = moveOrdersToAssignmentsConverter;
    }

    /**
     * Getter for the roomTransactionDao property.
     *
     * @return the roomTransactionDao property.
     */
    public IRoomTransactionDao getRoomTransactionDao() {
        return this.roomTransactionDao;
    }

    /**
     * Setter for the roomTransactionDao property.
     *
     * @param roomTransactionDao the roomTransactionDao to set.
     */
    public void setRoomTransactionDao(final IRoomTransactionDao roomTransactionDao) {
        this.roomTransactionDao = roomTransactionDao;
    }

    /**
     * Getter for the actionItemDao property.
     *
     * @return the actionItemDao property.
     */
    public IActionItemDao getActionItemDao() {
        return this.actionItemDao;
    }

    /**
     * Setter for the actionItemDao property.
     *
     * @param actionItemDao the actionItemDao to set.
     */
    public void setActionItemDao(final IActionItemDao actionItemDao) {
        this.actionItemDao = actionItemDao;
    }

    /**
     * Getter for the moveProjectDao property.
     *
     * @return the moveProjectDao property.
     */
    public IProjectDao getMoveProjectDao() {
        return this.moveProjectDao;
    }

    /**
     * Setter for the moveProjectDao property.
     *
     * @param moveProjectDao the moveProjectDao to set.
     */
    public void setMoveProjectDao(final IProjectDao moveProjectDao) {
        this.moveProjectDao = moveProjectDao;
    }

    /**
     * Getter for the moveOrderDao property.
     *
     * @return the moveOrderDao property.
     */
    public IMoveOrderDao getMoveOrderDao() {
        return this.moveOrderDao;
    }

    /**
     * Setter for the moveOrderDao property.
     *
     * @param moveOrderDao the moveOrderDao to set.
     */
    public void setMoveOrderDao(final IMoveOrderDao moveOrderDao) {
        this.moveOrderDao = moveOrderDao;
    }

    /**
     * Getter for the workRequestDao property.
     *
     * @return the workRequestDao property.
     */
    public IWorkRequestDao getWorkRequestDao() {
        return this.workRequestDao;
    }

    /**
     * Setter for the workRequestDao property.
     *
     * @param workRequestDao the workRequestDao to set.
     */
    public void setWorkRequestDao(final IWorkRequestDao workRequestDao) {
        this.workRequestDao = workRequestDao;
    }

    /**
     * Getter for the employeeDao property.
     *
     * @return the employeeDao property.
     */
    public IDao<Employee> getEmployeeDao() {
        return this.employeeDao;
    }

    /**
     * Setter for the employeeDao property.
     *
     * @param employeeDao the employeeDao to set.
     */
    public void setEmployeeDao(final IDao<Employee> employeeDao) {
        this.employeeDao = employeeDao;
    }

    /**
     * convert record values to hash map.
     *
     * @param record data record
     * @return map
     */
    private Map<String, Object> handleRecordValue(final DataRecord record) {

        final Map<String, Object> result = new HashMap<String, Object>();

        final List<DataValue> fields = record.getFields();
        for (final DataValue dataValue : fields) {
            final DataValue field = dataValue;
            final String key = field.getName();
            final Object value = field.getValue();
            result.put(key, value);
        }
        return result;
    }
}
