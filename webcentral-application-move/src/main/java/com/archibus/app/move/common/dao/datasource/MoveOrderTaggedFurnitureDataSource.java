package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IMoveOrderTaggedFurnitureDao;
import com.archibus.app.move.common.domain.MoveOrderTaggedFurniture;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Move Order Tagged Furniture.
 *
 * @author Zhang Yi
 *
 */
public class MoveOrderTaggedFurnitureDataSource extends ObjectDataSourceImpl<MoveOrderTaggedFurniture>
		implements IMoveOrderTaggedFurnitureDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "mo_id", "moveOrderId" },
			{ "ta_id", "taggedFurnitureId" }, { "fn_std", "furnitureStandard" }, { "from_bl_id", "fromBuildingId" },
			{ "from_fl_id", "fromFloorId" }, { "from_rm_id", "fromRoomId" }, { "cost_moving", "costMoving" } };

	/**
	 * Constructs MoveOrderTaggedFurnitureDataSource, mapped to
	 * <code>mo_ta</code> table, using <code>MoveOrderTaggedFurniture</code>
	 * bean.
	 */
	public MoveOrderTaggedFurnitureDataSource() {
		super("moveOrderTaggedFurniture", "mo_ta");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

}
