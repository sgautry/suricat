package com.archibus.app.move.common;

import java.util.*;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveCalculatorService. Provides calculating
 * methods related to Move Management.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr
 * methods about move calculation.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveCalculatorService implements IMoveCalculatorService {

	@Override
	public void calculateHistoricalEmployeeHeadcount() {
		final Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		final int month = now.get(Calendar.MONTH) + 1;
		final int year = now.get(Calendar.YEAR);

		/*
		 * 07/03/2010 IOAN Kb 3026371 check if data already exists for current
		 * month and exit is data exist
		 */

		final DataSource ds = DataSourceFactory.createDataSource();
		ds.addTable("hist_em_count");
		ds.addField("year");
		ds.addField("month");
		ds.addRestriction(Restrictions.and(Restrictions.eq("hist_em_count", "year", String.valueOf(year)),
				Restrictions.eq("hist_em_count", "month", String.valueOf(month))));
		final List<DataRecord> recs = ds.getRecords();
		if (!recs.isEmpty()) {
			return;
		}

		final String sqlStatement1 = "INSERT INTO hist_em_count (bl_id,dv_id,dp_id,year,month,count_em)"
				+ "(SELECT bl_id,dv_id,dp_id," + year + "," + month + ",count(*) "
				+ "FROM em WHERE bl_id IS NOT NULL AND dv_id IS NOT NULL AND dp_id IS NOT NULL "
				+ "GROUP by bl_id,dv_id,dp_id)";

		final String sqlStatement2 = "INSERT INTO hist_em_count (bl_id,dv_id,dp_id,year,month,count_em) "
				+ "(SELECT 'None',dv_id,dp_id," + year + "," + month + ",count(*) "
				+ "FROM em WHERE bl_id IS NULL AND dv_id IS NOT NULL AND dp_id IS NOT NULL " + "GROUP by dv_id,dp_id)";

		final String sqlStatement3 = "INSERT INTO hist_em_count (bl_id,dv_id,dp_id,year,month,count_em) "
				+ "(SELECT bl_id,dv_id,'None', " + year + "," + month + ",count(*) "
				+ "FROM em WHERE bl_id IS NOT NULL AND dv_id IS NOT NULL AND dp_id IS NULL " + "GROUP by bl_id,dv_id)";

		final String sqlStatement4 = "INSERT INTO hist_em_count (bl_id,dv_id,dp_id,year,month,count_em) "
				+ "(SELECT bl_id,'None','None', " + year + "," + month + ",count(*) "
				+ "FROM em WHERE bl_id IS NOT NULL AND dv_id IS NULL AND dp_id IS NULL " + "GROUP by bl_id)";

		SqlUtils.executeUpdate("hist_em_count", sqlStatement1.toString());
		SqlUtils.executeUpdate("hist_em_count", sqlStatement2.toString());
		SqlUtils.executeUpdate("hist_em_count", sqlStatement3.toString());
		SqlUtils.executeUpdate("hist_em_count", sqlStatement4.toString());
		SqlUtils.commit();
	}

}
