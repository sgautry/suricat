package com.archibus.app.move.notify;

import java.io.UnsupportedEncodingException;
import java.util.*;

import com.archibus.app.common.notification.dao.datasource.NotificationMessageDataSource;
import com.archibus.app.common.notification.domain.NotificationMessage;
import com.archibus.app.common.notification.message.NotificationMessageFormatter;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.utility.*;

/**
 *
 * Move Management email notifications. Provides methods to format and send
 * email notifications.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @since 22.1
 *
 */
public class MoveNotifications {

	/**
	 * Activity id.
	 */
	private static final String ACTIVITY_ID = "AbMoveManagement";

	/**
	 * Activity id.
	 */
	private static final String REFERENCED_BY = "MOVE_NOTIFICATIONS_WFR";

	/**
	 * Activity id.
	 */
	private static final String UNDERSCORE = "_";

	/**
	 * If notification is for group move.
	 */
	private boolean isGroupMove;

	/**
	 * Move id.
	 */
	private int moveId;

	/**
	 * Project id.
	 */
	private String projectId;

	/**
	 * Email subject.
	 */
	private NotificationMessage subject;

	/**
	 * Email body.
	 */
	private NotificationMessage body;

	/**
	 * Free marker variables data model.
	 */
	private Map<String, Object> dataModel;

	/**
	 * Locale DataSource.
	 */
	private final DataSource localeDs = DataSourceFactory.createDataSourceForFields("afm_users",
			new String[] { "locale", "email" });

	/**
	 * Send email to recipient.
	 *
	 * @param moveStep
	 *            move step (APPROVE, REQUEST, etc)
	 * @param notificationType
	 *            notification type (INFORM, REQUEST, INFORM_CONTACT)
	 * @param recipient
	 *            recipient email
	 * @throws ExceptionBase
	 *             exception
	 */
	public void sendEmailToRecipient(final String moveStep, final String notificationType, final String recipient)
			throws ExceptionBase {

		if (StringUtil.notNullOrEmpty(recipient)) {
			// Load email subject and body from database
			getEmailSubjectAndBody(moveStep, notificationType);

			// prepare data model.
			try {
				prepareDataModel();
			} catch (final UnsupportedEncodingException exception) {
				throw new ExceptionBase("Unsuported URL Encoding", exception);
			}

			final NotificationMessageFormatter messageFormatter = new NotificationMessageFormatter();
			final String recipientLocale = this.getLocaleByEmailAddress(recipient);
			final String formattedSubject = messageFormatter.formatMessage(recipientLocale, this.subject,
					this.dataModel);
			final String formattedBody = messageFormatter.formatMessage(recipientLocale, this.body, this.dataModel);

			sendEmail(formattedBody, formattedSubject, recipient, ACTIVITY_ID, new ArrayList<String>());
		}

	}

	/**
	 * Setter for the isGroupMove property.
	 *
	 * @see isGroupMove
	 * @param isGroupMoveLcl
	 *            the isGroupMove to set
	 */

	public void setGroupMove(final boolean isGroupMoveLcl) {
		this.isGroupMove = isGroupMoveLcl;
	}

	/**
	 * Setter for the moveId property.
	 *
	 * @see moveId
	 * @param moveId
	 *            the moveId to set
	 */

	public void setMoveId(final int moveId) {
		this.moveId = moveId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @see projectId
	 * @param projectId
	 *            the projectId to set
	 */

	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Load email subject and body from messages table.
	 *
	 * @param moveStep
	 *            move step (APPROVE, REQUEST, etc)
	 * @param notificationType
	 *            notification type (INFORM, REQUEST)
	 */
	private void getEmailSubjectAndBody(final String moveStep, final String notificationType) {
		final String moveType = this.isGroupMove ? "GROUP" : "SINGLE";
		final String subjectId = (moveStep + UNDERSCORE + moveType + UNDERSCORE + notificationType + UNDERSCORE
				+ "SUBJECT").toUpperCase();
		final String bodyId = (moveStep + UNDERSCORE + moveType + UNDERSCORE + notificationType + UNDERSCORE + "BODY")
				.toUpperCase();

		final NotificationMessageDataSource messageDataSource = new NotificationMessageDataSource();
		this.subject = messageDataSource.getByPrimaryKey(ACTIVITY_ID, REFERENCED_BY, subjectId);
		this.body = messageDataSource.getByPrimaryKey(ACTIVITY_ID, REFERENCED_BY, bodyId);
	}

	/**
	 * Prepare data model object.
	 *
	 * @throws UnsupportedEncodingException
	 *             URL Encoder exception
	 */
	private void prepareDataModel() throws UnsupportedEncodingException {
		this.dataModel = new HashMap<String, Object>();
		// add
		this.dataModel.put("web_central_path", ContextStore.get().getContextPath());

		if (this.isGroupMove) {
			final Map<String, Object> project = new HashMap<String, Object>();
			project.put("project_id", this.projectId);
			this.dataModel.put("project", project);
			this.dataModel.put("encoded_project_id", java.net.URLEncoder.encode(this.projectId, "UTF-8"));

		} else {
			final Map<String, Object> mo = new HashMap<String, Object>();
			mo.put("mo_id", this.moveId);
			this.dataModel.put("mo", mo);
			this.dataModel.put("encoded_mo_id", this.moveId);
		}
	}

	/**
	 * Send email message.
	 *
	 * @param emailBody
	 *            email body
	 * @param emailSubject
	 *            email subject
	 * @param recipient
	 *            email recipient
	 * @param activityCode
	 *            activity id
	 * @param attachmentFileNames
	 *            list of attachment files
	 */
	private void sendEmail(final String emailBody, final String emailSubject, final String recipient,
			final String activityCode, final List<String> attachmentFileNames) {

		final MailMessage message = new MailMessage();
		message.setSubject(emailSubject);
		message.setText(emailBody);
		message.setTo(recipient);
		message.setAttachments(attachmentFileNames);
		message.setActivityId(activityCode);

		final MailSender sender = new MailSender();
		sender.send(message);
	}

	/**
	 * Get locale name for given email address.
	 *
	 * @param email
	 *            email address
	 * @return string
	 */
	private String getLocaleByEmailAddress(final String email) {

		final DataRecord record = this.localeDs.getRecord("email = " + MoveCommonUtil.getLiteralString(email));

		final String locale = record == null ? "" : record.getString("afm_users.locale");

		return locale;
	}

}
