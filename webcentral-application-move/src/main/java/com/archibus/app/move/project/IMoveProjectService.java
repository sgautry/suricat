package com.archibus.app.move.project;

import com.archibus.datasource.data.DataRecord;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move Project(group move) related business behaviors.
 * <p>
 *
 * Referenced by Workflow rule handler class of Move Management to delegate
 * their wfr methods related to group move.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveProjectService {

	/**
	 * NEW INSERT INTO mo
	 * (project_id,status,em_id,requestor,dept_contact,date_created,mo_type,
	 * mo_class ,description,date_start_req,to_bl_id,to_fl_id,to_rm_id,
	 * phone,phone_dept_contact,dv_id,dp_id
	 * ,ac_id,from_bl_id,from_fl_id,from_rm_id,from_dv_id,from_dp_id,
	 * to_dv_id,to_dp_id,from_phone
	 * ,to_phone,from_fax,to_fax,from_mailstop,from_jk_id_data,
	 * from_jk_id_voice) SELECT project.project_id , %status% , %em_id% ,
	 * %requestor% , project.dept_contact , %date_created% , %mo_type% ,
	 * %mo_class% , %description% , %date_start_req% , %to_bl_id% , %to_fl_id% ,
	 * %to_rm_id% , project.phone_req, project.phone_dept_contact,
	 * project.dv_id, project.dp_id, project.ac_id, em.bl_id, em.fl_id,
	 * em.rm_id, em.dv_id, em.dp_id, em.dv_id, em.dp_id, em.phone, em.phone,
	 * em.fax, em.fax, em.mailstop, %from_jk_id_data% , %from_jk_id_voice% FROM
	 * em, project WHERE em.em_id = %em_id%, project.project_id = %project_id%
	 *
	 * @param record
	 */
	void addProjectMove(DataRecord record);

	/**
	 * INSERT INTO mo
	 * (project_id,status,em_id,requestor,dept_contact,date_created,mo_type,
	 * mo_class ,description, date_start_req,
	 * to_bl_id,to_fl_id,to_rm_id,phone,phone_dept_contact,dv_id,dp_id,ac_id)
	 * SELECT project.project_id, %status% , %em_id% , %requestor% ,
	 * project.dept_contact , %date_created% , %mo_type% , %mo_class% ,
	 * %description% , %date_start_req% , %to_bl_id% , %to_fl_id% , %to_rm_id% ,
	 * project.phone_req, project.phone_dept_contact, project.dv_id,
	 * project.dp_id, project.ac_id FROM project WHERE project.project_id =
	 * %project_id%
	 *
	 * @param record
	 */
	void addProjectMoveNewHire(DataRecord record);

	/**
	 * *NEW* INSERT INTO
	 * mo(project_id,status,em_id,requestor,dept_contact,date_created,mo_type,
	 * mo_class ,description,
	 * date_start_req,phone,phone_dept_contact,dv_id,dp_id,ac_id,from_bl_id,
	 * from_fl_id ,from_rm_id,
	 * from_dv_id,from_dp_id,from_phone,from_fax,from_mailstop,from_jk_id_data,
	 * from_jk_id_voice) SELECT project.project_id, %status% , %em_id% ,
	 * %requestor% , project.dept_contact, %date_created%, %mo_type% ,
	 * %mo_class% , %description% , %date_start_req%, project.phone_req,
	 * project.phone_dept_contact, project.dv_id, project.dp_id, project.ac_id,
	 * em.bl_id, em.fl_id, em.rm_id, em.dv_id, em.dp_id, em.phone, em.fax,
	 * em.mailstop, %from_jk_id_data% , %from_jk_id_voice% FROM em, project
	 * WHERE em.em_id = %em_id%, project.project_id = %project_id%
	 *
	 * @param record
	 */
	void addProjectMoveEmployeeLeaving(DataRecord record);

	/**
	 * *NEW* INSERT INTO
	 * mo(project_id,status,em_id,requestor,dept_contact,date_created,mo_type,
	 * mo_class ,description,
	 * date_start_req,phone,phone_dept_contact,dv_id,dp_id,ac_id,from_bl_id,
	 * from_fl_id ,from_rm_id, from_dv_id,from_dp_id,to_dv_id,to_dp_id) SELECT
	 * project.project_id, %status% , %em_id% , %requestor% ,
	 * project.dept_contact, %date_created%, %mo_type% , %mo_class% ,
	 * %description% , %date_start_req%, project.phone_req,
	 * project.phone_dept_contact, project.dv_id, project.dp_id, project.ac_id,
	 * eq.bl_id, eq.fl_id, eq.rm_id, eq.dv_id, eq.dp_id, eq.dv_id, eq.dp_id FROM
	 * eq, project WHERE eq.eq_id = %em_id%, project.project_id = %project_id%
	 *
	 * @param record
	 */
	void addProjectMoveEquipment(DataRecord record);

	/**
	 * INSERT INTO mo
	 * (project_id,status,em_id,requestor,dept_contact,date_created,mo_type,
	 * mo_class ,description,
	 * date_start_req,to_bl_id,to_fl_id,to_rm_id,phone,phone_dept_contact,dv_id,
	 * dp_id,ac_id) SELECT project.project_id , %status% , %em_id% , %requestor%
	 * , project.dept_contact , %date_created% , %mo_type% , %mo_class% ,
	 * %description% , %date_start_req% , %to_bl_id% , %to_fl_id% , %to_rm_id% ,
	 * project.phone_req, project.phone_dept_contact, project.dv_id,
	 * project.dp_id, project.ac_id FROM project WHERE project.project_id =
	 * %project_id%
	 *
	 * @param record
	 */
	void addProjectMoveAsset(DataRecord record);

	/**
	 *
	 * INSERT INTO mo
	 * (project_id,status,em_id,requestor,dept_contact,date_created,mo_type,
	 * mo_class ,description, date_start_req,to_bl_id,to_fl_id,to_rm_id,
	 * phone,phone_dept_contact,dv_id,dp_id,ac_id,from_bl_id,from_fl_id,
	 * from_rm_id) SELECT project.project_id , %status% , %em_id% , %requestor%
	 * , project.dept_contact , %date_created% , %mo_type% , %mo_class% ,
	 * %description% , %date_start_req% , %to_bl_id% , %to_fl_id% , %to_rm_id% ,
	 * project.phone_req, project.phone_dept_contact, project.dv_id,
	 * project.dp_id, project.ac_id, %from_bl_id% , %from_fl_id% , %from_rm_id%
	 * FROM project WHERE project.project_id = %project_id% TODO
	 * addProjectMoveRoom.
	 *
	 * @param record
	 */
	void addProjectMoveRoom(DataRecord record);

	/**
	 * NEW* INSERT INTO project (contact_id,
	 * project_id,project_type,description,bl_id, date_start,date_end,requestor,
	 * dept_contact,phone_req,phone_dept_contact,dv_id,dp_id,date_created)
	 * SELECT %contact_id% , %project_id% , 'Move' , %description% , %bl_id% ,
	 * %date_start%, %date_end% , %requestor%, %dept_contact%, requestor.phone,
	 * dept_contact.phone, dept_contact.dv_id, dept_contact.dp_id,
	 * %current_date% FROM em requestor, em dept_contact WHERE requestor.em_id =
	 * %requestor% AND dept_contact.em_id = %dept_contact%
	 *
	 * @param record
	 */
	void addMoveProject(DataRecord record);

	/**
	 *
	 * INSERT INTO mo (project_id, status,requestor,phone, dept_contact,
	 * phone_dept_contact, dv_id, dp_id, ac_id, mo_type, date_start_req,
	 * em_id,from_bl_id,from_fl_id,from_rm_id,from_dv_id
	 * ,from_dp_id,from_phone,from_fax,from_mailstop,
	 * to_dv_id,to_dp_id,to_phone,to_fax) SELECT project.project_id, 'Created',
	 * %requestor% project.phone_req, project.dept_contact,
	 * project.phone_dept_contact, project.dv_id, project.dp_id, project.ac_id,
	 * 'Employee', %date_start_req%, em.em_id, em.bl_id, em.fl_id, em.rm_id,
	 * em.dv_id, em.dp_id, em.phone, em.fax, em.mailstop, em.dv_id, em.dp_id,
	 * em.phone, em.fax FROM em, project WHERE project.project_id = %project_id%
	 * AND em.em_id NOT IN (SELECT em_id FROM mo WHERE project_id=%project_id%
	 * AND mo_type='Employee') AND %strWhereClause%
	 *
	 * @param record
	 */
	void addBulkMoves(DataRecord record);

}
