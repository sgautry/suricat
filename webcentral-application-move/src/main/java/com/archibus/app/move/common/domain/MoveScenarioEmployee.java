package com.archibus.app.move.common.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Move Scenario Employee.
 * <p>
 * Mapped to mo_scenario_em table.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public class MoveScenarioEmployee {

	/**
	 * Move Scenario Employee Id.
	 */
	private Integer id;

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Move Scenario Id.
	 */
	private String scenarioId;

	/**
	 * Move project Id.
	 */
	private String projectId;

	/**
	 * Move Scenario Employee Id.
	 */
	private String employeeId;

	/**
	 * Move Scenario Team Id.
	 */
	private String teamId;

	/**
	 * Move Scenario Layout plan's file name.
	 */
	private String fileName;

	/**
	 * Move Scenario To Building Id.
	 */
	private String buildingId;

	/**
	 * Move Scenario To Floor Id.
	 */
	private String floorId;

	/**
	 * Move Scenario To Room Id.
	 */
	private String roomId;

	/**
	 * Move Scenario Start Date.
	 */
	private Date dateStart;

	/**
	 * Getter for the scenarioId property.
	 *
	 * @return the scenarioId property.
	 */
	public String getScenarioId() {
		return this.scenarioId;
	}

	/**
	 * Setter for the scenarioId property.
	 *
	 * @param scenarioId
	 *            the scenarioId to set.
	 */
	public void setScenarioId(final String scenarioId) {
		this.scenarioId = scenarioId;
	}

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the employeeId property.
	 *
	 * @return the employeeId property.
	 */
	public String getEmployeeId() {
		return this.employeeId;
	}

	/**
	 * Setter for the employeeId property.
	 *
	 * @param employeeId
	 *            the employeeId to set.
	 */
	public void setEmployeeId(final String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Getter for the teamId property.
	 *
	 * @return the teamId property.
	 */
	public String getTeamId() {
		return this.teamId;
	}

	/**
	 * Setter for the teamId property.
	 *
	 * @param teamId
	 *            the teamId to set.
	 */
	public void setTeamId(final String teamId) {
		this.teamId = teamId;
	}

	/**
	 * Getter for the fileName property.
	 *
	 * @return the fileName property.
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Setter for the fileName property.
	 *
	 * @param fileName
	 *            the fileName to set.
	 */
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the floorId property.
	 *
	 * @return the floorId property.
	 */
	public String getFloorId() {
		return this.floorId;
	}

	/**
	 * Setter for the floorId property.
	 *
	 * @param floorId
	 *            the floorId to set.
	 */
	public void setFloorId(final String floorId) {
		this.floorId = floorId;
	}

	/**
	 * Getter for the roomId property.
	 *
	 * @return the roomId property.
	 */
	public String getRoomId() {
		return this.roomId;
	}

	/**
	 * Setter for the roomId property.
	 *
	 * @param roomId
	 *            the roomId to set.
	 */
	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}

	/**
	 * Getter for the dateStart property.
	 *
	 * @return the dateStart property.
	 */
	public Date getDateStart() {
		return this.dateStart;
	}

	/**
	 * Setter for the dateStart property.
	 *
	 * @param dateStart
	 *            the dateStart to set.
	 */
	public void setDateStart(final Date dateStart) {
		this.dateStart = dateStart;
	}

}
