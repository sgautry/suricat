package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Move Order Equipment.
 * <p>
 * Mapped to mo_eq table.
 *
 * @author Zhang Yi
 *
 */
public class MoveOrderEquipment {

	/**
	 * ID of the Move Order.
	 */
	private int moveOrderId;

	/**
	 * Move Equipment ID.
	 */
	private String equipmentId;

	/**
	 * Equipment Standard.
	 */
	private String equipmentStandard;

	/**
	 * Move Equipment Cost.
	 */
	private double costMoving;

	/**
	 * Getter for the moveOrderId property.
	 *
	 * @return the moveOrderId property.
	 */
	public int getMoveOrderId() {
		return this.moveOrderId;
	}

	/**
	 * Setter for the moveOrderId property.
	 *
	 * @param moveOrderId
	 *            the moveOrderId to set.
	 */
	public void setMoveOrderId(final int moveOrderId) {
		this.moveOrderId = moveOrderId;
	}

	/**
	 * Getter for the equipmentId property.
	 *
	 * @return the equipmentId property.
	 */
	public String getEquipmentId() {
		return this.equipmentId;
	}

	/**
	 * Setter for the equipmentId property.
	 *
	 * @param equipmentId
	 *            the equipmentId to set.
	 */
	public void setEquipmentId(final String equipmentId) {
		this.equipmentId = equipmentId;
	}

	/**
	 * Getter for the equipmentStandard property.
	 *
	 * @return the equipmentStandard property.
	 */
	public String getEquipmentStandard() {
		return this.equipmentStandard;
	}

	/**
	 * Setter for the equipmentStandard property.
	 *
	 * @param equipmentStandard
	 *            the equipmentStandard to set.
	 */
	public void setEquipmentStandard(final String equipmentStandard) {
		this.equipmentStandard = equipmentStandard;
	}

	/**
	 * Getter for the costMoving property.
	 *
	 * @return the costMoving property.
	 */
	public double getCostMoving() {
		return this.costMoving;
	}

	/**
	 * Setter for the costMoving property.
	 *
	 * @param costMoving
	 *            the costMoving to set.
	 */
	public void setCostMoving(final double costMoving) {
		this.costMoving = costMoving;
	}

	/**
	 * Getter for the fromBuildingId property.
	 *
	 * @return the fromBuildingId property.
	 */
	public String getFromBuildingId() {
		return this.fromBuildingId;
	}

	/**
	 * Setter for the fromBuildingId property.
	 *
	 * @param fromBuildingId
	 *            the fromBuildingId to set.
	 */
	public void setFromBuildingId(final String fromBuildingId) {
		this.fromBuildingId = fromBuildingId;
	}

	/**
	 * Getter for the fromFloorId property.
	 *
	 * @return the fromFloorId property.
	 */
	public String getFromFloorId() {
		return this.fromFloorId;
	}

	/**
	 * Setter for the fromFloorId property.
	 *
	 * @param fromFloorId
	 *            the fromFloorId to set.
	 */
	public void setFromFloorId(final String fromFloorId) {
		this.fromFloorId = fromFloorId;
	}

	/**
	 * Getter for the fromRoomId property.
	 *
	 * @return the fromRoomId property.
	 */
	public String getFromRoomId() {
		return this.fromRoomId;
	}

	/**
	 * Setter for the fromRoomId property.
	 *
	 * @param fromRoomId
	 *            the fromRoomId to set.
	 */
	public void setFromRoomId(final String fromRoomId) {
		this.fromRoomId = fromRoomId;
	}

	/**
	 *
	 * Move Equipment from Building.
	 */
	private String fromBuildingId;

	/**
	 * Move Equipment from Floor.
	 */
	private String fromFloorId;

	/**
	 * Move Equipment from Room.
	 */
	private String fromRoomId;

}
