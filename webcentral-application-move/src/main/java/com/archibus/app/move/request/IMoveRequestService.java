package com.archibus.app.move.request;

import com.archibus.datasource.data.DataRecord;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move Request related business behaviors.
 * <p>
 *
 * Referenced by Workflow rule handler class of Move Management to delegate their wfr methods
 * related to move request logics.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveRequestService {

    /**
     * Close a group move request by given move project id.
     *
     * When we close a Group Move as part of the closeGroupMove workflow rule we need to check if
     * all moves and actions are completed. We need to run two SELECT statements. The first checks
     * for moves: SELECT 1 FROM mo WHERE project_id = 'xxx' AND status NOT IN
     * ('Completed-Verified','Approved-Cancelled','Issued-Stopped') If there are moves that are not
     * completed and verified we can provide the following message: Please make sure to mark all
     * moves as Completed-Verified, Approved-Cancelled or Issued-Stopped prior to closing this
     * project. The second statement will check for activity log records: SELECT 1 FROM activity_log
     * WHERE project_id = 'xxx' AND status NOT IN ('COMPLETED-V','REJECTED','CANCELLED') If there
     * are actions that are not completed and verified we can provide the following message: Please
     * make sure to mark all actions as COMPLETED-VERIFIED, REJECTED or CANCELLED prior to closing
     * this project.
     *
     * @param projectId
     */
    void closeGroupMove(String projectId);

    /**
     *
     * Close an individual move request by given move order id.
     *
     * When we close an Individual Move as part of the closeIndividualMove worklfow rule we need to
     * check if any associated actions are completed. The SELECT statement would be: SELECT 1 FROM
     * activity_log WHERE mo_id = xxx AND status <> 'COMPLETED-V' If this selection returns records
     * that means that there are activity log records that remain open.
     *
     * UPDATE mo set status='Closed', date_closed=CURRENT where mo_id=xxx UPDATE activity_log set
     * status='Closed', date_closed=CURRENT where mo_id=xxx NOTE: John will add a date_closed to the
     * activity_log table
     *
     * @param moId
     */
    void closeIndividualMove(String moId);

    /**
     * Approve an individual move.
     *
     * Takes an mo_id and the approval manager status field name (like 'apprv_mgr1_status'). The
     * rule: 1) Updates the mo record for that mo_id and sets the approval manager status field to
     * 'A': String mo_id = getParameter("mo_id"); String status_field =
     * getParameter("status_field"); String sql = "UPDATE mo SET " + status_field + " = 'A' WHERE
     * mo_id = " +mo_id;
     *
     * This action will update the move status to 'A' only if all the Approving Managers have
     * already approved the move. Otherwise it will simply save the action each Approving Manager
     * has taken by updating the Date Manager Approved field and the Manager 1 Approve Status field.
     * Individual Moves: UPDATE mo SET status='A' WHERE mo.mo_id=xxx
     *
     *
     * @param moId
     * @param approveManager1
     * @param approveManager2
     * @param approveManager3
     */
    void approveIndividualMove(String moId, String approveManager1, String approveManager2,
            String approveManager3);

    /**
     * Approve a group move.
     *
     * @param projectId
     * @param approveManager1
     * @param approveManager2
     * @param approveManager3
     */
    void approveGroupMove(String projectId, String approveManager1, String approveManager2,
            String approveManager3);

    /**
     * Route group move for approval.
     *
     * For group moves UPDATE project set status = 'Requested-Routed' WHERE project.project_id =
     * xxxx For group moves we would also update the status of any associated moves: UPDATE mo set
     * status='Requested-Routed' WHERE mo.project_id = xxxx
     *
     * @param projectId
     * @param approveManager1
     * @param approveManager2
     * @param approveManager3
     */
    void routeGroupMoveForApproval(String projectId, String approveManager1, String approveManager2,
            String approveManager3);

    /**
     * Route an individual move for approval.
     *
     * UPDATE mo set status = 'Requested-Routed' appr_mgr1 =
     * appr_mgr1,appr_mgr1_email=appr_mgr1_email, appr_mgr1_phone= appr_mgr1_phone... WHERE mo.mo_id
     * = xxx
     *
     * An email will be sent to all the Approving Managers as well as the Move Coordinator. The
     * email will point the managers to the URL for approving the individual move. The text in the
     * email can be: 'Please indicate your approval to move order: xxxx by clicking in the URL below
     * and selecting from the action options that appear next to your name'.
     *
     * @param moId
     * @param approveManager1
     * @param approveManager2
     * @param approveManager3
     */
    void routeIndividualMoveForApproval(String moId, String approveManager1, String approveManager2,
            String approveManager3);

    /**
     * Auto approve an individual move.
     *
     * UPDATE mo SET apprv_mgr1 = literal(move_coordinator,context),
     * apprv_mgr1_status='Approved',apprv_mgr1_date=CURRENT,status='Approved' WHERE mo_id=xxx and
     * mo.status<>'Approved'
     *
     * @param moId
     *
     */
    void autoApproveIndividualMove(String moId);

    /**
     * Auto approve an individual move.
     *
     * UPDATE project SET apprv_mgr1 = literal(proj_mgr,context),
     * apprv_mgr1_status='Approved',apprv_mgr1_date=CURRENT,status='Approved' WHERE project_id='xxx'
     *
     * @param projectId
     */
    void autoApproveGroupMove(String projectId);

    /**
     * Complete selected moves: update their status to 'completed'. After that also update status of
     * their associated service requests.
     *
     * @param moIds
     */
    void completeSelectedMoves(String moIds);

    /**
     * Complete selected Action Items: update their status to 'completed'.
     *
     * @param activityLogIds
     */
    void completeSelectedActions(String activityLogIds);

    /**
     * Issue an individual move.
     *
     * Issuing a move will trigger a series of emails to the various parties involved. Any automatic
     * work requests associated with the actions will also be triggered. First an update SQL
     * statement will set the move's status to 'Issued' and the issue date to current date. The
     * status of all associated move actions will also be set to 'Issued'. UPDATE mo set status =
     * 'Issued-In Process', date_issued = current date where mo_id = xxx (No single quotes) UPDATE
     * activity_log set status='Issued-In Process' where mo_id=xxx (No single quotes) To generate
     * work requests we would issue separate INSERT statements for each action that will need a work
     * request.
     *
     * @param moId
     * @param requestor
     */
    void issueIndividualMove(String moId, String requestor);

    /**
     * Issue a group move.
     *
     * This is the action that starts the project and issues any associated actions and in turn work
     * requests associated with actions. The save statement will first change any edits made to the
     * Issue form. The workflow rule issueGroupMove will add the following: Update the status for
     * the project and the individual moves and actions. UPDATE project set status=' Issued-In
     * Process', date_issued = current date WHERE project.project_id=xxx
     *
     * UPDATE mo SET status='Issued-In Process', date_issued = current date WHERE
     * project.project_id=xxx
     *
     * UPDATE activity_log SET status='Issued-In Process' WHERE project.project_id=xxx
     *
     * @param projectId
     * @param requestor
     */
    void issueGroupMove(String projectId, String requestor);

    /**
     * Reject an individual move.
     *
     * Takes an mo_id and an approval manager status field name. The rule: 1) Updates the mo record
     * and sets the approval manager status field to "R" - Rejected 2) Sets the mo status field to
     * "Requested-Rejected". There is no need to query the other status fields, as a single
     * rejection is enough to reject the move. These two steps can be one SQL statement: String
     * mo_id = getParameter("mo_id"); String status_field = getParameter("status_field"); String sql
     * = "UPDATE mo SET " + status_field + " = 'R', status = 'Requested-Rejected'; sql += ' WHERE
     * mo_id = ' + mo_id;
     *
     * @param moId
     * @param approveManager1
     * @param approveManager2
     * @param approveManager3
     */
    void rejectIndividualMove(String moId, String approveManager1, String approveManager2,
            String approveManager3);

    /**
     * Reject a group move.
     *
     * UPDATE project set status='Approved' WHERE project.project_id=xxx UPDATE mo SET
     * status='Approved' WHERE project.project_id=xxx
     *
     * @param projectId
     * @param approveManager1
     * @param approveManager2
     * @param approveManager3
     */
    void rejectGroupMove(String projectId, String approveManager1, String approveManager2,
            String approveManager3);

    /**
     * This action will update a move record and set the status to Requested and the Date Requested
     * as the current date. This will also update any fields that have been changed in the form.
     * This rule triggers upon the pressing of the Request button. This button is only visible if
     * the move has the status of Created. Only the Save button is visible if the move status is set
     * to Requested. This action will also send a notification email to the requestor and one to the
     * department contact if one is entered and based on the dept contact email in the employee
     * table. The message for the notification will be built in the Javascript function and will be
     * passed as an argument to the workflow rule. Following is a sample message: Thank you for
     * submitting a move request. We will notify you via email upon the Approval, Issuing and
     * Closing of this move request. Mandatory fields are: Move Description, Requestor Name and
     * Phone, Division, Department and Requested Start Date. Once the user clicks on Request we
     * first Save the data either as a separate workflow rule or as an UPDATE within javascript. We
     * can then trigger a Workflow rule that will have the following two components: 1) Issue an SQL
     * statement to update the move status to requested and set the date requested field 2) Send an
     * email to both the Requestor and Department Contact based on their emails in the employee
     * table: The arguments passed to the Request Individual Move Workflow Rule program would be:
     * Move Code, Move Status, Date Requested, Request Message The SQL statement would be: UPDATE mo
     * set status='Requested',date_requested=CURRENT where mo_id = xxx
     *
     * @param moId
     */
    void requestIndividualMove(String moId);

    /**
     * Rule triggers upon the pressing of the Request and Approve button. This is only visible if
     * the person logged in belongs to the Move Coordinator role. Update a move project record and
     * set the status to Approved and the Date Approved to the current date. Set the Move
     * Coordinator name based on the person logged in. (If they were not the Move Coordinator they
     * would not have access to this button.) Update all the project moves and set their status to
     * Approved and the Date Approved to the current date. Enter the Move Coordinator as the
     * Approving Manager 1 and the Date Manager 1 Approved to the current date and the Manager 1
     * Approve Status field to Approved. Send an email notification to the requestor and the
     * department coordinator as follows: Thank you for submitting a move request. We will notify
     * you via email upon the Approval, Issuing and Closing of this move request. Mandatory fields
     * are: Project Description, Requestor Name, Phone, Division, Department and Requested Start
     * Date. It is also mandatory that at least one move must be assigned to the project in order
     * for the approval to go through.
     *
     * @param projectId
     */
    void requestGroupMove(String projectId);

    /**
     * Add an action to an individual move. Inserts a new activity_log record with the data for the
     * Action.
     *
     * INSERT INTO activity_log (mo_id,description,tr_id,assigned_to,status,activity_type,
     * date_created, prob_type,autocreate_wr) VALUES
     * (MO_ID,DESCRIPTION,TR_ID,ASSIGNED_TO,'Requested',ACTIVITY_TYPE, CURRENT DATE,
     * PROB_TYPE,AUTOCREATE_WR) Note: Building Code, Site Code
     *
     * @param record
     */
    void addActionIndividualMove(DataRecord record);

    /**
     * Add an action to a group move. Inserts a new activity_log record with the data for the
     * Action.
     *
     * INSERT INTO activity_log (project_id,description,tr_id,assigned_to,status,
     * activity_type,date_created,prob_type,autocreate_wr, requestor) VALUES
     * (PROJECT_ID,DESCRIPTION,TR_ID,ASSIGNED_TO,'Requested',ACTIVITY_TYPE, CURRENT DATE,
     * PROB_TYPE,AUTOCREATE_WR, REQUESTOR) Note: Building Code, Site Code
     *
     * @param record
     */
    void addActionGroupMove(DataRecord record);

    /**
     * Delete selected team moves.
     *
     * @param moIds
     */
    void deleteSelectedTeamMoves(String moIds);
}
