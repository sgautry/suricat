package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IMoveScenarioDao;
import com.archibus.app.move.common.domain.MoveScenario;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Move Scenario.
 *
 * @author Zhang Yi
 *
 */
public class MoveScenarioDataSource extends ObjectDataSourceImpl<MoveScenario> implements IMoveScenarioDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "scenario_id", "id" }, { "project_id", "projectId" },
			{ "description", "description" }, { "date_created", "dateCreated" } };

	/**
	 * Constructs MoveScenarioDataSource, mapped to <code>mo_scenario</code>
	 * table, using <code>MoveScenario</code> bean.
	 */
	public MoveScenarioDataSource() {
		super("moveScenario", "mo_scenario");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
