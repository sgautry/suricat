/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * This package contains DAOs for Move Management domain.
 *
 *
 * Used by Move Management services.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.1
 **/
package com.archibus.app.move.common.dao;
