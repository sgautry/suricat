package com.archibus.app.move.sql;

import java.util.List;

import org.json.JSONArray;

import com.archibus.app.common.organization.dao.datasource.EmployeeDataSource;
import com.archibus.app.move.common.dao.*;
import com.archibus.app.move.common.domain.*;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.datasource.SqlUtils;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides SQL operation methods on Move Orders.
 * <p>
 *
 * Used by Move Management Services.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file. *
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveOrdersSqlOperator {

	/**
	 * Dao for Move Order.
	 */
	private IMoveOrderDao moveOrderDao;

	/**
	 * Dao for Jacks.
	 */
	private IJackDao jacksDao;

	/**
	 * Getter for the moveOrderDao property.
	 *
	 * @return the moveOrderDao property.
	 */
	public IMoveOrderDao getMoveOrderDao() {
		return this.moveOrderDao;
	}

	/**
	 * Setter for the moveOrderDao property.
	 *
	 * @param moveOrderDao
	 *            the moveOrderDao to set.
	 */
	public void setMoveOrderDao(final IMoveOrderDao moveOrderDao) {
		this.moveOrderDao = moveOrderDao;
	}

	/**
	 * Getter for the jacksDao property.
	 *
	 * @return the jacksDao property.
	 */
	public IJackDao getJacksDao() {
		return this.jacksDao;
	}

	/**
	 * Setter for the jacksDao property.
	 *
	 * @param jacksDao
	 *            the jacksDao to set.
	 */
	public void setJacksDao(final IJackDao jacksDao) {
		this.jacksDao = jacksDao;
	}

	// @translatable
	protected final static String errorMsgAddBulkMoves = "The department and locations entered do not match any employees";

	/**
	 * SQL quote for varchars.
	 */
	public static final String SQL_QUOTE = "'";

	public void insertForBulkMoves(final DataRecord record) {

		final String strWhereClause = this.getWhereClauses(record);

		final DataRecord emRec = new EmployeeDataSource().getRecord(strWhereClause);
		if (emRec == null || !emRec.valueExists("em.em_id")) {
			final ExceptionBase exception = new ExceptionBase();
			exception.setPattern(errorMsgAddBulkMoves);
			exception.setTranslatable(true);
			throw exception;
		}

		// remember the existent moves for the project, in order to NOT add eq &
		// ta to them
		final String existingMoveIds = this.getExistingMoveIds(record.getString("mo.project_id"));

		this.insertMoveOrdersForBulkMoves(strWhereClause, record);

		this.updateJacksForBulkMoves(record.getString("mo.project_id"), strWhereClause);

		final String moJoinClauses = this.getMoJoinClauses(record.getString("mo.project_id"), strWhereClause,
				existingMoveIds);

		this.insertMoveEquipmentsForBulkMoves(moJoinClauses);
		this.insertMoveFurnituresForBulkMoves(moJoinClauses);

	}

	private String getWhereClauses(final DataRecord inputRec) {

		final StringBuilder strWhereClause = new StringBuilder();

		if (StringUtil.notNullOrEmpty(inputRec.getString("mo.from_dv_id"))) {
			strWhereClause.append(" em.dv_id=");
			strWhereClause.append(MoveCommonUtil.getLiteralString(inputRec.getString("mo.from_dv_id")));
		}

		if (StringUtil.notNullOrEmpty(inputRec.getString("mo.from_dp_id"))) {
			strWhereClause.append(strWhereClause.length() > 0 ? " AND " : "");
			strWhereClause.append(" em.dp_id=");
			strWhereClause.append(MoveCommonUtil.getLiteralString(inputRec.getString("mo.from_dp_id")));
		}

		if (StringUtil.notNullOrEmpty(inputRec.getString("mo.from_bl_id"))) {
			strWhereClause.append(strWhereClause.length() > 0 ? " AND " : "");
			strWhereClause.append(" em.bl_id=");
			strWhereClause.append(MoveCommonUtil.getLiteralString(inputRec.getString("mo.from_bl_id")));
		}

		if (StringUtil.notNullOrEmpty(inputRec.getString("mo.from_fl_id"))) {
			strWhereClause.append(strWhereClause.length() > 0 ? " AND " : "");
			strWhereClause.append(" em.fl_id=");
			strWhereClause.append(MoveCommonUtil.getLiteralString(inputRec.getString("mo.from_fl_id")));
		}

		return strWhereClause.toString();
	}

	private String getExistingMoveIds(final String projectId) {

		String movesIdList = "";

		final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
		restrictionDef.addClause("mo", "project_id", projectId, Operation.EQUALS);
		restrictionDef.addClause("mo", "mo_type", "Employee", Operation.EQUALS);

		final List<MoveOrder> moveList = this.moveOrderDao.find(restrictionDef);
		for (final MoveOrder mo : moveList) {

			movesIdList += ((StringUtil.isNullOrEmpty(movesIdList) ? "" : ",") + mo.getId());

		}

		return movesIdList;
	}

	private String getMoJoinClauses(final String projectId, final String strWhereClause, final String existingMoveIds) {

		final StringBuilder sqlMoJoin = new StringBuilder();

		sqlMoJoin.append(" JOIN (");
		sqlMoJoin.append(" 		SELECT mo.mo_id, mo.from_bl_id, mo.from_fl_id, mo.from_rm_id, mo.em_id ");
		sqlMoJoin.append(" 		FROM mo, em ");
		sqlMoJoin.append(" 		WHERE em.em_id = mo.em_id ");
		sqlMoJoin.append(" 		AND  mo.project_id=" + MoveCommonUtil.getLiteralString(projectId));
		sqlMoJoin.append(
				StringUtil.isNullOrEmpty(existingMoveIds) ? "" : "	AND mo.mo_id NOT IN ( " + existingMoveIds + ")");
		sqlMoJoin.append("		AND " + strWhereClause);
		sqlMoJoin.append("	) ${sql.as} mo_temp ");

		return sqlMoJoin.toString();
	}

	private void insertMoveOrdersForBulkMoves(final String strWhereClause, final DataRecord record) {

		final String projectId = record.getString("mo.project_id");
		final String date_start_req = SqlUtils.formatValueForSql(record.getDate("mo.date_start_req"));
		final String qDefValue = MoveCommonUtil.getDefaultQuestionnaireValues("Move Order - Employee");

		final StringBuilder sqlInsertMo = new StringBuilder();
		sqlInsertMo.append("INSERT INTO mo ");
		sqlInsertMo.append("(project_id, status,requestor,phone,dept_contact,phone_dept_contact,"
				+ "dv_id,dp_id,ac_id,to_bl_id,to_fl_id,mo_type,date_start_req,date_to_perform,em_id,"
				+ "from_bl_id,from_fl_id,from_rm_id,from_dv_id,from_dp_id,from_phone,from_fax,from_mailstop,"
				+ "to_dv_id,to_dp_id,to_phone,to_fax,mo_quest) ");

		sqlInsertMo.append("SELECT project.project_id, 'Created',");
		sqlInsertMo.append(MoveCommonUtil.getLiteralString(record.getString("mo.requestor")));
		sqlInsertMo.append(
				",project.phone_req, project.dept_contact, project.phone_dept_contact,project.dv_id, project.dp_id, project.ac_id,");
		sqlInsertMo.append(MoveCommonUtil.getLiteralString(record.getString("mo.to_bl_id")));
		sqlInsertMo.append(",");
		sqlInsertMo.append(MoveCommonUtil.getLiteralString(record.getString("mo.to_fl_id")));
		sqlInsertMo.append(" ,'Employee',");
		sqlInsertMo.append(date_start_req);
		sqlInsertMo.append(",");
		sqlInsertMo.append(date_start_req);
		sqlInsertMo.append(
				",em.em_id, em.bl_id, em.fl_id, em.rm_id, em.dv_id, em.dp_id, em.phone, em.fax, em.mailstop,em.dv_id, em.dp_id, em.phone, em.fax,");
		sqlInsertMo.append(MoveCommonUtil.getLiteralString(qDefValue));
		sqlInsertMo.append(" FROM em, project ");
		sqlInsertMo.append(" WHERE project.project_id = " + MoveCommonUtil.getLiteralString(projectId));
		sqlInsertMo.append("  AND em.em_id NOT IN ");
		sqlInsertMo.append("                     (SELECT em_id FROM mo WHERE project_id="
				+ MoveCommonUtil.getLiteralString(projectId));
		sqlInsertMo.append("                     AND mo_type='Employee')");
		sqlInsertMo.append("  AND " + strWhereClause);

		SqlUtils.executeUpdate("mo", sqlInsertMo.toString());
	}

	private void updateJacksForBulkMoves(final String projectId, final String whereClause) {

		// SELECT em_id,mo_id from mo WHERE em_id IN (select em_id from mo where
		// mo.project_id=xxx
		// and mo.mo_type='Employee'
		// AND em_id IN (SELECT em_id from em + strWhereClause))
		final StringBuilder moRestriction = new StringBuilder();
		moRestriction.append(" em_id IN (");
		moRestriction.append(" 			SELECT em_id FROM mo ");
		moRestriction
				.append(" 						 WHERE mo.project_id=" + MoveCommonUtil.getLiteralString(projectId));
		moRestriction.append(" 						 AND mo.mo_type='Employee' ");
		moRestriction.append(" 						 AND em_id IN ( ");
		moRestriction.append(" 										SELECT em_id FROM em  ");
		moRestriction.append(" 													 WHERE " + whereClause);
		moRestriction.append(" 									) ");
		moRestriction.append(" 			) ");

		final List<MoveOrder> moveOrders = this.moveOrderDao.getMoveOrdersByRestriction(moRestriction.toString());

		for (final MoveOrder mo : moveOrders) {

			final Jacks jacks = this.jacksDao.getEmployeeJacks(mo.getEmployeeId());

			if (jacks != null) {
				if (StringUtil.notNullOrEmpty(jacks.getDescription())) {
					mo.setDescription(mo.getDescription() + "\n\n" + jacks.getDescription());
				}

				mo.setFromJackData(jacks.getFromData());
				mo.setFromJackVoice(jacks.getFromVoice());
			}

			this.moveOrderDao.update(mo);
		}
	}

	private void insertMoveEquipmentsForBulkMoves(final String moJoinClauses) {

		final StringBuilder sqlInsertMoEq = new StringBuilder();

		sqlInsertMoEq.append("INSERT INTO mo_eq (eq_id,eq_std,mo_id,from_bl_id,from_fl_id,from_rm_id,cost_moving)");
		sqlInsertMoEq.append(" SELECT eq_id,eq.eq_std,mo_temp.mo_id,");
		sqlInsertMoEq.append(" 		  (CASE WHEN eq.bl_id IS NULL THEN mo_temp.from_bl_id ELSE eq.bl_id END), ");
		sqlInsertMoEq.append("		  (CASE WHEN eq.fl_id IS NULL THEN mo_temp.from_fl_id ELSE eq.fl_id END), ");
		sqlInsertMoEq.append("		  (CASE WHEN eq.rm_id IS NULL THEN mo_temp.from_rm_id ELSE eq.rm_id END), ");
		sqlInsertMoEq.append("		  ${sql.isNull('eqstd.cost_moving',0)} ");
		sqlInsertMoEq.append(" FROM eq ");
		sqlInsertMoEq.append(moJoinClauses);
		sqlInsertMoEq.append(" ON mo_temp.em_id = eq.em_id ");
		sqlInsertMoEq.append(" LEFT OUTER JOIN eqstd ON eqstd.eq_std = eq.eq_std");

		SqlUtils.executeUpdate("mo_eq", sqlInsertMoEq.toString());
	}

	private void insertMoveFurnituresForBulkMoves(final String moJoinClauses) {

		final StringBuilder sqlInsertMoTa = new StringBuilder();

		sqlInsertMoTa.append("INSERT INTO mo_ta (ta_id,fn_std,mo_id,from_bl_id,from_fl_id,from_rm_id,cost_moving)");
		sqlInsertMoTa.append(" SELECT ta_id,ta.fn_std,mo_temp.mo_id,");
		sqlInsertMoTa.append(" 		  (CASE WHEN ta.bl_id IS NULL THEN mo_temp.from_bl_id ELSE ta.bl_id END), ");
		sqlInsertMoTa.append("		  (CASE WHEN ta.fl_id IS NULL THEN mo_temp.from_fl_id ELSE ta.fl_id END), ");
		sqlInsertMoTa.append("		  (CASE WHEN ta.rm_id IS NULL THEN mo_temp.from_rm_id ELSE ta.rm_id END), ");
		sqlInsertMoTa.append("		  ${sql.isNull('fnstd.cost_moving',0)} ");
		sqlInsertMoTa.append(" FROM ta ");
		sqlInsertMoTa.append(moJoinClauses);
		sqlInsertMoTa.append(" ON mo_temp.em_id = ta.em_id ");
		sqlInsertMoTa.append(" LEFT OUTER JOIN fnstd ON fnstd.fn_std = ta.fn_std");

		SqlUtils.executeUpdate("mo_ta", sqlInsertMoTa.toString());
	}

	public void updateMoveOrdersForCloseGroupMove(final String projectId) {

		final String literalId = MoveCommonUtil.getLiteralString(projectId);

		final String sqlUpdateMo = "UPDATE mo set status='Closed', date_closed=${sql.currentDate} WHERE project_id="
				+ literalId;
		SqlUtils.executeUpdate("mo", sqlUpdateMo);

		final StringBuilder sqlUpdateMoCosts = new StringBuilder();
		sqlUpdateMoCosts.append(" UPDATE mo SET cost_actual = ");
		sqlUpdateMoCosts.append(
				"              (SELECT project.cost_paid FROM project WHERE mo.project_id = project.project_id) ");
		sqlUpdateMoCosts.append("				/");
		sqlUpdateMoCosts
				.append("			   (select count(*) FROM mo mo_int WHERE mo.project_id= mo_int.project_id) ");
		sqlUpdateMoCosts.append(" WHERE (select count(*) FROM mo mo_int WHERE mo.project_id= mo_int.project_id) > 0 ");
		sqlUpdateMoCosts.append("		AND mo.project_id=" + literalId);
		SqlUtils.executeUpdate("mo", sqlUpdateMoCosts.toString());

	}

	public void updateForRouteGroupMoveForApproval(final String projectId) {

		MoveCommonUtil.getLiteralString(projectId);

		final String sqlUpdateMo = "UPDATE mo set status='Requested-Routed' WHERE project_id="
				+ MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate("mo", sqlUpdateMo);

	}

	public void updateForApproveGroupMove(final String projectId, final String statusField, final String dateField,
			final boolean isAllApproved) {

		final String sqlUpdateMo = "UPDATE mo set " + statusField + "='A'," + dateField
				+ "= ${sql.currentDate}  WHERE mo.project_id=" + MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate("mo", sqlUpdateMo);

		if (isAllApproved) {
			final String sqlUpdateMoStatus = "UPDATE mo SET status = 'Approved', date_approved =${sql.currentDate} where mo.project_id="
					+ MoveCommonUtil.getLiteralString(projectId);

			SqlUtils.executeUpdate("mo", sqlUpdateMoStatus);
		}
	}

	public void updateForIssueGroupMove(final String projectId) {

		final String literalId = MoveCommonUtil.getLiteralString(projectId);

		final String updateMoves = "UPDATE mo SET status='Issued-In Process', date_issued =${sql.currentDate} WHERE mo.status <> 'Approved-Cancelled' AND mo.project_id ="
				+ literalId;
		SqlUtils.executeUpdate("mo", updateMoves);

		final String updateMoveDate = " UPDATE mo set mo.date_to_perform=(SELECT project.date_commence_work from project "
				+ " WHERE project.project_id=" + literalId + ") WHERE mo.date_to_perform is null and "
				+ " mo.project_id IN (SELECT project_id from project "
				+ " where date_commence_work is not null and project_id=" + literalId + ")";

		SqlUtils.executeUpdate("mo", updateMoveDate);
	}

	public void updateForRejectGroupMove(final String projectId, final String statusField) {

		final String updateMoves = "UPDATE mo SET " + statusField
				+ "='R', status='Requested-Rejected' WHERE mo.project_id ="
				+ MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate("mo", updateMoves);
	}

	public void updateForRequestGroupMove(final String projectId) {

		final String updateMoves = "UPDATE mo SET status='Requested', date_requested=${sql.currentDate} WHERE mo.project_id ="
				+ MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate("mo", updateMoves);
	}

	/**
	 * Remove employees from team.
	 *
	 * @param emIds
	 *            selected employees id
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	public void removeEmployeesFromTeam(final List<String> emIds, final String teamId, final String projectId,
			final String type, final String dateStartReq, final String dateToPerform) {

		final int len = emIds.size();

		for (int i = 0; i < len; i++) {
			final StringBuilder sqlRemoveEmFromTeam = new StringBuilder();

			sqlRemoveEmFromTeam.append(
					"INSERT INTO mo (project_id, em_id, from_team_id, mo_type, date_start_req, date_to_perform)");
			sqlRemoveEmFromTeam.append(" VALUES('");
			sqlRemoveEmFromTeam.append(projectId);
			sqlRemoveEmFromTeam.append("','");
			sqlRemoveEmFromTeam.append(emIds.get(i));
			sqlRemoveEmFromTeam.append("','");
			sqlRemoveEmFromTeam.append(teamId);
			sqlRemoveEmFromTeam.append("','");
			sqlRemoveEmFromTeam.append(type);
			sqlRemoveEmFromTeam.append("',");
			sqlRemoveEmFromTeam.append(getFormatDate(dateStartReq));
			sqlRemoveEmFromTeam.append(",");
			sqlRemoveEmFromTeam.append(getFormatDate(dateToPerform));
			sqlRemoveEmFromTeam.append(")");

			SqlUtils.executeUpdate("mo", sqlRemoveEmFromTeam.toString());
		}

	}

	/**
	 * Remove rooms from team.
	 *
	 * @param rooms
	 *            selected rooms id
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	public void removeRoomsFromTeam(final JSONArray rooms, final String teamId, final String projectId,
			final String type, final String dateStartReq, final String dateToPerform) {

		final int len = rooms.length();

		for (int i = 0; i < len; i++) {
			final StringBuilder sqlRemoveRmFromTeam = new StringBuilder();
			final String blId = rooms.getJSONObject(i).getString("bl_id");
			final String flId = rooms.getJSONObject(i).getString("fl_id");
			final String rmId = rooms.getJSONObject(i).getString("rm_id");

			sqlRemoveRmFromTeam.append(
					"INSERT INTO mo (project_id, from_bl_id, from_fl_id, from_rm_id, to_team_id, mo_type, date_start_req, date_to_perform)");
			sqlRemoveRmFromTeam.append(" VALUES('");
			sqlRemoveRmFromTeam.append(projectId);
			sqlRemoveRmFromTeam.append("','");
			sqlRemoveRmFromTeam.append(blId);
			sqlRemoveRmFromTeam.append("','");
			sqlRemoveRmFromTeam.append(flId);
			sqlRemoveRmFromTeam.append("','");
			sqlRemoveRmFromTeam.append(rmId);
			sqlRemoveRmFromTeam.append("','");
			sqlRemoveRmFromTeam.append(teamId);
			sqlRemoveRmFromTeam.append("','");
			sqlRemoveRmFromTeam.append(type);
			sqlRemoveRmFromTeam.append("',");
			sqlRemoveRmFromTeam.append(getFormatDate(dateStartReq));
			sqlRemoveRmFromTeam.append(",");
			sqlRemoveRmFromTeam.append(getFormatDate(dateToPerform));
			sqlRemoveRmFromTeam.append(")");

			SqlUtils.executeUpdate("mo", sqlRemoveRmFromTeam.toString());
		}

	}

	/**
	 * Format date.
	 *
	 * @param date
	 *            date to format
	 * @return formatDate formated date
	 */
	public String getFormatDate(final String date) {
		final String formatDate = DateTime.stringToDate(date, "yyyy-MM-dd").toString();
		if (SqlUtils.isOracle()) {
			return "TO_DATE('" + formatDate + "', 'YYYY-MM-DD')";
		} else {
			return SQL_QUOTE + formatDate + SQL_QUOTE;
		}
	}

}
