package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Move Order Tagged Furniture.
 * <p>
 * Mapped to mo_ta table.
 *
 * @author Zhang Yi
 *
 */
public class MoveOrderTaggedFurniture {

	/**
	 * ID of the Move Order.
	 */
	private int moveOrderId;

	/**
	 * Move Tagged Furniture ID.
	 */
	private String taggedFurnitureId;

	/**
	 * Furniture Standard.
	 */
	private String furnitureStandard;

	/**
	 * Move furniture Cost.
	 */
	private double costMoving;

	/**
	 * Getter for the moveOrderId property.
	 *
	 * @return the moveOrderId property.
	 */
	public int getMoveOrderId() {
		return this.moveOrderId;
	}

	/**
	 * Setter for the moveOrderId property.
	 *
	 * @param moveOrderId
	 *            the moveOrderId to set.
	 */
	public void setMoveOrderId(final int moveOrderId) {
		this.moveOrderId = moveOrderId;
	}

	/**
	 * Getter for the taggedFurnitureId property.
	 *
	 * @return the taggedFurnitureId property.
	 */
	public String getTaggedFurnitureId() {
		return this.taggedFurnitureId;
	}

	/**
	 * Setter for the taggedFurnitureId property.
	 *
	 * @param taggedFurnitureId
	 *            the taggedFurnitureId to set.
	 */
	public void setTaggedFurnitureId(final String taggedFurnitureId) {
		this.taggedFurnitureId = taggedFurnitureId;
	}

	/**
	 * Getter for the furnitureStandard property.
	 *
	 * @return the furnitureStandard property.
	 */
	public String getFurnitureStandard() {
		return this.furnitureStandard;
	}

	/**
	 * Setter for the furnitureStandard property.
	 *
	 * @param furnitureStandard
	 *            the furnitureStandard to set.
	 */
	public void setFurnitureStandard(final String furnitureStandard) {
		this.furnitureStandard = furnitureStandard;
	}

	/**
	 * Getter for the costMoving property.
	 *
	 * @return the costMoving property.
	 */
	public double getCostMoving() {
		return this.costMoving;
	}

	/**
	 * Setter for the costMoving property.
	 *
	 * @param costMoving
	 *            the costMoving to set.
	 */
	public void setCostMoving(final double costMoving) {
		this.costMoving = costMoving;
	}

	/**
	 * Getter for the fromBuildingId property.
	 *
	 * @return the fromBuildingId property.
	 */
	public String getFromBuildingId() {
		return this.fromBuildingId;
	}

	/**
	 * Setter for the fromBuildingId property.
	 *
	 * @param fromBuildingId
	 *            the fromBuildingId to set.
	 */
	public void setFromBuildingId(final String fromBuildingId) {
		this.fromBuildingId = fromBuildingId;
	}

	/**
	 * Getter for the fromFloorId property.
	 *
	 * @return the fromFloorId property.
	 */
	public String getFromFloorId() {
		return this.fromFloorId;
	}

	/**
	 * Setter for the fromFloorId property.
	 *
	 * @param fromFloorId
	 *            the fromFloorId to set.
	 */
	public void setFromFloorId(final String fromFloorId) {
		this.fromFloorId = fromFloorId;
	}

	/**
	 * Getter for the fromRoomId property.
	 *
	 * @return the fromRoomId property.
	 */
	public String getFromRoomId() {
		return this.fromRoomId;
	}

	/**
	 * Setter for the fromRoomId property.
	 *
	 * @param fromRoomId
	 *            the fromRoomId to set.
	 */
	public void setFromRoomId(final String fromRoomId) {
		this.fromRoomId = fromRoomId;
	}

	/**
	 *
	 * Move Equipment from Building.
	 */
	private String fromBuildingId;

	/**
	 * Move Equipment from Floor.
	 */
	private String fromFloorId;

	/**
	 * Move Equipment from Room.
	 */
	private String fromRoomId;

}
