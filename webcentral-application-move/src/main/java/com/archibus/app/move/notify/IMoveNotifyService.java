package com.archibus.app.move.notify;

import com.archibus.app.common.bldgops.domain.ActionItem;
import com.archibus.app.move.common.domain.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides notification and email sending behaviors for Move
 * Management Request logic.
 * <p>
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public interface IMoveNotifyService {

	/**
	 * Send mail when close a group move.
	 *
	 * @param project
	 */
	void sendMailForCloseGroupMove(Project project);

	/**
	 * Send mail when close an individual move.
	 *
	 * @param moveOrder
	 */
	void sendMailForCloseIndividualMove(MoveOrder moveOrder);

	/**
	 * Send mail when route an individual move.
	 *
	 * @param mo
	 */
	void sendMailForRouteInividualMove(MoveOrder mo);

	/**
	 * Send mail when route a group move.
	 *
	 * @param project
	 */
	void sendMailForRouteGroupMove(Project project);

	/**
	 * Send mail when approve a individual move.
	 *
	 * @param mo
	 */
	void sendMailForApproveIndividualMove(MoveOrder mo);

	/**
	 * Send mail when approve a group move.
	 *
	 * @param project
	 */
	void sendMailForApproveGroupMove(Project project);

	/**
	 * Send mail when auto-approve an individual move.
	 *
	 * @param mo
	 */
	void sendMailForAutoApproveIndividualMove(MoveOrder mo);

	/**
	 * Send mail when auto-approve a group move.
	 *
	 * @param project
	 */
	void sendMailForAutoApproveGroupMove(Project project);

	/**
	 * Send mail when issue an individual move.
	 *
	 * @param mo
	 */
	void sendMailForIssueIndividualMove(MoveOrder mo);

	/**
	 * Send mail when issue a service request.
	 *
	 * @param item
	 */
	void sendMailForIssueRequest(ActionItem item);

	/**
	 * Send mail when issue a group move.
	 *
	 * @param project
	 */
	void sendMailForIssueGroupMove(Project project);

	/**
	 * Send mail when reject an individual move.
	 *
	 * @param mo
	 */
	void sendMailForRejectIndividualMove(MoveOrder mo);

	/**
	 * Send mail when reject a group move.
	 *
	 * @param project
	 */
	void sendMailForRejectGroupMove(Project project);

	/**
	 * Send mail when request an individual move.
	 *
	 * @param mo
	 */
	void sendMailForRequestIndividualMove(MoveOrder mo);

	/**
	 * Send mail when request a group move.
	 *
	 * @param project
	 */
	void sendMailForRequestGroupMove(Project project);

}
