package com.archibus.app.move.report;

import java.io.File;
import java.util.*;

import org.apache.log4j.Logger;

import com.archibus.app.move.common.dao.IMoveOrderDao;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.context.ContextStore;
import com.archibus.datasource.DataStatistics;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.datasource.restriction.Restrictions.Restriction.Clause;
import com.archibus.ext.report.*;
import com.archibus.jobmanager.*;
import com.archibus.jobmanager.JobStatus.JobResult;
import com.archibus.utility.*;

/**
 * Job that generates Paginate report for Move Management application.
 *
 * @author ASC-BJ:Zhang Yi
 *
 */
public class MovePaginatedReportGeneratorJob extends JobBase {
	/*
	 * type of reports that will be generated valid valued 'group', 'single',
	 * 'scenario'
	 */
	private String reportType;

	/*
	 * selected move project
	 */
	private String projectId;

	/*
	 * selected move id , used for single move
	 */
	private String moveId;

	/*
	 * selected scenario id, used with projectId for group move
	 */
	private String scenarioId;

	private String viewName;

	private class MoveType {
		String file;
		String ds;
		String type;
	}

	// map with data for each type of report: file name and main datasource id
	private final List<MoveType> moveTypes = new ArrayList<MoveType>() {
		{
			add(new MoveType() {
				{
					this.file = "ab-mo-move-project-rpt.axvw";
					this.ds = "ds_abMoveProject_project";
					this.type = "project";
				}
			});
			add(new MoveType() {
				{
					this.file = "ab-mo-move-em-rpt.axvw";
					this.ds = "ds_abMoveEm_em";
					this.type = "Employee";
				}
			});
			add(new MoveType() {
				{
					this.file = "ab-mo-move-new-hire-rpt.axvw";
					this.ds = "ds_abMoveHire_em";
					this.type = "New Hire";
				}
			});
			add(new MoveType() {
				{
					this.file = "ab-mo-move-em-leaving-rpt.axvw";
					this.ds = "ds_abMoveLeaving_em";
					this.type = "Leaving";
				}
			});
			add(new MoveType() {
				{
					this.file = "ab-mo-move-asset-rpt.axvw";
					this.ds = "ds_abMoveAsset_asset";
					this.type = "Asset";
				}
			});
			add(new MoveType() {
				{
					this.file = "ab-mo-move-rm-rpt.axvw";
					this.ds = "ds_abMoveRoom_rm";
					this.type = "Room";
				}
			});
			add(new MoveType() {
				{
					this.file = "ab-mo-move-action-rpt.axvw";
					this.ds = "ds_abMoveAction_action";
					this.type = "action";
				}
			});

		}

	};

	// @translatable
	private final String DOC_TITLE_GP = "Group Move";

	// @translatable
	private final String JOB_TITLE_GP = "Group Moves";

	// @translatable
	private final String DOC_TITLE_SINGLE = "Individual Move";

	// @translatable
	private final String JOB_TITLE_SINGLE = "Individual Move";

	// @translatable
	private final String DOC_TITLE_SCENARIO = "Move Scenario";

	// @translatable
	private final String JOB_TITLE_SCENARIO = "Move Scenario";

	private final PaginatedReportsBuilder builder = new PaginatedReportsBuilder();

	private String jobTitle;

	private String fileName;

	/**
	 * Operation Message.
	 */
	// @non-translatable
	private static final String OPERATION = "Generate '%s' Paginate Report of Move Management for Move Project[%s]: %s.";

	/**
	 * Logger for this class and subclasses.
	 */
	protected final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * Set parameters passed from client side.
	 *
	 * @param rptType
	 *            report type
	 * @param projectId
	 *            project id
	 * @param selectedId
	 *            selected move order or move scenario id
	 */
	public void setParameters(final String reportType, final String projectId, final String selectedId) {

		this.reportType = reportType;
		this.projectId = projectId;

		if ("single".equalsIgnoreCase(this.reportType)) {

			this.moveId = selectedId;

		} else if ("scenario".equalsIgnoreCase(this.reportType)) {

			this.scenarioId = selectedId;
		}

	}

	@Override
	public void run() {

		if (this.logger.isInfoEnabled()) {

			final String message = String.format(OPERATION, this.reportType, this.projectId, "Started");
			this.logger.info(message);
		}

		final List<String> files = new ArrayList<String>();

		final boolean isGroupMove = "group".equalsIgnoreCase(this.reportType)
				&& StringUtil.notNullOrEmpty(this.projectId);
		final boolean isIndividualMove = "single".equalsIgnoreCase(this.reportType)
				&& StringUtil.notNullOrEmpty(this.moveId);
		final boolean isScenario = "scenario".equalsIgnoreCase(this.reportType)
				&& StringUtil.notNullOrEmpty(this.scenarioId);

		if (isGroupMove) {

			this.generateIndividualMoveReport(files);

		} else if (isIndividualMove) {

			this.generateGroupMoveReport(files);

		} else if (isScenario && StringUtil.notNullOrEmpty(this.projectId)) {

			this.generateScenarioReport(files);

		}

		if (files.size() > 0) {

			this.mergeFiles(files);
		}

		this.status.setCode(JobStatus.JOB_COMPLETE);
	}

	private void generateIndividualMoveReport(final List<String> files) {

		int counter = 1;
		this.jobTitle = this.JOB_TITLE_SINGLE;
		this.fileName = "move-management-individual-move-";
		for (final MoveType moveType : this.moveTypes) {

			final String currentMoType = getIndividualMoveType(this.moveId);

			final String strRestriction = "action".equalsIgnoreCase(moveType.type) ? " activity_log.mo_id = '"
					+ MoveCommonUtil.getLiteralString(this.moveId) + "' " + "AND activity_log.project_id IS NULL " :

					" mo.mo_id = '" + MoveCommonUtil.getLiteralString(this.moveId) + "' " + "AND mo.mo_type = '"
							+ MoveCommonUtil.getLiteralString(moveType.type) + "'";

			final boolean generate = this.isMoveDataExists(moveType.type, this.projectId, this.moveId)
					&& (currentMoType.equals(moveType.type) || "action".equalsIgnoreCase(moveType.type));
			if (generate) {
				this.viewName = moveType.file;
				final Map<String, Object> moRestriction = new HashMap<String, Object>();

				moRestriction.put(moveType.ds, strRestriction);

				final com.archibus.ext.report.docx.Report report = new com.archibus.ext.report.docx.Report();
				report.setTitle(this.DOC_TITLE_SINGLE);

				report.setFilename(
						this.fileName + moveType.type + "-" + (counter++) + "-" + DateTime.DATE_FORMAT_NOW + ".docx");
				report.setRestrictions(moRestriction);

				this.builder.buildDocxFromView(ContextStore.get(), report, this.viewName, null);

				files.add(report.getFileFullName());
			}
		}

	}

	private void generateGroupMoveReport(final List<String> files) {

		int counter = 1;
		this.jobTitle = this.JOB_TITLE_GP + " : " + this.projectId;
		this.fileName = "move-management-group-moves-";
		for (final MoveType moveType : this.moveTypes) {

			final String currentMoType = getIndividualMoveType(this.moveId);

			final String strRestriction = "project".equalsIgnoreCase(moveType.type) ? " project.project_id = '"
					+ MoveCommonUtil.getLiteralString(this.projectId) + "' " + "AND project.project_type = 'Move' " :

					" mo.project_id = '" + MoveCommonUtil.getLiteralString(this.projectId) + "' " + "AND mo.mo_type = '"
							+ MoveCommonUtil.getLiteralString(moveType.type) + "'";

			final boolean generate = this.isMoveDataExists(moveType.type, this.projectId, this.moveId)
					&& (currentMoType.equals(moveType.type) || "action".equalsIgnoreCase(moveType.type));

			if (generate) {
				this.viewName = moveType.file;
				final Map<String, Object> moRestriction = new HashMap<String, Object>();

				moRestriction.put(moveType.ds, strRestriction);

				final com.archibus.ext.report.docx.Report report = new com.archibus.ext.report.docx.Report();
				report.setTitle(this.DOC_TITLE_GP + " : " + this.projectId);

				report.setFilename(
						this.fileName + moveType.type + "-" + (counter++) + "-" + DateTime.DATE_FORMAT_NOW + ".docx");
				report.setRestrictions(moRestriction);

				this.builder.buildDocxFromView(ContextStore.get(), report, this.viewName, null);

				files.add(report.getFileFullName());
			}
		}
	}

	private void generateScenarioReport(final List<String> files) {

		this.viewName = "ab-mo-move-scenario-rpt.axvw";
		this.jobTitle = this.JOB_TITLE_SCENARIO;
		this.fileName = "move - management - move - scenario -";

		final String strRestriction = " AND mo_scenario_em.project_id = '"
				+ MoveCommonUtil.getLiteralString(this.projectId) + "' " + "AND mo_scenario_em.scenario_id = '"
				+ MoveCommonUtil.getLiteralString(this.scenarioId) + "'";

		final Map<String, Object> moRestriction = new HashMap<String, Object>();
		moRestriction.put("drawPanelRest", strRestriction);

		final com.archibus.ext.report.docx.Report report = new com.archibus.ext.report.docx.Report();
		report.setTitle(this.DOC_TITLE_SCENARIO + " : " + this.scenarioId + " ; " + this.projectId);

		report.setFilename(this.fileName + "1-" + DateTime.DATE_FORMAT_NOW + ".docx");
		report.setPatameters(moRestriction);

		this.builder.buildDocxFromView(ContextStore.get(), report, this.viewName, null);

		files.add(report.getFileFullName());
	}

	private void mergeFiles(final List<String> files) {
		// merging files into one file
		final String mainFileName = this.fileName + DateTime.DATE_FORMAT_NOW + ".docx";

		final String finalFileFullname = ReportUtility.getReportFilesStorePath(ContextStore.get()) + mainFileName;

		ReportUtility.appendDocxFiles(files, finalFileFullname);

		this.status.setResult(new JobResult(this.jobTitle, mainFileName, ContextStore.get().getContextPath()
				+ ReportUtility.getPerUserReportFilesPath(ContextStore.get()) + mainFileName));

		// delete partial results to save space on hd
		for (final String fullPath : files) {
			final File crtFile = new File(fullPath);
			if (crtFile.exists()) {
				crtFile.delete();
			}
		}

	}

	/*
	 * @return if exist data for current report/partial report
	 */
	private boolean isMoveDataExists(final String moveTypeKey, final String projectId, final String moId) {

		if (!"project".equalsIgnoreCase(moveTypeKey) && !"action".equalsIgnoreCase(moveTypeKey)) {

			final Clause idClause = StringUtil.notNullOrEmpty(projectId)
					? Restrictions.eq("mo", "project_id", projectId)
					: (StringUtil.notNullOrEmpty(moId) ? Restrictions.eq("mo", "mo_id", moId) : null);

			final int count = DataStatistics.getInt("mo", "mo_id", "count",
					Restrictions.and(idClause, Restrictions.eq("mo", "mo_type", moveTypeKey)));

			return (count > 0);

		} else if ("action".equalsIgnoreCase(moveTypeKey) && StringUtil.notNullOrEmpty(moId)) {

			return DataStatistics.getInt("activity_log", "activity_log_id", "count", "mo_id=" + moId) > 0;
		}

		return true;
	}

	/*
	 * return type of individual move
	 */
	private String getIndividualMoveType(final String moveOrderId) {
		return ((IMoveOrderDao) ContextStore.get().getBean("moveOrderDao")).get(moveOrderId).getMoveType();
	}
}