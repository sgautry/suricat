package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Jack.
 * <p>
 * Mapped to jk table.
 *
 * @author Zhang Yi
 *
 */
public class Jack {

	/**
	 * ID of the jack.
	 */
	private String id;

	/**
	 * Employee ID.
	 */
	private String employeeId;

	/**
	 * Jack Telecomm Service.
	 */
	private String tcService;

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter for the employeeId property.
	 *
	 * @return the employeeId property.
	 */
	public String getEmployeeId() {
		return this.employeeId;
	}

	/**
	 * Setter for the employeeId property.
	 *
	 * @param employeeId
	 *            the employeeId to set.
	 */
	public void setEmployeeId(final String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Getter for the tcService property.
	 *
	 * @return the tcService property.
	 */
	public String getTcService() {
		return this.tcService;
	}

	/**
	 * Setter for the tcService property.
	 *
	 * @param tcService
	 *            the tcService to set.
	 */
	public void setTcService(final String tcService) {
		this.tcService = tcService;
	}

}
