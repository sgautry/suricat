package com.archibus.app.move.request;

import java.util.*;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.archibus.app.common.bldgops.dao.IActionItemDao;
import com.archibus.app.common.bldgops.domain.ActionItem;
import com.archibus.app.common.finanal.impl.DateUtils;
import com.archibus.app.common.organization.domain.Employee;
import com.archibus.app.move.common.dao.*;
import com.archibus.app.move.common.domain.*;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.app.move.notify.IMoveNotifyService;
import com.archibus.app.move.sql.*;
import com.archibus.app.move.transactions.IMoveSpaceTransactionService;
import com.archibus.context.ContextStore;
import com.archibus.core.dao.IDao;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveRequestService. Provides Move Request related business
 * operations.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr methods about move
 * request.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveRequestService implements IMoveRequestService {

    /**
     * DAO for Move Project.
     */
    private IProjectDao moveProjectDao;

    /**
     * DAO for Move Order.
     */
    private IMoveOrderDao moveOrderDao;

    /**
     * DAO for Action Item.
     */
    private IActionItemDao actionItemDao;

    /**
     * DAO for Jacks.
     */
    private IJackDao jacksDao;

    /**
     * DAO for Employee.
     */
    private IDao<Employee> employeeDao;

    /**
     * Operator for Move Orders by sql.
     */
    private MoveOrdersSqlOperator moveOrdersSqlOperator;

    /**
     * Sql Operator for Move related assets employees/equipments/tagged furnitures/jacks etc.
     */
    private MoveAssetsSqlOperator moveAssetSqlOperator;

    /**
     * Sql Operator for Move team related logics.
     */
    private MoveTeamSqlOperator moveTeamSqlOperator;

    /**
     * Getter for the moveTeamSqlOperator property.
     *
     * @return the moveTeamSqlOperator property.
     */
    public MoveTeamSqlOperator getMoveTeamSqlOperator() {
        return this.moveTeamSqlOperator;
    }

    /**
     * Setter for the moveTeamSqlOperator property.
     *
     * @param moveTeamSqlOperator the moveTeamSqlOperator to set.
     */
    public void setMoveTeamSqlOperator(final MoveTeamSqlOperator moveTeamSqlOperator) {
        this.moveTeamSqlOperator = moveTeamSqlOperator;
    }

    /**
     * Move Space Transaction service.
     */
    private IMoveSpaceTransactionService moveSpaceTransactionService;

    /**
     * Move Notification service.
     */
    private IMoveNotifyService moveNotifyService;

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    // @translatable
    protected final static String errorMsgActivityTypeInvalid =
            "The activity type [{0}] is invalid";

    @Override
    public void requestIndividualMove(final String moId) {

        MoveCommonUtil.commonLog("Changing status to requested", "mo_id=[" + moId + "]",
            this.logger);

        final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));

        mo.setStatus("Requested");
        mo.setDateRequested(Utility.currentDate());
        mo.setTimeRequested(Utility.currentTime());

        this.moveOrderDao.update(mo);

        this.moveNotifyService.sendMailForRequestIndividualMove(mo);
    }

    @Override
    public void requestGroupMove(final String projectId) {

        MoveCommonUtil.commonLog("requestGroupMove", "project_id=[" + projectId + "]", this.logger);

        final Project project = this.moveProjectDao.get(projectId);

        project.setStatus("Requested");
        project.setDateRequested(Calendar.getInstance().getTime());

        this.moveProjectDao.update(project);

        this.moveOrdersSqlOperator.updateForRequestGroupMove(projectId);

        this.moveNotifyService.sendMailForRequestGroupMove(project);
    }

    @Override
    public void routeIndividualMoveForApproval(final String moId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        MoveCommonUtil.commonLog("routeIndividualMoveForApproval", "mo_id=[" + moId + "]",
            this.logger);

        final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));
        if (!"Requested-Routed".equalsIgnoreCase(mo.getStatus())) {

            mo.setStatus("Requested-Routed");

            // notNull(apprv_mgr1).length() == 0
            mo.setApproveManager1(SqlUtils.makeLiteralOrBlank(approveManager1));
            mo.setApproveManager2(SqlUtils.makeLiteralOrBlank(approveManager2));
            mo.setApproveManager3(SqlUtils.makeLiteralOrBlank(approveManager3));

            this.moveOrderDao.update(mo);

            this.moveNotifyService.sendMailForRouteInividualMove(mo);
        }
    }

    @Override
    public void routeGroupMoveForApproval(final String projectId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        MoveCommonUtil.commonLog("routeGroupMoveForApproval", "project_id=[" + projectId + "]",
            this.logger);

        final Project project = this.moveProjectDao.get(projectId);
        project.setStatus("Requested-Routed");
        this.moveProjectDao.update(project);

        this.moveOrdersSqlOperator.updateForRouteGroupMoveForApproval(projectId);
        // ??? Don't need to update project's approve manager?

        this.moveNotifyService.sendMailForRouteGroupMove(project);

    }

    @Override
    public void approveIndividualMove(final String moId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        MoveCommonUtil.commonLog("approveIndividualMove", "mo_id=[" + moId + "]", this.logger);

        final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));
        final String currentUserId = ContextStore.get().getUser().getEmployee().getId();

        if (currentUserId.equalsIgnoreCase(approveManager1)) {

            mo.setApproveManager1Status("A");
            mo.setApproveManager1Date(Calendar.getInstance().getTime());

        } else if (currentUserId.equalsIgnoreCase(approveManager2)) {

            mo.setApproveManager2Status("A");
            mo.setApproveManager2Date(Calendar.getInstance().getTime());

        } else if (currentUserId.equalsIgnoreCase(approveManager3)) {

            mo.setApproveManager3Status("A");
            mo.setApproveManager3Date(Calendar.getInstance().getTime());

        }

        if ((StringUtil.isNullOrEmpty(mo.getApproveManager1())
                || "A".equalsIgnoreCase(mo.getApproveManager1Status()))
                && (StringUtil.isNullOrEmpty(mo.getApproveManager2())
                        || "A".equalsIgnoreCase(mo.getApproveManager2Status()))
                && (StringUtil.isNullOrEmpty(mo.getApproveManager3())
                        || "A".equalsIgnoreCase(mo.getApproveManager3Status()))) {

            mo.setStatus("Approved");
            mo.setDateApproved(Calendar.getInstance().getTime());
        }

        this.moveOrderDao.update(mo);

        this.moveNotifyService.sendMailForApproveIndividualMove(mo);

    }

    @Override
    public void approveGroupMove(final String projectId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        MoveCommonUtil.commonLog("approveGroupMove", "project_id=[" + projectId + "]", this.logger);

        final Project project = this.moveProjectDao.get(projectId);
        final String currentUserId = ContextStore.get().getUser().getEmployee().getId();

        String statusField = "";
        String dateField = "";
        if (currentUserId.equalsIgnoreCase(approveManager1)) {

            project.setApproveManager1Status("A");
            project.setApproveManager1Date(Calendar.getInstance().getTime());
            statusField = "apprv_mgr1_status";
            dateField = "date_app_mgr1";

        } else if (currentUserId.equalsIgnoreCase(approveManager2)) {

            project.setApproveManager2Status("A");
            project.setApproveManager2Date(Calendar.getInstance().getTime());
            statusField = "apprv_mgr2_status";
            dateField = "date_app_mgr2";

        } else if (currentUserId.equalsIgnoreCase(approveManager3)) {

            project.setApproveManager3Status("A");
            project.setApproveManager3Date(Calendar.getInstance().getTime());
            statusField = "apprv_mgr3_status";
            dateField = "date_app_mgr3";

        }

        boolean isAllApproved = false;
        if ((StringUtil.isNullOrEmpty(project.getApproveManager1())
                || "A".equalsIgnoreCase(project.getApproveManager1Status()))
                && (StringUtil.isNullOrEmpty(project.getApproveManager2())
                        || "A".equalsIgnoreCase(project.getApproveManager2Status()))
                && (StringUtil.isNullOrEmpty(project.getApproveManager3())
                        || "A".equalsIgnoreCase(project.getApproveManager3Status()))) {

            project.setStatus("Approved");
            project.setDateApproved(Calendar.getInstance().getTime());
            isAllApproved = true;
        }

        if (StringUtil.notNullOrEmpty(statusField) && StringUtil.notNullOrEmpty(dateField)) {

            this.moveProjectDao.update(project);

            this.moveOrdersSqlOperator.updateForApproveGroupMove(projectId, statusField, dateField,
                isAllApproved);

            this.moveNotifyService.sendMailForApproveGroupMove(project);
        }

    }

    @Override
    public void autoApproveIndividualMove(final String moId) {

        MoveCommonUtil.commonLog("autoApproveIndividualMove", "mo_id=[" + moId + "]", this.logger);

        final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));
        final String currentUserId = ContextStore.get().getUser().getEmployee().getId();

        mo.setApproveManager1(currentUserId);
        mo.setApproveManager1Status("A");
        mo.setApproveManager1Date(Calendar.getInstance().getTime());
        mo.setStatus("Approved");

        this.moveOrderDao.update(mo);

        this.moveNotifyService.sendMailForAutoApproveIndividualMove(mo);

    }

    @Override
    public void autoApproveGroupMove(final String projectId) {

        MoveCommonUtil.commonLog("autoApproveGroupMove", "project_id=[" + projectId + "]",
            this.logger);

        final Project project = this.moveProjectDao.get(projectId);

        project.setApproveManager1(ContextStore.get().getUser().getEmployee().getId());
        project.setApproveManager1Status("A");
        project.setApproveManager1Date(Calendar.getInstance().getTime());
        project.setStatus("Approved");
        project.setDateApproved(Calendar.getInstance().getTime());
        this.moveProjectDao.update(project);

        this.moveOrdersSqlOperator.updateForApproveGroupMove(projectId, "apprv_mgr1_status",
            "date_app_mgr1", true);

        this.moveNotifyService.sendMailForAutoApproveGroupMove(project);

    }

    @Override
    public void rejectIndividualMove(final String moId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        MoveCommonUtil.commonLog("rejectIndividualMove", "mo_id=[" + moId + "]", this.logger);

        final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));
        final String currentUserId = ContextStore.get().getUser().getEmployee().getId();

        if (currentUserId.equalsIgnoreCase(approveManager1)) {

            mo.setApproveManager1Status("R");
            mo.setStatus("Requested-Rejected");

        } else if (currentUserId.equalsIgnoreCase(approveManager2)) {

            mo.setApproveManager2Status("R");
            mo.setStatus("Requested-Rejected");

        } else if (currentUserId.equalsIgnoreCase(approveManager3)) {

            mo.setApproveManager3Status("R");
            mo.setStatus("Requested-Rejected");
        }

        this.moveOrderDao.update(mo);

        this.moveNotifyService.sendMailForRejectIndividualMove(mo);

        this.moveSpaceTransactionService.updateForRejectIndividualMove(mo);

    }

    @Override
    public void rejectGroupMove(final String projectId, final String approveManager1,
            final String approveManager2, final String approveManager3) {

        MoveCommonUtil.commonLog("rejectGroupMove", "project_id=[" + projectId + "]", this.logger);

        final Project project = this.moveProjectDao.get(projectId);
        final String currentUserId = ContextStore.get().getUser().getEmployee().getId();

        String statusField = "";
        if (currentUserId.equalsIgnoreCase(approveManager1)) {

            project.setApproveManager1Status("R");
            project.setStatus("Requested-Rejected");
            statusField = "apprv_mgr1_status";

        } else if (currentUserId.equalsIgnoreCase(approveManager2)) {

            project.setApproveManager2Status("R");
            project.setStatus("Requested-Rejected");
            statusField = "apprv_mgr2_status";

        } else if (currentUserId.equalsIgnoreCase(approveManager3)) {

            project.setApproveManager3Status("R");
            project.setStatus("Requested-Rejected");
            statusField = "apprv_mgr3_status";
        }

        if (StringUtil.notNullOrEmpty(statusField)) {

            this.moveProjectDao.update(project);

            this.moveOrdersSqlOperator.updateForRejectGroupMove(projectId, statusField);

            this.moveNotifyService.sendMailForRejectGroupMove(project);

            this.moveSpaceTransactionService.updateForRejectGroupMove(project);
        }
    }

    @Override
    public void issueIndividualMove(final String moId, final String requestor) {

        MoveCommonUtil.commonLog("issueIndividualMove", "mo_id=[" + moId + "]", this.logger);

        final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));
        mo.setStatus("Issued-In Process");
        mo.setDateIssued(Calendar.getInstance().getTime());

        this.moveOrderDao.update(mo);

        // create service request for current move
        this.moveSpaceTransactionService.createServiceRequestForIndvidualMove(moId);

        mo.setRequestor(requestor);
        this.moveNotifyService.sendMailForIssueIndividualMove(mo);

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
        restrictionDef.addClause("activity_log", "mo_id", moId, Operation.EQUALS);
        final List<ActionItem> actionItems = this.actionItemDao.find(restrictionDef);

        for (final ActionItem item : actionItems) {
            this.moveNotifyService.sendMailForIssueRequest(item);
        }

        // updateAssociatedRmpct("mo", mo_id, false);
        this.moveSpaceTransactionService.recordSpaceTransactions(mo, false);

    }

    @Override
    public void issueGroupMove(final String projectId, final String requestor) {

        MoveCommonUtil.commonLog("issueGroupMove", "project_id=[" + projectId + "]", this.logger);

        final Project project = this.moveProjectDao.get(projectId);
        project.setStatus("Issued-In Process");
        project.setDateIssued(Calendar.getInstance().getTime());
        this.moveProjectDao.update(project);

        this.moveOrdersSqlOperator.updateForIssueGroupMove(projectId);

        this.moveSpaceTransactionService.createServiceRequestForGroupMove(projectId);

        project.setRequestor(requestor);
        this.moveNotifyService.sendMailForIssueGroupMove(project);

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
        restrictionDef.addClause("activity_log", "project_id", projectId, Operation.EQUALS);
        final List<ActionItem> actionItems = this.actionItemDao.find(restrictionDef);

        for (final ActionItem item : actionItems) {
            this.moveNotifyService.sendMailForIssueRequest(item);
        }

        // updateAssociatedRmpct("project", project_id, false);
        this.moveSpaceTransactionService.recordSpaceTransactions(project, false);

    }

    @Override
    public void closeGroupMove(final String projectId) {

        MoveCommonUtil.commonLog("closeGroupMove",
            "Close Group Move Request by calling closeGroupMove: [" + projectId + "].",
            this.logger);

        this.checkProjectCompleted(projectId);

        // added for 23.2 Update team related move
        final String moRestriction = "project_id=" + MoveCommonUtil.getLiteralString(projectId);

        this.checkMoveEmployeeTeamConflict(moRestriction);

        this.checkMoveTeamRoomConflict(moRestriction);

        this.moveTeamSqlOperator.updateForCLoseMove(moRestriction);

        // The following SQL statements we can issue for both employee and
        // equipment final moves. For "Employee" and "Room" moves, we update the
        // moves' equipments and tagged furniture
        this.moveAssetSqlOperator.updateEmForCloseGroupMove(projectId);
        this.moveAssetSqlOperator.updateEqForCloseEquipmentMove(moRestriction);        
        this.moveAssetSqlOperator.updateJacksForCloseGroupMove(projectId);
        this.moveAssetSqlOperator.updateEqForCloseGroupMove(projectId);
        this.moveAssetSqlOperator.updateTaForCloseGroupMove(projectId);
        this.moveOrdersSqlOperator.updateMoveOrdersForCloseGroupMove(projectId);

        final Project project = this.moveProjectDao.get(projectId);
        project.setStatus("Closed");
        project.setDateClosed(DateUtils.getCurrentDateWithoutTime());
        this.moveProjectDao.update(project);

        this.moveAssetSqlOperator.updateActionItemsForCloseMove(
            "project_id=" + MoveCommonUtil.getLiteralString(projectId));

        this.moveNotifyService.sendMailForCloseGroupMove(project);

        this.moveSpaceTransactionService.recordSpaceTransactions(project, true);

    }

    @Override
    public void closeIndividualMove(final String moId) {

        MoveCommonUtil.commonLog("closeIndividualMove",
            "Close Individual Move Request by calling closeIndividualMove: [" + moId + "].",
            this.logger);

        this.checkMoveOrderCompleted(moId);

        final MoveOrder moveOrder = this.moveOrderDao.get(Integer.valueOf(moId));

        moveOrder.setStatus("Completed-Verified");
        this.moveOrderDao.update(moveOrder);

        final String moveType = moveOrder.getMoveType();
        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Move type:" + moveType);
        }

        final String moRestriction = "mo_id=" + SqlUtils.makeLiteralOrBlank(moId);

        if ("Employee".equalsIgnoreCase(moveType) || "New Hire".equalsIgnoreCase(moveType)) {

            this.moveAssetSqlOperator.updateEmForCloseInividualMove(moId);
            // this.moveAssetSqlOperator.updateEqForCloseEquipmentMove(moId);
            this.moveAssetSqlOperator.updateJacksForCloseInividualMove(moId);

            this.moveAssetSqlOperator.updateEqForCloseInividualMove(moId);
            this.moveAssetSqlOperator.updateTaForCloseInividualMove(moId);

        } else if ("Leaving".equalsIgnoreCase(moveType)) {

            this.moveAssetSqlOperator.updateEmForCloseInividualMove(moId);
            // this.moveAssetSqlOperator.updateEqForCloseEquipmentMove(moId);
            this.moveAssetSqlOperator.updateJacksForCloseInividualMove(moId);

        } else if ("Room".equalsIgnoreCase(moveType)) {

            this.moveAssetSqlOperator.updateEqForCloseInividualMove(moId);
            this.moveAssetSqlOperator.updateTaForCloseInividualMove(moId);

        } else if ("Equipment".equalsIgnoreCase(moveType)) {

            this.moveAssetSqlOperator.updateEqForCloseEquipmentMove(moRestriction);

        }

        moveOrder.setStatus("Closed");
        moveOrder.setDateClosed(DateUtils.getCurrentDateWithoutTime());
        this.moveOrderDao.update(moveOrder);

        this.moveAssetSqlOperator.updateActionItemsForCloseMove(moRestriction);

        this.moveNotifyService.sendMailForCloseIndividualMove(moveOrder);

        this.moveSpaceTransactionService.recordSpaceTransactions(moveOrder, true);
    }

    @Override
    public void completeSelectedMoves(final String moIds) {

        MoveCommonUtil.commonLog("completeSelectedMoves", "mo_id_list=[" + moIds + "]",
            this.logger);

        final String formattedMoveIdList =
                MoveCommonUtil.removeQuotes(SqlUtils.formatValueForSql(moIds));

        SqlUtils.executeUpdate("mo",
            "UPDATE mo SET status='Completed-Verified' WHERE mo.mo_id IN  (" + formattedMoveIdList
                    + ")");

        // update associatecd space transaction service request status
        final String[] moIdArray = formattedMoveIdList.split(",");
        for (final String moId : moIdArray) {
            this.moveSpaceTransactionService
                .updateAssociatedServiceRequestStatusForIndividualMove(moId);
        }

    }

    @Override
    public void completeSelectedActions(final String activityLogIds) {

        MoveCommonUtil.commonLog("completeSelectedActions",
            "activity_log_id=[" + activityLogIds + "]", this.logger);

        final String formattedActivityLogIdList =
                MoveCommonUtil.removeQuotes(SqlUtils.formatValueForSql(activityLogIds));

        SqlUtils.executeUpdate("activity_log",
            "UPDATE activity_log SET status='COMPLETED-V' WHERE activity_log_id IN  ("
                    + formattedActivityLogIdList + ")");
    }

    @Override
    public void addActionIndividualMove(final DataRecord record) {

        final String moId = String.valueOf(record.getInt("activity_log.mo_id"));
        final String activityType = record.getString("activity_log.activity_type");

        final boolean isActivityTypeValid = (DataStatistics.getInt("activitytype", "activity_type",
            "count", "activity_type=" + MoveCommonUtil.getLiteralString(activityType)) == 1);
        final int autocreateWr = DataStatistics.getInt("activitytype", "autocreate_wr", "sum",
            "activity_type=" + MoveCommonUtil.getLiteralString(activityType));

        MoveCommonUtil.commonLog("addActionIndividualMove", "mo_id=[=[" + moId + "]", this.logger);

        if (isActivityTypeValid) {

            final ActionItem action = new ActionItem();
            final MoveOrder mo = this.moveOrderDao.get(Integer.valueOf(moId));

            action.setDescription(record.getString("activity_log.description"));
            action.setRequestor(record.getString("activity_log.requestor"));
            action.setMoveOrderId(record.getInt("activity_log.mo_id"));
            action.setStatus("REQUESTED");
            action.setActivityType(activityType);
            action.setDateRequested(Calendar.getInstance().getTime());
            action.setDateScheduled(null);
            action.setAutocreateWr(autocreateWr);

            action.setDivisionId(mo.getFromDivision());
            action.setDepartmentId(mo.getFromDepartment());
            action.setAccountId(mo.getAccountId());

            final ActionItem savedActionItem = this.actionItemDao.save(action);

            MoveCommonUtil.addPrimaryKeyValueToResponse(Integer.valueOf(savedActionItem.getId()),
                "activity_log", "activity_log_id");

        } else {

            final ExceptionBase exception = new ExceptionBase();
            exception.setPattern(errorMsgActivityTypeInvalid);
            exception.setTranslatable(true);

            final Object[] args = { activityType };
            exception.setArgs(args);
            throw exception;
        }
    }

    /**
     * Description of the Method
     *
     * @param context Description of the Parameter
     */
    @Override
    public void addActionGroupMove(final DataRecord record) {

        final String projectId = record.getString("activity_log.project_id");
        final String activityType = record.getString("activity_log.activity_type");

        final boolean isActivityTypeValid = (DataStatistics.getInt("activitytype", "activity_type",
            "count", "activity_type=" + MoveCommonUtil.getLiteralString(activityType)) == 1);
        final int autocreateWr = DataStatistics.getInt("activitytype", "autocreate_wr", "sum",
            "activity_type=" + MoveCommonUtil.getLiteralString(activityType));

        MoveCommonUtil.commonLog("addActionGroupMove", "project_id=[=[" + projectId + "]",
            this.logger);

        if (isActivityTypeValid) {

            final ActionItem action = new ActionItem();
            final Project project = this.moveProjectDao.get(projectId);

            action.setDescription(record.getString("activity_log.description"));
            action.setRequestor(record.getString("activity_log.requestor"));
            action.setProjectId(projectId);
            action.setStatus("REQUESTED");
            action.setDateScheduled(null);
            action.setActivityType(activityType);
            action.setDateRequested(Calendar.getInstance().getTime());
            action.setAutocreateWr(autocreateWr);

            action.setDivisionId(project.getDivisionId());
            action.setDepartmentId(project.getDepartmentId());
            action.setAccountId(project.getAccountId());

            final ActionItem savedActionItem = this.actionItemDao.save(action);

            MoveCommonUtil.addPrimaryKeyValueToResponse(Integer.valueOf(savedActionItem.getId()),
                "activity_log", "activity_log_id");

        } else {

            final ExceptionBase exception = new ExceptionBase();
            exception.setPattern(errorMsgActivityTypeInvalid);
            exception.setTranslatable(true);

            final Object[] args = { activityType };
            exception.setArgs(args);
            throw exception;
        }
    }

    /**
     * Check if the move order by given id has been completed.
     *
     * @param moId move order id.
     */
    private void checkMoveOrderCompleted(final String moId) {

        final ExceptionBase exception = new ExceptionBase();

        final int notCompletedMoveRequests = DataStatistics.getInt("activity_log",
            "activity_log_id", "count", "mo_id=" + MoveCommonUtil.getLiteralString(moId)
                    + " AND status NOT IN ('COMPLETED-V','REJECTED','CANCELLED')");

        if (notCompletedMoveRequests != 0) {
            // Not all actions are marked complete. Show error message
            // @translatable
            final String errorMessage =
                    "Please make sure to mark all actions as COMPLETED AND VERIFIED prior to closing this move.";
            exception.setPattern(errorMessage);
            exception.setTranslatable(true);

            throw exception;

        }
        return;
    }

    /**
     * Check if the move project by given id has been completed.
     *
     * When we close a Group Move as part of the closeGroupMove workflow rule we need to check if
     * all moves and actions are completed. We need to run two SELECT statements. The first checks
     * for moves: SELECT 1 FROM mo WHERE project_id = 'xxx' AND status NOT IN
     * ('Completed-Verified','Approved-Cancelled','Issued-Stopped') If there are moves that are not
     * completed and verified we can provide the following message: Please make sure to mark all
     * moves as Completed-Verified, Approved-Cancelled or Issued-Stopped prior to closing this
     * project. The second statement will check for activity log records: SELECT 1 FROM activity_log
     * WHERE project_id = 'xxx' AND status NOT IN ('COMPLETED-V','REJECTED','CANCELLED') If there
     * are actions that are not completed and verified we can provide the following message: Please
     * make sure to mark all actions as COMPLETED-VERIFIED, REJECTED or CANCELLED prior to closing
     * this project.
     *
     * @param projectId move project id.
     */
    private void checkProjectCompleted(final String projectId) {

        final ExceptionBase exception = new ExceptionBase();

        final int notCompletedMoveOrders = DataStatistics.getInt("mo", "mo_id", "count",
            "project_id=" + MoveCommonUtil.getLiteralString(projectId)
                    + " AND status NOT IN ('Completed-Verified','Approved-Cancelled','Issued-Stopped')");

        final int notCompletedMoveRequests = DataStatistics.getInt("activity_log",
            "activity_log_id", "count", "project_id=" + MoveCommonUtil.getLiteralString(projectId)
                    + " AND status NOT IN ('COMPLETED-V','REJECTED','CANCELLED')");

        if (notCompletedMoveOrders != 0) {
            // Not all moves are marked complete. Show error message
            // @translatable
            final String errorMessage =
                    "Please make sure to mark all moves as Completed-Verified, Approved-Cancelled or Issued-Stopped prior to closing this Project.";

            exception.setPattern(errorMessage);
            exception.setTranslatable(true);

            throw exception;

        } else if (notCompletedMoveRequests != 0) {
            // Not all actions are marked complete. Show error message
            // @translatable
            final String errorMessage =
                    "Please make sure to mark all actions as COMPLETED AND VERIFIED, REJECTED or CANCELLED prior to closing this Project.";
            exception.setPattern(errorMessage);
            exception.setTranslatable(true);

            throw exception;

        }
        return;
    }

    /**
     * When an employee was assigned to a team, or unassigned from a team, without this move order;
     * in this case, before making the transactions, the system should check that the employee's
     * team assignment was valid on the date that this move order close executes. If there is a
     * conflict, in that the team assignment is no longer valid, then show a warning message and
     * exclude that employee assignment from the move.
     *
     * @param moRestriction move order restriction.
     */
    private void checkMoveEmployeeTeamConflict(final String moRestriction) {

        final String conflictRes = this.getConflictRestrictionForEmployeeMove(moRestriction);

        final int conlflictEmployeeTeams =
                DataStatistics.getInt("mo", "mo_id", "count", conflictRes);

        if (conlflictEmployeeTeams == 0) {

            // @translatable
            final String conflictMessage =
                    "Found conflict between move order and employee's team assignment when moving employees between team - conflicted assignments are moved.";

            this.addResponseMessage("emTeamConflict", conflictMessage);

            this.removeInvalidEmployeeOrTeamMove(conflictRes);
        }
    }

    /**
     * Check if a team was assigned to a room, or unassigned from a room, without this move order.
     * In this case, before making the transactions above, the system should check that the
     * team-room assignment was valid on the date that this move order close executes. If there is a
     * conflict, in that the team-room assignment is no longer valid, then show a warning message
     * and exclude that team-room assignment from the move.
     *
     * @param moRestriction move order restriction.
     */
    private void checkMoveTeamRoomConflict(final String moRestriction) {
        final String conflictRes = this.getConflictRestrictionForTeamMove(moRestriction);

        final int conlflictRoomeTeams = DataStatistics.getInt("mo", "mo_id", "count", conflictRes);

        if (conlflictRoomeTeams == 0) {

            // @translatable
            final String conflictMessage =
                    "There is a conflict between a room's team assignment and this move order��s team assignments. The system will remove the conflicting move order assignments.";

            this.addResponseMessage("roomTeamConflict", conflictMessage);

            this.removeInvalidEmployeeOrTeamMove(conflictRes);
        }

        return;
    }

    /**
     * When an employee was assigned to a team, or unassigned from a team, without this move order;
     * in this case, before making the transactions, the system should check that the employee's
     * team assignment was valid on the date that this move order close executes. If there is a
     * conflict, in that the team assignment is no longer valid, then show a warning message and
     * exclude that employee assignment from the move.
     *
     * @param messageId alert message id.
     * @param localizedStr localized alert message.
     */
    private void addResponseMessage(final String messageId, final String localizedStr) {

        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();

        if (!context.getResponse().containsKey("jsonExpression")) {

            final JSONObject conflictMessage = new JSONObject();
            conflictMessage.put(messageId, localizedStr);

            context.addResponseParameter("jsonExpression", conflictMessage.toString());
        } else {

            final JSONObject conflictMessage = context.getJSONObject("jsonExpression");
            conflictMessage.put(messageId, localizedStr);

            context.addResponseParameter("jsonExpression", conflictMessage.toString());
        }

        return;
    }

    /**
     * Exclude that employee assignment or team assignment from the move.
     *
     * @param moRestriction move order restriction.
     */
    private void removeInvalidEmployeeOrTeamMove(final String moRestriction) {

        final StringBuilder deleteSql = new StringBuilder();

        deleteSql.append(" delete from mo where ");
        deleteSql.append(moRestriction);

        SqlUtils.executeUpdate("mo", deleteSql.toString());
    }

    /**
     * @return conflict restriction string for team move.
     * @param moRestriction move order restriction by mo_id or project_id.
     */
    private String getConflictRestrictionForTeamMove(final String moRestriction) {

        final StringBuilder conflictRes = new StringBuilder();

        conflictRes.append(moRestriction);
        conflictRes.append(" and exists ( ");
        conflictRes.append(" 	 select 1  from rm_team  ");
        conflictRes.append(" 		 	where rm_team.team_id=mo.to_team_id ");
        conflictRes.append(
            "					 and ( rm_team.bl_id=mo.from_bl_id and rm_team.fl_id=mo.from_fl_id and rm_team.rm_id=mo.from_rm_id "
                    + "									or rm_team.bl_id=mo.to_bl_id and rm_team.fl_id=mo.to_fl_id and rm_team.rm_id=mo.to_rm_id) ");
        conflictRes.append(
            "				 and ( rm_team.date_start is not null and rm_team.date_start &gt; ${sql.currentDate} ");
        conflictRes.append(
            "							or rm_team.date_end is not null and  rm_team.date_end &lt; ${sql.currentDate} )");
        conflictRes.append(" 	) ");
        conflictRes.append(" AND mo.em_id is null and mo.to_team_id is not null ");
        conflictRes.append(" and ( mo.from_rm_id is not null or mo.to_rm_id is not null) ");

        return conflictRes.toString();
    }

    /**
     * @return conflict restriction string for employee move.
     * @param moRestriction move order restriction by mo_id or project_id.
     */
    private String getConflictRestrictionForEmployeeMove(final String moRestriction) {
        final StringBuilder conflictRes = new StringBuilder();

        conflictRes.append(moRestriction);
        conflictRes.append(" AND exists( ");
        conflictRes.append(" 	 select 1  from team  ");
        conflictRes.append(
            " 		 	where ( team.team_id=mo.to_team_id  or team.team_id=mo.from_team_id )");
        conflictRes.append("					 and mo.em_id = team.em_id  ");
        conflictRes.append(
            " 				 and ( team.date_start is not null and team.date_start &gt; ${sql.currentDate} ");
        conflictRes.append(
            " 						or team.date_end is not null and  team.date_end &lt; ${sql.currentDate} )");
        conflictRes.append(" 	 ) ");
        conflictRes.append(
            " AND mo.em_id is not null and ( mo.from_team_id is not null or  mo.to_team_id is not null )");

        return conflictRes.toString();
    }

    /**
     * Getter for the moveSpaceTransactionService property.
     *
     * @return the moveSpaceTransactionService property.
     */
    public IMoveSpaceTransactionService getMoveSpaceTransactionService() {
        return this.moveSpaceTransactionService;
    }

    /**
     * Setter for the moveSpaceTransactionService property.
     *
     * @param moveSpaceTransactionService the moveSpaceTransactionService to set.
     */
    public void setMoveSpaceTransactionService(
            final IMoveSpaceTransactionService moveSpaceTransactionService) {
        this.moveSpaceTransactionService = moveSpaceTransactionService;
    }

    /**
     * Getter for the moveNotifyService property.
     *
     * @return the moveNotifyService property.
     */
    public IMoveNotifyService getMoveNotifyService() {
        return this.moveNotifyService;
    }

    /**
     * Setter for the moveNotifyService property.
     *
     * @param moveNotifyService the moveNotifyService to set.
     */
    public void setMoveNotifyService(final IMoveNotifyService moveNotifyService) {
        this.moveNotifyService = moveNotifyService;
    }

    /**
     * Getter for the moveAssetSqlOperator property.
     *
     * @return the moveAssetSqlOperator property.
     */
    public MoveAssetsSqlOperator getMoveAssetSqlOperator() {
        return this.moveAssetSqlOperator;
    }

    /**
     * Setter for the moveAssetSqlOperator property.
     *
     * @param moveAssetSqlOperator the moveAssetSqlOperator to set.
     */
    public void setMoveAssetSqlOperator(final MoveAssetsSqlOperator moveAssetSqlOperator) {
        this.moveAssetSqlOperator = moveAssetSqlOperator;
    }

    /**
     * Getter for the moveOrdersSqlOperator property.
     *
     * @return the moveOrdersSqlOperator property.
     */
    public MoveOrdersSqlOperator getMoveOrdersSqlOperator() {
        return this.moveOrdersSqlOperator;
    }

    /**
     * Setter for the moveOrdersSqlOperator property.
     *
     * @param moveOrdersSqlOperator the moveOrdersSqlOperator to set.
     */
    public void setMoveOrdersSqlOperator(final MoveOrdersSqlOperator moveOrdersSqlOperator) {
        this.moveOrdersSqlOperator = moveOrdersSqlOperator;
    }

    /**
     * Getter for the moveProjectDao property.
     *
     * @return the moveProjectDao property.
     */
    public IProjectDao getMoveProjectDao() {
        return this.moveProjectDao;
    }

    /**
     * Setter for the moveProjectDao property.
     *
     * @param moveProjectDao the moveProjectDao to set.
     */
    public void setMoveProjectDao(final IProjectDao moveProjectDao) {
        this.moveProjectDao = moveProjectDao;
    }

    /**
     * Getter for the moveOrderDao property.
     *
     * @return the moveOrderDao property.
     */
    public IMoveOrderDao getMoveOrderDao() {
        return this.moveOrderDao;
    }

    /**
     * Setter for the moveOrderDao property.
     *
     * @param moveOrderDao the moveOrderDao to set.
     */
    public void setMoveOrderDao(final IMoveOrderDao moveOrderDao) {
        this.moveOrderDao = moveOrderDao;
    }

    /**
     * Getter for the actionItemDao property.
     *
     * @return the actionItemDao property.
     */
    public IActionItemDao getActionItemDao() {
        return this.actionItemDao;
    }

    /**
     * Setter for the actionItemDao property.
     *
     * @param actionItemDao the actionItemDao to set.
     */
    public void setActionItemDao(final IActionItemDao actionItemDao) {
        this.actionItemDao = actionItemDao;
    }

    /**
     * Getter for the jacksDao property.
     *
     * @return the jacksDao property.
     */
    public IJackDao getJacksDao() {
        return this.jacksDao;
    }

    /**
     * Setter for the jacksDao property.
     *
     * @param jacksDao the jacksDao to set.
     */
    public void setJacksDao(final IJackDao jacksDao) {
        this.jacksDao = jacksDao;
    }

    /**
     * Getter for the employeeDao property.
     *
     * @return the employeeDao property.
     */
    public IDao<Employee> getEmployeeDao() {
        return this.employeeDao;
    }

    /**
     * Setter for the employeeDao property.
     *
     * @param employeeDao the employeeDao to set.
     */
    public void setEmployeeDao(final IDao<Employee> employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    public void deleteSelectedTeamMoves(final String moIds) {
        MoveCommonUtil.commonLog("deleteSelectedTeamMoves", "mo_id_list=[" + moIds + "]",
            this.logger);

        final String formattedMoveIdList =
                MoveCommonUtil.removeQuotes(SqlUtils.formatValueForSql(moIds));

        SqlUtils.executeUpdate("mo",
            "Delete from mo WHERE mo.mo_id IN  (" + formattedMoveIdList + ")");

    }

}
