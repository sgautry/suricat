package com.archibus.app.move.common.dao.datasource;

import java.util.List;

import com.archibus.app.move.common.dao.IJackDao;
import com.archibus.app.move.common.domain.*;
import com.archibus.datasource.ObjectDataSourceImpl;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Jack.
 *
 * @author Zhang Yi
 *
 */
public class JackDataSource extends ObjectDataSourceImpl<Jack> implements IJackDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "jk_id", "id" }, { "em_id", "employeeId" },
			{ "tc_service", "tcService" } };

	/**
	 * Constructs JackDataSource, mapped to <code>jk</code> table, using
	 * <code>Jack</code> bean.
	 */
	public JackDataSource() {
		super("jack", "jk");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

	@Override
	public Jacks getEmployeeJacks(final String emId) {
		final Jacks jacks = new Jacks();

		final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
		restrictionDef.addClause("jk", "em_id", emId, Operation.EQUALS);

		final List<DataRecord> records = this.createCopy().getRecords(restrictionDef);

		final List<Jack> jackList = this.convertRecordsToObjects(records);

		int indexD = 1;
		int indexV = 1;
		boolean firstD = true;
		boolean firstV = true;
		for (final Jack jk : jackList) {

			final String jkId = jk.getId();
			final String telecom = jk.getTcService();

			if ("D".equals(telecom)) {
				if (firstD) {

					jacks.setFromData(jkId);
					firstD = false;

				} else {
					jacks.setDescription(jacks.getDescription() + "\nData Jack " + ++indexD + ":" + jkId);
				}

			} else if ("V".equals(telecom)) {

				if (firstV) {

					jacks.setFromVoice(jkId);
					firstV = false;

				} else {
					jacks.setDescription(jacks.getDescription() + "\nVoice Jack " + ++indexV + ":" + jkId);
				}
			}
		}

		return jacks;

	}

}
