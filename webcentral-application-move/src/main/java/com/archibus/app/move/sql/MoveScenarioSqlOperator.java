package com.archibus.app.move.sql;

import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.datasource.SqlUtils;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides SQL operation methods on Move Scenarios.
 * <p>
 *
 * Used by Move Management Services.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file. *
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveScenarioSqlOperator {

	/**
	 * Property: RIGHT_PARENTHESIS. Typical value is TODO. Constant: TODO.
	 */
	private static final String RIGHT_PARENTHESIS = ")";
	/**
	 * Table name.
	 */
	private static final String T_MO = "mo";
	/**
	 * Table name.
	 */
	private static final String MO_SCENARIO_EM = "mo_scenario_em";

	/**
	 * Update move orders of project by given move scenario.
	 *
	 * @param scenarioId
	 *            scenario name
	 * @param projectId
	 *            project code
	 */
	public void deleteScenario(final String scenarioId, final String projectId) {

		String sqlStatment;
		sqlStatment = "DELETE FROM mo_scenario_em WHERE mo_scenario_em.scenario_id = "
				+ MoveCommonUtil.getLiteralString(scenarioId) + " AND  mo_scenario_em.project_id = "
				+ MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate(MO_SCENARIO_EM, sqlStatment);

		sqlStatment = "DELETE FROM mo_scenario WHERE mo_scenario.scenario_id ="
				+ MoveCommonUtil.getLiteralString(scenarioId) + " AND mo_scenario.project_id ="
				+ MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate("mo_scenario", sqlStatment);

	}

	/**
	 * Update move orders of project by given move scenario.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 */
	public void updateForMoveProject(final String projectId, final String scenarioId) {
		// "UPDATE mo SET mo.to_bl_id = mo_scenario_em.to_bl_id, mo.to_fl_id =
		// mo_scenario_em.to_fl_id, "
		// +
		// "mo.to_rm_id=mo_scenario_em.to_rm_id FROM mo_scenario_em WHERE
		// mo.project_id=mo_scenario_em.project_id "
		// +
		// "AND mo.em_id = mo_scenario_em.em_id AND mo_scenario_em.to_bl_id IS
		// NOT NULL AND
		// mo_scenario_em.to_fl_id IS NOT NULL AND mo_scenario_em.to_rm_id IS
		// NOT NULL AND "
		// +
		// "( mo.to_bl_id IS NULL OR mo.to_fl_id IS NULL OR mo.to_rm_id IS NULL
		// OR mo.to_bl_id <>
		// mo_scenario_em.to_bl_id OR mo.to_fl_id <> mo_scenario_em.to_fl_id OR
		// mo.to_rm_id <>
		// mo_scenario_em.to_rm_id )"
		// + " AND mo_scenario_em.scenario_id='"
		// + scenarioId
		// + "' and mo_scenario_em.project_id='" + projectId + "'";

		StringBuilder sqlStatement = new StringBuilder();

		// below sql update to location for moving employees
		if (!SqlUtils.isOracle()) {

			sqlStatement.append(
					"UPDATE mo SET mo.to_bl_id = mo_scenario_em.to_bl_id, mo.to_fl_id = mo_scenario_em.to_fl_id, mo.to_rm_id=mo_scenario_em.to_rm_id ");
			sqlStatement.append("FROM mo_scenario_em ");
			sqlStatement.append("WHERE mo.project_id=mo_scenario_em.project_id ");
			sqlStatement.append(" 	AND mo_scenario_em.scenario_id=" + MoveCommonUtil.getLiteralString(scenarioId));
			sqlStatement.append("	AND mo_scenario_em.project_id =" + MoveCommonUtil.getLiteralString(projectId));
			sqlStatement.append("	AND mo.em_id = mo_scenario_em.em_id and mo_scenario_em.em_id is not null ");
			sqlStatement.append(
					"				AND mo.from_team_id is null and mo.to_team_id is null and mo_scenario_em.team_id is null ");

			SqlUtils.executeUpdate(T_MO, sqlStatement.toString());

		} else {
			sqlStatement = new StringBuilder();
			sqlStatement.append(" UPDATE mo SET (mo.to_bl_id, mo.to_fl_id, mo.to_rm_id) = ");
			sqlStatement.append("(	SELECT mo_scenario_em.to_bl_id, mo_scenario_em.to_fl_id, mo_scenario_em.to_rm_id ");
			sqlStatement.append("	FROM mo_scenario_em  ");
			sqlStatement.append("	WHERE mo.project_id=mo_scenario_em.project_id ");
			sqlStatement
					.append(" 		AND mo_scenario_em.scenario_id =" + MoveCommonUtil.getLiteralString(scenarioId));
			sqlStatement.append("		AND mo_scenario_em.project_id =" + MoveCommonUtil.getLiteralString(projectId));
			sqlStatement.append(" 		AND mo.em_id = mo_scenario_em.em_id and mo_scenario_em.team_id is null )");
			sqlStatement.append(" WHERE mo.project_id = " + MoveCommonUtil.getLiteralString(projectId));
			sqlStatement.append(" 	AND  mo.from_team_id is null and mo.to_team_id is null ");
			sqlStatement.append(" 	AND mo.em_id IN ( SELECT em_id FROM mo_scenario_em ");
			sqlStatement.append("						WHERE project_id =" + MoveCommonUtil.getLiteralString(projectId)
					+ " AND scenario_id =" + MoveCommonUtil.getLiteralString(scenarioId));
			sqlStatement.append(
					"						and mo_scenario_em.team_id is null and mo_scenario_em.to_rm_id is not null )");

			SqlUtils.executeUpdate(T_MO, sqlStatement.toString());

		}

		// below sql insert to location for moving teams
		{
			sqlStatement = new StringBuilder();
			sqlStatement.append(
					"INSERT INTO mo (project_id, to_team_id, to_bl_id, to_fl_id, to_rm_id ,mo_type, date_start_req, date_to_perform) ");
			sqlStatement.append(
					"(	SELECT m.project_id, m.team_id, m.to_bl_id, m.to_fl_id, m.to_rm_id, 'Team', p.date_start, p.date_end ");
			sqlStatement.append(
					"	FROM mo_scenario_em ${sql.as} m left outer join project ${sql.as} p on p.project_id=m.project_id ");
			sqlStatement.append("	WHERE m.scenario_id=" + MoveCommonUtil.getLiteralString(scenarioId));
			sqlStatement.append("		AND m.project_id=" + MoveCommonUtil.getLiteralString(projectId));
			sqlStatement.append(" 		AND m.team_id is not null and m.to_rm_id is not null and m.em_id is null");
			sqlStatement.append("		and not exists ( select 1 from mo ${sql.as} o  ");
			sqlStatement.append(
					"									where o.to_bl_id=m.to_bl_id and o.to_fl_id=m.to_fl_id and o.to_rm_id=m.to_rm_id ");
			sqlStatement.append("						and o.project_id = m.project_id ");
			sqlStatement.append("						and o.to_team_id = m.team_id and o.em_id is null ) )");

			SqlUtils.executeUpdate(T_MO, sqlStatement.toString());
		}

		// below sql remove team's place holder from move_order for moving teams
		{
			sqlStatement = new StringBuilder();
			sqlStatement.append("Delete from mo ");
			sqlStatement.append("	where mo.project_id=" + MoveCommonUtil.getLiteralString(projectId));
			sqlStatement.append("		and mo.em_id is null and  mo.to_rm_id is null and mo.from_rm_id is null ");
			sqlStatement.append("		and mo.to_team_id is not null ");
			sqlStatement.append("		and exists ( select 1 from mo ${sql.as} o  ");
			sqlStatement.append("						where o.to_rm_id is not null and o.project_id=mo.project_id ");
			sqlStatement.append("						and o.to_team_id = mo.to_team_id and o.em_id is null )");

			SqlUtils.executeUpdate(T_MO, sqlStatement.toString());
		}

	}

	/**
	 * Insert new Move Scenario Employee records from another existing Scenario.
	 *
	 * @param origScenarioId
	 *            existing original scenario name
	 * @param scenarioId
	 *            scenario name
	 * @param projectId
	 *            project code
	 */
	public void insertForCopyScenario(final String origScenarioId, final String scenarioId, final String projectId) {
		String sqlStatement;
		sqlStatement = "INSERT INTO mo_scenario_em (project_id, scenario_id, team_id, em_id,to_rm_id,to_fl_id,to_bl_id,date_start,date_end)"
				+ " SELECT project_id, " + MoveCommonUtil.getLiteralString(scenarioId)
				+ ", team_id,  em_id,to_rm_id,to_fl_id,to_bl_id,date_start,date_end FROM mo_scenario_em WHERE mo_scenario_em.scenario_id = '"
				+ origScenarioId + "' AND mo_scenario_em.project_id = " + MoveCommonUtil.getLiteralString(projectId);

		SqlUtils.executeUpdate(MO_SCENARIO_EM, sqlStatement);
	}

	/**
	 * Insert new Move Scenario Employee records for Synchronizing Scenarios.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 */
	public void insertForSynchronizeScenario(final String scenarioId, final String projectId) {

		final String insertForEm = "INSERT INTO mo_scenario_em (project_id, scenario_id, em_id,  to_bl_id, to_fl_id, to_rm_id, from_bl_id, from_fl_id, from_rm_id ) "
				+ " SELECT mo.project_id, " + MoveCommonUtil.getLiteralString(scenarioId) + ", mo.em_id, "
				+ " mo.to_bl_id, mo.to_fl_id, mo.to_rm_id, mo.from_bl_id, mo.from_fl_id, mo.from_rm_id "
				+ "FROM mo WHERE mo.project_id =" + MoveCommonUtil.getLiteralString(projectId)
				+ "  AND mo.mo_type= 'Employee' AND mo.to_team_id is null "
				+ "AND NOT Exists ( SELECT 1 from mo_scenario_em ${sql.as} m where m.em_id=mo.em_id and m.project_id = "
				+ MoveCommonUtil.getLiteralString(projectId) + " AND m.scenario_id  = "
				+ MoveCommonUtil.getLiteralString(scenarioId) + " ) ";

		SqlUtils.executeUpdate(MO_SCENARIO_EM, insertForEm);

		final StringBuilder insertForTeamRoom = new StringBuilder();
		insertForTeamRoom
				.append("INSERT INTO mo_scenario_em (project_id, scenario_id, team_id, to_bl_id, to_fl_id, to_rm_id) ");
		insertForTeamRoom.append(" SELECT DISTINCT mo.project_id, ")
				.append(MoveCommonUtil.getLiteralString(scenarioId));
		insertForTeamRoom.append("	, mo.to_team_id, mo.to_bl_id, mo.to_fl_id, mo.to_rm_id  FROM mo ");
		insertForTeamRoom.append(" WHERE mo.project_id =").append(MoveCommonUtil.getLiteralString(projectId));
		insertForTeamRoom.append("  AND mo.mo_type = 'Team' AND mo.em_id is null AND mo.to_rm_id is not null ");
		insertForTeamRoom.append("	AND NOT Exists ( SELECT 1 FROM mo_scenario_em ${sql.as} m");
		insertForTeamRoom.append(" 						WHERE m.project_id = ")
				.append(MoveCommonUtil.getLiteralString(projectId));
		insertForTeamRoom.append(" 						AND   m.scenario_id = ")
				.append(MoveCommonUtil.getLiteralString(scenarioId));
		insertForTeamRoom.append(" 						AND   m.to_bl_id = mo.to_bl_id ");
		insertForTeamRoom.append(" 						AND   m.to_fl_id = mo.to_fl_id ");
		insertForTeamRoom.append(" 						AND   m.to_rm_id = mo.to_rm_id ");
		insertForTeamRoom.append(" 						AND   m.team_id = mo.to_team_id");
		insertForTeamRoom.append(" )");

		SqlUtils.executeUpdate(MO_SCENARIO_EM, insertForTeamRoom.toString());

		final String insertForTeam = "INSERT INTO mo_scenario_em (project_id, scenario_id, team_id) SELECT mo.project_id, "
				+ MoveCommonUtil.getLiteralString(scenarioId) + ", mo.to_team_id FROM mo WHERE mo.project_id ="
				+ MoveCommonUtil.getLiteralString(projectId)
				+ "  AND mo.mo_type = 'Team'  AND mo.em_id is null AND mo.to_rm_id is null AND mo.from_rm_id is null  "
				+ "AND NOT Exists ( SELECT 1 from mo_scenario_em ${sql.as} m where m.team_id=mo.to_team_id and m.project_id = "
				+ MoveCommonUtil.getLiteralString(projectId) + " AND   m.scenario_id = "
				+ MoveCommonUtil.getLiteralString(scenarioId) + RIGHT_PARENTHESIS;

		SqlUtils.executeUpdate(MO_SCENARIO_EM, insertForTeam);

	}

	/**
	 * Delete Move Scenario Employee records for Synchronizing Scenarios.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 */
	public void deleteForSynchronizeScenario(final String scenarioId, final String projectId) {

		final String deleteForEm = " DELETE FROM mo_scenario_em WHERE "
				+ "mo_scenario_em.em_id is not null and mo_scenario_em.em_id NOT IN (SELECT mo.em_id FROM mo WHERE mo.mo_type = 'Employee' AND mo.project_id = "
				+ MoveCommonUtil.getLiteralString(projectId) + ")  AND  mo_scenario_em.project_id  = "
				+ MoveCommonUtil.getLiteralString(projectId) + " AND  mo_scenario_em.scenario_id = "
				+ MoveCommonUtil.getLiteralString(scenarioId);

		SqlUtils.executeUpdate(MO_SCENARIO_EM, deleteForEm);

		final String deleteForTeam = "DELETE FROM mo_scenario_em WHERE "
				+ "mo_scenario_em.team_id is not null and mo_scenario_em.team_id NOT IN (SELECT mo.to_team_id FROM mo WHERE mo.mo_type = 'Team' AND mo.project_id = "
				+ MoveCommonUtil.getLiteralString(projectId) + ")  AND   mo_scenario_em.project_id  = "
				+ MoveCommonUtil.getLiteralString(projectId) + " AND mo_scenario_em.scenario_id = "
				+ MoveCommonUtil.getLiteralString(scenarioId);

		SqlUtils.executeUpdate(MO_SCENARIO_EM, deleteForTeam);
	}

	/**
	 * If the new employee copied has To Location, needs to fetch the filename,
	 * also the rooms added to team need to include filename.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 */
	public void updateFilenameForSynchronizeScenario(final String scenarioId, final String projectId) {

		final StringBuilder updateFilenameSql = new StringBuilder();
		updateFilenameSql.append(" UPDATE mo_scenario_em set filename=(");
		updateFilenameSql.append(" 		SELECT MIN(g.filename) from afm_enterprise_graphics  ${sql.as} g ");
		updateFilenameSql.append(" 			WHERE 	g.bl_id=mo_scenario_em.to_bl_id ");
		updateFilenameSql.append("  		AND 	g.fl_id=mo_scenario_em.to_fl_id ");
		updateFilenameSql.append("			AND 	g.dwg_name = (SELECT MAX(rm.dwgname) FROM rm ");
		updateFilenameSql.append(" 									WHERE rm.bl_id = mo_scenario_em.to_bl_id ");
		updateFilenameSql.append(" 									and rm.fl_id = mo_scenario_em.to_fl_id ");
		updateFilenameSql.append(" 									and rm.rm_id = mo_scenario_em.to_rm_id )");
		updateFilenameSql.append(" 			AND		g.usage_type='Inventory' and g.file_type='SVG' )");
		updateFilenameSql.append(" WHERE mo_scenario_em.project_id = ")
				.append(MoveCommonUtil.getLiteralString(projectId));
		updateFilenameSql.append(" 	AND   mo_scenario_em.scenario_id = ")
				.append(MoveCommonUtil.getLiteralString(scenarioId));
		updateFilenameSql.append(" 	AND   mo_scenario_em.to_rm_id is not null ");
		updateFilenameSql.append(" 	AND   mo_scenario_em.filename is null ");

		SqlUtils.executeUpdate(MO_SCENARIO_EM, updateFilenameSql.toString());
	}

	/**
	 * Add employees from move project to move scenario.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 */
	public void insertMoveScenarioEm(final String scenarioId, final String projectId) {

		final StringBuilder sqlStatment = new StringBuilder();
		sqlStatment.append(
				"INSERT INTO mo_scenario_em (project_id, scenario_id, em_id, to_bl_id, to_fl_id, to_rm_id, from_bl_id, from_fl_id, from_rm_id, filename)");
		sqlStatment.append("SELECT DISTINCT mo.project_id, ");
		sqlStatment.append(MoveCommonUtil.getLiteralString(scenarioId));
		sqlStatment.append(
				", mo.em_id, mo.to_bl_id, mo.to_fl_id, mo.to_rm_id, mo.from_bl_id, mo.from_fl_id, mo.from_rm_id");
        sqlStatment.append(
            ", ( select e.filename from afm_enterprise_graphics ${sql.as} e where e.bl_id=mo.to_bl_id and e.fl_id=mo.to_fl_id and e.usage_type='Inventory' and e.file_type='SVG'"
                    + " and e.dwg_name=( select r.dwgname from rm ${sql.as} r where r.bl_id=mo.to_bl_id and r.fl_id=mo.to_fl_id and r.rm_id=mo.to_rm_id)"
                    + " and e.filename NOT LIKE'%-regcompliance.svg' ) ");
		sqlStatment.append(" FROM  mo WHERE mo.project_id =" + MoveCommonUtil.getLiteralString(projectId));
		sqlStatment.append(" AND mo.mo_type = 'Employee' ");
		sqlStatment.append(" AND mo.to_team_id is null AND mo.from_team_id is null ");

		SqlUtils.executeUpdate(MO_SCENARIO_EM, sqlStatment.toString());

	}

	/**
	 * Add empoyees from move project to move scenario.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 */
	public void insertMoveScenarioTeam(final String scenarioId, final String projectId) {

		final StringBuilder insertSqlStatment = new StringBuilder();
		insertSqlStatment.append(
				"INSERT INTO mo_scenario_em (project_id, scenario_id, team_id, to_bl_id, to_fl_id, to_rm_id, filename)");
		insertSqlStatment.append("  SELECT DISTINCT mo.project_id, ");
		insertSqlStatment.append(MoveCommonUtil.getLiteralString(scenarioId));
		insertSqlStatment.append(", mo.to_team_id, mo.to_bl_id, mo.to_fl_id, mo.to_rm_id");
        insertSqlStatment.append(
            ", ( select e.filename from  afm_enterprise_graphics ${sql.as} e where e.bl_id=mo.to_bl_id and e.fl_id=mo.to_fl_id and e.usage_type='Inventory' and e.file_type='SVG'"
                    + " and e.dwg_name=(select r.dwgname from rm ${sql.as} r where r.bl_id=mo.to_bl_id and r.fl_id=mo.to_fl_id and r.rm_id=mo.to_rm_id)"
                    + " and e.filename NOT  LIKE'%-regcompliance.svg' ) ");
		insertSqlStatment.append(" FROM mo, em WHERE mo.project_id =" + MoveCommonUtil.getLiteralString(projectId));
		insertSqlStatment.append(" AND mo.mo_type = 'Team' AND mo.em_id is null AND mo.to_team_id is not null ");

		SqlUtils.executeUpdate(MO_SCENARIO_EM, insertSqlStatment.toString());

		final StringBuilder clearDuplicatedSql = new StringBuilder();
		clearDuplicatedSql.append("DELETE FROM mo_scenario_em ");
		clearDuplicatedSql.append(" WHERE mo_scenario_em.project_id =" + MoveCommonUtil.getLiteralString(projectId));
		clearDuplicatedSql.append(" AND mo_scenario_em.scenario_id =" + MoveCommonUtil.getLiteralString(scenarioId));
		clearDuplicatedSql.append(" AND mo_scenario_em.team_id is not null and mo_scenario_em.to_rm_id is null ");
		clearDuplicatedSql.append(" AND exists (select 1 from mo_scenario_em ${sql.as} e ");
		clearDuplicatedSql.append(" 			WHERE e.project_id =" + MoveCommonUtil.getLiteralString(projectId));
		clearDuplicatedSql.append(" 			AND e.scenario_id =" + MoveCommonUtil.getLiteralString(scenarioId));
		clearDuplicatedSql.append(" 			AND e.team_id=mo_scenario_em.team_id and e.to_rm_id is not null )");

		SqlUtils.executeUpdate(MO_SCENARIO_EM, clearDuplicatedSql.toString());
	}

	/**
	 * Copy occupancy between inventory and scenario layout.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 * @param toFilename
	 *            filename copy occupancy to
	 * @param toBlId
	 *            to_bl_id
	 * @param toFlId
	 *            to_fl_id
	 * @param fromFilename
	 *            filename copy occupancy from
	 */
	public void copyScenarioOccupancy(final String projectId, final String scenarioId, final String toFilename,
			final String toBlId, final String toFlId, final String fromFilename) {

		final StringBuilder validUpdateSql = getUpdateValidOccupancySql(projectId, scenarioId, toFilename,
				fromFilename);

		SqlUtils.executeUpdate(MO_SCENARIO_EM, validUpdateSql.toString());

		final StringBuilder invalidUpdateSql = getClearInvlaidOccupancySql(projectId, scenarioId, toFilename,
				fromFilename);

		SqlUtils.executeUpdate(MO_SCENARIO_EM, invalidUpdateSql.toString());
	}

	/**
	 * Get Update SQL for clearing Occupancies not valid anymore.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 * @param toFilename
	 *            copy occupancy to
	 * @param fromFilename
	 *            copy occupancy from
	 * @return update sql
	 */
	private StringBuilder getClearInvlaidOccupancySql(final String projectId, final String scenarioId,
			final String toFilename, final String fromFilename) {
		final StringBuilder invalidUpdateSql = new StringBuilder();
		invalidUpdateSql.append("UPDATE mo_scenario_em ");
		invalidUpdateSql.append("SET filename=null, to_bl_id=null, to_fl_id=null, to_rm_id=null ");
		invalidUpdateSql.append("WHERE mo_scenario_em.filename=").append(MoveCommonUtil.getLiteralString(fromFilename));
		invalidUpdateSql.append(" AND mo_scenario_em.project_id=").append(MoveCommonUtil.getLiteralString(projectId));
		invalidUpdateSql.append(" AND mo_scenario_em.scenario_id=").append(MoveCommonUtil.getLiteralString(scenarioId));
		invalidUpdateSql.append(" AND NOT EXISTS (SELECT 1 from rm_trial, afm_enterprise_graphics ");
		invalidUpdateSql.append("					where rm_trial.bl_id=mo_scenario_em.to_bl_id  ");
		invalidUpdateSql.append("					and rm_trial.fl_id=mo_scenario_em.to_fl_id ");
		invalidUpdateSql.append("					and rm_trial.rm_id=mo_scenario_em.to_rm_id ");
		invalidUpdateSql.append("					and rm_trial.dwgname=afm_enterprise_graphics.dwg_name ");
		invalidUpdateSql.append("					and rm_trial.layer_name=afm_enterprise_graphics.usage_subtype");
		invalidUpdateSql.append("					and afm_enterprise_graphics.filename =")
				.append(MoveCommonUtil.getLiteralString(toFilename));
		invalidUpdateSql.append("					and afm_enterprise_graphics.bl_id=mo_scenario_em.to_bl_id ");
		invalidUpdateSql.append("					and afm_enterprise_graphics.fl_id=mo_scenario_em.to_fl_id )");
		invalidUpdateSql.append(" AND NOT EXISTS ( SELECT 1 from rm, afm_enterprise_graphics ");
		invalidUpdateSql.append("					where mo_scenario_em.to_bl_id=rm.bl_id ");
		invalidUpdateSql.append("					and mo_scenario_em.to_fl_id=rm.fl_id  ");
		invalidUpdateSql.append("					and mo_scenario_em.to_rm_id=rm.rm_id  ");
		invalidUpdateSql.append("						and afm_enterprise_graphics.filename =")
				.append(MoveCommonUtil.getLiteralString(toFilename));
		invalidUpdateSql.append("					and afm_enterprise_graphics.bl_id=mo_scenario_em.to_bl_id  ");
		invalidUpdateSql.append("					and afm_enterprise_graphics.fl_id=mo_scenario_em.to_fl_id  ");
		invalidUpdateSql.append("					and afm_enterprise_graphics.dwg_name=rm.dwgname  ");
		invalidUpdateSql.append("					and afm_enterprise_graphics.usage_subtype is null ) ");
		return invalidUpdateSql;
	}

	/**
	 * Get Update SQL for Occupancies still valid.
	 *
	 * @param projectId
	 *            project code
	 * @param scenarioId
	 *            scenario name
	 * @param toFilename
	 *            copy occupancy to
	 * @param fromFilename
	 *            copy occupancy from
	 * @return update sql
	 */
	private StringBuilder getUpdateValidOccupancySql(final String projectId, final String scenarioId,
			final String toFilename, final String fromFilename) {
		final StringBuilder validUpdateSql = new StringBuilder();
		validUpdateSql.append(" UPDATE mo_scenario_em ");
		validUpdateSql.append(" SET filename=").append(MoveCommonUtil.getLiteralString(toFilename));
		validUpdateSql.append(" WHERE mo_scenario_em.filename=").append(MoveCommonUtil.getLiteralString(fromFilename));
		validUpdateSql.append("	AND mo_scenario_em.project_id=").append(MoveCommonUtil.getLiteralString(projectId));
		validUpdateSql.append("	AND mo_scenario_em.scenario_id=").append(MoveCommonUtil.getLiteralString(scenarioId));
		validUpdateSql.append("	AND ( EXISTS ( SELECT 1 from rm_trial, afm_enterprise_graphics ");
		validUpdateSql.append("					where mo_scenario_em.to_bl_id=rm_trial.bl_id  ");
		validUpdateSql.append("						and mo_scenario_em.to_fl_id=rm_trial.fl_id ");
		validUpdateSql.append("						and mo_scenario_em.to_rm_id=rm_trial.rm_id ");
		validUpdateSql.append("						and afm_enterprise_graphics.filename=")
				.append(MoveCommonUtil.getLiteralString(toFilename));
		validUpdateSql.append("						and afm_enterprise_graphics.bl_id=mo_scenario_em.to_bl_id ");
		validUpdateSql.append("						and afm_enterprise_graphics.fl_id=mo_scenario_em.to_fl_id ");
		validUpdateSql.append("						and afm_enterprise_graphics.dwg_name=rm_trial.dwgname ");
		validUpdateSql.append("						and afm_enterprise_graphics.usage_subtype=rm_trial.layer_name )");
		validUpdateSql.append("		or EXISTS ( SELECT 1 from rm, afm_enterprise_graphics ");
		validUpdateSql.append("					where mo_scenario_em.to_bl_id=rm.bl_id  ");
		validUpdateSql.append("						and mo_scenario_em.to_fl_id=rm.fl_id  ");
		validUpdateSql.append("						and mo_scenario_em.to_rm_id=rm.rm_id  ");
		validUpdateSql.append("						and afm_enterprise_graphics.filename  =")
				.append(MoveCommonUtil.getLiteralString(toFilename));
		validUpdateSql.append("						and afm_enterprise_graphics.bl_id=mo_scenario_em.to_bl_id  ");
		validUpdateSql.append("						and afm_enterprise_graphics.fl_id=mo_scenario_em.to_fl_id  ");
		validUpdateSql.append("						and afm_enterprise_graphics.dwg_name=rm.dwgname  ");
		validUpdateSql.append("						and afm_enterprise_graphics.usage_subtype is null ) ");
		validUpdateSql.append("	 )");
		return validUpdateSql;
	}

	/**
	 * Delete teams to which from_team_id or to_team_id equals .
	 *
	 * @param teams
	 *            selected teams
	 * @param projectId
	 *            project id
	 */
	public void deleteSelectedTeams(final String teams, final String projectId) {
		final StringBuilder clause = new StringBuilder();
		final String[] arrTeams = teams.split(",");
		final int len = arrTeams.length;
		clause.append("('");
		for (int i = 0; i < len; i++) {
			if (i < len - 1) {
				clause.append(arrTeams[i] + "','");
			} else {
				clause.append(arrTeams[i] + "')");
			}
		}
		StringBuilder sqlStatement = new StringBuilder();
		sqlStatement.append("Delete from mo where mo.mo_type='Team' and mo.project_id=");
		sqlStatement.append(MoveCommonUtil.getLiteralString(projectId));
		sqlStatement.append(" and (mo.from_team_id in ");
		sqlStatement.append(clause);
		sqlStatement.append(" and mo.to_team_id is null)");
		sqlStatement.append(" or (mo.to_team_id in ");
		sqlStatement.append(clause);
		sqlStatement.append(" and mo.from_team_id is null)");
		sqlStatement.append(" or  (mo.from_team_id in ");
		sqlStatement.append(clause);
		sqlStatement.append(" and mo.to_team_id in ");
		sqlStatement.append(clause);
		sqlStatement.append(RIGHT_PARENTHESIS);
		SqlUtils.executeUpdate(T_MO, sqlStatement.toString());

		sqlStatement = new StringBuilder();
		sqlStatement.append("update mo set mo.from_team_id=null where mo.mo_type='Team' and mo.project_id=");
		sqlStatement.append(MoveCommonUtil.getLiteralString(projectId));
		sqlStatement.append(" and mo.from_team_id in ");
		sqlStatement.append(clause);
		SqlUtils.executeUpdate(T_MO, sqlStatement.toString());

		sqlStatement = new StringBuilder();
		sqlStatement.append("update mo set mo.to_team_id=null where mo.mo_type='Team' and mo.project_id=");
		sqlStatement.append(MoveCommonUtil.getLiteralString(projectId));
		sqlStatement.append(" and mo.to_team_id  in ");
		sqlStatement.append(clause);
		SqlUtils.executeUpdate(T_MO, sqlStatement.toString());
	}
}
