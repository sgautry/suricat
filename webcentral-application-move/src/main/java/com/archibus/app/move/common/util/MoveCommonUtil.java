package com.archibus.app.move.common.util;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.jobmanager.*;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Utility class for Move Management. Provides Common used methods.
 * <p>
 *
 * Used by Move Management Services.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveCommonUtil {

	/**
	 * Add the primary key value to the WFR response in the JSON format expected
	 * by the edit form command.
	 *
	 * @param primaryKeyValue
	 * @param tableName
	 * @param primaryKeyName
	 */
	public static void addPrimaryKeyValueToResponse(final Object primaryKeyValue, final String tableName,
			final String primaryKeyName) {

		final JSONObject primaryKeyRecord = new JSONObject();
		primaryKeyRecord.put(tableName + "." + primaryKeyName, "" + primaryKeyValue);

		final JSONObject data = new JSONObject();
		data.put("record", primaryKeyRecord);

		ContextStore.get().getEventHandlerContext().addResponseParameter("jsonExpression", data.toString());
	}

	/**
	 * @Returns an xml string with default questionnaire answer values.
	 *
	 * @param questId
	 *            Questionnaire ID
	 */
	public static String getDefaultQuestionnaireValues(final String questId) {

		String xml = "";

		final DataSource ds = DataSourceFactory.createDataSource();
		ds.addTable("questions");
		ds.addField("questionnaire_id");
		ds.addField("quest_name");
		ds.addField("format_type");
		ds.addField("enum_list");
		ds.addRestriction(Restrictions.eq("questions", "questionnaire_id", questId));

		final List<DataRecord> records = ds.getRecords();

		if (!records.isEmpty()) {

			xml = "<questionnaire questionnaire_id=\"" + convert2validXMLValueCustom(questId) + "\">";

			for (int r = 0; r < records.size(); r++) {
				String new_value = "";
				if (records.get(r).getString("questions.format_type").equals("Enum")) {
					final String enumList = records.get(r).getString("questions.enum_list");
					final String[] splitList = enumList.split(";");
					if (splitList.length > 0) {
						new_value = splitList[0];
					}
				}
				final String quest_name = records.get(r).getString("questions.quest_name");
				xml = xml + "<question quest_name=\"" + convert2validXMLValueCustom(quest_name) + "\" value=\""
						+ convert2validXMLValueCustom(new_value) + "\"/>";
			}
			xml = xml + "</questionnaire>";
		}

		return xml;
	}

	/**
	 * @Returns encoded value for insertion into an xml string.
	 *
	 * @param fieldValue
	 *            Value to be encoded
	 */
	private static String convert2validXMLValueCustom(String fieldValue) {
		/*
		 * use custom encoder for all special characters since the hidden
		 * question XML field is automatically decoded upon view load. Special
		 * characters (like '&' and '>') must be encoded for evaluation of the
		 * XML statement.
		 */
		fieldValue = fieldValue.replaceAll("/&amp;/g", "&");
		fieldValue = fieldValue.replaceAll("/&/g", "%26");
		fieldValue = fieldValue.replaceAll("/&lt;/g", "<");
		fieldValue = fieldValue.replaceAll("/</g", "%3C");
		fieldValue = fieldValue.replaceAll("/&gt;/g", ">");
		fieldValue = fieldValue.replaceAll("/>/g", "%3E");
		fieldValue = fieldValue.replaceAll("/&apos;/g", "\'");
		fieldValue = fieldValue.replaceAll("/\'/g", "%27");
		fieldValue = fieldValue.replaceAll("/&quot;/g", "\"");
		fieldValue = fieldValue.replaceAll("/\"/g", "%22");

		return fieldValue;
	}

	/**
	 * Log the move operations.
	 *
	 * @param operation
	 *            operation for logging
	 * @param info
	 *            information for logging
	 * @param logger
	 *            logger object
	 */
	public static void commonLog(final String operation, final String info, final Logger logger) {

		if (logger.isDebugEnabled()) {
			logger.debug(info);
			logger.debug(operation);
		}

	}

	/**
	 * Record error message and throw exception with specific argument(s).
	 *
	 * @param errorMsg
	 *            error message for logging
	 * @param args
	 *            arguments list
	 */
	public static void throwException(final String errorMsg, final Object[] args) {

		final ExceptionBase exception = new ExceptionBase();
		exception.setPattern(errorMsg);
		exception.setTranslatable(true);

		exception.setArgs(args);
		throw exception;
	}

	/**
	 * @return string from which the quote are removed.
	 *
	 * @param field
	 *            field value in format of string to remove quote
	 */
	public static String removeQuotes(String field) {
		if (field == null) {
			return field;
		}

		field = field.trim();

		if (field.length() > 0 && field.charAt(0) == '\'') {
			field = field.substring(1);
		}

		if (field.length() > 0 && field.charAt(field.length() - 1) == '\'') {
			field = field.substring(0, field.length() - 1);
		}

		return field;
	}

	/**
	 * Start a job and return status as result
	 *
	 * @param context
	 * @param job
	 */
	public void startJob(final Job job) {

		final String jobId = ContextStore.get().getJobManager().startJob(job);

		// add the status to the response
		final JSONObject result = new JSONObject();
		result.put("jobId", jobId);

		// get the job status from the job id
		final JobStatus status = ContextStore.get().getJobManager().getJobStatus(jobId);
		result.put("jobStatus", status.toString());

		ContextStore.get().getEventHandlerContext().addResponseParameter("jsonExpression", result.toString());
	}

	/**
	 * @return string from which the quote are removed.
	 *
	 * @param str
	 *            String for converting.
	 */
	public static String getLiteralString(final String str) {

		return StringUtil.isNullOrEmpty(str) ? null : (" '" + SqlUtils.makeLiteralOrBlank(str) + "'");

	}

}
