package com.archibus.app.move.project;

import java.util.List;

import org.apache.log4j.Logger;

import com.archibus.app.common.depreciation.dao.IEquipmentDao;
import com.archibus.app.common.depreciation.domain.Equipment;
import com.archibus.app.common.finanal.impl.DateUtils;
import com.archibus.app.common.organization.domain.Employee;
import com.archibus.app.common.space.dao.datasource.*;
import com.archibus.app.common.space.domain.*;
import com.archibus.app.move.common.dao.*;
import com.archibus.app.move.common.domain.*;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.app.move.sql.MoveOrdersSqlOperator;
import com.archibus.core.dao.IDao;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMovePrjectService. Provides Move Project (group
 * move) related business operations.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr
 * methods about group move.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveProjectService implements IMoveProjectService {

	/**
	 * DAO for Jacks.
	 */
	private IJackDao jacksDao;

	/**
	 * DAO for Move Project.
	 */
	private IProjectDao moveProjectDao;

	/**
	 * DAO for Employee.
	 */
	private IDao<Employee> employeeDao;

	/**
	 * DAO for Equipment.
	 */
	private IEquipmentDao<Equipment> equipmentDao;

	/**
	 * DAO for Move Order.
	 */
	private IMoveOrderDao moveOrderDao;

	/**
	 * Inserter for Move Orders by sql.
	 */
	private MoveOrdersSqlOperator moveOrdersSqlOperator;

	// @translatable
	protected final static String errorMsgGroupEmExist = "Group move [{0}] already includes a move for Employee [{1}]";

	// @translatable
	protected final static String errorMsgDeptContactInvalid = "The department contact [{0}] is invalid";

	/**
	 * Logger for this class and subclasses.
	 */
	protected final Logger logger = Logger.getLogger(this.getClass());

	@Override
	public void addProjectMove(final DataRecord record) {

		final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

		MoveCommonUtil.commonLog("addProjectMove", "project_id: " + mo.getProjectId() + ".", this.logger);

		this.setCommonFields(mo);

		final Employee em = this.employeeDao.get(mo.getEmployeeId());

		if (em != null) {
			mo.setFromDivision(em.getDivisionId());
			mo.setFromDepartment(em.getDepartmentId());
			mo.setFromPhone(em.getPhone());
			mo.setFromFax(em.getFax());
			mo.setFromMailStop(em.getMailstop());
			mo.setToDivision(em.getDivisionId());
			mo.setToDepartment(em.getDepartmentId());
			mo.setToPhone(em.getPhone());
			mo.setToFax(em.getFax());
		}

		final Jacks jacks = this.jacksDao.getEmployeeJacks(mo.getEmployeeId());

		if (jacks != null) {
			mo.setFromJackData(jacks.getFromData());
			mo.setFromJackVoice(jacks.getFromVoice());
			if (StringUtil.notNullOrEmpty(jacks.getDescription())) {
				mo.setDescription(mo.getDescription() + "\n\n" + jacks.getDescription());
			}
		}

		this.saveGroupMoveOrder(mo, "addProjectMove");
	}

	@Override
	public void addProjectMoveNewHire(final DataRecord record) {

		final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

		this.setCommonFields(mo);

		this.saveGroupMoveOrder(mo, "addProjectMoveNewHire");
	}

	@Override
	public void addProjectMoveEmployeeLeaving(final DataRecord record) {

		final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

		this.setCommonFields(mo);

		final Employee em = this.employeeDao.get(mo.getEmployeeId());
		if (em != null) {
			mo.setFromDivision(em.getDivisionId());
			mo.setFromDepartment(em.getDepartmentId());
			mo.setFromPhone(em.getPhone());
			mo.setFromFax(em.getFax());
			mo.setFromMailStop(em.getMailstop());
		}

		final Jacks jacks = this.jacksDao.getEmployeeJacks(mo.getEmployeeId());
		if (jacks != null) {
			mo.setFromJackData(jacks.getFromData());
			mo.setFromJackVoice(jacks.getFromVoice());

			if (StringUtil.notNullOrEmpty(jacks.getDescription())) {
				mo.setDescription(mo.getDescription() + "\n\n" + jacks.getDescription());
			}
		}

		this.saveGroupMoveOrder(mo, "addProjectMoveEmployeeLeaving");
	}

	@Override
	public void addProjectMoveEquipment(final DataRecord record) {

		final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

		this.setCommonFields(mo);

		final Equipment eq = this.equipmentDao.get(mo.getEmployeeId());
		if (eq != null) {
			mo.setFromDivision(eq.getDivisionId());
			mo.setFromDepartment(eq.getDepartmentId());
			mo.setToDivision(eq.getDivisionId());
			mo.setToDepartment(eq.getDepartmentId());
		}

		this.saveGroupMoveOrder(mo, "addProjectMoveEquipment");
	}

	@Override
	public void addProjectMoveAsset(final DataRecord record) {

		final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

		this.setCommonFields(mo);

		this.saveGroupMoveOrder(mo, "addProjectMoveAsset");
	}

	@Override
	public void addProjectMoveRoom(final DataRecord record) {

		final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

		this.setCommonFields(mo);

		this.saveGroupMoveOrder(mo, "addProjectMoveRoom");
	}

	@Override
	public void addMoveProject(final DataRecord record) {

		MoveCommonUtil.commonLog("addMoveProjectRecord",
				"Adding project record: project_id: " + record.getString("project.project_id") + ".", this.logger);

		if (StringUtil.notNullOrEmpty(record.getString("project.dept_contact"))) {

			final Project project = new Project();

			project.setId(record.getString("project.project_id"));
			project.setDescription(record.getString("project.description"));
			project.setBuildingId(record.getString("project.bl_id"));
			project.setRequestor(record.getString("project.requestor"));
			project.setDepartmentContact(record.getString("project.dept_contact"));
			project.setContactId(record.getString("project.contact_id"));
			project.setDateStart(record.getDate("project.date_start"));
			project.setDateEnd(record.getDate("project.date_end"));
			project.setProjectType("Move");
			project.setDateCreated(DateUtils.getCurrentDateWithoutTime());

			final Employee requestor = this.employeeDao.get(project.getRequestor());
			project.setPhoneRequestor(requestor.getPhone());

			final Employee departmentContactor = this.employeeDao.get(project.getDepartmentContact());
			project.setPhoneDepartmentContact(departmentContactor.getPhone());
			project.setDivisionId(departmentContactor.getDivisionId());
			project.setDepartmentId(departmentContactor.getDepartmentId());

			final Building bl = new BuildingDataSource().get(project.getBuildingId());
			project.setSiteId(bl.getSiteId());

			this.moveProjectDao.save(project);

		} else {

			final Object[] args = { record.getString("project.dept_contact") };

			MoveCommonUtil.throwException(errorMsgGroupEmExist, args);

		}
	}

	@Override
	public void addBulkMoves(final DataRecord record) {

		MoveCommonUtil.commonLog("addBulkMoves",
				"Adding project record: project_id: " + record.getString("mo.project_id") + ".", this.logger);

		this.moveOrdersSqlOperator.insertForBulkMoves(record);
	}

	/**
	 * Save new group move order.
	 *
	 * @param moveOrder
	 *            move order object
	 * @param operation
	 *            indicate one of operations
	 *            'addProjectMove'/'addProjectMoveNewHire'/'
	 *            addProjectMoveEmployeeLeaving'/'addProjectMoveEquipment'/'
	 *            addProjectMoveAsset'/'addProjectMoveRoom'
	 */
	private void saveGroupMoveOrder(final MoveOrder mo, final String operation) {

		MoveCommonUtil.commonLog(operation, "project_id: " + mo.getProjectId() + ", em_id: " + mo.getEmployeeId() + ".",
				this.logger);

		if (!this.groupMoveExist(mo.getProjectId(), mo.getEmployeeId())) {

			final MoveOrder insertedMo = this.moveOrderDao.save(mo);
			final int moveOrderId = insertedMo.getId();

			MoveCommonUtil.addPrimaryKeyValueToResponse(moveOrderId, "mo", "mo_id");

		} else {

			final Object[] args = { mo.getProjectId(), mo.getEmployeeId() };

			MoveCommonUtil.throwException(errorMsgGroupEmExist, args);
		}
	}

	/**
	 * Set Common Fields value for move order from project.
	 *
	 * @param mo
	 */
	private void setCommonFields(final MoveOrder mo) {

		final Project project = this.moveProjectDao.get(mo.getProjectId());

		mo.setDatePerform(mo.getDateStart());
		mo.setDepartmentContact(project.getDepartmentContact());
		mo.setPhone(project.getPhoneRequestor());
		mo.setPhoneDeptContact(project.getPhoneDepartmentContact());
		mo.setDivision(project.getDivisionId());
		mo.setDepartment(project.getDepartmentId());
		mo.setAccountId(project.getAccountId());
		mo.setQuestionnarie(MoveCommonUtil.getDefaultQuestionnaireValues("Move Order - " + mo.getMoveType()));
	}

	/**
	 * Determine if the move project already exists for given project id and
	 * employee id.
	 *
	 * @param projectId
	 * @param emId
	 */
	private boolean groupMoveExist(final String projectId, final String emId) {

		final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();

		restrictionDef.addClause("mo", "project_id", projectId, Operation.EQUALS);
		restrictionDef.addClause("mo", "em_id", emId, Operation.EQUALS);

		final List<MoveOrder> founded = this.moveOrderDao.find(restrictionDef);

		return !founded.isEmpty();
	}

	/**
	 * Getter for the moveOrdersSqlOperator property.
	 *
	 * @return the moveOrdersSqlOperator property.
	 */
	public MoveOrdersSqlOperator getMoveOrdersSqlOperator() {
		return this.moveOrdersSqlOperator;
	}

	/**
	 * Setter for the moveOrdersSqlOperator property.
	 *
	 * @param moveOrdersSqlOperator
	 *            the moveOrdersSqlOperator to set.
	 */
	public void setMoveOrdersSqlOperator(final MoveOrdersSqlOperator moveOrdersSqlOperator) {
		this.moveOrdersSqlOperator = moveOrdersSqlOperator;
	}

	/**
	 * Getter for the jacksDao property.
	 *
	 * @return the jacksDao property.
	 */
	public IJackDao getJacksDao() {
		return this.jacksDao;
	}

	/**
	 * Setter for the jacksDao property.
	 *
	 * @param jacksDao
	 *            the jacksDao to set.
	 */
	public void setJacksDao(final IJackDao jacksDao) {
		this.jacksDao = jacksDao;
	}

	/**
	 * Getter for the moveProjectDao property.
	 *
	 * @return the moveProjectDao property.
	 */
	public IProjectDao getMoveProjectDao() {
		return this.moveProjectDao;
	}

	/**
	 * Setter for the moveProjectDao property.
	 *
	 * @param moveProjectDao
	 *            the moveProjectDao to set.
	 */
	public void setMoveProjectDao(final IProjectDao moveProjectDao) {
		this.moveProjectDao = moveProjectDao;
	}

	/**
	 * Getter for the employeeDao property.
	 *
	 * @return the employeeDao property.
	 */
	public IDao<Employee> getEmployeeDao() {
		return this.employeeDao;
	}

	/**
	 * Setter for the employeeDao property.
	 *
	 * @param employeeDao
	 *            the employeeDao to set.
	 */
	public void setEmployeeDao(final IDao<Employee> employeeDao) {
		this.employeeDao = employeeDao;
	}

	/**
	 * Getter for the equipmentDao property.
	 *
	 * @return the equipmentDao property.
	 */
	public IEquipmentDao<Equipment> getEquipmentDao() {
		return this.equipmentDao;
	}

	/**
	 * Setter for the equipmentDao property.
	 *
	 * @param equipmentDao
	 *            the equipmentDao to set.
	 */
	public void setEquipmentDao(final IEquipmentDao<Equipment> equipmentDao) {
		this.equipmentDao = equipmentDao;
	}

	/**
	 * Getter for the moveOrderDao property.
	 *
	 * @return the moveOrderDao property.
	 */
	public IMoveOrderDao getMoveOrderDao() {
		return this.moveOrderDao;
	}

	/**
	 * Setter for the moveOrderDao property.
	 *
	 * @param moveOrderDao
	 *            the moveOrderDao to set.
	 */
	public void setMoveOrderDao(final IMoveOrderDao moveOrderDao) {
		this.moveOrderDao = moveOrderDao;
	}

}
