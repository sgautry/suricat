package com.archibus.app.move.report;

import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.eventhandler.Moves.MovePaginatedReportGenerator;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveReportService. Provides methods on report
 * for Move Management.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr
 * methods about move reports.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveReportService implements IMoveReportService {

	/**
	 * Move Paginate Report Generation Job.
	 */
	private MoveCommonUtil moveUtil;

	/**
	 * Getter for the moveUtil property.
	 *
	 * @return the moveUtil property.
	 */
	public MoveCommonUtil getMoveUtil() {
		return this.moveUtil;
	}

	/**
	 * Setter for the moveUtil property.
	 *
	 * @param moveUtil
	 *            the moveUtil to set.
	 */
	public void setMoveUtil(final MoveCommonUtil moveUtil) {
		this.moveUtil = moveUtil;
	}

	@Override
	public void generatePaginateReport(final String rptType, final String projectId, final String selectedId) {

		final MovePaginatedReportGenerator moPaginatedReportGenerator = new MovePaginatedReportGenerator(rptType,
				projectId, selectedId);

		this.moveUtil.startJob(moPaginatedReportGenerator);

	}

}
