package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Furniture Standard.
 * <p>
 * Mapped to fnstd table.
 *
 * @author Zhang Yi
 *
 */
public class FurnitureStandard {

	/**
	 * Furniture Standard code.
	 */
	private String furnitureStandard;

	/**
	 * Move cost.
	 */
	private double costMoving;

	/**
	 * Getter for the furnitureStandard property.
	 *
	 * @return the furnitureStandard property.
	 */
	public String getFurnitureStandard() {
		return this.furnitureStandard;
	}

	/**
	 * Setter for the furnitureStandard property.
	 *
	 * @param furnitureStandard
	 *            the furnitureStandard to set.
	 */
	public void setFurnitureStandard(final String furnitureStandard) {
		this.furnitureStandard = furnitureStandard;
	}

	/**
	 * Getter for the costMoving property.
	 *
	 * @return the costMoving property.
	 */
	public double getCostMoving() {
		return this.costMoving;
	}

	/**
	 * Setter for the costMoving property.
	 *
	 * @param costMoving
	 *            the costMoving to set.
	 */
	public void setCostMoving(final double costMoving) {
		this.costMoving = costMoving;
	}

}
