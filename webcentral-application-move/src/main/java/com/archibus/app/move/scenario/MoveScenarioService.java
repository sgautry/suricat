package com.archibus.app.move.scenario;

import java.util.*;

import com.archibus.app.move.common.dao.IMoveScenarioDao;
import com.archibus.app.move.common.domain.MoveScenario;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.app.move.sql.MoveScenarioSqlOperator;
import com.archibus.datasource.DataStatistics;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.ExceptionBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveScenarioService. Provides Move scenario
 * related business operations.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr
 * methods about move scenario.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveScenarioService implements IMoveScenarioService {

	/**
	 * DAO for Move Scenario .
	 */
	private IMoveScenarioDao moveScenarioDao;

	// @translatable
	protected final static String errorMsgScenarioExist = "Scenario [{0}] for Project [{1}] already exists";

	/**
	 * Operator for Move Scenario by sql.
	 */
	private MoveScenarioSqlOperator moveScenarioSqlOperator;

	@Override
	public String createMoveScenario(final DataRecord record, final boolean isDefault) throws ExceptionBase {

		final MoveScenario moveScenario = new MoveScenario();

		String scenarioId = record.getString("mo_scenario.scenario_id");
		final String projectId = record.getString("mo_scenario.project_id");

		moveScenario.setProjectId(projectId);

		final int scenariosCount = DataStatistics.getInt("mo_scenario", "scenario_id", "count",
				"project_id=" + MoveCommonUtil.getLiteralString(projectId));

		if (isDefault && scenariosCount == 0) {

			scenarioId = "Scenario 1";
			moveScenario.setId(scenarioId);
			moveScenario.setDescription(scenarioId);

			this.saveMoveScenario(moveScenario);

			this.moveScenarioSqlOperator.insertMoveScenarioEm(scenarioId, projectId);
			this.moveScenarioSqlOperator.insertMoveScenarioTeam(scenarioId, projectId);

		} else if (!isDefault) {

			moveScenario.setId(scenarioId);
			this.saveMoveScenario(moveScenario);

			this.moveScenarioSqlOperator.insertMoveScenarioEm(scenarioId, projectId);
			this.moveScenarioSqlOperator.insertMoveScenarioTeam(scenarioId, projectId);
		}

		return scenarioId;
	}

	@Override
	public void copyMoveScenario(final DataRecord record, final String origScenarioId) throws ExceptionBase {

		final String scenarioId = record.getString("mo_scenario.scenario_id");
		final String projectId = record.getString("mo_scenario.project_id");

		final MoveScenario moveScenario = new MoveScenario();
		moveScenario.setId(scenarioId);
		moveScenario.setProjectId(projectId);

		this.saveMoveScenario(moveScenario);

		this.moveScenarioSqlOperator.insertForCopyScenario(origScenarioId, scenarioId, projectId);
	}

	@Override
	public void deleteMoveScenario(final String scenarioId, final String projectId) {

		this.moveScenarioSqlOperator.deleteScenario(scenarioId, projectId);
	}

	@Override
	public void updateMoveProject(final String projectId, final String scenarioId) {

		this.moveScenarioSqlOperator.updateForMoveProject(projectId, scenarioId);

	}

	@Override
	public void updateMoveScenario(final String projectId, final String scenarioId,
			final List<Map<String, Object>> records) {

		this.synchronizeScenario(projectId, scenarioId);
	}

	@Override
	public void synchronizeScenario(final String projectId, final String scenarioId) {

		this.moveScenarioSqlOperator.deleteForSynchronizeScenario(scenarioId, projectId);

		this.moveScenarioSqlOperator.insertForSynchronizeScenario(scenarioId, projectId);

		this.moveScenarioSqlOperator.updateFilenameForSynchronizeScenario(scenarioId, projectId);
	}

	/**
	 * Save a new move scenario
	 *
	 * @param moveScenario
	 *
	 */
	private void saveMoveScenario(final MoveScenario moveScenario) throws ExceptionBase {
		/*
		 * check the existence of the scenario to insert, for displaying - if
		 * it's the case - of a personalized error message
		 */
		final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
		restrictionDef.addClause("mo_scenario", "project_id", moveScenario.getProjectId(), Operation.EQUALS);
		restrictionDef.addClause("mo_scenario", "scenario_id", moveScenario.getId(), Operation.EQUALS);

		final List<MoveScenario> existingScenarios = this.moveScenarioDao.find(restrictionDef);
		if (existingScenarios.size() > 0) {

			final ExceptionBase exception = new ExceptionBase();
			exception.setPattern(errorMsgScenarioExist);
			exception.setTranslatable(true);

			final Object[] args = { moveScenario.getId(), moveScenario.getProjectId() };
			exception.setArgs(args);

			throw exception;
		}

		moveScenario.setDateCreated(new Date());

		this.moveScenarioDao.save(moveScenario);
	}

	/**
	 * Getter for the moveScenarioDao property.
	 *
	 * @return the moveScenarioDao property.
	 */
	public IMoveScenarioDao getMoveScenarioDao() {
		return this.moveScenarioDao;
	}

	/**
	 * Setter for the moveScenarioDao property.
	 *
	 * @param moveScenarioDao
	 *            the moveScenarioDao to set.
	 */
	public void setMoveScenarioDao(final IMoveScenarioDao moveScenarioDao) {
		this.moveScenarioDao = moveScenarioDao;
	}

	/**
	 * Getter for the moveScenarioSqlDeleter property.
	 *
	 * @return the moveScenarioSqlDeleter property.
	 */
	public MoveScenarioSqlOperator getMoveScenarioSqlOperator() {
		return this.moveScenarioSqlOperator;
	}

	/**
	 * Setter for the moveScenarioSqlDeleter property.
	 *
	 * @param moveScenarioSqlOperator
	 *            the moveScenarioSqlOperator to set.
	 */
	public void setMoveScenarioSqlOperator(final MoveScenarioSqlOperator moveScenarioSqlOperator) {
		this.moveScenarioSqlOperator = moveScenarioSqlOperator;
	}

	@Override
	public void copyScenarioOccupancy(final String projectId, final String scenarioId, final String filename,
			final String toBlId, final String toFlId, final String occupanyScenariofilename) throws ExceptionBase {
		this.moveScenarioSqlOperator.copyScenarioOccupancy(projectId, scenarioId, filename, toBlId, toFlId,
				occupanyScenariofilename);

	}

	@Override
	public void deleteSelectedTeams(final String teams, final String projectId) {
		this.moveScenarioSqlOperator.deleteSelectedTeams(teams, projectId);
	}

}
