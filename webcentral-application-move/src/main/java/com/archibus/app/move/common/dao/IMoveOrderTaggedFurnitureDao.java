package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.MoveOrderTaggedFurniture;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Move Order Tagged Furniture.
 *
 * Interface to be implemented by MoveOrderTaggedFurnitureDataSource.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveOrderTaggedFurnitureDao extends IDao<MoveOrderTaggedFurniture> {

}
