package com.archibus.app.move.order;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.archibus.app.common.depreciation.dao.*;
import com.archibus.app.common.depreciation.domain.*;
import com.archibus.app.common.organization.domain.Employee;
import com.archibus.app.move.common.dao.*;
import com.archibus.app.move.common.domain.*;
import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.app.move.sql.MoveOrdersSqlOperator;
import com.archibus.core.dao.IDao;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveOrderService. Provides Move Order (individual move) related
 * business operations.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr methods about
 * individual move.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public class MoveOrderService implements IMoveOrderService {

    /**
     * DAO for Jacks.
     */
    private IJackDao jacksDao;

    /**
     * DAO for Move Order.
     */
    private IMoveOrderDao moveOrderDao;

    /**
     * DAO for Employee.
     */
    private IDao<Employee> employeeDao;

    /**
     * Getter for the employeeDao property.
     *
     * @return the employeeDao property.
     */
    public IDao<Employee> getEmployeeDao() {
        return this.employeeDao;
    }

    /**
     * Setter for the employeeDao property.
     *
     * @param employeeDao the employeeDao to set.
     */
    public void setEmployeeDao(final IDao<Employee> employeeDao) {
        this.employeeDao = employeeDao;
    }

    /**
     * DAO for Equipment.
     */
    private IEquipmentDao equipmentDao;

    /**
     * DAO for Furniture.
     */
    private IFurnitureDao furnitureDao;

    /**
     * DAO for Equipment Standard.
     */
    private IEquipmentStandardDao equipmentStandardDao;

    /**
     * DAO for Furniture Standard.
     */
    private IFurnitureStandardDao furnitureStandardDao;

    /**
     * DAO for Move Equipment.
     */
    private IMoveOrderEquipmentDao moveOrderEquipmentDao;

    /**
     * DAO for Move Order Tagged Furniture.
     */
    private IMoveOrderTaggedFurnitureDao moveOrderTaggedFurnitureDao;

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    // @translatable
    protected final static String errorMsgDeptContactInvalid =
            "The department contact [{0}] is invalid";

    /**
     * Operator for Move Scenario by sql.
     */
    private MoveOrdersSqlOperator moveOrdersSqlOperator;

    @Override
    public void addIndividualMove(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);
        final Employee em = this.employeeDao.get(mo.getEmployeeId());
        final Jacks jacks = this.jacksDao.getEmployeeJacks(mo.getEmployeeId());

        this.setCommonFields(mo);
        this.setFromLocationAndOrganazationFields(mo, em);
        this.setFromCommunicationFields(mo, em, jacks);

        if (em != null) {
            mo.setToDivision(em.getDivisionId());
            mo.setToDepartment(em.getDepartmentId());
            mo.setToFax(em.getFax());
            mo.setToPhone(em.getPhone());
        }

        this.saveMoveOrder(mo, "addIndividualMove", addEq, addTa);
    }

    @Override
    public void addIndividualMoveNewHire(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

        this.setCommonFields(mo);

        this.saveMoveOrder(mo, "addIndividualMoveNewHire", addEq, addTa);
    }

    @Override
    public void addIndividualMoveEmployeeLeaving(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);
        final Employee em = this.employeeDao.get(mo.getEmployeeId());
        final Jacks jacks = this.jacksDao.getEmployeeJacks(mo.getEmployeeId());

        this.setCommonFields(mo);
        this.setFromLocationAndOrganazationFields(mo, em);
        this.setFromCommunicationFields(mo, em, jacks);

        this.saveMoveOrder(mo, "addIndividualMoveEmployeeLeaving", addEq, addTa);

    }

    @Override
    public void addIndividualMoveAsset(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

        this.setCommonFields(mo);

        this.saveMoveOrder(mo, "addIndividualMoveAsset", false, false);
    }

    @Override
    public void addIndividualMoveEquipment(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

        final Equipment eq = (Equipment) this.equipmentDao.get(mo.getEmployeeId());
        this.employeeDao.get(mo.getDepartmentContact());

        this.setCommonFields(mo);
        this.setFromLocationAndOrganazationFields(mo, eq);

        if (eq != null) {
            mo.setToDivision(eq.getDivisionId());
            mo.setToDepartment(eq.getDepartmentId());
        }

        this.saveMoveOrder(mo, "addIndividualMoveEquipment", false, false);
    }

    @Override
    public void addIndividualMoveRoom(final DataRecord record, final boolean addEq,
            final boolean addTa) {

        final MoveOrder mo = this.moveOrderDao.getMoveOrderByRecord(record);

        this.setCommonFields(mo);

        this.saveMoveOrder(mo, "addIndividualMoveRoom", addEq, addTa);

    }

    /**
     * Set Common Fields value for new move order.
     *
     * @param mo
     * @param requestor
     * @param departmentContact
     */
    private void setCommonFields(final MoveOrder mo) {

        final Employee requestor = this.employeeDao.get(mo.getRequestor());
        final Employee departmentContact = this.employeeDao.get(mo.getDepartmentContact());

        mo.setDatePerform(mo.getDateStart());
        mo.setPhone(requestor.getPhone());
        mo.setPhoneDeptContact(departmentContact.getPhone());
        mo.setDivision(departmentContact.getDivisionId());
        mo.setDepartment(departmentContact.getDepartmentId());
        mo.setQuestionnarie(
            MoveCommonUtil.getDefaultQuestionnaireValues("Move Order - " + mo.getMoveType()));
    }

    /**
     * Set From Location And Organization Fields value for new move order from employee.
     *
     * @param mo
     * @param em
     */
    private void setFromLocationAndOrganazationFields(final MoveOrder mo, final Employee em) {
        if (em != null) {
            mo.setFromBuilding(em.getBuildingId());
            mo.setFromFloor(em.getFloorId());
            mo.setFromRoom(em.getRoomId());
            mo.setFromDivision(em.getDivisionId());
            mo.setFromDepartment(em.getDepartmentId());
        }
    }

    /**
     * Set From Location And Organization Fields value for new move order from equipment.
     *
     * @param mo
     * @param em
     */
    private void setFromLocationAndOrganazationFields(final MoveOrder mo, final Equipment eq) {
        if (eq != null) {
            mo.setFromBuilding(eq.getBuildingId());
            mo.setFromFloor(eq.getFloorId());
            mo.setFromRoom(eq.getRoomId());
            mo.setFromDivision(eq.getDivisionId());
            mo.setFromDepartment(eq.getDepartmentId());
        }
    }

    /**
     * Set From Communication Fields value for new move order from employee and jacks.
     *
     * @param mo
     * @param em
     * @param jacks
     */
    private void setFromCommunicationFields(final MoveOrder mo, final Employee em,
            final Jacks jacks) {
        if (em != null) {
            mo.setFromPhone(em.getPhone());
            mo.setFromFax(em.getFax());
            mo.setFromMailStop(em.getMailstop());
        }
        if (jacks != null) {
            mo.setFromJackData(jacks.getFromData());
            mo.setFromJackVoice(jacks.getFromVoice());

            if (StringUtil.notNullOrEmpty(jacks.getDescription())) {
                mo.setDescription(mo.getDescription() + "\n\n" + jacks.getDescription());
            }
        }
    }

    /**
     * Save the new move order.
     *
     * @param mo
     * @param operation
     * @param addEq
     * @param addTa
     */
    private void saveMoveOrder(final MoveOrder mo, final String operation, final boolean addEq,
            final boolean addTa) {

        MoveCommonUtil.commonLog(operation, "em_id: " + mo.getEmployeeId() + ".", this.logger);

        final Employee departmentContact = this.employeeDao.get(mo.getDepartmentContact());
        if (departmentContact != null) {

            final MoveOrder insertedMo = this.moveOrderDao.save(mo);
            final int moveOrderId = insertedMo.getId();

            // add the equipments and tagged furniture
            this.addEquipmentsToMove(addEq, this.moveOrderDao.get(moveOrderId), mo.getEmployeeId());
            this.addTaggedFurnitureToMove(addTa, this.moveOrderDao.get(moveOrderId),
                mo.getEmployeeId());

            MoveCommonUtil.addPrimaryKeyValueToResponse(moveOrderId, "mo", "mo_id");

        } else {

            final Object[] args = { departmentContact };

            MoveCommonUtil.throwException(errorMsgDeptContactInvalid, args);
        }
    }

    /**
     * Adds employee's / employee leaving' / new hire's / room's equipments to the move. For
     * employee, employee leaving and room: blId, flId and rmId are taken from the move. For new
     * hire: blId, flId and rmId are those are taken from the equipment.
     *
     * @param addEq Add equipments ? true/false
     * @param moveOrder move order object
     * @param emId employee ID; if null, the equipments of the room will be added instead of those
     *            of the employee
     */
    private void addEquipmentsToMove(final boolean addEq, final MoveOrder moveOrder,
            final String emId) {

        if (moveOrder.getId() <= 0) {
            return;
        }

        if (addEq) {

            for (final Equipment equipment : this.getEquipmentsByMoveOrder(moveOrder)) {

                final MoveOrderEquipment moEq = new MoveOrderEquipment();

                moEq.setMoveOrderId(moveOrder.getId());
                moEq.setEquipmentId(equipment.getEqId());

                moEq.setFromBuildingId(StringUtil.isNullOrEmpty(equipment.getBuildingId())
                        ? moveOrder.getFromBuilding() : equipment.getBuildingId());
                moEq.setFromFloorId(StringUtil.isNullOrEmpty(equipment.getFloorId())
                        ? moveOrder.getFromFloor() : equipment.getFloorId());
                moEq.setFromRoomId(StringUtil.isNullOrEmpty(equipment.getRoomId())
                        ? moveOrder.getFromRoom() : equipment.getRoomId());

                moEq.setEquipmentStandard(equipment.getEqStd());
                moEq.setCostMoving(
                    this.equipmentStandardDao.get(equipment.getEqStd()).getCostMoving());

                this.moveOrderEquipmentDao.save(moEq);
            }
        }
    }

    /**
     * Adds employee's / employee leaving' / new hire's / room's tagged furniture to the move. For
     * employee, employee leaving and room: blId, flId and rmId are taken from the move. For new
     * hire: blId, flId and rmId are those are taken from the tagged furniture.
     *
     * @param addTa Add tagged furniture ? true/false
     * @param moveOrder move order object
     * @param emId employee ID; if null, the tagged furniture of the room will be added instead of
     *            those of the employee
     */
    private void addTaggedFurnitureToMove(final boolean addTa, final MoveOrder moveOrder,
            final String emId) {

        if (moveOrder.getId() <= 0) {
            return;
        }

        if (addTa) {

            for (final Furniture furniture : this.getTaggedFurnituresByMoveOrder(moveOrder)) {

                final MoveOrderTaggedFurniture moTa = new MoveOrderTaggedFurniture();

                moTa.setMoveOrderId(moveOrder.getId());
                moTa.setTaggedFurnitureId(furniture.getTaId());

                moTa.setFromBuildingId(StringUtil.isNullOrEmpty(furniture.getBuildingId())
                        ? moveOrder.getFromBuilding() : furniture.getBuildingId());
                moTa.setFromFloorId(StringUtil.isNullOrEmpty(furniture.getFloorId())
                        ? moveOrder.getFromFloor() : furniture.getFloorId());
                moTa.setFromRoomId(StringUtil.isNullOrEmpty(furniture.getRoomId())
                        ? moveOrder.getFromRoom() : furniture.getRoomId());

                moTa.setFurnitureStandard(furniture.getFnStd());
                moTa.setCostMoving(
                    this.furnitureStandardDao.get(furniture.getFnStd()).getCostMoving());

                this.moveOrderTaggedFurnitureDao.save(moTa);
            }
        }
    }

    /**
     * Returns a list of Equipments by given move order.
     *
     * @param moveOrder MoveOrder object
     */
    private List<Equipment> getEquipmentsByMoveOrder(final MoveOrder moveOrder) {

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();

        if (StringUtil.notNullOrEmpty(moveOrder.getEmployeeId())
                && !moveOrder.getMoveType().equalsIgnoreCase("Room")) {

            restrictionDef.addClause("eq", "em_id", moveOrder.getEmployeeId(), Operation.EQUALS);

        } else {

            restrictionDef.addClause("eq", "bl_id", moveOrder.getFromBuilding(), Operation.EQUALS);
            restrictionDef.addClause("eq", "fl_id", moveOrder.getFromFloor(), Operation.EQUALS);
            restrictionDef.addClause("eq", "rm_id", moveOrder.getFromRoom(), Operation.EQUALS);

        }

        return this.equipmentDao.getEquipments(restrictionDef);
    }

    /**
     * Returns a list of Tagged Furniture by given move order.
     *
     * @param moveOrder MoveOrder object
     */
    private List<Furniture> getTaggedFurnituresByMoveOrder(final MoveOrder moveOrder) {

        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();

        if (StringUtil.notNullOrEmpty(moveOrder.getEmployeeId())
                && !moveOrder.getMoveType().equalsIgnoreCase("Room")) {

            restrictionDef.addClause("ta", "em_id", moveOrder.getEmployeeId(), Operation.EQUALS);

        } else {

            restrictionDef.addClause("ta", "bl_id", moveOrder.getFromBuilding(), Operation.EQUALS);
            restrictionDef.addClause("ta", "fl_id", moveOrder.getFromFloor(), Operation.EQUALS);
            restrictionDef.addClause("ta", "rm_id", moveOrder.getFromRoom(), Operation.EQUALS);

        }

        return this.furnitureDao.getFurnitures(restrictionDef);
    }

    /**
     * Getter for the jacksDao property.
     *
     * @return the jacksDao property.
     */
    public IJackDao getJacksDao() {
        return this.jacksDao;
    }

    /**
     * Setter for the jacksDao property.
     *
     * @param jacksDao the jacksDao to set.
     */
    public void setJacksDao(final IJackDao jacksDao) {
        this.jacksDao = jacksDao;
    }

    /**
     * Getter for the moveOrderDao property.
     *
     * @return the moveOrderDao property.
     */
    public IMoveOrderDao getMoveOrderDao() {
        return this.moveOrderDao;
    }

    /**
     * Setter for the moveOrderDao property.
     *
     * @param moveOrderDao the moveOrderDao to set.
     */
    public void setMoveOrderDao(final IMoveOrderDao moveOrderDao) {
        this.moveOrderDao = moveOrderDao;
    }

    /**
     * Getter for the equipmentDao property.
     *
     * @return the equipmentDao property.
     */
    public IEquipmentDao getEquipmentDao() {
        return this.equipmentDao;
    }

    /**
     * Setter for the equipmentDao property.
     *
     * @param equipmentDao the equipmentDao to set.
     */
    public void setEquipmentDao(final IEquipmentDao equipmentDao) {
        this.equipmentDao = equipmentDao;
    }

    /**
     * Getter for the furnitureDao property.
     *
     * @return the furnitureDao property.
     */
    public IFurnitureDao getFurnitureDao() {
        return this.furnitureDao;
    }

    /**
     * Setter for the furnitureDao property.
     *
     * @param furnitureDao the furnitureDao to set.
     */
    public void setFurnitureDao(final IFurnitureDao furnitureDao) {
        this.furnitureDao = furnitureDao;
    }

    /**
     * Getter for the equipmentStandardDao property.
     *
     * @return the equipmentStandardDao property.
     */
    public IEquipmentStandardDao getEquipmentStandardDao() {
        return this.equipmentStandardDao;
    }

    /**
     * Setter for the equipmentStandardDao property.
     *
     * @param equipmentStandardDao the equipmentStandardDao to set.
     */
    public void setEquipmentStandardDao(final IEquipmentStandardDao equipmentStandardDao) {
        this.equipmentStandardDao = equipmentStandardDao;
    }

    /**
     * Getter for the furnitureStandardDao property.
     *
     * @return the furnitureStandardDao property.
     */
    public IFurnitureStandardDao getFurnitureStandardDao() {
        return this.furnitureStandardDao;
    }

    /**
     * Setter for the furnitureStandardDao property.
     *
     * @param furnitureStandardDao the furnitureStandardDao to set.
     */
    public void setFurnitureStandardDao(final IFurnitureStandardDao furnitureStandardDao) {
        this.furnitureStandardDao = furnitureStandardDao;
    }

    /**
     * Getter for the moveOrderEquipmentDao property.
     *
     * @return the moveOrderEquipmentDao property.
     */
    public IMoveOrderEquipmentDao getMoveOrderEquipmentDao() {
        return this.moveOrderEquipmentDao;
    }

    /**
     * Setter for the moveOrderEquipmentDao property.
     *
     * @param moveOrderEquipmentDao the moveOrderEquipmentDao to set.
     */
    public void setMoveOrderEquipmentDao(final IMoveOrderEquipmentDao moveOrderEquipmentDao) {
        this.moveOrderEquipmentDao = moveOrderEquipmentDao;
    }

    /**
     * Getter for the moveOrderTaggedFurnitureDao property.
     *
     * @return the moveOrderTaggedFurnitureDao property.
     */
    public IMoveOrderTaggedFurnitureDao getMoveOrderTaggedFurnitureDao() {
        return this.moveOrderTaggedFurnitureDao;
    }

    /**
     * Setter for the moveOrderTaggedFurnitureDao property.
     *
     * @param moveOrderTaggedFurnitureDao the moveOrderTaggedFurnitureDao to set.
     */
    public void setMoveOrderTaggedFurnitureDao(
            final IMoveOrderTaggedFurnitureDao moveOrderTaggedFurnitureDao) {
        this.moveOrderTaggedFurnitureDao = moveOrderTaggedFurnitureDao;
    }

    @Override
    public void assignEmployeesToTeam(final List<String> emIds, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrderDao.assignEmployeesToTeam(emIds, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

    @Override
    public void assignRoomsToTeam(final JSONArray rooms, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrderDao.assignRoomsToTeam(rooms, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

    /**
     * Setter for the moveOrdersSqlOperator property.
     *
     * @param moveOrdersSqlOperator the moveOrdersSqlOperator to set.
     */
    public void setMoveOrdersSqlOperator(final MoveOrdersSqlOperator moveOrdersSqlOperator) {
        this.moveOrdersSqlOperator = moveOrdersSqlOperator;
    }

    @Override
    public void removeEmployeesFromTeam(final List<String> emIds, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrdersSqlOperator.removeEmployeesFromTeam(emIds, teamId, projectId, type,
            dateStartReq, dateToPerform);
    }

    @Override
    public void removeRoomsFromTeam(final JSONArray rooms, final String teamId,
            final String projectId, final String type, final String dateStartReq,
            final String dateToPerform) {

        this.moveOrdersSqlOperator.removeRoomsFromTeam(rooms, teamId, projectId, type, dateStartReq,
            dateToPerform);
    }

}
