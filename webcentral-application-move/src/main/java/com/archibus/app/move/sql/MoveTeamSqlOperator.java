package com.archibus.app.move.sql;

import com.archibus.datasource.SqlUtils;
import com.archibus.utility.Utility;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides SQL operation methods on Move Scenarios.
 * <p>
 *
 * Used by Move Management Services.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file. *
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveTeamSqlOperator {

    /**
     * mill minutes of one day.
     *
     */
    private static final int A_DAY = 24 * 60 * 60 * 1000;

    /**
     * Table "team".
     *
     */
    private static final String TEAM = "team";

    /**
     * Table "rm_team".
     *
     */
    private static final String RM_TEAM = "rm_team";

    /**
     * This method fires upon closing Move Orders to include logic to accommodate employees moving
     * to and from teams.
     *
     * @param moRestriction move order restriction
     */
    public void updateForCLoseMove(final String moRestriction) {

        final StringBuilder dateEnd = new StringBuilder();
        final StringBuilder dateStart = new StringBuilder();
        final java.sql.Date currentDate = (java.sql.Date) Utility.currentDate().clone();
        currentDate.setTime(currentDate.getTime() - A_DAY);

        if (SqlUtils.isOracle()) {

            dateEnd.append(" TO_DATE('").append(currentDate.toString()).append("','YYYY-MM-DD') ");

        } else {

            dateEnd.append(" '").append(currentDate.toString()).append("' ");
        }

        if (SqlUtils.isOracle()) {

            dateStart.append("TO_DATE('").append(Utility.currentDate().toString())
                .append("','YYYY-MM-DD')");

        } else {

            dateStart.append("  '").append(Utility.currentDate().toString()).append("'");
        }

        this.updateForMoveEmployeeFromTeam(moRestriction, dateEnd);

        this.insertForMoveEmployeeToTeam(moRestriction, dateStart);

        this.updateForMoveTeamFromRoom(moRestriction, dateEnd);

        this.insertForMoveTeamToRoom(moRestriction, dateStart);

    }

    /**
     * If an employee moves from a team, then the system must make an appropriate transaction in the
     * team table, removing the employee from the team on the date before the current date (<current
     * date> - 1). .
     *
     * @param moRestriction move order restriction
     * @param dateEnd date end represents current date -1
     * @return
     */
    private void updateForMoveEmployeeFromTeam(final String moRestriction,
            final StringBuilder dateEnd) {
        final StringBuilder updateTeamSql = new StringBuilder();

        updateTeamSql.append("UPDATE team set team.date_end=").append(dateEnd.toString());
        updateTeamSql.append("  where ");
        updateTeamSql.append("  exists( ");
        updateTeamSql.append(" 	select 1  from mo  ");
        updateTeamSql
            .append(" 		 	where  team.team_id=mo.from_team_id and mo.em_id = team.em_id ");
        updateTeamSql
            .append(" 			AND mo.em_id is not null and mo.from_team_id is not null and ");
        updateTeamSql.append(moRestriction);
        updateTeamSql.append(")");

        SqlUtils.executeUpdate(TEAM, updateTeamSql.toString());
    }

    /**
     * If an employee moves to a team, then the system must make an appropriate transaction in the
     * team table, adding the employee to the team on the current date.
     *
     * @param moRestriction move order restriction
     * @param dateStart date start represents current date
     * @return
     */
    private void insertForMoveEmployeeToTeam(final String moRestriction,
            final StringBuilder dateStart) {

        final StringBuilder insertTeamSql = new StringBuilder();

        insertTeamSql.append("INSERT INTO team(em_id, team_id, date_start)");
        insertTeamSql.append(" select mo.em_id, mo.to_team_id, ");
        insertTeamSql.append(dateStart.toString());
        insertTeamSql.append(" from mo  ");
        insertTeamSql.append(" where ");
        insertTeamSql.append(moRestriction);
        insertTeamSql.append(" AND  not exists( ");
        insertTeamSql.append(" 	 select 1  from team  ");
        insertTeamSql
            .append(" 		 	where  team.team_id=mo.to_team_id and mo.em_id = team.em_id )");
        insertTeamSql.append(" AND mo.em_id is not null and mo.to_team_id is not null ");

        SqlUtils.executeUpdate(TEAM, insertTeamSql.toString());

    }

    /**
     * If a team moves from a location, then the system must make an appropriate transaction in the
     * rm_team table, end the team's assignment to the location on the date before the current date.
     *
     * @param moRestriction move order restriction
     * @param dateEnd date end represents current date -1
     * @return
     */
    private void updateForMoveTeamFromRoom(final String moRestriction,
            final StringBuilder dateEnd) {

        final StringBuilder updateRoomTeamSql = new StringBuilder();

        updateRoomTeamSql.append("UPDATE rm_team set rm_team.date_end=").append(dateEnd.toString());
        updateRoomTeamSql.append("where");
        updateRoomTeamSql.append("  exists( ");
        updateRoomTeamSql.append(" 	 select 1  from  mo  ");
        updateRoomTeamSql.append(" 		 	where   rm_team.team_id=mo.to_team_id ");
        updateRoomTeamSql.append(
            "							and rm_team.bl_id=mo.from_bl_id and rm_team.fl_id=mo.from_fl_id and rm_team.rm_id=mo.from_rm_id");
        updateRoomTeamSql.append(
            " 			AND mo.em_id is null and mo.to_team_id is not null and mo.from_rm_id is not null and ");
        updateRoomTeamSql.append(moRestriction);
        updateRoomTeamSql.append(")");

        SqlUtils.executeUpdate(RM_TEAM, updateRoomTeamSql.toString());
    }

    /**
     * If a team moves to a location, then the system must make an appropriate transaction in the
     * rm_team table, adding the team assignment to the new location on the current date.
     *
     * @param moRestriction move order restriction
     * @param dateStart date start represents current date
     * @return
     */
    private void insertForMoveTeamToRoom(final String moRestriction,
            final StringBuilder dateStart) {

        final StringBuilder insertRoomTeamSql = new StringBuilder();

        insertRoomTeamSql.append(" INSERT INTO rm_team(bl_id, fl_id, rm_id, team_id, date_start)");
        insertRoomTeamSql.append(" select mo.to_bl_id, mo.to_fl_id, mo.to_rm_id, mo.to_team_id, ");
        insertRoomTeamSql.append(dateStart.toString());
        insertRoomTeamSql.append(" from mo ");
        insertRoomTeamSql.append(" where ");
        insertRoomTeamSql.append(moRestriction);
        insertRoomTeamSql.append(" AND not exists( ");
        insertRoomTeamSql.append(" 	 select 1  from rm_team  ");
        insertRoomTeamSql.append(" 		 	where  rm_team.team_id=mo.to_team_id ");
        insertRoomTeamSql.append(
            "							and rm_team.bl_id=mo.from_bl_id and rm_team.fl_id=mo.from_fl_id and rm_team.rm_id=mo.from_rm_id) ");
        insertRoomTeamSql.append(
            " AND mo.em_id is null and mo.to_team_id is not null and mo.to_rm_id is not null ");

        SqlUtils.executeUpdate(RM_TEAM, insertRoomTeamSql.toString());

    }
}
