package com.archibus.app.move.report.xls;

import java.util.*;

import org.json.JSONArray;

import com.archibus.datasource.data.*;
import com.archibus.ext.report.xls.CrossTab2DBuilder;
import com.archibus.utility.StringUtil;

/**
 * Sum total value with division rows only to avoid repeating calculation with department.
 *
 * @author Jikai Xu
 *
 */

public class Cutom2DCrossTabXLSBuilder extends CrossTab2DBuilder {
    @Override
    public void addCustomTotalRow(final int totalRows, final int totalColumns,
            final List<Map<String, Object>> calculatedFields, final DataSet dataset) {
        final Map<String, Object> calculatedField = calculatedFields.get(0);
        final int decimal = getDecimals(calculatedField);
        final DataSet2D dataSet2D = (DataSet2D) dataset;
        final JSONArray columnValues = dataSet2D.getColumnValues();
        final JSONArray rowValues = dataSet2D.getRowValues();

        final int row = 4;
        for (int j = 0; j < columnValues.length() + 1; j++) {
            Double result = 0.00;
            // add up each column value for non-department rows
            for (int r = 0; r < rowValues.length(); r++) {
                final int rIndex = 1 + 1 + 1 + 1 + 1 + r;
                final int cIndex = 1 + 1 + j;
                final Object dvDp = this.xlsBuilder.getCellData(rIndex, 0);
                final Object area = this.xlsBuilder.getCellData(rIndex, cIndex);
                // add non-department value only
                if (StringUtil.toString(dvDp).indexOf('/') < 0 && StringUtil.notNullOrEmpty(area)) {
                    final Double numericValue = Double.valueOf(area.toString());
                    result += numericValue;
                }
            }
            addCustomTotalColumn(row, 1 + 1 + j + this.nRowDimensionFields, result, decimal);
        }
    }
}
