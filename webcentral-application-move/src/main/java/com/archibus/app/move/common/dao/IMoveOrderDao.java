package com.archibus.app.move.common.dao;

import java.util.List;

import org.json.JSONArray;

import com.archibus.app.move.common.domain.MoveOrder;
import com.archibus.core.dao.IDao;
import com.archibus.datasource.data.DataRecord;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Move Order.
 *
 * Interface to be implemented by MoveOrderDataSource.
 *
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveOrderDao extends IDao<MoveOrder> {

	/**
	 * Gets Move Order object by given record.
	 *
	 * @param record
	 *            to convert to object from.
	 * @return MoveOrder object.
	 */
	MoveOrder getMoveOrderByRecord(final DataRecord record);

	/**
	 * Gets Move Orders List by given restriction in sql.
	 *
	 * @param restriction
	 *            sql restriction.
	 * @return List<MoveOrder> list.
	 */
	List<MoveOrder> getMoveOrdersByRestriction(final String restriction);

	/**
	 * Assign employees to team.
	 *
	 * @param emIds
	 *            selected employees id
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	void assignEmployeesToTeam(final List<String> emIds, final String teamId, final String projectId, final String type,
			final String dateStartReq, final String dateToPerform);

	/**
	 * Assign employees to team.
	 *
	 * @param rooms
	 *            selected room info
	 * @param teamId
	 *            team id
	 * @param projectId
	 *            project id
	 * @param type
	 *            move type
	 * @param dateStartReq
	 *            request start date
	 * @param dateToPerform
	 *            perform date
	 */
	void assignRoomsToTeam(final JSONArray rooms, final String teamId, final String projectId, final String type,
			final String dateStartReq, final String dateToPerform);

}
