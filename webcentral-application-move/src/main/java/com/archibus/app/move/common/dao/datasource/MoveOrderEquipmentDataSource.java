package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IMoveOrderEquipmentDao;
import com.archibus.app.move.common.domain.MoveOrderEquipment;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Move Order Equipment.
 *
 * @author Zhang Yi
 *
 */
public class MoveOrderEquipmentDataSource extends ObjectDataSourceImpl<MoveOrderEquipment>
		implements IMoveOrderEquipmentDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "mo_id", "moveOrderId" }, { "eq_id", "equipmentId" },
			{ "eq_std", "equipmentStandard" }, { "from_bl_id", "fromBuildingId" }, { "from_fl_id", "fromFloorId" },
			{ "from_rm_id", "fromRoomId" }, { "cost_moving", "costMoving" } };

	/**
	 * Constructs MoveOrderEquipmentDataSource, mapped to <code>mo_eq</code>
	 * table, using <code>MoveOrderEquipment</code> bean.
	 */
	public MoveOrderEquipmentDataSource() {
		super("moveOrderEquipment", "mo_eq");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

}
