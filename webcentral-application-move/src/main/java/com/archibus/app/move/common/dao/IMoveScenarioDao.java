package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.MoveScenario;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Move Scenario.
 *
 * Interface to be implemented by MoveScenarioDataSource.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveScenarioDao extends IDao<MoveScenario> {

}
