package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.Project;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Dao for Project.
 *
 * Interface to be implemented by ProjectDataSource.
 *
 * @author Zhang Yi
 *
 */
public interface IProjectDao extends IDao<Project> {

}
