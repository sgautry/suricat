package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Equipment Standard.
 * <p>
 * Mapped to eqstd table.
 *
 * @author Zhang Yi
 *
 */
public class EquipmentStandard {

	/**
	 * Equipment Standard code.
	 */
	private String equipmentStandard;

	/**
	 * Move cost.
	 */
	private double costMoving;

	/**
	 * Getter for the equipmentStandard property.
	 *
	 * @return the equipmentStandard property.
	 */
	public String getEquipmentStandard() {
		return this.equipmentStandard;
	}

	/**
	 * Setter for the equipmentStandard property.
	 *
	 * @param equipmentStandard
	 *            the equipmentStandard to set.
	 */
	public void setEquipmentStandard(final String equipmentStandard) {
		this.equipmentStandard = equipmentStandard;
	}

	/**
	 * Getter for the costMoving property.
	 *
	 * @return the costMoving property.
	 */
	public double getCostMoving() {
		return this.costMoving;
	}

	/**
	 * Setter for the costMoving property.
	 *
	 * @param costMoving
	 *            the costMoving to set.
	 */
	public void setCostMoving(final double costMoving) {
		this.costMoving = costMoving;
	}

}
