package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.FurnitureStandard;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Furniture Standard.
 *
 * Interface to be implemented by FurnitureStandardDataSource.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IFurnitureStandardDao extends IDao<FurnitureStandard> {

}
