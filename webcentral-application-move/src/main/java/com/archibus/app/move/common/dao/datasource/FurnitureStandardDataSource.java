package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IFurnitureStandardDao;
import com.archibus.app.move.common.domain.FurnitureStandard;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Furniture Standard.
 *
 * @author Zhang Yi
 *
 */
public class FurnitureStandardDataSource extends ObjectDataSourceImpl<FurnitureStandard>
		implements IFurnitureStandardDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "fnstd", "furnitureStandard" },
			{ "cost_moving", "costMoving" } };

	/**
	 * Constructs FurnitureStandardDataSource, mapped to <code>fn_std</code>
	 * table, using <code>FurnitureStandard</code> bean.
	 */
	public FurnitureStandardDataSource() {
		super("furnitureStandard", "fnstd");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

}
