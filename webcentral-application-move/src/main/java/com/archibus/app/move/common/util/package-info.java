/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Contains common used classes such as utility class, etc.
 * <p>
 * *
 * 
 * @author zhangyi
 * @since 23.1
 *
 */
package com.archibus.app.move.common.util;