package com.archibus.app.move.common;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Constant class for Move Management. Provides constant variables.
 * <p>
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveConstants {

	// enable db exceptions for testing
	public final static boolean throwException = false;

	// enable for JUnit tests of mail
	public final static boolean testMail = false;

	public final static String newLine = " \n\n ";

	// @translatable
	public static String prefix = "[ARCHIBUS - Move Management]";

	// @translatable
	public final static String errorMsgGroupEmExist = "Group move [{0}] already includes a move for Employee [{1}]";

	// @translatable
	public final static String errorMsgGroupItExist = "Group move [{0}] already includes a move for Item [{1}]";

	// @translatable
	public final static String errorMsgGroupRmExist = "Group move [{0}] already includes a move for Room [{1}]";

	// @translatable
	public final static String errorMsgAddBulkMoves = "The department and locations entered do not match any employees";

	// @translatable
	public final static String errorMsgDeptContactInvalid = "The department contact [{0}] is invalid";

	// @translatable
	public final static String errorMsgActivityTypeInvalid = "The activity type [{0}] is invalid";

	// @translatable
	public final static String errorMsgScenarioExist = "Scenario [{0}] for Project [{1}] already exists";

	// @translatable
	public final static String errorMsgMissingRequestor = "Requestor for Action Item [{0}] is invalid or is missing";

	// @translatable
	public final static String errorMsgMissingBuilding = "Building for Action Item [{0}] is invalid or is missing";

}
