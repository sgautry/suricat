package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IMoveScenarioEmployeeDao;
import com.archibus.app.move.common.domain.MoveScenarioEmployee;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Move Scenario Employee.
 *
 * @author Zhang Yi
 *
 */
public class MoveScenarioEmployeeDataSource extends ObjectDataSourceImpl<MoveScenarioEmployee>
		implements IMoveScenarioEmployeeDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "auto_number", "id" },
			{ "scenario_id", "scenarioId" }, { "project_id", "projectId" }, { "date_start", "dateStart" },
			{ "em_id", "employeeId" }, { "team_id", "teamId" }, { "filename", "fileName" },
			{ "to_bl_id", "buildingId" }, { "to_fl_id", "floorId" }, { "to_rm_id", "roomId" } };

	/**
	 * Constructs MoveScenarioDataSource, mapped to <code>mo_scenario_em</code>
	 * table, using <code>MoveScenarioEmployee</code> bean.
	 *
	 */
	public MoveScenarioEmployeeDataSource() {
		super("moveScenarioEmployee", "mo_scenario_em");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
