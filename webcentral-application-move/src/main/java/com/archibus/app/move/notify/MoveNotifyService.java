package com.archibus.app.move.notify;

import com.archibus.app.common.bldgops.domain.ActionItem;
import com.archibus.app.common.organization.domain.Employee;
import com.archibus.app.move.common.dao.ICraftpersonDao;
import com.archibus.app.move.common.domain.*;
import com.archibus.context.ContextStore;
import com.archibus.core.dao.IDao;
import com.archibus.eventhandler.Moves.MoveNotifications;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 *
 * Implementation of Interface IMoveNotifyService. Provides notification and
 * email sending behaviors for Move Management Request logic.
 * <p>
 *
 * Used by Move Request Service to send notifications to process coordinators.
 *
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveNotifyService implements IMoveNotifyService {

	/**
	 * DAO for Employee.
	 */
	private IDao<Employee> employeeDao;

	/**
	 * DAO for Craftperson.
	 */
	private ICraftpersonDao craftpersonDao;

	@Override
	public void sendMailForRequestIndividualMove(final MoveOrder mo) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(false);
		moveNotifications.setMoveId(mo.getId());

		moveNotifications.sendEmailToRecipient("REQUEST", "INFORM", getEmailAddress(mo.getRequestor()));
		moveNotifications.sendEmailToRecipient("REQUEST", "INFORM_CONTACT", getEmailAddress(mo.getDepartmentContact()));
	}

	@Override
	public void sendMailForRequestGroupMove(final Project project) {
		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(true);
		moveNotifications.setProjectId(project.getId());

		moveNotifications.sendEmailToRecipient("REQUEST", "INFORM", getEmailAddress(project.getRequestor()));
		moveNotifications.sendEmailToRecipient("REQUEST", "INFORM_CONTACT",
				getEmailAddress(project.getDepartmentContact()));
	}

	@Override
	public void sendMailForRouteInividualMove(final MoveOrder moveOrder) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(false);
		moveNotifications.setMoveId(moveOrder.getId());

		moveNotifications.sendEmailToRecipient("ROUTE", "INFORM", getEmailAddress(moveOrder.getRequestor()));
		moveNotifications.sendEmailToRecipient("ROUTE", "INFORM", getEmailAddress(moveOrder.getDepartmentContact()));
		moveNotifications.sendEmailToRecipient("ROUTE", "INFORM", getEmailAddress(moveOrder.getMoveCoordinator()));
		moveNotifications.sendEmailToRecipient("ROUTE", "REQUEST", getEmailAddress(moveOrder.getApproveManager1()));
		moveNotifications.sendEmailToRecipient("ROUTE", "REQUEST", getEmailAddress(moveOrder.getApproveManager2()));
		moveNotifications.sendEmailToRecipient("ROUTE", "REQUEST", getEmailAddress(moveOrder.getApproveManager3()));

	}

	@Override
	public void sendMailForRouteGroupMove(final Project project) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(true);
		moveNotifications.setProjectId(project.getId());

		moveNotifications.sendEmailToRecipient("ROUTE", "INFORM", getEmailAddress(project.getRequestor()));
		moveNotifications.sendEmailToRecipient("ROUTE", "INFORM", getEmailAddress(project.getDepartmentContact()));
		moveNotifications.sendEmailToRecipient("ROUTE", "INFORM", getEmailAddress(project.getProjectManager()));
		moveNotifications.sendEmailToRecipient("ROUTE", "REQUEST", getEmailAddress(project.getApproveManager1()));
		moveNotifications.sendEmailToRecipient("ROUTE", "REQUEST", getEmailAddress(project.getApproveManager2()));
		moveNotifications.sendEmailToRecipient("ROUTE", "REQUEST", getEmailAddress(project.getApproveManager3()));

	}

	@Override
	public void sendMailForAutoApproveIndividualMove(final MoveOrder mo) {

		this.sendMailForIndividualMove(mo, "AUTOAPPROVE");

	}

	@Override
	public void sendMailForAutoApproveGroupMove(final Project project) {

		this.sendMailForGroupMove(project, "AUTOAPPROVE");
	}

	@Override
	public void sendMailForApproveIndividualMove(final MoveOrder mo) {

		this.sendMailForIndividualMove(mo, "APPROVE");

	}

	@Override
	public void sendMailForApproveGroupMove(final Project project) {

		this.sendMailForGroupMove(project, "APPROVE");

	}

	@Override
	public void sendMailForRejectIndividualMove(final MoveOrder mo) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(false);
		moveNotifications.setMoveId(mo.getId());

		moveNotifications.sendEmailToRecipient("REJECT", "INFORM", getEmailAddress(mo.getRequestor()));
		moveNotifications.sendEmailToRecipient("REJECT", "INFORM", getEmailAddress(mo.getDepartmentContact()));

	}

	@Override
	public void sendMailForRejectGroupMove(final Project project) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(true);
		moveNotifications.setProjectId(project.getId());

		moveNotifications.sendEmailToRecipient("REJECT", "INFORM", getEmailAddress(project.getRequestor()));
		moveNotifications.sendEmailToRecipient("REJECT", "INFORM", getEmailAddress(project.getDepartmentContact()));
	}

	@Override
	public void sendMailForIssueIndividualMove(final MoveOrder mo) {

		this.sendMailForIndividualMove(mo, "ISSUE");

	}

	@Override
	public void sendMailForIssueGroupMove(final Project project) {

		this.sendMailForGroupMove(project, "ISSUE");

	}

	@Override
	public void sendMailForIssueRequest(final ActionItem item) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		final String emEmailAddress = getEmailAddress(item.getAssignedTo());

		if (StringUtil.notNullOrEmpty(item.getAssignedTo())) {
			final String cfEmailAddress = this.craftpersonDao.get(item.getAssignedTo()).getEmail();

			moveNotifications.sendEmailToRecipient("ISSUE", "REQUEST",
					StringUtil.isNullOrEmpty(emEmailAddress) ? cfEmailAddress : emEmailAddress);
		}

	}

	@Override
	public void sendMailForCloseIndividualMove(final MoveOrder moveOrder) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(false);
		moveNotifications.setMoveId(moveOrder.getId());

		moveNotifications.sendEmailToRecipient("CLOSE", "INFORM", getEmailAddress(moveOrder.getRequestor()));

		moveNotifications.sendEmailToRecipient("CLOSE", "INFORM", getEmailAddress(moveOrder.getDepartmentContact()));

		moveNotifications.sendEmailToRecipient("CLOSE", "INFORM", getEmailAddress(moveOrder.getMoveCoordinator()));
	}

	@Override
	public void sendMailForCloseGroupMove(final Project project) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(true);
		moveNotifications.setProjectId(project.getId());

		moveNotifications.sendEmailToRecipient("CLOSE", "INFORM", getEmailAddress(project.getRequestor()));

		moveNotifications.sendEmailToRecipient("CLOSE", "INFORM", getEmailAddress(project.getDepartmentContact()));

		moveNotifications.sendEmailToRecipient("CLOSE", "INFORM", getEmailAddress(project.getProjectManager()));

	}

	/**
	 * Send mail for individual move.
	 *
	 * @param mo
	 *            move order
	 * @param operation
	 *            one operation of
	 *            REQUEST/ROUTE/AUTOAPPROVE/APPROVE/REJECT/ISSUE/CLOSE
	 */
	private void sendMailForIndividualMove(final MoveOrder mo, final String operation) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(false);
		moveNotifications.setMoveId(mo.getId());

		// send mail to move order's requestor, department contact and
		// coordinator.
		moveNotifications.sendEmailToRecipient(operation, "INFORM", getEmailAddress(mo.getRequestor()));
		moveNotifications.sendEmailToRecipient(operation, "INFORM", getEmailAddress(mo.getDepartmentContact()));
		moveNotifications.sendEmailToRecipient(operation, "INFORM", getEmailAddress(mo.getMoveCoordinator()));

	}

	/**
	 * Send mail for group move.
	 *
	 * @param project
	 *            move project
	 * @param operation
	 *            one operation of
	 *            REQUEST/ROUTE/AUTOAPPROVE/APPROVE/REJECT/ISSUE/CLOSE
	 */
	private void sendMailForGroupMove(final Project project, final String operation) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final MoveNotifications moveNotifications = new MoveNotifications(context);

		moveNotifications.setGroupMove(true);
		moveNotifications.setProjectId(project.getId());

		// send mail to move project's requestor, department contact and
		// coordinator.
		moveNotifications.sendEmailToRecipient(operation, "INFORM", getEmailAddress(project.getRequestor()));
		moveNotifications.sendEmailToRecipient(operation, "INFORM", getEmailAddress(project.getDepartmentContact()));
		moveNotifications.sendEmailToRecipient(operation, "INFORM", getEmailAddress(project.getProjectManager()));
	}

	/**
	 * @return The email address by given employee id
	 *
	 * @param emId
	 *            employee id
	 *
	 */
	private String getEmailAddress(final String emId) {
		String address;

		if (StringUtil.isNullOrEmpty(emId)) {

			address = null;

		} else {

			final Employee employee = this.employeeDao.get(emId);

			address = employee.getEmail();
		}
		return address;
	}

	/**
	 * Getter for the craftpersonDao property.
	 *
	 * @return the craftpersonDao property.
	 */
	public ICraftpersonDao getCraftpersonDao() {
		return this.craftpersonDao;
	}

	/**
	 * Setter for the craftpersonDao property.
	 *
	 * @param craftpersonDao
	 *            the craftpersonDao to set.
	 */
	public void setCraftpersonDao(final ICraftpersonDao craftpersonDao) {
		this.craftpersonDao = craftpersonDao;
	}

	/**
	 * Getter for the employeeDao property.
	 *
	 * @return the employeeDao property.
	 */
	public IDao<Employee> getEmployeeDao() {
		return this.employeeDao;
	}

	/**
	 * Setter for the employeeDao property.
	 *
	 * @param employeeDao
	 *            the employeeDao to set.
	 */
	public void setEmployeeDao(final IDao<Employee> employeeDao) {
		this.employeeDao = employeeDao;
	}

}
