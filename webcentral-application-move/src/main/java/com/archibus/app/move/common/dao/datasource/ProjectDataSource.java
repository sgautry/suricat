package com.archibus.app.move.common.dao.datasource;

import com.archibus.app.move.common.dao.IProjectDao;
import com.archibus.app.move.common.domain.Project;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * DataSource for Project.
 *
 * @author Zhang Yi
 *
 */
public class ProjectDataSource extends ObjectDataSourceImpl<Project> implements IProjectDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "project_id", "id" }, { "em_id", "employeeId" },
			{ "status", "status" }, { "date_closed", "dateClosed" }, { "requestor", "requestor" },
			{ "dept_contact", "departmentContact" }, { "phone_req", "phoneRequestor" },
			{ "phone_dept_contact", "phoneDepartmentContact" }, { "dv_id", "divisionId" }, { "dp_id", "departmentId" },
			{ "ac_id", "accountId" }, { "description", "description" }, { "bl_id", "buildingId" },
			{ "contact_id", "contactId" }, { "date_start", "dateStart" }, { "date_end", "dateEnd" },
			{ "date_created", "dateCreated" }, { "project_type", "projectType" }, { "site_id", "siteId" },
			{ "apprv_mgr1", "approveManager1" }, { "apprv_mgr2", "approveManager2" },
			{ "apprv_mgr3", "approveManager3" }, { "apprv_mgr1_status", "approveManager1Status" },
			{ "apprv_mgr2_status", "approveManager2Status" }, { "apprv_mgr3_status", "approveManager3Status" },
			{ "date_app_mgr1", "approveManager1Date" }, { "date_app_mgr2", "approveManager2Date" },
			{ "date_app_mgr3", "approveManager3Date" }, { "date_approved", "dateApproved" },
			{ "date_issued", "dateIssued" }, { "date_requested", "dateRequested" } };

	/**
	 * Constructs ProjectDataSource, mapped to <code>project</code> table, using
	 * <code>Project</code> bean.
	 */
	public ProjectDataSource() {
		super("moveProject", "project");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

}
