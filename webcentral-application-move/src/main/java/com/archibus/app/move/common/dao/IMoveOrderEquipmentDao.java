package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.MoveOrderEquipment;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Move Order Equipment.
 *
 * Interface to be implemented by MoveOrderEquipmentDataSource.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IMoveOrderEquipmentDao extends IDao<MoveOrderEquipment> {

}
