package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.MoveScenarioEmployee;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Move Scenario Employee.
 *
 * Interface to be implemented by MoveScenarioEmployeeDataSource.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public interface IMoveScenarioEmployeeDao extends IDao<MoveScenarioEmployee> {

}
