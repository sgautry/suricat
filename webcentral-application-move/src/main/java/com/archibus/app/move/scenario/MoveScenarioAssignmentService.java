package com.archibus.app.move.scenario;

import java.util.List;

import org.json.*;

import com.archibus.app.move.common.dao.IMoveScenarioEmployeeDao;
import com.archibus.app.move.common.domain.MoveScenarioEmployee;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.model.view.datasource.ClauseDef.*;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Implementation of Interface IMoveScenarioAssignmentService. Provides Move
 * scenario Assignment related business operations.
 * <p>
 *
 * Used by Workflow rule handler class of Move Management to delegate their wfr
 * methods about move scenario assignment.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public class MoveScenarioAssignmentService implements IMoveScenarioAssignmentService {

	/**
	 * Property: MO_SCENARIO_EM_SCENARIO_ID.
	 */
	private static final String MO_SCENARIO_EM_SCENARIO_ID = "mo_scenario_em.scenario_id";

	/**
	 * Property: MO_SCENARIO_EM_PROJECT_ID.
	 */
	private static final String MO_SCENARIO_EM_PROJECT_ID = "mo_scenario_em.project_id";

	/**
	 * Property: MO_SCENARIO_EM_TEAM_ID. Typical value is TODO. Constant: TODO.
	 */
	private static final String MO_SCENARIO_EM_TEAM_ID = "mo_scenario_em.team_id";

	/**
	 * Property: MO_SCENARIO_EM_EM_ID. Typical value is TODO. Constant: TODO.
	 */
	private static final String MO_SCENARIO_EM_EM_ID = "mo_scenario_em.em_id";

	/**
	 * Property: TEAM.
	 */
	private static final String TEAM = "team";

	/**
	 * Property: MO_SCENARIO_EM_DATE_START.
	 */
	private static final String MO_SCENARIO_EM_DATE_START = "mo_scenario_em.date_start";

	/**
	 * Property: MO_SCENARIO_EM_AUTO_NUMBER.
	 */
	private static final String MO_SCENARIO_EM_AUTO_NUMBER = "mo_scenario_em.auto_number";

	/**
	 * Property: TO_RM_ID.
	 */
	private static final String TO_RM_ID = "to_rm_id";

	/**
	 * Property: TEAM_ID.
	 */
	private static final String TEAM_ID = "team_id";

	/**
	 * Property: EM_ID.
	 */
	private static final String EM_ID = "em_id";

	/**
	 * Property: FILENAME.
	 */
	private static final String FILENAME = "filename";

	/**
	 * Property: TO_FL_ID.
	 */
	private static final String TO_FL_ID = "to_fl_id";

	/**
	 * Property: TO_BL_ID.
	 */
	private static final String TO_BL_ID = "to_bl_id";

	/**
	 * Property: SCENARIO_ID.
	 */
	private static final String SCENARIO_ID = "scenario_id";

	/**
	 * Property: PROJECT_ID.
	 */
	private static final String PROJECT_ID = "project_id";

	/**
	 * Property: MO_SCENARIO_EM.
	 */
	private static final String MO_SCENARIO_EM = "mo_scenario_em";

	/**
	 * Property: MO_SCENARIO_EM_TO_RM_ID.
	 */
	private static final String MO_SCENARIO_EM_TO_RM_ID = "mo_scenario_em.to_rm_id";

	/**
	 * Property: MO_SCENARIO_EM_TO_FL_ID.
	 */
	private static final String MO_SCENARIO_EM_TO_FL_ID = "mo_scenario_em.to_fl_id";

	/**
	 * Property: MO_SCENARIO_EM_TO_BL_ID.
	 */
	private static final String MO_SCENARIO_EM_TO_BL_ID = "mo_scenario_em.to_bl_id";

	/**
	 * Property: MO_SCENARIO_EM_FILENAME.
	 */
	private static final String MO_SCENARIO_EM_FILENAME = "mo_scenario_em.filename";

	/**
	 * DAO for Move Scenario Employee.
	 */
	private IMoveScenarioEmployeeDao moveScenarioEmployeeDao;

	@Override
	public void removeAssignment(final int moveScenarioEmId, final String assignType) throws ExceptionBase {

		final MoveScenarioEmployee moveScenarioEmployee = this.moveScenarioEmployeeDao.get(moveScenarioEmId);

		final boolean isLastLayoutHolder = this.isLastLayoutHolder(moveScenarioEmployee);

		if (TEAM.equalsIgnoreCase(assignType)) {

			this.removeTeamAssignment(moveScenarioEmployee, isLastLayoutHolder);

		} else {

			this.removeEmployeeAssignment(moveScenarioEmployee, isLastLayoutHolder);
		}

	}

	/**
	 * Remove Employee Assignment.
	 *
	 * @param moveScenarioEmployee
	 *            Move Scenario Employee object
	 * @param isLastLayoutHolder
	 *            boolean indicate if is last layout holder
	 */
	private void removeEmployeeAssignment(final MoveScenarioEmployee moveScenarioEmployee,
			final boolean isLastLayoutHolder) {

		if (isLastLayoutHolder) {

			this.createLayoutPlaceHolder(moveScenarioEmployee);

		}

		this.clearAssignedLocation(moveScenarioEmployee);
	}

	/**
	 * Remove Team Assignment.
	 *
	 * @param moveScenarioEmployee
	 *            Move Scenario Employee object
	 * @param isLastLayoutHolder
	 *            boolean indicate if is last layout holder
	 **/
	private void removeTeamAssignment(final MoveScenarioEmployee moveScenarioEmployee,
			final boolean isLastLayoutHolder) {

		if (isLastLayoutHolder) {

			if (this.isLastTeamHolder(moveScenarioEmployee)) {

				this.createLayoutPlaceHolder(moveScenarioEmployee);

				this.clearAssignedLocation(moveScenarioEmployee);

			} else {

				this.moveScenarioEmployeeDao.delete(moveScenarioEmployee);
			}

		} else {
			if (this.isLastTeamHolder(moveScenarioEmployee)) {

				this.clearAssignedLocation(moveScenarioEmployee);

			} else {

				this.moveScenarioEmployeeDao.delete(moveScenarioEmployee);
			}
		}
	}

	/**
	 * Create Layout Place Holder record in table mo_scenario_em.
	 *
	 * @param moveScenarioEmployee
	 *            Move Scenario Employee object
	 */
	private void createLayoutPlaceHolder(final MoveScenarioEmployee moveScenarioEmployee) {

		final MoveScenarioEmployee layoutHolder = new MoveScenarioEmployee();

		layoutHolder.setProjectId(moveScenarioEmployee.getProjectId());
		layoutHolder.setScenarioId(moveScenarioEmployee.getScenarioId());
		layoutHolder.setBuildingId(moveScenarioEmployee.getBuildingId());
		layoutHolder.setFloorId(moveScenarioEmployee.getFloorId());
		layoutHolder.setFileName(moveScenarioEmployee.getFileName());

		this.moveScenarioEmployeeDao.save(layoutHolder);
	}

	/**
	 * @return if the given Move Scenario Employee object is the last layout
	 *         place holder.
	 *
	 * @param moveScenarioEmployee
	 *            Move Scenario Employee object
	 */
	private boolean isLastLayoutHolder(final MoveScenarioEmployee moveScenarioEmployee) {

		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(MO_SCENARIO_EM, PROJECT_ID, moveScenarioEmployee.getProjectId(), Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, SCENARIO_ID, moveScenarioEmployee.getScenarioId(), Operation.EQUALS);

		restriction.addClause(MO_SCENARIO_EM, TO_BL_ID, moveScenarioEmployee.getBuildingId(), Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, TO_FL_ID, moveScenarioEmployee.getFloorId(), Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, FILENAME, moveScenarioEmployee.getFileName(), Operation.EQUALS);

		restriction.addClause(MO_SCENARIO_EM, EM_ID, moveScenarioEmployee.getEmployeeId(), Operation.IS_NOT_NULL,
				RelativeOperation.AND_BRACKET);
		restriction.addClause(MO_SCENARIO_EM, TEAM_ID, moveScenarioEmployee.getTeamId(), Operation.IS_NOT_NULL,
				RelativeOperation.OR);

		final List<MoveScenarioEmployee> moveScenarioEmployees = this.moveScenarioEmployeeDao.find(restriction);

		return (moveScenarioEmployees != null && moveScenarioEmployees.size() > 1) ? false : true;
	}

	/**
	 * Getter for the moveScenarioEmployeeDao property.
	 *
	 * @return the moveScenarioEmployeeDao property.
	 */
	public IMoveScenarioEmployeeDao getMoveScenarioEmployeeDao() {
		return this.moveScenarioEmployeeDao;
	}

	/**
	 * Setter for the moveScenarioEmployeeDao property.
	 *
	 * @param moveScenarioEmployeeDao
	 *            the moveScenarioEmployeeDao to set.
	 */
	public void setMoveScenarioEmployeeDao(final IMoveScenarioEmployeeDao moveScenarioEmployeeDao) {
		this.moveScenarioEmployeeDao = moveScenarioEmployeeDao;
	}

	@Override
	public void commitAssignments(final JSONArray assignments, final String assignType) throws ExceptionBase {
		for (int i = 0; i < assignments.length(); i++) {

			final JSONObject assignment = assignments.getJSONObject(i);
			final DataRecord record = DataRecord.createRecordFromJSON(assignment);

			final MoveScenarioEmployee moveScenarioEmployee = this.getToLocateMoveScenarioEm(assignType, record);

			moveScenarioEmployee.setFileName(record.getString(MO_SCENARIO_EM_FILENAME));
			moveScenarioEmployee.setBuildingId(record.getString(MO_SCENARIO_EM_TO_BL_ID));
			moveScenarioEmployee.setFloorId(record.getString(MO_SCENARIO_EM_TO_FL_ID));
			moveScenarioEmployee.setRoomId(record.getString(MO_SCENARIO_EM_TO_RM_ID));

			if (TEAM.equalsIgnoreCase(assignType)) {

				moveScenarioEmployee.setDateStart(record.getDate(MO_SCENARIO_EM_DATE_START));

				if (moveScenarioEmployee.getId() == null) {

					moveScenarioEmployee.setProjectId(record.getString(MO_SCENARIO_EM_PROJECT_ID));
					moveScenarioEmployee.setScenarioId(record.getString(MO_SCENARIO_EM_SCENARIO_ID));
					moveScenarioEmployee.setTeamId(record.getString(MO_SCENARIO_EM_TEAM_ID));

					this.moveScenarioEmployeeDao.save(moveScenarioEmployee);

				} else {
					this.moveScenarioEmployeeDao.update(moveScenarioEmployee);
				}

				this.deleteTeamHolder(moveScenarioEmployee);

			} else {

				this.moveScenarioEmployeeDao.update(moveScenarioEmployee);
			}

			this.deleteLayoutPlaceHolder(moveScenarioEmployee);
		}
	}

	/**
	 * Get the Move Scenario Employee Object that is going to assign location.
	 *
	 * @param assignType
	 *            assignment type: "team" or "employee"
	 * @param record
	 *            to locate move scenario employee record
	 *
	 * @return Move Scenario Employee Object
	 */
	private MoveScenarioEmployee getToLocateMoveScenarioEm(final String assignType, final DataRecord record) {

		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(MO_SCENARIO_EM, PROJECT_ID, record.getString(MO_SCENARIO_EM_PROJECT_ID),
				Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, SCENARIO_ID, record.getString(MO_SCENARIO_EM_SCENARIO_ID),
				Operation.EQUALS);

		if (TEAM.equalsIgnoreCase(assignType)) {

			restriction.addClause(MO_SCENARIO_EM, EM_ID, record.getString(MO_SCENARIO_EM_EM_ID), Operation.IS_NULL);
			restriction.addClause(MO_SCENARIO_EM, TEAM_ID, record.getString(MO_SCENARIO_EM_TEAM_ID), Operation.EQUALS);
			restriction.addClause(MO_SCENARIO_EM, TO_BL_ID, record.getString(MO_SCENARIO_EM_TO_BL_ID),
					Operation.EQUALS);
			restriction.addClause(MO_SCENARIO_EM, TO_FL_ID, record.getString(MO_SCENARIO_EM_TO_FL_ID),
					Operation.EQUALS);
			restriction.addClause(MO_SCENARIO_EM, TO_RM_ID, record.getString(MO_SCENARIO_EM_TO_RM_ID),
					Operation.EQUALS);

		} else {

			restriction.addClause(MO_SCENARIO_EM, EM_ID, record.getString(MO_SCENARIO_EM_EM_ID), Operation.EQUALS);
			restriction.addClause(MO_SCENARIO_EM, TEAM_ID, record.getString(MO_SCENARIO_EM_TEAM_ID), Operation.IS_NULL);
		}

		final List<MoveScenarioEmployee> moveScenarioEmployees = this.moveScenarioEmployeeDao.find(restriction);

		return moveScenarioEmployees.isEmpty() ? new MoveScenarioEmployee() : moveScenarioEmployees.get(0);
	}

	/**
	 * Delete the 'Holder' record for given team's move scenario employee
	 * object.
	 *
	 * @param moveScenarioEm
	 *            Move Scenario Employee object
	 */
	private void deleteTeamHolder(final MoveScenarioEmployee moveScenarioEm) {

		final ParsedRestrictionDef placeHolderRestriction = new ParsedRestrictionDef();

		placeHolderRestriction.addClause(MO_SCENARIO_EM, PROJECT_ID, moveScenarioEm.getProjectId(), Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, SCENARIO_ID, moveScenarioEm.getScenarioId(), Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, TEAM_ID, moveScenarioEm.getTeamId(), Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, TO_RM_ID, "", Operation.IS_NULL);

		final List<MoveScenarioEmployee> placeHolder = this.moveScenarioEmployeeDao.find(placeHolderRestriction);
		for (final MoveScenarioEmployee mse : placeHolder) {
			this.moveScenarioEmployeeDao.delete(mse);
		}
	}

	/**
	 * @return if current move scenario employee object contains the last team
	 *         in current project and scenario.
	 *
	 * @param moveScenarioEm
	 *            Move Scenario Employee object
	 *
	 */
	private boolean isLastTeamHolder(final MoveScenarioEmployee moveScenarioEm) {

		final ParsedRestrictionDef placeHolderRestriction = new ParsedRestrictionDef();

		placeHolderRestriction.addClause(MO_SCENARIO_EM, PROJECT_ID, moveScenarioEm.getProjectId(), Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, SCENARIO_ID, moveScenarioEm.getScenarioId(), Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, TEAM_ID, moveScenarioEm.getTeamId(), Operation.EQUALS);

		final List<MoveScenarioEmployee> placeHolder = this.moveScenarioEmployeeDao.find(placeHolderRestriction);

		return (placeHolder != null && placeHolder.size() > 1) ? false : true;
	}

	/**
	 * Delete the 'PlaceHolder' record for given move scenario employee object.
	 *
	 * @param moveScenarioEmployee
	 *            Move Scenario Employee object
	 */
	private void deleteLayoutPlaceHolder(final MoveScenarioEmployee moveScenarioEmployee) {

		final ParsedRestrictionDef placeHolderRestriction = new ParsedRestrictionDef();

		placeHolderRestriction.addClause(MO_SCENARIO_EM, PROJECT_ID, moveScenarioEmployee.getProjectId(),
				Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, SCENARIO_ID, moveScenarioEmployee.getScenarioId(),
				Operation.EQUALS);

		placeHolderRestriction.addClause(MO_SCENARIO_EM, TEAM_ID, "", Operation.IS_NULL);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, EM_ID, "", Operation.IS_NULL);

		placeHolderRestriction.addClause(MO_SCENARIO_EM, TO_RM_ID, "", Operation.IS_NULL);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, TO_BL_ID, moveScenarioEmployee.getBuildingId(),
				Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, TO_FL_ID, moveScenarioEmployee.getFloorId(), Operation.EQUALS);
		placeHolderRestriction.addClause(MO_SCENARIO_EM, FILENAME, moveScenarioEmployee.getFileName(),
				Operation.EQUALS);

		final List<MoveScenarioEmployee> placeHolder = this.moveScenarioEmployeeDao.find(placeHolderRestriction);
		for (final MoveScenarioEmployee mse : placeHolder) {

			this.moveScenarioEmployeeDao.delete(mse);
		}
	}

	@Override
	public void validLayoutLocation(final String projectId, final String scenarioId) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();

		final DataSource checkLayoutChangeDs = DataSourceFactory
				.loadDataSourceFromFile("ab-mo-console-employees-to-locate.axvw", "checkLayoutChangeDs");

		checkLayoutChangeDs.addParameter("projectId", projectId, DataSource.DATA_TYPE_TEXT);
		checkLayoutChangeDs.addParameter("scenarioId", scenarioId, DataSource.DATA_TYPE_TEXT);

		final List<DataRecord> records = checkLayoutChangeDs.getAllRecords();

		if (!records.isEmpty()) {

			this.clearInvalidToLocation(records);

			context.addResponseParameter("jsonExpression", "changed");
		}
	}

	/**
	 * Clear to location fields of Move Scenario Employee records that reference
	 * to invalid locations( rm or rm_trial).
	 *
	 * @param records
	 *            DataRecord list of table mo_scenario_em
	 */
	private void clearInvalidToLocation(final List<DataRecord> records) {

		for (final DataRecord record : records) {

			final int moveScenarioEmId = record.getInt(MO_SCENARIO_EM_AUTO_NUMBER);

			final MoveScenarioEmployee moveScenarioEmployee = this.moveScenarioEmployeeDao.get(moveScenarioEmId);

			this.clearAssignedLocation(moveScenarioEmployee);

		}
	}

	/**
	 * Clear assign layout location of given Move Scenario Employee object.
	 *
	 * @param moveScenarioEmployee
	 *            Move Scenario Employee object
	 */
	private void clearAssignedLocation(final MoveScenarioEmployee moveScenarioEmployee) {

		moveScenarioEmployee.setDateStart(null);
		moveScenarioEmployee.setBuildingId("");
		moveScenarioEmployee.setFloorId("");
		moveScenarioEmployee.setRoomId("");
		moveScenarioEmployee.setFileName("");

		this.moveScenarioEmployeeDao.update(moveScenarioEmployee);
	}

	@Override
	public void deleteLayout(final JSONObject record) {

		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(MO_SCENARIO_EM, PROJECT_ID, record.getString(MO_SCENARIO_EM_PROJECT_ID),
				Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, SCENARIO_ID, record.getString(MO_SCENARIO_EM_SCENARIO_ID),
				Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, FILENAME, record.getString(MO_SCENARIO_EM_FILENAME), Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, TO_BL_ID, record.getString(MO_SCENARIO_EM_TO_BL_ID), Operation.EQUALS);
		restriction.addClause(MO_SCENARIO_EM, TO_FL_ID, record.getString(MO_SCENARIO_EM_TO_FL_ID), Operation.EQUALS);

		final List<MoveScenarioEmployee> occupants = this.moveScenarioEmployeeDao.find(restriction);

		for (final MoveScenarioEmployee moveScenarioEmployee : occupants) {

			if (StringUtil.isNullOrEmpty(moveScenarioEmployee.getEmployeeId())
					&& StringUtil.isNullOrEmpty(moveScenarioEmployee.getTeamId())) {

				this.moveScenarioEmployeeDao.delete(moveScenarioEmployee);

			} else {

				this.clearAssignedLocation(moveScenarioEmployee);
			}
		}
	}

	@Override
	public void addLayoutPlaceholder(final JSONObject record) {

		final MoveScenarioEmployee moveScenarioEmployee = new MoveScenarioEmployee();

		moveScenarioEmployee.setProjectId(record.getString(MO_SCENARIO_EM_PROJECT_ID));
		moveScenarioEmployee.setScenarioId(record.getString(MO_SCENARIO_EM_SCENARIO_ID));
		moveScenarioEmployee.setBuildingId(record.getString(MO_SCENARIO_EM_TO_BL_ID));
		moveScenarioEmployee.setFloorId(record.getString(MO_SCENARIO_EM_TO_FL_ID));
		moveScenarioEmployee.setFileName(record.getString(MO_SCENARIO_EM_FILENAME));

		final boolean isLastLayoutHolder = this.isLastLayoutHolder(moveScenarioEmployee);

		if (isLastLayoutHolder) {

			this.createLayoutPlaceHolder(moveScenarioEmployee);

		}
	}
}
