package com.archibus.app.move.common;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Interface that provides Move related calculating behaviors for Move
 * Management Domain.
 * <p>
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public interface IMoveCalculatorService {

	/**
	 * Calculate historical employee headcount implemented as job that run on
	 * first day of the month.
	 *
	 */
	void calculateHistoricalEmployeeHeadcount();

}
