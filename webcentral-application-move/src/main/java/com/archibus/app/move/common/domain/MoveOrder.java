package com.archibus.app.move.common.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Move Order.
 * <p>
 * Mapped to mo table.
 *
 * @author Zhang Yi
 *
 */
public class MoveOrder {

	/**
	 * ID of the Move Order.
	 */
	private int id;

	/**
	 * Move Employee ID.
	 */
	private String employeeId;

	/**
	 * Move Status.
	 */
	private String status;

	/**
	 * Move Type.
	 */
	private String moveType;

	/**
	 * Move Class.
	 */
	private String moveClass;

	/**
	 * Move Closed Date.
	 */
	private Date dateClosed;

	/**
	 * Move Start Date.
	 */
	private Date dateStart;

	/**
	 * Move Date to Perform.
	 */
	private Date datePerform;

	/**
	 * Move Project Manage
	 */
	private String projectManager;

	/**
	 * Move Department Contact.
	 */
	private String departmentContact;

	/**
	 * Move Requestor.
	 */
	private String requestor;

	/**
	 * Move Description.
	 */
	private String description;

	/**
	 * Move To Building.
	 */
	private String toBuilding;

	/**
	 * Move To Floor.
	 */
	private String toFloor;

	/**
	 * Move To Room.
	 */
	private String toRoom;

	/**
	 * Move Requestor's Phone.
	 */
	private String phone;

	/**
	 * Move Department Contact's Phone.
	 */
	private String phoneDeptContact;

	/**
	 * Department Contact's Division.
	 */
	private String division;

	/**
	 * Department Contact's Department.
	 */
	private String department;

	/**
	 *
	 * Move from Building.
	 */
	private String fromBuilding;

	/**
	 * Move from Floor.
	 */
	private String fromFloor;

	/**
	 * Move from Room.
	 */
	private String fromRoom;

	/**
	 * Move from Division.
	 */
	private String fromDivision;

	/**
	 * Move from Department.
	 */
	private String fromDepartment;

	/**
	 * Move from Phone.
	 */
	private String fromPhone;

	/**
	 * Move to Division.
	 */
	private String toDivision;

	/**
	 * Move to Department.
	 */
	private String toDepartment;

	/**
	 * Move to Phone.
	 */
	private String toPhone;

	/**
	 * Move to Fax.
	 */
	private String toFax;

	/**
	 * Move from Fax.
	 */
	private String fromFax;

	/**
	 * Move from Mail Stop.
	 */
	private String fromMailStop;

	/**
	 * Move from Jack Data.
	 */
	private String fromJackData;

	/**
	 * Move from Jack Voice.
	 */
	private String fromJackVoice;

	/**
	 * Move Questionnaire.
	 */
	private String questionnarie;

	/**
	 * Move project Id.
	 */
	private String projectId;

	/**
	 * Move account Id.
	 */
	private String accountId;

	/**
	 * Move Coordinator.
	 */
	private String moveCoordinator;

	/**
	 * Move approve Manager1.
	 */
	private String approveManager1;

	/**
	 * Move approve Manager2.
	 */
	private String approveManager2;

	/**
	 * Move approve Manager3.
	 */
	private String approveManager3;

	/**
	 * Move approve Manager1 status.
	 */
	private String approveManager1Status;

	/**
	 * Move approve Manager2 status.
	 */
	private String approveManager2Status;

	/**
	 * Move approve Manager3 status.
	 */
	private String approveManager3Status;

	/**
	 * Move approve Manager1 status.
	 */
	private Date approveManager1Date;

	/**
	 * Move approve Manager2 status.
	 */
	private Date approveManager2Date;

	/**
	 * Move approve Manager3 status.
	 */
	private Date approveManager3Date;

	/**
	 * Move approve Date.
	 */
	private Date dateApproved;

	/**
	 * Move Issue Date.
	 */
	private Date dateIssued;

	/**
	 * Move Request Date.
	 */
	private Date dateRequested;

	/**
	 * Move Request Time.
	 */
	private Date timeRequested;

	/**
	 * Move Request activityLog Id.
	 */
	private Integer activityLogId;

	/**
	 * From Team Code.
	 */
	private String fromTeamId;

	/**
	 * To Team Code.
	 */
	private String toTeamId;

	/**
	 * Getter for the approveManager1 property.
	 *
	 * @return the approveManager1 property.
	 */
	public String getApproveManager1() {
		return this.approveManager1;
	}

	/**
	 * Getter for the dateRequested property.
	 *
	 * @return the dateRequested property.
	 */
	public Date getDateRequested() {
		return this.dateRequested;
	}

	/**
	 * Setter for the dateRequested property.
	 *
	 * @param dateRequested
	 *            the dateRequested to set.
	 */
	public void setDateRequested(final Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	/**
	 * Getter for the timeRequested property.
	 *
	 * @return the timeRequested property.
	 */
	public Date getTimeRequested() {
		return this.timeRequested;
	}

	/**
	 * Setter for the timeRequested property.
	 *
	 * @param timeRequested
	 *            the timeRequested to set.
	 */
	public void setTimeRequested(final Date timeRequested) {
		this.timeRequested = timeRequested;
	}

	/**
	 * Getter for the approveManager1Status property.
	 *
	 * @return the approveManager1Status property.
	 */
	public String getApproveManager1Status() {
		return this.approveManager1Status;
	}

	/**
	 * Setter for the approveManager1Status property.
	 *
	 * @param approveManager1Status
	 *            the approveManager1Status to set.
	 */
	public void setApproveManager1Status(final String approveManager1Status) {
		this.approveManager1Status = approveManager1Status;
	}

	/**
	 * Getter for the approveManager2Status property.
	 *
	 * @return the approveManager2Status property.
	 */
	public String getApproveManager2Status() {
		return this.approveManager2Status;
	}

	/**
	 * Setter for the approveManager2Status property.
	 *
	 * @param approveManager2Status
	 *            the approveManager2Status to set.
	 */
	public void setApproveManager2Status(final String approveManager2Status) {
		this.approveManager2Status = approveManager2Status;
	}

	/**
	 * Getter for the approveManager3Status property.
	 *
	 * @return the approveManager3Status property.
	 */
	public String getApproveManager3Status() {
		return this.approveManager3Status;
	}

	/**
	 * Setter for the approveManager3Status property.
	 *
	 * @param approveManager3Status
	 *            the approveManager3Status to set.
	 */
	public void setApproveManager3Status(final String approveManager3Status) {
		this.approveManager3Status = approveManager3Status;
	}

	/**
	 * Getter for the approveManager1Date property.
	 *
	 * @return the approveManager1Date property.
	 */
	public Date getApproveManager1Date() {
		return this.approveManager1Date;
	}

	/**
	 * Setter for the approveManager1Date property.
	 *
	 * @param approveManager1Date
	 *            the approveManager1Date to set.
	 */
	public void setApproveManager1Date(final Date approveManager1Date) {
		this.approveManager1Date = approveManager1Date;
	}

	/**
	 * Getter for the approveManager2Date property.
	 *
	 * @return the approveManager2Date property.
	 */
	public Date getApproveManager2Date() {
		return this.approveManager2Date;
	}

	/**
	 * Setter for the approveManager2Date property.
	 *
	 * @param approveManager2Date
	 *            the approveManager2Date to set.
	 */
	public void setApproveManager2Date(final Date approveManager2Date) {
		this.approveManager2Date = approveManager2Date;
	}

	/**
	 * Getter for the approveManager3Date property.
	 *
	 * @return the approveManager3Date property.
	 */
	public Date getApproveManager3Date() {
		return this.approveManager3Date;
	}

	/**
	 * Setter for the approveManager3Date property.
	 *
	 * @param approveManager3Date
	 *            the approveManager3Date to set.
	 */
	public void setApproveManager3Date(final Date approveManager3Date) {
		this.approveManager3Date = approveManager3Date;
	}

	/**
	 * Setter for the approveManager1 property.
	 *
	 * @param approveManager1
	 *            the approveManager1 to set.
	 */
	public void setApproveManager1(final String approveManager1) {
		this.approveManager1 = approveManager1;
	}

	/**
	 * Getter for the approveManager2 property.
	 *
	 * @return the approveManager2 property.
	 */
	public String getApproveManager2() {
		return this.approveManager2;
	}

	/**
	 * Setter for the approveManager2 property.
	 *
	 * @param approveManager2
	 *            the approveManager2 to set.
	 */
	public void setApproveManager2(final String approveManager2) {
		this.approveManager2 = approveManager2;
	}

	/**
	 * Getter for the approveManager3 property.
	 *
	 * @return the approveManager3 property.
	 */
	public String getApproveManager3() {
		return this.approveManager3;
	}

	/**
	 * Setter for the approveManager3 property.
	 *
	 * @param approveManager3
	 *            the approveManager3 to set.
	 */
	public void setApproveManager3(final String approveManager3) {
		this.approveManager3 = approveManager3;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the toBuilding property.
	 *
	 * @return the toBuilding property.
	 */
	public String getToBuilding() {
		return this.toBuilding;
	}

	/**
	 * Setter for the toBuilding property.
	 *
	 * @param toBuilding
	 *            the toBuilding to set.
	 */
	public void setToBuilding(final String toBuilding) {
		this.toBuilding = toBuilding;
	}

	/**
	 * Getter for the toFloor property.
	 *
	 * @return the toFloor property.
	 */
	public String getToFloor() {
		return this.toFloor;
	}

	/**
	 * Setter for the toFloor property.
	 *
	 * @param toFloor
	 *            the toFloor to set.
	 */
	public void setToFloor(final String toFloor) {
		this.toFloor = toFloor;
	}

	/**
	 * Getter for the toRoom property.
	 *
	 * @return the toRoom property.
	 */
	public String getToRoom() {
		return this.toRoom;
	}

	/**
	 * Setter for the toRoom property.
	 *
	 * @param toRoom
	 *            the toRoom to set.
	 */
	public void setToRoom(final String toRoom) {
		this.toRoom = toRoom;
	}

	/**
	 * Getter for the dateStart property.
	 *
	 * @return the dateStart property.
	 */
	public Date getDateStart() {
		return this.dateStart;
	}

	/**
	 * Setter for the dateStart property.
	 *
	 * @param dateStart
	 *            the dateStart to set.
	 */
	public void setDateStart(final Date dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for the dateCreated property.
	 *
	 * @return the dateCreated property.
	 */
	public Date getDateCreated() {
		return this.dateCreated;
	}

	/**
	 * Setter for the dateCreated property.
	 *
	 * @param dateCreated
	 *            the dateCreated to set.
	 */
	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * Project Closed Date.
	 */
	private Date dateCreated;

	/**
	 * Getter for the employeeId property.
	 *
	 * @return the employeeId property.
	 */
	public String getEmployeeId() {
		return this.employeeId;
	}

	/**
	 * Setter for the employeeId property.
	 *
	 * @param employeeId
	 *            the employeeId to set.
	 */
	public void setEmployeeId(final String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the dateClosed property.
	 *
	 * @return the dateClosed property.
	 */
	public Date getDateClosed() {
		return this.dateClosed;
	}

	/**
	 * Setter for the dateClosed property.
	 *
	 * @param dateClosed
	 *            the dateClosed to set.
	 */
	public void setDateClosed(final Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Getter for the projectManager property.
	 *
	 * @return the projectManager property.
	 */
	public String getProjectManager() {
		return this.projectManager;
	}

	/**
	 * Setter for the projectManager property.
	 *
	 * @param projectManager
	 *            the projectManager to set.
	 */
	public void setProjectManager(final String projectManager) {
		this.projectManager = projectManager;
	}

	/**
	 * Getter for the departmentContact property.
	 *
	 * @return the departmentContact property.
	 */
	public String getDepartmentContact() {
		return this.departmentContact;
	}

	/**
	 * Setter for the departmentContact property.
	 *
	 * @param departmentContact
	 *            the departmentContact to set.
	 */
	public void setDepartmentContact(final String departmentContact) {
		this.departmentContact = departmentContact;
	}

	/**
	 * Getter for the requestor property.
	 *
	 * @return the requestor property.
	 */
	public String getRequestor() {
		return this.requestor;
	}

	/**
	 * Setter for the requestor property.
	 *
	 * @param requestor
	 *            the requestor to set.
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}

	/**
	 * Getter for the datePerform property.
	 *
	 * @return the datePerform property.
	 */
	public Date getDatePerform() {
		return this.datePerform;
	}

	/**
	 * Setter for the datePerform property.
	 *
	 * @param datePerform
	 *            the datePerform to set.
	 */
	public void setDatePerform(final Date datePerform) {
		this.datePerform = datePerform;
	}

	/**
	 * Getter for the phone property.
	 *
	 * @return the phone property.
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * Setter for the phone property.
	 *
	 * @param phone
	 *            the phone to set.
	 */
	public void setPhone(final String phone) {
		this.phone = phone;
	}

	/**
	 * Getter for the phoneDeptContact property.
	 *
	 * @return the phoneDeptContact property.
	 */
	public String getPhoneDeptContact() {
		return this.phoneDeptContact;
	}

	/**
	 * Setter for the phoneDeptContact property.
	 *
	 * @param phoneDeptContact
	 *            the phoneDeptContact to set.
	 */
	public void setPhoneDeptContact(final String phoneDeptContact) {
		this.phoneDeptContact = phoneDeptContact;
	}

	/**
	 * Getter for the division property.
	 *
	 * @return the division property.
	 */
	public String getDivision() {
		return this.division;
	}

	/**
	 * Setter for the division property.
	 *
	 * @param division
	 *            the division to set.
	 */
	public void setDivision(final String division) {
		this.division = division;
	}

	/**
	 * Getter for the fromBuilding property.
	 *
	 * @return the fromBuilding property.
	 */
	public String getFromBuilding() {
		return this.fromBuilding;
	}

	/**
	 * Setter for the fromBuilding property.
	 *
	 * @param fromBuilding
	 *            the fromBuilding to set.
	 */
	public void setFromBuilding(final String fromBuilding) {
		this.fromBuilding = fromBuilding;
	}

	/**
	 * Getter for the fromFloor property.
	 *
	 * @return the fromFloor property.
	 */
	public String getFromFloor() {
		return this.fromFloor;
	}

	/**
	 * Setter for the fromFloor property.
	 *
	 * @param fromFloor
	 *            the fromFloor to set.
	 */
	public void setFromFloor(final String fromFloor) {
		this.fromFloor = fromFloor;
	}

	/**
	 * Getter for the fromRoom property.
	 *
	 * @return the fromRoom property.
	 */
	public String getFromRoom() {
		return this.fromRoom;
	}

	/**
	 * Setter for the fromRoom property.
	 *
	 * @param fromRoom
	 *            the fromRoom to set.
	 */
	public void setFromRoom(final String fromRoom) {
		this.fromRoom = fromRoom;
	}

	/**
	 * Getter for the fromDivision property.
	 *
	 * @return the fromDivision property.
	 */
	public String getFromDivision() {
		return this.fromDivision;
	}

	/**
	 * Setter for the fromDivision property.
	 *
	 * @param fromDivision
	 *            the fromDivision to set.
	 */
	public void setFromDivision(final String fromDivision) {
		this.fromDivision = fromDivision;
	}

	/**
	 * Getter for the fromDepartment property.
	 *
	 * @return the fromDepartment property.
	 */
	public String getFromDepartment() {
		return this.fromDepartment;
	}

	/**
	 * Setter for the fromDepartment property.
	 *
	 * @param fromDepartment
	 *            the fromDepartment to set.
	 */
	public void setFromDepartment(final String fromDepartment) {
		this.fromDepartment = fromDepartment;
	}

	/**
	 * Getter for the toDivision property.
	 *
	 * @return the toDivision property.
	 */
	public String getToDivision() {
		return this.toDivision;
	}

	/**
	 * Setter for the toDivision property.
	 *
	 * @param toDivision
	 *            the toDivision to set.
	 */
	public void setToDivision(final String toDivision) {
		this.toDivision = toDivision;
	}

	/**
	 * Getter for the toDepartment property.
	 *
	 * @return the toDepartment property.
	 */
	public String getToDepartment() {
		return this.toDepartment;
	}

	/**
	 * Setter for the toDepartment property.
	 *
	 * @param toDepartment
	 *            the toDepartment to set.
	 */
	public void setToDepartment(final String toDepartment) {
		this.toDepartment = toDepartment;
	}

	/**
	 * Getter for the toPhone property.
	 *
	 * @return the toPhone property.
	 */
	public String getToPhone() {
		return this.toPhone;
	}

	/**
	 * Setter for the toPhone property.
	 *
	 * @param toPhone
	 *            the toPhone to set.
	 */
	public void setToPhone(final String toPhone) {
		this.toPhone = toPhone;
	}

	/**
	 * Getter for the fromFax property.
	 *
	 * @return the fromFax property.
	 */
	public String getFromFax() {
		return this.fromFax;
	}

	/**
	 * Setter for the fromFax property.
	 *
	 * @param fromFax
	 *            the fromFax to set.
	 */
	public void setFromFax(final String fromFax) {
		this.fromFax = fromFax;
	}

	/**
	 * Getter for the fromMailStop property.
	 *
	 * @return the fromMailStop property.
	 */
	public String getFromMailStop() {
		return this.fromMailStop;
	}

	/**
	 * Setter for the fromMailStop property.
	 *
	 * @param fromMailStop
	 *            the fromMailStop to set.
	 */
	public void setFromMailStop(final String fromMailStop) {
		this.fromMailStop = fromMailStop;
	}

	/**
	 * Getter for the fromJackData property.
	 *
	 * @return the fromJackData property.
	 */
	public String getFromJackData() {
		return this.fromJackData;
	}

	/**
	 * Setter for the fromJackData property.
	 *
	 * @param fromJackData
	 *            the fromJackData to set.
	 */
	public void setFromJackData(final String fromJackData) {
		this.fromJackData = fromJackData;
	}

	/**
	 * Getter for the fromJackVoice property.
	 *
	 * @return the fromJackVoice property.
	 */
	public String getFromJackVoice() {
		return this.fromJackVoice;
	}

	/**
	 * Setter for the fromJackVoice property.
	 *
	 * @param fromJackVoice
	 *            the fromJackVoice to set.
	 */
	public void setFromJackVoice(final String fromJackVoice) {
		this.fromJackVoice = fromJackVoice;
	}

	/**
	 * Getter for the questionnarie property.
	 *
	 * @return the questionnarie property.
	 */
	public String getQuestionnarie() {
		return this.questionnarie;
	}

	/**
	 * Setter for the questionnarie property.
	 *
	 * @param questionnarie
	 *            the questionnarie to set.
	 */
	public void setQuestionnarie(final String questionnarie) {
		this.questionnarie = questionnarie;
	}

	/**
	 * Getter for the department property.
	 *
	 * @return the department property.
	 */
	public String getDepartment() {
		return this.department;
	}

	/**
	 * Setter for the department property.
	 *
	 * @param department
	 *            the department to set.
	 */
	public void setDepartment(final String department) {
		this.department = department;
	}

	/**
	 * Getter for the moveType property.
	 *
	 * @return the moveType property.
	 */
	public String getMoveType() {
		return this.moveType;
	}

	/**
	 * Setter for the moveType property.
	 *
	 * @param moveType
	 *            the moveType to set.
	 */
	public void setMoveType(final String moveType) {
		this.moveType = moveType;
	}

	/**
	 * Getter for the moveClass property.
	 *
	 * @return the moveClass property.
	 */
	public String getMoveClass() {
		return this.moveClass;
	}

	/**
	 * Setter for the moveClass property.
	 *
	 * @param moveClass
	 *            the moveClass to set.
	 */
	public void setMoveClass(final String moveClass) {
		this.moveClass = moveClass;
	}

	/**
	 * Getter for the toFax property.
	 *
	 * @return the toFax property.
	 */
	public String getToFax() {
		return this.toFax;
	}

	/**
	 * Setter for the toFax property.
	 *
	 * @param toFax
	 *            the toFax to set.
	 */
	public void setToFax(final String toFax) {
		this.toFax = toFax;
	}

	/**
	 * Getter for the fromPhone property.
	 *
	 * @return the fromPhone property.
	 */
	public String getFromPhone() {
		return this.fromPhone;
	}

	/**
	 * Setter for the fromPhone property.
	 *
	 * @param fromPhone
	 *            the fromPhone to set.
	 */
	public void setFromPhone(final String fromPhone) {
		this.fromPhone = fromPhone;
	}

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the accountId property.
	 *
	 * @return the accountId property.
	 */
	public String getAccountId() {
		return this.accountId;
	}

	/**
	 * Setter for the accountId property.
	 *
	 * @param accountId
	 *            the accountId to set.
	 */
	public void setAccountId(final String accountId) {
		this.accountId = accountId;
	}

	/**
	 * Getter for the moveCoordinator property.
	 *
	 * @return the moveCoordinator property.
	 */
	public String getMoveCoordinator() {
		return this.moveCoordinator;
	}

	/**
	 * Setter for the moveCoordinator property.
	 *
	 * @param moveCoordinator
	 *            the moveCoordinator to set.
	 */
	public void setMoveCoordinator(final String moveCoordinator) {
		this.moveCoordinator = moveCoordinator;
	}

	/**
	 * Getter for the dateApproved property.
	 *
	 * @return the dateApproved property.
	 */
	public Date getDateApproved() {
		return this.dateApproved;
	}

	/**
	 * Setter for the dateApproved property.
	 *
	 * @param dateApproved
	 *            the dateApproved to set.
	 */
	public void setDateApproved(final Date dateApproved) {
		this.dateApproved = dateApproved;
	}

	/**
	 * Getter for the activityLogId property.
	 *
	 * @return the activityLogId property.
	 */
	public Integer getActivityLogId() {
		return this.activityLogId;
	}

	/**
	 * Setter for the activityLogId property.
	 *
	 * @param activityLogId
	 *            the activityLogId to set.
	 */
	public void setActivityLogId(final Integer activityLogId) {
		this.activityLogId = activityLogId;
	}

	/**
	 * Getter for the dateIssued property.
	 *
	 * @return the dateIssued property.
	 */
	public Date getDateIssued() {
		return this.dateIssued;
	}

	/**
	 * Setter for the dateIssued property.
	 *
	 * @param dateIssued
	 *            the dateIssued to set.
	 */
	public void setDateIssued(final Date dateIssued) {
		this.dateIssued = dateIssued;
	}

	/**
	 * Getter for the fromTeamId property.
	 *
	 * @return the fromTeamId property.
	 */
	public String getFromTeamId() {
		return this.fromTeamId;
	}

	/**
	 * Setter for the fromTeamId property.
	 *
	 * @param fromTeamId
	 *            the fromTeamId to set.
	 */
	public void setFromTeamId(final String fromTeamId) {
		this.fromTeamId = fromTeamId;
	}

	/**
	 * Getter for the toTeamId property.
	 *
	 * @return the toTeamId property.
	 */
	public String getToTeamId() {
		return this.toTeamId;
	}

	/**
	 * Setter for the toTeamId property.
	 *
	 * @param toTeamId
	 *            the toTeamId to set.
	 */
	public void setToTeamId(final String toTeamId) {
		this.toTeamId = toTeamId;
	}

}
