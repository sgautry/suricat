package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Plain object represents employee's all jack connection, used for Jack DAO.
 *
 * @author zhangyi
 * @since 24.1
 *
 */
public class Jacks {

	private String description = "";

	private String fromVoice = "";

	private String fromData = "";

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the fromVoice property.
	 *
	 * @return the fromVoice property.
	 */
	public String getFromVoice() {
		return this.fromVoice;
	}

	/**
	 * Setter for the fromVoice property.
	 *
	 * @param fromVoice
	 *            the fromVoice to set.
	 */
	public void setFromVoice(final String fromVoice) {
		this.fromVoice = fromVoice;
	}

	/**
	 * Getter for the fromData property.
	 *
	 * @return the fromData property.
	 */
	public String getFromData() {
		return this.fromData;
	}

	/**
	 * Setter for the fromData property.
	 *
	 * @param fromData
	 *            the fromData to set.
	 */
	public void setFromData(final String fromData) {
		this.fromData = fromData;
	}

}