package com.archibus.app.move.common.dao.datasource;

import java.util.List;

import org.json.JSONArray;

import com.archibus.app.move.common.dao.IMoveOrderDao;
import com.archibus.app.move.common.domain.MoveOrder;
import com.archibus.datasource.ObjectDataSourceImpl;
import com.archibus.datasource.data.DataRecord;
import com.archibus.utility.DateTime;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for Move Order.
 *
 * @author Zhang Yi
 *
 */
public class MoveOrderDataSource extends ObjectDataSourceImpl<MoveOrder> implements IMoveOrderDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "mo_id", "id" }, { "em_id", "employeeId" },
			{ "status", "status" }, { "mo_type", "moveType" }, { "mo_class", "moveClass" },
			{ "requestor", "requestor" }, { "dept_contact", "departmentContact" }, { "description", "description" },
			{ "to_bl_id", "toBuilding" }, { "to_fl_id", "toFloor" }, { "to_rm_id", "toRoom" },
			{ "date_start_req", "dateStart" }, { "date_to_perform", "datePerform" }, { "date_created", "dateCreated" },
			{ "phone", "phone" }, { "phone_dept_contact", "phoneDeptContact" }, { "dv_id", "division" },
			{ "dp_id", "department" }, { "from_bl_id", "fromBuilding" }, { "from_fl_id", "fromFloor" },
			{ "from_rm_id", "fromRoom" }, { "from_dv_id", "fromDivision" }, { "from_dp_id", "fromDepartment" },
			{ "to_dv_id", "toDivision" }, { "to_dp_id", "toDepartment" }, { "from_phone", "fromPhone" },
			{ "to_phone", "toPhone" }, { "from_fax", "fromFax" }, { "from_mailstop", "fromMailStop" },
			{ "from_jk_id_data", "fromJackData" }, { "from_jk_id_voice", "fromJackVoice" },
			{ "mo_quest", "questionnarie" }, { "project_id", "projectId" }, { "ac_id", "accountId" },
			{ "mo_coord", "moveCoordinator" }, { "apprv_mgr1", "approveManager1" }, { "apprv_mgr2", "approveManager2" },
			{ "apprv_mgr3", "approveManager3" }, { "apprv_mgr1_status", "approveManager1Status" },
			{ "apprv_mgr2_status", "approveManager2Status" }, { "apprv_mgr3_status", "approveManager3Status" },
			{ "date_app_mgr1", "approveManager1Date" }, { "date_app_mgr2", "approveManager2Date" },
			{ "date_app_mgr3", "approveManager3Date" }, { "date_approved", "dateApproved" },
			{ "date_issued", "dateIssued" }, { "date_requested", "dateRequested" },
			{ "time_requested", "timeRequested" }, { "activity_log_id", "activityLogId" },
			{ "from_team_id", "fromTeamId" }, { "to_team_id", "toTeamId" } };

	/** mo type. */
	private static final String TEAM = "Team";

	private static final String DATEFORMAT = "yyyy-MM-dd";

	/**
	 * Constructs MoveOrderDataSource, mapped to <code>mo</code> table, using
	 * <code>MoveOrder</code> bean.
	 */
	public MoveOrderDataSource() {
		super("moveOrder", "mo");
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

	@Override
	public MoveOrder getMoveOrderByRecord(final DataRecord record) {
		return this.convertRecordToObject(record);
	}

	@Override
	public List<MoveOrder> getMoveOrdersByRestriction(final String restriction) {

		return this.convertRecordsToObjects(this.createCopy().getRecords(restriction));
	}

	@Override
	public void assignEmployeesToTeam(final List<String> emIds, final String teamId, final String projectId,
			final String type, final String dateStartReq, final String dateToPerform) {

		final int len = emIds.size();

		for (int i = 0; i < len; i++) {
			final MoveOrder mo = new MoveOrder();
			mo.setEmployeeId(emIds.get(i));
			mo.setToTeamId(teamId);
			mo.setProjectId(projectId);
			mo.setMoveType(TEAM);
			mo.setDateStart(DateTime.stringToDate(dateStartReq, DATEFORMAT));
			mo.setDatePerform(DateTime.stringToDate(dateToPerform, DATEFORMAT));
			save(mo);
		}

	}

	@Override
	public void assignRoomsToTeam(final JSONArray rooms, final String teamId, final String projectId, final String type,
			final String dateStartReq, final String dateToPerform) {

		final int len = rooms.length();

		for (int i = 0; i < len; i++) {
			final MoveOrder mo = new MoveOrder();
			mo.setToBuilding(rooms.getJSONObject(i).getString("bl_id"));
			mo.setToFloor(rooms.getJSONObject(i).getString("fl_id"));
			mo.setToRoom(rooms.getJSONObject(i).getString("rm_id"));
			mo.setToTeamId(teamId);
			mo.setProjectId(projectId);
			mo.setMoveType(TEAM);
			mo.setDateStart(DateTime.stringToDate(dateStartReq, DATEFORMAT));
			mo.setDatePerform(DateTime.stringToDate(dateToPerform, DATEFORMAT));
			save(mo);
		}

	}

}
