package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.Craftperson;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Craftperson.
 *
 * @author Zhang Yi
 *
 */
public interface ICraftpersonDao extends IDao<Craftperson> {

}
