package com.archibus.app.move.common.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Craftperson.
 * <p>
 * Mapped to cf table.
 *
 * @author Zhang Yi
 *
 */
public class Craftperson {

	/**
	 * ID of the Craftperson.
	 */
	private String id;

	/**
	 * Craftperson Email.
	 */
	private String email;

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter for the email property.
	 *
	 * @return the email property.
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Setter for the email property.
	 *
	 * @param email
	 *            the email to set.
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

}
