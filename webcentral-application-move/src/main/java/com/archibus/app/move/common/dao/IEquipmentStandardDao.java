package com.archibus.app.move.common.dao;

import com.archibus.app.move.common.domain.EquipmentStandard;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for Equipment Standard.
 *
 * Interface to be implemented by EquipmentStandardDataSource.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public interface IEquipmentStandardDao extends IDao<EquipmentStandard> {

}
