package com.archibus.app.move.common.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Move Scenario.
 * <p>
 * Mapped to mo_scenario table.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
public class MoveScenario {

	/**
	 * Move Scenario Id.
	 */
	private String id;

	/**
	 * Move project Id.
	 */
	private String projectId;

	/**
	 * Move Scenario description.
	 */
	private String description;

	/**
	 * Move Scenario date Created.
	 */
	private Date dateCreated;

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter for the dateCreated property.
	 *
	 * @return the dateCreated property.
	 */
	public Date getDateCreated() {
		return this.dateCreated;
	}

	/**
	 * Setter for the dateCreated property.
	 *
	 * @param dateCreated
	 *            the dateCreated to set.
	 */
	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
