package com.archibus.app.move.sql;

import java.util.List;

import com.archibus.app.move.common.util.MoveCommonUtil;
import com.archibus.context.ContextStore;
import com.archibus.datasource.SqlUtils;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.EventHandlerContext;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides SQL operation methods on Move Equipments/Tagged
 * Furniture/Employees/Jacks/Action Items.
 * <p>
 *
 * Used by Move Management Services.
 *
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author zhangyi
 * @since 23.2
 *
 */
public class MoveAssetsSqlOperator {

	/**
	 * Property: DOT. Typical value is TODO. Constant: TODO.
	 */
	private static final String DOT = ".";
	/**
	 * Property: COMMA. Typical value is TODO. Constant: TODO.
	 */
	private static final String COMMA = ",";
	/**
	 * Property: EMPLOYEE. Typical value is TODO. Constant: TODO.
	 */
	private static final String EMPLOYEE = "employee";

	public void updateTaForCloseGroupMove(final String projectId) {

		this.updateTaForCloseMove("mo.project_id=" + MoveCommonUtil.getLiteralString(projectId));

	}

	public void updateTaForCloseInividualMove(final String moId) {

		this.updateTaForCloseMove("mo.mo_id=" + MoveCommonUtil.getLiteralString(moId));

	}

	public void updateTaForCloseMove(final String condition) {
		final StringBuilder sqlUpdate = new StringBuilder();

		if (SqlUtils.isOracle()) {
			sqlUpdate.append("UPDATE ta");
			sqlUpdate.append(" SET (bl_id, fl_id, rm_id) = (SELECT mo.to_bl_id, mo.to_fl_id, mo.to_rm_id FROM mo,mo_ta"
					+ " WHERE ta.ta_id=mo_ta.ta_id AND mo_ta.mo_id=mo.mo_id AND " + condition
					+ " AND mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','Room', 'New Hire')) ");
			sqlUpdate
					.append(" WHERE ta.ta_id = (SELECT mo_ta.ta_id FROM mo,mo_ta WHERE ta.ta_id=mo_ta.ta_id AND mo_ta.mo_id=mo.mo_id AND "
							+ condition);
			sqlUpdate.append(" AND mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','Room','New Hire'))");

		} else {

			sqlUpdate.append("UPDATE ta");
			sqlUpdate.append(" SET bl_id=mo.to_bl_id,fl_id=mo.to_fl_id,rm_id=mo.to_rm_id");
			sqlUpdate.append(" FROM mo,mo_ta");
			sqlUpdate.append(" WHERE ta.ta_id=mo_ta.ta_id AND mo_ta.mo_id=mo.mo_id" + " AND " + condition);
			sqlUpdate.append(" AND mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','Room', 'New Hire')");

		}

		SqlUtils.executeUpdate("ta", sqlUpdate.toString());
	}

	public void updateEmForCloseGroupMove(final String projectId) {

		this.updateEmForCloseMove("project_id=" + MoveCommonUtil.getLiteralString(projectId)+" and mo.from_team_id is null and mo.to_team_id is null ");

	}

	private void updateEmForCloseMove(final String condition) {
		final StringBuilder sqlUpdate = new StringBuilder();

		sqlUpdate.append(" UPDATE em SET ");
		sqlUpdate.append(" em.bl_id= (SELECT mo.to_bl_id FROM mo where " + condition
				+ "  and mo.em_id=em.em_id and mo.to_bl_id in (SELECT bl_id from bl)), ");
		sqlUpdate.append(" em.fl_id= (select mo.to_fl_id from mo where " + condition
				+ " and mo.em_id=em.em_id and mo.to_bl_id in (select bl_id from bl) and mo.to_fl_id in (select fl_id from fl where fl.bl_id=mo.to_bl_id)), ");
		sqlUpdate.append(" em.rm_id= (select mo.to_rm_id from mo where " + condition
				+ " and mo.em_id=em.em_id and mo.to_bl_id in (select bl_id from bl) and mo.to_fl_id in (select fl_id from fl where fl.bl_id=mo.to_bl_id) and mo.to_rm_id in (select rm_id from rm where rm.bl_id=mo.to_bl_id and rm.fl_id=mo.to_fl_id)), ");
		sqlUpdate.append(" em.dv_id= (SELECT mo.to_dv_id FROM mo where " + condition
				+ " and mo.em_id=em.em_id and mo.to_dv_id in (SELECT dv_id from dv)), ");
		sqlUpdate.append(" em.dp_id= (select mo.to_dp_id from mo where " + condition
				+ " and mo.em_id=em.em_id and mo.to_dv_id in (select dv_id from dv) and mo.to_dp_id in (select dp_id from dp where dp.dp_id=mo.to_dp_id)), ");
		sqlUpdate.append(" em.phone= (select mo.to_phone from mo where " + condition + " and mo.em_id=em.em_id), ");
		sqlUpdate.append(" em.fax= (select mo.to_fax from mo where " + condition + " and mo.em_id=em.em_id), ");
		sqlUpdate
				.append(" em.mailstop= (select mo.to_mailstop from mo where " + condition + " and mo.em_id=em.em_id) ");
		sqlUpdate
				.append(" WHERE em.em_id IN (SELECT em_id from mo WHERE mo.status='Completed-Verified' AND mo.mo_type in ('Employee', 'New Hire','Leaving') and "
						+ condition);
		
        // to keep consistent with space transaction, do not update the
        // Employees table when it is not a primary location move
        final String useWorkspaceTransactions =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue("AbSpaceRoomInventoryBAR-UseWorkspaceTransactions");
        if (useWorkspaceTransactions.equals("1")) {
            sqlUpdate.append(
                "                AND NOT EXISTS(SELECT 1 FROM rmpct WHERE rmpct.activity_log_id IS NOT NULL AND rmpct.activity_log_id = mo.activity_log_id AND rmpct.em_id = mo.em_id AND rmpct.primary_em=0)");
        }

        sqlUpdate.append("    )");

		SqlUtils.executeUpdate("em", sqlUpdate.toString());
	}

	public void updateEmForCloseInividualMove(final String moId) {

		this.updateEmForCloseMove("mo_id=" + MoveCommonUtil.getLiteralString(moId));

	}

	public void updateEqForCloseGroupMove(final String projectId) {

		this.updateEqForCloseMove("mo.project_id=" + MoveCommonUtil.getLiteralString(projectId));

	}

	public void updateEqForCloseInividualMove(final String moId) {

		this.updateEqForCloseMove("mo.mo_id=" + MoveCommonUtil.getLiteralString(moId));

	}

	public void updateActionItemsForCloseMove(final String restriction) {

		final String sqlUpdateActLog = "UPDATE activity_log set status='CLOSED', date_closed=${sql.currentDate} where "
				+ restriction;
		SqlUtils.executeUpdate("activity_log", sqlUpdateActLog);

	}

	private void updateEqForCloseMove(final String condition) {

		final StringBuilder sqlUpdate = new StringBuilder();

		if (SqlUtils.isOracle()) {
			sqlUpdate.append("UPDATE eq");
			sqlUpdate.append(" SET (bl_id, fl_id, rm_id) = (SELECT mo.to_bl_id, mo.to_fl_id, mo.to_rm_id FROM mo,mo_eq"
					+ " WHERE eq.eq_id=mo_eq.eq_id AND mo_eq.mo_id=mo.mo_id AND " + condition
					+ " AND mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','Room', 'New Hire'))");
			sqlUpdate
					.append(" WHERE eq.eq_id = (SELECT mo_eq.eq_id FROM mo,mo_eq WHERE eq.eq_id=mo_eq.eq_id AND mo_eq.mo_id=mo.mo_id AND "
							+ condition);
			sqlUpdate.append(" AND mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','Room', 'New Hire'))");

		} else {

			sqlUpdate.append("UPDATE eq");
			sqlUpdate.append(" SET bl_id=mo.to_bl_id,fl_id=mo.to_fl_id,rm_id=mo.to_rm_id");
			sqlUpdate.append(" FROM mo,mo_eq");
			sqlUpdate.append(" WHERE eq.eq_id=mo_eq.eq_id AND mo_eq.mo_id=mo.mo_id" + " AND " + condition);
			sqlUpdate.append(" AND mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','Room','New Hire')");

		}

		SqlUtils.executeUpdate("eq", sqlUpdate.toString());

	}

	public void updateEqForCloseEquipmentMove(final String condition) {

		final StringBuilder sqlUpdate = new StringBuilder();

		sqlUpdate.append("UPDATE eq SET");
		sqlUpdate.append(" eq.bl_id=(SELECT mo.to_bl_id FROM mo where mo.em_id=eq.eq_id and " + condition
				+ " and mo.to_bl_id in (SELECT bl_id from bl)), ");
		sqlUpdate.append(" eq.fl_id=(SELECT mo.to_fl_id FROM mo where mo.em_id=eq.eq_id and " + condition
				+ " and mo.to_bl_id in (select bl_id from bl) and mo.to_fl_id in (select fl_id from fl where fl.bl_id=mo.to_bl_id)), ");
		sqlUpdate.append(" eq.rm_id=(SELECT mo.to_rm_id FROM mo where mo.em_id=eq.eq_id and " + condition
				+ " and mo.to_bl_id in (select bl_id from bl) and mo.to_fl_id in (select fl_id from fl where fl.bl_id=mo.to_bl_id) and mo.to_rm_id in (select rm_id from rm where rm.bl_id=mo.to_bl_id and rm.fl_id=mo.to_fl_id)),");
		sqlUpdate.append(" eq.dv_id=(SELECT mo.to_dv_id FROM mo where mo.em_id=eq.eq_id and " + condition
				+ " and mo.to_dv_id in (SELECT dv_id from dv)), ");
		sqlUpdate.append(" eq.dp_id=(select mo.to_dp_id from mo where mo.em_id=eq.eq_id and " + condition
				+ " and mo.to_dv_id in (select dv_id from dv) and mo.to_dp_id in (select dp_id from dp where dp.dp_id=mo.to_dp_id)) ");
		sqlUpdate.append(" WHERE eq.eq_id IN (SELECT mo.em_id FROM mo WHERE mo.mo_type in ('Equipment') and mo.status='Completed-Verified' and " + condition + ")");

		SqlUtils.executeUpdate("eq", sqlUpdate.toString());

	}

	public void updateJacksForCloseGroupMove(final String projectId) {

		this.updateJacksForCloseMove("mo.project_id=" + MoveCommonUtil.getLiteralString(projectId));

	}

	public void updateJacksForCloseInividualMove(final String moId) {

		this.updateJacksForCloseMove("mo.mo_id=" + MoveCommonUtil.getLiteralString(moId));

	}

	private void updateJacksForCloseMove(final String condition) {

		final String sqlUpdateJacks1 = " UPDATE jk SET "
				+ " em_id= (SELECT mo.em_id FROM mo where jk.jk_id=mo.to_jk_id_voice and " + condition
				+ " and mo.em_id IN (SELECT em_id from em)) "
				+ " WHERE jk_id IN (SELECT to_jk_id_voice FROM mo WHERE mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','New Hire') AND "
				+ condition + ")";
		SqlUtils.executeUpdate("jk", sqlUpdateJacks1);

		final String sqlUpdateJacks2 = " UPDATE jk SET "
				+ " em_id=(SELECT mo.em_id FROM mo where jk.jk_id=mo.to_jk_id_data and " + condition
				+ " and mo.em_id IN (SELECT em_id from em)) "
				+ " WHERE jk_id IN (SELECT to_jk_id_data FROM mo WHERE mo.status='Completed-Verified' AND mo.mo_type IN ('Employee','New Hire') AND "
				+ condition + ")";
		SqlUtils.executeUpdate("jk", sqlUpdateJacks2);

		final String sqlUpdateJacks3 = " UPDATE jk SET "
				+ " em_id = NULL WHERE em_id in (SELECT mo.em_id FROM mo WHERE mo.status='Completed-Verified' AND mo.mo_type='Leaving' AND "
				+ condition + " AND mo.em_id IN (SELECT em_id from em)) ";
		SqlUtils.executeUpdate("jk", sqlUpdateJacks3);
	}

	/**
	 * Insert assements after adding a move of an employee or a room
	 *
	 * @param primaryKeys
	 *            list of primary key
	 * @param emId
	 *            employee code
	 * @param moId
	 *            move order code
	 * @param table
	 *            table name for move asset
	 * @param assetsFor
	 *            table name for move asset
	 *
	 */
	public static void addEmAssetsMove(final List<String> primaryKeys, final String emId, final String moId,
			final String table, final String assetsFor) {

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();

		final String sqlEmIds = assetsFor.equals(EMPLOYEE) ? "em_id," : "";
		final String insertEmIds = assetsFor.equals(EMPLOYEE) ? ("'" + emId + "',") : "";

		final String stdTableName = "eq".equalsIgnoreCase(table) ? "eqstd" : "fnstd";
		final String fieldStd = "eq".equalsIgnoreCase(table) ? "eq" : "fn";

		final String selectFromBlId = "( CASE WHEN  " + table + ".bl_id IS NOT NULL THEN " + table
				+ ".bl_id ELSE (select from_bl_id from mo where mo_id = '" + moId + "') END )";

		final String selectFromFlId = "( CASE WHEN " + table + ".fl_id IS NOT NULL THEN " + table
				+ ".fl_id ELSE (select from_fl_id from mo where mo_id = '" + moId + "') END) ";

		final String selectFromRmId = "(CASE WHEN " + table + ".rm_id IS NOT NULL THEN " + table
				+ ".rm_id ELSE (select from_rm_id from mo where mo_id = '" + moId + "') END)";

		for (int i = 0; i < primaryKeys.size(); i++) {

			final StringBuilder sqlStatment = new StringBuilder();
			sqlStatment.append("INSERT INTO mo_" + table + " (" + table + "_id," + fieldStd + "_std,mo_id," + sqlEmIds
					+ "from_bl_id,from_fl_id,from_rm_id,cost_moving)");
			sqlStatment.append(" (SELECT  " + table + "_id, " + fieldStd + "_std,");
			sqlStatment.append(moId + COMMA);
			sqlStatment.append(insertEmIds);
			sqlStatment.append((assetsFor.equals(EMPLOYEE) ? selectFromBlId : "bl_id") + COMMA);
			sqlStatment.append((assetsFor.equals(EMPLOYEE) ? selectFromFlId : "fl_id") + COMMA);
			sqlStatment.append((assetsFor.equals(EMPLOYEE) ? selectFromRmId : "rm_id") + COMMA);

			final String costMovingSelect = "(select cost_moving from " + stdTableName + " where " + table + DOT
					+ fieldStd + "_std = " + stdTableName + DOT + fieldStd + "_std) ";

			sqlStatment.append(EventHandlerBase.formatSqlIsNull(context, costMovingSelect + ",0 "));

			if (EMPLOYEE.equalsIgnoreCase(assetsFor)) {

				sqlStatment.append(" from " + table + " where em_id='" + emId + "' and " + table + "_id  = '"
						+ primaryKeys.get(i) + "' )");

			} else if ("room".equalsIgnoreCase(assetsFor)) {

				sqlStatment.append("from " + table + " where bl_id = " + selectFromBlId + " and fl_id = "
						+ selectFromFlId + " and rm_id = " + selectFromRmId + " and " + table + "_id = '"
						+ primaryKeys.get(i) + "')");
			}

			SqlUtils.executeUpdate("mo_" + table, sqlStatment.toString());

			// make sure we commit for Oracle
			if (SqlUtils.isOracle()) {
				SqlUtils.commit();
			}

		}
	}
}
