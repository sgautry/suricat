package com.archibus.eventhandler.Moves;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;

@Category({ DatabaseTest.class })
public class MovePaginatedReportGeneratorTest extends DataSourceTestBase {
	MovePaginatedReportGenerator classHandler = null;

	public void testGenerateReport() {
		final String rptType = "group";
		final String projectId = "PROJECT 12";
		final String moveId = "";
		this.classHandler = new MovePaginatedReportGenerator(rptType, projectId, moveId);
		this.classHandler.run();
	}
}
