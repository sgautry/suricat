package com.archibus.eventhandler.Moves;

import java.util.*;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;

public class MovesHandlerTest extends DataSourceTestBase {

	private MovesHandler moveHandler;

	public MovesHandler getMovesHandler() {
		return this.moveHandler;
	}

	public void setMovesHandler(final MovesHandler moveHandler) {
		this.moveHandler = moveHandler;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "/context/core/core-infrastructure.xml", "appContext-test.xml",
				"/com/archibus/app/move/appContext-move-test.xml" };
	}

	/**
	 * calculate employee headcount
	 *
	 * scheduled job that runs on first days of each month and fill
	 * hist_em_count table
	 */
	public void testCalculateEmployeeHeadcount() {
		this.moveHandler.calculateHistoricalEmployeeHeadcount();
	}

	/**
	 * Create Move Scenario
	 *
	 * dataRecord with project_id and scenario_id isDefault true if is the
	 * default scenario created by the app, false otherwise
	 */

	public void testCreateMoveScenario() {
		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo_scenario",
				new String[] { "project_id", "scenario_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo_scenario.project_id", "MARKETING MOVE");
		record.setValue("mo_scenario.scenario_id", "Scenario 2");
		final boolean isDefault = false;

		this.moveHandler.createMoveScenario(record, isDefault);
	}

	/**
	 * Copy Move Scenario
	 */
	public void testCopyMoveScenario() {
		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo_scenario",
				new String[] { "project_id", "scenario_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo_scenario.project_id", "MARKETING MOVE");
		record.setValue("mo_scenario.scenario_id", "Scenario 2");
		final String origScenarioId = "Scenario 1";

		this.moveHandler.copyMoveScenario(record, origScenarioId);
	}

	/**
	 * Delete Move Scenario
	 */
	public void testDeleteMoveScenario() {
		final String scenarioId = "Scenario 2";
		final String projectId = "MARKETING MOVE";

		this.moveHandler.deleteMoveScenario(scenarioId, projectId);
	}

	public void testUpdateMoveProject() {
		final String scenarioId = "Scenario 1";
		final String projectId = "MARKETING MOVE";

		this.moveHandler.updateMoveProject(projectId, scenarioId);
	}

	public void testUpdateMoveScenario() {
		final String scenarioId = "scenario 1";
		final String projectId = "project 1";
		final List<Map<String, Object>> records = null;

		this.moveHandler.updateMoveScenario(projectId, scenarioId, records);

	}

	/*
	 * PAGINATED REPORT
	 */
	public void testOnPaginatedReport() {
		final String rptType = "single";
		final String projectId = "";
		final String moId = "199700142";

		this.moveHandler.onPaginatedReport(rptType, projectId, moId);
	}

	/*
	 * SINGLE MOVES METHODS
	 */
	public void testAddIndividualMove() {

		final Date now = new Date();
		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created" });
		final DataRecord record = ds.createNewRecord();

		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "YEN, USENG");
		record.setValue("mo.description", "Move");
		record.setValue("mo.mo_type", "Employee");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.to_rm_id", "110");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		final boolean addEq = true;
		final boolean addTa = true;

		this.moveHandler.addIndividualMove(record, addEq, addTa);
	}

	public void testAddIndividualMoveNewHire() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created" });
		final DataRecord record = ds.createNewRecord();

		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "BECKWITH, BILL");
		record.setValue("mo.description", "Move New Hire");
		record.setValue("mo.mo_type", "New Hire");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.to_rm_id", "110");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		final boolean addEq = false;
		final boolean addTa = false;

		this.moveHandler.addIndividualMoveNewHire(record, addEq, addTa);
	}

	public void testAddIndividualMoveEmployeeLeaving() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created" });
		final DataRecord record = ds.createNewRecord();

		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "YEN, USENG");
		record.setValue("mo.description", "Move Leaving");
		record.setValue("mo.mo_type", "Leaving");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		final boolean addEq = true;
		final boolean addTa = true;

		this.moveHandler.addIndividualMoveEmployeeLeaving(record, addEq, addTa);
	}

	public void testAddIndividualMoveEquipment() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "2000000029");
		record.setValue("mo.description", "Move Equipment");
		record.setValue("mo.mo_type", "Equipment");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.to_rm_id", "100");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		final boolean addEq = true;
		final boolean addTa = true;

		this.moveHandler.addIndividualMoveEquipment(record, addEq, addTa);
	}

	public void testAddIndividualMoveAsset() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "ASSET1");
		record.setValue("mo.description", "Move Asset");
		record.setValue("mo.mo_type", "Asset");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "16");
		record.setValue("mo.to_rm_id", "110");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		final boolean addEq = true;
		final boolean addTa = true;

		this.moveHandler.addIndividualMoveAsset(record, addEq, addTa);
	}

	public void testAddIndividualMoveRoom() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "HQ|17|110");
		record.setValue("mo.description", "Room");
		record.setValue("mo.mo_type", "Room");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.from_bl_id", "HQ");
		record.setValue("mo.from_fl_id", "17");
		record.setValue("mo.from_rm_id", "110");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "18");
		record.setValue("mo.to_rm_id", "112");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		final boolean addEq = false;
		final boolean addTa = false;

		this.moveHandler.addIndividualMoveRoom(record, addEq, addTa);
	}

	public void testAddActionIndividualMove() {

		final DataSource ds = DataSourceFactory.createDataSourceForFields("activity_log",
				new String[] { "description", "activity_type", "requestor", "mo_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("activity_log.description", " xxx ");
		record.setValue("activity_log.activity_type", "PROPERTY - GENERAL");
		record.setValue("activity_log.requestor", "AFM");
		record.setValue("activity_log.mo_id", 199500014);

		this.moveHandler.addActionIndividualMove(record);
	}

	public void testAddActionGroupMove() {
		new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("activity_log",
				new String[] { "project_id", "description", "activity_type", "requestor" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("activity_log.project_id", "RENOVATION OF HQ");
		record.setValue("activity_log.description", "PROPERTY - MAINTENANCE");
		record.setValue("activity_log.activity_type", "PROPERTY - MAINTENANCE");
		record.setValue("activity_log.requestor", "AFM");

		this.moveHandler.addActionGroupMove(record);
	}

	public void testRequestIndividualMove() {
		final String mo_id = "199500008";
		this.moveHandler.requestIndividualMove(mo_id);
	}

	public void testRouteIndividualMoveForApproval() {

		final String mo_id = "199500008";
		final String apprv_mgr1 = "AFM";
		final String apprv_mgr2 = "";
		final String apprv_mgr3 = "";

		this.moveHandler.routeIndividualMoveForApproval(mo_id, apprv_mgr1, apprv_mgr2, apprv_mgr3);
	}

	public void testApproveIndividualMove() {
		final String mo_id = "199500008";
		final String apprv_mgr1 = "AFM";
		final String apprv_mgr2 = "";
		final String apprv_mgr3 = "";

		this.moveHandler.approveIndividualMove(mo_id, apprv_mgr1, apprv_mgr2, apprv_mgr3);
	}

	public void testAutoApproveIndividualMove() {
		final String mo_id = "199500008";

		this.moveHandler.autoApproveIndividualMove(mo_id);
	}

	public void testRejectIndividualMove() {
		final String mo_id = "199500008";
		final String apprv_mgr1 = "AFM";
		final String apprv_mgr2 = "";
		final String apprv_mgr3 = "";

		this.moveHandler.rejectIndividualMove(mo_id, apprv_mgr1, apprv_mgr2, apprv_mgr3);
	}

	public void testIssueIndividualMove() {
		final String mo_id = "199500008";
		final String requestor = "AFM";

		this.moveHandler.issueIndividualMove(mo_id, requestor);
	}

	public void testCloseIndividualMove() {
		final String mo_id = "199700041";

		this.moveHandler.closeIndividualMove(mo_id);
	}

	/*
	 * GROUP MOVES METHODS
	 */

	public void testAddProjectMoveRecord() {
		final Date now = new Date();
		final DataSource ds = DataSourceFactory.createDataSourceForFields("project", new String[] { "project_id",
				"description", "bl_id", "requestor", "dept_contact", "contact_id", "date_start", "date_end" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("project.project_id", "NEW MARKETING MOVE");
		record.setValue("project.description", "NEW MARKETING MOVE");
		record.setValue("project.bl_id", "HQ");
		record.setValue("project.requestor", "AFM");
		record.setValue("project.dept_contact", "AFM");
		record.setValue("project.date_start", now);
		record.setValue("project.date_end", now);
		record.setValue("project.contact_id", "TUCKER, TIM");

		this.moveHandler.addProjectMoveRecord(record);
	}

	public void testAddProjectMove() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "BARTLETT, JOAN");
		record.setValue("mo.description", "Employee ");
		record.setValue("mo.mo_type", "Employee");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "SMITH, ALBERT");
		record.setValue("mo.from_bl_id", "HQ");
		record.setValue("mo.from_fl_id", "17");
		record.setValue("mo.from_rm_id", "110");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "18");
		record.setValue("mo.to_rm_id", "112");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		this.moveHandler.addProjectMove(record);
	}

	public void testAddProjectMoveNewHire() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "BARTLETT, JOAN");
		record.setValue("mo.description", "New Hire ");
		record.setValue("mo.mo_type", "New Hire");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "YEN, USENG");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "18");
		record.setValue("mo.to_rm_id", "112");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		this.moveHandler.addProjectMoveNewHire(record);
	}

	public void testAddProjectMoveEmployeeLeaving() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "BARTLETT, JOAN");
		record.setValue("mo.description", "Leaving ");
		record.setValue("mo.mo_type", "Leaving");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "YEN, USENG");
		record.setValue("mo.from_bl_id", "HQ");
		record.setValue("mo.from_fl_id", "18");
		record.setValue("mo.from_rm_id", "112");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		this.moveHandler.addProjectMoveEmployeeLeaving(record);
	}

	public void testAddProjectMoveEquipment() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "2000000029");
		record.setValue("mo.description", "Equipment ");
		record.setValue("mo.mo_type", "Equipment");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "YEN, USENG");
		record.setValue("mo.from_bl_id", "HQ");
		record.setValue("mo.from_fl_id", "18");
		record.setValue("mo.from_rm_id", "112");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.to_rm_id", "110");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		this.moveHandler.addProjectMoveEquipment(record);
	}

	public void testAddProjectMoveAsset() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "Asset 22");
		record.setValue("mo.description", "Asset ");
		record.setValue("mo.mo_type", "Asset");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "YEN, USENG");
		record.setValue("mo.from_bl_id", "HQ");
		record.setValue("mo.from_fl_id", "18");
		record.setValue("mo.from_rm_id", "112");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.to_rm_id", "110");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		this.moveHandler.addProjectMoveAsset(record);
	}

	public void testAddProjectMoveRoom() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "status", "em_id", "description", "mo_type", "mo_class", "requestor", "dept_contact",
						"to_bl_id", "to_fl_id", "to_rm_id", "date_start_req", "date_created", "from_bl_id",
						"from_fl_id", "from_rm_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.status", "Created");
		record.setValue("mo.em_id", "HQ|18|112");
		record.setValue("mo.description", "Room ");
		record.setValue("mo.mo_type", "Room");
		record.setValue("mo.mo_class", "N/A");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.dept_contact", "AFM");
		record.setValue("mo.from_bl_id", "HQ");
		record.setValue("mo.from_fl_id", "18");
		record.setValue("mo.from_rm_id", "112");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.to_rm_id", "110");
		record.setValue("mo.date_start_req", now);
		record.setValue("mo.date_created", now);

		this.moveHandler.addProjectMoveRoom(record);
	}

	public void testAddBulkMoves() {
		final Date now = new Date();

		final DataSource ds = DataSourceFactory.createDataSourceForFields("mo",
				new String[] { "from_dv_id", "from_dp_id", "requestor", "to_bl_id", "to_fl_id", "date_start_req",
						"from_bl_id", "from_fl_id", "project_id" });
		final DataRecord record = ds.createNewRecord();
		record.setValue("mo.project_id", "MARKETING MOVE");
		record.setValue("mo.from_dv_id", "ACCESSORIES");
		record.setValue("mo.from_dp_id", "MARKETING");
		record.setValue("mo.from_bl_id", "XC");
		record.setValue("mo.from_fl_id", "04");
		record.setValue("mo.to_bl_id", "HQ");
		record.setValue("mo.to_fl_id", "17");
		record.setValue("mo.requestor", "AFM");
		record.setValue("mo.date_start_req", now);

		this.moveHandler.addBulkMoves(record);
	}

	public void testRequestGroupMove() {
		final String project_id = "MARKETING MOVE";
		this.moveHandler.requestGroupMove(project_id);
	}

	public void testRouteGroupMoveForApproval() {
		final String project_id = "MARKETING MOVE";

		final String apprv_mgr1 = "AFM";
		final String apprv_mgr2 = "";
		final String apprv_mgr3 = "";

		this.moveHandler.routeGroupMoveForApproval(project_id, apprv_mgr1, apprv_mgr2, apprv_mgr3);
	}

	public void testApproveGroupMove() {
		final String project_id = "MARKETING MOVE";

		final String apprv_mgr1 = "AI";
		final String apprv_mgr2 = "";
		final String apprv_mgr3 = "";

		this.moveHandler.approveGroupMove(project_id, apprv_mgr1, apprv_mgr2, apprv_mgr3);
	}

	public void testAutoApproveGroupMove() {
		final String project_id = "MARKETING MOVE";
		this.moveHandler.autoApproveGroupMove(project_id);
	}

	public void testRejectGroupMove() {
		final String project_id = "MARKETING MOVE";

		final String apprv_mgr1 = "AI";
		final String apprv_mgr2 = "";
		final String apprv_mgr3 = "";

		this.moveHandler.rejectGroupMove(project_id, apprv_mgr1, apprv_mgr2, apprv_mgr3);
	}

	public void testIssueGroupMove() {
		final String project_id = "MARKETING MOVE";

		final String requestor = "AFM";

		this.moveHandler.issueGroupMove(project_id, requestor);
	}

	public void testCloseGroupMove() {
		final String project_id = "TOPOGRAPHICAL MAPS";

		this.moveHandler.closeGroupMove(project_id);
	}

	/*
	 * WORK REQUEST
	 */

	public void testGenWorkRequest() {
		final String activityLogId = "1";
		final String projectId = "";
		final String moId = "199500011";
		this.moveHandler.genWorkRequest(activityLogId, projectId, moId);
	}

	public void testOnProcessDeleteRmpctRecord() {
		final String projectId = "MARKETING MOVE";

		this.moveHandler.onProcessDeleteRmpctRecord("project", projectId);
	}

	public void testDeleteRequestedSpaceTransactionsd() {

		final String moIds = "199500008, 199700041";

		this.moveHandler.deleteRequestedSpaceTransactions(moIds);
	}

	public void testUpdateStatusForReviewAndEstimate() {

		final String projectId = "MARKETING MOVE";

		this.moveHandler.updateStatusForReviewAndEstimate("project", projectId);
	}

}
