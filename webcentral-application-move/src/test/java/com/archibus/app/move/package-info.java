/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or 
 * Represents TODO. - if it has state (entity, domain object, model object). 
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author zhangyi
 * @since 23.1
 *
 */
package com.archibus.app.move;