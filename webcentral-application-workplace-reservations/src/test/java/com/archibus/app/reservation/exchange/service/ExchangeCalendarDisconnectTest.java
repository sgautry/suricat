package com.archibus.app.reservation.exchange.service;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.RoomReservation;
import com.archibus.core.test.fixture.category.*;
import com.archibus.utility.ExceptionBase;

import microsoft.exchange.webservices.data.*;

/**
 * Test class for disconnect methods of ExchangeCalendarService.
 *
 * @author Yorik Gerlo
 * @since 23.2
 */
@Category({ DatabaseTest.class, VerySlowTest.class })
public class ExchangeCalendarDisconnectTest extends ExchangeCalendarServiceTestBase {

    /** The Exchange disconnect service. */
    private ExchangeDisconnectService disconnectService;

    /**
     * Test disconnecting an appointment via the calendar service.
     */
    public void testDisconnectAppointment() {
        final RoomReservation reservation = createRoomReservation();
        populateReservation(reservation);
        createAppointment(reservation);
        Assert.assertNotNull(reservation.getUniqueId());

        this.disconnectService.disconnectAppointment(reservation, "test disconnecting a regular appointment", false);
        final Appointment appointment = this.appointmentBinder.bindToAppointment(reservation.getEmail(),
                reservation.getUniqueId());

        try {
            Assert.assertNull(appointment.getLocation());
            Assert.assertNull(this.appointmentHelper.getAppointmentPropertiesHelper().getReservationId(appointment));
        } catch (final ServiceLocalException exception) {
            Assert.fail("Error verifying disconnected appointment - " + exception);
        }
    }

    /**
     * Test creating an appointment via the calendar service.
     */
    public void testDisconnectRecurringAppointment() {
        final RoomReservation reservation = createRoomReservation();
        populateReservation(reservation);
        addRecurrence(reservation);
        createAppointment(reservation);

        Assert.assertNotNull(reservation.getUniqueId());
        this.disconnectService.disconnectAppointmentSeries(reservation, "test disconnecting the series", false);

        try {
            final Appointment master = this.appointmentBinder.bindToAppointment(reservation.getEmail(),
                    reservation.getUniqueId());
            Assert.assertNull(master.getLocation());
            Assert.assertNull(
                    this.appointmentHelper.getAppointmentPropertiesHelper().getRecurringReservationIds(master));
        } catch (final ServiceLocalException exception) {
            Assert.fail("Error verifying disconnected series - " + exception);
        }
    }

    /**
     * Test disconnecting a single occurrence of a recurring meeting.
     */
    public void testDisconnectSingleOccurrence() {
        try {
            // Create a simple recurring appointment.
            final RoomReservation reservation = createRoomReservation();
            populateReservation(reservation);
            addRecurrence(reservation);
            createAppointment(reservation);

            // Disconnect the first one.
            this.disconnectService.disconnectAppointmentOccurrence(reservation, "Disconnect the first occurrence as a test.",
                    false);
            Appointment master = this.appointmentBinder.bindToAppointment(reservation.getEmail(),
                    reservation.getUniqueId());
            Assert.assertEquals(1, master.getModifiedOccurrences().getCount());
            Appointment occurrence = this.appointmentBinder.bindToOccurrence(master.getService(), reservation,
                    master);
            Assert.assertNull(occurrence.getLocation());

            // Disconnect the third one.
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(reservation.getStartDateTime());
            calendar.add(Calendar.DATE, DAYS_IN_WEEK * 2);
            reservation.setStartDateTime(calendar.getTime());
            reservation.setStartDate(new java.sql.Date(reservation.getStartDate().getTime()));
            reservation.setEndDate(reservation.getStartDate());
            this.disconnectService.disconnectAppointmentOccurrence(reservation, "Cancel the third occurrence for testing.",
                    false);
            master = this.appointmentBinder.bindToAppointment(reservation.getEmail(), reservation.getUniqueId());
            Assert.assertEquals(2, master.getModifiedOccurrences().getCount());
            occurrence = this.appointmentBinder.bindToOccurrence(master.getService(), reservation,
                    master);
            Assert.assertNull(occurrence.getLocation());
        } catch (final ExceptionBase exception) {
            Assert.fail(exception.toStringForLogging());
        } catch (final ServiceLocalException exception) {
            Assert.fail(exception.toString());
        }
    }

    /**
     * Set the disconnect service for testing.
     * @param disconnectService the service
     */
    public void setDisconnectService(final ExchangeDisconnectService disconnectService) {
        this.disconnectService = disconnectService;
    }

}
