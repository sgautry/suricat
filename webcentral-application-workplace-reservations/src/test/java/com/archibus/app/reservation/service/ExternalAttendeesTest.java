package com.archibus.app.reservation.service;

import java.text.ParseException;
import java.util.*;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.dao.datasource.*;
import com.archibus.app.reservation.domain.*;
import com.archibus.app.reservation.domain.recurrence.Recurrence;
import com.archibus.core.test.fixture.category.DatabaseTest;

/**
 * Test cases for finding and booking rooms with external attendees.
 */
@Category({ DatabaseTest.class })
public class ExternalAttendeesTest extends ReservationServiceTestBase {

	/**
	 * Error message that indicates the room you tried to book doesn't allow
	 * external attendees.
	 */
	private static final String EXTERNAL_ERROR_MESSAGE = "The room HQ-19-110 is not available for external guests.";

	/** Arrangement type only available for internal guests. */
	private static final String INTERNAL_ARRANGE_TYPE = "Classroom";

	/** Internal attendee e-mails. */
	private static final String INTERNAL_ATTENDEES = "abernathy@tgd.com;abbot@tgd.com";

	/** External attendee e-mails. */
	private static final String EXTERNAL_ATTENDEES = "joel@mail.com;trenton@mail.com";

	/**
	 * Test find available rooms.
	 */
	public void testFindAvailableRooms() {
		final Date startDate = Utils.getDate(10);

		final TimePeriod timePeriod = new TimePeriod(startDate, startDate, this.startTime, this.endTime);
		final RoomReservation roomReservation = new RoomReservation(timePeriod, BL_ID, FL_ID, RM_ID, CONFIG_ID,
				INTERNAL_ARRANGE_TYPE);
		roomReservation.setAttendees(INTERNAL_ATTENDEES);

		List<RoomArrangement> rooms = this.reservationService.findAvailableRooms(roomReservation, null, false, null,
				false, null);
		Assert.assertFalse(rooms.isEmpty());

		roomReservation.setAttendees(EXTERNAL_ATTENDEES);
		rooms = this.reservationService.findAvailableRooms(roomReservation, null, false, null, false, null);
		Assert.assertTrue(rooms.isEmpty());

		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(ARRANGE_TYPE_ID);
		rooms = this.reservationService.findAvailableRooms(roomReservation, null, false, null, false, null);
		Assert.assertFalse(rooms.isEmpty());
	}

	/**
	 * Test finding available rooms with recurrence.
	 */
	public void testFindAvailableRoomsRecurrence() {
		final Recurrence recurrence = createRecurrence();
		final RoomReservation roomReservation = createRoomReservation();
		roomReservation.setStartDate(recurrence.getStartDate());
		roomReservation.setEndDate(recurrence.getStartDate());
		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(INTERNAL_ARRANGE_TYPE);

		roomReservation.setAttendees(INTERNAL_ATTENDEES);
		List<RoomArrangement> rooms = this.reservationService.findAvailableRoomsRecurrence(roomReservation, null, false,
				null, false, recurrence, HQ_TIMEZONE);
		Assert.assertFalse(rooms.isEmpty());

		// reset the reservation date, it's modified when finding rooms
		roomReservation.setStartDate(recurrence.getStartDate());
		roomReservation.setEndDate(recurrence.getStartDate());

		roomReservation.setAttendees(EXTERNAL_ATTENDEES);
		rooms = this.reservationService.findAvailableRoomsRecurrence(roomReservation, null, false, null, false,
				recurrence, HQ_TIMEZONE);
		Assert.assertTrue(rooms.isEmpty());

		// reset the reservation date, it's modified when finding rooms
		roomReservation.setStartDate(recurrence.getStartDate());
		roomReservation.setEndDate(recurrence.getStartDate());

		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(ARRANGE_TYPE_ID);
		rooms = this.reservationService.findAvailableRoomsRecurrence(roomReservation, null, false, null, false,
				recurrence, HQ_TIMEZONE);
		Assert.assertFalse(rooms.isEmpty());
	}

	/**
	 * Test save room reservation in a room that only allows internal attendees.
	 */
	public void testSaveRoomReservation() {
		final RoomReservation reservation = saveRoomReservation(INTERNAL_ARRANGE_TYPE, INTERNAL_ATTENDEES);
		Assert.assertNotNull(reservation.getReserveId());
	}

	/**
	 * Test saving a room reservation with external attendees in a room that
	 * allows it.
	 */
	public void testSaveRoomReservationExternal() {
		final RoomReservation reservation = saveRoomReservation(ARRANGE_TYPE_ID, EXTERNAL_ATTENDEES);
		Assert.assertNotNull(reservation.getReserveId());
	}

	private RoomReservation saveRoomReservation(final String arrangeType, final String attendees) {
		final RoomReservation roomReservation = createRoomReservation();
		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(arrangeType);
		roomReservation.setAttendees(attendees);

		try {
			this.reservationService.saveReservation(roomReservation);
		} catch (final ReservationException exception) {
			Assert.fail(exception.toString());
		}
		return roomReservation;
	}

	/**
	 * Test adding external attendees to a room reservation.
	 */
	public void testAddExternalAttendees() {
		final RoomReservation reservation = saveRoomReservation(INTERNAL_ARRANGE_TYPE, INTERNAL_ATTENDEES);
		Assert.assertNotNull(reservation.getReserveId());

		reservation.setAttendees(EXTERNAL_ATTENDEES);
		try {
			this.reservationService.saveReservation(reservation);
			Assert.fail("This arrangement should not be available for externals.");
		} catch (final ReservationException exception) {
			Assert.assertEquals(EXTERNAL_ERROR_MESSAGE, exception.getPattern());
		}

		reservation.getRoomAllocations().get(0).setArrangeTypeId(ARRANGE_TYPE_ID);
		this.reservationService.saveReservation(reservation);
		Assert.assertEquals(EXTERNAL_ATTENDEES,
				this.reservationService.getActiveReservation(reservation.getReserveId(), null).getAttendees());
	}

	/**
	 * Test changing a reservation with external attendees to an internal room.
	 */
	public void testMoveToInternalRoom() {
		final RoomReservation reservation = saveRoomReservation(ARRANGE_TYPE_ID, EXTERNAL_ATTENDEES);
		Assert.assertNotNull(reservation.getReserveId());

		reservation.getRoomAllocations().get(0).setArrangeTypeId(INTERNAL_ARRANGE_TYPE);
		try {
			this.reservationService.saveReservation(reservation);
			Assert.fail("This arrangement should not be available for externals.");
		} catch (final ReservationException exception) {
			Assert.assertEquals(EXTERNAL_ERROR_MESSAGE, exception.getPattern());
		}
	}

	/**
	 * Test save room reservation with external attendees in a room that doesn't
	 * allow them.
	 */
	public void testSaveRoomReservationNotAllowed() {
		final RoomReservation roomReservation = createRoomReservation();
		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(INTERNAL_ARRANGE_TYPE);
		roomReservation.setAttendees(EXTERNAL_ATTENDEES);

		try {
			this.reservationService.saveReservation(roomReservation);
			fail("External attendees should not be allowed in this room.");
		} catch (final ReservationException e) {
			Assert.assertEquals(EXTERNAL_ERROR_MESSAGE, e.getPattern());
		}
	}

	/**
	 * Test saving a recurring reservation.
	 */
	public void testSaveRecurringRoomReservation() {
		final Recurrence recurrence = createRecurrence();
		final RoomReservation roomReservation = createRoomReservation();
		roomReservation.setStartDate(recurrence.getStartDate());
		roomReservation.setEndDate(recurrence.getStartDate());
		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(INTERNAL_ARRANGE_TYPE);
		roomReservation.setAttendees(INTERNAL_ATTENDEES);

		final List<RoomReservation> reservations = this.reservationService.saveRecurringReservation(roomReservation,
				recurrence, null);
		Assert.assertEquals(recurrence.getNumberOfOccurrences(), Integer.valueOf(reservations.size()));
		for (final RoomReservation reservation : reservations) {
			Assert.assertEquals(Constants.STATUS_CONFIRMED, reservation.getStatus());
		}
	}

	/**
	 * Test saving a recurring reservation with external attendees.
	 */
	public void testSaveRecurringRoomReservationNotAllowed() {
		final Recurrence recurrence = createRecurrence();
		final RoomReservation roomReservation = createRoomReservation();
		roomReservation.setStartDate(recurrence.getStartDate());
		roomReservation.setEndDate(recurrence.getStartDate());
		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(INTERNAL_ARRANGE_TYPE);
		roomReservation.setAttendees(EXTERNAL_ATTENDEES);

		try {
			this.reservationService.saveRecurringReservation(roomReservation, recurrence, null);
			Assert.fail("This room should not allow external guests.");
		} catch (final ReservationException exception) {
			Assert.assertEquals(EXTERNAL_ERROR_MESSAGE, exception.getPattern());
		}
	}

	/**
	 * Test editing a recurring reservation without exceptions.
	 *
	 * @throws ParseException
	 *             when a time string is invalid
	 */
	public void testEditRecurringRoomReservation() {
		final Recurrence recurrence = createRecurrence();
		final RoomReservation roomReservation = createRoomReservation();
		roomReservation.setStartDate(recurrence.getStartDate());
		roomReservation.setEndDate(recurrence.getStartDate());
		roomReservation.getRoomAllocations().get(0).setArrangeTypeId(INTERNAL_ARRANGE_TYPE);
		roomReservation.setAttendees(INTERNAL_ATTENDEES);

		final List<RoomReservation> reservations = this.reservationService.saveRecurringReservation(roomReservation,
				recurrence, null);
		Assert.assertNotNull(roomReservation.getReserveId());

		final RoomReservation reservationForEdit = reservations.get(0);
		reservationForEdit.setAttendees(EXTERNAL_ATTENDEES);

		try {
			this.reservationService.saveRecurringReservation(reservationForEdit, recurrence, null);
			fail("External attendees should not be allowed.");
		} catch (final ReservationException exception) {
			Assert.assertEquals(EXTERNAL_ERROR_MESSAGE, exception.getPattern());
		}
	}

}
