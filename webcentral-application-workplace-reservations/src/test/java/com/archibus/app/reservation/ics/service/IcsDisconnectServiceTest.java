package com.archibus.app.reservation.ics.service;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.RoomReservation;
import com.archibus.app.reservation.service.ReservationServiceTestBase;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.utility.ExceptionBase;

/**
 * Test class for IcsDisconnectService.
 */
@Category({ DatabaseTest.class })
public class IcsDisconnectServiceTest extends ReservationServiceTestBase {

    /** The Calendar service instance being tested. */
    private IcsDisconnectService icsDisconnectService;

    /**
     * Set the disconnect service.
     *
     * @param icsDisconnectService
     *            the new disconnect service
     */
    public void setIcsDisconnectService(final IcsDisconnectService icsDisconnectService) {
        this.icsDisconnectService = icsDisconnectService;
    }

    /**
     * Test sending a disconnect.
     */
    public void testDisconnectAppointment() {
        final RoomReservation roomReservation = createRoomReservation();
        this.reservationService.saveReservation(roomReservation);
        try {
            this.icsDisconnectService.disconnectAppointment(roomReservation, "reservation disconnected", true);
        } catch (final ExceptionBase exception) {
            Assert.fail(exception.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "com/archibus/app/reservation/reservation-no-integration.xml", "com/archibus/app/reservation/adminService.xml" };
    }

}
