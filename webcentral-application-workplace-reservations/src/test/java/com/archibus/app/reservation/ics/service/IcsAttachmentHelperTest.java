package com.archibus.app.reservation.ics.service;

import java.sql.Time;
import java.text.DateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.junit.*;

import com.archibus.app.reservation.domain.TimePeriod;
import com.archibus.app.reservation.domain.recurrence.*;
import com.archibus.app.reservation.ics.domain.*;
import com.archibus.app.reservation.util.TimeZoneConverter;
import com.archibus.utility.Utility;

import junit.framework.TestCase;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.component.*;

/**
 * Test the methods from the IcsAttachmentHelper.
 * <p>
 *
 * @author PROCOS
 * @since 23.2
 */
public final class IcsAttachmentHelperTest extends TestCase {

	/** Constant: MARTIN. Email for one attendee. */
	private static final String MARTIN = "martin@mailinator.com";

	/** Constant: JASON. Email for another attendee. */
	private static final String JASON = "jason@mailinator.com";

	/** Time zone of the HQ building. */
	private static final String HQ_TIMEZONE = "America/New_York";

	/** The bl id. */
	private static final String BL_ID = "HQ";

	/** The fl id. */
	private static final String FL_ID = "19";

	/** The rm id. */
	private static final String RM_ID = "110";

	/** The formatted location. */
	private static final String LOCATION = String.format("%s-%s-%s", BL_ID, FL_ID, RM_ID);

	/** Example daily pattern. */
	private static final String DAILY_PATTERN = "<recurring type=\"day\""
			+ " value1=\"1\" value2=\"\" value3=\"\" value4=\"\" total=\"5\">" + "</recurring>";

	/** Constant: DAILY_FREQ_TEST. */
	private static final String DAILY_FREQ_TEST = "FREQ=DAILY;WKST=SU;UNTIL=%s;INTERVAL=1";

	/** The error message. */
	private static final String ERROR_MSG = "%s does not match";

	/** The subject/summary. */
	private static final String SUBJECT = "TEST SUBJECT";

	/** The body/description. */
	private static final String BODY = "TEST BODY";

	/** Constant: REQUESTOR. The email for the organizer. */
	private static final String REQUESTOR = "linda@mailinator.com";

	/** The meeting duration. */
	private static final int MEETING_LENGTH = 20;

	/** The number of recurrences. */
	private static final int TOTAL_RECURRENCES = 5;

	/** The dummy UID value. */
	private static final String UID = "123";

	/** Constant: ATTENDEE_PARAM. To test the Attendee parameter value. */
	private static final String ATTENDEE_PARAM = ";ROLE=REQ-PARTICIPANT;CN=%s;RSVP=TRUE";

	/** Constant: MAILTO. */
	private static final String MAILTO = "mailto:%s";

	/** Constant: MULTIPLE_ROOMS. */
	private static final String MULTIPLE_ROOMS = "Multiple rooms";

	/** Constant: SEQUENCE. */
	private static final String SEQUENCE = "Sequence";

	/** Constant: DESCRIPTION. */
	private static final String DESCRIPTION = "Description";

	/** Constant: SUMMARY. */
	private static final String SUMMARY = "Summary";

	/** Constant: STATUS. */
	private static final String STATUS = "Status";

	/** Constant: UID_STR. The UID field string. */
	private static final String UID_STR = "UID";

	/** Constant: ORGANIZER. */
	private static final String ORGANIZER = "Organizer";

	/** Constant: END_DATE. */
	private static final String END_DATE = "End date";

	/** Constant: START_DATE. */
	private static final String START_DATE = "Start date";

	/** Constant: LOCATION_STR. The location field string. */
	private static final String LOCATION_STR = "Location";

	/** Constant: VEVENT. */
	private static final String VEVENT = "VEVENT";

	/** Constant: TZID. */
	private static final String TZID = "TZID";

	/** Constant: VTIMEZONE. */
	private static final String VTIMEZONE = "VTIMEZONE";

	/** Constant: METHOD. */
	private static final String METHOD = "Method";

	/** Constant: RRULE. */
	private static final String RRULE = "RRULE";

	/** Constant: UTC. */
	private static final String UTC = "UTC";

	/** Constant: CANCELLED. */
	private static final String CANCELLED = "CANCELLED";

	/** Constant: ATTENDEE. */
	private static final String ATTENDEE = "ATTENDEE";

	/** Constant: SPLITTER. */
	private static final int SPLITTER = 8;

	/**
	 * Test the createIcsAttachment for a new single room reservation.
	 */
	@Test
	public void testCreateNewSingleRoomIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_NEW, null, false, true, false,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), LOCATION, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

	}

	/**
	 * Test the createIcsAttachment for editing a single room reservation.
	 */
	@Test
	public void testCreateEditSingleRoomIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_UPDATE, null, false, true, false,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), LOCATION, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

	}

	/**
	 * Test the createIcsAttachment for canceling a single room reservation.
	 */
	@Test
	public void testCreateCancelSingleRoomIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_CANCEL, null, false, true, false,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), LOCATION, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, STATUS), CANCELLED, meeting.getStatus().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "1", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

	}

	/**
	 * Test the createIcsAttachment for a new conference call reservation.
	 */
	@Test
	public void testCreateNewConfCallIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_NEW, 123, false, true, false,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), MULTIPLE_ROOMS, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

	}

	/**
	 * Test the createIcsAttachment for editing a conference call reservation.
	 */
	@Test
	public void testCreateEditConfCallIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_UPDATE, 123, false, true, false,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), MULTIPLE_ROOMS, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

	}

	/**
	 * Test the createIcsAttachment for canceling a conference call reservation.
	 */
	@Test
	public void testCreateCancelConfCallIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_CANCEL, 123, false, true, false,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), MULTIPLE_ROOMS, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, STATUS), CANCELLED, meeting.getStatus().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "1", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

	}

	/**
	 * Test the createIcsAttachment for a new recurring room reservation.
	 */
	@Test
	public void testCreateNewRecurringRoomIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_NEW, null, true, true, true,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final Date until = TimeZoneConverter.calculateDateTime(
				new Date(time.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES)), HQ_TIMEZONE, UTC);

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), LOCATION, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

		Assert.assertEquals(String.format(ERROR_MSG, RRULE),
				String.format(DAILY_FREQ_TEST, formatAsTimestampString(until)), meeting.getProperty(RRULE).getValue());

	}

	/**
	 * Test the createIcsAttachment for editing a recurring room reservation.
	 */
	@Test
	public void testCreateEditRecurringRoomIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_UPDATE, null, true, true, true,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final Date until = TimeZoneConverter.calculateDateTime(
				new Date(time.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES)), HQ_TIMEZONE, UTC);

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), LOCATION, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

		Assert.assertEquals(String.format(ERROR_MSG, RRULE),
				String.format(DAILY_FREQ_TEST, formatAsTimestampString(until)), meeting.getProperty(RRULE).getValue());

	}

	/**
	 * Test the createIcsAttachment for canceling a recurring room reservation.
	 */
	@Test
	public void testCreateCancelRecurringRoomIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_CANCEL, null, true, true, true,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final Date until = TimeZoneConverter.calculateDateTime(
				new Date(time.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES)), HQ_TIMEZONE, UTC);

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), LOCATION, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "1", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

		Assert.assertEquals(String.format(ERROR_MSG, RRULE),
				String.format(DAILY_FREQ_TEST, formatAsTimestampString(until)), meeting.getProperty(RRULE).getValue());

		Assert.assertEquals(String.format(ERROR_MSG, STATUS), CANCELLED, meeting.getStatus().getValue());

	}

	/**
	 * Test the createIcsAttachment for a new recurring conference call
	 * reservation.
	 */
	@Test
	public void testCreateNewRecurringConfCallIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_NEW, 123, true, true, true,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final Date until = TimeZoneConverter.calculateDateTime(
				new Date(time.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES)), HQ_TIMEZONE, UTC);

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), MULTIPLE_ROOMS, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

		Assert.assertEquals(String.format(ERROR_MSG, RRULE),
				String.format(DAILY_FREQ_TEST, formatAsTimestampString(until)), meeting.getProperty(RRULE).getValue());

	}

	/**
	 * Test the createIcsAttachment for editing a recurring conference call
	 * reservation.
	 */
	@Test
	public void testCreateEditRecurringConferenceCallIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_UPDATE, 123, true, true, true,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final Date until = TimeZoneConverter.calculateDateTime(
				new Date(time.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES)), HQ_TIMEZONE, UTC);

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), MULTIPLE_ROOMS, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertNull(String.format(ERROR_MSG, STATUS), meeting.getStatus());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "0", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

		Assert.assertEquals(String.format(ERROR_MSG, RRULE),
				String.format(DAILY_FREQ_TEST, formatAsTimestampString(until)), meeting.getProperty(RRULE).getValue());

	}

	/**
	 * Test the createIcsAttachment for canceling a recurring conference call
	 * reservation.
	 */
	@Test
	public void testCreateCancelRecurringConferenceCallIcsAttachment() {

		final String[] attendees = { JASON, MARTIN };

		final EmailModel emailModel = new EmailModel(EmailModel.TYPE_CANCEL, 123, true, true, true,
				Locale.US.getLanguage());

		final Date start = Utility.currentDate();
		final Time time = Utility.currentTime();

		final Date end = new Date(time.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH));

		final Date until = TimeZoneConverter.calculateDateTime(
				new Date(time.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES)), HQ_TIMEZONE, UTC);

		final net.fortuna.ical4j.model.Calendar icsCal = IcsCalendarBuilder.createIcsAttachment(
				createDummyIcsModel(emailModel, UID, start, time), attendees, false);

		Assert.assertEquals(String.format(ERROR_MSG, METHOD), emailModel.getMethod(), icsCal.getMethod());

		final VTimeZone tzone = (VTimeZone) icsCal.getComponent(VTIMEZONE);

		Assert.assertEquals(String.format(ERROR_MSG, VTIMEZONE), HQ_TIMEZONE, tzone.getTimeZoneId().getValue());

		final VEvent meeting = (VEvent) icsCal.getComponent(VEVENT);

		Assert.assertEquals(String.format(ERROR_MSG, LOCATION_STR), MULTIPLE_ROOMS, meeting.getLocation().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, START_DATE), formatAsTimestampString(start),
				meeting.getStartDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, END_DATE), formatAsTimestampString(end),
				meeting.getEndDate().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, ORGANIZER), String.format(MAILTO, REQUESTOR),
				meeting.getOrganizer().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, UID_STR), UID, meeting.getUid().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SUMMARY), SUBJECT, meeting.getSummary().getValue());
		Assert.assertEquals(String.format(ERROR_MSG, DESCRIPTION), BODY, meeting.getDescription().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, SEQUENCE), "1", meeting.getSequence().getValue());

		Assert.assertEquals(String.format(ERROR_MSG, TZID), HQ_TIMEZONE, meeting.getProperty(TZID).getValue());

		for (final Property attendee : meeting.getProperties(ATTENDEE)) {

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getParameters().toString(), String.format(ATTENDEE_PARAM, JASON))
							|| StringUtils.equals(attendee.getParameters().toString(),
									String.format(ATTENDEE_PARAM, MARTIN)));

			Assert.assertTrue(String.format(ERROR_MSG, ATTENDEE),
					StringUtils.equals(attendee.getValue(), String.format(MAILTO, JASON))
							|| StringUtils.equals(attendee.getValue(), String.format(MAILTO, MARTIN)));
		}

		Assert.assertEquals(String.format(ERROR_MSG, RRULE),
				String.format(DAILY_FREQ_TEST, formatAsTimestampString(until)), meeting.getProperty(RRULE).getValue());

		Assert.assertEquals(String.format(ERROR_MSG, STATUS), CANCELLED, meeting.getStatus().getValue());

	}

	/**
	 * Auxiliary method to format the date as a timestamp string.
	 *
	 * @param date
	 *            the date to format
	 * @return the formatted string
	 */
	private String formatAsTimestampString(final Date date) {

		final DateFormat dateFormatter = new java.text.SimpleDateFormat("yyyyMMddHHmmss");

		final String formatted = dateFormatter.format(date);

		return formatted.substring(0, SPLITTER) + "T" + formatted.substring(SPLITTER);
	}

	/**
	 * Auxiliary method to create a dummy IcsModel.
	 *
	 * @param emailModel
	 *            the email type model
	 * @param uid
	 *            the dummy UID
	 * @param start
	 *            the start date
	 * @param startTime
	 *            the start time
	 * @return the dummy IcsModel
	 */
	private IcsModel createDummyIcsModel(final EmailModel emailModel, final String uid, final Date start,
			final Time startTime) {

		final MeetingLocationModel location = new MeetingLocationModel();
		location.setTimezone(HQ_TIMEZONE);
		if (emailModel.isConferenceCall()) {
			location.setLocation(MULTIPLE_ROOMS);
		} else {
			location.setLocation(LOCATION);
		}

		final Time endTime = new Time(
				new Date(startTime.getTime() + TimeUnit.MINUTES.toMillis(MEETING_LENGTH)).getTime());

		final IcsModel model = new IcsModel(location, emailModel, SUBJECT, BODY, REQUESTOR, uid,
				new TimePeriod(start, null, startTime, endTime));

		if (emailModel.isAllRecurrences()) {

			final Date end = new Date(start.getTime() + TimeUnit.DAYS.toMillis(TOTAL_RECURRENCES));

			model.setUntilDate(TimeZoneConverter.calculateDateTime(end, HQ_TIMEZONE, UTC));

			final Recurrence rec = RecurrenceParser.parseRecurrence(start, start, DAILY_PATTERN);
			model.setRecurrence((AbstractIntervalPattern) rec);

			if (emailModel.isChange()) {
				// canceled tomorrow's meeting
				final List<Date> cancelDates = new ArrayList<Date>();
				cancelDates.add(new Date(start.getTime() + TimeUnit.DAYS.toMillis(1)));

				model.setExceptionDates(cancelDates);
			}
		} else if (emailModel.isRecurring()) {
			model.setRecurrenceId(start);
		}

		return model;
	}
}
