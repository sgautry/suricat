package com.archibus.app.reservation.exchange.service;

import java.net.*;
import java.text.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.junit.Assert;

import junit.framework.TestCase;
import microsoft.exchange.webservices.data.*;

/**
 * Some ad-hoc tests for Exchange Web Services, without database connection.
 *
 * @author Yorik Gerlo
 */
public class ExchangeWebServiceTest extends TestCase {

	/** The logger. */
	private final Logger logger = Logger.getLogger(this.getClass());

	private final ExchangeVersion exchangeVersion = ExchangeVersion.Exchange2007_SP1;
	private final String email = "";
	private final String username = "";
	private final String password = "";
	private final String url = "";

	/**
	 * Check if the properties defined above have been configured.
	 * 
	 * @return true if all properties are filled in, false if one is empty
	 */
	private boolean configured() {
		return !(this.username.isEmpty() || this.email.isEmpty() || this.password.isEmpty() || this.url.isEmpty());
	}

	/**
	 * Test retrieving free/busy information from the Exchange Server without
	 * impersonation.
	 */
	public void testGetFreeBusy() {
		if (!configured()) {
			Assert.fail("This test requires manual configuration.");
		}

		final ExchangeService service = getExchangeService();
		// Create a list of attendees for which to request availability
		// information and meeting time suggestions.

		final List<AttendeeInfo> attendees = new ArrayList<AttendeeInfo>();
		attendees.add(new AttendeeInfo(this.email));
		GetUserAvailabilityResults results = null;

		try {
			final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			final Date start = formatter.parse("2016/10/20");
			final Date end = formatter.parse("2016/12/14");

			// Call the availability service.
			results = service.getUserAvailability(attendees, new TimeWindow(start, end),
					AvailabilityData.FreeBusyAndSuggestions);

		} catch (final ParseException exception) {
			Assert.fail("Invalid date - " + exception.toString());
		} catch (final Exception e) {
			Assert.fail("Error calling Exchange Web Service - " + e.toString());
		}
		Assert.assertNotNull(results);
		Assert.assertNotNull(results.getAttendeesAvailability());
		Assert.assertEquals(1, results.getAttendeesAvailability().getCount());

		// Output attendee availability information.
		int attendeeIndex = 0;

		for (final AttendeeAvailability attendeeAvailability : results.getAttendeesAvailability()) {
			this.logger.info("Availability for [" + attendees.get(attendeeIndex).getSmtpAddress() + "]: ");
			if (attendeeAvailability.getErrorCode() == ServiceError.NoError) {
				if (attendeeAvailability.getCalendarEvents().isEmpty()) {
					this.logger.info("- No events found");
					continue;
				}
				for (final CalendarEvent calendarEvent : attendeeAvailability.getCalendarEvents()) {
					this.logger.info("- Calendar event");
					this.logger.info("  Start time: " + calendarEvent.getStartTime().toString());
					this.logger.info("  End time: " + calendarEvent.getEndTime().toString());
					this.logger.info("  Status: " + calendarEvent.getFreeBusyStatus().toString());

					if (calendarEvent.getDetails() != null) {
						// this is populated if the service account has full
						// access to the user's calendar
						this.logger.info("  Subject: " + calendarEvent.getDetails().getSubject());
					}
				}
			}
			attendeeIndex++;
		}
	}

	/**
	 * Get an Exchange Web Service client.
	 *
	 * @return the client object
	 */
	private ExchangeService getExchangeService() {
		final ExchangeService service = new ExchangeService(this.exchangeVersion);
		final ExchangeCredentials credentials = new WebCredentials(this.username, this.password);
		service.setCredentials(credentials);
		try {
			service.setUrl(new URI(this.url));
		} catch (final URISyntaxException e) {
			fail("Invalid URL: " + e.toString());
		}

		return service;
	}

}
