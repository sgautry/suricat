package com.archibus.app.reservation.ics.service;

import java.util.List;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.*;
import com.archibus.app.reservation.service.ReservationServiceTestBase;
import com.archibus.core.test.fixture.category.DatabaseTest;

/**
 * Test class for WebCentralCalendarService.
 */
@Category({ DatabaseTest.class })
public class IcsAttendeeServiceTest extends ReservationServiceTestBase {

    /** The Availability service instance being tested. */
    private IcsAttendeeService icsAttendeeService;

    /**
     * Set the WebCentral availability service.
     *
     * @param webCentralAvailabilityService
     *            the new WebCentral availability service
     */
    public void setIcsAttendeeService(final IcsAttendeeService icsAttendeeService) {
        this.icsAttendeeService = icsAttendeeService;
    }

    /**
     * Test getting attendee response status.
     */
    public void testGetAttendeesResponseStatus() {
        final RoomReservation reservation = createRoomReservation();
        final List<AttendeeResponseStatus> responses = this.icsAttendeeService
                .getAttendeesResponseStatus(reservation);
        Assert.assertEquals(reservation.getAttendees().split(";").length, responses.size());
        for (final AttendeeResponseStatus response : responses) {
            Assert.assertEquals(AttendeeResponseStatus.ResponseStatus.Unknown, response.getResponseStatus());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "com/archibus/app/reservation/reservation-no-integration.xml", "com/archibus/app/reservation/adminService.xml" };
    }

}
