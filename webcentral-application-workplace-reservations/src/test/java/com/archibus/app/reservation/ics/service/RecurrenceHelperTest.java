package com.archibus.app.reservation.ics.service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import com.archibus.app.reservation.domain.recurrence.*;

import junit.framework.TestCase;
import net.fortuna.ical4j.model.DateTime;

/**
 * Test the methods from the RecurrenceHelper.
 * <p>
 *
 * @author PROCOS
 * @since 23.2
 */
public final class RecurrenceHelperTest extends TestCase {

    /** The constant value 5, for 5 occurrences. */
    private static final int FIVE = 5;

    /** Date format used in ICS recurrence patterns. */
    private static final String DATEFORMAT = "yyyyMMdd";

    /** Pattern mismatch error message. */
    private static final String PATTERN_MISMATCH =
            "Failed to get the expected pattern";

    /** Example daily pattern. */
    private static final String DAILY_PATTERN = "<recurring type=\"day\""
            + " value1=\"1\" value2=\"\" value3=\"\" value4=\"\" total=\"5\">"
            + "</recurring>";

    /** Example weekly pattern. */
    private static final String WEEKLY_PATTERN = "<recurring type=\"week\""
            + " value1=\"1,1,0,0,0,1,1\" value2=\"1\" value3=\"\" value4=\"\""
            + " total=\"5\"></recurring>";

    /** Example fixed monthly pattern. */
    private static final String MONTHLY_PATTERN = "<recurring type=\"month\""
            + " value1=\"8\" value2=\"\" value3=\"1\" value4=\"\" total=\"5\">"
            + "</recurring>";

    /** Example relative monthly pattern. */
    private static final String MONTHLY_PATTERN2 = "<recurring type=\"month\""
            + " value1=\"3rd\" value2=\"sat\" value3=\"1\" value4=\"\""
            + " total=\"5\"></recurring>";

    /** Example fixed yearly pattern. */
    private static final String YEARLY_PATTERN = "<recurring type=\"year\""
            + " value1=\"9\" value2=\"mar\" value3=\"1\" value4=\"\""
            + " total=\"3\"></recurring>";

    /** Example relative yearly pattern. */
    private static final String YEARLY_PATTERN2 = "<recurring type=\"year\""
            + " value1=\"last\" value2=\"thu\" value3=\"may\" value4=\"2\""
            + " total=\"3\"></recurring>";

    /**
     * Test the getRecurringRulePattern for a Daily pattern.
     */
    @Test
    public void testRecurringRuleDailyPattern() {

        final Date start = new Date();
        final Date end =
                new Date(start.getTime() + TimeUnit.DAYS.toMillis(FIVE));

        final Recurrence rec =
                RecurrenceParser.parseRecurrence(start, start, DAILY_PATTERN);
        final DateTime until = new DateTime(end);

        final StringBuilder builder = new StringBuilder("FREQ=DAILY;UNTIL=");
        builder.append(new java.text.SimpleDateFormat(DATEFORMAT).format(end));

        Assert.assertEquals(PATTERN_MISMATCH,
            builder.toString(), RecurrenceHelper
                .getRecurringRulePattern((AbstractIntervalPattern) rec, until));

    }

    /**
     * Test the getRecurringRulePattern for a Weekly pattern.
     */
    @Test
    public void testRecurringRuleWeeklyPattern() {

        final int totalRecs = 7 * 5;
        final Date start = new Date();
        final Date end =
                new Date(start.getTime() + TimeUnit.DAYS.toMillis(totalRecs));

        final Recurrence rec =
                RecurrenceParser.parseRecurrence(start, start, WEEKLY_PATTERN);
        final DateTime until = new DateTime(end);

        final StringBuilder builder = new StringBuilder(36);
        builder.append("FREQ=WEEKLY;UNTIL=")
            .append(new java.text.SimpleDateFormat(DATEFORMAT).format(end))
            .append(";BYDAY=MO,TU,SA,SU");

        Assert.assertEquals(PATTERN_MISMATCH,
            builder.toString(), RecurrenceHelper
                .getRecurringRulePattern((AbstractIntervalPattern) rec, until));
    }

    /**
     * Test the getRecurringRulePattern for a Monthly pattern, on a specific
     * day.
     */
    @Test
    public void testRecurringRuleMonthlyDayOfMonthPattern() {

        final int totalRecs = 31 * 5;
        final Date start = new Date();
        final Date end =
                new Date(start.getTime() + TimeUnit.DAYS.toMillis(totalRecs));

        final Recurrence rec =
                RecurrenceParser.parseRecurrence(start, start, MONTHLY_PATTERN);
        final DateTime until = new DateTime(end);

        final StringBuilder builder = new StringBuilder(36);
        builder.append("FREQ=MONTHLY;UNTIL=")
            .append(new java.text.SimpleDateFormat(DATEFORMAT).format(end))
            .append(";BYMONTHDAY=8");

        Assert.assertEquals(PATTERN_MISMATCH,
            builder.toString(), RecurrenceHelper
                .getRecurringRulePattern((AbstractIntervalPattern) rec, until));
    }

    /**
     * Test the getRecurringRulePattern for a Monthly pattern, on a set week
     * position.
     */
    @Test
    public void testRecurringRuleMonthlyDayOfWeekPattern() {

        final int totalRecs = 31 * 5;
        final Date start = new Date();
        final Date end =
                new Date(start.getTime() + TimeUnit.DAYS.toMillis(totalRecs));

        final Recurrence rec = RecurrenceParser.parseRecurrence(start, start,
            MONTHLY_PATTERN2);
        final DateTime until = new DateTime(end);

        final StringBuilder builder = new StringBuilder(39);
        builder.append("FREQ=MONTHLY;UNTIL=")
            .append(new java.text.SimpleDateFormat(DATEFORMAT).format(end))
            .append(";BYDAY=SA;BYSETPOS=3");

        Assert.assertEquals(PATTERN_MISMATCH,
            builder.toString(), RecurrenceHelper
                .getRecurringRulePattern((AbstractIntervalPattern) rec, until));
    }

    /**
     * Test the getRecurringRulePattern for a Yearly pattern, on a specific day.
     */
    @Test
    public void testRecurringRuleYearlyDayOfMonthPattern() {
        // Yearly
        final int totalRecs = 365 * 3;
        final Date start = new Date();
        final Date end =
                new Date(start.getTime() + TimeUnit.DAYS.toMillis(totalRecs));

        final Recurrence rec =
                RecurrenceParser.parseRecurrence(start, start, YEARLY_PATTERN);
        final DateTime until = new DateTime(end);

        final StringBuilder builder = new StringBuilder(64);
        builder.append("FREQ=YEARLY;UNTIL=")
            .append(new java.text.SimpleDateFormat(DATEFORMAT).format(end))
            .append(";BYMONTH=3;BYMONTHDAY=9");

        Assert.assertEquals(PATTERN_MISMATCH,
            builder.toString(), RecurrenceHelper
                .getRecurringRulePattern((AbstractIntervalPattern) rec, until));
    }

    /**
     * Test the getRecurringRulePattern for a Yearly pattern, on a set week
     * position.
     */
    @Test
    public void testRecurringRuleYearlyDayOfWeekPattern() {
        // Yearly
        final int totalRecs = 365 * 3;
        final Date start = new Date();
        final Date end =
                new Date(start.getTime() + TimeUnit.DAYS.toMillis(totalRecs));

        final Recurrence rec =
                RecurrenceParser.parseRecurrence(start, start, YEARLY_PATTERN2);
        final DateTime until = new DateTime(end);

        final StringBuilder builder = new StringBuilder(64);
        builder.append("FREQ=YEARLY;UNTIL=")
            .append(new java.text.SimpleDateFormat(DATEFORMAT).format(end))
            .append(";BYDAY=TH;BYSETPOS=5;BYMONTH=5");

        Assert.assertEquals(PATTERN_MISMATCH,
            builder.toString(), RecurrenceHelper
                .getRecurringRulePattern((AbstractIntervalPattern) rec, until));
    }

}
