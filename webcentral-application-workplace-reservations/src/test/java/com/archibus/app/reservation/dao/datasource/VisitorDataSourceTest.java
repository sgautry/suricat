package com.archibus.app.reservation.dao.datasource;

import java.util.List;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.Visitor;
import com.archibus.app.reservation.util.DataSourceUtils;
import com.archibus.context.ContextStore;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.datasource.data.DataRecord;

/**
 * Test for VisitorDataSource.
 */
@Category({ DatabaseTest.class })
public class VisitorDataSourceTest extends DataSourceTestBase {

	/** The data source under test. */
	private VisitorDataSource visitorDataSource;

	/**
	 * Test method for the primary functionality.
	 */
	public void testVisitors() {
		Assert.assertNotNull(ContextStore.get().getDatabase());

		this.visitorDataSource.setApplyVpaRestrictions(DataSourceUtils.isVpaEnabled());
		final List<DataRecord> allRecords = this.visitorDataSource.getRecords();

		Assert.assertNotNull(allRecords);

		final List<Visitor> allVisitors = this.visitorDataSource.getAllVisitors();

		Assert.assertNotNull(allVisitors);

		Assert.assertEquals(allRecords.size(), allVisitors.size());
	}

	/**
	 * Test method for VisitorDataSource.get().
	 */
	public void testGetVisitor() {
		final List<Visitor> allVisitors = this.visitorDataSource.getAllVisitors();
		if (!allVisitors.isEmpty()) {
			final Visitor visitor = this.visitorDataSource.get(allVisitors.get(0).getVisitorId());
			Assert.assertNotNull(visitor);
			Assert.assertEquals(allVisitors.get(0).getEmail(), visitor.getEmail());
		}
	}

	/**
	 * Set the VisitorDataSource.
	 *
	 * @param visitorDataSource
	 *            the data source to set.
	 */
	public void setVisitorDataSource(final VisitorDataSource visitorDataSource) {
		this.visitorDataSource = visitorDataSource;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.archibus.fixture.IntegrationTestBase#getConfigLocations()
	 */
	@Override
	protected final String[] getConfigLocations() {
		return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
				"com/archibus/app/reservation/dao/datasource/reservation-datasources.xml",
				"com/archibus/app/reservation/adminService.xml"};
	}

}
