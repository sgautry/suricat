package com.archibus.app.reservation.service.helpers;

import java.util.*;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.*;
import com.archibus.app.reservation.domain.recurrence.*;
import com.archibus.app.reservation.service.*;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.utility.LocalDateTimeUtil;

/**
 * Test for Reservation Service Helper.
 *
 * @author Yorik Gerlo
 */
@Category({ DatabaseTest.class })
public class ReservationServiceHelperTest extends ReservationServiceTestBase {
	// TODO this is actually a test for RecurrenceHelper.

	/** The recurrence pattern used in unit tests. */
	private Recurrence recurrence;

	/**
	 * Used in several unit tests to indicate how many occurrences are skipped.
	 */
	private int skipCount;

	/**
	 * Used in several unit tests to track all dates in a recurrence pattern.
	 */
	private List<Date> allDates;

	/**
	 * {@inheritDoc}
	 *
	 * @throws Exception
	 *             when setup fails
	 *             <p>
	 *             Suppress Warning "PMD.SignatureDeclareThrowsException"
	 *             <p>
	 *             Justification: the overridden method also throws it.
	 */
	@SuppressWarnings({ "PMD.SignatureDeclareThrowsException" })
	@Override
	public void onSetUp() throws Exception {
		super.onSetUp();

		this.recurrence = createRecurrenceStartingInThePast();
		this.allDates = RecurrenceService.getDateList(this.recurrence.getStartDate(), this.recurrence.getEndDate(),
				this.recurrence.toString());
		this.skipCount = RecurrenceHelper.moveToNextOccurrence(this.recurrence, HQ_TIMEZONE);
		Assert.assertEquals(this.skipCount, this.recurrence.getNumberOfSkippedOccurrences());

		// reset the recurrence to its original state
		this.recurrence = createRecurrenceStartingInThePast();
	}

	/**
	 * Test moving a normal recurring reservation to the next occurrence (i.e.
	 * not in the past).
	 */
	public void testMoveToNextOccurrence() {
		final RoomReservation roomReservation = createReservationForRecurrenceTest(HQ_TIMEZONE,
				this.recurrence.getStartDate());
		final Date originalStartDate = roomReservation.getStartDate();
		this.skipCount = RecurrenceHelper.moveToNextOccurrence(roomReservation, this.recurrence);

		Assert.assertTrue(this.skipCount > 0);
		Assert.assertEquals(this.skipCount, this.recurrence.getNumberOfSkippedOccurrences());
		Assert.assertEquals(roomReservation.getStartDate(), this.recurrence.getStartDate());
		Assert.assertTrue(roomReservation.getStartDate().after(originalStartDate));

		int index = 0;
		while (index < this.allDates.size()) {
			if (this.allDates.get(index).equals(roomReservation.getStartDate())) {
				break;
			}
			++index;
		}

		Assert.assertEquals(index, this.skipCount);
		Assert.assertEquals(this.allDates.get(index), roomReservation.getStartDate());
	}

	/**
	 * Test moving a recurring reservation to the next occurrence having
	 * cancelled the occurrence which should be the next one.
	 */
	public void testMoveToNextOccurrenceCancelled() {
		final List<OccurrenceInfo> infos = new ArrayList<OccurrenceInfo>();
		final OccurrenceInfo info = new OccurrenceInfo();
		info.setOriginalDate(this.allDates.get(this.skipCount));
		info.setCancelled(true);
		infos.add(info);
		this.recurrence.setExceptions(infos);

		final int skipCountWithCancel = RecurrenceHelper.moveToNextOccurrence(this.recurrence, HQ_TIMEZONE);
		Assert.assertEquals(this.skipCount + 1, skipCountWithCancel);
		Assert.assertEquals(this.allDates.get(skipCountWithCancel), this.recurrence.getStartDate());
		Assert.assertEquals(skipCountWithCancel, this.recurrence.getNumberOfSkippedOccurrences());
	}

	/**
	 * Test moving a customized recurring reservation to the next occurrence,
	 * where the expected next occurrence is modified to occur in the past.
	 */
	public void testMoveToNextOccurrenceModifiedToThePast() {
		final List<OccurrenceInfo> infos = new ArrayList<OccurrenceInfo>();
		final OccurrenceInfo info = new OccurrenceInfo();
		info.setOriginalDate(this.allDates.get(this.skipCount));
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(LocalDateTimeUtil.currentLocalDateForTimeZone(HQ_TIMEZONE));
		calendar.add(Calendar.DATE, -1);
		info.setModifiedTimePeriod(new TimePeriod(calendar.getTime(), calendar.getTime(), HQ_TIMEZONE));
		infos.add(info);
		this.recurrence.setExceptions(infos);

		final int skipCountWithModification = RecurrenceHelper.moveToNextOccurrence(this.recurrence, HQ_TIMEZONE);
		Assert.assertEquals(this.skipCount + 1, skipCountWithModification);
		Assert.assertEquals(this.allDates.get(skipCountWithModification), this.recurrence.getStartDate());
		Assert.assertEquals(skipCountWithModification, this.recurrence.getNumberOfSkippedOccurrences());
	}

	/**
	 * Test moving a customized recurring reservation to the next occurrence,
	 * where the last date that is expected to be skipped is moved to today.
	 */
	public void testMoveToNextOccurrenceModifiedToTheFuture() {
		final List<OccurrenceInfo> infos = new ArrayList<OccurrenceInfo>();
		final OccurrenceInfo info = new OccurrenceInfo();
		info.setOriginalDate(this.allDates.get(this.skipCount - 1));
		final Date today = LocalDateTimeUtil.currentLocalDateForTimeZone(HQ_TIMEZONE);
		info.setModifiedTimePeriod(new TimePeriod(today, today, HQ_TIMEZONE));
		infos.add(info);
		this.recurrence.setExceptions(infos);

		final int skipCountWithModification = RecurrenceHelper.moveToNextOccurrence(this.recurrence, HQ_TIMEZONE);
		Assert.assertEquals(this.skipCount - 1, skipCountWithModification);
		Assert.assertEquals("The recurrence pattern should still have the date in the past",
				this.allDates.get(skipCountWithModification), this.recurrence.getStartDate());
		Assert.assertTrue("The modified start date should be after the recurrence pattern start date",
				this.recurrence.getModifiedTimePeriod(this.allDates.get(skipCountWithModification)).getStartDate()
						.after(this.recurrence.getStartDate()));
		Assert.assertEquals(skipCountWithModification, this.recurrence.getNumberOfSkippedOccurrences());
	}
}
