package com.archibus.app.reservation.domain.recurrence;

import java.util.*;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.UnitTest;

import junit.framework.TestCase;

/**
 * The Class RecurrenceTest.
 *
 * Test parsing the recurrence patterns with start date and number of
 * occurrences.
 *
 */
@Category({ UnitTest.class })
public class RecurrenceParserTest extends TestCase {

	/** Example daily pattern. */
	private static final String DAILY_PATTERN = "<recurring type=\"day\" value1=\"1\" value2=\"\" value3=\"\" value4=\"\" total=\"5\"></recurring>";

	/** Example weekly pattern. */
	private static final String WEEKLY_PATTERN = "<recurring type=\"week\" value1=\"1,1,0,0,0,1,1\" value2=\"1\" value3=\"\" value4=\"\" total=\"5\"></recurring>";

	/** Example fixed monthly pattern. */
	private static final String MONTHLY_PATTERN = "<recurring type=\"month\" value1=\"8\" value2=\"\" value3=\"1\" value4=\"\" total=\"5\"></recurring>";

	/** Example relative monthly pattern. */
	private static final String MONTHLY_PATTERN2 = "<recurring type=\"month\" value1=\"3rd\" value2=\"sat\" value3=\"1\" value4=\"\" total=\"5\"></recurring>";

	/** Example fixed yearly pattern. */
	private static final String YEARLY_PATTERN = "<recurring type=\"year\" value1=\"9\" value2=\"mar\" value3=\"1\" value4=\"\" total=\"3\"></recurring>";

	/** Example relative yearly pattern. */
	private static final String YEARLY_PATTERN2 = "<recurring type=\"year\" value1=\"last\" value2=\"thu\" value3=\"may\" value4=\"2\" total=\"3\"></recurring>";

	/**
	 * Test parse weekly recurrence.
	 */
	public void testParseDailyRecurrence() {
		final Date startDate = new Date();

		final Recurrence dailyRecurrence = RecurrenceParser.parseRecurrence(startDate, null, DAILY_PATTERN);
		Assert.assertEquals(DailyPattern.class, dailyRecurrence.getClass());
		Assert.assertEquals(Integer.valueOf(5), dailyRecurrence.getNumberOfOccurrences());
		Assert.assertEquals(startDate, dailyRecurrence.getStartDate());
		Assert.assertNull(dailyRecurrence.getEndDate());
		Assert.assertEquals(1, ((AbstractIntervalPattern) dailyRecurrence).getInterval());

		Assert.assertNotNull(dailyRecurrence.toString());
	}

	/**
	 * Test parse weekly recurrence.
	 */
	public void testParseWeeklyRecurrence() {
		final Date startDate = new Date();

		final Recurrence weeklyRecurrence = RecurrenceParser.parseRecurrence(startDate, null, WEEKLY_PATTERN);
		Assert.assertEquals(WeeklyPattern.class, weeklyRecurrence.getClass());
		Assert.assertEquals(Integer.valueOf(5), weeklyRecurrence.getNumberOfOccurrences());
		Assert.assertEquals(startDate, weeklyRecurrence.getStartDate());
		Assert.assertNull(weeklyRecurrence.getEndDate());
		Assert.assertEquals(1, ((AbstractIntervalPattern) weeklyRecurrence).getInterval());
		final Set<DayOfTheWeek> daysOfTheWeek = new HashSet<DayOfTheWeek>(
				((WeeklyPattern) weeklyRecurrence).getDaysOfTheWeek());
		Assert.assertEquals(4, daysOfTheWeek.size());
		Assert.assertTrue(daysOfTheWeek.contains(DayOfTheWeek.Monday));
		Assert.assertTrue(daysOfTheWeek.contains(DayOfTheWeek.Tuesday));
		Assert.assertTrue(daysOfTheWeek.contains(DayOfTheWeek.Saturday));
		Assert.assertTrue(daysOfTheWeek.contains(DayOfTheWeek.Sunday));

		Assert.assertNotNull(weeklyRecurrence.toString());
	}

	/**
	 * Test parse monthly recurrence.
	 */
	public void testParseMonthlyRecurrence() {
		final Date startDate = new Date();

		final Recurrence monthlyRecurrence = RecurrenceParser.parseRecurrence(startDate, null, MONTHLY_PATTERN);

		Assert.assertEquals(MonthlyPattern.class, monthlyRecurrence.getClass());
		Assert.assertEquals(Integer.valueOf(5), monthlyRecurrence.getNumberOfOccurrences());
		Assert.assertEquals(startDate, monthlyRecurrence.getStartDate());
		Assert.assertNull(monthlyRecurrence.getEndDate());
		Assert.assertEquals(1, ((AbstractIntervalPattern) monthlyRecurrence).getInterval());
		Assert.assertEquals(Integer.valueOf(8), ((MonthlyPattern) monthlyRecurrence).getDayOfMonth());
		Assert.assertNull(((MonthlyPattern) monthlyRecurrence).getDayOfTheWeek());
		Assert.assertNull(((MonthlyPattern) monthlyRecurrence).getWeekOfMonth());

		Assert.assertNotNull(monthlyRecurrence.toString());
	}

	/**
	 * Test parse monthly recurrence2.
	 */
	public void testParseMonthlyRecurrence2() {
		final Date startDate = new Date();

		final Recurrence monthlyRecurrence = RecurrenceParser.parseRecurrence(startDate, null, MONTHLY_PATTERN2);

		Assert.assertEquals(MonthlyPattern.class, monthlyRecurrence.getClass());
		Assert.assertEquals(Integer.valueOf(5), monthlyRecurrence.getNumberOfOccurrences());
		Assert.assertEquals(startDate, monthlyRecurrence.getStartDate());
		Assert.assertNull(monthlyRecurrence.getEndDate());
		Assert.assertEquals(1, ((AbstractIntervalPattern) monthlyRecurrence).getInterval());
		Assert.assertNull(((MonthlyPattern) monthlyRecurrence).getDayOfMonth());
		Assert.assertEquals(DayOfTheWeek.Saturday, ((MonthlyPattern) monthlyRecurrence).getDayOfTheWeek());
		Assert.assertEquals(Integer.valueOf(3), ((MonthlyPattern) monthlyRecurrence).getWeekOfMonth());

		Assert.assertNotNull(monthlyRecurrence.toString());
	}

	/**
	 * Test parse yearly recurrence.
	 */
	public void testParseYearlyRecurrence() {
		final Date startDate = new Date();

		final Recurrence yearlyRecurrence = RecurrenceParser.parseRecurrence(startDate, null, YEARLY_PATTERN);

		Assert.assertEquals(YearlyPattern.class, yearlyRecurrence.getClass());
		Assert.assertEquals(Integer.valueOf(3), yearlyRecurrence.getNumberOfOccurrences());
		Assert.assertEquals(startDate, yearlyRecurrence.getStartDate());
		Assert.assertNull(yearlyRecurrence.getEndDate());
		Assert.assertEquals(1, ((AbstractIntervalPattern) yearlyRecurrence).getInterval());
		Assert.assertEquals(Integer.valueOf(9), ((YearlyPattern) yearlyRecurrence).getDayOfMonth());
		Assert.assertEquals(Month.March, ((YearlyPattern) yearlyRecurrence).getMonth());
		Assert.assertNull(((YearlyPattern) yearlyRecurrence).getDayOfTheWeek());
		Assert.assertNull(((YearlyPattern) yearlyRecurrence).getWeekOfMonth());

		Assert.assertNotNull(yearlyRecurrence.toString());
	}

	/**
	 * Test parse yearly recurrence2.
	 */
	public void testParseYearlyRecurrence2() {
		final Date startDate = new Date();

		final Recurrence yearlyRecurrence = RecurrenceParser.parseRecurrence(startDate, null, YEARLY_PATTERN2);

		Assert.assertEquals(YearlyPattern.class, yearlyRecurrence.getClass());
		Assert.assertEquals(Integer.valueOf(3), yearlyRecurrence.getNumberOfOccurrences());
		Assert.assertEquals(startDate, yearlyRecurrence.getStartDate());
		Assert.assertNull(yearlyRecurrence.getEndDate());
		Assert.assertEquals(2, ((AbstractIntervalPattern) yearlyRecurrence).getInterval());
		Assert.assertNull(((YearlyPattern) yearlyRecurrence).getDayOfMonth());
		Assert.assertEquals(Month.May, ((YearlyPattern) yearlyRecurrence).getMonth());
		Assert.assertEquals(DayOfTheWeek.Thursday, ((YearlyPattern) yearlyRecurrence).getDayOfTheWeek());
		Assert.assertEquals(Integer.valueOf(5), ((YearlyPattern) yearlyRecurrence).getWeekOfMonth());

		Assert.assertNotNull(yearlyRecurrence.toString());
	}

}
