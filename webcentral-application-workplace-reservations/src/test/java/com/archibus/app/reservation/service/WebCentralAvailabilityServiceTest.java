package com.archibus.app.reservation.service;

import java.util.*;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.*;
import com.archibus.core.test.fixture.category.DatabaseTest;

/**
 * Test class for WebCentralCalendarService.
 */
@Category({ DatabaseTest.class })
public class WebCentralAvailabilityServiceTest extends ReservationServiceTestBase {

    /** First email address used for testing. */
    private static final String EMAIL1 = "afm@tgd.com";

    /** The Availability service instance being tested. */
    private WebCentralAvailabilityService webCentralAvailabilityService;

    /**
     * Set the WebCentral availability service.
     *
     * @param webCentralAvailabilityService
     *            the new WebCentral availability service
     */
    public void setWebCentralAvailabilityService(final WebCentralAvailabilityService webCentralAvailabilityService) {
        this.webCentralAvailabilityService = webCentralAvailabilityService;
    }

    /**
     * Test getting attendee availability.
     */
    public void testAttendeeAvailibility() {
        final TimeZone timeZone = TimeZone.getDefault();

        final AttendeeAvailability freeBusy = this.webCentralAvailabilityService.findAttendeeAvailability(null,
                this.startDate, timeZone, EMAIL1, Arrays.asList(new String[] { EMAIL1 })).get(EMAIL1);
        Assert.assertTrue(freeBusy.isSuccessful());
        final List<ICalendarEvent> events = freeBusy.getCalendarEvents();
        Assert.assertNotNull(events);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "com/archibus/app/reservation/reservation-no-integration.xml", "com/archibus/app/reservation/adminService.xml" };
    }

}
