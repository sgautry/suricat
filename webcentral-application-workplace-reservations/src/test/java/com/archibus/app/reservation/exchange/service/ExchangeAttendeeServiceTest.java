package com.archibus.app.reservation.exchange.service;

import java.util.List;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.*;
import com.archibus.core.test.fixture.category.*;

/**
 * Test class for ExchangeAttendeeService.
 *
 * @author Yorik Gerlo
 * @since 23.2
 */
@Category({ DatabaseTest.class, VerySlowTest.class })
public class ExchangeAttendeeServiceTest extends ExchangeCalendarServiceTestBase {

    /** The Availability service being tested. */
    private ExchangeAttendeeService attendeeService;

    /**
     * Test getting attendees' response status.
     */
    public void testGetAttendeesResponseStatus() {
        final RoomReservation reservation = createRoomReservation();
        populateReservation(reservation);
        createAppointment(reservation);
        final List<AttendeeResponseStatus> responses = this.attendeeService.getAttendeesResponseStatus(reservation);
        Assert.assertEquals(reservation.getAttendees().split(SEMICOLON).length, responses.size());
        for (final AttendeeResponseStatus response : responses) {
            Assert.assertEquals(AttendeeResponseStatus.ResponseStatus.Unknown, response.getResponseStatus());
        }
    }

    /**
     * Test getting attendees' response status for an occurrence of a recurring
     * reservation.
     */
    public void testGetAttendeeResponseStatusRecurring() {
        final RoomReservation reservation = createRoomReservation();
        populateReservation(reservation);
        addRecurrence(reservation);
        createAppointment(reservation);

        final List<AttendeeResponseStatus> responses = this.attendeeService.getAttendeesResponseStatus(reservation);
        Assert.assertEquals(reservation.getAttendees().split(SEMICOLON).length, responses.size());
        for (final AttendeeResponseStatus response : responses) {
            Assert.assertEquals(AttendeeResponseStatus.ResponseStatus.Unknown, response.getResponseStatus());
        }
    }

    /**
     * Set the Attendee Service for this test.
     *
     * @param attendeeService
     *            the attendee service
     */
    public void setAttendeeService(final ExchangeAttendeeService attendeeService) {
        this.attendeeService = attendeeService;
    }

}
