package com.archibus.app.reservation.ics.service;

import org.junit.Assert;
import org.junit.experimental.categories.Category;

import com.archibus.app.reservation.domain.RoomReservation;
import com.archibus.app.reservation.service.ReservationServiceTestBase;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.utility.ExceptionBase;

/**
 * Test class for IcsCalendarService.
 */
@Category({ DatabaseTest.class })
public class IcsCalendarServiceTest extends ReservationServiceTestBase {

    /** The Calendar service instance being tested. */
    private IcsCalendarService icsCalendarService;

    /**
     * Set the WebCentral calendar service.
     *
     * @param icsCalendarService
     *            the new WebCentral calendar service
     */
    public void setIcsCalendarService(final IcsCalendarService icsCalendarService) {
        this.icsCalendarService = icsCalendarService;
    }

    /**
     * Test sending an invitation for a new appointment.
     */
    public void testCreateAppointment() {
        final RoomReservation roomReservation = createRoomReservation();
        this.reservationService.saveReservation(roomReservation);
        final String uniqueId = this.icsCalendarService.createAppointment(roomReservation);

        Assert.assertEquals("", uniqueId);
    }

    /**
     * Test sending an update invitation.
     */
    public void testUpdateAppointment() {
        final RoomReservation roomReservation = createRoomReservation();
        this.reservationService.saveReservation(roomReservation);
        try {
            this.icsCalendarService.updateAppointment(roomReservation);
        } catch (final ExceptionBase exception) {
            Assert.fail(exception.toString());
        }
    }

    /**
     * Test sending a cancellation.
     */
    public void testCancelAppointment() {
        final RoomReservation roomReservation = createRoomReservation();
        this.reservationService.saveReservation(roomReservation);
        try {
            this.icsCalendarService.cancelAppointment(roomReservation, "reservation cancelled", true);
        } catch (final ExceptionBase exception) {
            Assert.fail(exception.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "com/archibus/app/reservation/reservation-no-integration.xml", "com/archibus/app/reservation/adminService.xml" };
    }

}
