package com.archibus.app.reservation.service.helpers;

import java.util.*;

import org.junit.Assert;

import com.archibus.app.reservation.domain.*;
import com.archibus.app.reservation.service.RoomReservationServiceTestBase;
import com.archibus.datasource.data.DataRecord;

/**
 * Test for the Conference Call Messages Service class.
 */
// @Category({ DatabaseTest.class })
public class ReservationConflictsMessagesServiceTest extends RoomReservationServiceTestBase {

	/** URL prefix expected in the conflicts description. */
	private static final String URL_PREFIX = "http://";

	/** A new line character. */
	private static final String NEWLINE = "\n";

	/** The reservation messages service. */
	private ConferenceCallMessagesService messagesService;

	/**
	 * Test getting the conflicts description for single-room recurring reservations.
	 */
	public void testCreateConflictsDescription() {
		for (int i = 1; i < 4; ++i) {
			final ReservationMessage description = doCreateConflictsDescription(i, false);
			Assert.assertNotNull(description);
		}
	}

	/**
	 * Test getting the conflicts description for recurring conference call reservations.
	 */
	public void testCreateConfCallConflictsDescription() {
		for (int i = 1; i < 4; ++i) {
			final ReservationMessage description = doCreateConflictsDescription(i, true);
			Assert.assertNotNull(description);
		}
	}

	/**
	 * Actual test code for generating the conflicts description.
	 * @param conflictCount number of conflicts to simulate
	 * @param confCall whether to generate for a conference call
	 * @return the conflicts description
	 */
	private ReservationMessage doCreateConflictsDescription(final int conflictCount, final boolean confCall) {
		final SortedSet<Date> conflictDates = generateDates(conflictCount);
		final DataRecord record = this.reservationDataSource.createNewRecord();
		final RoomReservation reservation = this.reservationDataSource
				.convertRecordToObject(createReservation(record, true));
		if (confCall) {
			reservation.setConferenceId(1);
		}
		reservation.setCreatedReservations(new ArrayList<RoomReservation>());

		final ReservationMessage description = this.messagesService.createConflictsDescription(reservation,
				conflictDates);
		Assert.assertNotNull(description.getSubject());
		Assert.assertTrue(description.getBody().contains(URL_PREFIX));
		Assert.assertTrue(description.getBody().contains(Integer.toString(conflictCount)));
		Assert.assertTrue(description.getBody().contains("Resolve Conflicts"));
		if (conflictCount > 1) {
			Assert.assertTrue(description.getBody().contains("and"));
		}
		if (conflictCount > 2) {
			Assert.assertTrue(description.getBody().contains(", "));
		}
		if (confCall || conflictCount > 1) {
			if (confCall) {
				Assert.assertTrue(description.getBody().contains("which rooms"));
			}
			Assert.assertTrue(description.getBody().contains("these conflicts"));
		} else {
			Assert.assertTrue(description.getBody().contains("this conflict"));
		}
		return description;
	}


	/**
	 * Test getting the location string for a conference call reservation and
	 * also updating an existing one.
	 */
	public void testInsertConflictsDescription() {
		final SortedSet<Date> conflictDates = generateDates(1);
		final DataRecord record = this.reservationDataSource.createNewRecord();
		final RoomReservation reservation = this.reservationDataSource
				.convertRecordToObject(createReservation(record, true));
		reservation.setCreatedReservations(new ArrayList<RoomReservation>());

		this.messagesService.insertConflictsDescription(reservation, conflictDates);
		Assert.assertNotNull(reservation.getHtmlComments());
		Assert.assertTrue(reservation.getHtmlComments().contains(URL_PREFIX));
		Assert.assertFalse(reservation.getComments().contains(URL_PREFIX));

		final ReservationMessage description = this.messagesService.createConflictsDescription(reservation,
				conflictDates);
		Assert.assertTrue(reservation.getComments().startsWith(description.getSubject() + NEWLINE));
		Assert.assertTrue(reservation.getComments().indexOf(NEWLINE + description.getSubject() + NEWLINE) > 0);
		Assert.assertTrue(reservation.getHtmlComments().startsWith(description.getSubject() + NEWLINE));
		Assert.assertTrue(reservation.getHtmlComments().indexOf(NEWLINE + description.getSubject() + NEWLINE) > 0);
	}

	/**
	 * Test stripping the conflicts description from a reservation comment.
	 */
	public void testStripConflictDescription() {
		final SortedSet<Date> conflictDates = generateDates(1);
		final DataRecord record = this.reservationDataSource.createNewRecord();
		final RoomReservation reservation = this.reservationDataSource
				.convertRecordToObject(createReservation(record, true));
		reservation.setCreatedReservations(new ArrayList<RoomReservation>());
		final String originalComments = reservation.getComments();

		this.messagesService.insertConflictsDescription(reservation, conflictDates);

		final String strippedComments = this.messagesService.stripConflictsDescription(reservation.getEmail(),
				reservation.getComments());
		Assert.assertEquals(originalComments, strippedComments);
	}

	/**
	 * Sets the messages service.
	 *
	 * @param messagesService
	 *            the new messages service
	 */
	public void setMessagesService(final ConferenceCallMessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * Generate a sorted set of dates.
	 * @param count number of dates to generate
	 * @return the generated dates
	 */
	private SortedSet<Date> generateDates(final int count) {
		final SortedSet<Date> dates = new TreeSet<Date>();
		final Calendar calendar = Calendar.getInstance();
		for (int i = 0; i < count; ++i) {
			calendar.add(Calendar.DATE, 1);
			dates.add(calendar.getTime());
		}
		return dates;
	}

}
