webcentral-gradle-common is a generic Eclipse project. It does not have any builders. 
It is just a collection of files that don't belong to any particular component:
- Gradle files common for all components;
