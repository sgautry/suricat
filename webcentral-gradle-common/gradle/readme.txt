This folder contains Gradle files shared between components.

To run Gradle from command line:
SET GRADLE_HOME=C:\eclipse-archibus-23.2\gradle
SET GRADLE_USER_HOME=C:\eclipse-archibus-23.2\gradle\bin
set PATH=%GRADLE_HOME%\bin;%PATH%

Bamboo: run gradle with sonarqube task