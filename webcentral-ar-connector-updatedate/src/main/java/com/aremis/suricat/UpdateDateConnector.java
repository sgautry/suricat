package com.aremis.suricat;

import java.io.*;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.archibus.context.ContextStore;
import com.archibus.db.DbConnection;
import com.aremis.eventhandler.AremisContractService;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONArray;

public class UpdateDateConnector{
    static Logger log = Logger.getLogger(UpdateDateConnector.class);
    
    public static JSONObject retrieveDate(){
        JSONObject obj = new JSONObject();
        JSONObject params = new JSONObject();
        String path= null ;
        try{
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute("SELECT * from AR_DATE_CONNECTOR" , 0);
            while(internalSet.next()){
                obj.put("chene",internalSet.getString(2));
                obj.put("tiers",internalSet.getString(3));
                obj.put("patrimoine",internalSet.getString(4));
                obj.put("contrat",internalSet.getString(5));
                obj.put("bail",internalSet.getString(6));
            }
        }catch(Exception ex){
            logDebug(ex.getMessage(),ex);
        }
        return obj;
    }
    protected static void logDebug(String message){
        logDebug(message,null);
    }
    protected static void logDebug(String message,Throwable t){
        
        if(log.isDebugEnabled()){
            log.debug(message,t);
        }
    }
}