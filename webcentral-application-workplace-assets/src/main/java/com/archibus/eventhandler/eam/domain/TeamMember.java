package com.archibus.eventhandler.eam.domain;

import java.util.Date;

import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.manage.AssetType;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for team member. Mapped to team table
 * <p>
 *
 *
 * @author draghici
 * @since 23.2
 *
 *        <p>
 *        SuppressWarnings Justification: this class represent a database
 *        record. Using nested classes will make the code difficult to
 *        understand.
 */

@SuppressWarnings({ "PMD.ExcessivePublicCount", "PMD.TooManyFields" })
public class TeamMember {
	/**
	 * Field address_archive.
	 */
	private String addressArchive;

	/**
	 * Field : autonumbered_id.
	 */
	private int autonumberedId;

	/**
	 * Field : bl_id.
	 */
	private String buildingId;

	/**
	 * Field : cell_num_archive.
	 */
	private String cellNumArchive;

	/**
	 * Field : company_archive.
	 */
	private String companyArchive;

	/**
	 * Field : contact_id.
	 */
	private String contactId;

	/**
	 * Field : contact_type_archive.
	 */
	private String contactTypeArchive;

	/**
	 * Field : custodian_type.
	 */
	private String custodianType;

	/**
	 * Field : date_end.
	 */
	private Date dateEnd;

	/**
	 * Field : date_start.
	 */
	private Date dateStart;

	/**
	 * Field : date_verified.
	 */
	private Date dateVerified;

	/**
	 * Field : digital_signature.
	 */
	private String digitalSignature;

	/**
	 * Field : em_id.
	 */
	private String employeeId;

	/**
	 * Field : email_archive.
	 */
	private String emailArchive;

	/**
	 * Field : eq_id.
	 */
	private String equipmentId;

	/**
	 * Field : fax_archive.
	 */
	private String faxArchive;

	/**
	 * Field : is_owner.
	 */
	private String isOwner;

	/**
	 * Field : member_type.
	 */
	private String memberType;

	/**
	 * Field : name_archive.
	 */
	private String nameArchive;

	/**
	 * Field : notes.
	 */
	private String notes;

	/**
	 * Field : option1.
	 */
	private String option1;

	/**
	 * Field : option2.
	 */
	private String option2;

	/**
	 * Field : pct_time.
	 */
	private double pctTime;

	/**
	 * Field : phone_archive.
	 */
	private String phoneArchive;

	/**
	 * Field : pr_id.
	 */
	private String propertyId;

	/**
	 * Field : project_id.
	 */
	private String projectId;

	/**
	 * Field : site_id.
	 */
	private String siteId;

	/**
	 * Field : source_table.
	 */
	private String sourceTable;

	/**
	 * Field : status.
	 */
	private String status;

	/**
	 * Field : ta_id.
	 */
	private String taggedFurnitureId;

	/**
	 * Field : team_id.
	 */
	private String teamId;

	/**
	 * Field : team_type.
	 */
	private String teamType;

	/**
	 * Field : vn_id.
	 */
	private String vendorId;

	/**
	 * Getter for the addressArchive property.
	 *
	 * @return the addressArchive property.
	 */
	public String getAddressArchive() {
		return this.addressArchive;
	}

	/**
	 * Setter for the addressArchive property.
	 *
	 * @param addressArchive
	 *            the addressArchive to set.
	 */
	public void setAddressArchive(final String addressArchive) {
		this.addressArchive = addressArchive;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the cellNumArchive property.
	 *
	 * @return the cellNumArchive property.
	 */
	public String getCellNumArchive() {
		return this.cellNumArchive;
	}

	/**
	 * Setter for the cellNumArchive property.
	 *
	 * @param cellNumArchive
	 *            the cellNumArchive to set.
	 */
	public void setCellNumArchive(final String cellNumArchive) {
		this.cellNumArchive = cellNumArchive;
	}

	/**
	 * Getter for the companyArchive property.
	 *
	 * @return the companyArchive property.
	 */
	public String getCompanyArchive() {
		return this.companyArchive;
	}

	/**
	 * Setter for the companyArchive property.
	 *
	 * @param companyArchive
	 *            the companyArchive to set.
	 */
	public void setCompanyArchive(final String companyArchive) {
		this.companyArchive = companyArchive;
	}

	/**
	 * Getter for the contactId property.
	 *
	 * @return the contactId property.
	 */
	public String getContactId() {
		return this.contactId;
	}

	/**
	 * Setter for the contactId property.
	 *
	 * @param contactId
	 *            the contactId to set.
	 */
	public void setContactId(final String contactId) {
		this.contactId = contactId;
	}

	/**
	 * Getter for the contactTypeArchive property.
	 *
	 * @return the contactTypeArchive property.
	 */
	public String getContactTypeArchive() {
		return this.contactTypeArchive;
	}

	/**
	 * Setter for the contactTypeArchive property.
	 *
	 * @param contactTypeArchive
	 *            the contactTypeArchive to set.
	 */
	public void setContactTypeArchive(final String contactTypeArchive) {
		this.contactTypeArchive = contactTypeArchive;
	}

	/**
	 * Getter for the custodianType property.
	 *
	 * @return the custodianType property.
	 */
	public String getCustodianType() {
		return this.custodianType;
	}

	/**
	 * Setter for the custodianType property.
	 *
	 * @param custodianType
	 *            the custodianType to set.
	 */
	public void setCustodianType(final String custodianType) {
		this.custodianType = custodianType;
	}

	/**
	 * Getter for the dateEnd property.
	 *
	 * @return the dateEnd property.
	 */
	public Date getDateEnd() {
		return this.dateEnd;
	}

	/**
	 * Setter for the dateEnd property.
	 *
	 * @param dateEnd
	 *            the dateEnd to set.
	 */
	public void setDateEnd(final Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for the dateStart property.
	 *
	 * @return the dateStart property.
	 */
	public Date getDateStart() {
		return this.dateStart;
	}

	/**
	 * Setter for the dateStart property.
	 *
	 * @param dateStart
	 *            the dateStart to set.
	 */
	public void setDateStart(final Date dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for the dateVerified property.
	 *
	 * @return the dateVerified property.
	 */
	public Date getDateVerified() {
		return this.dateVerified;
	}

	/**
	 * Setter for the dateVerified property.
	 *
	 * @param dateVerified
	 *            the dateVerified to set.
	 */
	public void setDateVerified(final Date dateVerified) {
		this.dateVerified = dateVerified;
	}

	/**
	 * Getter for the digitalSignature property.
	 *
	 * @return the digitalSignature property.
	 */
	public String getDigitalSignature() {
		return this.digitalSignature;
	}

	/**
	 * Setter for the digitalSignature property.
	 *
	 * @param digitalSignature
	 *            the digitalSignature to set.
	 */
	public void setDigitalSignature(final String digitalSignature) {
		this.digitalSignature = digitalSignature;
	}

	/**
	 * Getter for the employeeId property.
	 *
	 * @return the employeeId property.
	 */
	public String getEmployeeId() {
		return this.employeeId;
	}

	/**
	 * Setter for the employeeId property.
	 *
	 * @param employeeId
	 *            the employeeId to set.
	 */
	public void setEmployeeId(final String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Getter for the emailArchive property.
	 *
	 * @return the emailArchive property.
	 */
	public String getEmailArchive() {
		return this.emailArchive;
	}

	/**
	 * Setter for the emailArchive property.
	 *
	 * @param emailArchive
	 *            the emailArchive to set.
	 */
	public void setEmailArchive(final String emailArchive) {
		this.emailArchive = emailArchive;
	}

	/**
	 * Getter for the equipmentId property.
	 *
	 * @return the equipmentId property.
	 */
	public String getEquipmentId() {
		return this.equipmentId;
	}

	/**
	 * Setter for the equipmentId property.
	 *
	 * @param equipmentId
	 *            the equipmentId to set.
	 */
	public void setEquipmentId(final String equipmentId) {
		this.equipmentId = equipmentId;
	}

	/**
	 * Getter for the faxArchive property.
	 *
	 * @return the faxArchive property.
	 */
	public String getFaxArchive() {
		return this.faxArchive;
	}

	/**
	 * Setter for the faxArchive property.
	 *
	 * @param faxArchive
	 *            the faxArchive to set.
	 */
	public void setFaxArchive(final String faxArchive) {
		this.faxArchive = faxArchive;
	}

	/**
	 * Getter for the isOwner property.
	 *
	 * @return the isOwner property.
	 */
	public String getIsOwner() {
		return this.isOwner;
	}

	/**
	 * Setter for the isOwner property.
	 *
	 * @param isOwner
	 *            the isOwner to set.
	 */
	public void setIsOwner(final String isOwner) {
		this.isOwner = isOwner;
	}

	/**
	 * Getter for the memberType property.
	 *
	 * @return the memberType property.
	 */
	public String getMemberType() {
		return this.memberType;
	}

	/**
	 * Setter for the memberType property.
	 *
	 * @param memberType
	 *            the memberType to set.
	 */
	public void setMemberType(final String memberType) {
		this.memberType = memberType;
	}

	/**
	 * Getter for the nameArchive property.
	 *
	 * @return the nameArchive property.
	 */
	public String getNameArchive() {
		return this.nameArchive;
	}

	/**
	 * Setter for the nameArchive property.
	 *
	 * @param nameArchive
	 *            the nameArchive to set.
	 */
	public void setNameArchive(final String nameArchive) {
		this.nameArchive = nameArchive;
	}

	/**
	 * Getter for the notes property.
	 *
	 * @return the notes property.
	 */
	public String getNotes() {
		return this.notes;
	}

	/**
	 * Setter for the notes property.
	 *
	 * @param notes
	 *            the notes to set.
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/**
	 * Getter for the option1 property.
	 *
	 * @return the option1 property.
	 */
	public String getOption1() {
		return this.option1;
	}

	/**
	 * Setter for the option1 property.
	 *
	 * @param option1
	 *            the option1 to set.
	 */
	public void setOption1(final String option1) {
		this.option1 = option1;
	}

	/**
	 * Getter for the option2 property.
	 *
	 * @return the option2 property.
	 */
	public String getOption2() {
		return this.option2;
	}

	/**
	 * Setter for the option2 property.
	 *
	 * @param option2
	 *            the option2 to set.
	 */
	public void setOption2(final String option2) {
		this.option2 = option2;
	}

	/**
	 * Getter for the pctTime property.
	 *
	 * @return the pctTime property.
	 */
	public double getPctTime() {
		return this.pctTime;
	}

	/**
	 * Setter for the pctTime property.
	 *
	 * @param pctTime
	 *            the pctTime to set.
	 */
	public void setPctTime(final double pctTime) {
		this.pctTime = pctTime;
	}

	/**
	 * Getter for the phoneArchive property.
	 *
	 * @return the phoneArchive property.
	 */
	public String getPhoneArchive() {
		return this.phoneArchive;
	}

	/**
	 * Setter for the phoneArchive property.
	 *
	 * @param phoneArchive
	 *            the phoneArchive to set.
	 */
	public void setPhoneArchive(final String phoneArchive) {
		this.phoneArchive = phoneArchive;
	}

	/**
	 * Getter for the propertyId property.
	 *
	 * @return the propertyId property.
	 */
	public String getPropertyId() {
		return this.propertyId;
	}

	/**
	 * Setter for the propertyId property.
	 *
	 * @param propertyId
	 *            the propertyId to set.
	 */
	public void setPropertyId(final String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the siteId property.
	 *
	 * @return the siteId property.
	 */
	public String getSiteId() {
		return this.siteId;
	}

	/**
	 * Setter for the siteId property.
	 *
	 * @param siteId
	 *            the siteId to set.
	 */
	public void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the sourceTable property.
	 *
	 * @return the sourceTable property.
	 */
	public String getSourceTable() {
		return this.sourceTable;
	}

	/**
	 * Setter for the sourceTable property.
	 *
	 * @param sourceTable
	 *            the sourceTable to set.
	 */
	public void setSourceTable(final String sourceTable) {
		this.sourceTable = sourceTable;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the taggedFurnitureId property.
	 *
	 * @return the taggedFurnitureId property.
	 */
	public String getTaggedFurnitureId() {
		return this.taggedFurnitureId;
	}

	/**
	 * Setter for the taggedFurnitureId property.
	 *
	 * @param taggedFurnitureId
	 *            the taggedFurnitureId to set.
	 */
	public void setTaggedFurnitureId(final String taggedFurnitureId) {
		this.taggedFurnitureId = taggedFurnitureId;
	}

	/**
	 * Getter for the teamId property.
	 *
	 * @return the teamId property.
	 */
	public String getTeamId() {
		return this.teamId;
	}

	/**
	 * Setter for the teamId property.
	 *
	 * @param teamId
	 *            the teamId to set.
	 */
	public void setTeamId(final String teamId) {
		this.teamId = teamId;
	}

	/**
	 * Getter for the teamType property.
	 *
	 * @return the teamType property.
	 */
	public String getTeamType() {
		return this.teamType;
	}

	/**
	 * Setter for the teamType property.
	 *
	 * @param teamType
	 *            the teamType to set.
	 */
	public void setTeamType(final String teamType) {
		this.teamType = teamType;
	}

	/**
	 * Getter for the vendorId property.
	 *
	 * @return the vendorId property.
	 */
	public String getVendorId() {
		return this.vendorId;
	}

	/**
	 * Setter for the vendorId property.
	 *
	 * @param vendorId
	 *            the vendorId to set.
	 */
	public void setVendorId(final String vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * Getter for the autonumberedId property.
	 *
	 * @return the autonumberedId property.
	 */
	public int getAutonumberedId() {
		return this.autonumberedId;
	}

	/**
	 * Setter for the autonumberedId property.
	 *
	 * @param autonumberedId
	 *            the autonumberedId to set.
	 */
	public void setAutonumberedId(final int autonumberedId) {
		this.autonumberedId = autonumberedId;
	}

	/**
	 * Returns true if is owner.
	 *
	 * @return boolean
	 */
	public boolean isAssetOwner() {
		return "1".equals(this.isOwner) && DbConstants.STATUS_ACTIVE.equals(this.status);
	}

	/**
	 * Returns team member name based on source table.
	 *
	 * @return String
	 */
	public String getTeamMemberName() {
		String memberName = null;
		if (TeamMemberSource.CONTACT.toString().equals(this.sourceTable)) {
			memberName = this.contactId;
		} else if (TeamMemberSource.EMPLOYEE.toString().equals(this.sourceTable)) {
			memberName = this.employeeId;
		} else if (TeamMemberSource.VENDOR.toString().equals(this.sourceTable)) {
			memberName = this.vendorId;
		}
		return memberName;
	}

	/**
	 * Return asset id for current team member.
	 *
	 * @return String
	 */
	public String getAssetId() {
		String assetId = null;
		if (StringUtil.notNullOrEmpty(this.buildingId)) {
			assetId = this.buildingId;
		} else if (StringUtil.notNullOrEmpty(this.propertyId)) {
			assetId = this.propertyId;
		} else if (StringUtil.notNullOrEmpty(this.equipmentId)) {
			assetId = this.equipmentId;
		} else if (StringUtil.notNullOrEmpty(this.taggedFurnitureId)) {
			assetId = this.taggedFurnitureId;
		}
		return assetId;
	}

	/**
	 * Return asset type for current team member.
	 *
	 * @return {@link AssetType}
	 */
	public AssetType getAssetType() {
		AssetType assetType = null;
		if (StringUtil.notNullOrEmpty(this.buildingId)) {
			assetType = AssetType.BUILDING;
		} else if (StringUtil.notNullOrEmpty(this.propertyId)) {
			assetType = AssetType.PROPERTY;
		} else if (StringUtil.notNullOrEmpty(this.equipmentId)) {
			assetType = AssetType.EQUIPMENT;
		} else if (StringUtil.notNullOrEmpty(this.taggedFurnitureId)) {
			assetType = AssetType.FURNITURE;
		}
		return assetType;
	}
}
