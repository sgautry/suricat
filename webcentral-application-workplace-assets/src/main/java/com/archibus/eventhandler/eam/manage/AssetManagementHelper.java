package com.archibus.eventhandler.eam.manage;

import java.util.*;

import org.json.*;

import com.archibus.app.common.util.SchemaUtils;
import com.archibus.context.ContextStore;
import com.archibus.core.event.data.ChangeType;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.*;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.service.DocumentService;
import com.archibus.service.DocumentService.DocumentParameters;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Helper class for Asset management.
 * <p>
 *
 *
 * @author draghici
 * @author Radu Bunea
 * @since 23.2
 *
 */
final class AssetManagementHelper {
    /**
     * Document service bean. Used to add /delete documents.
     */
    private static final String DOCUMENT_SERVICE_BEAN = "documentService";

    /**
     * Private default constructor: utility class is non-instantiable.
     */
    private AssetManagementHelper() {

    }

    /**
     * Returns list with asset registry records.
     *
     * @param dataSourceParameters object with data source parameters
     * @param restriction additional restriction
     * @return List<DataRecord>
     */
    static List<DataRecord> getAssetRecords(final Map<String, String> dataSourceParameters,
            final Restriction restriction) {
        // load asset registry data source from file from file
        final DataSource assetRegistryDataSource = DataSourceFactory
            .loadDataSourceFromFile("ab-eam-asset-registry-ds.axvw", "abEamAssetRegistry_ds");

        assetRegistryDataSource.setParameter(Constants.BL_TYPE_RESTRICTION_KEY,
            dataSourceParameters.get(Constants.BL_TYPE_RESTRICTION_KEY));
        assetRegistryDataSource.setParameter(Constants.PROPERTY_TYPE_RESTRICTION_KEY,
            dataSourceParameters.get(Constants.PROPERTY_TYPE_RESTRICTION_KEY));
        assetRegistryDataSource.setParameter(Constants.EQ_TYPE_RESTRICTION_KEY,
            dataSourceParameters.get(Constants.EQ_TYPE_RESTRICTION_KEY));
        assetRegistryDataSource.setParameter(Constants.TA_TYPE_RESTRICTION_KEY,
            dataSourceParameters.get(Constants.TA_TYPE_RESTRICTION_KEY));
        assetRegistryDataSource.setParameter(Constants.SQL_TYPE_RESTRICTION_KEY,
            dataSourceParameters.get(Constants.SQL_TYPE_RESTRICTION_KEY));
        if (StringUtil.notNullOrEmpty(restriction)) {
            assetRegistryDataSource.addRestriction(restriction);
        }
        // Check backward compatible to 23.1 schema.
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        eventHandlerContext.addInputParameter("useCustodian",
            SchemaUtils.tableExistsInSchema(DbConstants.CUSTODIANTYPE_TABLE));
        final boolean useCostDepreciation =
                SchemaUtils.fieldExistsInSchema(DbConstants.EQUIPMENT_TABLE,
                    DbConstants.DATE_DEP_CLOSING_MONTH)
                        && SchemaUtils.fieldExistsInSchema(DbConstants.FURNITURE_TABLE,
                            DbConstants.COST_DEP_VALUE)
                        && SchemaUtils.fieldExistsInSchema(DbConstants.FURNITURE_TABLE,
                            DbConstants.DATE_DEP_CLOSING_MONTH);
        eventHandlerContext.addInputParameter("useCostDepreciation", useCostDepreciation);
        return assetRegistryDataSource.getRecords();
    }

    /**
     * Returns data source parameters.
     *
     * @param selectedAssets JSON array with selected assets
     * @param useCustomRestriction boolean if custom restriction is used
     * @param customRestriction object with custom restriction
     * @return Map<String, String>
     */
    static Map<String, String> getDataSourceParameters(final JSONArray selectedAssets,
            final boolean useCustomRestriction, final Map<String, String> customRestriction) {
        if (!useCustomRestriction) {
            // prepare sql restriction from selected assets
            String sqlRestriction = "";
            for (int counter = 0; counter < selectedAssets.length(); counter++) {
                final JSONObject asset = selectedAssets.getJSONObject(counter);
                final String assetId = asset.getString(Constants.ASSET_ID_KEY);
                final String assetType = asset.getString(Constants.ASSET_TYPE_KEY);
                final String assetRestriction = String.format(
                    "(bl.asset_id = %s AND bl.asset_type = %s)",
                    SqlUtils.formatValueForSql(assetId), SqlUtils.formatValueForSql(assetType));
                sqlRestriction += (sqlRestriction.length() > 0 ? Constants.OR_OPERATOR : "")
                        + assetRestriction;
            }

            customRestriction.put(Constants.SQL_TYPE_RESTRICTION_KEY, sqlRestriction);
            customRestriction.put(Constants.BL_TYPE_RESTRICTION_KEY,
                Constants.DATA_SOURCE_PARAMETER_DEFAULT);
            customRestriction.put(Constants.PROPERTY_TYPE_RESTRICTION_KEY,
                Constants.DATA_SOURCE_PARAMETER_DEFAULT);
            customRestriction.put(Constants.EQ_TYPE_RESTRICTION_KEY,
                Constants.DATA_SOURCE_PARAMETER_DEFAULT);
            customRestriction.put(Constants.TA_TYPE_RESTRICTION_KEY,
                Constants.DATA_SOURCE_PARAMETER_DEFAULT);
        }
        return customRestriction;
    }

    /**
     * Terminate custodian - return asset transaction.
     *
     * @param assetTransaction new asset transaction object
     * @param custodian custodian object
     * @param oldStatus previous custodian status
     * @return {@link AssetTransaction}
     */
    static AssetTransaction getTerminateCustodianTransaction(
            final AssetTransaction assetTransaction, final TeamMember custodian,
            final String oldStatus) {
        assetTransaction.setTransactionType(TransactionType.OWNER_CUSTODIAN);
        assetTransaction.setDateEndCustodian(custodian.getDateEnd());
        assetTransaction.setTableName(DbConstants.TEAM_TABLE);
        assetTransaction.setAssetId(String.valueOf(custodian.getAssetId()));
        assetTransaction.setFieldName(DbConstants.STATUS);
        assetTransaction.setOldValue(oldStatus);
        assetTransaction.setNewValue(custodian.getStatus());
        assetTransaction.setUserName(ContextStore.get().getUser().getName());
        assetTransaction.setActionType(ChangeType.UPDATE);
        return assetTransaction;
    }

    /**
     * Add owner custodian- return asset transaction.
     *
     * @param assetTransaction new asset transaction object
     * @param custodian custodian object
     * @param oldOwner previous owner name
     * @return {@link AssetTransaction}
     */
    static AssetTransaction getAddOwnerCustodianTransaction(final AssetTransaction assetTransaction,
            final TeamMember custodian, final String oldOwner) {
        assetTransaction.setTransactionType(TransactionType.OWNER_CUSTODIAN);
        assetTransaction.setDateStartCustodian(custodian.getDateStart());
        assetTransaction.setTableName(DbConstants.TEAM_TABLE);
        assetTransaction.setAssetId(custodian.getAssetId());
        assetTransaction.setFieldName(DbConstants.NAME_ARCHIVE);
        assetTransaction.setNewValue(custodian.getTeamMemberName());
        if (StringUtil.notNullOrEmpty(oldOwner)) {
            assetTransaction.setOldValue(oldOwner);
            assetTransaction.setActionType(ChangeType.UPDATE);
        } else {
            assetTransaction.setActionType(ChangeType.INSERT);
        }
        assetTransaction.setUserName(ContextStore.get().getUser().getName());
        return assetTransaction;
    }

    /**
     *
     * Copy digital signature document for all team records for selected assets.
     *
     * @param sourceDocumentId team.autonumbered_id
     * @param targetDocumentIds List of team.autonumbered_id
     * @param fieldName digital_signature field
     * @param parameters document parameters
     */
    static void copyDigitalSignatureDocument(final String sourceDocumentId,
            final List<Integer> targetDocumentIds, final String fieldName,
            final JSONObject parameters) {
        final String docTable = DbConstants.TEAM_TABLE;
        final String docField = fieldName.split(DbConstants.TEAM_TABLE + DbConstants.DOT)[1];
        // document prop
        final String fileName = parameters.getString("fileName");
        // source doc parameters
        final Map<String, String> sourceKeys = new HashMap<String, String>();
        sourceKeys.put(DbConstants.AUTONUMBERED_ID, sourceDocumentId);
        final DocumentParameters sourceDocParam =
                new DocumentParameters(sourceKeys, docTable, docField, null, true);
        // get document service object
        final DocumentService documentService =
                (DocumentService) ContextStore.get().getBean(DOCUMENT_SERVICE_BEAN);
        for (final Integer targetDocumentId : targetDocumentIds) {
            // target document parameters
            final Map<String, String> targetKeys = new HashMap<String, String>();
            targetKeys.put(DbConstants.AUTONUMBERED_ID, String.valueOf(targetDocumentId));
            final DocumentParameters targetDocParam = new DocumentParameters(targetKeys, docTable,
                docField, fileName, parameters.getString("description"), "0");
            // copy document
            documentService.copyDocument(sourceDocParam, targetDocParam);
        }
    }

    /**
     *
     * Delete digital signature document for all team records for selected assets.
     *
     * @param sourceDocumentId team.autonumbered_id
     * @param targetDocumentIds List of team.autonumbered_id
     * @param fieldName digital_signature field
     * @param parameters document parameters
     */
    static void deleteDigitalSignatureDocument(final String sourceDocumentId,
            final List<Integer> targetDocumentIds, final String fieldName,
            final JSONObject parameters) {
        final String docTable = DbConstants.TEAM_TABLE;
        final String docField = fieldName.split(DbConstants.TEAM_TABLE + DbConstants.DOT)[1];
        // get document service object
        final DocumentService documentService =
                (DocumentService) ContextStore.get().getBean(DOCUMENT_SERVICE_BEAN);
        for (final Integer targetDocumentId : targetDocumentIds) {
            // target document parameters
            final Map<String, String> targetKeys = new HashMap<String, String>();
            targetKeys.put(DbConstants.AUTONUMBERED_ID, String.valueOf(targetDocumentId));
            // delete document
            documentService.markDeleted(targetKeys, docTable, docField);
        }
    }

    /**
     * Returns asset id from team data record.
     *
     * @param record team data record
     * @return String
     */
    static String getAssetId(final DataRecord record) {
        String assetId = null;
        final String buildingId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.BL_ID));
        final String propertyId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.PR_ID));
        final String equipmentId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.EQ_ID));
        final String taggedFurnitureId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.TA_ID));

        if (StringUtil.notNullOrEmpty(buildingId)) {
            assetId = buildingId;
        } else if (StringUtil.notNullOrEmpty(propertyId)) {
            assetId = propertyId;
        } else if (StringUtil.notNullOrEmpty(equipmentId)) {
            assetId = equipmentId;
        } else if (StringUtil.notNullOrEmpty(taggedFurnitureId)) {
            assetId = taggedFurnitureId;
        }
        return assetId;
    }

    /**
     * Returns asset id from team data record.
     *
     * @param record team data record
     * @return {@link AssetType}
     */
    static AssetType getAssetType(final DataRecord record) {
        AssetType assetType = null;
        final String buildingId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.BL_ID));
        final String propertyId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.PR_ID));
        final String equipmentId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.EQ_ID));
        final String taggedFurnitureId = record
            .getString(DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.TA_ID));

        if (StringUtil.notNullOrEmpty(buildingId)) {
            assetType = AssetType.BUILDING;
        } else if (StringUtil.notNullOrEmpty(propertyId)) {
            assetType = AssetType.PROPERTY;
        } else if (StringUtil.notNullOrEmpty(equipmentId)) {
            assetType = AssetType.EQUIPMENT;
        } else if (StringUtil.notNullOrEmpty(taggedFurnitureId)) {
            assetType = AssetType.FURNITURE;
        }
        return assetType;
    }

    /**
     *
     * Check if asset is disposed.
     *
     * @param record asset registry data record
     * @return true if asset is disposed
     */
    static boolean isAssetDisposed(final DataRecord record) {
        final String assetTypeFieldName =
                DbConstants.getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_TYPE);
        final AssetType assetType = AssetType.fromString(record.getString(assetTypeFieldName));
        final String statusFieldName =
                DbConstants.getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_STATUS);
        final String status = record.getString(statusFieldName);
        final String[][] disposalSatusValues = AssetDisposal.DISPOSAL_TYPE_STATUS.get(assetType);
        boolean assetIsDisposed = false;
        for (final String[] disposalSatus : disposalSatusValues) {
            if (disposalSatus[1].equals(status)) {
                assetIsDisposed = true;
                break;
            }
        }
        return assetIsDisposed;

    }

    /**
     * Convert records to JSON Array.
     *
     * @param records data records list
     * @param fields array with field names
     * @return {@link JSONArray}
     */
    static JSONArray toJSONArray(final List<DataRecord> records, final String[] fields) {
        final JSONArray result = new JSONArray();
        for (final DataRecord record : records) {
            final JSONObject asset = new JSONObject();
            for (final String field : fields) {
                asset.put(field, record.getValue(field).toString());
            }
            result.put(asset);
        }
        return result;
    }

    /**
     * Date add function.
     *
     * @param date date value
     * @param datePart date part (Calendar date part)
     * @param amount amout to add
     * @return date
     */
    static Date dateAdd(final Date date, final int datePart, final int amount) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(datePart, amount);
        return calendar.getTime();
    }



    /**
     * Returns localized string.
     *
     * @param message message name
     * @return string
     */
    static String getLocalizedMessage(final String message, final String className) {
        final String sourceClassName = StringUtil.notNullOrEmpty(className) ? className
                : "com.archibus.eventhandler.eam.manage.AssetManagementHelper";
        return EventHandlerBase.localizeString(ContextStore.get().getEventHandlerContext(), message,
            sourceClassName);
    }
}
