package com.archibus.eventhandler.eam.dao.datasource;

import org.apache.commons.lang.ArrayUtils;

import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.AssetCommon;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for building table.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class BuildingDataSource extends AbstractAssetDataSource<AssetCommon> {
	/**
	 * Field names to building names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { DbConstants.BL_ID, "buildingId" } };

	/**
	 * Constructs BuildingDataSource, mapped to <code>bl</code> table, using
	 * <code>building</code> bean.
	 *
	 */
	public BuildingDataSource() {
		super("assetBuilding", DbConstants.BUILDING_TABLE);
	}

	/**
	 * Merge fieldsToProperties from the base class.
	 *
	 * @return fieldsToPropertiesMerged
	 */
	@Override
	protected String[][] getFieldsToProperties() {
		final String[][] fieldsToPropertiesMerged = (String[][]) ArrayUtils.addAll(super.getFieldsToProperties(),
				FIELDS_TO_PROPERTIES);
		return fieldsToPropertiesMerged;
	}
}