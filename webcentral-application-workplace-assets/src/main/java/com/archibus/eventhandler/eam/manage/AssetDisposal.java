package com.archibus.eventhandler.eam.manage;

import java.text.SimpleDateFormat;
import java.util.*;

import com.archibus.context.ContextStore;
import com.archibus.eventhandler.eam.dao.IOwnershipTransactionDao;
import com.archibus.eventhandler.eam.dao.datasource.OwnershipTransactionDataSource;
import com.archibus.eventhandler.eam.domain.*;
import com.archibus.utility.Utility;
import com.ibm.icu.text.MessageFormat;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * <p>
 * Provides methods for asset management disposal.
 * <p>
 * Dispose an asset and apply ownership.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class AssetDisposal {
    /**
     * Property status donated.
     */
    private static final String STATUS_PROPERTY_DONATED = "DONATED";

    /**
     * Property status disposed.
     */
    private static final String STATUS_PROPERTY_DISPOSED = "DISPOSED";

    /**
     * Property status sold.
     */
    private static final String STATUS_PROPERTY_SOLD = "SOLD";

    /**
     * Equipment status missing.
     */
    private static final String STATUS_EQ_MISSING = "miss";

    /**
     * Equipment|furniture status stolen.
     */
    private static final String STATUS_EQ_TA_STOLEN = "sto";

    /**
     * Equipment|furniture status donated.
     */
    private static final String STATUS_EQ_TA_DONATED = "don";

    /**
     * Equipment|furniture status disposed.
     */
    private static final String STATUS_EQ_TA_DISPOSED = "disp";

    /**
     * Equipment|furniture status sold.
     */
    private static final String STATUS_EQ_TA_SOLD = "sold";

    /**
     * Building status donated.
     */
    private static final String STATUS_BL_DONATED = "Donated";

    /**
     * Building|ownership (ot) status disposed.
     */
    private static final String STATUS_DISPOSED = "Disposed";

    /**
     * Disposal type lost.
     */
    private static final String DISPOSAL_TYPE_LOST = "Lost";

    /**
     * Disposal type stolen.
     */
    private static final String DISPOSAL_TYPE_STOLEN = "Stolen";

    /**
     * Disposal type donate.
     */
    private static final String DISPOSAL_TYPE_DONATE = "Donate";

    /**
     * Disposal type discard.
     */
    private static final String DISPOSAL_TYPE_DISCARD = "Discard";

    /**
     * Disposal type sell.
     */
    private static final String DISPOSAL_TYPE_SELL = "Sell";

    /**
     * Pending action status.
     */
    private static final String PENDING_ACTION_STATUS_N_A = "N/A";

    /**
     * Asset status based on disposal type.
     */
    @SuppressWarnings({ "PMD.AvoidStaticFields" })
    public static final Map<AssetType, String[][]> DISPOSAL_TYPE_STATUS;
    static {
        final Map<AssetType, String[][]> statusValues = new HashMap<AssetType, String[][]>();
        statusValues.put(AssetType.BUILDING,
            new String[][] { { DISPOSAL_TYPE_SELL, STATUS_DISPOSED },
                    { DISPOSAL_TYPE_DISCARD, STATUS_DISPOSED },
                    { DISPOSAL_TYPE_DONATE, STATUS_BL_DONATED } });
        statusValues.put(AssetType.EQUIPMENT,
            new String[][] { { DISPOSAL_TYPE_SELL, STATUS_EQ_TA_SOLD },
                    { DISPOSAL_TYPE_DISCARD, STATUS_EQ_TA_DISPOSED },
                    { DISPOSAL_TYPE_DONATE, STATUS_EQ_TA_DONATED },
                    { DISPOSAL_TYPE_STOLEN, STATUS_EQ_TA_STOLEN },
                    { DISPOSAL_TYPE_LOST, STATUS_EQ_MISSING } });
        statusValues.put(AssetType.FURNITURE,
            new String[][] { { DISPOSAL_TYPE_SELL, STATUS_EQ_TA_SOLD },
                    { DISPOSAL_TYPE_DISCARD, STATUS_EQ_TA_DISPOSED },
                    { DISPOSAL_TYPE_DONATE, STATUS_EQ_TA_DONATED },
                    { DISPOSAL_TYPE_STOLEN, STATUS_EQ_TA_STOLEN } });
        statusValues.put(AssetType.PROPERTY,
            new String[][] { { DISPOSAL_TYPE_SELL, STATUS_PROPERTY_SOLD },
                    { DISPOSAL_TYPE_DISCARD, STATUS_PROPERTY_DISPOSED },
                    { DISPOSAL_TYPE_STOLEN, STATUS_PROPERTY_DISPOSED },
                    { DISPOSAL_TYPE_LOST, STATUS_PROPERTY_DISPOSED },
                    { DISPOSAL_TYPE_DONATE, STATUS_PROPERTY_DONATED } });
        DISPOSAL_TYPE_STATUS = Collections.unmodifiableMap(statusValues);
    }

    /**
     * Ownership transaction description action.
     */
    // @translatable
    private static final String OWNERSHIP_DESCRIPTION_ACTION = "Action: {0}";

    /**
     * Ownership transaction description date.
     */
    // @translatable
    private static final String OWNERSHIP_DESCRIPTION_ON_DATE = "Executed On: {0}";

    /**
     * Asset type.
     */
    private final String assetType;

    /**
     * Asset id.
     */
    private final String assetId;

    /**
     * Disposal type.
     */
    private final String disposalType;

    /**
     * Pending action.
     */
    private String pendingAction;

    /**
     * Disposal comments.
     */
    private final String disposalComments;

    /**
     * Disposed value.
     */
    private double disposedValue;

    /**
     * Date disposed.
     */
    private final Date dateDisposed;

    /**
     * References to custom data sources used to load ownership transaction objects from the
     * database. These references are set by the Web Central container based on the Spring
     * configuration file com/archibus/eventhandler/eam/appContext-services.xml
     */
    private IOwnershipTransactionDao<OwnershipTransaction> ownershipTransactionDao;

    /**
     * Default AssetDisposal constructor.
     *
     * @param assetType Asset type.
     * @param assetId Asset id.
     * @param disposalType Disposal type.
     * @param disposalComments Disposal comments.
     * @param dateDisposed Date disposed.
     */
    public AssetDisposal(final String assetType, final String assetId, final String disposalType,
            final String disposalComments, final Date dateDisposed) {
        super();
        this.assetType = assetType;
        this.assetId = assetId;
        this.disposalType = disposalType;
        this.disposalComments = disposalComments;
        this.dateDisposed = dateDisposed;
    }

    /**
     * Dispose asset.
     *
     * @param applyOwnership Apply ownership transaction.
     */
    public void disposeAsset(final boolean applyOwnership) {
        final Asset assetDao = new Asset(AssetType.fromString(this.assetType));
        final AssetCommon asset = assetDao.getAsset(this.assetId);
        if (asset instanceof Building) {
            disposeBuilding((Building) asset, applyOwnership);
            if (applyOwnership) {
                writeOwnershipTransaction(assetDao, asset);
            }
        }
        if (asset instanceof Equipment) {
            disposeEquipment((Equipment) asset, applyOwnership);
        }
        if (asset instanceof Furniture) {
            disposeFurniture((Furniture) asset, applyOwnership);
        }
        if (asset instanceof Property) {
            disposeProperty((Property) asset, applyOwnership);
            if (applyOwnership) {
                writeOwnershipTransaction(assetDao, asset);
            }
        }
        assetDao.getDao().update(asset);
    }

    /**
     * Dispose building.
     *
     * @param asset Asset building.
     * @param applyOwnership Apply ownership transaction.
     */
    private void disposeBuilding(final Building asset, final boolean applyOwnership) {
        String assetStatus = asset.getStatus();
        if (applyOwnership) {
            assetStatus = getAssetDisposalStatus(AssetType.BUILDING, this.disposalType);
            this.pendingAction = PENDING_ACTION_STATUS_N_A;
        }
        asset.setDisposalType(this.disposalType);
        asset.setDateDisposal(this.dateDisposed);
        asset.setCommentDisposal(this.disposalComments);
        asset.setPendingAction(this.pendingAction);
        asset.setStatus(assetStatus);
    }

    /**
     * Dispose equipment.
     *
     * @param asset Asset equipment.
     * @param applyOwnership Apply ownership transaction.
     */
    private void disposeEquipment(final Equipment asset, final boolean applyOwnership) {
        String assetStatus = asset.getStatus();
        if (applyOwnership) {
            assetStatus = getAssetDisposalStatus(AssetType.EQUIPMENT, this.disposalType);
            this.pendingAction = PENDING_ACTION_STATUS_N_A;
        }
        asset.setDisposalType(this.disposalType);
        asset.setDateDisposal(this.dateDisposed);
        asset.setCommentDisposal(this.disposalComments);
        asset.setPendingAction(this.pendingAction);
        asset.setValueSalvage(this.disposedValue);
        asset.setStatus(assetStatus);
        asset.setDateOfStatusChanged(Utility.currentDate());
    }

    /**
     * Dispose furniture.
     *
     * @param asset Asset furniture.
     * @param applyOwnership Apply ownership transaction.
     */
    private void disposeFurniture(final Furniture asset, final boolean applyOwnership) {
        String assetStatus = asset.getStatus();
        if (applyOwnership) {
            assetStatus = getAssetDisposalStatus(AssetType.FURNITURE, this.disposalType);
            this.pendingAction = PENDING_ACTION_STATUS_N_A;
        }
        asset.setDisposalType(this.disposalType);
        asset.setDateDisposal(this.dateDisposed);
        asset.setCommentDisposal(this.disposalComments);
        asset.setPendingAction(this.pendingAction);
        asset.setValueSalvage(this.disposedValue);
        asset.setStatus(assetStatus);
    }

    /**
     * Dispose property.
     *
     * @param asset Asset property.
     * @param applyOwnership Apply ownership transaction.
     */
    private void disposeProperty(final Property asset, final boolean applyOwnership) {
        String assetStatus = asset.getStatus();
        if (applyOwnership) {
            assetStatus = getAssetDisposalStatus(AssetType.PROPERTY, this.disposalType);
            this.pendingAction = PENDING_ACTION_STATUS_N_A;
        }
        asset.setDisposalType(this.disposalType);
        asset.setDateDisposal(this.dateDisposed);
        asset.setCommentDisposal(this.disposalComments);
        asset.setPendingAction(this.pendingAction);
        asset.setStatus(assetStatus);
    }

    /**
     * Write ownership transaction for asset.
     *
     * @param assetDao Asset dao object.
     * @param asset Asset object.
     */
    private void writeOwnershipTransaction(final Asset assetDao, final AssetCommon asset) {
        if (this.ownershipTransactionDao == null) {
            this.ownershipTransactionDao = new OwnershipTransactionDataSource();
        }
        final OwnershipTransaction ownershipTransaction =
                this.ownershipTransactionDao.createNewTransaction();
        if (asset instanceof Building) {
            ownershipTransaction.setBuildingId(((Building) asset).getBuildingId());
        } else if (asset instanceof Property) {
            ownershipTransaction.setPropertyId(((Property) asset).getPropertyId());
        }

        ownershipTransaction.setStatus(STATUS_DISPOSED);
        ownershipTransaction.setDescription(getOwnershipDescription(assetDao));
        ownershipTransaction.setComments(this.disposalComments);
        ownershipTransaction.setDateSold(this.dateDisposed);
        ownershipTransaction.setCostSelling(this.disposedValue);
        this.ownershipTransactionDao.save(ownershipTransaction);
    }

    /**
     *
     * Get ownership description.
     *
     * @param assetDao Asset dao object.
     * @return localized description
     */
    private String getOwnershipDescription(final Asset assetDao) {
        final String localizedDescription = AssetManagementHelper
            .getLocalizedMessage(OWNERSHIP_DESCRIPTION_ACTION, this.getClass().getName());
        String description = MessageFormat.format(localizedDescription,
            assetDao.getDao().getLocalizedDisposalType(this.disposalType));
        if (this.dateDisposed != null) {
            final String localizedDescriptionDate = AssetManagementHelper
                .getLocalizedMessage(OWNERSHIP_DESCRIPTION_ON_DATE, this.getClass().getName());
            description += " " + MessageFormat.format(localizedDescriptionDate,
                SimpleDateFormat
                    .getDateInstance(SimpleDateFormat.SHORT, ContextStore.get().getLocale())
                    .format(this.dateDisposed));
        }
        return description;
    }

    /**
     *
     * Get asset disposal status.
     *
     * @param assetType Asset type.
     * @param disposalType Disposal type.
     * @return Asset status based on disposal type.
     */
    private String getAssetDisposalStatus(final AssetType assetType, final String disposalType) {
        String result = null;
        final String[][] disposalSatusValues = DISPOSAL_TYPE_STATUS.get(assetType);
        for (final String[] disposalSatus : disposalSatusValues) {
            if (disposalSatus[0].equals(disposalType)) {
                result = disposalSatus[1];
                break;
            }
        }
        return result;
    }

    /**
     * Getter for the assetType property.
     *
     * @return the assetType property.
     */
    public String getAssetType() {
        return this.assetType;
    }

    /**
     * Getter for the assetId property.
     *
     * @return the assetId property.
     */
    public String getAssetId() {
        return this.assetId;
    }

    /**
     * Getter for the disposalType property.
     *
     * @return the disposalType property.
     */
    public String getDisposalType() {
        return this.disposalType;
    }

    /**
     * Getter for the pendingAction property.
     *
     * @return the pendingAction property.
     */
    public String getPendingAction() {
        return this.pendingAction;
    }

    /**
     * Setter for the pendingAction property.
     *
     * @param pendingAction the pendingAction to set.
     */
    public void setPendingAction(final String pendingAction) {
        this.pendingAction = pendingAction;
    }

    /**
     * Getter for the disposalComments property.
     *
     * @return the disposalComments property.
     */
    public String getDisposalComments() {
        return this.disposalComments;
    }

    /**
     * Getter for the disposedValue property.
     *
     * @return the disposedValue property.
     */
    public double getDisposedValue() {
        return this.disposedValue;
    }

    /**
     * Setter for the disposedValue property.
     *
     * @param disposedValue the disposedValue to set.
     */
    public void setDisposedValue(final double disposedValue) {
        this.disposedValue = disposedValue;
    }

    /**
     * Getter for the dateDisposed property.
     *
     * @return the dateDisposed property.
     */
    public Date getDateDisposed() {
        return this.dateDisposed;
    }
}