package com.archibus.eventhandler.eam.dao.datasource;

import org.apache.commons.lang.ArrayUtils;

import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.AssetCommon;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for equipment table.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class EquipmentDataSource extends AbstractAssetDataSource<AssetCommon> {
	/**
	 * Field names to equipment names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { DbConstants.EQ_ID, "equipmentId" },
			{ "value_salvage", "valueSalvage" }, { "date_of_stat_chg", "dateOfStatusChanged" } };

	/**
	 * Constructs EquipmentDataSource, mapped to <code>eq</code> table, using
	 * <code>equipment</code> bean.
	 */
	public EquipmentDataSource() {
		super("assetEquipment", DbConstants.EQUIPMENT_TABLE);
	}

	/**
	 * Merge fieldsToProperties from the base class.
	 *
	 * @return fieldsToPropertiesMerged
	 */
	@Override
	protected String[][] getFieldsToProperties() {
		final String[][] fieldsToPropertiesMerged = (String[][]) ArrayUtils.addAll(super.getFieldsToProperties(),
				FIELDS_TO_PROPERTIES);
		return fieldsToPropertiesMerged;
	}
}