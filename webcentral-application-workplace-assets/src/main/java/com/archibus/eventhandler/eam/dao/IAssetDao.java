package com.archibus.eventhandler.eam.dao;

import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for asset object.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 * @param <AssetCommon>
 *            Asset common object.
 */
public interface IAssetDao<AssetCommon> extends IDao<AssetCommon> {
	/**
	 * Get localized disposal type.
	 *
	 * @param disposalTypeValue
	 *            Disposal type value.
	 * @return Localized disposal type.
	 */
	String getLocalizedDisposalType(String disposalTypeValue);
}
