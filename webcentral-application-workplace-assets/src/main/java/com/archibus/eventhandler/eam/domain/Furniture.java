package com.archibus.eventhandler.eam.domain;

import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for furniture. Mapped to <code>ta</code> table
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class Furniture extends AssetCommon {
	/**
	 * Field ta_id.
	 */
	private String furnitureId;

	/**
	 * Field value_salvage.
	 */
	private double valueSalvage;

	/**
	 * Default constructor.
	 */
	public Furniture() {
		super(AssetType.FURNITURE);
	}

	/**
	 * Furniture constructor.
	 *
	 * @param furnitureId
	 *            Furniture id.
	 */
	public Furniture(final String furnitureId) {
		super(AssetType.FURNITURE);
		this.furnitureId = furnitureId;
	}

	/**
	 * Getter for the furnitureId property.
	 *
	 * @return the furnitureId property.
	 */
	public String getFurnitureId() {
		return this.furnitureId;
	}

	/**
	 * Setter for the furnitureId property.
	 *
	 * @param furnitureId
	 *            the furnitureId to set.
	 */
	public void setFurnitureId(final String furnitureId) {
		this.furnitureId = furnitureId;
	}

	/**
	 * Getter for the valueSalvage property.
	 *
	 * @return the valueSalvage property.
	 */
	public double getValueSalvage() {
		return this.valueSalvage;
	}

	/**
	 * Setter for the valueSalvage property.
	 *
	 * @param valueSalvage
	 *            the valueSalvage to set.
	 */
	public void setValueSalvage(final double valueSalvage) {
		this.valueSalvage = valueSalvage;
	}

}