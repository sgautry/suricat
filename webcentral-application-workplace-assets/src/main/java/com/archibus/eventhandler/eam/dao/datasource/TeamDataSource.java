package com.archibus.eventhandler.eam.dao.datasource;

import java.util.*;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.db.ViewField;
import com.archibus.eventhandler.eam.dao.ITeamDao;
import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.TeamMember;
import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for team table.
 * <p>
 *
 *
 * @author draghici
 * @since 23.2
 *
 */
public class TeamDataSource extends ObjectDataSourceImpl<TeamMember> implements ITeamDao<TeamMember> {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "address_archive", "addressArchive" },
			{ "autonumbered_id", "autonumberedId" }, { DbConstants.BL_ID, "buildingId" },
			{ "cell_num_archive", "cellNumArchive" }, { "company_archive", "companyArchive" },
			{ "contact_id", "contactId" }, { "contact_type_archive", "contactTypeArchive" },
			{ DbConstants.CUSTODIAN_TYPE, "custodianType" }, { "date_end", "dateEnd" }, { "date_start", "dateStart" },
			{ "date_verified", "dateVerified" }, { "digital_signature", "digitalSignature" }, { "em_id", "employeeId" },
			{ "email_archive", "emailArchive" }, { "eq_id", "equipmentId" }, { "fax_archive", "faxArchive" },
			{ DbConstants.IS_OWNER, "isOwner" }, { "member_type", "memberType" }, { "name_archive", "nameArchive" },
			{ DbConstants.NOTES, DbConstants.NOTES }, { DbConstants.OPTION1, DbConstants.OPTION1 },
			{ DbConstants.OPTION2, DbConstants.OPTION2 }, { "pct_time", "pctTime" },
			{ "phone_archive", "phoneArchive" }, { "pr_id", "propertyId" }, { "project_id", "projectId" },
			{ "site_id", "siteId" }, { "source_table", "sourceTable" }, { DbConstants.STATUS, DbConstants.STATUS },
			{ "ta_id", "taggedFurnitureId" }, { "team_id", "teamId" }, { DbConstants.TEAM_TYPE, "teamType" },
			{ "vn_id", "vendorId" } };

	/**
	 * Constructs SpaceBudgetDataSource, mapped to <code>sb</code> table, using
	 * <code>spaceBudget</code> bean.
	 *
	 */
	public TeamDataSource() {
		super("teamMember", "team");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TeamMember getOwner(final String assetId, final AssetType assetType) {
		final DataRecord record = getOwnerRecord(assetId, assetType);
		return new DataSourceObjectConverter<TeamMember>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TeamMember getTeamMemberById(final int autonumberedId) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();
		dataSource.addRestriction(Restrictions.eq(this.tableName, DbConstants.AUTONUMBERED_ID, autonumberedId));
		final DataRecord record = dataSource.getRecord();
		return new DataSourceObjectConverter<TeamMember>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DataRecord getOwnerRecord(final String assetId, final AssetType assetType) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();
		final String assetIdField = getAssetFieldName(assetType);
		dataSource.addRestriction(Restrictions.and(Restrictions.eq(this.tableName, DbConstants.IS_OWNER, 1),
				Restrictions.eq(this.tableName, DbConstants.TEAM_TYPE, DbConstants.TEAM_TYPE_EQUIPMENT),
				Restrictions.eq(this.tableName, DbConstants.STATUS, DbConstants.STATUS_ACTIVE)));
		dataSource.addRestriction(Restrictions.and(Restrictions.isNotNull(this.tableName, DbConstants.CUSTODIAN_TYPE),
				Restrictions.eq(this.tableName, assetIdField, assetId)));
		return dataSource.getRecord();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TeamMember getLastOwner(final String assetId, final AssetType assetType) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();
		final String assetIdField = getAssetFieldName(assetType);
		dataSource.addRestriction(Restrictions.and(Restrictions.eq(this.tableName, DbConstants.IS_OWNER, 1),
				Restrictions.eq(this.tableName, DbConstants.TEAM_TYPE, DbConstants.TEAM_TYPE_EQUIPMENT),
				Restrictions.eq(this.tableName, DbConstants.STATUS, DbConstants.STATUS_INACTIVE)));
		dataSource.addRestriction(Restrictions.and(Restrictions.isNotNull(this.tableName, DbConstants.CUSTODIAN_TYPE),
				Restrictions.eq(this.tableName, assetIdField, assetId),
				Restrictions.isNotNull(this.tableName, DbConstants.DATE_END)));
		dataSource.addSort(this.tableName, DbConstants.DATE_END, DataSource.SORT_DESC);
		final DataRecord record = dataSource.getRecord();
		return new DataSourceObjectConverter<TeamMember>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TeamMember> getTeamMembers(final String assetId, final AssetType assetType,
			final Restriction restriction) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();
		final String assetIdField = getAssetFieldName(assetType);
		dataSource.addRestriction(Restrictions.and(Restrictions.eq(this.tableName, assetIdField, assetId),
				Restrictions.eq(this.tableName, DbConstants.TEAM_TYPE, DbConstants.TEAM_TYPE_EQUIPMENT),
				Restrictions.isNotNull(this.tableName, DbConstants.CUSTODIAN_TYPE)));
		if (restriction != null) {
			dataSource.addRestriction(restriction);
		}
		final List<TeamMember> teamMembers = new ArrayList<TeamMember>();
		final List<DataRecord> records = dataSource.getRecords();
		for (final DataRecord record : records) {
			final int autonumberedId = record
					.getInt(DbConstants.getFullFieldName(this.tableName, DbConstants.AUTONUMBERED_ID));
			final TeamMember teamMember = this.getTeamMemberById(autonumberedId);
			teamMembers.add(teamMember);
		}
		return teamMembers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TeamMember createNewMember(final DataRecord teamRecord, final String assetId, final AssetType assetType) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();

        DataRecord record = getTeamMemberRecordWithUnassignedAssets(teamRecord);
        if (record == null) {
            record = dataSource.createNewRecord();
        }

		final List<ViewField.Immutable> fields = dataSource.getAllFields();
		for (final ViewField.Immutable fieldDef : fields) {
			if (!fieldDef.isAutoNumber() && teamRecord.valueExists(fieldDef.fullName())) {
				record.setValue(fieldDef.fullName(), teamRecord.getValue(fieldDef.fullName()));
			}
		}
		final String assetIdField = DbConstants.getFullFieldName(this.tableName, getAssetFieldName(assetType));
		record.setValue(assetIdField, assetId);
		return new DataSourceObjectConverter<TeamMember>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

    /**
     *
     * Return team member record with unassigned assets.
     *
     * @param teamRecord team form data record
     * @return {@link DataRecord}
     */
    private DataRecord getTeamMemberRecordWithUnassignedAssets(final DataRecord teamRecord) {
        final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
        dataSource.checkSetContext();
        dataSource.addRestriction(Restrictions.and(new Restrictions.Restriction.Clause[] {
                Restrictions.eq(this.tableName, DbConstants.TEAM_TYPE,
                    DbConstants.TEAM_TYPE_EQUIPMENT),
                Restrictions.eq(this.tableName, DbConstants.STATUS, DbConstants.STATUS_ACTIVE),
                Restrictions.eq(this.tableName, DbConstants.CUSTODIAN_TYPE,
                    teamRecord.getValue(
                        DbConstants.getFullFieldName(this.tableName, DbConstants.CUSTODIAN_TYPE))),
                Restrictions.isNull(this.tableName, DbConstants.EQ_ID),
                Restrictions.isNull(this.tableName, DbConstants.TA_ID),
                Restrictions.isNull(this.tableName, DbConstants.BL_ID),
                Restrictions.isNull(this.tableName, DbConstants.PR_ID) }));

        final String sourceTable = teamRecord
            .getString(DbConstants.getFullFieldName(this.tableName, DbConstants.SOURCE_TABLE));
        final String fieldName = sourceTable + "_id";
        final String custodianName = teamRecord
            .getString(DbConstants.getFullFieldName(this.tableName, fieldName));
        dataSource
            .addRestriction(Restrictions.eq(this.tableName, fieldName, custodianName));
        return dataSource.getRecord();
    }

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

	/**
	 * Get database field name for asset type.
	 *
	 * @param assetType
	 *            asset type
	 * @return String
	 */
	private String getAssetFieldName(final AssetType assetType) {
		String result = "";
		if (AssetType.EQUIPMENT.equals(assetType)) {
			result = DbConstants.EQ_ID;
		} else if (AssetType.FURNITURE.equals(assetType)) {
			result = DbConstants.TA_ID;
		} else if (AssetType.PROPERTY.equals(assetType)) {
			result = DbConstants.PR_ID;
		} else if (AssetType.BUILDING.equals(assetType)) {
			result = DbConstants.BL_ID;
		}
		return result;
	}

}
