package com.archibus.eventhandler.eam.dao.datasource;

import java.util.List;

import com.archibus.context.ContextStore;
import com.archibus.datasource.ObjectDataSourceImpl;
import com.archibus.db.ViewField;
import com.archibus.eventhandler.eam.dao.IAssetDao;
import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.schema.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Data source for asset object.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public abstract class AbstractAssetDataSource<AssetCommon> extends ObjectDataSourceImpl<AssetCommon>
		implements IAssetDao<AssetCommon> {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 * <p>
	 * Fields common for all asset data sources are specified here.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { DbConstants.STATUS, "status" },
			{ "disposal_type", "disposalType" }, { "date_disposal", "dateDisposal" },
			{ "comment_disposal", "commentDisposal" }, { "pending_action", "pendingAction" } };

	/**
	 * Constructs AbstractAssetDataSource, mapped to <code>tableName</code>
	 * table, using <code>beanName</code> bean.
	 *
	 * @param tableName
	 *            Table name to map to.
	 * @param beanName
	 *            Bean name to use.
	 */
	protected AbstractAssetDataSource(final String beanName, final String tableName) {
		super(beanName, tableName);
	}

	/** {@inheritDoc} */
	@Override
	public String getLocalizedDisposalType(final String disposalTypeValue) {
		String localizedValue = null;
		final ViewField.Immutable field = this.findField(this.tableName + ".disposal_type");
		final ArchibusFieldDefBase.Immutable fieldDef = field.getFieldDef();
		if (fieldDef instanceof FieldEnumImpl) {
			final FieldEnumImpl fieldDefEnum = (FieldEnumImpl) fieldDef;
			for (final Object enumMappingObject : fieldDefEnum
					.getValues(ContextStore.get().getUserSession().getLocale())) {
				final List<?> enumMapping = (List<?>) enumMappingObject;
				final String value = String.valueOf(enumMapping.get(0));
				final String displayedValue = String.valueOf(enumMapping.get(1));
				if (disposalTypeValue.equals(value)) {
					localizedValue = displayedValue;
					break;
				}
			}
		}
		return localizedValue;
	}

	/** {@inheritDoc} */
	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}