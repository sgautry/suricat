package com.archibus.eventhandler.eam.domain;

import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for property. Mapped to <code>property</code> table
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class Property extends AssetCommon {
	/**
	 * Field pr_id.
	 */
	private String propertyId;

	/**
	 * Default constructor.
	 */
	public Property() {
		super(AssetType.PROPERTY);
	}

	/**
	 * Property constructor.
	 *
	 * @param propertyId
	 *            Property id.
	 */
	public Property(final String propertyId) {
		super(AssetType.PROPERTY);
		this.propertyId = propertyId;
	}

	/**
	 * Getter for the propertyId property.
	 *
	 * @return the propertyId property.
	 */
	public String getPropertyId() {
		return this.propertyId;
	}

	/**
	 * Setter for the propertyId property.
	 *
	 * @param propertyId
	 *            the propertyId to set.
	 */
	public void setPropertyId(final String propertyId) {
		this.propertyId = propertyId;
	}
}