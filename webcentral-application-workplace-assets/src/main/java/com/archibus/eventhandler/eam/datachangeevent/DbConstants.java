package com.archibus.eventhandler.eam.datachangeevent;

/**
 * Database related constants = table and field names.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @since 22.1
 *
 */
public final class DbConstants {

	/**
	 * Constant - table name.
	 */
	public static final String EQUIPMENT_DEPRECIATION_TABLE = "eq_dep";

	/**
	 * Constant - table name.
	 */
	public static final String FURNITURE_DEPRECIATION_TABLE = "ta_dep";

	/**
	 * Constant - table name.
	 */
	public static final String BUILDING_TABLE = "bl";

	/**
	 * Constant - table name.
	 */
	public static final String EQUIPMENT_TABLE = "eq";

	/**
	 * Constant - table name.
	 */
	public static final String FURNITURE_TABLE = "ta";

	/**
	 * Constant - table name.
	 */
	public static final String PROPERTY_TABLE = "property";

	/**
	 * Constant - table name.
	 */
	public static final String TEAM_TABLE = "team";

    /**
     * Constant - table name.
     */
    public static final String CUSTODIANTYPE_TABLE = "custodiantype";

	/**
	 * Constant - field name.
	 */
	public static final String EQ_ID = "eq_id";

	/**
	 * Constant - field name.
	 */
	public static final String TA_ID = "ta_id";

	/**
	 * Constant - field name.
	 */
	public static final String NOTES = "notes";

	/**
	 * Constant - field name.
	 */
	public static final String OPTION1 = "option1";

	/**
	 * Constant - field name.
	 */
	public static final String OPTION2 = "option2";

	/**
	 * Constant - field name.
	 */
	public static final String STATUS = "status";

	/**
	 * Constant - field name.
	 */
	public static final String BL_ID = "bl_id";

	/**
	 * Constant - field name.
	 */
	public static final String CUSTODIAN_TYPE = "custodian_type";

	/**
	 * Constant - field name.
	 */
	public static final String IS_OWNER = "is_owner";

    /**
     * Constant - field name.
     */
    public static final String SOURCE_TABLE = "source_table";

	/**
	 * Constant - field name.
	 */
	public static final String PR_ID = "pr_id";

	/**
	 * Constant - field name.
	 */
	public static final String TEAM_TYPE = "team_type";

	/**
	 * Constant - field value.
	 */
	public static final String TEAM_TYPE_EQUIPMENT = "Equipment";

	/**
	 * Constant - field value.
	 */
	public static final String STATUS_ACTIVE = "Active";

	/**
	 * Constant - field value.
	 */
	public static final String STATUS_INACTIVE = "Inactive";

	/**
	 * Constant - field name.
	 */
	public static final String AUTONUMBERED_ID = "autonumbered_id";

	/**
	 * Constant - field name.
	 */
	public static final String ASSET_ID = "asset_id";

	/**
	 * Constant - field name.
	 */
	public static final String ASSET_TYPE = "asset_type";

    /**
     * Constant - field name.
     */
    public static final String ASSET_STATUS = "asset_status";

	/**
	 * Constant - field name.
	 */
	public static final String NAME_ARCHIVE = "name_archive";

	/**
	 * Constant - field name.
	 */
	public static final String DATE_END = "date_end";

	/**
	 * Constant - field name.
	 */
	public static final String DATE_SOLD = "date_sold";
	/**
	 * Constant - field name.
	 */
	public static final String DIGITAL_SIGNATURE = "digital_signature";

    /**
     * Constant - field name.
     */
    public static final String DATE_DEP_CLOSING_MONTH = "date_dep_closing_month";

    /**
     * Constant - field name.
     */
    public static final String COST_DEP_VALUE = "cost_dep_value";

	/**
	 * Constant - field name.
	 */
	public static final String COMMENTS = "comments";

	/**
	 * Constant - field name.
	 */
	public static final String DESCRIPTION = "description";

	/**
	 * Constant.
	 */
	public static final String DOT = ".";

	/**
	 *
	 * Private default constructor: utility class is non-instantiable.
	 */
	private DbConstants() {

	}

	/**
	 * Returns full field name.
	 *
	 * @param tableName
	 *            table name
	 * @param fieldName
	 *            field name
	 * @return string
	 */
	public static String getFullFieldName(final String tableName, final String fieldName) {
		return tableName + DOT + fieldName;
	}

}
