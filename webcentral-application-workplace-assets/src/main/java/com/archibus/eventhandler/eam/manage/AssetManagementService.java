package com.archibus.eventhandler.eam.manage;

import java.text.SimpleDateFormat;
import java.util.*;

import org.json.*;

import com.archibus.context.ContextStore;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.eventhandler.eam.dao.*;
import com.archibus.eventhandler.eam.dao.datasource.*;
import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.*;
import com.archibus.jobmanager.*;
import com.archibus.utility.*;
import com.ibm.icu.text.MessageFormat;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Version 23.2 Enterprise Asset Management - Chain if Custody.
 * <p>
 * Registered in Archibus WFR-ules table as AbAssetManagement-AssetManagementService
 * <p>
 * Provides methods for asset management - change asset owner and custodians
 * <p>
 *
 *
 * @author draghici
 * @author Radu Bunea
 * @since 23.2
 *        <p>
 *        Suppress Warning "PMD.TooManyMethods". Justification: All methods belong in the
 *        AssetManagementService, there are using domain object for assets
 */
@SuppressWarnings({ "PMD.TooManyMethods" })
public class AssetManagementService extends JobBase {
    /**
     * Custodian note on terminate ownerdate.
     */
    // @translatable
    private static final String CUSTODIAN_TERMINATE_ON_DATE =
            "Custodian terminated due to asset disposal on {0}";

    /**
     * When assign owner to disposed asset.
     */
    // @translatable
    private static final String ASSIGN_OWNER_TO_DISPOSED_ASSET =
            "Cannot add owner custodian to a disposed asset. [{0}: {1}]";

    /**
     * References to custom data sources used to load team objects from the database. These
     * references are set by the Web Central container based on the Spring configuration file
     * com/archibus/eventhandler/eam/appContext-services.xml
     */
    private ITeamDao<TeamMember> teamDao;

    /**
     * References to custom data sources used to load asset transaction objects from the database.
     * These references are set by the Web Central container based on the Spring configuration file
     * applications-child-context.xml
     */
    private IAssetTransactionDao<AssetTransaction> assetTransactionDao;

    /**
     * Validate new owner values before save. Verify if start date is at least one day after start
     * date of selected assets.
     *
     * @param formValues form values
     * @param selectedAssets array with selected assets ( {assetType, assetId})
     * @param useCustomRestriction if custom restriction is used to receive assets
     * @param customRestriction object with custom restriction
     * @return Map<String, String> object with assets that fail the test
     */
    public JSONObject validateNewOwner(final Map<String, String> formValues,
            final JSONArray selectedAssets, final boolean useCustomRestriction,
            final Map<String, String> customRestriction) {
        initPerRequestState();
        final Map<String, String> dataSourceParameters = AssetManagementHelper
            .getDataSourceParameters(selectedAssets, useCustomRestriction, customRestriction);
        final Restriction dateDiffRestriction =
                Restrictions.sql("${sql.dateDiffCalendar('day', 'bl.owner_start_date', '#"
                        + formValues.get("team.date_start") + "')} < 1");
        final List<DataRecord> records =
                AssetManagementHelper.getAssetRecords(dataSourceParameters, dateDiffRestriction);

        final JSONObject result = new JSONObject();
        if (records.isEmpty()) {
            result.put(Constants.VALID_KEY, true);
        } else {
            result.put(Constants.VALID_KEY, false);
            result.put("assets", AssetManagementHelper.toJSONArray(records, new String[] {
                    "bl.asset_type", "bl.asset_id", "bl.owner_custodian", "bl.owner_start_date" }));
        }

        return result;
    }


    /**
     * Change owner for selected assets.
     *
     * @param recordOwner new owner data record
     * @param selectedAssets selected assets
     * @param useCustomRestriction if custom restriction is used to get assets
     * @param customRestriction custom restriction object
     */
    public void changeOwner(final DataRecord recordOwner, final JSONArray selectedAssets,
            final boolean useCustomRestriction, final Map<String, String> customRestriction) {
        initPerRequestState();
        final List<Integer> teamPkeyIds = new ArrayList<Integer>();
        final Map<String, String> dataSourceParameters = AssetManagementHelper
            .getDataSourceParameters(selectedAssets, useCustomRestriction, customRestriction);
        final List<DataRecord> records =
                AssetManagementHelper.getAssetRecords(dataSourceParameters, null);
        final int assetNo = records.size();
        this.status.setTotalNumber(assetNo);
        for (final DataRecord record : records) {
            if (this.status.isStopRequested()) {
                this.status.setCode(JobStatus.JOB_STOPPED);
                break;
            }

            final boolean assetIsDsposed = AssetManagementHelper.isAssetDisposed(record);
            if (assetIsDsposed) {
                this.status.setCode(JobStatus.JOB_STOPPED);

                final String assetIdFieldName = DbConstants
                    .getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_ID);
                final String assetTypeFieldName = DbConstants
                    .getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_TYPE);

                final String assetId = record.getString(assetIdFieldName);
                final AssetType assetType =
                        AssetType.fromString(record.getString(assetTypeFieldName));

                final ExceptionBase exception = new ExceptionBase();
                exception.setPattern(ASSIGN_OWNER_TO_DISPOSED_ASSET);
                exception.setTranslatable(true);
                exception.setArgs(new Object[] { assetType, assetId });
                throw exception;
            } else {
                teamPkeyIds.add(changeOwnerForAsset(recordOwner, record));
                this.status.incrementCurrentNumber();
            }

        }
        this.status.setCurrentNumber(assetNo);
        this.status.addProperty("teamPkeyIds", teamPkeyIds.toString());
    }

    /**
     * Add custodian to selected assets.
     *
     * @param recordCustodian new custodian data
     * @param selectedAssets selected assets
     * @param useCustomRestriction if custom restriction is used to get assets
     * @param customRestriction custom restriction object
     */
    public void addCustodian(final DataRecord recordCustodian, final JSONArray selectedAssets,
            final boolean useCustomRestriction, final Map<String, String> customRestriction) {
        initPerRequestState();
        final Map<String, String> dataSourceParameters = AssetManagementHelper
            .getDataSourceParameters(selectedAssets, useCustomRestriction, customRestriction);
        final List<DataRecord> records =
                AssetManagementHelper.getAssetRecords(dataSourceParameters, null);
        final int assetNo = records.size();
        this.status.setTotalNumber(assetNo);
        for (final DataRecord record : records) {
            if (this.status.isStopRequested()) {
                this.status.setCode(JobStatus.JOB_STOPPED);
                break;
            }
            addCustodianToAsset(recordCustodian, record);
            this.status.incrementCurrentNumber();
        }
        this.status.setCurrentNumber(assetNo);
    }

    /**
     * Terminate owner custodian. Called when custodian status is changed from active to inactive.
     *
     * @param custodianRecord custodian record (pk value, status and end date)
     * @param lastStatus custodian previous status
     */
    public void terminateOwnerCustodian(final DataRecord custodianRecord, final String lastStatus) {
        initPerRequestState();
        final int custodianId = custodianRecord.getInt(
            DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.AUTONUMBERED_ID));
        final TeamMember custodian = this.teamDao.getTeamMemberById(custodianId);
        terminateOwnerCustodian(custodian, lastStatus);
    }

    /**
     * Terminate owner custodian. Called when asset for custodian is disposed.
     *
     * @param custodian custodian team member object
     * @param lastStatus custodian previous status
     */
    public void terminateOwnerCustodian(final TeamMember custodian, final String lastStatus) {
        initPerRequestState();
        final AssetTransaction assetTransaction =
                AssetManagementHelper.getTerminateCustodianTransaction(
                    this.assetTransactionDao.createNewTransaction(), custodian, lastStatus);
        this.assetTransactionDao.save(assetTransaction);
    }

    /**
     * Add new owner custodian transaction.
     *
     * @param custodianRecord custodian record (pk value)
     */
    public void addOwnerCustodian(final DataRecord custodianRecord) {
        initPerRequestState();
        final int custodianId = custodianRecord.getInt(
            DbConstants.getFullFieldName(DbConstants.TEAM_TABLE, DbConstants.AUTONUMBERED_ID));
        final TeamMember custodian = this.teamDao.getTeamMemberById(custodianId);
        final String assetId = custodian.getAssetId();
        final AssetType assetType = custodian.getAssetType();

        final TeamMember lastOwner = this.teamDao.getLastOwner(assetId, assetType);
        String ownerName = null;
        if (StringUtil.notNullOrEmpty(lastOwner)) {
            ownerName = lastOwner.getTeamMemberName();
        }
        final AssetTransaction assetTransaction =
                AssetManagementHelper.getAddOwnerCustodianTransaction(
                    this.assetTransactionDao.createNewTransaction(), custodian, ownerName);
        this.assetTransactionDao.save(assetTransaction);
    }

    /**
     * Dispose assets and write ownership transactions.
     *
     * @param selectedAssets List of assets to dispose.
     * @param disposalRecord Disposal form record.
     * @param disposedValue Disposal value.
     * @param applyOwnership Apply ownership transaction.
     */
    public void disposeAssets(final JSONObject selectedAssets, final DataRecord disposalRecord,
            final double disposedValue, final boolean applyOwnership) {
        this.status.setTotalNumber(selectedAssets.length());
        initPerRequestState();
        for (final Iterator<?> iterator = selectedAssets.keys(); iterator.hasNext();) {
            if (this.status.isStopRequested()) {
                this.status.setCode(JobStatus.JOB_STOPPED);
                break;
            }
            this.status.incrementCurrentNumber();
            final String assetType = (String) iterator.next();
            final JSONArray assetIds = selectedAssets.getJSONArray(assetType);
            for (int i = 0; i < assetIds.length(); i++) {
                final String assetId = assetIds.getString(i);
                final String disposalType = disposalRecord.getString("bl.disposal_type");
                final String pendingAction = disposalRecord.getString("bl.pending_action");
                final String disposalComments = disposalRecord.getString("bl.comment_disposal");
                Date dateDisposed = disposalRecord.getDate("bl.date_disposal");
                if (dateDisposed == null) {
                    dateDisposed = Utility.currentDate();
                }

                // dispose asset
                final AssetDisposal assetDisposal = new AssetDisposal(assetType, assetId,
                    disposalType, disposalComments, dateDisposed);
                assetDisposal.setPendingAction(pendingAction);
                assetDisposal.setDisposedValue(disposedValue);
                assetDisposal.disposeAsset(applyOwnership);

                if (applyOwnership) {
                    // get owner custodian terminate ownership
                    terminateAssetOwnerCustodian(AssetType.fromString(assetType), assetId,
                        dateDisposed);
                    // get other active custodians and terminate ownership
                    terminateAssetOtherActiveCustodians(AssetType.fromString(assetType), assetId,
                        dateDisposed);
                }
            }
        }
    }

    /**
     *
     * Copy digital signature document for all team records for selected assets.
     *
     * @param sourceDocumentId team.autonumbered_id
     * @param targetDocumentIds List of team.autonumbered_id
     * @param fieldName digital_signature field
     * @param parameters document parameters
     */
    public void copyDigitalSignatureDocument(final String sourceDocumentId,
            final List<Integer> targetDocumentIds, final String fieldName,
            final JSONObject parameters) {
        AssetManagementHelper.copyDigitalSignatureDocument(sourceDocumentId, targetDocumentIds,
            fieldName, parameters);
    }

    /**
     *
     * Delete digital signature document for all team records for selected assets.
     *
     * @param sourceDocumentId team.autonumbered_id
     * @param targetDocumentIds List of team.autonumbered_id
     * @param fieldName digital_signature field
     * @param parameters document parameters
     */
    public void deleteDigitalSignatureDocument(final String sourceDocumentId,
            final List<Integer> targetDocumentIds, final String fieldName,
            final JSONObject parameters) {
        AssetManagementHelper.deleteDigitalSignatureDocument(sourceDocumentId, targetDocumentIds,
            fieldName, parameters);
    }

    /**
     *
     * Terminate asset owner custodian for a selected asset.
     *
     * @param assetType Asset type.
     * @param assetId Asset id.
     * @param dateDisposed Date disposed.
     */
    private void terminateAssetOwnerCustodian(final AssetType assetType, final String assetId,
            final Date dateDisposed) {
        final TeamMember ownerCustodian = this.teamDao.getOwner(assetId, assetType);
        if (StringUtil.notNullOrEmpty(ownerCustodian)) {
            terminateCustodian(ownerCustodian, dateDisposed);
        }
    }

    /**
     *
     * Terminate asset other active custodians for a selected asset.
     *
     * @param assetType Asset type.
     * @param assetId Asset id.
     * @param dateDisposed Date disposed.
     */
    private void terminateAssetOtherActiveCustodians(final AssetType assetType,
            final String assetId, final Date dateDisposed) {
        final Restriction restriction = Restrictions.and(
            Restrictions.eq(DbConstants.TEAM_TABLE, DbConstants.IS_OWNER, 0),
            Restrictions.eq(DbConstants.TEAM_TABLE, DbConstants.STATUS, DbConstants.STATUS_ACTIVE));
        final List<TeamMember> otherActiveCustodians =
                this.teamDao.getTeamMembers(assetId, assetType, restriction);
        for (final TeamMember custodian : otherActiveCustodians) {
            if (StringUtil.notNullOrEmpty(custodian)) {
                terminateCustodian(custodian, dateDisposed);
            }
        }
    }

    /**
     *
     * Terminate asset custodian.
     *
     * @param custodian Custodian.
     * @param dateDisposed Date disposed.
     */
    private void terminateCustodian(final TeamMember custodian, final Date dateDisposed) {
        custodian.setDateEnd(dateDisposed);
        final String lastStatus = custodian.getStatus();
        custodian.setStatus(DbConstants.STATUS_INACTIVE);
        String notes =
                StringUtil.notNullOrEmpty(custodian.getNotes()) ? custodian.getNotes() + " - " : "";
        notes += MessageFormat.format(
            AssetManagementHelper.getLocalizedMessage(CUSTODIAN_TERMINATE_ON_DATE,
                this.getClass().getName()),
            SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT, ContextStore.get().getLocale())
                .format(dateDisposed));
        custodian.setNotes(notes);
        this.teamDao.update(custodian);
        if (custodian.isAssetOwner()) {
            terminateOwnerCustodian(custodian, lastStatus);
        }
        this.status.addProperty("terminateAssetCustodian", "true");
    }

    /**
     * Change owner for specified asset and insert a record in asset transaction.
     *
     * @param owner new owner record
     * @param asset asset record
     * @return team autonumber id
     */
    private int changeOwnerForAsset(final DataRecord owner, final DataRecord asset) {
        final String assetIdFieldName =
                DbConstants.getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_ID);
        final String assetTypeFieldName =
                DbConstants.getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_TYPE);

        final String assetId = asset.getString(assetIdFieldName);
        final AssetType assetType = AssetType.fromString(asset.getString(assetTypeFieldName));

        final TeamMember newOwner = this.teamDao.createNewMember(owner, assetId, assetType);
        final TeamMember currentOwner = this.teamDao.getOwner(assetId, assetType);

        String lastOwnerName = null;
        if (StringUtil.notNullOrEmpty(currentOwner)) {
            // terminate current owner
            final Date dateEnd =
                    AssetManagementHelper.dateAdd(newOwner.getDateStart(), Calendar.DATE, -1);
            final String status = currentOwner.getStatus();
            currentOwner.setDateEnd(dateEnd);
            currentOwner.setStatus(DbConstants.STATUS_INACTIVE);
            lastOwnerName = currentOwner.getTeamMemberName();

            final AssetTransaction lastOwnerTransaction =
                    AssetManagementHelper.getTerminateCustodianTransaction(
                        this.assetTransactionDao.createNewTransaction(), currentOwner, status);
            this.teamDao.update(currentOwner);
            this.assetTransactionDao.save(lastOwnerTransaction);
        }
        final AssetTransaction newOwnerTransaction =
                AssetManagementHelper.getAddOwnerCustodianTransaction(
                    this.assetTransactionDao.createNewTransaction(), newOwner, lastOwnerName);
        int autonumberedId = newOwner.getAutonumberedId();
        // check custodian has record with unassigned assets
        if (autonumberedId > 0) {
            this.teamDao.update(newOwner);
        } else {
            autonumberedId = this.teamDao.save(newOwner).getAutonumberedId();
        }
        this.assetTransactionDao.save(newOwnerTransaction);
        return autonumberedId;
    }

    /**
     * Add custodian to selected asset.
     *
     * @param custodianRecord new custodian record
     * @param assetRecord asset record
     */
    private void addCustodianToAsset(final DataRecord custodianRecord,
            final DataRecord assetRecord) {
        final String assetIdFieldName =
                DbConstants.getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_ID);
        final String assetTypeFieldName =
                DbConstants.getFullFieldName(DbConstants.BUILDING_TABLE, DbConstants.ASSET_TYPE);

        final String assetId = assetRecord.getString(assetIdFieldName);
        final AssetType assetType = AssetType.fromString(assetRecord.getString(assetTypeFieldName));
        final TeamMember custodian =
                this.teamDao.createNewMember(custodianRecord, assetId, assetType);
        final int autonumberedId = custodian.getAutonumberedId();
        // check custodian has record with unassigned assets
        if (autonumberedId > 0) {
            this.teamDao.update(custodian);
        } else {
            this.teamDao.save(custodian);
        }
    }

    /**
     * Initializes per-request state variables.
     */
    private void initPerRequestState() {
        if (this.teamDao == null) {
            this.teamDao = new TeamDataSource();
        }

        if (this.assetTransactionDao == null) {
            this.assetTransactionDao = new AssetTransactionDataSource();
        }
    }
}