package com.archibus.eventhandler.eam.domain;

import java.util.Date;

import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for equipment. Mapped to <code>eq</code> table
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class Equipment extends AssetCommon {
	/**
	 * Field eq_id.
	 */
	private String equipmentId;

	/**
	 * Field value_salvage.
	 */
	private double valueSalvage;

	/**
	 * Field date_of_stat_chg.
	 */
	private Date dateOfStatusChanged;

	/**
	 * Default constructor.
	 */
	public Equipment() {
		super(AssetType.EQUIPMENT);
	}

	/**
	 * Equipment constructor.
	 *
	 * @param equipmentId
	 *            Equipment id.
	 */
	public Equipment(final String equipmentId) {
		super(AssetType.EQUIPMENT);
		this.equipmentId = equipmentId;
	}

	/**
	 * Getter for the equipmentId property.
	 *
	 * @return the equipmentId property.
	 */
	public String getEquipmentId() {
		return this.equipmentId;
	}

	/**
	 * Setter for the equipmentId property.
	 *
	 * @param equipmentId
	 *            the equipmentId to set.
	 */
	public void setEquipmentId(final String equipmentId) {
		this.equipmentId = equipmentId;
	}

	/**
	 * Getter for the valueSalvage property.
	 *
	 * @return the valueSalvage property.
	 */
	public double getValueSalvage() {
		return this.valueSalvage;
	}

	/**
	 * Setter for the valueSalvage property.
	 *
	 * @param valueSalvage
	 *            the valueSalvage to set.
	 */
	public void setValueSalvage(final double valueSalvage) {
		this.valueSalvage = valueSalvage;
	}

	/**
	 * Getter for the dateOfStatusChanged property.
	 *
	 * @return the dateOfStatusChanged property.
	 */
	public Date getDateOfStatusChanged() {
		return this.dateOfStatusChanged;
	}

	/**
	 * Setter for the dateOfStatusChanged property.
	 *
	 * @param dateOfStatusChanged
	 *            the dateOfStatusChanged to set.
	 */
	public void setDateOfStatusChanged(final Date dateOfStatusChanged) {
		this.dateOfStatusChanged = dateOfStatusChanged;
	}
}