package com.archibus.eventhandler.eam.domain;

import com.archibus.utility.EnumTemplate;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Enumeration for team member source.
 * <p>
 *
 *
 * @author draghici
 * @since 23.2
 *
 */
public enum TeamMemberSource {
	/**
	 * Member source.
	 */
	VENDOR, CONTACT, EMPLOYEE;
	/**
	 * Member source definition.
	 */
	private static final Object[][] STRINGS_TO_ENUMS = { { "vn", VENDOR }, { "contact", CONTACT }, { "em", EMPLOYEE } };

	/**
	 * Member source database field name definition.
	 */
	private static final Object[][] FIELDS_TO_ENUMS = { { "vn_id", VENDOR }, { "contact_id", CONTACT },
			{ "em_id", EMPLOYEE } };

	/**
	 *
	 * Convert from string.
	 *
	 * @param source
	 *            string value
	 * @return vat type
	 */
	public static TeamMemberSource fromString(final String source) {
		return (TeamMemberSource) EnumTemplate.fromString(source, STRINGS_TO_ENUMS, TeamMemberSource.class);
	}

	/**
	 * Returns database field name.
	 *
	 * @return String
	 */
	public String getFieldName() {
		return EnumTemplate.toString(FIELDS_TO_ENUMS, this);
	}

	@Override
	public String toString() {
		return EnumTemplate.toString(STRINGS_TO_ENUMS, this);
	}
}
