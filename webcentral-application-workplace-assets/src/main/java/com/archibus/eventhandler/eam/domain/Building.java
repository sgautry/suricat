package com.archibus.eventhandler.eam.domain;

import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for building. Mapped to <code>bl</code> table
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class Building extends AssetCommon {
	/**
	 * Field bl_id.
	 */
	private String buildingId;


	/**
	 * Default constructor.
	 */
	public Building() {
		super(AssetType.BUILDING);
	}

	/**
	 * Building constructor.
	 *
	 * @param buildingId
	 *            Building id.
	 */
	public Building(final String buildingId) {
		super(AssetType.BUILDING);
		this.buildingId = buildingId;
	}


	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

}