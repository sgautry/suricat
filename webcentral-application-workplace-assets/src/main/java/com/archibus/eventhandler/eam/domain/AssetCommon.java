package com.archibus.eventhandler.eam.domain;

import java.util.Date;

import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Common domain object for assets.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class AssetCommon {
	private final AssetType assetType;
	/**
	 * Field status.
	 */
	private String status;
	/**
	 * Field disposal_type.
	 */
	private String disposalType;
	/**
	 * Field comment_disposal.
	 */
	private String commentDisposal;
	/**
	 * Field pending_action.
	 */
	private String pendingAction;
	/**
	 * Field date_disposal.
	 */
	private Date dateDisposal;

	/**
	 * Default TODO constructor specifying TODO. Private default constructor:
	 * utility class is non-instantiable.
	 *
	 */
	public AssetCommon(final AssetType assetType) {
		this.assetType = assetType;
	}

	/**
	 * Getter for the assetType property.
	 *
	 * @return the assetType property.
	 */
	public AssetType getAssetType() {
		return this.assetType;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the disposalType property.
	 *
	 * @return the disposalType property.
	 */
	public String getDisposalType() {
		return this.disposalType;
	}

	/**
	 * Setter for the disposalType property.
	 *
	 * @param disposalType
	 *            the disposalType to set.
	 */
	public void setDisposalType(final String disposalType) {
		this.disposalType = disposalType;
	}

	/**
	 * Getter for the commentDisposal property.
	 *
	 * @return the commentDisposal property.
	 */
	public String getCommentDisposal() {
		return this.commentDisposal;
	}

	/**
	 * Setter for the commentDisposal property.
	 *
	 * @param commentDisposal
	 *            the commentDisposal to set.
	 */
	public void setCommentDisposal(final String commentDisposal) {
		this.commentDisposal = commentDisposal;
	}

	/**
	 * Getter for the pendingAction property.
	 *
	 * @return the pendingAction property.
	 */
	public String getPendingAction() {
		return this.pendingAction;
	}

	/**
	 * Setter for the pendingAction property.
	 *
	 * @param pendingAction
	 *            the pendingAction to set.
	 */
	public void setPendingAction(final String pendingAction) {
		this.pendingAction = pendingAction;
	}

	/**
	 * Getter for the dateDisposal property.
	 *
	 * @return the dateDisposal property.
	 */
	public Date getDateDisposal() {
		return this.dateDisposal;
	}

	/**
	 * Setter for the dateDisposal property.
	 *
	 * @param dateDisposal
	 *            the dateDisposal to set.
	 */
	public void setDateDisposal(final Date dateDisposal) {
		this.dateDisposal = dateDisposal;
	}

}
