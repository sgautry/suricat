package com.archibus.eventhandler.eam.manage;

import com.archibus.utility.EnumTemplate;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Types of assets.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public enum AssetType {
	/**
	 * building asset.
	 */
	BUILDING,
	/**
	 * equipment asset.
	 */
	EQUIPMENT,
	/**
	 * furniture asset.
	 */
	FURNITURE,
	/**
	 * property asset.
	 */
	PROPERTY;
	/**
	 * Mapping to String values.
	 */
	private static final Object[][] STRINGS_TO_ENUMS = { { "bl", BUILDING }, { "eq", EQUIPMENT }, { "ta", FURNITURE },
			{ "property", PROPERTY } };

	/**
	 * Converts given string to enum object.
	 *
	 * @param source
	 *            string to convert to enum.
	 * @return result of conversion.
	 */
	public static AssetType fromString(final String source) {
		return (AssetType) EnumTemplate.fromString(source, STRINGS_TO_ENUMS, AssetType.class);
	}

	@Override
	public String toString() {
		return EnumTemplate.toString(STRINGS_TO_ENUMS, this);
	}
}