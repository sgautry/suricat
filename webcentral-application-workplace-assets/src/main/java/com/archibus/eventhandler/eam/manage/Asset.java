package com.archibus.eventhandler.eam.manage;

import com.archibus.eventhandler.eam.dao.IAssetDao;
import com.archibus.eventhandler.eam.dao.datasource.*;
import com.archibus.eventhandler.eam.domain.AssetCommon;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * <p>
 * Provides DAO for asset object.
 *
 * @author Radu Bunea
 * @since 24.1
 *
 */
public class Asset {

	/**
	 * Asset type.
	 */
	private final AssetType assetType;

	/**
	 * References to custom data sources used to load asset objects from the
	 * database. These references are set by the Web Central container based on
	 * the Spring configuration file
	 * com/archibus/eventhandler/eam/appContext-services.xml
	 */
	private final IAssetDao<AssetCommon> assetDao;

	/**
	 * Default Asset constructor.
	 *
	 * @param assetType
	 *            Asset type.
	 */
	public Asset(final AssetType assetType) {
		super();
		this.assetType = assetType;
		switch (this.assetType) {
		case BUILDING:
			this.assetDao = new BuildingDataSource();
			break;
		case FURNITURE:
			this.assetDao = new FurnitureDataSource();
			break;
		case PROPERTY:
			this.assetDao = new PropertyDataSource();
			break;
		default:
			this.assetDao = new EquipmentDataSource();
			break;
		}
	}

	/**
	 *
	 * Returns asset domain object.
	 *
	 * @param assetId
	 *            Asset id.
	 * @return asset
	 */
	public AssetCommon getAsset(final String assetId) {
		return this.assetDao.get(assetId);
	}

	/**
	 * Get DAO for asset object.
	 *
	 * @return DAO asset object.
	 */
	public IAssetDao<AssetCommon> getDao() {
		return this.assetDao;
	};

	/**
	 * Getter for the assetType property.
	 *
	 * @return the assetType property.
	 */
	public AssetType getAssetType() {
		return this.assetType;
	}
}