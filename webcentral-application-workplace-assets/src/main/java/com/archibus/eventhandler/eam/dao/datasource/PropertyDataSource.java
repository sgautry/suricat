package com.archibus.eventhandler.eam.dao.datasource;

import org.apache.commons.lang.ArrayUtils;

import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.AssetCommon;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for property table.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class PropertyDataSource extends AbstractAssetDataSource<AssetCommon> {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { DbConstants.PR_ID, "propertyId" } };

	/**
	 * Constructs EquipmentDataSource, mapped to <code>property</code> table,
	 * using <code>property</code> bean.
	 */
	public PropertyDataSource() {
		super("assetProperty", DbConstants.PROPERTY_TABLE);
	}

	/**
	 * Merge fieldsToProperties from the base class.
	 *
	 * @return fieldsToPropertiesMerged
	 */
	@Override
	protected String[][] getFieldsToProperties() {
		final String[][] fieldsToPropertiesMerged = (String[][]) ArrayUtils.addAll(super.getFieldsToProperties(),
				FIELDS_TO_PROPERTIES);
		return fieldsToPropertiesMerged;
	}
}