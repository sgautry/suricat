package com.archibus.eventhandler.eam.dao;

import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for ownership transaction object.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 * @param <OwnershipTransaction>
 *            ownership transaction object
 */
public interface IOwnershipTransactionDao<OwnershipTransaction> extends IDao<OwnershipTransaction> {
	/**
	 * Create new asset transaction object.
	 *
	 * @return {@link com.archibus.eventhandler.eam.domain.OwnershipTransaction}
	 */
	OwnershipTransaction createNewTransaction();
}
