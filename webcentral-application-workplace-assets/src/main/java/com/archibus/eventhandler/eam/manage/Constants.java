package com.archibus.eventhandler.eam.manage;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Constants for Asset management.
 * <p>
 *
 *
 * @author draghici
 * @since 23.2
 *
 */
public final class Constants {

	/**
	 * Constant.
	 */
	public static final String ASSET_TYPE_KEY = "asset_type";

	/**
	 * Constant.
	 */
	public static final String ASSET_ID_KEY = "asset_id";

	/**
	 * Constant.
	 */
	public static final String BL_TYPE_RESTRICTION_KEY = "blTypeRestriction";

	/**
	 * Constant.
	 */
	public static final String PROPERTY_TYPE_RESTRICTION_KEY = "propertyTypeRestriction";

	/**
	 * Constant.
	 */
	public static final String EQ_TYPE_RESTRICTION_KEY = "eqTypeRestriction";

	/**
	 * Constant.
	 */
	public static final String TA_TYPE_RESTRICTION_KEY = "taTypeRestriction";

	/**
	 * Constant.
	 */
	public static final String SQL_TYPE_RESTRICTION_KEY = "sqlTypeRestriction";

	/**
	 * Constant.
	 */
	public static final String DATA_SOURCE_PARAMETER_DEFAULT = " 1 = 1 ";

	/**
	 * Constant.
	 */
	public static final String OR_OPERATOR = " OR ";

	/**
	 * Constant.
	 */
	public static final String VALID_KEY = "valid";

	/**
	 *
	 * Private default constructor: utility class is non-instantiable.
	 */
	private Constants() {

	}
}
