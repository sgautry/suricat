package com.archibus.eventhandler.eam.dao.datasource;

import org.apache.commons.lang.ArrayUtils;

import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.AssetCommon;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for furniture table.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class FurnitureDataSource extends AbstractAssetDataSource<AssetCommon> {
	/**
	 * Field names to furniture names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { DbConstants.TA_ID, "furnitureId" },
			{ "value_salvage", "valueSalvage" } };

	/**
	 * Constructs FurnitureDataSource, mapped to <code>ta</code> table, using
	 * <code>furniture</code> bean.
	 *
	 */
	public FurnitureDataSource() {
		super("assetFurniture", DbConstants.FURNITURE_TABLE);
	}

	/**
	 * Merge fieldsToProperties from the base class.
	 *
	 * @return fieldsToPropertiesMerged
	 */
	@Override
	protected String[][] getFieldsToProperties() {
		final String[][] fieldsToPropertiesMerged = (String[][]) ArrayUtils.addAll(super.getFieldsToProperties(),
				FIELDS_TO_PROPERTIES);
		return fieldsToPropertiesMerged;
	}
}