package com.archibus.eventhandler.eam.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or
 * Represents TODO. - if it has state (entity, domain object, model object).
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class OwnershipTransaction {
	/**
	 * Asset transaction id; field ot_id.
	 */
	private int transactionId;
	/**
	 * Field bl_id.
	 */
	private String buildingId;
	/**
	 * Field pr_id.
	 */
	private String propertyId;
	/**
	 * Field status.
	 */
	private String status;
	/**
	 * Field comments.
	 */
	private String comments;
	/**
	 * Field description.
	 */
	private String description;
	/**
	 * Field cost_selling.
	 */
	private double costSelling;
	/**
	 * Field date_sold.
	 */
	private Date dateSold;

	/**
	 * Getter for the transactionId property.
	 *
	 * @return the transactionId property.
	 */
	public int getTransactionId() {
		return this.transactionId;
	}

	/**
	 * Setter for the transactionId property.
	 *
	 * @param transactionId
	 *            the transactionId to set.
	 */
	public void setTransactionId(final int transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the propertyId property.
	 *
	 * @return the propertyId property.
	 */
	public String getPropertyId() {
		return this.propertyId;
	}

	/**
	 * Setter for the propertyId property.
	 *
	 * @param propertyId
	 *            the propertyId to set.
	 */
	public void setPropertyId(final String propertyId) {
		this.propertyId = propertyId;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the comments property.
	 *
	 * @return the comments property.
	 */
	public String getComments() {
		return this.comments;
	}

	/**
	 * Setter for the comments property.
	 *
	 * @param comments
	 *            the comments to set.
	 */
	public void setComments(final String comments) {
		this.comments = comments;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the costSelling property.
	 *
	 * @return the costSelling property.
	 */
	public double getCostSelling() {
		return this.costSelling;
	}

	/**
	 * Setter for the costSelling property.
	 *
	 * @param costSelling
	 *            the costSelling to set.
	 */
	public void setCostSelling(final double costSelling) {
		this.costSelling = costSelling;
	}

	/**
	 * Getter for the dateSold property.
	 *
	 * @return the dateSold property.
	 */
	public Date getDateSold() {
		return this.dateSold;
	}

	/**
	 * Setter for the dateSold property.
	 *
	 * @param dateSold
	 *            the dateSold to set.
	 */
	public void setDateSold(final Date dateSold) {
		this.dateSold = dateSold;
	}
}