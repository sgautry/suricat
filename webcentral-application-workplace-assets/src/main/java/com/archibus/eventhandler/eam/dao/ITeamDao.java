package com.archibus.eventhandler.eam.dao;

import java.util.List;

import com.archibus.core.dao.IDao;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.eventhandler.eam.manage.AssetType;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides methods to load team records.
 * <p>
 *
 *
 * @author draghici
 * @since 23.2
 *
 * @param <TeamMember>
 *            type of object
 */
public interface ITeamDao<TeamMember> extends IDao<TeamMember> {

	/**
	 * Returns current owner for given asset.
	 *
	 * @param assetId
	 *            asset code
	 * @param assetType
	 *            asset type
	 * @return {@link com.archibus.eventhandler.eam.domain.TeamMember}
	 */
	TeamMember getOwner(final String assetId, final AssetType assetType);

	/**
	 * Returns owner data record for asset.
	 *
	 * @param assetId
	 *            asset id
	 * @param assetType
	 *            asset type
	 * @return {@link DataRecord}
	 */
	DataRecord getOwnerRecord(final String assetId, final AssetType assetType);

	/**
	 * Returns last owner for given asset.
	 *
	 * @param assetId
	 *            asset code
	 * @param assetType
	 *            asset type
	 * @return {@link com.archibus.eventhandler.eam.domain.TeamMember}
	 */
	TeamMember getLastOwner(final String assetId, final AssetType assetType);

	/**
	 * Returns a list of team members for given asset.
	 *
	 * @param assetId
	 *            asset code
	 * @param assetType
	 *            asset type
	 * @param restriction
	 *            restriction to apply
	 * @return List<{@link com.archibus.eventhandler.eam.domain.TeamMember}>
	 */
	List<TeamMember> getTeamMembers(final String assetId, final AssetType assetType, final Restriction restriction);

	/**
	 * Returns team member name by id.
	 *
	 * @param autonumberedId
	 *            autonumbered id
	 * @return {@link com.archibus.eventhandler.eam.domain.TeamMember}
	 */
	TeamMember getTeamMemberById(final int autonumberedId);

	/**
	 * Create new team member object.
	 *
	 * @param teamRecord
	 *            team form data record
	 * @param assetId
	 *            asset id
	 * @param assetType
	 *            asset type
	 * @return {@link com.archibus.eventhandler.eam.domain.TeamMember}
	 */
	TeamMember createNewMember(final DataRecord teamRecord, final String assetId, final AssetType assetType);

}
