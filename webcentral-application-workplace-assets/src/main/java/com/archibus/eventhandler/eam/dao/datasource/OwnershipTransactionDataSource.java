package com.archibus.eventhandler.eam.dao.datasource;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.eventhandler.eam.dao.IOwnershipTransactionDao;
import com.archibus.eventhandler.eam.datachangeevent.DbConstants;
import com.archibus.eventhandler.eam.domain.OwnershipTransaction;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for ownership transactions table.
 * <p>
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class OwnershipTransactionDataSource extends ObjectDataSourceImpl<OwnershipTransaction>
		implements IOwnershipTransactionDao<OwnershipTransaction> {
	/**
	 * Field names to ot names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "ot_id", "transactionId" },
			{ DbConstants.BL_ID, "buildingId" }, { DbConstants.PR_ID, "propertyId" }, { DbConstants.STATUS, "status" },
			{ DbConstants.COMMENTS, "comments" }, { DbConstants.DESCRIPTION, "description" },
			{ DbConstants.DATE_SOLD, "dateSold" }, { "cost_selling", "costSelling" } };

	/**
	 * Constructs OwnershipTransactionDataSource, mapped to <code>ot</code>
	 * table, using <code>ownershipTransaction</code> bean.
	 */
	public OwnershipTransactionDataSource() {
		super("ownershipTransaction", "ot");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OwnershipTransaction createNewTransaction() {
		final DataSource dataSource = this.createCopy();
		final DataRecord record = dataSource.createNewRecord();
		return new DataSourceObjectConverter<OwnershipTransaction>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
