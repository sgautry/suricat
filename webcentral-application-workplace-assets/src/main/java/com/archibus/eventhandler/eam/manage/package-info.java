/**
 * This package contains new java classes for 23.2 Enterprise Asset management -
 * Chain of Custody.
 *
 * @since 23.2
 *
 */
package com.archibus.eventhandler.eam.manage;