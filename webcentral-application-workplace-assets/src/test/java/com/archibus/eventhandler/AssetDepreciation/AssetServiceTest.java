package com.archibus.eventhandler.AssetDepreciation;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;

@Category({ DatabaseTest.class })
public class AssetServiceTest extends DataSourceTestBase {
	/*
	 * calculate equipment depreciation
	 */
	public void testCalculateEquipmentDepreciation() {
		final AssetService handler = new AssetService();
		handler.calculateEquipmentDepreciation();
	}

	/*
	 * calculate tagged furniture depreciation
	 */
	public void testCalculateTaggedFurnitureDepreciation() {
		final AssetService handler = new AssetService();
		handler.calculateTaggedFurnitureDepreciation();
	}

}
