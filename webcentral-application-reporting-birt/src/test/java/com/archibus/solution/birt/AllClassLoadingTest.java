package com.archibus.solution.birt;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

/**
 * Test for loading all classes in "org" package.
 *
 * @author tydykov
 * @since 23.2
 *
 */
// @Category({ SlowTest.class })
public class AllClassLoadingTest {
    @Test
    public void testLoadAllClasses() throws ClassNotFoundException {
        final ScanResult scanResult = new FastClasspathScanner("org").scan();

        final List<String> namesOfclasses =
                scanResult.getNamesOfSubclassesOf(Object.class.getName());
        assertTrue(!namesOfclasses.isEmpty());

        for (final String name : namesOfclasses) {
            try {
                Class.forName(name);
            } catch (final NoClassDefFoundError e) {
                // ignore
            } catch (final ClassNotFoundException e) {
                // ignore
            } catch (final UnsatisfiedLinkError e) {
                // ignore
            } catch (final SecurityException e) {
                fail(e.getCause().getMessage());
            } catch (final Exception e) {
                // report exceptions
                System.err.println(e);
            } catch (final VerifyError e) {
                fail(e.getCause().getMessage());
            } catch (final ExceptionInInitializerError e) {
                // ignore
            } catch (final UnsupportedClassVersionError e) {
                // ignore
            } catch (final IllegalAccessError e) {
                // ignore
            } catch (final AssertionError e) {
                // ignore
            }
        }
    }
}
