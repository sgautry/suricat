package com.archibus.solution.birt;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.FastTest;

/**
 * Test for loading DeferredFileOutputStream class.
 *
 * @author tydykov
 * @since 23.2
 *
 */
@Category({ FastTest.class })
public class ClassLoadingTest {
    @Test
    public void testLoadClassDeferredFileOutputStream() throws ClassNotFoundException {
        // Test that DeferredFileOutputStream class can be loaded without SecurityException caused
        // by corrupted BATIK 1.6 JAR.
        Class.forName("org.apache.commons.io.output.DeferredFileOutputStream");
    }
}
