package com.archibus.solution.birt.service.impl;

/**
 * Provides supported report file formats.
 *
 * @author tydykov
 * @author nemtsev
 * @since 23.2
 *
 */
public enum ReportFormat {
    /**
     * Supported report file formats.
     */
    PDF("pdf"), HTML("html"), MS_DOC("docx"), MS_EXCEL("xls");

    /**
     * Property: file extension.
     */
    private final String extension;

    /**
     * Constructor.
     *
     * @param extension file extension.
     */
    ReportFormat(final String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return this.extension;
    }

    /**
     * Getter for the extension property.
     *
     * @return the extension property.
     */
    public String getExtension() {
        return this.extension;
    }
}
