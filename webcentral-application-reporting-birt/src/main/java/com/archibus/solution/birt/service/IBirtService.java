package com.archibus.solution.birt.service;

import org.json.JSONObject;

/**
 * Provides TODO. - if it has behavior (service), or Represents TODO. - if it has state (entity,
 * domain object, model object). Utility class. Provides methods TODO. Interface to be implemented
 * by classes that TODO.
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author tydykov
 * @since 23.2
 *
 */
public interface IBirtService {

    /**
     * Generates BIRT report.
     *
     * @param reportDesignFileName Name of BIRT .rptdesign file.
     * @param dataSourceParameters ARCHIBUS data source parameters.
     * @param formatName name of the output report format.
     * @return name of the generated report file.
     */
    String generateReport(String reportDesignFileName, JSONObject dataSourceParameters,
            String formatName);
}