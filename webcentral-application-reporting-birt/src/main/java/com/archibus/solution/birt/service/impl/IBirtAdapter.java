package com.archibus.solution.birt.service.impl;

import java.io.File;

import org.eclipse.birt.report.engine.api.IReportEngine;
import org.json.JSONObject;

/**
 * Wraps BIRT functionality.
 *
 * @author tydykov
 * @since 23.2
 *
 */
public interface IBirtAdapter {

    /**
     * Getter for the reportEngine property.
     *
     * @return the reportEngine property.
     */
    IReportEngine getReportEngine();

    /**
     * Setter for the reportEngine property.
     *
     * @param reportEngine the reportEngine to set.
     */
    void setReportEngine(IReportEngine reportEngine);

    /**
     * Generates BIRT report in specified format from reportDesignFile to outputFile. Applies
     * dataSourceParameters.
     *
     * @param dataSourceParameters parameters to be used by WebCentral DataSource.
     * @param format output file format.
     * @param outputFile generated file.
     * @param reportDesignFile file with report design definition.
     */
    void generateReport(JSONObject dataSourceParameters, ReportFormat format, File outputFile,
            File reportDesignFile);
}