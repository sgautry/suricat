package com.archibus.solution.birt.service.impl;

import java.io.*;

import org.eclipse.birt.report.engine.api.*;

/**
 * Utility class. Provides methods that use BIRT API.
 *
 *
 * @author tydykov
 * @since 23.2
 *
 */
public final class BirtUtilities {
    /**
     * Constant: PNG image format.
     */
    private static final String IMAGE_FORMAT_PNG = "PNG";

    /**
     * Default constructor - should never be instantiated.
     */
    private BirtUtilities() {
    }

    /**
     * Prepares IRenderOption for a report in specified format.
     *
     * @param outputFile output file.
     * @param format report file format.
     * @return prepared option.
     */
    static IRenderOption prepareOption(final File outputFile, final ReportFormat format) {
        final IRenderOption option;
        switch (format) {
            case HTML:
                option = prepareOptionForHtml();
                break;
            case MS_DOC:
                option = prepareOptionForDocx();
                break;
            case MS_EXCEL:
                option = prepareOptionForExcel();
                break;
            // PDF is the default format
            case PDF:
            default:
                option = prepareOptionForPdf();
                break;
        }

        option.setOutputStream(new ByteArrayOutputStream());
        option.setOutputFileName(outputFile.getAbsolutePath());

        return option;
    }

    /**
     * Prepares IRenderOption for a report formatted as HTML document.
     *
     * @return prepared option.
     */
    private static IRenderOption prepareOptionForHtml() {
        final HTMLRenderOption option = new HTMLRenderOption();
        option.setOutputFormat(IRenderOption.OUTPUT_FORMAT_HTML);
        option.setEmitterID(IRenderOption.OUTPUT_EMITTERID_HTML);
        option.setSupportedImageFormats(IMAGE_FORMAT_PNG);

        return option;
    }

    /**
     * Prepares IRenderOption for a report formatted as MS Docx document.
     *
     * @return prepared option.
     */
    private static IRenderOption prepareOptionForDocx() {
        final DocxRenderOption option = new DocxRenderOption();
        option.setOutputFormat("docx");
        option.setEmitterID("org.eclipse.birt.report.engine.emitter.docx");

        return option;
    }

    /**
     * Prepares IRenderOption for a report formatted as MS Excel document.
     *
     * @return prepared option.
     */
    private static IRenderOption prepareOptionForExcel() {
        final EXCELRenderOption option = new EXCELRenderOption();
        option.setOutputFormat("xls");
        option.setOfficeVersion("office2007");

        return option;
    }

    /**
     * Prepares IRenderOption for a report formatted as PDF document.
     *
     * @return prepared option.
     */
    private static IRenderOption prepareOptionForPdf() {
        final PDFRenderOption option = new PDFRenderOption();
        option.setOutputFormat(IRenderOption.OUTPUT_FORMAT_PDF);
        option.setEmitterID(IRenderOption.OUTPUT_EMITTERID_PDF);
        option.setSupportedImageFormats(IMAGE_FORMAT_PNG);

        return option;
    }
}
