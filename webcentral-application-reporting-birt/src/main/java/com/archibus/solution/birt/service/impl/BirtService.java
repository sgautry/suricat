package com.archibus.solution.birt.service.impl;

import java.io.File;

import org.json.JSONObject;

import com.archibus.solution.birt.service.IBirtService;
import com.archibus.utility.StringUtil;

/**
 * Provides methods to generate BIRT report.
 *
 * @author jmartin
 * @author dsantamaria
 * @author nemtsev
 *
 * @since 23.2
 *
 */
public class BirtService implements IBirtService {

    /**
     * Property: birtAdapter.
     */
    private IBirtAdapter birtAdapter;

    /**
     * Property: folder where BIRT report design files are located.
     */
    private String reportDesignFolder;

    /**
     * Property: userName. Name of the current user.
     */
    private String userName;

    /**
     * Property: Absolute path of folder where this Web application is installed.
     */
    private String webAppPath;

    /**
     * Getter for the birtAdapter property.
     *
     * @return the birtAdapter property.
     */
    public IBirtAdapter getBirtAdapter() {
        return this.birtAdapter;
    }

    /**
     * Getter for the reportDesignFolder property.
     *
     * @return the reportDesignFolder property.
     */
    public String getReportDesignFolder() {
        return this.reportDesignFolder;
    }

    /**
     * Getter for the userName property.
     *
     * @return the userName property.
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Getter for the webAppPath property.
     *
     * @return the webAppPath property.
     */
    public String getWebAppPath() {
        return this.webAppPath;
    }

    @Override
    public String generateReport(final String reportDesignFileName,
            final JSONObject dataSourceParameters, final String formatName) {
        final ReportFormat format = determineReportFormat(formatName);

        final String outputFileName =
                BirtFileUtilities.generateOutputFileName(reportDesignFileName, this.userName, format);
        final File outputFile = BirtFileUtilities.determineReportOutputFile(this.webAppPath,
            this.userName, outputFileName);

        final File reportDesignFile = BirtFileUtilities.determineReportDesignFile(this.webAppPath,
            reportDesignFileName, this.reportDesignFolder);

        this.birtAdapter.generateReport(dataSourceParameters, format, outputFile, reportDesignFile);

        return outputFileName;
    }

    /**
     * Setter for the birtAdapter property.
     *
     * @param birtAdapter the birtReportAdapter to set.
     */
    public void setBirtAdapter(final IBirtAdapter birtAdapter) {
        this.birtAdapter = birtAdapter;
    }

    /**
     * Setter for the reportDesignFolder property.
     *
     * @param reportDesignFolder the reportDesignFolder to set.
     */
    public void setReportDesignFolder(final String reportDesignFolder) {
        this.reportDesignFolder = reportDesignFolder;
    }

    /**
     * Setter for the userName property.
     *
     * @param userName the userName to set.
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Setter for the webAppPath property.
     *
     * @param webAppPath the webAppPath to set.
     */
    public void setWebAppPath(final String webAppPath) {
        this.webAppPath = webAppPath;
    }

    /**
     * Determines output file format based on the format name. If format name is not specified, PDF
     * format will be used.
     *
     * @param formatName name of the format.
     * @return ReportOutputFormat instance.
     */
    static ReportFormat determineReportFormat(final String formatName) {
        ReportFormat format = ReportFormat.PDF;
        if (StringUtil.notNullOrEmpty(formatName)) {
            format = ReportFormat.valueOf(formatName);
        }

        return format;
    }
}
