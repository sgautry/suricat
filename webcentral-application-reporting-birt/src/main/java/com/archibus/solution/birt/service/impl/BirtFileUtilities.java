package com.archibus.solution.birt.service.impl;

import java.io.File;
import java.text.*;
import java.util.*;

import org.apache.commons.lang.RandomStringUtils;

/**
 * Utility class. Provides methods that manipulate files and file names.
 *
 *
 * @author tydykov
 * @since 23.2
 *
 */
public final class BirtFileUtilities {
    /**
     * Constant: dash character.
     */
    private static final String DASH = "-";

    /**
     * Constant: path to the folder where report file will be generated.
     */
    private static final String OUTPUT_FILENAME_FOLDER_PREFIX = "projects/users";

    /**
     * Constant: number of random characters in the generated filename.
     */
    private static final int RANDOM_CHARS = 32;

    /**
     * Constant: default report folder location.
     */
    private static final String REPORT_FOLDER = "projects/reports";

    /**
     * Constant: part of the parameter value that represents a WebApp path and needs to be resolved
     * at runtime.
     */
    private static final String REPORT_LOCATION_SUBSTITUTE_PART = "/#Attribute%//@webAppDirectory%";

    /**
     * Constant: report file prefix.
     */
    private static final String RPT_FILE_PREFIX = "birt-rpt-";

    /**
     * Default constructor - should never be instantiated.
     */
    private BirtFileUtilities() {
    }

    /**
     * Determines file with report design. If report location is specified as application parameter,
     * uses it. Otherwise uses webAppPath and "projects/reports" folder.
     *
     * @param webAppPath absolute path of folder where this Web application is installed.
     * @param reportDesignFileName Name of BIRT .rptdesign file.
     * @param reportDesignFolder folder where BIRT report design files are located.
     * @return created File.
     */
    static File determineReportDesignFile(final String webAppPath,
            final String reportDesignFileName, final String reportDesignFolder) {
        final File reportFile;
        if (reportDesignFolder.indexOf(REPORT_LOCATION_SUBSTITUTE_PART) >= 0) {
            // report location is specified as application parameter
            final String reportLocation =
                    reportDesignFolder.replace(REPORT_LOCATION_SUBSTITUTE_PART, webAppPath);

            reportFile = new File(new File(reportLocation), reportDesignFileName);
        } else {
            // report location is NOT specified as application parameter
            reportFile =
                    new File(new File(new File(webAppPath), REPORT_FOLDER), reportDesignFileName);
        }

        return reportFile;
    }

    /**
     * Determines report output file based on webAppPath, userName, reportOutputFileName.
     *
     * @param webAppPath absolute path of folder where this Web application is installed.
     * @param userName name of user.
     * @param reportOutputFileName name of file with generated report.
     * @return report output file.
     */
    static File determineReportOutputFile(final String webAppPath, final String userName,
            final String reportOutputFileName) {
        return new File(new File(new File(new File(webAppPath), OUTPUT_FILENAME_FOLDER_PREFIX),
            userName.toLowerCase()), reportOutputFileName);
    }

    /**
     * Generates file name for the reportDesignFileName. Uses random characters in the file name to
     * obfuscate the file.
     *
     * @param reportDesignFileName Name of BIRT .rptdesign file.
     * @param userName name of user.
     * @param format output file format.
     * @return generated file name in lower case.
     */
    static String generateOutputFileName(final String reportDesignFileName, final String userName,
            final ReportFormat format) {
        final String reportId = reportDesignFileName.replace(".rptdesign", "");
        String reportOutputFileName = RPT_FILE_PREFIX;

        reportOutputFileName =
                reportOutputFileName.concat(reportId).concat(DASH).concat(userName).concat(DASH);

        final DateFormat dateFormat = new SimpleDateFormat("MMddyyyy_HHmmssSSS", Locale.ENGLISH);
        reportOutputFileName = reportOutputFileName.concat(dateFormat.format(new Date()));

        final String random = RandomStringUtils.randomAlphanumeric(RANDOM_CHARS);
        reportOutputFileName = reportOutputFileName.concat(DASH);
        reportOutputFileName = reportOutputFileName.concat(random);
        reportOutputFileName = reportOutputFileName.concat(".");
        reportOutputFileName = reportOutputFileName.concat(format.getExtension());

        return reportOutputFileName.toLowerCase();
    }
}
