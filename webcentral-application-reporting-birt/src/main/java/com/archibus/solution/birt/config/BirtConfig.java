package com.archibus.solution.birt.config;

import javax.annotation.PreDestroy;

import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

import com.archibus.config.*;
import com.archibus.security.UserAccount;
import com.archibus.solution.birt.service.IBirtService;
import com.archibus.solution.birt.service.impl.*;
import com.archibus.utility.ExceptionBase;

/**
 * Provides Spring configuration for integration with BIRT.
 * <p>
 *
 * @author tydykov
 * @author nemtsev
 * @since 23.2
 *
 */
@Configuration
public class BirtConfig {
    /**
     * Constant: name of parameter that specifies location of folder with report design files.
     */
    private static final String REPORT_LOCATION_PARAMETER = "AbCommonResources-birtReportLocation";

    /**
     * Property: Spring applicationContext.
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * Property: configManager.
     */
    @Autowired
    private ConfigManager.Immutable configManager;

    /**
     * Property: project.
     */
    @Autowired
    private Project.Immutable project;

    /**
     * Property: userAccount.
     */
    @Autowired
    private UserAccount.Immutable userAccount;

    /**
     * Creates birtAdapter bean.
     *
     * @return created bean.
     */
    @Bean
    public IBirtAdapter birtAdapter() {
        final IBirtAdapter birtAdapter = new BirtAdapter();
        birtAdapter.setReportEngine(reportEngine());

        return birtAdapter;
    }

    /**
     * Creates BirtService bean.
     * <p>
     * This bean is registered as WFR, so the been ID must match WFR name "BirtService".
     *
     * @return created bean.
     */
    @Scope("prototype")
    @Bean
    @edu.umd.cs.findbugs.annotations.SuppressWarnings("NM_METHOD_NAMING_CONVENTION")
    // CHECKSTYLE:OFF : Justification: been ID must match WFR name "BirtService".
    public IBirtService BirtService() {
        // CHECKSTYLE:ON
        final BirtService birtService = new BirtService();
        birtService.setUserName(this.userAccount.getName());
        birtService.setWebAppPath(this.configManager.getWebAppPath());
        birtService.setBirtAdapter(birtAdapter());
        birtService.setReportDesignFolder(this.project.getActivityParameterManager()
            .getParameterValue(REPORT_LOCATION_PARAMETER));

        return birtService;
    }

    /**
     * Destroys reportEngine bean.
     */
    @PreDestroy
    public void destroy() {
        try {
            reportEngine().destroy();
            Platform.shutdown();
            // CHECKSTYLE:OFF : Justification: BIRT API throws Throwable.
        } catch (final Throwable e) {
            // CHECKSTYLE:ON
            // @non-translatable
            throw new ExceptionBase("Could not stop the BIRT Reporting engine!", e);
        }
    }

    /**
     * Creates reportEngine bean.
     *
     * @return created bean.
     */
    @Bean
    // Justification: BIRT API uses raw type HashMap.
    @SuppressWarnings({ "unchecked" })
    public IReportEngine reportEngine() {
        final EngineConfig config = new EngineConfig();

        // Inject the Spring Context into the BIRT Context
        config.getAppContext().put("spring", this.applicationContext);

        try {
            Platform.startup(config);
        } catch (final BirtException e) {
            // @non-translatable
            throw new ExceptionBase("Could not start the BIRT Reporting engine!", e);
        }

        final IReportEngineFactory factory = (IReportEngineFactory) Platform
            .createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);

        return factory.createReportEngine(config);
    }
}
