package com.archibus.solution.birt.service.impl;

import java.io.File;

import org.eclipse.birt.report.engine.api.*;
import org.json.JSONObject;

import com.archibus.utility.ExceptionBase;

/**
 * Wraps BIRT functionality. Maps BIRT exceptions to ExceptionBase.
 *
 * @author jmartin
 * @author dsantamaria
 * @author tydykov
 * @author nemtsev
 *
 * @since 23.2
 *
 */
public class BirtAdapter implements IBirtAdapter {

    /**
     * Constant: data source parameter key.
     */
    private static final String DATA_SOURCE_PARAMETERS_KEY = "dataSourceParameters";

    /**
     * Constant: Error message.
     */
    // @non-translatable
    private static final String ERROR_GENERATING_REPORT = "Error generating the report.";

    /**
     * Property: BIRT reportEngine.
     */
    private IReportEngine reportEngine;

    /**
     * Getter for the reportEngine property.
     *
     * @return the reportEngine property.
     */
    @Override
    public IReportEngine getReportEngine() {
        return this.reportEngine;
    }

    /**
     * Setter for the reportEngine property.
     *
     * @param reportEngine the reportEngine to set.
     */
    @Override
    public void setReportEngine(final IReportEngine reportEngine) {
        this.reportEngine = reportEngine;
    }

    @Override
    // Justification: BIRT uses raw types.
    @SuppressWarnings("unchecked")
    public void generateReport(final JSONObject dataSourceParameters, final ReportFormat format,
            final File outputFile, final File reportDesignFile) {
        try {
            final IReportRunnable runnable =
                    this.reportEngine.openReportDesign(reportDesignFile.getAbsolutePath());

            final IRunAndRenderTask runAndRenderTask =
                    this.reportEngine.createRunAndRenderTask(runnable);

            // Store the data source parameters in the report context so that they can
            // be accessed by the report scripted data set.
            runAndRenderTask.getAppContext().put(DATA_SOURCE_PARAMETERS_KEY, dataSourceParameters);
            runAndRenderTask.getAppContext().put(EngineConstants.APPCONTEXT_CLASSLOADER_KEY,
                Thread.currentThread().getContextClassLoader());

            runAndRenderTask.setRenderOption(BirtUtilities.prepareOption(outputFile, format));

            runAndRenderTask.run();
        } catch (final EngineException e) {
            throw new ExceptionBase(ERROR_GENERATING_REPORT, e);
        }
    }
}
