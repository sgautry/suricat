package com.archibus.solution.birt.service.impl;

import java.util.*;
import java.util.Map.Entry;

import com.archibus.datasource.*;
import com.archibus.datasource.data.*;

/**
 * Provides iterator over WebCentral data source.
 *
 * @author jmartin
 * @author tydykov
 * @author nemtsev
 * @since 23.2
 *
 */
public class BirtIterableDataSource extends DataSourceImpl
        implements Iterable<Map<String, Object>> {

    /**
     * Property: data source parameters applied to the data source used to instantiate the class.
     */
    private final Map<String, Parameter> dataSourceParameters;

    /**
     * Provides iterator over WebCentral data source.
     */
    private static class DataRecordIterator implements Iterator<Map<String, Object>> {

        /**
         * Property: the data source being iterated over.
         */
        private final DataSource dataSource;

        /**
         * Property: the records returned by the last call to getRecords().
         */
        private List<DataRecord> buffer;

        /**
         * Creates an iterator for the given data source.
         *
         * @param dataSource the data source to be iterated over for records.
         */
        DataRecordIterator(final DataSource dataSource) {
            this.dataSource = dataSource;
            this.buffer = this.dataSource.getRecords();
        }

        @Override
        public boolean hasNext() {
            return !this.buffer.isEmpty() || this.dataSource.hasMoreRecords();
        }

        @Override
        public Map<String, Object> next() {
            if (this.buffer.isEmpty()) {
                if (this.dataSource.hasMoreRecords()) {
                    this.buffer = this.dataSource.getRecords();
                } else {
                    throw new NoSuchElementException("DataSource has no more records.");
                }
            }

            final Map<String, Object> fields = new HashMap<String, Object>();
            for (final Entry<String, DataValue> field : this.buffer.remove(0).getFieldsByName()
                .entrySet()) {
                final DataValue value = field.getValue();
                fields.put(field.getKey().split("\\.")[1], value == null ? null : value.getValue());
            }

            return fields;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException(
                "DataSource records may only ever be read once.");
        }
    }

    /**
     * Constructs the BirtIterableDataSource.
     *
     * @param anotherDataSource the DataSource to create the iterator on.
     */
    public BirtIterableDataSource(final DataSourceImpl anotherDataSource) {
        super(anotherDataSource);
        // Save the applied parameters. The datasource parameters are not persisted through
        // the copy operation.
        this.dataSourceParameters = anotherDataSource.getParameters();
    }

    @Override
    public Iterator<Map<String, Object>> iterator() {
        final DataSource copiedDataSource = this.createCopy();

        // Apply the datasource parameters
        for (final Map.Entry<String, Parameter> entry : this.dataSourceParameters.entrySet()) {
            final String key = entry.getKey();
            final Parameter value = entry.getValue();
            copiedDataSource.setParameter(key, value.getValue());
        }

        return new DataRecordIterator(copiedDataSource);
    }
}
