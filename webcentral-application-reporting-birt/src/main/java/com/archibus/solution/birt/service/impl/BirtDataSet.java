package com.archibus.solution.birt.service.impl;

import java.io.Serializable;
import java.util.*;

import org.json.JSONObject;

import com.archibus.datasource.*;

/**
 * Provides WebCentral data to BIRT Data Set.
 * <p>
 * Called by BIRT scripted data set when BIRT report is generated.
 *
 * @author jmartin
 * @author tydykov
 * @author nemtsev
 * @since 23.2
 *
 */
public class BirtDataSet implements Serializable {

    /**
     * Property: serialVersionUID.
     */
    private static final long serialVersionUID = -3687703238967850551L;

    /**
     * Returns an iterator for the WebCentral Data Source. Called by the BIRT scripted data set when
     * the BIRT report is rendered.
     *
     * @param viewFileName Name of the WebCentral view file.
     * @param dataSourceName Name of the dataSource defined in the WebCentral view file.
     * @param dataSourceParameters Data source parameters to be applied to the WebCentral data
     *            source.
     * @return created iterator.
     */
    // Justification: BIRT uses raw type.
    @SuppressWarnings("unchecked")
    public Iterator<Map<String, Object>> getDataIterator(final String viewFileName,
            final String dataSourceName, final JSONObject dataSourceParameters) {

        DataSourceFactory.clearCache();
        final DataSource dataSource =
                DataSourceFactory.loadDataSourceFromFile(viewFileName, dataSourceName);

        if (dataSourceParameters.length() > 0) {
            final Iterator<String> keys = dataSourceParameters.keys();
            while (keys.hasNext()) {
                final String key = keys.next();
                final Object value = dataSourceParameters.get(key);
                dataSource.setParameter(key, value);
            }
        }

        final BirtIterableDataSource iterableDataSource =
                new BirtIterableDataSource((DataSourceImpl) dataSource);

        return iterableDataSource.iterator();
    }
}
